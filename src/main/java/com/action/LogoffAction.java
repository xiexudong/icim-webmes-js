package com.action;

import javax.servlet.http.*; 

import org.apache.struts2.ServletActionContext;

import com.map.SessionMap;
import com.model.UserPO;
import com.opensymphony.xwork2.ActionSupport;

public class LogoffAction extends ActionSupport   {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String logoff(){  
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();

		 /**
		 * 删除SessionMap中的信息
		 */
		UserPO userSession = (UserPO) session.getAttribute("userInfo");
		if(userSession != null){
			SessionMap.sessionMap.remove(userSession.getUser_id().toUpperCase());
		}
		session.removeAttribute("userInfo");
		return ERROR;
	} 

}
