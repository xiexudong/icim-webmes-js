package com.action;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GotoAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private String opeID;
	private String pathNO;
	
	public String gotoOpe() {
		HttpServletRequest request = ServletActionContext.getRequest();
		pathNO = opeID.substring(0, 1) + "000"; 
		return SUCCESS;
	}

	public String getOpeID() {
		return opeID;
	}

	public void setOpeID(String opeID) {
		this.opeID = opeID;
	}

	public String getPathNO() {
		return pathNO;
	}

	public void setPathNO(String pathNO) {
		this.pathNO = pathNO;
	}
	
}
