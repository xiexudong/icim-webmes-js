package com.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

import com.map.SessionMap;
import com.model.IpPO;
import com.model.UserPO;
import com.opensymphony.xwork2.ActionSupport;

/**   
* @Title: FindJsonAction.java
* @Package com.action
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2015-9-8 下午4:07:02    
* @version V1.0   
*/
public class FindSessionAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 1L;

	private String user_id;
	private int isLogin; 
	private String isSingleLogin;//标记账户是否已经登录
	private String loginIp;//登录IP
	private String loginTimestamp;//登录时间
	private Map session;

	public String findSession() {
		UserPO userSession = (UserPO) session.get("userInfo");
		setUser_id(userSession.getUser_id());
		setIsLogin(userSession.getIsLogin());
		return SUCCESS;
	}

	public String findSingleLogin(){
		HttpSession obj = SessionMap.sessionMap.get(user_id.toUpperCase());
		if(obj != null){
			IpPO ipSession = (IpPO) obj.getAttribute("ipInfo");
			if(ipSession!=null){
				setLoginIp(ipSession.getIp());
				setLoginTimestamp(ipSession.getTimestamp());
			}
			setIsSingleLogin("N");
		}else{
			setIsSingleLogin("Y");
		}
		return SUCCESS;
	}
	
	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public int getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(int isLogin) {
		this.isLogin = isLogin;
	}



	public void setSession(Map session) {
		this.session = session;
	}

	public String getIsSingleLogin() {
		return isSingleLogin;
	}

	public void setIsSingleLogin(String isSingleLogin) {
		this.isSingleLogin = isSingleLogin;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public String getLoginTimestamp() {
		return loginTimestamp;
	}

	public void setLoginTimestamp(String loginTimestamp) {
		this.loginTimestamp = loginTimestamp;
	}
}
