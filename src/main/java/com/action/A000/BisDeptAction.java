package com.action.A000;

import com.pojo.Bis_user_main;
import com.pojo.TreeNode;
import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;
import com.service.IBisDeptService;
import com.tx.BisUserfnc.BisUserfncI;
import com.pojo.Bis_dept;


public class BisDeptAction extends ActionSupport{
	private IBisDeptService bisDeptService;
	
	private String nodeId;
	private String nodeName;
	private int nodeLevel = -1;
	private List<Bis_dept> list;
	private List<Bis_user_main>listA;
	private List<TreeNode> root;
	private String testRpy;
	
	public String deptZtree() throws Exception {
		root = new ArrayList<TreeNode>();
		if (nodeLevel == -1) {
			TreeNode node = new TreeNode();
			node.setId("wg");
			node.setName("鸿创");
			node.setIcon("img/home.jpg");
			node.setIsParent(true);
			node.setOpen(true);
			root.add(node);
			return SUCCESS;
			
		}
		if (nodeLevel == 0) {
			list = bisDeptService.getDeptInfo();
			for (Bis_dept c : list) {
				TreeNode node = new TreeNode();
				node.setId(c.getDept_id());
				node.setName(c.getDept_name());
				node.setIsParent(true);
				root.add(node);
			}
		}
		if( nodeLevel == 1 ){
			BisUserfncI bisUserfncI = new BisUserfncI();
			bisUserfncI.setDept_id_fk(nodeId);
			System.out.println("test");
			listA = bisDeptService.getUserByDeptInfo(bisUserfncI);
			for (Bis_user_main c : listA) {
				TreeNode node = new TreeNode();
				node.setId(c.getUsr_id());
				node.setName(c.getUsr_name());
				node.setIcon("img/user.jpg");
				node.setIsParent(false);
				root.add(node);
			}
		}
		return SUCCESS;
	}
//	List<Bis_dept>
	public String deptInfo() throws Exception {
		list = bisDeptService.getDeptInfo();
		return SUCCESS;
	}
	public IBisDeptService getBisDeptService() {
		return bisDeptService;
	}
	public void setBisDeptService(IBisDeptService bisDeptService) {
		this.bisDeptService = bisDeptService;
	}
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public int getNodeLevel() {
		return nodeLevel;
	}
	public void setNodeLevel(int nodeLevel) {
		this.nodeLevel = nodeLevel;
	}
	public List<TreeNode> getRoot() {
		return root;
	}
	public String getTestRpy() {
		return testRpy;
	}
	public List<Bis_dept> getList() {
		return list;
	}
	
}
