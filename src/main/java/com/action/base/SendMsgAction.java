package com.action.base;
 
import com.opensymphony.xwork2.ActionSupport;
import com.service.ISendMsgService;
import com.util.JsonXMLConvert;

public class SendMsgAction  extends ActionSupport{
 
	private static final long serialVersionUID = 1L;
	
	private ISendMsgService sendMsgService; 

	private String strInMsg;
	private String strInMsgXML;
	private String strOutMsg;
	private String strOutMsgXML;
	public String sendWithRepaly(){ 
		JsonXMLConvert jx = new JsonXMLConvert();
		strInMsgXML =jx.toXml(strInMsg);
		strOutMsgXML = sendMsgService.sendWithRepaly(strInMsgXML);
		strOutMsg = jx.toJson(strOutMsgXML);
		return SUCCESS;
	}
	
	public void setStrInMsgXML(String strInMsgXML) {
		this.strInMsgXML = strInMsgXML;
	}

	public void setStrOutMsgXML(String strOutMsgXML) {
		this.strOutMsgXML = strOutMsgXML;
	}

	public void setSendMsgService(ISendMsgService sendMsgService) {
		this.sendMsgService = sendMsgService;
	}

	public String getStrInMsg() {
		return strInMsg;
	}

	public void setStrInMsg(String strInMsg) {
		this.strInMsg = strInMsg;
	}

	public String getStrOutMsg() {
		return strOutMsg;
	}

	public void setStrOutMsg(String strOutMsg) {
		this.strOutMsg = strOutMsg;
	}

}
