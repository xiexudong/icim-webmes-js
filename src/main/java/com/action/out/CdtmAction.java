package com.action.out;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.out.cdtm.model.CsotFilePo;
import com.out.cdtm.service.impl.CtmtMainService;
import com.tx.CdtmOut.CdtmOutO;

public class CdtmAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private CtmtMainService ctmtMainService;
	private CdtmOutO cdtmOutO;

	private String boxId;
	private String fileName;

	public CsotFilePo buildXmlString(String boxId) throws Exception {
		return  ctmtMainService.subMainProc(boxId,
				CtmtMainService.UPLOAD_XML_ACTION);
		
	}


	public InputStream getInputStream() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String boxId = request.getParameter("boxId");
		CsotFilePo csotFilePo = buildXmlString(boxId);
		ByteArrayInputStream in = new ByteArrayInputStream(csotFilePo.getFileContent().getBytes());
		fileName = csotFilePo.getFileName();
		return in;
	}

	public String exportFile() {
		return SUCCESS;
	}

	public CtmtMainService getCtmtMainService() {
		return ctmtMainService;
	}

	public void setCtmtMainService(CtmtMainService ctmtMainService) {
		this.ctmtMainService = ctmtMainService;
	}

	public String getBoxId() {
		return boxId;
	}

	public void setBoxId(String boxId) {
		this.boxId = boxId;
	}

	public CdtmOutO getCdtmOutO() {
		return cdtmOutO;
	}

	public void setCdtmOutO(CdtmOutO cdtmOutO) {
		this.cdtmOutO = cdtmOutO;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
