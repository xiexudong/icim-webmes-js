package com.action;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.map.SessionMap;
import com.model.IpPO;
import com.model.UserPO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.pojo.Bis_user_main;
import com.service.IUASCheckMainService;
import com.tx.UasCheckMain.UasCheckMainI;

public class LoginAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private IUASCheckMainService uasCheckMainService;
	private UasCheckMainI uasCheckMainI;
	private String userName;
	private String passWord;
	private String url_param;




	String message = null;
	List<Bis_user_main> uasCheckMainList = new ArrayList<Bis_user_main>();
	
	public String login() throws Exception {
		int reason_code=0;
		HttpServletRequest request = ServletActionContext.getRequest();
		uasCheckMainI = new UasCheckMainI();
		ActionContext ctx = ActionContext.getContext();
		
		if (userName == null || userName.length() <= 0 || userName == null
				|| passWord.length() <= 0) {
			return ERROR;
		}
		UserPO user = new UserPO();
//		IpPO ipInfo = new IpPO();
		user.setUser_id(userName.toUpperCase());
		user.setUser_password(passWord);
		uasCheckMainI.setUsr_id(userName.toUpperCase());
		uasCheckMainList = uasCheckMainService.findPSWDMain(uasCheckMainI);
		
		if( uasCheckMainList.size()==0 ){
			reason_code=1;
			url_param = "usr_id="+userName+"&reason_code="+reason_code;
/*			message = "<p>账号或者密码错误</p>"
					+ "<p></p>";
				ctx.put("message", message);
				ctx.put("title", "访问拒绝");*/
			return ERROR;
		}else{
			Bis_user_main bis_data_main = uasCheckMainList.get(0);
			bis_data_main.getUsr_key();

			if( passWord.equals(bis_data_main.getUsr_key())){
				
				if("Y".equals(bis_data_main.getLock_flg())){
					message = "<p>User is locked</p>"
							+ "<p>登陆人员已被锁定，请确认！</p>";
						ctx.put("message", message);
						ctx.put("title", "访问拒绝");
						return "denied";     				
				}else{
//					HttpSession obj = (HttpSession)SessionMap.sessionMap.get(userName.toUpperCase());
//					if(obj != null){
//						obj.removeAttribute("userInfo");
//					}
					user.setIsLogin(1);
				 	UserPO userSession = (UserPO) ctx.getSession().get("userInfo");
				 	if (userSession == null) {
				 		ctx.getSession().put("userInfo", user);
//				 		ipInfo.setIp(request.getRemoteHost());
//				 		ipInfo.setTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//				 		ctx.getSession().put("ipInfo", ipInfo);
				 	} else {
				 		user = userSession;
				 	}
//				 	SessionMap.sessionMap.put(userName.toUpperCase(), request.getSession());
					return SUCCESS;
				}
			}else{
				reason_code=2;
				url_param = "usr_id="+userName+"&reason_code="+reason_code;
				return ERROR;
			}
		}
		 
	}


	public String getUrl_param() {
		return url_param;
	}
	public void setUrl_param(String url_param) {
		this.url_param = url_param;
	}
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	public void setUasCheckMainService(IUASCheckMainService uasCheckMainService) {
		this.uasCheckMainService = uasCheckMainService;
	}
}
