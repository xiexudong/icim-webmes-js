package com.action;

import java.util.Map;

import com.model.UserPO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.service.impl.UserGfncService;
import com.tx.UasUserGfnc.UasUserGfncI;

public class UserGfncAction extends ActionSupport{
	private UserGfncService userGfncService;
	private UasUserGfncI uasUserGfncI = new UasUserGfncI();
	private boolean flag;
	private String function_code;
	private String check_admin_flg;
	ActionContext ctx = ActionContext.getContext();
	Map session = ctx.getSession();
	
	public String checkUserAuthority()throws Exception{
		UserPO user =  (UserPO) session.get("userInfo");
		String message= null;
		if (user == null) {
			message = "<p>Not login or login time out, please login again</p>"
				+ "<p>未登录或登录超时, 请重新登录</p>";
			ctx.put("message", message);
			ctx.put("title", "访问拒绝");
			return "denied";
		}else{
			String user_id = ((UserPO) session.get("userInfo"))
			.getUser_id();
			uasUserGfncI.setUsr_id(user_id);
			uasUserGfncI.setFunc_code(function_code);
			if(check_admin_flg.equals("N")){
				flag = userGfncService.checkAuthority(uasUserGfncI);
			}else{
				flag =userGfncService.checkAdmin(uasUserGfncI);
				if( flag == false){
					flag = userGfncService.checkAuthority(uasUserGfncI);
				}
			}
	
		}
		
		return SUCCESS;
	}
	public String checkUserAdmin () throws Exception{
		String user_id = ((UserPO) session.get("userInfo"))
		.getUser_id();
		uasUserGfncI.setUsr_id(user_id);
		flag =userGfncService.checkAdmin(uasUserGfncI);
		
		return SUCCESS;
	}
	public void setUserGfncService(UserGfncService userGfncService) {
		this.userGfncService = userGfncService;
	}

	public String getFunction_code() {
		return function_code;
	}

	public void setFunction_code(String function_code) {
		this.function_code = function_code;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getCheck_admin_flg() {
		return check_admin_flg;
	}
	public void setCheck_admin_flg(String check_admin_flg) {
		this.check_admin_flg = check_admin_flg;
	}

	
}
