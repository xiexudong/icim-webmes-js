package com.action.spc;

import com.opensymphony.xwork2.ActionSupport;
import com.pojo.Spc_bis_grp_main;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.service.ISPCGroupService;
import com.tx.SpcGroupMain.SpcGroupMainI;
import com.tx.SpcGroupMain.SpcGroupMainO;
public class SPCGroupAction extends ActionSupport{
	private ISPCGroupService spcGroupService;
	private String grp_name;
	private String col_typ;
	private String grp_no;
//	private List<Spc_bis_grp_main> spcBisGrpMainList;
	private SpcGroupMainO outTrxObj = new SpcGroupMainO();
	public String findSpcGroupMain(){
		List<Spc_bis_grp_main> spcBisGrpMainList = new ArrayList<Spc_bis_grp_main>();
		try{
			SpcGroupMainI spcGroupMain = new SpcGroupMainI();
			spcGroupMain.setCol_typ(col_typ);
			spcGroupMain.setGrp_name(grp_name);
			spcBisGrpMainList = spcGroupService.findSPCGroupMain(spcGroupMain);
		}catch(Exception ex){
			ex.printStackTrace();
			outTrxObj.setRtn_code(1);
			return SUCCESS;
		}
		if(spcBisGrpMainList.size()>0){
			grp_name = spcBisGrpMainList.get(0).getGrp_name() ;
		}
		outTrxObj.setRtn_code(0);
		outTrxObj.setOary(spcBisGrpMainList);
		return SUCCESS;
	}
	public String findScpGroupDt(){
		
		return SUCCESS;
	}
	public String getGrp_name() {
		return grp_name;
	}
	
	public void setGrp_name(String grp_name) {
		this.grp_name = grp_name;
	}
	public void setCol_typ(String col_typ) {
		this.col_typ = col_typ;
	}
	public void setSpcGroupService(ISPCGroupService spcGroupService) {
		this.spcGroupService = spcGroupService;
	}


	public SpcGroupMainO getOutTrxObj() {
		return outTrxObj;
	}


	
	
}
