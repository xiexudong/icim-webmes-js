package com.action.spc;

import com.opensymphony.xwork2.ActionSupport;
import com.pojo.Spc_bis_grp_dt;
import com.pojo.Spc_bis_grp_main;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.service.ISPCGroupService;
import com.tx.SpcGroupDt.SpcGroupDtO;
import com.tx.SpcGroupDt.SpcGroupDtI;
public class SPCGroupDtAction extends ActionSupport{
	private ISPCGroupService spcGroupService;
//	private String grp_name;
	private String col_typ;
	private String grp_no;
	private String sub_grp_no;
	private SpcGroupDtO outTrxObj = new SpcGroupDtO();
	public String findSpcGroupDT(){
		List<Spc_bis_grp_dt> spcBisGrpDtList = new ArrayList<Spc_bis_grp_dt>();
		try{
			SpcGroupDtI spcGroupDtI = new SpcGroupDtI();
			spcGroupDtI.setGrp_no(grp_no);
			spcGroupDtI.setSub_grp_no(sub_grp_no);
//			spcGroupDtI.setCol_typ(col_typ);
//			spcGroupDtI.setGrp_name(grp_name);
//			spcBisGrpDtList = spcGroupService.findSPCGroupDT(spcGroupDtI);
		}catch(Exception ex){
			ex.printStackTrace();
			outTrxObj.setRtn_code(1);
			return SUCCESS;
		}
//		if(spcBisGrpDtList.size()>0){
//			grp_name = spcBisGrpDtList.get(0).getGrp_name() ;
//		}
		outTrxObj.setRtn_code(0);
		outTrxObj.setOary(spcBisGrpDtList);
		return SUCCESS;
	}
	public void setCol_typ(String col_typ) {
		this.col_typ = col_typ;
	}
	
	public void setGrp_no(String grp_no) {
		this.grp_no = grp_no;
	}
	public void setSub_grp_no(String sub_grp_no) {
		this.sub_grp_no = sub_grp_no;
	}
	public void setSpcGroupService(ISPCGroupService spcGroupService) {
		this.spcGroupService = spcGroupService;
	}
	public SpcGroupDtO getOutTrxObj() {
		return outTrxObj;
	}




	
	
}
