package com.action.spc;

import com.opensymphony.xwork2.ActionSupport;
import com.pojo.Spc_ret_chart_raw;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.service.ISPCChartRawService;
import com.tx.SpcChartRaw.SpcChartRawI;
import com.tx.SpcChartRaw.SpcChartRawO;
public class SPCChartRawAction extends ActionSupport{
	private String col_typ_fk;
	private String grp_no_fk;
	private String chart_no_fk;
	private Double input_time_d;
	private ISPCChartRawService spcChartRawService;
	private SpcChartRawO outTrxObj = new SpcChartRawO();
	public String findSpcChartRaw(){
		List<Spc_ret_chart_raw> spcRetChartRawList = new ArrayList<Spc_ret_chart_raw>();
		try{
			SpcChartRawI spcChartRawI = new SpcChartRawI();
			spcChartRawI.setCol_typ_fk(col_typ_fk);
			spcChartRawI.setGrp_no_fk(grp_no_fk);
			spcChartRawI.setChart_no_fk(chart_no_fk);
			spcChartRawI.setProc_time_d(input_time_d);
			
			spcRetChartRawList = spcChartRawService.findSPCChartRaw(spcChartRawI);
		}catch(Exception ex){
			ex.printStackTrace();
			outTrxObj.setRtn_code(1);
			return SUCCESS;
		}
		outTrxObj.setRtn_code(0);
		outTrxObj.setOary(spcRetChartRawList);
		return SUCCESS;
	}

	public void setCol_typ_fk(String col_typ_fk) {
		this.col_typ_fk = col_typ_fk;
	}

	public void setGrp_no_fk(String grp_no_fk) {
		this.grp_no_fk = grp_no_fk;
	}

	public void setChart_no_fk(String chart_no_fk) {
		this.chart_no_fk = chart_no_fk;
	}

	public void setInput_time_d(Double input_time_d) {
		this.input_time_d = input_time_d;
	}

	public SpcChartRawO getOutTrxObj() {
		return outTrxObj;
	}

	public void setSpcChartRawService(ISPCChartRawService spcChartRawService) {
		this.spcChartRawService = spcChartRawService;
	}
	
}
