package com.action.spc;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.service.ISPCChartMainService;
import com.tx.SpcChartMain.SpcChartMainI;
import com.tx.SpcChartMain.SpcChartMainO;
import com.pojo.Spc_bis_chart_main;
public class SPCChartMainAction extends ActionSupport{
	private ISPCChartMainService spcChartMainService;
	private String col_typ_fk;
	private String grp_no_fk;
	private String chart_no;
	private SpcChartMainO outTrxObj = new SpcChartMainO();
	public String findSPCChartMain(){
		List<Spc_bis_chart_main> spcBisChartMainList = new ArrayList<Spc_bis_chart_main>();
		try{
			SpcChartMainI spcChartMainI = new SpcChartMainI();
			spcChartMainI.setCol_typ_fk(col_typ_fk);
			spcChartMainI.setGrp_no_fk(grp_no_fk);
			spcChartMainI.setChart_no(chart_no);
			spcBisChartMainList = spcChartMainService.findChartMain(spcChartMainI);
		}catch(Exception ex){
			ex.printStackTrace();
			outTrxObj.setRtn_code(1);
			return SUCCESS;
		}
		outTrxObj.setRtn_code(0);
		outTrxObj.setOary(spcBisChartMainList);
		return SUCCESS;
	}


	public void setSpcChartMainService(ISPCChartMainService spcChartMainService) {
		this.spcChartMainService = spcChartMainService;
	}


	public SpcChartMainO getOutTrxObj() {
		return outTrxObj;
	}

	public void setCol_typ_fk(String col_typ_fk) {
		this.col_typ_fk = col_typ_fk;
	}

	public void setGrp_no_fk(String grp_no_fk) {
		this.grp_no_fk = grp_no_fk;
	}

	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}


	
	
}
