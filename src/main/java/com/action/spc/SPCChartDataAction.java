package com.action.spc;

import com.opensymphony.xwork2.ActionSupport;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.service.ISPCChartDataService;
import com.service.ISPCChartRawService;
import com.service.ISPCGroupService;
import com.tx.SpcChartData.SpcChartDataI;
import com.tx.SpcChartData.SpcChartDataO;
import com.tx.SpcChartRaw.SpcChartRawI;
//import com.tx.SpcChartRaw.SpcChartRawO;
import com.pojo.Spc_ret_chart_data;
import com.pojo.Spc_ret_chart_raw;
public class SPCChartDataAction extends ActionSupport{
	private ISPCChartDataService spcChartDataService;
	private ISPCChartRawService spcChartRawService;
	private String col_typ_fk;
	private String grp_no_fk;
	private String chart_no_fk;
	private String chart_typ_fk;
	private SpcChartDataO outTrxObj = new SpcChartDataO();
	public String findChartData(){
		List<Spc_ret_chart_data> spcBisRetChartDataList = new ArrayList<Spc_ret_chart_data>();
		try{
			SpcChartDataI spcChartDataI = new SpcChartDataI();
			spcChartDataI.setCol_typ_fk(col_typ_fk);
			spcChartDataI.setGrp_no_fk(grp_no_fk);
			spcChartDataI.setChart_no_fk(chart_no_fk);
			spcChartDataI.setChart_typ_fk(chart_typ_fk);
			spcBisRetChartDataList = spcChartDataService.findChartDataFnc(spcChartDataI);
			if(("01").equals(chart_typ_fk) ){
				for( int i=0;i<spcBisRetChartDataList.size();i++ ){
					Spc_ret_chart_data spc_ret_chart_data = spcBisRetChartDataList.get(i);
					Timestamp input_timestamp = spc_ret_chart_data.getInput_timestamp();
					SpcChartRawI spcChartRawI = new SpcChartRawI();
					spcChartRawI.setCol_typ_fk(col_typ_fk);
					spcChartRawI.setGrp_no_fk(grp_no_fk);
					spcChartRawI.setChart_no_fk(chart_no_fk);
					spcChartRawI.setProc_time_d(spc_ret_chart_data.getInput_time_d());
					
					List<Spc_ret_chart_raw> spcChartRawList =spcChartRawService.findSPCChartRaw(spcChartRawI);
					spcBisRetChartDataList.get(i).setRawData(spcChartRawList);
				}
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			outTrxObj.setRtn_code(1);
			return SUCCESS;
		}
		outTrxObj.setRtn_code(0);
		outTrxObj.setOary(spcBisRetChartDataList);
		return SUCCESS;
	}
	public String findScpGroupDt(){
		
		return SUCCESS;
	}


	public void setSpcChartDataService(ISPCChartDataService spcChartDataService) {
		this.spcChartDataService = spcChartDataService;
	}
	public SpcChartDataO getOutTrxObj() {
		return outTrxObj;
	}
	public void setCol_typ_fk(String col_typ_fk) {
		this.col_typ_fk = col_typ_fk;
	}
	public void setGrp_no_fk(String grp_no_fk) {
		this.grp_no_fk = grp_no_fk;
	}
	public void setChart_no_fk(String chart_no_fk) {
		this.chart_no_fk = chart_no_fk;
	}
	public void setChart_typ_fk(String chart_typ_fk) {
		this.chart_typ_fk = chart_typ_fk;
	}
	public void setSpcChartRawService(ISPCChartRawService spcChartRawService) {
		this.spcChartRawService = spcChartRawService;
	}


	
	
}
