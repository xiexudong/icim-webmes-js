package com.action.spc;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.service.ISPCChartSettingService;
import com.tx.SpcChartSetting.SpcChartSettingI;
import com.tx.SpcChartSetting.SpcChartSettingO;
import com.pojo.Spc_chart_setting;
public class SPCChartSettingAction extends ActionSupport{
	private ISPCChartSettingService spcChartSettingService;
	private String col_typ;
	private String grp_no;
	private String chart_no;
	private String chart_typ;
	private SpcChartSettingO outTrxObj = new SpcChartSettingO();
	public String findSPCChartSetting(){
		List<Spc_chart_setting> spcBisRetChartDataList = new ArrayList<Spc_chart_setting>();
		try{
			SpcChartSettingI spcChartSettingI = new SpcChartSettingI();
			spcChartSettingI.setCol_typ(col_typ);
			spcChartSettingI.setGrp_no(grp_no);
			spcChartSettingI.setChart_no(chart_no);
			spcChartSettingI.setChart_typ(chart_typ);
			spcBisRetChartDataList = spcChartSettingService.findChartSetting(spcChartSettingI);
		}catch(Exception ex){
			ex.printStackTrace();
			outTrxObj.setRtn_code(1);
			return SUCCESS;
		}
		outTrxObj.setRtn_code(0);
		outTrxObj.setOary(spcBisRetChartDataList);
		return SUCCESS;
	}

	public void setSpcChartSettingService(
			ISPCChartSettingService spcChartSettingService) {
		this.spcChartSettingService = spcChartSettingService;
	}
	public void setCol_typ(String col_typ) {
		this.col_typ = col_typ;
	}
	public void setGrp_no(String grp_no) {
		this.grp_no = grp_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public void setChart_typ(String chart_typ) {
		this.chart_typ = chart_typ;
	}
	public SpcChartSettingO getOutTrxObj() {
		return outTrxObj;
	}


	
	
}
