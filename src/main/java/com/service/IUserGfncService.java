package com.service;

//po import 
//tx import 
import com.tx.UasUserGfnc.UasUserGfncI;
public interface IUserGfncService {

	public boolean checkAuthority(UasUserGfncI uasUserGfncI);
	public boolean checkAdmin(UasUserGfncI uasUserGfncI);

}
