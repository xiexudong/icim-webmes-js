package com.service;

import java.util.List;
//po import 
import com.pojo.Spc_ret_chart_raw;
//tx import 
import com.tx.SpcChartRaw.SpcChartRawI;
public interface ISPCChartRawService {

	public List<Spc_ret_chart_raw> findSPCChartRaw(SpcChartRawI spcChartRawI);
}
