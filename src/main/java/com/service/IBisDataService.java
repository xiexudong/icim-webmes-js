package com.service;

import java.util.List;

import com.pojo.Bis_data;
import com.tx.BisDatafnc.BisDatafncI;

public interface IBisDataService {
	public List<Bis_data> findFabMain(BisDatafncI bisDatafncI);
}
