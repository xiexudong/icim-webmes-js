package com.service;

import java.util.List;
//po import 
import com.pojo.Spc_chart_setting;
//tx import 
import com.tx.SpcChartSetting.SpcChartSettingI;
public interface ISPCChartSettingService {

	public List<Spc_chart_setting> findChartSetting (SpcChartSettingI spcChartSettingI);
}
