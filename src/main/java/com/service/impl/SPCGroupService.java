package com.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import com.service.ISPCGroupService;
import com.dao.SPCGroupDao;
import com.pojo.Spc_bis_grp_dt;
import com.pojo.Spc_bis_grp_main;
import com.tx.SpcGroupDt.SpcGroupDtI;
import com.tx.SpcGroupMain.SpcGroupMainI;
import com.util.Printer;

@Component("spcGroupService")
public class SPCGroupService implements ISPCGroupService{

	private Printer printer = new Printer(SPCGroupService.class);
	private SPCGroupDao spcGroupDao;

	@Override
	public List<Spc_bis_grp_main> findSPCGroupMain(SpcGroupMainI spcGroupMain) {
		List<Spc_bis_grp_main> spcBisGrpMainList= spcGroupDao.findSPCGroupMain(spcGroupMain);
		return spcBisGrpMainList;
//		String grp_name = spcBisGrpMainList;
//		return grp_name;
	}

	public SPCGroupDao getSpcGroupDao() {
		return spcGroupDao;
	}

	public void setSpcGroupDao(SPCGroupDao spcGroupDao) {
		this.spcGroupDao = spcGroupDao;
	}

//	@Override
//	public List<Spc_bis_grp_dt> findSPCGroupDT(SpcGroupDtI spcGroupDtI) {
//		List<Spc_bis_grp_dt> spcBisGrpDtList = spcGroupDao.findSPCGroupDT(spcGroupDtI);
//		return null;
//	}


}
