package com.service.impl;

import java.util.List;

import com.dao.BisDeptDao;
import com.pojo.Bis_dept;
import com.pojo.Bis_user_main;
import com.service.IBisDeptService;
import com.tx.BisDeptfnc.BisDeptfncI;
import com.tx.BisUserfnc.BisUserfncI;

public class BisDeptService implements IBisDeptService {

	private BisDeptDao bisDeptDao;

	public void setBisDeptDao(BisDeptDao bisDeptDao) {
		this.bisDeptDao = bisDeptDao;
	}
	public List<Bis_dept> getDeptInfo() {
		List<Bis_dept> bisDeptList= bisDeptDao.getDeptInfo();
		return bisDeptList;
	}
	
	public List<Bis_user_main> getUserByDeptInfo(BisUserfncI bisUserfncI) {
		List<Bis_user_main> bisUserList= bisDeptDao.getUserByDeptInfo(bisUserfncI);
		return bisUserList;
	}
}
