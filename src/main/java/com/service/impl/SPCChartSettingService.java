package com.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import com.dao.SPCChartSettingDao;
import com.pojo.Spc_chart_setting;
import com.pojo.Spc_ret_chart_data;
//tx import 
import com.service.ISPCChartSettingService;
import com.tx.SpcChartSetting.SpcChartSettingI;
import com.util.Printer;

public class SPCChartSettingService implements ISPCChartSettingService{

	private Printer printer = new Printer(SPCChartSettingService.class);
	private SPCChartSettingDao spcChartSettingDao;

	public void setSpcChartSettingDao(SPCChartSettingDao spcChartSettingDao) {
		this.spcChartSettingDao = spcChartSettingDao;
	}

	@Override
	public List<Spc_chart_setting> findChartSetting(SpcChartSettingI spcChartSettingI) {
		return spcChartSettingDao.findSPCChartSetting(spcChartSettingI);
	}


}
