package com.service.impl;

import com.dao.UserGfncDao;
import com.service.IUserGfncService;
import com.tx.UasUserGfnc.UasUserGfncI;

public class UserGfncService implements IUserGfncService {

	private UserGfncDao userGfncDao;

	public UserGfncDao getUserGfncDao() {
		return userGfncDao;
	}

	public void setUserGfncDao(UserGfncDao userGfncDao) {
		this.userGfncDao = userGfncDao;
	}

	public boolean checkAuthority(UasUserGfncI uasUserGfncI) {
		return userGfncDao.getByUserAndFunction(uasUserGfncI).size() > 0;
	}

	public boolean checkAdmin(UasUserGfncI uasUserGfncI) {
		return userGfncDao.getByAdminUser(uasUserGfncI).size() > 0;
	}

}
