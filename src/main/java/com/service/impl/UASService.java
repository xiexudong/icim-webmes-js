package com.service.impl;

import com.model.UserPO;
import com.service.IUASService;
import com.util.Printer;

public class UASService implements IUASService {
	private Printer printer = new Printer(UASService.class);

	private static final String USER_ADMIN = "prod";

	@Override
	public boolean checkPassword(UserPO user) {
		if (user.getUser_id().equals(USER_ADMIN)
				&& user.getUser_password().equals("@prod")) {
			return true;
		}
		printer.info("[" + user.getUser_id() + "][" + user.getUser_password()
				+ "] login failed!");
		return false;
	}

	@Override
	public boolean gotoCheck(String ope, UserPO user) {
		if (user.getUser_id().equals(USER_ADMIN)) {
			return true;
		}
		return false;
	}

}

