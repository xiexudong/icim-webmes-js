package com.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import com.dao.SPCChartDataDao;
import com.pojo.Spc_ret_chart_data;
//tx import 
import com.service.ISPCChartDataService;
import com.tx.SpcChartData.SpcChartDataI;
import com.tx.SpcChartData.SpcChartDataO;
import com.util.Printer;

@Component("spcGroupService")
public class SPCChartDataService implements ISPCChartDataService{

	private Printer printer = new Printer(SPCChartDataService.class);
	private SPCChartDataDao spcChartDataDao;

	@Override
	public List<Spc_ret_chart_data> findChartDataFnc(SpcChartDataI spcChartDataI) {
		List<Spc_ret_chart_data> spcRetChartDataList= spcChartDataDao.findChartDataFnc(spcChartDataI);
		return spcRetChartDataList;
	}

	public void setSpcChartDataDao(SPCChartDataDao spcChartDataDao) {
		this.spcChartDataDao = spcChartDataDao;
	}




}
