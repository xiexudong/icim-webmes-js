package com.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;
//tx import 
import com.service.IUASCheckMainService;
import com.tx.UasCheckMain.UasCheckMainI;
import com.dao.UASCheckMainDao;
import com.pojo.Bis_user_main;
import com.util.Printer;
@Component("uasheckMainService")
public class UASCheckMainService implements IUASCheckMainService{

	private Printer printer = new Printer(UASCheckMainService.class);
	private UASCheckMainDao uasCheckMainDao;


	public void setUasCheckMainDao(UASCheckMainDao uasCheckMainDao) {
		this.uasCheckMainDao = uasCheckMainDao;
	}


	@Override
	public List<Bis_user_main> findPSWDMain(UasCheckMainI uasCheckMainI) {
		return uasCheckMainDao.findPSWDMain(uasCheckMainI);
	}

}
