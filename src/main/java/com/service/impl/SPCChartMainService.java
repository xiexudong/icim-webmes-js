package com.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import com.dao.SPCChartMainDao;
import com.pojo.Spc_chart_setting;
import com.pojo.Spc_ret_chart_data;
//tx import 
import com.service.ISPCChartMainService;
import com.service.ISPCChartSettingService;
import com.pojo.Spc_bis_chart_main;
import com.tx.SpcChartMain.SpcChartMainI;
import com.util.Printer;

public class SPCChartMainService implements ISPCChartMainService{

	private Printer printer = new Printer(SPCChartMainService.class);
	private SPCChartMainDao spcChartMainDao;


	public void setSpcChartMainDao(SPCChartMainDao spcChartMainDao) {
		this.spcChartMainDao = spcChartMainDao;
	}

	@Override
	public List<Spc_bis_chart_main> findChartMain(SpcChartMainI spcChartMainI) {
		return spcChartMainDao.findSPCChartMain(spcChartMainI);
	}





}
