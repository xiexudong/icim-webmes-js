package com.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import com.service.ISPCChartRawService;
import com.dao.SPCChartRawDao;
import com.pojo.Spc_bis_grp_dt;
import com.pojo.Spc_bis_grp_main;
import com.pojo.Spc_ret_chart_raw;
import com.tx.SpcChartRaw.SpcChartRawI;
import com.tx.SpcGroupDt.SpcGroupDtI;
import com.tx.SpcGroupMain.SpcGroupMainI;
import com.util.Printer;

public class SPCChartRawService implements ISPCChartRawService{

	private Printer printer = new Printer(SPCChartRawService.class);
	private SPCChartRawDao spcChartRawDao;



	public SPCChartRawDao getSpcChartRawDao() {
		return spcChartRawDao;
	}



	public void setSpcChartRawDao(SPCChartRawDao spcChartRawDao) {
		this.spcChartRawDao = spcChartRawDao;
	}



	@Override
	public List<Spc_ret_chart_raw> findSPCChartRaw(SpcChartRawI spcChartRawI) {
		if(spcChartRawDao==null){
			spcChartRawDao = new SPCChartRawDao();
		} 
		return  spcChartRawDao.findSPCChartRaw(spcChartRawI);
	}



}
