package com.service.impl;

 
import org.springframework.stereotype.Component;

import com.icim.rest.RestCallManager;
import com.service.ISendMsgService;
import com.util.Printer;


@Component("sendMsgService")
public class SendMsgService implements ISendMsgService{

	private Printer printer = new Printer(SendMsgService.class);
	
	private RestCallManager restCallManager;
	
	@Override
	public String sendWithRepaly(String strInMsg) {
		int index = strInMsg.indexOf("<trx_id>");
		if(index == -1){
			return "the xml not have trx_id property";
		}
		String appName = strInMsg.substring(index+8, index+16)+"I";
		printer.info(strInMsg);
		String strOutMsg = RestCallManager.sendMessage(appName, strInMsg);
		printer.info(strOutMsg);
		return strOutMsg;
	}

	public RestCallManager getRestCallManager() {
		return restCallManager;
	}

	public void setRestCallManager(RestCallManager restCallManager) {
		this.restCallManager = restCallManager;
	}
	
	
}
