package com.service.impl;

import java.util.List;

import com.dao.BisDataDao;
import com.pojo.Bis_data;
import com.service.IBisDataService;
import com.tx.BisDatafnc.BisDatafncI;

public class BisDataService implements IBisDataService{
	private BisDataDao bisDataDao;
	
	public List<Bis_data> findFabMain(BisDatafncI bisDatafncI) {
		List<Bis_data> bisFabList= bisDataDao.findFabMain(bisDatafncI);
		return bisFabList;
	}

	public void setBisDataDao(BisDataDao bisDataDao) {
		this.bisDataDao = bisDataDao;
	}
	
}
