package com.service;

import java.util.List;
//po import 
import com.pojo.Spc_bis_grp_main;
import com.pojo.Spc_bis_grp_dt;
//tx import 
import com.tx.SpcGroupMain.SpcGroupMainI;
import com.tx.SpcGroupDt.SpcGroupDtI;
public interface ISPCGroupService {

	public List<Spc_bis_grp_main> findSPCGroupMain(SpcGroupMainI spcGroupMain);
//	public List<Spc_bis_grp_dt> findSPCGroupDT(SpcGroupDtI spcGroupDtI);
}
