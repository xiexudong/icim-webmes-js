package com.service;

import java.util.List;
//po import 
import com.pojo.Spc_ret_chart_data;
//tx import 
import com.tx.SpcChartData.SpcChartDataI;
import com.tx.SpcChartData.SpcChartDataO;
public interface ISPCChartDataService {

	public List<Spc_ret_chart_data> findChartDataFnc(SpcChartDataI spcChartDataI);
}
