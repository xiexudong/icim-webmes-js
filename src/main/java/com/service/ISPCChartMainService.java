package com.service;

import java.util.List;
//po import 
import com.pojo.Spc_bis_chart_main;
//tx import 
import com.tx.SpcChartMain.SpcChartMainI;
public interface ISPCChartMainService {

	public List<Spc_bis_chart_main> findChartMain(SpcChartMainI spcChartMainI);
}
