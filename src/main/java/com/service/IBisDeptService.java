package com.service;

import java.util.List;

import com.pojo.Bis_dept;
import com.pojo.Bis_user_main;
import com.tx.BisUserfnc.BisUserfncI;

//po import 
//tx import 

public interface IBisDeptService {

	public List<Bis_dept> getDeptInfo();
	public List<Bis_user_main> getUserByDeptInfo(BisUserfncI bisUserfncI);


}
