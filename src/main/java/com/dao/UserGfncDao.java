package com.dao;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.pojo.UserGfncPO;
import com.tx.UasUserGfnc.UasUserGfncI;


public class UserGfncDao extends SqlMapClientDaoSupport {

	public List<UserGfncPO> getByUserAndFunction(UasUserGfncI uasUserGfncI) {
		return getSqlMapClientTemplate().queryForList(
				"userGfnc.getByUserAndFunction", uasUserGfncI);
	}
	public List<UserGfncPO> getByAdminUser(UasUserGfncI uasUserGfncI) {
		return getSqlMapClientTemplate().queryForList(
				"userGfnc.getByAdminUser", uasUserGfncI);
	}

}
