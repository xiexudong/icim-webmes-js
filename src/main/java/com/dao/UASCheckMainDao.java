package com.dao;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Component;

import com.pojo.Bis_user_main;
import com.tx.UasCheckMain.UasCheckMainI;
public class UASCheckMainDao extends SqlMapClientDaoSupport {
	public List<Bis_user_main> findPSWDMain(UasCheckMainI uasCheckMainI)  {
		List<Bis_user_main> uasCheckMainList = 
			getSqlMapClientTemplate().queryForList("bis_user_main.findPSWDMain",uasCheckMainI);
		return uasCheckMainList;
	}

}
