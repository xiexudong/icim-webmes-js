package com.dao;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.pojo.Bis_dept;
import com.pojo.Bis_user_main;
import com.tx.BisDeptfnc.BisDeptfncI;
import com.tx.BisUserfnc.BisUserfncI;


public class BisDeptDao extends SqlMapClientDaoSupport {

	public List<Bis_dept> getDeptInfo() {
		return getSqlMapClientTemplate().queryForList(
				"bis_dept.getDeptInfo");
	}
	public List<Bis_user_main> getUserByDeptInfo(BisUserfncI bisUserfncI) {
		return getSqlMapClientTemplate().queryForList(
				"bis_dept.getUserByDeptInfo", bisUserfncI);
	}
}
