package com.dao;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Component;

import com.pojo.Spc_chart_setting;
import com.tx.SpcChartSetting.SpcChartSettingI;
public class SPCChartSettingDao extends SqlMapClientDaoSupport {
	public List<Spc_chart_setting> findSPCChartSetting(SpcChartSettingI spcChartSettingI)  {
		List<Spc_chart_setting> spcChartSettingList = 
			getSqlMapClientTemplate().queryForList("spc_chart_setting.findChartSetting",spcChartSettingI);
		return spcChartSettingList;
	}

}
