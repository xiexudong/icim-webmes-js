package com.dao;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.pojo.Spc_bis_grp_dt;
import com.pojo.Spc_bis_grp_main;
import com.tx.SpcGroupMain.SpcGroupMainI;
import com.tx.SpcGroupDt.SpcGroupDtI;

public class SPCGroupDao extends SqlMapClientDaoSupport {
	public List<Spc_bis_grp_main> findSPCGroupMain(SpcGroupMainI spcGroupMain)  {
		List<Spc_bis_grp_main> spcBisGrpMainList = 
			getSqlMapClientTemplate().queryForList("spc_group.findSPCGroup",spcGroupMain);
		return spcBisGrpMainList;
	}
//	public List<Spc_bis_grp_dt> findSPCGroupDT(SpcGroupDtI spcGroupDtI){
//		List<Spc_bis_grp_dt> spcBisGrpDTList = 
//			getSqlMapClientTemplate().queryForList("spc_group.findSPCGroupDT",spcGroupDtI);
//		return spcBisGrpDTList;
//	}

}
