package com.dao;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Component;

import com.pojo.Spc_bis_chart_main;
import com.tx.SpcChartMain.SpcChartMainI;
public class SPCChartMainDao extends SqlMapClientDaoSupport {
	public List<Spc_bis_chart_main> findSPCChartMain(SpcChartMainI spcChartMainI)  {
		return 	getSqlMapClientTemplate().queryForList("spc_chart_main.findChartMain",spcChartMainI);
	}

}
