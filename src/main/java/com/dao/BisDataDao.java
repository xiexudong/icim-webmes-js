package com.dao;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.pojo.Bis_data;
import com.tx.BisDatafnc.BisDatafncI;

public class BisDataDao extends SqlMapClientDaoSupport {

	public List<Bis_data> findFabMain(BisDatafncI bisDatafncI) {
		return getSqlMapClientTemplate().queryForList(
				"bis_data.findFabMain", bisDatafncI);
	}
}
