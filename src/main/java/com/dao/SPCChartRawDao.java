package com.dao;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.pojo.Spc_ret_chart_raw;
import com.tx.SpcChartRaw.SpcChartRawI;

public class SPCChartRawDao extends SqlMapClientDaoSupport {
	public List<Spc_ret_chart_raw> findSPCChartRaw(SpcChartRawI spcChartRawI)  {
		 
		return	getSqlMapClientTemplate().queryForList("spc_chart_raw.findSPCChartRaw",spcChartRawI);
	}

}
