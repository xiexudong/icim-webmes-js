package com.dao;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.pojo.Spc_ret_chart_data;
import com.tx.SpcChartData.SpcChartDataI;

public class SPCChartDataDao extends SqlMapClientDaoSupport {
	public List<Spc_ret_chart_data> findChartDataFnc(SpcChartDataI spcChartDataI)  {
		List<Spc_ret_chart_data> spcBisGrpMainList = 
			getSqlMapClientTemplate().queryForList("spc_chart_data.findChartData",spcChartDataI);
		return spcBisGrpMainList;
	}
//	public List<Spc_bis_grp_dt> findSPCGroupDT(SpcGroupDtI spcGroupDtI){
//		List<Spc_bis_grp_dt> spcBisGrpDTList = 
//			getSqlMapClientTemplate().queryForList("spc_group.findSPCGroupDT",spcGroupDtI);
//		return spcBisGrpDTList;
//	}

}
