package com.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.map.SessionMap;
import com.model.UserPO;

public class SessionListener implements HttpSessionListener{
	
	@Override
	public void sessionCreated(HttpSessionEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		HttpSession session = event.getSession();
		 /**
		 * session过期删除SessionMap中的信息
		 */
//		UserPO userSession = (UserPO) session.getAttribute("userInfo");
//		if(userSession != null){
//			SessionMap.sessionMap.remove(userSession.getUser_id().toUpperCase());
//		}
		session.removeAttribute("userInfo");
	}
}
