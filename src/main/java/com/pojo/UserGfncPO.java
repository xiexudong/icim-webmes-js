package com.pojo;

public class UserGfncPO {
	private String user_id;
	private String group_id;
	private String func_code;
	private String system_id;
	private String admin_flg;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getFunc_code() {
		return func_code;
	}

	public void setFunc_code(String func_code) {
		this.func_code = func_code;
	}

	public String getSystem_id() {
		return system_id;
	}

	public void setSystem_id(String system_id) {
		this.system_id = system_id;
	}

	public String getAdmin_flg() {
		return admin_flg;
	}

	public void setAdmin_flg(String admin_flg) {
		this.admin_flg = admin_flg;
	}
}
