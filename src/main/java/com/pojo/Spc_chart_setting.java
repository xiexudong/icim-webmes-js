package com.pojo;

import java.sql.Timestamp;

public class Spc_chart_setting {
	private String col_typ;
	private String grp_no;
	private String grp_name;
	private String grp_dsc;
	
	private String chart_no;
	private String chart_name;
	private String chart_dsc;
	private String chart_owner;
	private Integer max_chart_point;
	private String data_group_fk;
	
	private String chart_typ_fk;
	
	private Double chart_upl;
	private Double chart_ual;
	private Double chart_usl;
	private Double chart_ucl;
	private Double chart_uwl;
	private Double chart_target;
	private Double chart_mean;
	private Double chart_lwl;
	private Double chart_lcl;
	private Double chart_lsl;
	private Double chart_lal;
	private Double chart_lpl;
	public String getCol_typ() {
		return col_typ;
	}
	public void setCol_typ(String col_typ) {
		this.col_typ = col_typ;
	}
	public String getGrp_no() {
		return grp_no;
	}
	public void setGrp_no(String grp_no) {
		this.grp_no = grp_no;
	}
	public String getGrp_name() {
		return grp_name;
	}
	public void setGrp_name(String grp_name) {
		this.grp_name = grp_name;
	}
	public String getGrp_dsc() {
		return grp_dsc;
	}
	public void setGrp_dsc(String grp_dsc) {
		this.grp_dsc = grp_dsc;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getChart_name() {
		return chart_name;
	}
	public void setChart_name(String chart_name) {
		this.chart_name = chart_name;
	}
	public String getChart_dsc() {
		return chart_dsc;
	}
	public void setChart_dsc(String chart_dsc) {
		this.chart_dsc = chart_dsc;
	}
	public String getChart_owner() {
		return chart_owner;
	}
	public void setChart_owner(String chart_owner) {
		this.chart_owner = chart_owner;
	}
	public Integer getMax_chart_point() {
		return max_chart_point;
	}
	public void setMax_chart_point(Integer max_chart_point) {
		this.max_chart_point = max_chart_point;
	}
	public String getData_group_fk() {
		return data_group_fk;
	}
	public void setData_group_fk(String data_group_fk) {
		this.data_group_fk = data_group_fk;
	}
	public String getChart_typ_fk() {
		return chart_typ_fk;
	}
	public void setChart_typ_fk(String chart_typ_fk) {
		this.chart_typ_fk = chart_typ_fk;
	}
	public Double getChart_upl() {
		return chart_upl;
	}
	public void setChart_upl(Double chart_upl) {
		this.chart_upl = chart_upl;
	}
	public Double getChart_ual() {
		return chart_ual;
	}
	public void setChart_ual(Double chart_ual) {
		this.chart_ual = chart_ual;
	}
	public Double getChart_usl() {
		return chart_usl;
	}
	public void setChart_usl(Double chart_usl) {
		this.chart_usl = chart_usl;
	}
	public Double getChart_ucl() {
		return chart_ucl;
	}
	public void setChart_ucl(Double chart_ucl) {
		this.chart_ucl = chart_ucl;
	}
	public Double getChart_uwl() {
		return chart_uwl;
	}
	public void setChart_uwl(Double chart_uwl) {
		this.chart_uwl = chart_uwl;
	}
	public Double getChart_target() {
		return chart_target;
	}
	public void setChart_target(Double chart_target) {
		this.chart_target = chart_target;
	}
	public Double getChart_mean() {
		return chart_mean;
	}
	public void setChart_mean(Double chart_mean) {
		this.chart_mean = chart_mean;
	}
	public Double getChart_lwl() {
		return chart_lwl;
	}
	public void setChart_lwl(Double chart_lwl) {
		this.chart_lwl = chart_lwl;
	}
	public Double getChart_lcl() {
		return chart_lcl;
	}
	public void setChart_lcl(Double chart_lcl) {
		this.chart_lcl = chart_lcl;
	}
	public Double getChart_lsl() {
		return chart_lsl;
	}
	public void setChart_lsl(Double chart_lsl) {
		this.chart_lsl = chart_lsl;
	}
	public Double getChart_lal() {
		return chart_lal;
	}
	public void setChart_lal(Double chart_lal) {
		this.chart_lal = chart_lal;
	}
	public Double getChart_lpl() {
		return chart_lpl;
	}
	public void setChart_lpl(Double chart_lpl) {
		this.chart_lpl = chart_lpl;
	}
	
}
