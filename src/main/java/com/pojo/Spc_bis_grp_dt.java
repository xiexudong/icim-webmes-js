package com.pojo;

public class Spc_bis_grp_dt {
	private String col_typ_fk;
	private String grp_no_fk;
	private String sub_grp_no;
	private String tool_id_fk;
	private String valid_flg;
	private String cond_typ;
	private String mdl_typ_fk;
	private String mdl_cate_fk;
	private String mdl_id_fk;
	private String recipe_id_fk;
	private String path_id_fk;
	private String path_ver_fk;
	private String ope_no_fk;
	private String ope_id_fk;
	private String ope_ver_fk;
	private String proc_id;
	public String getCol_typ_fk() {
		return col_typ_fk;
	}
	public void setCol_typ_fk(String col_typ_fk) {
		this.col_typ_fk = col_typ_fk;
	}
	public String getGrp_no_fk() {
		return grp_no_fk;
	}
	public void setGrp_no_fk(String grp_no_fk) {
		this.grp_no_fk = grp_no_fk;
	}
	public String getSub_grp_no() {
		return sub_grp_no;
	}
	public void setSub_grp_no(String sub_grp_no) {
		this.sub_grp_no = sub_grp_no;
	}
	public String getTool_id_fk() {
		return tool_id_fk;
	}
	public void setTool_id_fk(String tool_id_fk) {
		this.tool_id_fk = tool_id_fk;
	}
	public String getValid_flg() {
		return valid_flg;
	}
	public void setValid_flg(String valid_flg) {
		this.valid_flg = valid_flg;
	}
	public String getCond_typ() {
		return cond_typ;
	}
	public void setCond_typ(String cond_typ) {
		this.cond_typ = cond_typ;
	}
	public String getMdl_typ_fk() {
		return mdl_typ_fk;
	}
	public void setMdl_typ_fk(String mdl_typ_fk) {
		this.mdl_typ_fk = mdl_typ_fk;
	}
	public String getMdl_cate_fk() {
		return mdl_cate_fk;
	}
	public void setMdl_cate_fk(String mdl_cate_fk) {
		this.mdl_cate_fk = mdl_cate_fk;
	}
	public String getMdl_id_fk() {
		return mdl_id_fk;
	}
	public void setMdl_id_fk(String mdl_id_fk) {
		this.mdl_id_fk = mdl_id_fk;
	}
	public String getRecipe_id_fk() {
		return recipe_id_fk;
	}
	public void setRecipe_id_fk(String recipe_id_fk) {
		this.recipe_id_fk = recipe_id_fk;
	}
	public String getPath_id_fk() {
		return path_id_fk;
	}
	public void setPath_id_fk(String path_id_fk) {
		this.path_id_fk = path_id_fk;
	}
	public String getPath_ver_fk() {
		return path_ver_fk;
	}
	public void setPath_ver_fk(String path_ver_fk) {
		this.path_ver_fk = path_ver_fk;
	}
	public String getOpe_no_fk() {
		return ope_no_fk;
	}
	public void setOpe_no_fk(String ope_no_fk) {
		this.ope_no_fk = ope_no_fk;
	}
	public String getOpe_id_fk() {
		return ope_id_fk;
	}
	public void setOpe_id_fk(String ope_id_fk) {
		this.ope_id_fk = ope_id_fk;
	}
	public String getOpe_ver_fk() {
		return ope_ver_fk;
	}
	public void setOpe_ver_fk(String ope_ver_fk) {
		this.ope_ver_fk = ope_ver_fk;
	}
	public String getProc_id() {
		return proc_id;
	}
	public void setProc_id(String proc_id) {
		this.proc_id = proc_id;
	}
	
}
