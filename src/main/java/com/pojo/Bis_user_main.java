package com.pojo;

public class Bis_user_main {
	private String usr_id;
	private String usr_name;
	private String usr_key;
	private String lock_flg;
	public String getUsr_id() {
		return usr_id;
	}
	public void setUsr_id(String usr_id) {
		this.usr_id = usr_id;
	}
	public String getUsr_name() {
		return usr_name;
	}
	public void setUsr_name(String usr_name) {
		this.usr_name = usr_name;
	}
	public String getUsr_key() {
		return usr_key;
	}
	public void setUsr_key(String usr_key) {
		this.usr_key = usr_key;
	}
	public String getLock_flg() {
		return lock_flg;
	}
	public void setLock_flg(String lock_flg) {
		this.lock_flg = lock_flg;
	}
	
}
