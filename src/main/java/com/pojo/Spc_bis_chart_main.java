package com.pojo;

import java.sql.Timestamp;

public class Spc_bis_chart_main {
	private String col_typ_fk;
	private String grp_no_fk;
	private String chart_no;
	private String data_group_fk;
	private String chart_name;
	private String chart_dsc;
	
//	private String chart_typ;
//	private String chart_owner;
//	private Integer sample_size;
//	private Integer max_chart_point;
	public String getCol_typ_fk() {
		return col_typ_fk;
	}
	public void setCol_typ_fk(String col_typ_fk) {
		this.col_typ_fk = col_typ_fk;
	}
	public String getGrp_no_fk() {
		return grp_no_fk;
	}
	public void setGrp_no_fk(String grp_no_fk) {
		this.grp_no_fk = grp_no_fk;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getData_group_fk() {
		return data_group_fk;
	}
	public void setData_group_fk(String data_group_fk) {
		this.data_group_fk = data_group_fk;
	}
//	public String getChart_typ() {
//		return chart_typ;
//	}
//	public void setChart_typ(String chart_typ) {
//		this.chart_typ = chart_typ;
//	}
	public String getChart_name() {
		return chart_name;
	}
	public void setChart_name(String chart_name) {
		this.chart_name = chart_name;
	}
	public String getChart_dsc() {
		return chart_dsc;
	}
	public void setChart_dsc(String chart_dsc) {
		this.chart_dsc = chart_dsc;
	}
//	public String getChart_owner() {
//		return chart_owner;
//	}
//	public void setChart_owner(String chart_owner) {
//		this.chart_owner = chart_owner;
//	}
//	public Integer getSample_size() {
//		return sample_size;
//	}
//	public void setSample_size(Integer sample_size) {
//		this.sample_size = sample_size;
//	}
//	public Integer getMax_chart_point() {
//		return max_chart_point;
//	}
//	public void setMax_chart_point(Integer max_chart_point) {
//		this.max_chart_point = max_chart_point;
//	}
	
}
