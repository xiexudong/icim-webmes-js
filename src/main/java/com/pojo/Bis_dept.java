package com.pojo;

public class Bis_dept {
	private String dept_id      ;
	private String dept_name    ;
	private String up_dept_id   ;
	private Integer dept_lvl    ;
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getUp_dept_id() {
		return up_dept_id;
	}
	public void setUp_dept_id(String up_dept_id) {
		this.up_dept_id = up_dept_id;
	}
	public Integer getDept_lvl() {
		return dept_lvl;
	}
	public void setDept_lvl(Integer dept_lvl) {
		this.dept_lvl = dept_lvl;
	}
	
	
}
