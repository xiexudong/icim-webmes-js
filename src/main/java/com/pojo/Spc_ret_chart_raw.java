package com.pojo;

import java.sql.Timestamp;

public class Spc_ret_chart_raw {
	private String data_group_seq_fk;
	private Double prd_raw_value;
	private Double proc_time_d;
	public String getData_group_seq_fk() {
		return data_group_seq_fk;
	}
	public void setData_group_seq_fk(String data_group_seq_fk) {
		this.data_group_seq_fk = data_group_seq_fk;
	}
	public Double getPrd_raw_value() {
		return prd_raw_value;
	}
	public void setPrd_raw_value(Double prd_raw_value) {
		this.prd_raw_value = prd_raw_value;
	}
	public Double getProc_time_d() {
		return proc_time_d;
	}
	public void setProc_time_d(Double proc_time_d) {
		this.proc_time_d = proc_time_d;
	}
	
}
