package com.pojo;

public class Spc_ret_chart_info {
	private String col_typ_fk;
	private String grp_no_fk;
	private String chart_no_fk;
	private String chart_typ_fk;
	private Double chart_upl;
	private Double chart_ual;
	private Double chart_usl;
	private Double chart_ucl;
	private Double chart_uwl;
	private Double chart_target;
	private Double chart_mean;
	private Double chart_lwl;
	private Double chart_lcl;
	private Double chart_lsl;
	private Double chart_lal;
	private Double chart_lpl;
	public String getCol_typ_fk() {
		return col_typ_fk;
	}
	public void setCol_typ_fk(String col_typ_fk) {
		this.col_typ_fk = col_typ_fk;
	}
	public String getGrp_no_fk() {
		return grp_no_fk;
	}
	public void setGrp_no_fk(String grp_no_fk) {
		this.grp_no_fk = grp_no_fk;
	}
	public String getChart_no_fk() {
		return chart_no_fk;
	}
	public void setChart_no_fk(String chart_no_fk) {
		this.chart_no_fk = chart_no_fk;
	}
	public String getChart_typ_fk() {
		return chart_typ_fk;
	}
	public void setChart_typ_fk(String chart_typ_fk) {
		this.chart_typ_fk = chart_typ_fk;
	}
	public Double getChart_upl() {
		return chart_upl;
	}
	public void setChart_upl(Double chart_upl) {
		this.chart_upl = chart_upl;
	}
	public Double getChart_ual() {
		return chart_ual;
	}
	public void setChart_ual(Double chart_ual) {
		this.chart_ual = chart_ual;
	}
	public Double getChart_usl() {
		return chart_usl;
	}
	public void setChart_usl(Double chart_usl) {
		this.chart_usl = chart_usl;
	}
	public Double getChart_ucl() {
		return chart_ucl;
	}
	public void setChart_ucl(Double chart_ucl) {
		this.chart_ucl = chart_ucl;
	}
	public Double getChart_uwl() {
		return chart_uwl;
	}
	public void setChart_uwl(Double chart_uwl) {
		this.chart_uwl = chart_uwl;
	}
	public Double getChart_target() {
		return chart_target;
	}
	public void setChart_target(Double chart_target) {
		this.chart_target = chart_target;
	}
	public Double getChart_mean() {
		return chart_mean;
	}
	public void setChart_mean(Double chart_mean) {
		this.chart_mean = chart_mean;
	}
	public Double getChart_lwl() {
		return chart_lwl;
	}
	public void setChart_lwl(Double chart_lwl) {
		this.chart_lwl = chart_lwl;
	}
	public Double getChart_lcl() {
		return chart_lcl;
	}
	public void setChart_lcl(Double chart_lcl) {
		this.chart_lcl = chart_lcl;
	}
	public Double getChart_lsl() {
		return chart_lsl;
	}
	public void setChart_lsl(Double chart_lsl) {
		this.chart_lsl = chart_lsl;
	}
	public Double getChart_lal() {
		return chart_lal;
	}
	public void setChart_lal(Double chart_lal) {
		this.chart_lal = chart_lal;
	}
	public Double getChart_lpl() {
		return chart_lpl;
	}
	public void setChart_lpl(Double chart_lpl) {
		this.chart_lpl = chart_lpl;
	}
	
}
