package com.pojo;

import java.sql.Timestamp;
import java.util.List;

public class Spc_ret_chart_data {
	private String col_typ_fk;
	private String grp_no_fk;
	private String chart_no_fk;
	private String chart_typ_fk;
	private Double input_time_d;
	private Timestamp input_timestamp;
//	private Timestamp proc_timestamp;
//	private String sub_grp_no_fk;
//	private String evt_usr;
	private String prd_seq_id_fk;
//	private String exd_flg;
	private Double chart_value;
//	private Integer sample_size_fk;
//	private String chart_spec_rlt_fk;
//	private String chart_rule_1_rlt_fk;
//	private String chart_rule_2_rlt_fk;
//	private String ooc_ocap_fk;
//	private String oos_ocap_fk;
//	private String chart_total_action_fk;
//	private String chart_detail_action_fk;
//	private Double chart_usl_fk;
//	private Double chart_ucl_fk;
//	private Double chart_uwl_fk;
//	private Double chart_mean_fk;
//	private Double chart_lwl_fk;
//	private Double chart_lcl_fk;
//	private Double chart_lsl_fk;
	private String mdl_id_fk;
	private String p_tool_id_fk;
	private String p_recipe_id_fk;
	private String p_path_id_fk;
	private String p_ope_no_fk;
	private String p_ope_id_fk;
	private String p_proc_id;
	private String m_tool_id_fk;
	private String chart_comment;
	private String evt_usr;
	private List<Spc_ret_chart_raw> rawData;
//	private String m_recipe_id_fk;
//	private String chart_comment_rsn;

//	private String chart_comment_usr;
	
	public String getCol_typ_fk() {
		return col_typ_fk;
	}
	public String getPrd_seq_id_fk() {
		return prd_seq_id_fk;
	}
	public void setPrd_seq_id_fk(String prd_seq_id_fk) {
		this.prd_seq_id_fk = prd_seq_id_fk;
	}
	public void setCol_typ_fk(String col_typ_fk) {
		this.col_typ_fk = col_typ_fk;
	}
	public String getGrp_no_fk() {
		return grp_no_fk;
	}
	public void setGrp_no_fk(String grp_no_fk) {
		this.grp_no_fk = grp_no_fk;
	}
	public String getChart_no_fk() {
		return chart_no_fk;
	}
	public void setChart_no_fk(String chart_no_fk) {
		this.chart_no_fk = chart_no_fk;
	}
	public String getChart_typ_fk() {
		return chart_typ_fk;
	}
	public void setChart_typ_fk(String chart_typ_fk) {
		this.chart_typ_fk = chart_typ_fk;
	}
	public Double getInput_time_d() {
		return input_time_d;
	}
	public void setInput_time_d(Double input_time_d) {
		this.input_time_d = input_time_d;
	}
	public Timestamp getInput_timestamp() {
		return input_timestamp;
	}
	public void setInput_timestamp(Timestamp input_timestamp) {
		this.input_timestamp = input_timestamp;
	}
	public Double getChart_value() {
		return chart_value;
	}
	public void setChart_value(Double chart_value) {
		this.chart_value = chart_value;
	}
	public String getMdl_id_fk() {
		return mdl_id_fk;
	}
	public void setMdl_id_fk(String mdl_id_fk) {
		this.mdl_id_fk = mdl_id_fk;
	}
	public String getP_tool_id_fk() {
		return p_tool_id_fk;
	}
	public void setP_tool_id_fk(String p_tool_id_fk) {
		this.p_tool_id_fk = p_tool_id_fk;
	}
	public String getP_recipe_id_fk() {
		return p_recipe_id_fk;
	}
	public void setP_recipe_id_fk(String p_recipe_id_fk) {
		this.p_recipe_id_fk = p_recipe_id_fk;
	}
	public String getP_path_id_fk() {
		return p_path_id_fk;
	}
	public void setP_path_id_fk(String p_path_id_fk) {
		this.p_path_id_fk = p_path_id_fk;
	}
	public String getP_ope_no_fk() {
		return p_ope_no_fk;
	}
	public void setP_ope_no_fk(String p_ope_no_fk) {
		this.p_ope_no_fk = p_ope_no_fk;
	}
	public String getP_ope_id_fk() {
		return p_ope_id_fk;
	}
	public void setP_ope_id_fk(String p_ope_id_fk) {
		this.p_ope_id_fk = p_ope_id_fk;
	}
	public String getP_proc_id() {
		return p_proc_id;
	}
	public void setP_proc_id(String p_proc_id) {
		this.p_proc_id = p_proc_id;
	}
	public String getM_tool_id_fk() {
		return m_tool_id_fk;
	}
	public void setM_tool_id_fk(String m_tool_id_fk) {
		this.m_tool_id_fk = m_tool_id_fk;
	}
	public String getChart_comment() {
		return chart_comment;
	}
	public void setChart_comment(String chart_comment) {
		this.chart_comment = chart_comment;
	}
	public String getEvt_usr() {
		return evt_usr;
	}
	public void setEvt_usr(String evt_usr) {
		this.evt_usr = evt_usr;
	}
	public List<Spc_ret_chart_raw> getRawData() {
		return rawData;
	}
	public void setRawData(List<Spc_ret_chart_raw> rawData) {
		this.rawData = rawData;
	}
	
}
