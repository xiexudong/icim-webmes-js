package com.pojo;
import java.sql.Timestamp;

public class Spc_bis_grp_main {
	private String col_typ;
	private String grp_no;
	private String grp_name;
	private String grp_dsc;
	private String evt_usr;
	private Timestamp evt_timestamp;
	public String getCol_typ() {
		return col_typ;
	}
	public void setCol_typ(String col_typ) {
		this.col_typ = col_typ;
	}
	public String getGrp_no() {
		return grp_no;
	}
	public void setGrp_no(String grp_no) {
		this.grp_no = grp_no;
	}
	public String getGrp_name() {
		return grp_name;
	}
	public void setGrp_name(String grp_name) {
		this.grp_name = grp_name;
	}
	public String getGrp_dsc() {
		return grp_dsc;
	}
	public void setGrp_dsc(String grp_dsc) {
		this.grp_dsc = grp_dsc;
	}
	public String getEvt_usr() {
		return evt_usr;
	}
	public void setEvt_usr(String evt_usr) {
		this.evt_usr = evt_usr;
	}
	public Timestamp getEvt_timestamp() {
		return evt_timestamp;
	}
	public void setEvt_timestamp(Timestamp evt_timestamp) {
		this.evt_timestamp = evt_timestamp;
	}
	
	
}
