package com.tx.SpcChartRaw;

import java.util.List;

import com.pojo.Spc_ret_chart_raw;

public class SpcChartRawO {
	private long rtn_code;
	private List<Spc_ret_chart_raw> oary;
	public long getRtn_code() {
		return rtn_code;
	}
	public void setRtn_code(long rtn_code) {
		this.rtn_code = rtn_code;
	}
	public List<Spc_ret_chart_raw> getOary() {
		return oary;
	}
	public void setOary(List<Spc_ret_chart_raw> oary) {
		this.oary = oary;
	}
	
}
