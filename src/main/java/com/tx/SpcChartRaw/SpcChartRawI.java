package com.tx.SpcChartRaw;

import java.sql.Timestamp;

public class SpcChartRawI {
	private String col_typ_fk;
	private String grp_no_fk;
	private String chart_no_fk;
	private String chart_typ_fk;
	private Double proc_time_d;
	public String getCol_typ_fk() {
		return col_typ_fk;
	}
	public void setCol_typ_fk(String col_typ_fk) {
		this.col_typ_fk = col_typ_fk;
	}
	public String getGrp_no_fk() {
		return grp_no_fk;
	}
	public void setGrp_no_fk(String grp_no_fk) {
		this.grp_no_fk = grp_no_fk;
	}
	public String getChart_no_fk() {
		return chart_no_fk;
	}
	public void setChart_no_fk(String chart_no_fk) {
		this.chart_no_fk = chart_no_fk;
	}
	public String getChart_typ_fk() {
		return chart_typ_fk;
	}
	public void setChart_typ_fk(String chart_typ_fk) {
		this.chart_typ_fk = chart_typ_fk;
	}
	public Double getProc_time_d() {
		return proc_time_d;
	}
	public void setProc_time_d(Double proc_time_d) {
		this.proc_time_d = proc_time_d;
	}
	
}
