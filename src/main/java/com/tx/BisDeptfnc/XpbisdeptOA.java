package com.tx.BisDeptfnc;

import java.sql.Timestamp;

public class XpbisdeptOA {
	private String dept_id      ;
	private String dept_name    ;
	private String dept_s_name  ;
	private String dept_e_name  ;
	private String up_dept_id   ;
	private Integer dept_lvl     ;
	private String valid_flg    ;
	private String evt_usr      ;
	private Timestamp evt_timestamp;
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getDept_s_name() {
		return dept_s_name;
	}
	public void setDept_s_name(String dept_s_name) {
		this.dept_s_name = dept_s_name;
	}
	public String getDept_e_name() {
		return dept_e_name;
	}
	public void setDept_e_name(String dept_e_name) {
		this.dept_e_name = dept_e_name;
	}
	public String getUp_dept_id() {
		return up_dept_id;
	}
	public void setUp_dept_id(String up_dept_id) {
		this.up_dept_id = up_dept_id;
	}
	public Integer getDept_lvl() {
		return dept_lvl;
	}
	public void setDept_lvl(Integer dept_lvl) {
		this.dept_lvl = dept_lvl;
	}
	public String getValid_flg() {
		return valid_flg;
	}
	public void setValid_flg(String valid_flg) {
		this.valid_flg = valid_flg;
	}
	public String getEvt_usr() {
		return evt_usr;
	}
	public void setEvt_usr(String evt_usr) {
		this.evt_usr = evt_usr;
	}
	public Timestamp getEvt_timestamp() {
		return evt_timestamp;
	}
	public void setEvt_timestamp(Timestamp evt_timestamp) {
		this.evt_timestamp = evt_timestamp;
	}
	
}
