package com.tx.SpcChartMain;

import java.util.List;

import com.pojo.Spc_bis_chart_main;

public class SpcChartMainO {
	private long rtn_code;
	private List<Spc_bis_chart_main> oary;
	public long getRtn_code() {
		return rtn_code;
	}
	public void setRtn_code(long rtn_code) {
		this.rtn_code = rtn_code;
	}
	public List<Spc_bis_chart_main> getOary() {
		return oary;
	}
	public void setOary(List<Spc_bis_chart_main> oary) {
		this.oary = oary;
	}
	
}
