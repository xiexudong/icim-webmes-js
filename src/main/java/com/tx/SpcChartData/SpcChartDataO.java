package com.tx.SpcChartData;

import java.util.List;

import com.pojo.Spc_ret_chart_data;

public class SpcChartDataO {
	private long rtn_code;
	private List<Spc_ret_chart_data> oary;
	public long getRtn_code() {
		return rtn_code;
	}
	public void setRtn_code(long rtn_code) {
		this.rtn_code = rtn_code;
	}
	public List<Spc_ret_chart_data> getOary() {
		return oary;
	}
	public void setOary(List<Spc_ret_chart_data> oary) {
		this.oary = oary;
	}
	
}
