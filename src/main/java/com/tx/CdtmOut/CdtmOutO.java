package com.tx.CdtmOut;
/**   
* @Title: CdtmOutO.java
* @Package com.tx.CdtmOut
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2014-6-5 下午12:37:40    
* @version V1.0   
*/
public class CdtmOutO {
	private long rtn_code;
	private String rtn_mesg;
	private String xml;
	public long getRtn_code() {
		return rtn_code;
	}
	public void setRtn_code(long rtn_code) {
		this.rtn_code = rtn_code;
	}
	public String getRtn_mesg() {
		return rtn_mesg;
	}
	public void setRtn_mesg(String rtn_mesg) {
		this.rtn_mesg = rtn_mesg;
	}
	public String getXml() {
		return xml;
	}
	public void setXml(String xml) {
		this.xml = xml;
	}
	
}
