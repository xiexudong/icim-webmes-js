package com.tx.BisDatafnc;

public class BisDatafncI {
	private String data_ext;
	private String ext_1;
	private String data_cate;
	
	public String getData_ext() {
		return data_ext;
	}
	public void setData_ext(String data_ext) {
		this.data_ext = data_ext;
	}
	public String getExt_1() {
		return ext_1;
	}
	public void setExt_1(String ext_1) {
		this.ext_1 = ext_1;
	}
	public String getData_cate() {
		return data_cate;
	}
	public void setData_cate(String data_cate) {
		this.data_cate = data_cate;
	}
}
