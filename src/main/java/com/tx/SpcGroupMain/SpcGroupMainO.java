package com.tx.SpcGroupMain;

import java.util.List;

import com.pojo.Spc_bis_grp_main;

public class SpcGroupMainO {
	private long rtn_code;
	private List<Spc_bis_grp_main> oary;
	public long getRtn_code() {
		return rtn_code;
	}
	public void setRtn_code(long rtn_code) {
		this.rtn_code = rtn_code;
	}
	public List<Spc_bis_grp_main> getOary() {
		return oary;
	}
	public void setOary(List<Spc_bis_grp_main> oary) {
		this.oary = oary;
	}
	
}
