package com.tx.SpcGroupDt;

import java.util.List;

import com.pojo.Spc_bis_grp_dt;

public class SpcGroupDtO {
	private long rtn_code;
	private List<Spc_bis_grp_dt> oary;
	public long getRtn_code() {
		return rtn_code;
	}
	public void setRtn_code(long rtn_code) {
		this.rtn_code = rtn_code;
	}
	public List<Spc_bis_grp_dt> getOary() {
		return oary;
	}
	public void setOary(List<Spc_bis_grp_dt> oary) {
		this.oary = oary;
	}
	
}
