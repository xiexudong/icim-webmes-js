package com.tx.SpcGroupDt;

public class SpcGroupDtI {
	private String col_typ;
	private String grp_no;
	private String sub_grp_no;
	public String getCol_typ() {
		return col_typ;
	}
	public void setCol_typ(String col_typ) {
		this.col_typ = col_typ;
	}
	public String getGrp_no() {
		return grp_no;
	}
	public void setGrp_no(String grp_no) {
		this.grp_no = grp_no;
	}
	public String getSub_grp_no() {
		return sub_grp_no;
	}
	public void setSub_grp_no(String sub_grp_no) {
		this.sub_grp_no = sub_grp_no;
	}
	
}
