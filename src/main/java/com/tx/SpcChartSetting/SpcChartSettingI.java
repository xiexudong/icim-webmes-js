package com.tx.SpcChartSetting;

public class SpcChartSettingI {
	private String col_typ;
	private String grp_no;
	private String chart_no;
	private String chart_typ;
	public String getCol_typ() {
		return col_typ;
	}
	public void setCol_typ(String col_typ) {
		this.col_typ = col_typ;
	}
	public String getGrp_no() {
		return grp_no;
	}
	public void setGrp_no(String grp_no) {
		this.grp_no = grp_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getChart_typ() {
		return chart_typ;
	}
	public void setChart_typ(String chart_typ) {
		this.chart_typ = chart_typ;
	}
	
}
