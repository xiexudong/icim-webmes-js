package com.tx.SpcChartSetting;

import java.util.List;

import com.pojo.Spc_chart_setting;

public class SpcChartSettingO {
	private long rtn_code;
	private List<Spc_chart_setting> oary;
	public long getRtn_code() {
		return rtn_code;
	}
	public void setRtn_code(long rtn_code) {
		this.rtn_code = rtn_code;
	}
	public List<Spc_chart_setting> getOary() {
		return oary;
	}
	public void setOary(List<Spc_chart_setting> oary) {
		this.oary = oary;
	}
	
}
