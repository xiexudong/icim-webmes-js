package com.interceptor;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import com.action.AuthenticationAction;
import com.action.GotoAction;
import com.action.LoginAction;
import com.model.UserPO;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.service.IUASService;
import com.service.IUserGfncService;
import com.tx.UasUserGfnc.UasUserGfncI;
import com.util.AuthenticationUtils;
import com.util.AuthenticationUtils.License;
import com.util.Printer;

/**
 * Default action intercept.
 * 
 * @author Unknown
 * @author Anthony.M
 */
public class AlldefaultInterceptor extends AbstractInterceptor {
	private Printer printer = new Printer(AlldefaultInterceptor.class);

	private static final long serialVersionUID = 1L;
	private IUASService uasService;
	private static Properties props;
	private IUserGfncService userGfncService;
	private UasUserGfncI uasUserGfncI = new UasUserGfncI();
	static {
		props = new Properties();
		try {
			props.load(AlldefaultInterceptor.class.getResourceAsStream("/function_code.properties"));
		} catch (IOException e) {
			System.out.println("Read function code configuration error");
			e.printStackTrace();
		}
	}

	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		ActionContext ctx = ActionContext.getContext();
		Map session = ctx.getSession();
		Action action = (Action) actionInvocation.getAction();
		String message = null;

		if (action instanceof AuthenticationAction) {
			return actionInvocation.invoke();
		}

		if (session.get("license") == null) {
			License license = null;

			try {
				byte[] key = AuthenticationUtils.loadLicenseKey();
				license = AuthenticationUtils.parseLicenseKey(key);
				session.put("license", license);
			} catch (Exception e) {
				printer.info(e.getMessage());
				return "setuplicense";
			}
		}

		if (action instanceof LoginAction) {
			return actionInvocation.invoke();
		}
		UserPO user = (UserPO) session.get("userInfo");
		if (user == null) {
			message = "<p>Not login or login time out, please login again</p>" + "<p>未登录或登录超时, 请重新登录</p>";
			ctx.put("message", message);
			ctx.put("title", "访问拒绝");
			return "denied";
		} else {
			if (action instanceof GotoAction) {
				Object obj = ctx.getParameters().get("opeID");
				if (obj == null) {
					message = "<p>Please specify operation ID</p>" + "<p>请指定操作代码</p>";
					ctx.put("message", message);
					ctx.put("title", "访问拒绝");
					return "denied";
				}
				String operation = ((String[]) obj)[0];
				String user_id = ((UserPO) session.get("userInfo")).getUser_id();
				// String function = props.getProperty(operation);
				String function = "F_" + operation;
				uasUserGfncI.setUsr_id(user_id);
				uasUserGfncI.setFunc_code(function);
				printer.info("Operation ID: " + operation);

				// if(!uasService.gotoCheck(operation,user)){
				// return Action.LOGIN;
				// }
				if (!userGfncService.checkAdmin(uasUserGfncI)) {
					if (!userGfncService.checkAuthority(uasUserGfncI)) {
						message = "<p>You hava no permission to view this page, please check</p>"
						        + "<p>您没有权限浏览本页, 请确认</p>";
						ctx.put("message", message);
						ctx.put("title", "访问拒绝");
						return "denied";
					}
				}
			}
		}
		return actionInvocation.invoke();
	}

	public void setUasService(IUASService uasService) {
		this.uasService = uasService;
	}

	public void setUserGfncService(IUserGfncService userGfncService) {
		this.userGfncService = userGfncService;
	}

}
