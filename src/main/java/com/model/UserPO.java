package com.model;

import java.io.Serializable;

public class UserPO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String user_id;
	private String user_password;
	private int isLogin; 
	
	public String getUser_id() {
		return user_id;
	}
	public int getIsLogin() {
		return isLogin;
	}
	public void setIsLogin(int isLogin) {
		this.isLogin = isLogin;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	
}
