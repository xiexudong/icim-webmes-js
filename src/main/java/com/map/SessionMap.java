package com.map;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * 用来保存已经登陆的用户和该用的session
 */
public class SessionMap {
	/**
	 * 用来保存已经登陆的用户和该用的session
	 */
	public static final Map<String,HttpSession> sessionMap = new HashMap<String,HttpSession>();
}
