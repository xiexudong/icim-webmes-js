package com.out.cdtm.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**   
* @Title: CdtmUtil.java
* @Package com.out.cdtm.util
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2014-6-4 下午12:52:41    
* @version V1.0   
*/
public class CdtmUtil {
	public static String getThicknessLevel(double dTftThickness,double dCfThickness){
		return "K";
	}
	public static String getThicknessLevel(double dTftThickness){
		dTftThickness = dTftThickness *0.5;
		if(dTftThickness==0.4){
			return "A";
		}else if(dTftThickness==0.5){
			return "B";
		}else if(dTftThickness==0.6){
			return "C";
		}else if(dTftThickness==0.63){
			return "D";
		}else if(dTftThickness==0.7){
			return "E";
		}else if(dTftThickness==1.1){
			return "F";
		}else if(dTftThickness==0.2){
			return "G";
		}else if(dTftThickness==0.3){
			return "H";
		}else if(dTftThickness==0.25){
			return "J";
		}else if(dTftThickness==0.225){
			return "K";
		}else if(dTftThickness==0.215){
			return "M";
		}else if(dTftThickness==0.265){
			return "N";
		}else if(dTftThickness==0.1){
			return "P";
		}else if(dTftThickness==0.15){
			return "Q";
		}else if(dTftThickness==0.18){
			return "T";
		}
			
		return null;	
	}
	public static String GetNowDate (){
	    String temp_str="";   
	    Date dt = new Date();   
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");   
	    temp_str=sdf.format(dt);   
	    return temp_str;   
	}
}
