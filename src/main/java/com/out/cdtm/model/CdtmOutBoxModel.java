package com.out.cdtm.model;

import java.util.List;

/**   
* @Title: CdtmOutModel.java
* @Package com.ftp.cdtm.model
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2014-6-4 上午10:36:43    
* @version V1.0   
*/
public class CdtmOutBoxModel {
	private Boolean rtnCode;
	private String rtnMesg;
	private String boxId;
	private String ppboxId;
	private String product;
	private String lotId;
	private Integer qty;
	private Integer stdQty;
	private String thicknessLvl;
	private String shipDate;
	private String cusId;
	private String slotInfo;
	private String mtrl_pallet_id;
	private List<CdtmOutPrdModel> prdList ;
	public String getBoxId() {
		return boxId;
	}
	public void setBoxId(String boxId) {
		this.boxId = boxId;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public String getThicknessLvl() {
		return thicknessLvl;
	}
	public void setThicknessLvl(String thicknessLvl) {
		this.thicknessLvl = thicknessLvl;
	}
	public String getShipDate() {
		return shipDate;
	}
	public void setShipDate(String shipDate) {
		this.shipDate = shipDate;
	}
	public String getSlotInfo() {
		return slotInfo;
	}
	public void setSlotInfo(String slotInfo) {
		this.slotInfo = slotInfo;
	}
	public List<CdtmOutPrdModel> getPrdList() {
		return prdList;
	}
	public void setPrdList(List<CdtmOutPrdModel> prdList) {
		this.prdList = prdList;
	}
	
	public Integer getStdQty() {
		return stdQty;
	}
	public void setStdQty(Integer stdQty) {
		this.stdQty = stdQty;
	}
	
	public String getLotId() {
		return lotId;
	}
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}
	
	public Boolean getRtnCode() {
		return rtnCode;
	}
	public void setRtnCode(Boolean rtnCode) {
		this.rtnCode = rtnCode;
	}
	public String getRtnMesg() {
		return rtnMesg;
	}
	public void setRtnMesg(String rtnMesg) {
		this.rtnMesg = rtnMesg;
	}
	public String getPpboxId() {
		return ppboxId;
	}
	public void setPpboxId(String ppboxId) {
		this.ppboxId = ppboxId;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
	public String getMtrl_pallet_id() {
		return mtrl_pallet_id;
	}
	public void setMtrl_pallet_id(String mtrl_pallet_id) {
		this.mtrl_pallet_id = mtrl_pallet_id;
	}
	@Override
	public String toString() {
		return "CdtmOutBoxModel [rtnCode=" + rtnCode + ", rtnMesg=" + rtnMesg + ", boxId=" + boxId + ", ppboxId="
				+ ppboxId + ", product=" + product + ", lotId=" + lotId + ", qty=" + qty + ", stdQty=" + stdQty
				+ ", thicknessLvl=" + thicknessLvl + ", shipDate=" + shipDate + ", cusId=" + cusId + ", slotInfo="
				+ slotInfo + ", mtrl_pallet_id=" + mtrl_pallet_id + ", prdList=" + prdList + "]";
	}
	
}
