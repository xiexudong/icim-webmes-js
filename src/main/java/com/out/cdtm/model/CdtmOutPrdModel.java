package com.out.cdtm.model;
/**   
* @Title: CdtmOutPrdModel.java
* @Package com.ftp.cdtm.model
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2014-6-4 上午10:39:39    
* @version V1.0   
*/
public class CdtmOutPrdModel {
	private String slotNo;
	private String lotId;
	private String glsId;
	private String reasonCode;
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getSlotNo() {
		return slotNo;
	}
	public void setSlotNo(String slotNo) {
		this.slotNo = slotNo;
	}
	public String getLotId() {
		return lotId;
	}
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}
	public String getGlsId() {
		return glsId;
	}
	public void setGlsId(String glsId) {
		this.glsId = glsId;
	}
	@Override
	public String toString() {
		return "CdtmOutPrdModel [slotNo=" + slotNo + ", lotId=" + lotId
				+ ", glsId=" + glsId + ", reasonCode=" + reasonCode + "]";
	}
	
}
