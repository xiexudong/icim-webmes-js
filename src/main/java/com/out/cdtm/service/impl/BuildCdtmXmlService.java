package com.out.cdtm.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.stereotype.Service;

import com.out.cdtm.model.CdtmOutBoxModel;
import com.out.cdtm.model.CdtmOutPrdModel;
import com.out.core.conf.DbConfig;
import com.out.core.ftp.FtpService;

/**   
* @Title: CdtmFtpService.java
* @Package com.ftp.cdtm.service.impl
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2014-6-4 上午8:50:43    
* @version V1.0   
*/
@Service
public class BuildCdtmXmlService {
	private static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	private static String HEAD2 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
	private static Logger logger = Logger.getLogger(BuildCdtmXmlService.class);


	private FTPClient ftpClient;

	public String getXmlString(Document document){
		String xml = document.asXML();
		xml = xml.replace(HEAD, HEAD2);
		logger.info(xml);
		return xml;
	}
	
	public boolean doc2XmlFile(Document document, String filename) {
		boolean isSuccess = true;
		try {
			/* 将document中的内容写入文件中 */
			//默认为UTF-8格式，指定为"GB2312" 
			OutputFormat format = OutputFormat.createPrettyPrint();
			//             format.setEncoding("GB2312"); 
			XMLWriter writer = new XMLWriter(new FileWriter(new File(filename)), format);
			writer.write(document);
			writer.close();
		} catch (Exception ex) {
			isSuccess = false;
			ex.printStackTrace();
		}
		return isSuccess;
	}

	public Document buildXml(CdtmOutBoxModel boxModel,String customer) {

		int qty;

		Document document = DocumentHelper.createDocument();
		Element bodyElement = document.addElement("BODY");
		/**
		 * TPONCELLONLY
		 */
		Element tpOnCellOnlyElement = bodyElement.addElement("TPONCELLONLY");
		/**
		 * DIRECTION
		 */
		Element directionElement = tpOnCellOnlyElement.addElement("DIRECTION");
		String direction=null;
		if("WHTM".equals(customer)){
			direction="HCTWH";
			
		}else if("ZH".equals(customer)){
			direction="HCTZH";
		}else{
			direction="HCTCD";
		}
		directionElement.setText(direction);
		/**
		 * MAKERID
		 */
		Element makerElement = tpOnCellOnlyElement.addElement("MAKERID");
		makerElement.setText("HC");

		/**
		 * FABID
		 */
		Element fabIdElement = tpOnCellOnlyElement.addElement("FABID");
		fabIdElement.setText("E1");
		/**
		 * SHIPDATE
		 */
		Element shipDateElement = tpOnCellOnlyElement.addElement("SHIPDATE");
		shipDateElement.setText(boxModel.getShipDate());

		/**
		 * PPBOXID
		 */
		if(!StringUtils.isEmpty(boxModel.getBoxId())){
			Element ppboxIdElement = tpOnCellOnlyElement.addElement("PPBOXID");
			ppboxIdElement.setText(boxModel.getBoxId());
		}
		
		/**
		 * THICKNESS
		 */
		Element thicnessElement = tpOnCellOnlyElement.addElement("THICKNESS");
		thicnessElement.setText(boxModel.getThicknessLvl());

		/**
		 * BOXNAME
		 */
		if(!StringUtils.isEmpty(boxModel.getMtrl_pallet_id())){
			Element boxNameElement = tpOnCellOnlyElement.addElement("BOXNAME");
			boxNameElement.setText(boxModel.getMtrl_pallet_id());
		}
		
		/**
		 * *************************************
		 * LOTINFO
		 * **************************************
		 */
		Element lotInfoElement = bodyElement.addElement("LOTINFO");
		/**
		 * LOTID
		 */
		Element lotIdElement = lotInfoElement.addElement("LOTID");
		lotIdElement.setText(boxModel.getLotId());
		/**
		 * PRODUCT
		 */
		Element productElement = lotInfoElement.addElement("PRODUCT");
		productElement.setText(boxModel.getProduct());
		/**
		 * QTY
		 */
		qty = boxModel.getQty();
		Element qtyElement = lotInfoElement.addElement("QTY");
		qtyElement.setText(String.valueOf(boxModel.getQty()));
		/**
		 * SLOT
		 */
		Element slotElement = lotInfoElement.addElement("SLOT");
		slotElement.setText(boxModel.getSlotInfo());

		for (int i = 0; i < qty; i++) {
			Element glsInfoElement = bodyElement.addElement("GLSINFO");
			CdtmOutPrdModel prdModel = boxModel.getPrdList().get(i);
			Element slotNoInfoElement = glsInfoElement.addElement("SLOTNO");
			slotNoInfoElement.setText(prdModel.getSlotNo());

			Element glsLotElement = glsInfoElement.addElement("LOTID");
			glsLotElement.setText(prdModel.getLotId());

			Element glsIdElement = glsInfoElement.addElement("GLSID");
			String glsId = prdModel.getGlsId();
//			glsId = "WHTM".equals(customer)  ?  glsId.substring(0,glsId.length()-1) : glsId;   C1407020
			glsIdElement.setText(glsId);

			Element glsJudgeElement = glsInfoElement.addElement("REASONCODE");
			glsJudgeElement.setText(prdModel.getReasonCode());
		}

		return document;

	}

	/**
	 * TODO:根据表格返回厚度代码
	 */
	private String getThickness(double dThickness) {
		return "H";
	}

	private String getSlotInfo() {
		return "OOOOOOOXXXXXXXXXXXXXXOOOO";
	}

	private static String GetNowDate() {
		String temp_str = "";
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		temp_str = sdf.format(dt);
		return temp_str;
	}
}
