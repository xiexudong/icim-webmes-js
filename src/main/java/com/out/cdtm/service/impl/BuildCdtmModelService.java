package com.out.cdtm.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.out.cdtm.model.CdtmOutBoxModel;
import com.out.cdtm.model.CdtmOutPrdModel;
import com.out.cdtm.util.CdtmUtil;
import com.out.core.db.QueryDbService;

/**   
* @Title: QueryShtService.java
* @Package com.ftp.cdtm.service.impl
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2014-6-4 上午10:20:41    
* @version V1.0   
*/
@Service
public class BuildCdtmModelService {

	private Logger logger = Logger.getLogger(BuildCdtmModelService.class);
	private static final int MAX_SLOT_CNT = 25;
	Connection connection = null;

	ResultSet boxRs = null;
	ResultSet shtRs = null;
	ResultSet mdlRs = null;
	ResultSet bomRs = null;
	@Resource
	QueryDbService queryDbService;

	public static void main(String[] args) {
//		(new BuildCdtmModelService()).getShtList("BBRG5400004", "E4E43D01400000", "E4E42D00900000");
		String glsId = "1234";
		glsId = glsId.substring(0, glsId.length()-1);
		System.out.println(glsId);
	}

	public CdtmOutBoxModel getShtList(String boxId) throws Exception {

		StringBuffer sqlBuffer = new StringBuffer();
		String mdlId = "";
		CdtmOutBoxModel boxModel = new CdtmOutBoxModel();
		String glsId;
		try {
			queryDbService.connectDb();

			sqlBuffer.setLength(0);
			sqlBuffer.append("SELECT MDL_ID_FK,PRD_QTY,STD_QTY,LOT_ID_FK,SHIP_BOX_ID FROM RET_BOX");
			sqlBuffer.append(" WHERE BOX_ID = '").append(boxId).append("'");

			boxRs = queryDbService.getQueryResult(sqlBuffer);
			while (boxRs.next()) {
				boxModel.setBoxId(boxRs.getString("SHIP_BOX_ID"));
				boxModel.setPpboxId(comCheckString(boxRs.getString("SHIP_BOX_ID")));
//				boxModel.setProduct(comCheckString(boxRs.getString("MTRL_PROD_ID_FK")));
				boxModel.setQty(boxRs.getInt("PRD_QTY"));
				boxModel.setStdQty(boxRs.getInt("STD_QTY"));
				boxModel.setLotId(boxRs.getString("LOT_ID_FK"));
				mdlId =comCheckString( boxRs.getString("MDL_ID_FK"));
				boxModel.setProduct(mdlId);
			}
			logger.info("mdlId" + mdlId);

			if (!("").equals(mdlId)) {

				sqlBuffer.setLength(0);
				sqlBuffer.append("SELECT TO_THICKNESS,CUS_ID FROM BIS_MDL_DEF");
				sqlBuffer.append(" WHERE MDL_ID = '").append(mdlId).append("'");
				mdlRs = queryDbService.getQueryResult(sqlBuffer);
				while (mdlRs.next()) {
					String sThickness = mdlRs.getString("TO_THICKNESS");
					
					logger.info("sThickness: " + sThickness);
					if (sThickness.indexOf("CF") == -1) {
						Double dTftThickness = Double.valueOf(sThickness);
						String thicknessLvl = CdtmUtil.getThicknessLevel(dTftThickness);
						logger.info("thicknessLvl: " + thicknessLvl);
						boxModel.setThicknessLvl(comCheckString(thicknessLvl));
						//boxModel.setCusId(mdlRs.getString("CUS_ID"));
					}
				}
				
//				sqlBuffer.setLength(0);
//				sqlBuffer.append(" SELECT MTRL_PROD_ID FROM BIS_BOM");
//				sqlBuffer.append(" LEFT JOIN BIS_MTRL ");
//				sqlBuffer.append(" ON MTRL_PROD_ID_FK = MTRL_PROD_ID");
//				sqlBuffer.append(" WHERE MTRL_CATE = 'SHMT'  AND MDL_ID_FK = '").append(mdlId).append("'");
//				sqlBuffer.append(" LIMIT 1");
//				bomRs = queryDbService.getQueryResult(sqlBuffer);
//				while (bomRs.next()) {
//					boxModel.setProduct(comCheckString(bomRs.getString("MTRL_PROD_ID")));
//				}
			}
			
			
			StringBuffer slotnoInfoBuffer = new StringBuffer();
			for (int i = 0; i < (boxModel.getStdQty()>25?25:boxModel.getStdQty()); i++) {
				slotnoInfoBuffer.append("X");
			}

			sqlBuffer.setLength(0);
			sqlBuffer.append("SELECT PRD_SEQ_ID,SLOT_NO,LOT_ID,MTRL_PROD_ID_FK,MTRL_BOX_ID_FK,PRD_GRADE");
			sqlBuffer.append(" FROM RET_PRD_INFO ");
			sqlBuffer.append(" WHERE BOX_ID_FK='").append(boxId).append("'");
			sqlBuffer.append(" ORDER BY CONVERT(SLOT_NO,UNSIGNED)");
			shtRs = queryDbService.getQueryResult(sqlBuffer);
			List<CdtmOutPrdModel> oaryList = new ArrayList<CdtmOutPrdModel>();
			String firstShtId = null;
			while (shtRs.next()) {
				if (shtRs != null) {
					
					CdtmOutPrdModel prdOary = new CdtmOutPrdModel();
					glsId = shtRs.getString("PRD_SEQ_ID");
					if(firstShtId==null){
						firstShtId = glsId;
					}
					String prdGrade = shtRs.getString("PRD_GRADE");
					
					String ppboxId = shtRs.getString("MTRL_BOX_ID_FK");
					String lotId = shtRs.getString("LOT_ID");
					if(ppboxId==null){
						boxModel.setRtnCode(false);
						throw new Exception(glsId+"来料型号为空,错误");
//						boxModel.setRtnMesg();
//						return boxModel;
					}
					
					
					
					prdOary.setGlsId(glsId);
					
					//					prdOary.setLotId(shtRs.getString("LOT_ID"));
					prdOary.setLotId(lotId);
					prdOary.setSlotNo(shtRs.getString("SLOT_NO"));
					prdOary.setReasonCode(prdGrade.equals("OK") ? "P" : prdGrade);
					int iSlotNo = Integer.valueOf(shtRs.getString("SLOT_NO"));
					if(iSlotNo>MAX_SLOT_CNT){
						boxModel.setRtnCode(false);
						throw new Exception("最大玻璃数量超过25,错误");
//						boxModel.setRtnMesg("");
//						return boxModel;
					}
					oaryList.add(prdOary);
					slotnoInfoBuffer.replace(iSlotNo - 1, iSlotNo, "O");
					
				}
			}
			String mtrlPalletId = null;
			String cusID = null;
			if(firstShtId!=null){
				sqlBuffer.setLength(0);
				sqlBuffer.append("SELECT MTRL_PALLET_ID,CUS_ID  FROM RET_PRD_IN");
				sqlBuffer.append(" WHERE MTRL_BOX_ID IN (");
				sqlBuffer.append(" SELECT MTRL_BOX_ID_FK FROM RET_PRD_INDT");
				sqlBuffer.append(" WHERE VDR_PRD_SEQ_ID = '");
				sqlBuffer.append(firstShtId).append("')");
				shtRs = queryDbService.getQueryResult(sqlBuffer);
				while (shtRs.next()) {
					if (shtRs != null) {
						mtrlPalletId = shtRs.getString("MTRL_PALLET_ID");
						cusID = shtRs.getString("CUS_ID");
					}
				}
			}
		


			boxModel.setSlotInfo(slotnoInfoBuffer.toString());
			boxModel.setPrdList(oaryList);
			boxModel.setShipDate(CdtmUtil.GetNowDate());
			boxModel.setMtrl_pallet_id(mtrlPalletId);
			boxModel.setCusId(cusID);
			boxModel.setRtnCode(true);
		} catch (Exception ex) {
			ex.printStackTrace();
			boxModel.setRtnCode(false);
			boxModel.setRtnMesg(ex.toString());
			throw ex;
		} finally {
			queryDbService.disConnectDb();
		}

		logger.info(boxModel);
		return boxModel;
	}
	private String comCheckString(String str){
		return str == null ? "": str;
	}
	
}
