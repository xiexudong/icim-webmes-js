package com.out.cdtm.service.impl;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.out.cdtm.model.CdtmOutBoxModel;
import com.out.cdtm.model.CsotFilePo;
import com.out.core.conf.FtpConfig;
import com.out.core.ftp.FtpService;
import com.tx.CdtmOut.CdtmOutO;

@Service
@Scope("prototype")
public class CtmtMainService {
	private Logger logger = Logger.getLogger(CtmtMainService.class);

	private static ApplicationContext applicationContext;
	@Resource
	private BuildCdtmModelService buildCdtmModelService;

	@Resource
	private BuildCdtmXmlService buildCdtmXmlService;

	@Resource
	private FtpService cdtmFtpService;

	@Resource
	private FtpConfig cdtmFtpConfig;

	// private String xml;

	public static final long RETURN_CODE_OK = 0;
	public static final long RETURN_CODE_ERROR = 1;
	public static final int GET_XML_ACTION = 1;
	public static final int UPLOAD_XML_ACTION = 2;

	public CsotFilePo subMainProc(String boxId, int action) throws Exception {
		CsotFilePo csotFilePo = new CsotFilePo();
		CdtmOutBoxModel boxModel = buildCdtmModelService.getShtList(boxId);
		
		String cusId = boxModel.getCusId();
		String productId=boxModel.getProduct();
		String filePrefixCusName = null;
		if("018".equals(cusId)){
			String exportFileName = "WHTM_BH_"
					+ boxModel.getPpboxId() + "_PanelSize("+productId.substring(productId.length()-7,productId.length()-4)+")_"
					+ boxModel.getShipDate() + ".xml";
			Document document = buildCdtmXmlService.buildXml(boxModel,"WHTM");
			
			csotFilePo.setFileContent(parse2Xml(document));
			csotFilePo.setFileName(exportFileName);
		}else if("098".equals(cusId)){
			String exportFileName = "ZH_BH_"
					+ boxModel.getPpboxId() + "_PanelSize("+productId.substring(productId.length()-7,productId.length()-4)+")_"
					+ boxModel.getShipDate() + ".xml";
			Document document = buildCdtmXmlService.buildXml(boxModel,"ZH");
			
			csotFilePo.setFileContent(parse2Xml(document));
			csotFilePo.setFileName(exportFileName);
		}else{
			String exportFileName = "CDTM_BH_"
					+ boxModel.getPpboxId() + "_PanelSize("+productId.substring(productId.length()-7,productId.length()-4)+")_"
					+ boxModel.getShipDate() + ".xml";
			Document document = buildCdtmXmlService.buildXml(boxModel,"CDTM");
			
			csotFilePo.setFileContent(parse2Xml(document));
			csotFilePo.setFileName(exportFileName);
		}
		
		
//		String localfileName = cdtmFtpConfig.getLocalTempPath() + "/CDTM_BH_"
//				+ boxModel.getPpboxId() + "_PanelSize(499)_"
//				+ boxModel.getShipDate() + ".xml";
		
		
		return csotFilePo;
	}

	public String parse2Xml(Document document) {
		return buildCdtmXmlService.getXmlString(document);
	}

	public void uploadXml(String fileName) throws IOException {
		cdtmFtpService.connectFtp();
		cdtmFtpService.uploadFile(new File(fileName));
		cdtmFtpService.disconnectFtp();
	}
	/*
	 * @BeforeClass public static void beforeClass() { applicationContext = new
	 * ClassPathXmlApplicationContext("applicationContext-common.xml"); }
	 */
}
