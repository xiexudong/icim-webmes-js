package com.out.core.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.stereotype.Service;

import com.out.core.conf.DbConfig;

/**   
* @Title: QueryShtService.java
* @Package com.ftp.cdtm.service.impl
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2014-6-4 上午10:20:41    
* @version V1.0   
*/
@Service("queryDbService")
@Scope("prototype")
public class QueryDbService {

	private Logger logger = Logger.getLogger(QueryDbService.class);
	Connection connection = null;
	@Resource
	DbConfig dbConfig;


	public ResultSet getQueryResult(StringBuffer queryBuffer) throws SQLException {
		logger.info(queryBuffer);
		Statement stmt = connection.createStatement();
		return stmt.executeQuery(queryBuffer.toString());
	}

	public void connectDb() throws ClassNotFoundException, SQLException {
		String driver = dbConfig.getDriver();
		String url = dbConfig.getUrl();
		String username = dbConfig.getUsername();
		String password = dbConfig.getPassword();
		Class.forName(driver);
		connection = DriverManager.getConnection(url, username, password);
//		String driver = "com.mysql.jdbc.Driver";
//		String url = "jdbc:mysql://10.20.2.75:3306/iCIM_CX";
//		String username = "root";
//		String password = "Ivo123";
//		Class.forName(driver);
//		connection = DriverManager.getConnection(url, username, password);
	}

	public void disConnectDb() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
