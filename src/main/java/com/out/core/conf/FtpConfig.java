package com.out.core.conf;

import org.springframework.stereotype.Component;

/**   
* @Title: FtpConfig.java
* @Package com.out.core.conf
* @Description: TODO(添加描述)
* @author Lynn.Sea
* @date 2014-6-4 下午1:01:46    
* @version V1.0   
*/
public class FtpConfig {

	private String destinFilePath;
	private String host;
	private int port;
	private String username;
	private String password;
	private String localTempPath;

	public String getDestinFilePath() {
		return destinFilePath;
	}

	public void setDestinFilePath(String destinFilePath) {
		this.destinFilePath = destinFilePath;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLocalTempPath() {
		return localTempPath;
	}

	public void setLocalTempPath(String localTempPath) {
		this.localTempPath = localTempPath;
	}

}
