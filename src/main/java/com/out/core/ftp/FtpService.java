package com.out.core.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import com.out.core.conf.FtpConfig;

public class FtpService {
	private Logger logger = Logger.getLogger(FtpService.class);
	private FTPClient ftpClient;
	private FtpConfig ftpConfig;
	public FtpConfig getFtpConfig() {
		return ftpConfig;
	}

	public void setFtpConfig(FtpConfig ftpConfig) {
		this.ftpConfig = ftpConfig;
	}

	/**
	 * 连接FTP
	 * @throws IOException 
	 */
	public void connectFtp() throws IOException {
		ftpClient = new FTPClient();
		ftpClient.connect(ftpConfig.getHost(), ftpConfig.getPort());
		ftpClient.login(ftpConfig.getUsername(), ftpConfig.getPassword());
		ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
		int reply = ftpClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftpClient.disconnect();
			logger.error("FTP server refused connection.");
			System.exit(1);
		}
		ftpClient.changeWorkingDirectory(ftpConfig.getDestinFilePath());
	}

	/**
	 * 断开FTP连接
	 */
	public void disconnectFtp() {
		if (ftpClient != null) {
			try {
				ftpClient.disconnect();
			} catch (IOException e) {
				logger.error("关闭连接时出错" + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public void uploadFile(File file) throws IOException {
		if (file.isDirectory()) {
			logger.error("file type can not be director");
			return;
		} else {
			//            File file2 = new File(file.getPath());      
			FileInputStream input = new FileInputStream(file);
			ftpClient.storeFile(file.getName(), input);
			input.close();
		}

	}
}
