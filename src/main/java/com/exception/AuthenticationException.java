package com.exception;

/**
 * Common exception for Authenticating license key.
 * 
 * @author Anthony.M
 *
 */
public class AuthenticationException extends Exception {

	private static final long serialVersionUID = -3254692462175207184L;

	private String message;

	public String getMessage() {
		return message;
	}

	public AuthenticationException(String message) {
		this.message = message;
	}

}
