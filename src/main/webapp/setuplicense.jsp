<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Setup License</title>
<link rel="stylesheet" href="<%=path%>/lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=path%>/lib/bootstrap/css/bootstrap-responsive.min.css">
<script type="text/javascript" src="<%=path%>/lib/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path%>/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    var w1 = $("#licenseKey").width();
	    var w2 = $("#currentLicenseKey").width();
	    $("#licenseKey").css({
	        "min-width" : w1 + "px",
	        "max-width" : w1 + "px"
	    });
	    $("#currentLicenseKey").css({
	        "min-width" : w2 + "px",
	        "max-width" : w2 + "px"
	    });
	    $("#licenseKey").val("");
	    $("#installOrUpdateLicense").on("click", function() {
	    	var licenseKey = $.trim($("#licenseKey").val());
	    	if (licenseKey !== "") {
		    var action = "<%=path%>/jcom/installOrUpdateLicense.action";
			    $.post(action, {
				    "licenseKey" : licenseKey
			    }, function(data) {
				    if (!data.success) {
					    var s = '<p class="text-error">';
					    s += data.message;
					    s += '</p>';
					    $("#messageBox").html(s);
				    } else {
					    window.location.reload();
				    }
			    });
		    }
	    });
    });
</script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="span6">
                <form>
                    <fieldset>
                        <legend>Install Or Update License</legend>
                        <label for="serverId">Server ID</label>
                        <input class="span6" type="text" id="serverId" placeholder="Server ID" disabled="disabled" value="${serverId}">
                        <label for="licenseKey">License Key</label>
                        <textarea class="span6" id="licenseKey" rows="3" placeholder="License Key"></textarea>
                        <button type="button" id="installOrUpdateLicense" class="btn">Submit</button>
                    </fieldset>
                </form>
            </div>
            <div class="span6">
                <form>
                    <fieldset>
                        <legend>Current Installed License</legend>
                        <label for="currentLicenseKey">Current License Key</label>
                        <textarea class="span6" id="currentLicenseKey" rows="3" placeholder="Current License Key">${license.encodedKey}</textarea>
                        <label for="currentLicenseKey">Expire Date</label>
                        <input class="span6" type="text" id="expireDate" placeholder="Expire Date" value="${license.expireDate}">
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="row">
            <div id="messageBox" class="span6"></div>
        </div>
    </div>
</body>
</html>