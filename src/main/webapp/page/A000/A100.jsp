<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_A100_TITLE_TAG" /></title>
<%--       <style>
      	   #userFuncGroupDialog {
	        margin-left: 10%;
	        width: 70%;
	       	height:70%;
	        left: 8%;
	      }
	      .modal-body {
	       	height:80%;
	      }
      </style> --%>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

</head>
<body>
	<div class="container">
		<div class="col-md-12">
			<button class="btn btn-primary" id="f1_query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="f6_btn">
				<s:text name="F6_UPDATE_USER_PSWD_TAG" />
			</button>
			<button class="btn btn-primary" id="f9_btn">
				<s:text name="F9_GROUP_AUTH_TAG" />
			</button>
			<button class="btn btn-primary" id="userFuncGroupDialog_executeBtn">
				<s:text name="F8_EXECUTE_TAG" />
			</button>
		</div>
	</div>
	<div class="container">
		<div class="col-md-5">
			<fieldset>
				<legend>
					<s:text name="USER_INFO_TAG" />
				</legend>
				<div id="userInfoDiv">
					<table id="userInfoGrd"></table>
					<div id="userInfoPg"></div>
				</div>
			</fieldset>
		</div>

		<div class="col-md-7">
			<fieldset>
				<legend>
					<s:text name="USER_FUNC_GROUP_AUTH_TAG" />
				</legend>
				<div id="userFncGroupDiv">
					<table id="userFncGroupGrd"></table>
					<div id="userFncGroupPg"></div>
				</div>
			</fieldset>
		</div>
	</div>
	<!-- 查询对话框 -->

	<div id="userInfoDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="PLEASE_INPUT_USER_INFO_TAG" />
					</h4>
				</div>

				<!--Modal header-->

				<div class="modal-body">
					<div class="row">
						<form id="userInfoDialog_Form" class="form-horizontal">
							<div class="control-group ">
								<label class="control-label col-sm-4" for="userInfoDialog_userIDTxt"> <s:text name="USER_ID_TAG" />
								</label>
								<div class="col-sm-6">
									<input type="text" id="userInfoDialog_userIDTxt" class="form-control" maxlength="10" data-original-title="必须输入不超过10位的非中文字符"
										onKeyUp="value=value.replace(/[\W]/g,''); this.value=this.value.toLocaleUpperCase() " />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-sm-4" for="userInfoDialog_userNameTxt"> <s:text name="USER_NAME_TAG" />
								</label>
								<div class="col-sm-6">
									<input type="text" id="userInfoDialog_userNameTxt" class="form-control" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-sm-4" for="userInfoDialog_pswdTxt"> <s:text name="PASSWORD_TAG" />
								</label>
								<div class="col-sm-6">
									<input type="password" id="userInfoDialog_pswdTxt" class="form-control" />
								</div>
							</div>
						</form>

					</div>
				</div>

				<!--Modal body-->
				<div class="modal-footer">
					<a id="userInfoDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="userInfoDialog_executeBtn" class="btn btn-primary"> <s:text name="F8_EXECUTE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>


	<div id="queryUserDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div class="row">
						<form id="queryUserDialog_userForm" class="row form-horizontal">
							<div class="row">
								<label class="control-label col-sm-4" for="queryUserDialog_userIDTxt"> <s:text name="USER_ID_TAG" />
								</label>
								<div class="col-sm-4">
									<input type="text" id="queryUserDialog_userIDTxt" class="form-control" placeholder="请输入用户代码">
								</div>
							</div>
						</form>
					</div>
				</div>

				<!--Modal body-->

				<div class="modal-footer">
					<a id="queryUserDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryUserDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>

				</div>
			</div>
		</div>
	</div>

	<!-- Dialog-->
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
	<script src="js/dialog/showSuccess.js"></script>
	<script src="js/page/A000/A100.js"></script>

</body>
</html>
