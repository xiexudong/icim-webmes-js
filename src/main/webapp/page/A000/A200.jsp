<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>">

<title><s:text name="M_A200_TITLE_TAG" /></title>


<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

<style>
body {
	padding-top: 60px;
}

.keyCss:after {
	float: right;
	/* font-size: 20px; */
	content: '*';
	color: red;
	/* margin-left: 2px; */
}

#userListDiv {
	position: absolute;
	margin-top: 55px;
}

#deptInfoDiv {
	position: absolute;
	height: 80%;
}

.tab-pane {
	margin-top: 5px;
}
</style>

</head>

<body>

	<div class="container">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>

		<button class="btn btn-danger" id="f4_del_btn">
			<s:text name="F4_DELETE_TAG" />
		</button>

		<button class="btn btn-warning" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG" />
		</button>

		<button class="btn btn-primary" id="f6_add_btn">
			<s:text name="F6_ADD_TAG" />
		</button>

		<button class="btn btn-info" id="f9_save_btn">
			<s:text name="F9_SAVE_BTN" />
		</button>

		<button class="btn btn-warning" id="f10_clear_btn">
			<s:text name="F10_CLEAR_TAG" />
		</button>

	</div>

	<div class="container">
		<div class="container-fluid-fluid">
			<div class="col-md-12">
				<h3>
					<s:text name="USER_INFO_TAG" />
				</h3>
			</div>
		</div>

		<div id="userListDiv" class="col-md-3">
			<table id="userListGrd"></table>
			<div id="userListPg"></div>
		</div>

		<div id="tabDiv" class="col-md-offset-5 col-md-8">
			<ul id="mytab" class="nav nav-tabs">
				<li class="active"><a id="tab1" href="#tabPane_user" data-toggle="tab"> <s:text name="USER_DETAIL_INFO_TAG" />
				</a></li>
				<li><a id="tab2" href="#tabPane_dept" data-toggle="tab" class="hide"> <s:text name="DEPT_DETAIL_INTO_TAG" />
				</a></li>
				<li><a id="tab3" href="#settings" data-toggle="tab" class="hide"> <s:text name="DEPT_ORGNIZATION_TREE_TAG" />
				</a></li>
				<%-- <li><a id="tab4" href="#fab" data-toggle="tab"> <s:text name="USER_LINK_FAB_TAG" />
				</a></li> --%>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tabPane_user">
					<!-- <div class="container-fluid span8"> -->

					<form id="userConditionForm" class="form-horizontal">
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 keyCss" for="userIDTxt"> <s:text name="USER_ID_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userIDTxt" class="form-control user" />
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 keyCss" for="userNameTxt"> <s:text name="USER_NAME_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userNameTxt" class="form-control user" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5" for="userENameTxt"> <s:text name="USER_ENGLISH_NAME_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userENameTxt" class="form-control user" />
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 " for="userCateSel"> <s:text name="USER_CATEGORY_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="userCateSel" class="form-control user"></select>
								</div>
							</div>
						</div>
<%-- 						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 keyCss" for="userDeptIDSel"> <s:text name="DEPT_ID_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="userDeptIDSel" class="form-control user"></select>
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 " for="userFromTxt"> <s:text name="USER_COME_FROM_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userFromTxt" class="form-control user" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 " for="userLevelTxt"> <s:text name="USER_LEVEL_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userLevelTxt" class="form-control user" />
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 " for="userPostTxt"> <s:text name="USER_POST_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userPostTxt" class="form-control user" />
								</div>
							</div>
						</div>--%>
						<div class="row">
<%-- 							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 " for="userWorkBayTxt"> <s:text name="USER_WORK_BAY_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userWorkBayTxt" class="form-control user" />
								</div>
							</div> --%>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 keyCss" for="userRegisterTimeTxt"> <s:text name="USER_REGISTER_DATE" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userRegisterTimeTxt" class="form-control user" />
								</div>
							</div>
						</div> 
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 " for="userPhoneTxt"> <s:text name="USER_PHONE_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userPhoneTxt" class="form-control user" />
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 " for="userTelTxt"> <s:text name="USER_TEL_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userTelTxt" class="form-control user" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-5 " for="userMailTxt"> <s:text name="USER_MAIL_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="userMailTxt" class="form-control user" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 control-group">
								<label class="control-label col-sm-2" for="inlineCheckboxes">选择项</label>
								<div class="col-sm-8 form-inline">
									<%-- <span class="checkbox"> <input type="checkbox" id="officerChk" value="option1" class="user"> <s:text name="HEAD_OF_DEPT_FLAG_TAG" />
									</span> --%> <span class="checkbox inline"> <input type="checkbox" id="validFlgChk" value="option2" class="user"> <s:text name="VALID_FLG_TAG" />
									</span> <span class="checkbox inline"> <input type="checkbox" id="lockFlgChk" value="option3" class="user"> <s:text name="LOCK_FLG_TAG" />
									</span>
								</div>
							</div>
						</div>
					</form>
					<!-- </div> -->
				</div>
				<div class="tab-pane" id="tabPane_dept">
					<div id="deptInfoDiv" class="container-fluid span7">
						<button id="listDept" class="btn btn-primary">查询</button>
						<button id="addDept" class="btn btn-primary">添加</button>
						<button id="updateDept" class="btn btn-warning">修改</button>
						<button id="deleteDept" class="btn btn-danger">删除</button>
						<button id="saveDept" class="btn btn-primary">保存</button>
						<form id="deptConditionForm" class="container-fluid span8 form-horizontal">
							<div class="control-group"></div>
							<div class="control-group"></div>
							<div class="container-fluid">
								<div class="col-sm-6 control-group">
									<label class="control-label col-sm-5 keyCss" for="DeptIDTxt"> <s:text name="DEPT_ID_TAG" />
									</label>
									<div class="col-sm-7">
										<input type="text" id="DeptIDTxt" class="form-control dept">
									</div>
								</div>
								<div class="col-sm-6 control-group">
									<label class="control-label col-sm-5 keyCss" for="DeptNameTxt"> <s:text name="DEPT_NAME_TAG" />
									</label>
									<div class="col-sm-7">
										<input type="text" id="DeptNameTxt" class="form-control dept">
									</div>
								</div>
							</div>
							<div class="container-fluid">
								<div class="col-sm-6 control-group">
									<label class="control-label col-sm-5 " for="DeptShortNameTxt"> <s:text name="DEPT_SHORT_NAME_TAG" />
									</label>
									<div class="col-sm-7">
										<input type="text" id="DeptShortNameTxt" class="form-control dept">
									</div>
								</div>
								<div class="col-sm-6 control-group">
									<label class="control-label col-sm-5 " for="DeptOffcierTxt"> <s:text name="HEAD_OF_DEPT_TAG" />
									</label>
									<div class="col-sm-7">
										<input type="text" id="DeptOffcierTxt" class="form-control dept">
									</div>
								</div>
							</div>
							<div class="container-fluid">
								<div class="col-sm-6 control-group">
									<label class="control-label col-sm-5 keyCss" for="DeptLevelSel"> <s:text name="DEPT_LEVEL_TAG" />
									</label>
									<div class="col-sm-7">
										<select id="DeptLevelSel" class="form-control sel dept"></select>
									</div>
								</div>
								<div class="col-sm-6 control-group">
									<label class="control-label col-sm-5 keyCss" for="DeptSuperSel"> <s:text name="SUPERVISOR_DEPT_ID_TAG" />
									</label>
									<div class="col-sm-7">
										<select id="DeptSuperSel" class="form-control sel dept"></select>
									</div>
								</div>
							</div>
							<div class="container-fluid">
								<div class="col-sm-6 control-group">
									<label class="control-label col-sm-5" for="inlineCheckboxes">选择项</label>
									<div class="col-sm-7">
										<span class="checkbox inline"> <input type="checkbox" id="deptvalidFlgChk" value="option1"> <s:text name="VALID_FLG_TAG" />
										</span>
									</div>
								</div>
							</div>
						</form>
						<div id="deptListDiv" class="container-fluid" style="margin-left: 10px">
							<table id="deptListGrd"></table>
							<div id="deptListPg"></div>
						</div>
					</div>

				</div>
				<div class="tab-pane" id="settings">
					<div class="container-fluid">
						<div class="container-fluid-fluid">
							<div class="span9">
								<h5>
									<i class="icon-calendar"></i>
									<s:text name="DEPT_ORGNIZATION_TREE_TAG" />
								</h5>
							</div>
						</div>
					</div>
					<div id="leftchoose" class="col-md-6">
						<div class="col-sm-7">
							<ul id="tree" class="ztree" style="height: 360px"></ul>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="fab">
					<div id="fabInfoDiv" class="container-fluid span7">
						<button id="addFab" class="btn btn-primary">添加</button>
						<button id="deleteFab" class="btn btn-danger">删除</button>
						<span class="label label-warning">对厂别的所有更改都要==>保存</span>
					</div>
					<div id="fabListDiv" class="container-fluid span4">
						<table id="fabListGrd"></table>
						<div id="fabListPg"></div>
					</div>
				</div>
				<div class="tab-pane" id="ope">
					<div id="opeInfoDiv" class="container-fluid span7">
						<button id="addOpe" class="btn btn-primary">添加</button>
						<button id="deleteOpe" class="btn btn-danger">删除</button>
						<button id="saveOpe" class="btn btn-info">保存</button>
					</div>
					<div id="opeListDiv" class="container-fluid span4">
						<table id="opeListGrd"></table>
						<div id="opeListPg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 查询对话框 -->

	<div id="queryUserDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>

				<!--Modal header-->
				<div class="modal-body">
					<div class="container-fluid">
						<form id="queryUserDialog_userForm" class="container-fluid form-horizontal">
							<div class="control-group ">
								<label class="control-label col-sm-4" for="queryUserDialog_userIDTxt"> <s:text name="USER_ID_TAG" />
								</label>
								<div class="col-sm-4">
									<input type="text" id="queryUserDialog_userIDTxt" class="form-control" />
								</div>
								<div class="col-sm-4">
									<p class="form-control-static" id="queryUserDialog_helpLineSpn">请输入用户代码 </p>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="queryMdlDefDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryMdlDefDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>
				</div>
			</div>
			<!-- 添加厂别 -->
			<div id="fabInfoDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">
								<s:text name="CHOOSE_FAB_ID_TAG" />
							</h4>
						</div>
						<!--Modal header-->
						<div class="modal-body">
							<div class="container-fluid">
								<form id="fabInfoDialog_Form" class="container-fluid form-horizontal">
									<div class="control-group ">
										<label class="control-label col-sm-5" for="fabInfoDialog_fabIdSelect"> <s:text name="FAB_ID_TAG" />
										</label>
										<div class="col-sm-7">
											<select id="fabInfoDialog_fabIdSelect"></select>
										</div>

									</div>
								</form>
							</div>
						</div>

						<!--Modal body-->
						<div class="modal-footer">
							<a id="fabInfoDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
							</a> <a id="fabInfoDialog_queryBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 添加站点信息 -->
	<div id="opeInfoDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="SELECT_OPE_TAG" />
					</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid" id="opeList2Div">
						<fieldset>
							<legend>
								<s:text name="OPE_ID_TAG" />
							</legend>
							<div id="opeList2Div" class="col-sm-offset-1 col-sm-11">
								<table id="opeList2Grd"></table>
								<div id="opeList2Pg"></div>
							</div>
						</fieldset>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="opeInfoDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="opeInfoDialog_queryBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/A000/A200.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<link rel="stylesheet" href="lib/jQueryUI-ztree/zTreeStyle/zTreeStyle.css" type="text/css">
<link rel="stylesheet" href="css/page/A000/A200.css" type="text/css">
<script type="text/javascript" src="lib/jQueryUI-ztree/jquery.ztree.all-3.5.min.js"></script>
</html>