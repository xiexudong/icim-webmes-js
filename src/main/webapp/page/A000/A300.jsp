<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>">

<title><s:text name="M_A300_TITLE_TAG" /></title>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

<style>
#eqptListDiv,#fabEqptListDiv,#lineListDiv,#destListDiv{
    width:550px;
}
body {
	padding-top: 60px;
}

.keyCss:after {
	float: right;
	/* font-size: 20px; */
	content: '*';
	color: red;
	/* margin-left: 2px; */
}

#userListDiv {
	position: absolute;
	height: 60%;
}

#deptInfoDiv {
	position: absolute;
	height: 80%;
}

.tab-pane {
	margin-top: 5px;
}
</style>

</head>

<body>

	<div class="container">
		<div class="container-fluid">
			<button class="btn btn-info" id="f1_query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
		</div>
	</div>

	<div class="container">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="col-lg-12">
					<h3>
						<s:text name="USER_FAB_OPER_EQPT_DEST_TAG" />
					</h3>
				</div>
			</div>
		</div>
	</div>
	<%--<div id ="userListDiv" class="row span3">
			<table id="userListGrd"></table>
			<div id="userListPg"></div>
		</div>                 

		--%>
	<div class="container">
		<div id="tabDiv" class="col-lg-7">
			<ul id="mytab" class="nav nav-tabs">
				<!-- <li class="active"><a id="tab1" href="#fabOpe" data-toggle="tab"> <s:text name="FAB_LINK_OPE_TAG" />
				</a></li>
				<li><a id="tab2" href="#fabEqpt" data-toggle="tab"> <s:text name="FAB_LINK_EQPT_TAG" />
				</a></li> -->
				<li class="active"><a id="tab3" href="#ope" data-toggle="tab"> <s:text name="USER_LINK_OPE_TAG" />
				</a></li>
				<%--<li><a id="tab4" href="#eqpt" data-toggle="tab"> <s:text name="USER_LINK_EQPT_TAG" />
				</a></li>--%>
				<li><a id="tab5" href="#dest" data-toggle="tab"> <s:text name="USER_LINK_DEST_TAG" />
				</a></li>
				<!-- <li><a id="tab6" href="#line" data-toggle="tab"> <s:text name="用户与线别" />
				</a></li> -->
			</ul>

			<div class="tab-content">
				<div class="tab-pane row" id="fabOpe">
					<div id="fabOpeInfoDiv" class="col-sm-11">
						<div class="row control-group">
							<form class="form-horizontal">
								<label class="control-label col-sm-4" for="fabIdSelect"><s:text name="FAB_ID_TAG" /></label>
								<div class="col-sm-4">
									<select id="fabIdSelect" class="form-control"></select>
								</div>
								<div class="col-sm-4"></div>
							</form>
						</div>
						<button id="addFabOpe" class="btn btn-primary">添加</button>
						<button id="deleteFabOpe" class="btn btn-danger">删除</button>
						<button id="savedFabOpe" class="btn btn-info">保存</button>
					</div>
					<div id="fabOpeListDiv" class="col-sm-11">
						<table id="fabOpeListGrd"></table>
						<div id="fabOpeListPg"></div>
					</div>
				</div>
				<div class="tab-pane" id="fabEqpt">
					<div id="fabEqptInfoDiv" class="col-sm-11">
						<div class="row control-group">
							<form class="form-horizontal">
								<label class="control-label col-sm-3" for="fabIdSelect1"><s:text name="FAB_ID_TAG" /></label>
								<div class="col-sm-5">
									<select id="fabIdSelect1" class="form-control"></select>
								</div>
								<div class="col-sm-4"></div>
							</form>
						</div>
						<button id="addFabEqpt" class="btn btn-primary">添加</button>
						<button id="deleteFabEqpt" class="btn btn-danger">删除</button>
						<button id="savedFabEqpt" class="btn btn-info">保存</button>
					</div>
					<div id="fabEqptListDiv" class="col-sm-11">
						<table id="fabEqptListGrd"></table>
						<div id="fabEqptListPg"></div>
					</div>
				</div>
				<div class="tab-pane active" id="ope">
					<div id="opeInfoDiv" class="col-sm-11">
						<div class="row control-group">
							<form class="form-horizontal">
								<label class="control-label col-sm-3 " for="ope_userIdSelect"><s:text name="USER_ID_TAG" /></label>
								<div class="col-sm-5">
									<%-- <select id="ope_userIdSelect" class="span2"></select> --%>
									<input type="text" id="ope_userIdTxt" class="form-control"></input>
								</div>
								<div class="col-sm-4"></div>
							</form>
						</div>

						<button id="addOpe" class="btn btn-primary">添加</button>
						<button id="deleteOpe" class="btn btn-danger">删除</button>
						<button id="saveOpe" class="btn btn-info">保存</button>
					</div>
					<div id="opeListDiv" class="col-sm-11">
						<table id="opeListGrd"></table>
						<div id="opeListPg"></div>
					</div>
				</div>
				<div class="tab-pane" id="eqpt">
					<div id="eqptInfoDiv" class="col-sm-11">
						<div class="row control-group">
							<form class="form-horizontal">
								<label class="control-label col-sm-3" for="eqpt_userIdSelect"><s:text name="USER_ID_TAG" /></label>
								<div class="col-sm-5">
									<%-- <select id="eqpt_userIdSelect" class="span2"></select> --%>
									<input type="text" id="eqpt_userIdTxt" class="form-control"></input>
								</div>
							</form>
						</div>
						<button id="addEqpt" class="btn btn-primary">添加</button>
						<button id="deleteEqpt" class="btn btn-danger">删除</button>
						<button id="saveEqpt" class="btn btn-info">保存</button>
					</div>
					<div id="eqptListDiv" class="col-sm-11">
						<table id="eqptListGrd"></table>
						<div id="eqptListPg"></div>
					</div>
				</div>
				<div class="tab-pane" id="dest">
					<div id="destInfoDiv" class="col-sm-11">
						<div class="row control-group">
							<form class="form-horizontal">
								<label class="control-label col-sm-3" for="dest_userIdSelect"><s:text name="USER_ID_TAG" /></label>
								<div class="col-sm-5">
									<%--  <select id="dest_userIdSelect" class="span2"></select> --%>
									<input type="text" id="dest_userIdTxt" class="form-control"></input>
								</div>
							</form>
						</div>
						<button id="addDest" class="btn btn-primary">添加</button>
						<button id="deleteDest" class="btn btn-danger">删除</button>
						<button id="saveDest" class="btn btn-info">保存</button>
					</div>
					<div id="destListDiv" class="col-sm-11">
						<table id="destListGrd"></table>
						<div id="destListPg"></div>
					</div>
				</div>
				<div class="tab-pane" id="line">
					<div id="lineInfoDiv" class="col-sm-11">
						<div class="row control-group">
							<form class="form-horizontal">
								<label class="control-label col-sm-3" for="line_userIdTxt"><s:text name="USER_ID_TAG" /></label>
								<div class="col-sm-3">
									<input type="text" id="line_userIdTxt" class="form-control"></input>
								</div>
							</form>
						</div>
						<button id="addLine" class="btn btn-primary">添加</button>
						<button id="deleteLine" class="btn btn-danger">删除</button>
						<button id="saveLine" class="btn btn-info">保存</button>
					</div>
					<div id="lineListDiv" class="col-sm-11">
						<table id="lineListGrd"></table>
						<div id="lineListPg"></div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-lg-4">
			<fieldset class="col-sm-12">
				<legend>
					<s:text name="用户信息" />
				</legend>
				<div class="col-xs-12">
					<div class="form-group">
						<label class="control-label col-sm-4" for="search_userid">用户代码</label>
						<div class="col-sm-8">
							<input type="text" id="search_userid" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="search_username">用户名称</label>
						<div class="col-sm-8">
							<input type="text" id="search_username" class="form-control" />
						</div>
					</div>

				</div>
				<div id="opeuserListGrddiv" class="col-xs-12" style="margin-top: 15px">
					<table id="opeuserListGrd"></table>
					<div id="opeuserListPg"></div>
				</div>
			</fieldset>
		</div>
	</div>

	<!-- 查询对话框 -->

	<div id="queryUserDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>

				<!--Modal header-->
				<div class="modal-body">
					<div class="row">
						<form id="queryUserDialog_userForm" class="row form-horizontal">
							<div class="control-group ">
								<label class="control-label col-md-4" for="queryUserDialog_userIDTxt"> <s:text name="USER_ID_TAG" />
								</label>
								<div class="col-md-4">
									<input type="text" id="queryUserDialog_userIDTxt" class="form-control" /> <span id="queryUserDialog_helpLineSpn" class="help-inline"> 请输入用户代码 </span>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="queryMdlDefDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryMdlDefDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 添加站点信息 -->
	<div id="opeInfoDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="SELECT_OPE_TAG" />
					</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<fieldset>
							<legend>
								<s:text name="OPE_ID_TAG" />
							</legend>
							<div id="opeList2Div" class="col-xs-offset-1 col-xs-8">
								<table id="opeList2Grd"></table>
								<div id="opeList2Pg"></div>
							</div>
						</fieldset>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="opeInfoDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="opeInfoDialog_queryBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 添加机台信息 -->
	<div id="eqptInfoDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="SELECT_TOOL_TAG" />
					</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<fieldset>
							<legend>
								<s:text name="TOOL_ID_TAG" />
							</legend>
							<div id="eqptList2Div" class="col-xs-offset-1 col-xs-8">
								<table id="eqptList2Grd"></table>
								<div id="eqptList2Pg"></div>
							</div>
						</fieldset>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="eqptInfoDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="eqptInfoDialog_queryBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>

	<!-- 添加机台信息 -->
	<div id="eqptInfoDialog2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="eqptInfoDialog2-title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="eqptInfoDialog2-title">
						<s:text name="SELECT_TOOL_TAG" />
					</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<fieldset>
							<legend>
								<s:text name="TOOL_ID_TAG" />
							</legend>
							<div id="fabEqptList2Div" class="col-xs-offset-1 col-xs-8">
								<table id="fabEqptList2Grd"></table>
								<div id="fabEqptList2Pg"></div>
							</div>
						</fieldset>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="eqptInfoDialog2_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="eqptInfoDialog2_queryBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
					</a>
				</div>
			</div>

		</div>
	</div>
	<!-- 添加仓位信息 -->
	<div id="destInfoDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="SELECT_DEST_TAG" />
					</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<fieldset>
							<legend>
								<s:text name="DEST_SHOP_TAG" />
							</legend>
							<div id="destList2Div" class="col-xs-offset-1 col-xs-8">
								<table id="destList2Grd"></table>
								<div id="destList2Pg"></div>
							</div>
						</fieldset>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="destInfoDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="destInfoDialog_queryBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 添加厂别站点信息 -->
	<div id="fabOpeInfoDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="SELECT_OPE_TAG" />
					</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<fieldset>
							<legend>
								<s:text name="OPE_ID_TAG" />
							</legend>
							<div id="fabOpeList2Div" class="col-xs-offset-1 col-xs-8">
								<table id="fabOpeList2Grd"></table>
								<div id="fabOpeList2Pg"></div>
							</div>
						</fieldset>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="fabOpeInfoDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="fabOpeInfoDialog_queryBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 添加线别信息 -->
	<div id="lineInfoDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="选择线别" />
					</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<fieldset>
							<legend>
								<s:text name="线别" />
							</legend>
							<div id="lineList2Div" class="col-xs-offset-1 col-xs-8">
								<table id="lineList2Grd"></table>
								<div id="lineList2Pg"></div>
							</div>
						</fieldset>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="lineInfoDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="lineInfoDialog_queryBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/A000/A300.js"></script>
</html>