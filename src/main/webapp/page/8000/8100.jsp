<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
   <head>
      <base href="<%=basePath%>">
      <title><s:text name="M_8100_TITLE_TAG"/></title>
	    
		<!-- 引入 jQuery样式-->
		<link rel="stylesheet" type="text/css" href="lib/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css" />
		<link rel="stylesheet" type="text/css" href="lib/jquery-ui-1.9.2.custom/development-bundle/themes/base/jquery.ui.all.css" />
		<!-- Bootstrap -->
	    <link href="lib/bootstrap/assets/css/bootstrap.css" rel="stylesheet" media="screen">	    
	    <!-- jqgrid -->
		<link href="lib/jquery.jqGrid-4.4.1/css/ui.jqgrid.css" rel="stylesheet"media="screen"  />	    
	    <style>
	      body {
	        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
	      }
	    </style>
	    <link href="lib/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">  
	    
	    <!-- Fav and touch icons -->
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" 
	      href="lib/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" 
	      href="lib/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" 
	      href="lib/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" 
	      href="lib/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
	    <link rel="shortcut icon" 
	      href="lib/bootstrap/assets/ico/favicon.png">
	      
   </head>
   <body>
     <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">iMES</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">联系我们</a></li>
              <li><a href="#usage" id="usageHref">使用说明</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    
    <div >
	    <div class="span12">
	        <button class="btn" id="f3_btn">F3:退出 </button>
	        <button class="btn" id="f1_btn">F1:查询 </button>
	    </div>
      <div class="span12">
          <div class="control-group">
            <form class="well form-inline">
              <label class="control-label" for="alarmCate">报警类别</label>
              <select id="alarmCateSlt">
                <option value=''>----请选择----</option>
              </select>
              <label class="control-label offset1" for="updateTimeSpan">更新时间</label>
              <span id="updateTimeSpan" class="input-xlarge uneditable-input"></span>
            </form>
          </div>
          
      </div>
	    <div class="span12" id="alarmInfoDiv">
	 	   <fieldset>
	     		<legend>信息浏览</legend>
	     		<table id="alarmInfoGrd"></table>
	           <div id="alarmInfoPg"></div>
	     	</fieldset>	
	    </div>                
   </div> 
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="lib/bootstrap/assets/js/jquery.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-transition.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-alert.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-modal.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-dropdown.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-scrollspy.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-popover.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-button.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-collapse.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-carousel.js"></script>
    <script src="lib/bootstrap/assets/js/bootstrap-typeahead.js"></script> 

	<!-- jquery UI核心库 -->
	<script src="lib/jquery-ui-1.9.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="lib/jquery-ui-1.9.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="lib/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>    
	<!-- jqGrid -->
	<script src="lib/jquery.jqGrid-4.4.1/js/i18n/grid.locale-en.js"></script>
	<script src="lib/jquery.jqGrid-4.4.1/js/jquery.jqGrid.src.js" ></script>
    
    <script src="lib/json2/json2.min.js" ></script>
    <script src="js/com/comfunction.js" ></script>

    <script src="js/page/8000/8110.js" ></script>
 
   </body>
</html>
