<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_7200_REPORT_SPC_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<link href="lib/HubSpot-messenger/build/css/messenger-theme-future.css"
	rel="stylesheet" media="screen" />
<link href="lib/HubSpot-messenger/build/css/messenger.css"
	rel="stylesheet" media="screen" />
<style>
#bodyDiv {
	margin: 30px 0 0 0;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#sidebar ul.navigation li {
	width: 2gro00px;
	height: 40px;
	position: relative;
	/*line-height:40px;*/
	background-image: url('img/menu-separator.jpg');
	backund-repeat: no-repeat;
	background-position: 0 35px;
}

.Header_Text {
	color: blue;
	background-color: #999;
	white-space: nowrap;
	border-left: 1px solid #fff;
	border-top: 1px solid #ccc;
	border-right: 1px solid #fff;
	border-bottom: 1px solid #fff;
	text-align: center;
	height: 24px;
	font-size: 12px;
}

.dataItemTdCss {
	text-align: center;
	border-left: 1px solid #fff;
	border-top: 1px solid #ccc;
	border-right: 1px solid #fff;
	border-bottom: 1px solid #fff;
}
/*  #itemInfoDiv{
	  	  	height:90%;
	  	  }
	  	  #itemDiv{
	  	  	height: 80%;
	  	  }*/
/* #itemDiv{
	  	  	margin: 200px 0 0 0;
	  	  }*/
</style>
<body>

	<div class="container-fluid">
		<div class="col-lg-12">
			<button class="btn btn-info hide" id="f1_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="f8_btn">
				<s:text name="F8_REPORT_TAG" />
			</button>
			<button class="btn btn-primary" id="rpt_all_btn">批量上报</button>
			<button class="btn btn-primary" id="clear_btn">清空输入值</button>

		</div>
		<div class="col-lg-12" id="grdDiv">
			<div class="form-group">
				<div class="controls">
					<legend>
						<i class="icon-calendar"></i>
						<s:text name="PLEASE_SELECT_TAG" />
					</legend>
				</div>
			</div>
			<div class="col-lg-12">
				<form id="unpackForm" class="form-horizontal">
					<div class="row">
						<div id="rbxDiv" class="col-lg-12">
							<label class="col-lg-4"> <input type="radio" id="lotRdb"
								name="items" value="lot" /> <s:text name="LOT_ID_TAG" />
							</label> <label class="col-lg-4"> <input type="radio" id="boxRdb"
								name="items" value="box" /> <s:text name="BOX_ID_TAG" />
							</label> <label class="col-lg-4"> <input type="radio" id="prdSeqRdb"
								name="items" value="prdSeq" checked /> <s:text
									name="PRD_SEQ_ID_TAG" />
							</label>
						</div>
						<div class="form-group row col-lg-4">
							<label class="control-label col-lg-5" for="lotIDTxt"> <s:text
									name="LOT_ID_TAG" />
							</label>
							<div class="col-lg-6 ">
								<input type="text" id="lotIDTxt" class="form-control"/>
								<!-- <select id="bayAreaSel" class="span2"></select> -->
							</div>
						</div>
						<div class="form-group col-lg-4 ">
							<label class="control-label col-lg-5" for="boxIDTxt"> <s:text
									name="BOX_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<input type="text" id="boxIDTxt" class="form-control" />
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label col-lg-5" for="prdSeqIdTxt"> <s:text
									name="PRD_SEQ_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<input type="text" id="prdSeqIdTxt" class="form-control">
							</div>
						</div>
						<!-- TR -->

						<div class="form-group col-lg-4">
							<label class="control-label col-lg-5" for="opeIDSel"> <s:text
									name="OPE_ID_2_TAG" />
							</label>
							<div class="col-lg-6">
								<select id="opeIDSel"  class="form-control"></select>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label col-lg-5" for="toolIDSel"> <s:text
									name="TOOL_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<select id="toolIDSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label col-lg-5" for="mesIDSel"> 机台参数 </label>
							<div class="col-lg-6">
								<select id="mesIDSel" class="form-control"></select>
							</div>
						</div>
						<div id="rbxDiv2" class="col-lg-12">
							<label class="col-lg-4"> <input type="radio" id="woRdb"
								name="items" value="worder" /> <s:text name="WORDER_ID_TAG" />
							</label>
						</div>
						<div class="form-group row col-lg-12">
							<label class="control-label col-lg-2" for="woIDTxt"> <s:text
									name="WORDER_ID_TAG" />
							</label>
							<div class="col-lg-2">
								<input type="text" id="woIDTxt" class="form-control" />
								<!-- <select id="bayAreaSel" class="span2"></select> -->
							</div>
						</div>

<%-- 						<div class="form-group span3">
							<label class="control-label" for="opeIDSel"> <s:text
									name="OPE_ID_2_TAG" />
							</label>
							<div class="controls">
								<select id="opeIDSel"></select>
							</div>
						</div> --%>
					</div>
				</form>
			</div>

			<div id="prdSeqInfoDiv" class="col-lg-4">
				<div class="form-group">
					<div class="controls">
						<legend>
							<i class="icon-calendar"></i>
							<s:text name="BOX_INFO_TAG" />
						</legend>
					</div>
				</div>
				<div id="prdSeqInfoDiv" class="col-lg-4">
					<table id="prdSeqInfoGrd"></table>
					<div id="prdSeqInfoPg"></div>
				</div>
			</div>
			<div id="itemDiv" class="col-lg-7">
				<div class="form-group">
					<div class="controls">
						<legend>
							<i class="icon-calendar"></i>
							<s:text name="DATA_INFO_INFO_TAG" />
						</legend>
					</div>
				</div>
				<div id="thickNessDiv" class="col-lg-10">
					<form id="thicknessForm" class="row form-horizontal">
						<div class="form-group col-lg-6">
							<label class="control-label col-lg-5"  for="fromThicknessSpn"> <s:text
									name="FROM_THICKNESS_TAG" />
							</label>
							<div class="col-lg-7">
								<input type="text" id="fromThicknessSpn" class="form-control" disabled />
							</div>
						</div>
						<div class="form-group col-lg-6">
							<label class="control-label col-lg-5" for="toThicknessSpn"> <s:text
									name="TO_THICKNESS_2_TAG" />
							</label>
							<div class="col-lg-7">
								<input type="text" id="toThicknessSpn" class="form-control" disabled />
							</div>
						</div>
					</form>
				</div>
				<div id="itemInfoDiv" class="col-lg-7">
					<!-- <table id="itemInfoGrd"></table>
					<div id="itemInfoPg"></div> -->
					<!-- <table class="table">
						<tr>
							<td class="Header_Text span1" ><s:text name="参数序号"/> </td>
							<td class="Header_Text span1" ><s:text name="参数名称"/> </td>
							<td class="Header_Text span1" ><s:text name="组内序号"/> </td>
							<td class="Header_Text span1" ><s:text name="参数说明"/> </td>
							<td class="Header_Text span1" ><s:text name="值"/> </td>	
						</tr>
					</table> -->
				</div>
				<div id="dataItemEditDiv" class="col-lg-7"></div>


			</div>
		</div>

	</div>

	<%@ include file="/page/comPage/comJS.html"%>
	<script type="text/javascript" src="js/com/Regex.js"></script>
	<script src="js/page/7000/7200.js"></script>
	<script src="lib/HubSpot-messenger/build/js/messenger.min.js"></script>
	<script src="js/dialog/showMessenger.js"></script>
	<script type="text/javascript"
		src="js/dialog/showFabSnDetailInfoDialog.js"></script>
	<script type="text/javascript" src="js/com/SelectDom.js"></script>
	<script src="js/com/spcThicknessRule.js"></script>
</body>
</html>