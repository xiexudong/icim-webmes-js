<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
		 isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title>7300:显示管制图</title>
<meta charset="utf-8">
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<link
	href="lib/datetimepicker/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" media="screen">
<!-- <link rel="stylesheet" type="text/css" href="css/spc/spc.css"> -->
<style>
#bodyDiv {
	margin: 30px 0 0 0;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#sidebar ul.navigation li {
	width: 2gro00px;
	height: 40px;
	position: relative;
	/*line-height:40px;*/
	background-image: url('img/menu-separator.jpg');
	backund-repeat: no-repeat;
	background-position: 0 35px;
}

#calCpkDialogForm .control-group {
	margin-bottom: 3px;
}
#filterChartDialogForm .control-group {
	margin-bottom: 3px;
}

.modal-body{
    overflow:scroll;
    overflow-x:auto;
    overflow-y:auto;
}

#queryDialog,#filterChartDialog{
	margin-left: 10%;
	width: 60%;
	left: 10%;
	margin-top:5%;
}

#chartListDiv {
	height: 30%;
}
.addWith{
   width:180px;
}
#filterChartDialog{
	width: 60%;
	left: 3%;
	margin-top:8%;
}
	.time_style{
		padding-left:150px;
	}

 .axis path,
 .axis line{
	 fill: none;
	 stroke: black;
	 shape-rendering: crispEdges;
 }

.axis text {
	font-family: sans-serif;
	font-size: 11px;
}
form .form-group {
	margin: 3px 0px;
}
.form-horizontal {
	padding-top: 10px;
}
</style>
</head>
<body>

	<div class="container-fluid">

		<div class="col-lg-12 col-md-12 col-sm-12">

			<button class="btn btn-info" id="f1_btn">查询</button>
			<button class="btn btn-primary" id="f2_filter_btn">管制图筛选</button>
			<button class="btn btn-primary" id="f3_refresh_btn">刷新</button>
			<!-- <button class="btn btn-primary" id="f2_btn">过滤</button> -->
			<button class="btn btn-primary" id="f5_btn">计算CPK</button>
			<button class="btn btn-primary" id="f10_btn">清空界面</button>
			<button class="btn btn-primary" id="f11_btn">导出文件</button>
			<button class="btn btn-primary" id="f12_btn">按更多条件筛选</button>
			<button class="btn btn-primary" id="analysis_btn">直方图分析</button>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12">
			<h3 class="page-header">查询条件</h3>
			<form id="queryChartForm" class="form-horizontal">
				<div class="row">
					<div class="form-group col-lg-3 col-md-3 col-sm-3">
						<label class="control-label col-lg-4 col-md-4 col-sm-4" for="cusIdSel"> 客户 </label>
						<div class="col-lg-8 col-md-8 col-sm-8">
							<select id="cusIdSel" class="form-control col-lg-10 col-md-10 col-sm-10" style="width: 100%;">
							</select>
						</div>
					</div>
					<div class="form-group col-lg-3 col-md-3 col-sm-3">
						<label class="control-label col-lg-4 col-md-4 col-sm-4" for="mdlIdSel"> 产品名称 </label>
						<div class="col-lg-8 col-md-8 col-sm-8">
							<select id="mdlIdSel" class="form-control col-lg-10 col-md-10 col-sm-10" style="width: 100%;">
							</select>
						</div>
					</div>
					<div class="form-groupcol-lg-3 col-md-3 col-sm-3">
						<label class="control-label col-lg-4 col-md-4 col-sm-4" for="woIdSel"> 工单号 </label>
						<div class="col-lg-8 col-md-8 col-sm-8">
							<select id="woIdSel" class="form-control col-lg-10 col-md-10 col-sm-10" style="width: 100%;">
							</select>
						</div>
					</div>
				</div>
				<%--<div class="form-group col-lg-4">
                    <label class="control-label col-lg-4" for="lotIdSel"> 批次 </label>
                    <div class="form-control col-lg-8">
                        <select id="lotIdSel" class="col-lg-10">
                        </select>
                    </div>
                </div>--%>
				<div class="row">
					<div class="form-group col-lg-3 col-md-3 col-sm-3">
						<label class="control-label col-lg-4 col-md-4 col-sm-4" for="opeIdSel"> 站点信息 </label>
						<div class="col-lg-8 col-md-8 col-sm-8">
							<select id="opeIdSel" class="form-control col-lg-10 col-md-10 col-sm-10" style="width: 100%;">
							</select>
						</div>
					</div>
					<div class="form-group col-lg-3 col-md-3 col-sm-3">
						<label class="control-label col-lg-4 col-md-4 col-sm-4" for="eqptIdSel"> 设备代码 </label>
						<div class="col-lg-8 col-md-8 col-sm-8">
							<select id="eqptIdSel" class="form-control col-lg-10 col-md-10 col-sm-10" style="width: 100%;">
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 time_style">
						<input type="checkbox" id="reportBeginTimestampChk" />
						<label class="control-label" for="reportBeginDatepicker">上报开始时间</label>
						<div style="width:200px">
							<div id="reportBeginDatepicker" class="input-group input-append">
								<input data-format="yyyy-MM-dd" type="text" class="form-control"> <span class="input-group-addon add-on"> <i
									data-date-icon="glyphicon-calendar"
									data-time-icon="glyphicon-time"
									class="glyphicon glyphicon-calendar"></i>
						</span>
							</div>
						</div>
						<div style="width:200px">
							<div id="reportBeginTimepicker" class="input-group input-append">
								<input data-format="hh:mm:ss" type="text" class="form-control">
								<span class="input-group-addon add-on"> <i data-date-icon="glyphicon-calendar"
																		   data-time-icon="glyphicon-time" class="glyphicon glyphicon-time"></i>
						</span>
							</div>
						</div>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4" >
						<input type="checkbox" id="reportEndSTimestampChk"/>
						<label class="control-label keyCss" for="reportEndDatepicker">上报结束时间</label>
						<div style="width:200px">
							<div id="reportEndDatepicker" class="input-group input-append">
								<input data-format="yyyy-MM-dd" type="text" class="form-control"> <span class="input-group-addon add-on"> <i
									data-date-icon="glyphicon-calendar"
									data-time-icon="glyphicon-time"
									class="glyphicon glyphicon-calendar"></i>
						</span>
							</div>
						</div>
						<div style="width:200px">
							<div id="reportEndTimepicker" class="input-group input-append">
								<input data-format="hh:mm:ss" type="text" class="form-control">
								<span class="input-group-addon add-on"> <i data-date-icon="glyphicon-calendar"
																		   data-time-icon="glyphicon-time" class="glyphicon glyphicon-time"></i>
						</span>
							</div>
						</div>
					</div>
				</div>


			</form>
		</div>

<%--		<div class="col-lg-12 col-md-12 col-sm-12">
			<canvas id="chart"></canvas>
		</div>--%>

	  </div>

	<div id="canvastotal"></div>
	<%--<canvas id="chart"></canvas>--%>
	<!-- <div id="rawDataDiv" class="span12" ></div>
	<div id="chartDataDiv" class="span12 " ></div> -->

	<div id="rawDataDiv" class="col-lg-12 col-md-12 col-sm-12" >
	</div>
	<div id="chartDataDiv" class="col-lg-12 col-md-12 col-sm-12" >
		<!-- <fieldset>管制图数据</fieldset>
		<div id="chartDataGrd" class="span12" style="height: 150px;"></div>	 -->	
	</div>
	<div id="sumaryDataDiv" class="col-lg-12 hide" >
		<fieldset>汇总数据</fieldset>
		<div id="sumaryDataGrd" class="col-lg-12" style="height: 150px;"></div>
	</div>
	<table id="chartPointInfoTbl" class="hide"></table>
	<!-- 查询对话框 -->

	<div id="queryDialog" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-content">
		     <div class="modal-header">
			     <button type="button" class="close" data-dismiss="modal">&times;</button>
				     <center>
						<h3 class="modal-title">
							查询条件
						</h3>
			         </center>
		    </div>
		<!--Modal header-->
			<div class="modal-body">
				<div class="row">
					<form id="queryDialog_grpNameForm" class="row form-horizontal">
						<div class="form-group">
							<label class="control-label col-lg-5 col-md-5 col-sm-5" for="queryDialog_grpNameSel">
								管制图组 </label>
							<div class="col-lg-5 col-md-5 col-sm-5">
								<select id="queryDialog_grpNameSel" style="width:100%;"></select>
							</div>
						</div>
					</form>
					<div id="chartListDiv" class="col-lg-10 col-md-10 col-sm-10" style="height: 300px;">
						<table id="chartListGrd"></table>
					</div>
				</div>	
			</div>

		<!--Modal body-->

			<div class="modal-footer">
				<a id="queryDialog_cancelBtn" class="btn" data-dismiss="modal">取消</a>
				<a id="queryDialog_queryBtn" class="btn btn-primary"> 查询 </a>
				<%--<a id="queryMerge" class="btn btn-primary">合并显示</a>--%>
			</div>
	  </div>
	</div>

	<!-- 计算CPK对话框-->
	<div id="calCpkDialog"  class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<center>
				<h3 class="modal-title">计算结果</h3>
			</center>
		</div>
		<!--Modal header-->
		<div class="modal-body">
			<!-- <div class="row"> -->
			<form id="calCpkDialogForm" class="row form-horizontal">
				<div class="form-group col-lg-4 col-md-4 col-sm-4">
					<label class="control-label col-lg-5 col-md-5 col-sm-5" for="meanTxt"> MEAN: </label>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<input type="text" id="meanTxt" class="form-control" />
					</div>
				</div>

				<div class="form-group col-lg-4 col-md-4 col-sm-4">
					<label class="control-label col-lg-5 col-md-5 col-sm-5" for="sigmaTxt"> sigma </label>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<input type="text" id="sigmaTxt" class="form-control" />
					</div>
				</div>

				<div class="form-group col-lg-4 col-md-4 col-sm-4">
					<label class="control-label col-lg-5 col-md-5 col-sm-5" for="caTxt"> ca </label>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<input type="text" id="caTxt"class="form-control"  />
					</div>
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-4">
					<label class="control-label col-lg-5 col-md-5 col-sm-5" for="cpTxt"> cp </label>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<input type="text" id="cpTxt" class="form-control" />
					</div>
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-4">
					<label class="control-label col-lg-5 col-md-5 col-sm-5" for="cpkTxt"> cpk </label>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<input type="text" id="cpkTxt" class="form-control"  />
					</div>
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-4">
					<label class="control-label col-lg-5 col-md-5 col-sm-5" for="ppTxt"> pp </label>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<input type="text" id="ppTxt" class="form-control"  />
					</div>
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-4">
					<label class="control-label col-lg-5 col-md-5 col-sm-5" for="ppkTxt"> ppk </label>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<input type="text" id="ppkTxt" class="form-control" />
					</div>
				</div>
				<%--<div class="form-group col-lg-4 col-md-4 col-sm-4">
					<label class="control-label col-lg-5 col-md-5 col-sm-5" for="gradeTxt"> grade </label>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<input type="text" id="gradeTxt" class="form-control" />
					</div>
				</div>--%>
			</form>
		</div>
		<!--Modal body-->
		<div class="modal-footer">
			<a id="calCpkDialog_cancelBtn" class="btn" data-dismiss="modal">
				取消 </a>
		</div>
		<!--Modal footer-->
	  </div>
		</div>
	</div>

	<!-- CHART 过滤条件  -->
	<div id="filterChartDialog" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="width:1000px">
		 <div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<center>
					<h3 class="modal-title">查询条件</h3>
				</center>
			</div>
		<!--Modal header-->
			<div class="modal-body">
				<form id="filterChartDialogForm" class="form-horizontal">

					<div class="row">
						<div class="form-group col-lg-4 col-md-4 col-sm-4">
							<label class="control-label col-lg-4 col-md-4 col-sm-4" for="filterChartDialog_cusIdSel">
								客户: </label>
							<div class="col-lg-8 col-md-8 col-sm-8">
								<select id="filterChartDialog_cusIdSel" class="addWith form-control" style="width: 100%;"></select>
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-4 col-sm-4">
							<label class="control-label col-lg-5 col-md-5 col-sm-5" for="filterChartDialog_mdlIdSel">
								产品名称: </label>
							<div class="col-lg-7 col-md-7 col-sm-7">
								<select  id="filterChartDialog_mdlIdSel" class="form-control addWith" style="width: 100%;"></select>
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-4 col-sm-4">
							<label class="control-label col-lg-5 col-md-5 col-sm-5" for="filterChartDialog_woIdSel">
								工单: </label>
							<div class="col-lg-7 col-md-7 col-sm-7">
								<select  id="filterChartDialog_woIdSel" class="form-control addWith" style="width: 100%;"></select>
							</div>
						</div>

						<div class="form-group col-lg-4 col-md-4 col-sm-4">
							<label class="control-label col-lg-4 col-md-4 col-sm-4" for="filterChartDialog_opeIdSel">
								站点: </label>
							<div class="col-lg-8 col-md-8 col-sm-8">
								<select  id="filterChartDialog_opeIdSel" class="form-control addWith" style="width: 100%;"></select>
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-4 col-sm-4">
							<label class="control-label col-lg-5 col-md-5 col-sm-5" for="filterChartDialog_toolIdSel">
								设备代码: </label>
							<div class="col-lg-7 col-md-7 col-sm-7">
								<select  id="filterChartDialog_toolIdSel" class="form-control addWith" style="width: 100%;"></select>
							</div>
						</div>
					</div>


					<%--<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="filterChartDialog_lotIdTxt">
							批次: </label>
						<div class="form-control col-lg-7">
							<select id="filterChartDialog_lotIdSel" class="col-lg-12 addWith"></select>
						</div>
					</div>--%>

					<div class="row">
						<div class="form-group col-lg-5 col-md-5 col-sm-5 time_style">
							<label class="control-label keyCss" for="filterChartDialog_reportBeginDatepicker">上报开始时间</label>
							<div style="width:200px">
								<div id="filterChartDialog_reportBeginDatepicker" class="input-group input-append">
									<input data-format="yyyy-MM-dd" type="text" class="form-control"> <span class="input-group-addon add-on"> <i
										data-date-icon="glyphicon-calendar"
										data-time-icon="glyphicon-time"
										class="glyphicon glyphicon-calendar"></i>
						        </span>
								</div>
							</div>
							<div style="width:200px">
								<div id="filterChartDialog_reportBeginTimepicker" class="input-group input-append">
									<input data-format="hh:mm:ss" type="text" class="form-control"> <span class="input-group-addon add-on"> <i
										data-date-icon="glyphicon-calendar"
										data-time-icon="glyphicon-time"
										class="glyphicon glyphicon-time"></i>
								</span>
								</div>
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-4 col-sm-4 " >
							<label class="control-label keyCss" for="filterChartDialog_reportEndDatepicker">上报结束时间</label>
							<div style="width:200px">
								<div id="filterChartDialog_reportEndDatepicker" class="input-group input-append">
									<input data-format="yyyy-MM-dd" type="text" class="form-control"> <span class="input-group-addon add-on"> <i
										data-date-icon="glyphicon-calendar"
										data-time-icon="glyphicon-time"
										class="glyphicon glyphicon-calendar"></i>
						        </span>
								</div>
							</div>
							<div style="width:200px">
								<div id="filterChartDialog_reportEndTimepicker" class="input-group input-append">
									<input data-format="hh:mm:ss" type="text" class="form-control"> <span class="input-group-addon add-on"> <i
										data-date-icon="glyphicon-calendar"
										data-time-icon="glyphicon-time"
										class="glyphicon glyphicon-time"></i>
								</span>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
		<!--Modal body-->
		<div class="modal-footer">
			<a id="filterChartDialog_cancelBtn" class="btn" data-dismiss="modal">
				取消 </a> <a id="filterChartDialog_queryBtn" class="btn btn-primary">
				查询 </a>
		</div>
		<!--Modal footer-->
	  </div>
	</div>


	<div id="filterChartDialog" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

		<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<center>
				<h3>
					<span class="modal-title">查询条件</span>
				</h3>
			</center>

		</div>

		<!--Modal header-->

		<div class="modal-body">

			<div class="row">



				<form id="filterChartDialog_form" class="row form-horizontal">

					<div class="form-group ">

						<label class="control-label col-lg-5" for="filterChartDialog_cusId">
							管制图组 </label>

						<div class="col-lg-5">

							<!-- <input type="text" id="queryDialog_grpNameTxt"  class="span2" /> -->
							<select id="queryDialog_grpNameSel" class="form-control" style="width: 100%;"></select>

							<!-- <span id="queryDialog_helpLineSpn" class="help-inline">

		                	模糊查询请使用<span style="color:red;font-weight : bold;">*</span>

		                </span> -->


						</div>

					</div>

				</form>
				<!-- <div id="chartListDiv" class="span5">
					<table id="chartListGrd"></table>
					<div id="chartListPg"></div>
				</div>
				style="width: 1300px; height: 500px;" 
				-->
				<div id="chartListDiv" class="col-lg-10" style="height: 450px;">
					<table id="chartListGrd"></table>
				</div>
				
				

			</div>

		</div>

		<!--Modal body-->

		<div class="modal-footer">

			<a id="queryDialog_cancelBtn" class="btn" data-dismiss="modal">

				取消 </a> <a id="queryDialog_queryBtn" class="btn btn-primary"> 查询 </a>

		</div>

	</div>

	<!-- 直方图 模态框-->
	<div id="histogramDialog" class="modal fade"
		 data-target=".bs-example-modal-lg" tabindex="-1" role="dialog"
		 aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog" style="width: 1200px">
			<div class="modal-content">
				<!-- 模框头部 -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">×</button>
					<h4 class="modal-title">直方图</h4>
				</div>
				<!-- 模框主体 -->
				<div class="modal-body">
					<div class="container-fluid">
						<div id="svgdiv">
							<%--<table id="cusOrderGrd"></table>
							<div id="cusOrderPg"></div>--%>
						</div>
					</div>
				</div>
				<!-- 模框底部 -->
				<div class="modal-footer">
					<form id="orderList" class="form-horizontal">
					<div class="form-group col-lg-12 col-md-12 col-sm-12">
						<label class="control-label col-lg-10 col-md-10 col-sm-10"
							   for="groupNum">组数</label>
						<div class="col-lg-2 col-md-2 col-sm-2">
						<input type="text" class="form-control" id="groupNum" class ="numberCssA">
						</div>
					</div>
					</form>
					<button type="button" class="btn btn-default"
							id="reBuildBtn">
						重新生成
					</button>

					<button type="button" class="btn btn-default" data-dismiss="modal"
							id="histogramDialog_sureBtn">
						确定
					</button>

				</div>
			</div>
		</div>
	</div>


</body>
	<%@ include file="/page/comPage/comJS.html"%>
	<script type="text/javascript" src="js/spc/chart.js"></script>
	<script type="text/javascript" src="js/spc/spc.js"></script>
	<script type="text/javascript" src="js/spc/d3.js"></script>
	<script type="text/javascript" src="js/page/7000/7300.js"></script>
	<script type="text/javascript" src="js/com/comDefine.js"></script>
	<script type="text/javascript" src="js/com/SelectDom.js"></script>
	<script
		src="lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script
		src="lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
</html>
						