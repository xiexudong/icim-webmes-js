<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_7100_SPC_CHART_SETTING_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<style>
#bodyDiv {
	margin: 30px 0 0 0;
}
.modal-height{
    height:550px;
}
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
#chartListDiv{
    width:950px;
    height:450px;
}
fieldset{
    height:350px;
}
div .form-width{

	padding-left:0px;
	padding-right:0px;
}

#sidebar ul.navigation li {
	width: 2gro00px;
	height: 40px;
	position: relative;
	/*line-height:40px;*/
	background-image: url('img/menu-separator.jpg');
	backund-repeat: no-repeat;
	background-position: 0 35px;
}

#groupListDiv {
	position: absolute;
	height: 520px;
}

#groupConditionForm .form-group {
	margin-bottom: 3px;
}

#subGroupEditDialogForm .form-group {
	margin-bottom: 3px;
}

#chartEditDialogForm .form-group {
	margin-bottom: 3px;
}

#chartEditDialog {
	margin-left: 10%;
	left: 10%;
}

.displayNoneCss {
	display: none;
}

.borderCss {
	border-style: solid;
	border-width: 1px;
}
.modal-width{
    width:800px;
}
.hiddenCss { /*overflow:hidden;	*/
	visibility: hidden;
}

input {
	position: relative;
	z-index: 1;
}

select {
	position: relative;
	z-index: 1;
}
</style>
<body>

<div class="container-fluid">
		<div class="col-lg-12">
			<button class="btn btn-primary" id="f1_query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="f4_del_btn">

				<s:text name="F4_DELETE_TAG" />

			</button>

			<button class="btn btn-primary" id="f5_upd_btn">

				<s:text name="F5_UPDATE_TAG" />

			</button>

			<button class="btn btn-primary" id="f6_add_btn">

				<s:text name="F6_ADD_TAG" />

			</button>

			<button class="btn btn-primary" id="f8_btn">
				<s:text name="F8_REGIST_TAG" />
			</button>

		</div>


		<div class="col-lg-12">

			<div class="container-fluid">

				<div class="row-fluid">

					<div class="col-lg-12">

						<h3>

							<s:text name="GROUP_LIST_TAG" />

						</h3>

					</div>

				</div>

			</div>

			<div id="groupListDiv" class="row col-lg-3">
				<table id="groupListGrd"></table>
				<div id="groupListPg"></div>
			</div>

			<div id="tabDiv" class="col-lg-9 col-lg-offset-3 ">
				<ul id="mytab" class="nav nav-tabs">
					<li class="active"><a id="tab1" href="#tabPane_subGroupList"
						data-toggle="tab"> <s:text name="SPC_GROUP_DETAIL_INFO_TAG" />
					</a>
					</li>
					<li><a id="tab2" href="#tabPane_chartList" data-toggle="tab">
							<s:text name="CHART_LIST_TAG" /> </a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tabPane_subGroupList">

						<div id="subGroupListDiv" class="row col-lg-8">
							<button id="addSubGroupBtn" class="btn btn-primary">
								<s:text name="ADD_TAG" />
							</button>
							<button id="updateSubGroupBtn" class="btn btn-primary col-lg-offset-1">
								<s:text name="UPDATE_TAG" />
							</button>
							<button id="deleteSubGroupBtn" class="btn btn-primary col-lg-offset-1">
								<s:text name="DELETE_TAG" />
							</button>

							<table id="subGroupListGrd"></table>
							<div id="subGroupListPg"></div>

						</div>

					</div>
					<div class="tab-pane" id="tabPane_chartList">

						<div id="chartListDiv" class="row col-lg-8">
							<button id="addChartBtn" class="btn btn-primary">
								<s:text name="ADD_TAG" />
							</button>
							<button id="updateChartBtn" class="btn btn-primary col-lg-offset-1">
								<s:text name="UPDATE_TAG" />
							</button>
							<button id="deleteChartBtn" class="btn btn-primary col-lg-offset-1">
								<s:text name="DELETE_TAG" />
							</button>
							<button id="setChartRuleBtn" class="btn btn-primary col-lg-offset-1">
								设置管制图规则</button>

							<table id="chartListGrd"></table>
							<div id="chartListPg"></div>
						</div>

					</div>
				</div>

			</div>
			<!-- 查询对话框 -->
			<div class="modal fade" id="queryGroupDialog" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">

							<s:text name="QUERY_CONDITION_TAG" />

						</h4>

					</center>

				</div>

				<!--Modal header-->

				<div class="modal-body">

					<div class="row">
						<form id="queryGroupDialog_GroupForm" class="row form-horizontal">

							<div class="form-group ">
								<label class="control-label col-lg-5" for="queryGroupDialog_grpNameTxt">
									<s:text name="SPC_GROUP_NAME_TAG" /> </label>
								<div class="col-lg-5">
									<input id="queryGroupDialog_groupNameTxt" class="form-control" /> <span
										class="help-inline"> 模糊查询请使用<span
										style="color:red;font-weight : bold;">*</span> </span>
								</div>
							</div>

						</form>

					</div>

				</div>

				<!--Modal body-->

				<div class="modal-footer">

					<a id="queryGroupDialog_cancelBtn" class="btn" data-dismiss="modal">

						<s:text name="CANCEL_TAG" /> </a> <a id="queryGroupDialog_queryBtn"
						class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" /> </a>

				</div>
				</div>
				</div>

			</div>

			<!-- Main GROUP 维护对话框 -->
			<div class="modal fade" id="mainGroupEditDialog" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">
							<!-- <s:text name="BOM_EDIT_DIALOG_TAG"/> -->
							管制图过滤条件
						</h4>
					</center>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<!-- <div class="row"> -->
					<form id="mainGroupEditDialogForm" class="row form-horizontal">
						<div class="form-group">
							<label class="control-label col-lg-5" for="colTypSel"> <s:text
									name="SPC_GROUP_CATE_TAG" /> </label>
							<div class="col-lg-5">
								<select id="colTypSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="grpNameTxt"> <s:text
									name="SPC_GROUP_NAME_TAG" /> </label>
							<div class="col-lg-5">
								<input type="text" id="grpNameTxt" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="grpDscTxt"> <s:text
									name="SPC_GROUP_DSC_TAG" /> </label>
							<div class="col-lg-5">
								<input type="text" id="grpDscTxt" class="form-control" />
							</div>
						</div>
					</form>
					<!-- </div> -->
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="mainGroupEditDialog_cancelBtn" class="btn"
						data-dismiss="modal"> <s:text name="CANCEL_TAG" /> </a> <a
						id="mainGroupEditDialog_saveBtn" class="btn btn-primary"> <s:text
							name="F5_UPATE_COMMIT_TAG" /> </a>
				</div>
				<!--Modal footer-->
			</div>
           </div>
           </div>

			<!-- SUB GROUP 维护对话框 -->
			<div class="modal fade" id="subGroupEditDialog" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">
							<s:text name="BOM_EDIT_DIALOG_TAG" />
						</h4>
					</center>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<!-- <div class="row"> -->
					<form id="subGroupEditDialogForm" class="row form-horizontal">

						<div class="form-group displayNoneCss">
							<label class="control-label col-lg-5 " for="subGroupEdit_colTypTxt">
								<s:text name="TOOL_LIST_TAG" /> </label>
							<div class="col-lg-5">
								<input type="text" id="subGroupEdit_colTypTxt"  class="form-control"/>
							</div>
						</div>
						<div class="form-group displayNoneCss">
							<label class="control-label col-lg-5" for="subGroupEdit_grpNoTxt">
								<s:text name="TOOL_LIST_TAG" /> </label>
							<div class="col-lg-5">
								<input type="text" id="subGroupEdit_grpNoTxt"  class="form-control"/>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-lg-5" for="cusIDSel"> 客户名称 </label>
							<div class="col-lg-5">
								<select id="cusIDSel"  class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="mdlIDSel"> <s:text
									name="MDL_ID_TAG" /> </label>
							<div class="col-lg-5">
								<select id="mdlIDSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="woIDSel"> 内部订单 </label>
							<div class="col-lg-5">
								<select id="woIDSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="pathIDSel"> <s:text
									name="ROUTE_TAG" /> </label>
							<div class="col-lg-5">
								<select id="pathIDSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="opeIDSel"> <s:text
									name="OPE_ID_TAG" /> </label>
							<div class="col-lg-5">
								<select id="opeIDSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="toolIDSel"> 设备代码 </label>
							<div class="col-lg-5">
								<select id="toolIDSel" class="form-control"></select>
							</div>
						</div>
					</form>
					<!-- </div> -->
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="subGroupEditDialog_cancelBtn" class="btn"
						data-dismiss="modal"> <s:text name="CANCEL_TAG" /> </a> <a
						id="subGroupEditDialog_saveBtn" class="btn btn-primary"> <s:text
							name="F5_UPATE_COMMIT_TAG" /> </a>
				</div>
				<!--Modal footer-->
			</div>
</div>
</div>
			<!-- CHART维护对话框 -->
			<div class="modal fade" id="chartEditDialog" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-width">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">
							<s:text name="CHART_EDIT_DIALOG_TAG" />
						</h4>
					</center>
				</div>
				<!--Modal header-->
				<div class="modal-body modal-height">
					<!-- <div class="row"> -->
					<form id="chartEditDialogForm" class="row form-horizontal">

						<div class="form-group displayNoneCss">
							<label class="control-label " for="chartEditDialog_colTypTxt">
								<s:text name="COL_TYP_TAG" /> </label>
								<input type="text" id="chartEditDialog_colTypTxt" class="form-control" />
						</div>
						<div class="form-group displayNoneCss">
							<label class="control-label " for="chartEditDialog_grpNoTxt">
								<s:text name="GROUP_NO_TAG" /> </label>
								<input type="text" id="chartEditDialog_grpNoTxt" class="form-control"/>
						</div>
						<div class="form-group displayNoneCss">
							<label class="control-label " for="chartEditDialog_chartNoTxt">
								<s:text name="CHART_NO_TAG" /> </label>
								<input type="text" id="chartEditDialog_chartNoTxt" class="form-control"/>
						</div>
						<div class="form-group col-lg-6 form-width">
							<label class="control-label col-lg-4" for="dataGroupSel"> <s:text
									name="MEASUREMENT_NAME_TAG" /> </label>
							<div class="col-lg-6">
								<select id="dataGroupSel" class="form-control"></select>
							</div>
						</div>

						<div class="form-group col-lg-6 form-width">
							<label class="control-label col-lg-5 " for="chartNameTxt"> <s:text
									name="CHART_NAME_TAG" /> </label>
							<div class="col-lg-7">
								<input type="text" id="chartNameTxt" class="form-control" />
							</div>
						</div>

						<div class="form-group col-lg-12 form-width ">
							<label class="control-label col-lg-2" for="chartDscTxt"> <s:text
									name="CHART_DSC_TAG" /> </label>
							<div class="col-lg-6">
								<input type="text" id="chartDscTxt" class="form-control"/>
							</div>
						</div>

						<div class="form-group col-lg-4 form-width">
							<label class="control-label col-lg-6" for="chartOwnerTxt"> <s:text
									name="CHART_OWNER_TAG" /> </label>
							<div class="col-lg-6">
								<input type="text" id="chartOwnerTxt" class="form-control" />
							</div>
						</div>
						<div class="form-group col-lg-4 form-width">
							<label class="control-label col-lg-6" for="sampleSizeTxt"> <s:text
									name="SAMPLE_SIZE_TAG" /> </label>
							<div class="col-lg-6">
								<input type="text" id="sampleSizeTxt" class="form-control intCss" />
							</div>
						</div>
						<div class="form-group col-lg-4 form-width">
							<label class="control-label col-lg-6" for="maxChartPointTxt"> <s:text
									name="MAX_CHART_POINT_TAG" /> </label>
							<div class="col-lg-6">
								<input type="text" id="maxChartPointTxt" class="form-control intCss" />
							</div>
						</div>

								
						<div class="form-group col-lg-8 form-width">
							<label class="control-label col-lg-3" for="inlineCheckboxes"> 选择项
							</label>
							<div class="col-lg-8">

								<label class="checkbox  col-lg-6"> <input type="checkbox"
									id="singleFLgChk" value="option1"> <s:text
										name="SINGELE_FLG_TAG" /> </label> <label class="checkbox  col-lg-6">
									<input type="checkbox" id="keepRawChk" value="option1">
									<s:text name="KEEP_RAW_TAG" /> </label>
							</div>
						</div>
						
						<div class="form-group col-lg-4 form-width">
							<label class="control-label col-lg-5" for="thicknessTypeSel">厚度类型</label>
							<div class="col-lg-6">
								<select id="thicknessTypeSel" class="form-control">
	    								<option value="O"></option>
	    								<option value="S">来料厚度</option>
	    								<option value="T">目标厚度</option>
								</select>
							</div>
						</div>
						
						<div class="form-group col-lg-4 form-width">
							<label class="control-label col-lg-6" for="chartTypSel"> <s:text
									name="CHART_TYP_TAG" /> </label>
							<div class="col-lg-6">
								<select id="chartTypSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group col-lg-8">
							<label class="control-label col-lg-4" for="inlineCheckboxes">
								子chart信息: </label>
							<div class="col-lg-8">

								<label class="checkbox col-lg-4"> <input type="checkbox"
									id="type1Chk" value="option1"> <span id="type1Spn"></span>
								</label> <label class="checkbox col-lg-4"> <input type="checkbox"
									id="type2Chk" value="option1"> <span id="type2Spn"></span>
								</label> <label class="checkbox col-lg-4"> <input type="checkbox"
									id="type3Chk" value="option1"> <span id="type3Spn"></span>
								</label>
							</div>
						</div>
						<div id="chartInfoDIv" class="col-lg-12">
							<fieldset class="borderCss">
								<div id="div1" class="col-lg-4 form-width">
									<div class="form-group col-lg-12">
										<label id="chart1Lbel" class="control-label col-lg-6"
											for="inlineCheckboxes"> <b></b> </label>
										<div class="col-lg-6">
											<label class="checkbox inline"> <!-- <input type="checkbox" id="div1_enable_chk" value="option1">		 -->
												<!-- <s:text name="SINGELE_FLG_TAG"/>		 --> </label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="inlineCheckboxes"> <s:text
												name="CHECK_SPEC_TAG" /> </label>
										<div class="col-lg-6">
											<label class="checkbox inline"> <input
												type="checkbox" id="div1_checkSpecFlgChk" value="option1">
												<!-- <s:text name="SINGELE_FLG_TAG"/>		 --> </label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6 " for="div1_uplTxt"> <s:text
												name="UPL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div1_uplTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div1_uslTxt"> <s:text
												name="USL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div1_uslTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div1_uclTxt"> <s:text
												name="UCL_TAG" /> </label>
										<div class="controls col-lg-6">
											<input type="text" id="div1_uclTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6 " for="div1_targetTxt"> <s:text
												name="TARGET_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div1_targetTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div1_lclTxt"> <s:text
												name="LCL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div1_lclTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div1_lslTxt"> <s:text
												name="LSL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div1_lslTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div1_lplTxt"> <s:text
												name="LPL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div1_lplTxt" class="form-control numberCss" />
										</div>
									</div>

								</div>
								<div id="div2" class="col-lg-4 form-width hiddenCss">
									<div class="form-group col-lg-12">
										<label id="chart2Lbel" class="control-label col-lg-6"
											for="inlineCheckboxes"> <b></b> </label>
										<div class="col-lg-6">
											<label class="checkbox inline"> <!-- <input type="checkbox" id="div2_enable_chk" value="option1">		 -->
												<!-- <s:text name="SINGELE_FLG_TAG"/>		 --> </label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="inlineCheckboxes"> <s:text
												name="CHECK_SPEC_TAG" /> </label>
										<div class="col-lg-6">
											<label class="checkbox inline"> <input
												type="checkbox" id="div2_checkSpecFlgChk" value="option1">
												<!-- <s:text name="SINGELE_FLG_TAG"/>		 --> </label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div2_uplTxt"> <s:text
												name="UPL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div2_uplTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div2_uslTxt"> <s:text
												name="USL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div2_uslTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div2_uclTxt"> <s:text
												name="UCL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div2_uclTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6 " for="div2_targetTxt"> <s:text
												name="TARGET_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div2_targetTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div2_lclTxt"> <s:text
												name="LCL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div2_lclTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div2_lslTxt"> <s:text
												name="LSL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div2_lslTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div2_lplTxt"> <s:text
												name="LPL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div2_lplTxt" class="form-control numberCss" />
										</div>
									</div>
								</div>
								<div id="div3" class="col-lg-4 form-width hiddenCss">
									<div class="form-group col-lg-12">
										<label id="chart3Lbel" class="control-label col-lg-6"
											for="inlineCheckboxes"> <b></b> </label>
										<div class="col-lg-6">
											<label class="checkbox inline"> <!-- <input type="checkbox" id="div3_enable_chk" value="option1">		 -->
												<!-- <s:text name="SINGELE_FLG_TAG"/>		 --> </label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="inlineCheckboxes"> <s:text
												name="CHECK_SPEC_TAG" /> </label>
										<div class="col-lg-6">
											<label class="checkbox inline"> <input
												type="checkbox" id="div3_checkSpecFlgChk" value="option1">
												<!-- <s:text name="SINGELE_FLG_TAG"/>		 --> </label>
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div3_uplTxt"> <s:text
												name="UPL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div3_uplTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div3_uslTxt"> <s:text
												name="USL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div3_uslTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div3_uclTxt"> <s:text
												name="UCL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div3_uclTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div3_targetTxt"> <s:text
												name="TARGET_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div3_targetTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div3_lclTxt"> <s:text
												name="LCL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div3_lclTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div3_lslTxt"> <s:text
												name="LSL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div3_lslTxt" class="form-control numberCss" />
										</div>
									</div>
									<div class="form-group col-lg-12">
										<label class="control-label col-lg-6" for="div3_lplTxt"> <s:text
												name="LPL_TAG" /> </label>
										<div class="col-lg-6">
											<input type="text" id="div3_lplTxt" class="form-control numberCss" />
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<!-- test collse-->

					</form>
					<!-- </div> -->
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="chartEditDialog_cancelBtn" class="btn" data-dismiss="modal">
						<s:text name="CANCEL_TAG" /> </a> <a id="chartEditDialog_saveBtn"
						class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
           </div>
           </div>

			<!-- 管制图规则设置对话框 -->
		<div class="modal fade" id="chartRuleDialog" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">管制图规则设定</h4>
					</center>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<form id="chartRuleDialogForm" class="col-lg-6">
							<label class="checkbox"> <input type="checkbox" id="rule1Chk" value="">一点跳出管界限外 </label>
							<label class="checkbox" ><input type="checkbox" id="rule2Chk" value=""> 连续八点落在中心线同一侧         </label>
							<label class="checkbox" ><input type="checkbox" id="rule3Chk" value=""> 连续六点上升或下降 </label>
							<label class="checkbox" ><input type="checkbox" id="rule4Chk" value=""> 连续十四点交互着上下跳动         </label>
							<label class="checkbox" ><input type="checkbox" id="rule5Chk" value=""> 连续三点中有两点落在2σ以外      </label>
							<label class="checkbox" ><input type="checkbox" id="rule6Chk" value=""> 连续五点钟有四点落在1σ以外       </label>
							<label class="checkbox" ><input type="checkbox" id="rule7Chk" value=""> 连续十五点在中心线上下两侧1σ以内</label>
					</form>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="chartRuelDialog_cancelBtn" class="btn"
						data-dismiss="modal"> <s:text name="CANCEL_TAG" /> </a> <a
						id="chartRuleDialog_saveBtn" class="btn btn-primary"> <s:text
							name="F5_UPATE_COMMIT_TAG" /> </a>
				</div>
				</div>
				</div>
				<!--Modal footer-->
			</div>

		</div>
	</div>
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/com/comSpc.js"></script>
	<script src="js/page/7000/7100.js"></script>
	<script src="js/com/Regex.js"></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
</body>
</html>