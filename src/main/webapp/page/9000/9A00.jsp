<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9A00_QUERY_DEFECT_TITLE_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#defectInfoListDiv {
	margin-top: 10px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="f2_export_btn">
			<s:text name="EXPORT_FILE_TAG" />
		</button>
	</div>

	<div class="container">
		<div class="control-group">
			<div class="controls col-xs-12"></div>
		</div>
		<div class="col-md-12">
			<form id="queryForm" class="form-horizontal">

				<div class="control-group col-xs-4">
					<label class="control-label col-xs-5 keyCss" for="cusIdInput">客户代码</label>
					<div class="col-xs-7">
						<input id="cusIdInput" class="form-control input-sm" type="text">
					</div>
					<label class="control-label col-xs-5" for="woIdInput">内部订单号</label>
					<div class="col-xs-7">
						<input id="woIdInput" class="form-control input-sm" type="text">
					</div>
					<label class="control-label col-xs-5" for="beginTimeTxt">生产日期(开始)</label>
					<div class="col-xs-7">
						<input id="beginTimeTxt" class="form-control input-sm" type="text">
					</div>
					<label class="control-label col-xs-5" for="endTimeTxt">生产日期(结束)</label>
					<div class="col-xs-7">
						<input id="endTimeTxt" class="form-control input-sm" type="text">
					</div>
				</div>
				<div class="control-group col-xs-8">
					<label class="control-label col-xs-1" for="inlineCheckboxes">&nbsp;</label>
					<div class="col-xs-10">
						<label class="checkbox-inline"> <input type="checkbox" id="gradeGKChk" value="option1"> 管控品
						</label> <label class="checkbox-inline"> <input type="checkbox" id="gradeLZChk" value="option2"> 滞留品
						</label> <label class="checkbox-inline"> <input type="checkbox" id="gradeNGChk" value="option5"> NG品
						</label> <label class="checkbox-inline"> <input type="checkbox" id="gradeSCChk" value="option6"> SC品
						</label> <label class="checkbox-inline"> <input type="checkbox" id="dmProcChk" value="option3"> 镀膜
						</label> <label class="checkbox-inline"> <input type="checkbox" id="jbProcChk" value="option4"> 减薄
						</label>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="defectInfoListDiv" class="container">
		<table id="defectInfoListGrd"></table>
		<div id="defectInfoListPg"></div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="js/com/TimestampUtil.js"></script>
<script src="js/page/9000/9A00.js"></script>
</html>
