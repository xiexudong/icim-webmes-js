<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9600_QUERY_SO_INFO_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#inputDiv {
	margin: 20px 0 0 -15px;
}

#tabDiv {
	width: 98%;
	margin-top: 10px;
}

#tabContentDiv {
	width: 100%;
}

#button-group {
	margin-left: 15px;
}
#tabContentDiv{
	margin-top: 10px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="export_wo_btn">
			<s:text name="EXPORT_FILE_TAG" />
			订单信息
		</button>
		<button class="btn btn-primary" id="export_box_btn">
			<s:text name="EXPORT_FILE_TAG" />
			箱号信息
		</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid" id="inputDiv">
		<div class="control-group">
			<div class="col-xs-12"></div>
		</div>
		<div class="row" id="input-panel">
			<form id="queryForm" class="form-horizontal">
				<div class="control-group col-sm-3">
					<label class="control-label col-xs-4" for="cusIDTxt"> <s:text name="CUS_ID_TAG" />
					</label>
					<div class="col-xs-8">
						<input id="cusIDTxt" class="form-control input-sm" type="text">
					</div>
				</div>
				<div class="control-group col-sm-3">
					<label class="control-label col-xs-4" for="beginTimeTxt"> <s:text name="BEGIN_TIME_TAG" />
					</label>
					<div class="col-xs-8">
						<input id="beginTimeTxt" class="form-control input-sm" type="text">
					</div>
				</div>
				<div class="control-group col-sm-3">
					<label class="control-label col-xs-4" for="endTimeTxt"> <s:text name="END_TIME_TAG" />
					</label>
					<div class="col-xs-8">
						<input id="endTimeTxt" class="form-control input-sm" type="text">
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="tabDiv" class="container-fluid">
		<ul id="mytab" class="nav nav-tabs">
			<li class="active"><a id="tab1" href="#tabPane_wo" data-toggle="tab"> <s:text name="WO_INFO_TAG" />
			</a></li>
			<li><a id="tab2" href="#tabPane_box" data-toggle="tab"> <s:text name="BOX_INFO_TAG" />
			</a></li>
		</ul>
		<div class="tab-content" id="tabContentDiv">
			<div class="tab-pane active" id="tabPane_wo">
				<table id="woInfoListGrd"></table>
				<div id="woInfoListPg"></div>
			</div>
			<div class="tab-pane" id="tabPane_box">
				<table id="boxInfoListGrd"></table>
				<div id="boxInfoListPg"></div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/com/TimestampUtil.js"></script>
<script src="js/page/9000/9600.js"></script>
</html>
