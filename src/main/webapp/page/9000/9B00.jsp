<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9B00_QUERY_DN_INFO_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
#inputDiv{
	margin : 20px 0 0 -15px;
}
#tabDiv{
	width:96%;
	margin-top: 10px;
}
#tabContentDiv{
	width:100%;
	margin-top: 10px;
}
#button-group {
	margin-left: 25px;
}

</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="export_btn">
			<s:text name="EXPORT_FILE_TAG" />
		</button>
	</div>

	<!-- 输入区--开始 -->

     <div class="container-fluid">
         <div class="col-sm-4">
           <form class="form-horizontal">
               <label class="control-label col-xs-4" for="beginTimeTxt1"><s:text name="制定开始日期"/></label>
               <div class="col-xs-8">
                 <input class="form-control input-sm" type="text" id="beginTimeTxt1"/>
               </div>
               <label class="control-label col-xs-4" for="endTimeTxt1"><s:text name="制定结束日期"/></label>
               <div class="col-xs-8">
                 <input class="form-control input-sm" type="text" id="endTimeTxt1"/>
               </div>
            </form>
         </div>
         <div class="col-sm-4"> 
           <div class="form-horizontal" >
               <label class="control-label col-xs-4" for="beginTimeTxt2"><s:text name="交货开始日期"/></label>
               <div class="col-xs-8">
                 <input class="form-control input-sm" type="text" id="beginTimeTxt2"/>
                </div>
               <label class="control-label col-xs-4" for="endTimeTxt2"><s:text name="交货结束日期"/></label>
               <div class="col-xs-8">
                <input class="form-control input-sm" type="text" id="endTimeTxt2"/>
               </div>
            </div>
         </div>
          <div class="col-sm-4"> 
           <div class="form-horizontal" >
               <label class="control-label col-xs-4" for="dnIDTxt"><s:text name="DELIVERY_ORDER_ID_TAG"/></label>
               <div class="col-xs-8">
                 <input id="dnIDTxt" class="form-control input-sm" type="text">
                </div>
           </div>
         </div>
     </div>

	<div id="tabDiv" class="container-fluid">
		<ul id="mytab" class="nav nav-tabs">
			<li class="active">
				<a id="tab1" href="#tabPane_wo" data-toggle="tab">
					查询信息
				</a>
			</li>
		</ul>
		<div class="tab-content" id="tabContentDiv">
			<div class="tab-pane active" id="tabPane_wo">
				<table id="woInfoListGrd"></table>
				<div id="woInfoListPg"></div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/com/TimestampUtil.js"></script>
<script src="js/page/9000/9B00.js"></script>
</html> 	