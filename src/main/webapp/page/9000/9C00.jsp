<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9C00_QUERY_OPE_INFO_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#inputDiv {
	margin: 20px 0 0 -15px;
}

#tabDiv {
	width: 96%;
	margin-top: 10px;
}

#tabContentDiv {
	width: 100%;
	margin-top: 10px;
}

#button-group {
	margin-left: 25px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="export_ope_btn">
			<s:text name="EXPORT_FILE_TAG" />
			站点信息
		</button>
		<button class="btn btn-primary" id="export_detail_btn">
			<s:text name="EXPORT_FILE_TAG" />
			详细信息
		</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid" id="inputDiv">
		<div class="row container-fluid">
			<div class="control-group">
				<div class="controls"></div>
			</div>
			<div class="row">
				<form id="queryForm" class="form-horizontal">
					<div class="control-group col-xs-4">
						<label class="control-label col-xs-4" for="opeIDSel"> <s:text name="OPE_NO_TAG" />
						</label>
						<div class="col-xs-8">
							<select id="opeIDSel" class="form-control input-sm"></select>
						</div>
					</div>
					<div class="control-group col-xs-4">
						<label class="control-label col-xs-4" for="woIdSelect"> <s:text name="WORDER_ID_TAG" />
						</label>
						<div class="col-xs-8">
							<input type="text" id="woIdSelect" class="form-control input-sm"></input>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="tabDiv" class="container-fluid">
		<ul id="mytab" class="nav nav-tabs">
			<li class="active"><a id="tab1" href="#tabPane_ope" data-toggle="tab"> <s:text name="OPE_INFO_TAG" />
			</a></li>
			<li><a id="tab2" href="#tabPane_detail" data-toggle="tab"> <s:text name="DETAIL_INFO_TAG" />
			</a></li>
		</ul>
		<div class="tab-content" id="tabContentDiv">
			<div class="tab-pane active" id="tabPane_ope">
				<table id="opeInfoListGrd"></table>
				<div id="opeInfoListPg"></div>
			</div>
			<div class="tab-pane" id="tabPane_detail">
				<table id="detailInfoListGrd"></table>
				<div id="detailInfoListPg"></div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/com/TimestampUtil.js"></script>
<script src="js/page/9000/9C00.js"></script>
</html>
