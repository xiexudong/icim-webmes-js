<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9E00_QUERY_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
.row-inline{
	padding-top: 10px;
}
#button-group,#tabDiv{
	margin-left: 25px;
}
#tabDiv{
	width: 96%;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="f2_clear_btn">清空</button>
		<button class="btn btn-primary" id="f3_export_btn">导出</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid" id="inputDiv">
		<div class="control-group">
			<div class="controls col-xs-12"></div>
		</div>
		<div class="container-fluid">
			<form id="queryForm" class="form-horizontal">
				<div class="row row-inline">
					<div class="control-group col-md-4">
						<label class="control-label col-xs-5" for="cusIdSel">客户ID</label>
						<div class="col-xs-7">
							<select id="cusIdSel" class="form-control input-sm"></select>
						</div>
					</div>
					<div class="control-group col-md-4">
						<label class="control-label col-xs-5" for="woIdSel">内部订单号</label>
						<div class="col-xs-7">
							<select id="woIdSel" class="form-control input-sm"></select>
						</div>
					</div>
					<div class="control-group col-md-4">
						<label class="control-label col-xs-5" for="lotIdTxt">批次号</label>
						<div class="col-xs-7">
							<input id="lotIdTxt" type="text" class="form-control input-sm" />
						</div>
					</div>
				</div>
				<div class="row row-inline">
					<div class="control-group col-md-4">
						<label class="control-label col-xs-5" for="boxIdTxt">箱号</label>
						<div class="col-xs-7">
							<input id="boxIdTxt" type="text" class="form-control input-sm" />
						</div>
					</div>
					<div class="control-group col-md-4">
						<label class="control-label col-xs-5" for="fromDateTxt">生产日期(开始)</label>
						<div class="col-xs-7">
							<input id="fromDateTxt" type="text" class="form-control input-sm" />
						</div>
					</div>
					<div class="control-group col-md-4">
						<label class="control-label col-xs-5" for="toDateTxt">生产日期(结束)</label>
						<div class="col-xs-7">
							<input id="toDateTxt" type="text" class="form-control input-sm" />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="tabDiv" class="container-fluid row-inline">
		<ul id="mytab" class="nav nav-tabs">
			<li class="active"><a id="tab1" href="#tabPane_holdBox" data-toggle="tab">保留箱信息 </a></li>
			<li><a id="tab2" href="#tabPane_exchangeBox" data-toggle="tab"> 交换箱信息 </a></li>
			<li><a id="tab3" href="#tabPane_scrpBox" data-toggle="tab"> 报废箱信息 </a></li>
			<li><a id="tab4" href="#tabPane_exWhBox" data-toggle="tab"> 转仓箱信息 </a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tabPane_holdBox">
				<div id="holdBoxDiv" class="span11">
					<table id="holdBoxGrd"></table>
					<div id="holdBoxPg"></div>
				</div>
			</div>
			<div class="tab-pane" id="tabPane_exchangeBox">
				<div id="exchangeBoxDiv" class="span11">
					<table id="exchangeBoxGrd"></table>
					<div id="exchangeBoxPg"></div>
				</div>
			</div>
			<div class="tab-pane" id="tabPane_scrpBox">
				<div id="scrpBoxDiv" class="span11">
					<table id="scrpBoxGrd"></table>
					<div id="scrpBoxPg"></div>
				</div>
			</div>
			<div class="tab-pane" id="tabPane_exWhBox">
				<div id="exWhBoxDiv" class="span11">
					<table id="exWhBoxGrd"></table>
					<div id="exWhBoxPg"></div>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/com/Translator.js"></script>
<script src="js/page/9000/9E00.js"></script>
</html>
