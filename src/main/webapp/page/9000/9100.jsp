<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9100_TITLE_TAG" />
</title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#pnlInfoListDiv {
	height: 100%;
}

.control-group {
	margin-top:10px;
}
.fullWidthCss{
  width : 99% ;
}
/*#pnlInfoListDiv{
      	position:absolute;
      	height:90%;	
    }*/
#boxInfoListDiv{
	width:97%;
}
#pnlInfoListDiv{
	width:97%;
}

#button-group{
	margin-left: 15px;
}
#grdDiv{
	margin-top:15px;
	width:100%;
}
#tabDiv{
	width:100%;
}
#tabDiv1{
	margin-top:15px;
	width:97%;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="f2ExportBoxInfo">
			导出箱号信息
		</button>
		<button class="btn btn-primary" id="f3ExportPrdInfo">
			导出玻璃信息
		</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid">
		<div class="container-fluid">
			<div class="control-group">
				<div class="col-sm-12"></div>
			</div>
			<div class="container-fluid">
				<form id="queryForm" class="form-horizontal">
					<div class="control-group col-md-4">
						<label class="col-sm-4 control-label" for="lotIDTxt"> <s:text
								name="WORDER_ID_TAG" /> </label>
						<div class="col-sm-6">
							<input id="woIDTxt" class="form-control input-sm" type="text">
						</div>
						<div class="col-sm-2">
						</div>
					</div>
					<div class="control-group col-md-4">
						<label class="col-sm-4 control-label" for="ppboxIDTxt"> 来料箱号</label>
						<div class="col-sm-6">
							<input id="ppboxIDTxt" class="form-control input-sm" type="text">
						</div>
						<div class="col-sm-2">
						</div>
					</div>
					<div class="control-group col-md-4">
						<label class="col-sm-4 control-label" for="boxIDTxt"> 厂内箱号 </label>
						<div class="col-sm-6">
							<input id="boxIDTxt" class="form-control input-sm" type="text">
						</div>
						<div class="col-sm-2">
						</div>
					</div>
					<div class="control-group col-md-4">
						<label class="col-sm-4 control-label" for="outboxIDTxt"> 出货箱号 </label>
						<div class="col-sm-6">
							<input id="outboxIDTxt" class="form-control input-sm" type="text">
						</div>
						<div class="col-sm-2">
						</div>
					</div>
					<div class="control-group col-md-4">
						<label class="col-sm-4 control-label" for="palletIDTxt"> 栈板号 </label>
						<div class="col-sm-6">
							<input id="palletIDTxt" class="form-control input-sm" type="text">
						</div>
						<div class="col-sm-2">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 输入区--结束 -->

	<!-- 列表区--开始 -->

	<!-- 列表区--结束 -->
	<div id="tabDiv">
		<div class="container" id="tabDiv1">
			<ul id="mytab" class="nav nav-tabs">
				<li class="active"><a id="tab1" href="#tabPane_boxInfo"
					data-toggle="tab"> <s:text name="BOX_INFO_TAG" /> </a>
				</li>
				<li><a id="tab2" href="#tabPane_pnlInfo" data-toggle="tab">
						<s:text name="PANEL_INFO_TAG" /> </a>
				</li>
				<li><a id="tab3" href="#tabPallet_pnlInfo" data-toggle="tab">
						<s:text name="栈板数量信息" /> </a>
				</li>
			</ul>
			<div class="tab-content" id="grdDiv">
				<div class="tab-pane active" id="tabPane_boxInfo">
					<div id="boxInfoListDiv" class="fullWidthCss">
						<table id="boxInfoListGrd"></table>
						<div id="boxInfoListPg"></div>
					</div>
				</div>
				<div class="tab-pane" id="tabPane_pnlInfo">
					<div id="pnlInfoListDiv" class="fullWidthCss">
						<table id="pnlInfoListGrd"></table>
						<div id="pnlInfoListPg"></div>
					</div>
				</div>
					<div class="tab-pane" id="tabPallet_pnlInfo">
					<div id="palletInfoListDiv" class="fullWidthCss">
						<table id="palletInfoListGrd"></table>
						<div id="palletInfoListPg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/page/9000/9100.js"></script>
</html>