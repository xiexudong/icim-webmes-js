<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9200_TITLE_TAG" />
</title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
	}
	#pnlInfoListDiv{
		height: 100%;
	}

    #boxInfoForm .control-group col-md-4{
	     margin-bottom: 3px;
	}
	#pnlHistoryDiv{
		width:97%;
	}
	/*#pnlInfoListDiv{
      	position:absolute;
      	height:90%;	
    }*/
    
#button-group{
	margin-left: 15px;
}
</style>

	<%@ include file="/page/comPage/comCSS.html"%> 
	<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		 <button class="btn btn-primary" id="f2_export_btn">
			<s:text name="EXPORT_FILE_TAG" />
		</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid">
		<div class="row">
			<div class="control-group col-md-12">
				<div class="col-sm-6"></div>
			</div>
			<div class="row ">
				<form id="queryForm" class="form-horizontal">
					<div class="control-group col-md-4">
						<label class="col-sm-4 control-label" for="prdSeqIDTxt"> <s:text name="PRD_SEQ_ID_TAG" /> </label>
						<div class="col-sm-6">
							<input id="prdSeqIDTxt" class="form-control input-sm" type="text">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 输入区--结束 -->

	<!-- 列表区--开始 -->
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="container-fluid">
				<h3>
					查询结果
				</h3>
			</div>
		</div>
	</div>
	<div id="pnlHistoryDiv" class ="container-fluid">
		<table id="pnlHistroyGrd"></table>
		<div id="pnlHistroyPg"></div>
	</div>
	<!-- 列表区--结束 -->
</body>
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/9000/9200.js"></script>
	<script type="text/javascript" src="js/dialog/showFabSnDetailInfoDialog.js"></script>
</html>