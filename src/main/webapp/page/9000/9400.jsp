<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9400_TITLE_TAG" />
</title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#pnlInfoListDiv {
	height: 100%;
}

#boxInfoForm .control-group {
	margin-bottom: 3px;
}
/*#pnlInfoListDiv{
      	position:absolute;
      	height:90%;	
    }*/
#tabDiv{
	width:97%;
}
#input-panl,#tabPaneDiv{
	width:100%;
	margin-top: 10px;
}
#button-group{
	margin-left: 15px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="f2ExportLotInfo">
			导出批次信息
		</button>
		<button class="btn btn-primary" id="f3ExportPrdInfo">
			导出玻璃信息
		</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid">
		<div class="control-group">
			<div class="controls"></div>
		</div>
		<div class="row" id="input-panl">
			<form id="queryForm" class="form-horizontal">
				<div class="control-group span6">
					<label class="col-sm-2 control-label" for="lotIDTxt"> <s:text name="LOT_ID_TAG" /> </label>
					<div class="col-sm-2">
						<input id="lotIDTxt" class="form-control input-sm" type="text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
					</div>
					<div class="col-sm-8">
						<input type="radio" name="partRadios" id="jbRadio" value="JB" checked>减薄 &nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="partRadios" id="dmRadio" value="DM">镀膜
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- 输入区--结束 -->

	<!-- 列表区--开始 -->

	<!-- 列表区--结束 -->
	<div id="tabDiv" class="container-fluid">
		<ul id="mytab" class="nav nav-tabs">
			<li class="active"><a id="tab1" href="#tabPane_lotInfo"
				data-toggle="tab"> <s:text name="LOT_INFO_TAG" /> </a>
			</li>
			<li><a id="tab2" href="#tabPane_pnlInfo" data-toggle="tab">
					<s:text name="PANEL_INFO_TAG" /> </a>
			</li>
		</ul>
		<div class="tab-content" id="tabPaneDiv">
			<div class="tab-pane active" id="tabPane_lotInfo">
				<table id="lotInfoListGrd"></table>
				<div id="lotInfoListPg"></div>
			</div>
			<div class="tab-pane" id="tabPane_pnlInfo">
				<table id="pnlInfoListGrd"></table>
				<div id="pnlInfoListPg"></div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/page/9000/9400.js"></script>
</html>