<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9300_TITLE_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#pnlInfoListDiv {
	height: 100%;
}

#boxInfoForm .control-group {
	margin-bottom: 3px;
}

/* #pnlWipInfoDiv{
		width:97%;
	} */
/*#pnlInfoListDiv{
      	position:absolute;
      	height:90%;	
    }*/
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<%-- <div class="container-fluid">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
	</div> --%>

	<!-- 输入区--开始 -->
	<%-- <div class="container-fluid">
		<div class="container-fluid">
			<div class="control-group">
				<div class="controls"></div>
			</div>
			<div class="container-fluid ">
				<form id="queryForm" class="form-horizontal">
					<div class="control-group span3">
						<label class="control-label" for="prdSeqIDTxt"> <s:text name="PRD_SEQ_ID_TAG" /> </label>
						<div class="controls">
							<input id="prdSeqIDTxt" class="span2" type="text">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div> --%>
	<!-- 输入区--结束 -->

	<!-- 列表区--开始 -->
	<div class="container-fluid">
		<div class="col-md-6">
			<div class="container-fluid">
				<button class="btn btn-primary" id="f1_query_btn">
					<s:text name="F1_QUERY_TAG" />
				</button>
			</div>
			<div class="container-fluid">
				<form id="queryForm" class="form-horizontal">
					<div class="control-group">
						<label class="col-sm-3 control-label" for="prdSeqIDTxt"> <s:text name="PRD_SEQ_ID_TAG" /></label>
						<div class="col-sm-5">
							<input id="prdSeqIDTxt" class="form-control" type="text">
						</div>
						<div class="col-sm-4"></div>
					</div>
				</form>
			</div>
			<div class="container-fluid">
				<div class="container-fluid">
					<h3>玻璃当前信息</h3>
				</div>
				<div id="pnlWipInfoDiv">
					<table id="pnlWipInfoGrd"></table>
					<div id="pnlWipInfoPg"></div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="container-fluid">
				<div class="container-fluid">
					<h5>不良判定信息</h5>
				</div>
				<div id="defectInfoDiv">
					<table id="defectInfoGrd"></table>
					<div id="defectInfoPg"></div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="container-fluid">
					<h5>厚度判定信息</h5>
				</div>
				<div id="cdpcInfoDiv">
					<table id="cdpcInfoGrd"></table>
					<div id="cdpcInfoPg"></div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="container-fluid">
					<h5>阻值判定信息</h5>
				</div>
				<div id="cdpzInfoDiv">
					<table id="cdpzInfoGrd"></table>
					<div id="cdpzInfoPg"></div>
				</div>
			</div>
		</div>
	</div>


	<!-- <div class="container-fluid">
		<div class="container-fluid">
			<div class="container-fluid">
				<h3>
					玻璃当前信息
				</h3>
			</div>
		</div>
	</div>
	<div id="pnlWipInfoDiv" class ="col-md-6">
		<table id="pnlWipInfoGrd"></table>
		<div id="pnlWipInfoPg"></div>
	</div>
	<div id="cdpcInfoDiv" class ="span5">
		<table id="cdpcInfoGrd"></table>
		<div id="cdpcInfoPg"></div>
	</div> -->
	<!-- 列表区--结束 -->
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="js/page/9000/9300.js"></script>
<script type="text/javascript" src="js/dialog/showFabSnDetailInfoDialog.js"></script>
</html>