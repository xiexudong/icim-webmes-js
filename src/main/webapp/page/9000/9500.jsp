<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9500_QUERY_WO_INFO_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#pnlInfoListDiv {
	height: 100%;
}

#boxInfoForm .control-group {
	margin-bottom: 3px;
}
/*#pnlInfoListDiv{
      	position:absolute;
      	height:90%;	
    }*/
#tabDiv {
	width: 98%;
	margin-top: 10px;
}

#tabContentDiv {
	width: 100%;
}
#button-group{
	margin-left: 15px;
}
#queryForm,#input-panel,#tabContentDiv{
	margin-top: 10px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="f2ExportWoInfo">导出内部订单信息</button>
		<button class="btn btn-primary" id="f3ExportSoInfo">导出订单数量信息</button>
		<button class="btn btn-primary" id="f4ExportDnInfo">导出交货订单信息</button>
		<button class="btn btn-primary" id="f5ExportDpsPlanInfo">导出排产计划信息</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid" id="input-panel">
		<div class="control-group">
			<div class="controls"></div>
		</div>
		<div class="row">
			<form id="queryForm" class="form-horizontal">
				<div class="control-group">
					<label class="col-sm-2 control-label" for="lotIDTxt"> <s:text name="WORDER_ID_TAG" />
					</label>
					<div class="col-sm-2">
						<input id="woIDTxt" class="form-control input-sm" type="text">
					</div>
					<div class="col-sm-8"></div>
				</div>
			</form>
		</div>
	</div>


	<div class="container-fluid" id="tabDiv">
		<ul id="mytab" class="nav nav-tabs">
			<li class="active"><a id="tab1" href="#tabPane_woInfo" data-toggle="tab">内部订单信息 </a></li>
			<li><a id="tab2" href="#tabPane_woQtyInfo" data-toggle="tab"> 内部订单数量信息 </a></li>
			<li><a id="tab3" href="#tabPane_shipNoticeInfo" data-toggle="tab"> 交货订单信息 </a></li>
			<li><a id="tab4" href="#tabPane_woDpsInfo" data-toggle="tab"> 排产计划信息 </a></li>
		</ul>
		<div class="tab-content" id="tabContentDiv">
			<div class="tab-pane active" id="tabPane_woInfo">
				<table id="woInfoGrd"></table>
				<div id="woInfoPg"></div>
			</div>
			<div class="tab-pane" id="tabPane_woQtyInfo">
				<table id="woQtyGrd"></table>
				<div id="woQtyPg"></div>
			</div>
			<div class="tab-pane" id="tabPane_shipNoticeInfo">
				<table id="shipNoticeGrd"></table>
				<div id="shipNoticePg"></div>
			</div>
			<div class="tab-pane" id="tabPane_woDpsInfo">
				<table id="woDpsInfoListGrd"></table>
				<div id="woDpsInfoListPg"></div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/com/TransUtil.js"></script>
<script src="js/com/TimestampUtil.js"></script>
<script src="js/page/9000/9500.js"></script>
</html>