<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_9800_QUERY_SO_INFO_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#tabDiv {
	width: 96%;
}

#tabContentDiv {
	width: 100%;
}

#button-group {
	margin-left: 15px;
}
#input-panel{
	width: 98%;
}
#inputDiv,#tabContentDiv, #input-panel{
	margin-top: 10px;
}
#input-panel{
	margin-left: 10px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="export_wo_btn">
			<s:text name="EXPORT_FILE_TAG" />
			订单信息
		</button>
		<button class="btn btn-primary" id="export_box_btn">
			<s:text name="EXPORT_FILE_TAG" />
			详细信息
		</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid" id="inputDiv">
		<div class="control-group">
			<div class="controls col-xs-12"></div>
		</div>
		<div class="row">
			<div class="col-xs-3">
				<div class="control-group" id="input-panel">
					<label class="control-label" for="soIDTxt"><s:text name="SOR_ID_TAG" /></label>
					<input id="soIDTxt" class="form-control input-sm" type="text">
				</div>
			</div>
			<div class="col-xs-3">
				<fieldset id="bayeqpFld" class="row">
					<legend>制定日期</legend>
					<div class="control-group">
						<label class="control-label col-xs-4">开始时间：</label>
						<div class="col-xs-8">
							<input id="beginTimeTxt1" class="form-control input-sm" type="text">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-xs-4">结束时间：</label>
						<div class="col-xs-8">
							<input id="endTimeTxt1" class="form-control input-sm" type="text">
						</div>
					</div>
				</fieldset>
			</div>
			<div class="col-xs-3">
				<fieldset id="bayeqpFld" class="row">
					<legend>计来日期</legend>
					<div class="control-group">
						<label class="control-label col-xs-4">开始时间：</label>
						<div class="col-xs-8">
							<input id="beginTimeTxt2" class="form-control input-sm" type="text">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-xs-4">结束时间：</label>
						<div class="col-xs-8">
							<input id="endTimeTxt2" class="form-control input-sm" type="text">
						</div>
					</div>
				</fieldset>
			</div>
			<div class="col-xs-3">
				<fieldset id="bayeqpFld" class="row">
					<legend>计交日期</legend>
					<div class="control-group">
						<label class="control-label col-xs-4">开始时间：</label>
						<div class="col-xs-8">
							<input id="beginTimeTxt3" class="form-control input-sm" type="text">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-xs-4">结束时间：</label>
						<div class="col-xs-8">
							<input id="endTimeTxt3" class="form-control input-sm" type="text">
						</div>
					</div>
				</fieldset>				
			</div>
		</div>
	</div>
	<div id="tabDiv" class="container-fluid">
		<ul id="mytab" class="nav nav-tabs">
			<li class="active"><a id="tab1" href="#tabPane_wo" data-toggle="tab"> <s:text name="WO_INFO_TAG" />
			</a></li>
			<li><a id="tab2" href="#tabPane_box" data-toggle="tab"> <s:text name="DETAIL_INFO_TAG" />
			</a></li>
		</ul>
		<div class="tab-content" id="tabContentDiv">
			<div class="tab-pane active" id="tabPane_wo">
				<table id="woInfoListGrd"></table>
				<div id="woInfoListPg"></div>
			</div>
			<div class="tab-pane" id="tabPane_box">
				<table id="boxInfoListGrd"></table>
				<div id="boxInfoListPg"></div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/com/TimestampUtil.js"></script>
<script src="js/page/9000/9800.js"></script>
</html>
