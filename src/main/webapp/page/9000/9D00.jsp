<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_9D00_QUERY_MANY_INFO_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
#inputDiv{
	margin : 20px 0 0 -15px;
}
#tabDiv{
	width:96%;
}
#tabContentDiv{
	width:100%;
}
.row-inline{
	padding-top: 10px;
}
#button-group {
	margin-left: 25px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container-fluid" id="button-group">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="export_btn">
			<s:text name="EXPORT_FILE_TAG" />
		</button>
	</div>

	<!-- 输入区--开始 -->
	<div class="container-fluid" id="inputDiv">
		<div class="row container-fluid">
			<div class="control-group">
				<div class="col-xs-12"></div>
			</div>
			<div class="container-fluid">
				<form id="queryForm" class="form-horizontal">
					<div class="control-group col-md-4 row-inline">
						<label class="control-label col-xs-4" for="cusIDSel"> <s:text
								name="CUS_ID_TAG" /> </label>
						<div class="col-xs-7">
							<select id="cusIDSel" class="form-control input-sm"></select>
						</div>
						<div class="col-xs-1"></div>
					</div>
					<div class="control-group col-md-4 row-inline">
						<label class="control-label col-xs-4" for="mdlIDSel"> <s:text
								name="MDL_ID_TAG" /> </label>
						<div class="col-xs-7">
							<select id="mdlIDSel" class="form-control input-sm"></select>
						</div>
						<div class="col-xs-1"></div>
					</div>
					<div class="control-group col-md-4 row-inline">
						<label class="control-label col-xs-4" for="woIDSel"> <s:text
								name="WORDER_ID_TAG" /> </label>
						<div class="col-xs-7">
							<select id="woIDSel" class="form-control input-sm"></select>
						</div>
						<div class="col-xs-1"></div>
					</div>
					<div class="control-group col-md-4 row-inline">
						<label class="control-label col-xs-4" for="beginTimeTxt"> <s:text
								name="BEGIN_TIME_TAG" /> </label>
						<div class="col-xs-7">
							<input id="beginTimeTxt" class="form-control input-sm" type="text">
						</div>
						<div class="col-xs-1"></div>
					</div>
					<div class="control-group col-md-4 row-inline">
						<label class="control-label col-xs-4" for="endTimeTxt"> <s:text
								name="END_TIME_TAG" /> </label>
						<div class="col-xs-7">
							<input id="endTimeTxt" class="form-control input-sm" type="text">
						</div>
						<div class="col-xs-1"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="tabDiv" class="container-fluid row-inline">
			<ul id="mytab" class="nav nav-tabs">
				<li class="active">
					<a id="tab1" href="#tabPane_so" data-toggle="tab">
						<s:text name="SO_INFO_TAG"/>	
					</a>
				</li>
				<li>
					<a id="tab2" href="#tabPane_wo" data-toggle="tab">
						内部<s:text name="WO_INFO_TAG"/>	
					</a>
				</li>
				<li>
					<a id="tab3" href="#tabPane_dps" data-toggle="tab">
						<s:text name="DPS_INFO_TAG"/>
					</a>
				</li>
				<li>
					<a id="tab4" href="#tabPane_dn" data-toggle="tab">
						<s:text name="DN_INFO_TAG"/>
					</a>
				</li>
			</ul>
			<div class="tab-content row-inline" id="tabContentDiv">
				<div class="tab-pane active" id="tabPane_so">
						<table id="soInfoListGrd"></table>
						<div id="soInfoListPg"></div>
				</div>
				<div class="tab-pane" id="tabPane_wo">
						<table id="woInfoListGrd"></table>
						<div id="woInfoListPg"></div>
				</div>
				<div class="tab-pane" id="tabPane_dps">
						<table id="dpsInfoListGrd"></table>
						<div id="dpsInfoListPg"></div>
				</div>
				<div class="tab-pane" id="tabPane_dn">
						<table id="dnInfoListGrd"></table>
						<div id="dnInfoListPg"></div>
				</div>
			</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/com/TimestampUtil.js"></script>
<script src="js/page/9000/9D00.js"></script>
</html> 	