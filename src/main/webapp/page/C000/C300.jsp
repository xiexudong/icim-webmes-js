<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>

<%@taglib prefix="s" uri="/struts-tags"%>

<%

String path = request.getContextPath();

String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>



<!DOCTYPE html>

<html>

<head>

	<base href="<%=basePath%>">

	<title><s:text name="M_C300_TITLE_TAG"/></title> 
	<!-- Bootstrap -->
	
	<script type="text/javascript">
		var basePath = "<%=basePath%>";
	</script>

	<%@ include file="/page/comPage/comCSS.html"%> 
	<%@ include file="/page/comPage/navbar.html"%>
	<applet alt="Printers" name="Printers" mayscript="mayscript" codebase="." code="com.print.PrinterApplet.class" archive="<%=basePath%>jar/print.jar, <%=basePath%>jar/barcode4j.jar, <%=basePath%>jar/jackson-all-1.9.11.jar" width="0" height="0"></applet>  
	<script type="text/javascript">
      try{
	      var applet = document.Printers;
	      var printers = applet.findAllPrinters();
	      printers = eval(printers);
	      console.log(printers);
      }catch(ex){
      	 console.info(ex);
      }
	</script>
</head>

<body>
	<div class="container">
		<div class="span12">
			<button class="btn btn-primary" id="f1_query_btn">                                                                                                                     
				补印                                                                                                                                     
			</button>                                                                                                                                                    
		</div>
		
		<div class ="span12">
         	<div class="control-group">
				<div class="controls">
					<legend> 
						<i class="icon-calendar"></i>
						请选择打印条件并输入箱号
					</legend>
				</div>
         	</div>
        </div> 
       	<div class="row ">
       		<div id ="boxForm" class="row form-horizontal">
       		     <div class="control-group span3">                          
			      	<label class="control-label " for="proc_id">
			      		补印站点                      
			      	</label>                                         
			      	<div class="controls">                           
			        	<select id="proc_id">
			        		<option value="JBQA">减薄QA</option>
			        		<option value="DMQA">镀膜QA</option>
			        	</select>
				    </div>                                             
		   	 	</div>
       			<div class="control-group span4">                          
			      	<label class="control-label " for="boxidTxt">
			      		<s:text name="BOX_ID_TAG"/>                      
			      	</label>                                         
			      	<div class="controls">                           
			        	<input type="text" id="boxidTxt" class="span3"/>
				    </div>                                             
		   	 	</div>
		   	 	<div class="control-group span4">                          
			      	<label class="control-label " for="boxidTxt">打印机：</label>                                         
			      	<div class="controls">                           
			        	<select id="print2Select" class="span3"></select>
				    </div>                                             
		   	 	</div>
			</div>
		</div>	
		<div id ="shtListDiv003" class="span12">
			<table id="shtListGrd003"></table>
			<div id="shtListPg003"></div>
		</div>
		<div id ="shtListDiv006" class="span12">
			<table id="shtListGrd006"></table>
			<div id="shtListPg006"></div>
		</div>
		<div id ="shtListDiv008" class="span12">
			<table id="shtListGrd008"></table>
			<div id="shtListPg008"></div>
		</div>
		<div id ="shtListDiv063" class="span12">
			<table id="shtListGrd063"></table>
			<div id="shtListPg063"></div>
		</div>
		<div id ="shtListDivTM" class="span12">
			<table id="shtListGrdTM"></table>
			<div id="shtListPgTM"></div>
		</div>
		<div id ="shtListDiv115" class="span12">
			<table id="shtListGrd115"></table>
			<div id="shtListPg115"></div>
		</div>
		<div id ="shtListDivOther" class="span12">
			<table id="shtListGrdOther"></table>
			<div id="shtListPgOther"></div>
		</div>
	</div>
</body>
	<%@ include file="/page/comPage/comJS.html"%>
	<script type="text/javascript" src="js/page/C000/C300.js" ></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/com//printl.js"></script>
</html>