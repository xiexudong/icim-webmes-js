<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>

<%@taglib prefix="s" uri="/struts-tags"%>

<%

String path = request.getContextPath();

String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>



<!DOCTYPE html>

<html>

<head>

	<base href="<%=basePath%>">

	<title>C500:栈板标签补印</title> 
	<!-- Bootstrap -->
	
	<script type="text/javascript">
		var basePath = "<%=basePath%>";
	</script>

	<%@ include file="/page/comPage/comCSS.html"%> 
	<%@ include file="/page/comPage/navbar.html"%>
	<style>
		.color{
          	color:red;
          }
         .right{
         	margin : 0 50px 0 0;
         }
         .left{
         	margin : 0 0 0 90px;
         }
	</style>
	    <link href="lib/HubSpot-messenger/build/css/messenger-theme-future.css"
        rel="stylesheet" media="screen" />
        <link href="lib/HubSpot-messenger/build/css/messenger.css"
        rel="stylesheet" media="screen" />
</head>

<body>
	<div class="container-fluid">
		<div class="row col-lg-12">
			<button class="btn btn-info"    id="f1_query_btn">                                                                                                                     
				<s:text name="F1_QUERY_TAG"/>                                                                                                                                        
			</button>                                                                                                                                                              
			<button class="btn btn-primary" id="print_btn">                                                                                                                       
				<s:text name="PRINT_TAG"/>                                                                                                                                       
			</button>                                                                                                                                                            
		</div>
		
		<div class ="row col-lg-12">
			<div class = "row">
		<fieldset>
			<form class="form-horizontal">
				<div class = "row">
				<div class="form-group col-lg-4 ">
				<label class="control-label col-lg-5" for="palletIdTxt"> <s:text name="PALLET_ID_TAG" /></label> 
				<div class="col-lg-7">
				<input id="palletIdTxt" type="text" class="form-control">
				</div>
				</div>
				<div class="form-group col-lg-4 "> 
				<label class="control-label col-lg-5" for="noteTxt"> <s:text name="备注" /></label> 
				<div class="col-lg-7">
				<input id="noteTxt" type="text" class="form-control">
				</div>
				</div>
				<div class="form-group col-lg-4 ">
				 <label class="control-label col-lg-5" for="print1Select"> <s:text name="打印机" /></label> 
				 <div class="col-lg-7">
				 <select id="print1Select" class="form-control"></select>
				 </div>
				</div>
				</div>
			</form>

		</fieldset>
		</div>
		</div>
		<div class="row col-lg-12">
			<form class="form-inline " >
				<div class="row">
					<div id ="boxInfDiv" class="controls col-lg-12 col-lg-offset-1">
						<table id="boxInfGrd"></table>
						<div id="boxInfPg"></div>
					</div>
				</div>
			</form>
		</div>
	</div>
	
</body>
	<%@ include file="/page/comPage/comJS.html"%>
	<script type="text/javascript" src="js/page/C000/C500.js" ></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/com/printl.js"></script>
	<script src="lib/HubSpot-messenger/build/js/messenger.min.js"></script>
	<script src="js/dialog/showMessenger.js"></script>
	<applet alt="Printers" name="Printers" mayscript="mayscript" codebase="." code="com.print.PrinterApplet.class" archive="<%=basePath%>jar/print.jar, <%=basePath%>jar/barcode4j.jar, <%=basePath%>jar/jackson-all-1.9.11.jar" width="0" height="0"></applet>  
	<script type="text/javascript">
      try{
	      var applet = document.Printers;
	      var printers = applet.findAllPrinters();
	      printers = eval(printers);
	      console.log(printers);
      }catch(ex){
      	 console.info(ex);
      }
	</script>
</html>