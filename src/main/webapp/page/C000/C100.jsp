<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>

<%@taglib prefix="s" uri="/struts-tags"%>

<%

String path = request.getContextPath();

String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>



<!DOCTYPE html>

<html>

<head>

	<base href="<%=basePath%>">

	<title><s:text name="M_C100_TITLE_TAG"/></title> 
	<!-- Bootstrap -->
	
	<script type="text/javascript">
		var basePath = "<%=basePath%>";
	</script>

	<%@ include file="/page/comPage/comCSS.html"%> 
	<%@ include file="/page/comPage/navbar.html"%>
	<style>
		.color{
          	color:red;
          }
         .right{
         	margin : 0 50px 0 0;
         }
         .left{
         	margin : 0 0 0 90px;
         }
	</style>
</head>

<body>
	<div class="container">
		<div class="span12">
			<button class="btn btn-info"    id="f1_query_btn">                                                                                                                     
				<s:text name="F1_QUERY_TAG"/>                                                                                                                                        
			</button>                                                                                                                                                              
			<button class="btn btn-primary" id="print_btn">                                                                                                                       
				<s:text name="PRINT_TAG"/>                                                                                                                                       
			</button> 
			<button class="btn btn-primary" id="printNew_btn">
				<s:text name="新打印"/>
			</button>                                                                                                                                                             
		</div>
		
		<div class ="span12">
			<legend>补印信息</legend>
	     	<div class ="span3 right">
				<fieldset>
					<div class="row control-group">
						<form class="form-horizontal">
							<label class="control-label keyCss" for="cusIdSel">
								<s:text name="CUS_ID_TAG" />
							</label>
							<div class="controls">                           
								<select id="cusIdSel" class="span2"></select>
							</div>
							<label class="control-label keyCss" for="woIdTxt">
								<s:text name="WORDER_ID_TAG"/>
							</label>
							<div class="controls">
								<input  id="woIdTxt" type="text" class="span2">
							</div>
							<label class="control-label" for="boxIdTxt">
								<s:text name="PPBOX_ID_TAG"/>
							</label>
							<div class="controls">
								<input  id="boxIdTxt" type="text" class="span2">
							</div>
						</form>
					</div>
				</fieldset>
	     	</div>
	     	<div class ="span3">
				<fieldset>        
					<div class="row control-group">
						<form class="form-horizontal">
							<label class="control-label color keyCss" for="woBoxTotalCntInput">
								<s:text name="WO_BOX_COUNT_TAG"/>
							</label>
							<div class="controls">
								<input  id="woBoxTotalCntInput" type="text" class="span1">
							</div>
							<label class="control-label color" for="mtrlIdInput">料号</label>
							<div class="controls">
								<input  id="mtrlIdInput" type="text" class="span2">
							</div>
							<label class="control-label color" for="hdIdInput">手册号</label>
							<div class="controls">
								<input  id="hdIdInput" type="text" class="span2">
							</div>
						</form>
					</div>
				</fieldset>
	     	</div>
		</div>
		<div class="span12 row">
			<!-- 已下线产品详细信息Div -->
			<!-- 应该保持Div ==>
			form ==> Div ....的好习惯 -->
			<form class="form-inline " >
				<div class="row">
					<div id ="boxInfDiv" class="controls span11">
						<table id="boxInfGrd"></table>
						<div id="boxInfPg"></div>
					</div>
				</div>
			</form>
		</div>
	</div>
	
</body>
	<%@ include file="/page/comPage/comJS.html"%>
	<script type="text/javascript" src="js/page/C000/C100.js" ></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/com/printl.js"></script>
	<script type="text/javascript" src="js/com/DestDesc.js"></script>
</html>