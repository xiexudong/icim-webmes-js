<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>

<%@taglib prefix="s" uri="/struts-tags"%>

<%

String path = request.getContextPath();

String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>



<!DOCTYPE html>

<html>

<head>

	<base href="<%=basePath%>">

	<title><s:text name="M_C200_TITLE_TAG"/></title> 
	<!-- Bootstrap -->
	
	<script type="text/javascript">
		var basePath = "<%=basePath%>";
	</script>

	<%@ include file="/page/comPage/comCSS.html"%> 
	<%@ include file="/page/comPage/navbar.html"%>

</head>

<body>
	<div class="container">
		<div class="span12">
			<button class="btn btn-info"    id="f1_query_btn">                                                                                                                     
				<s:text name="F1_QUERY_TAG"/>                                                                                                                                        
			</button>                                                                                                                                                              
			<button class="btn btn-primary" id="print_btn">                                                                                                                       
				<s:text name="PRINT_TAG"/>                                                                                                                                       
			</button>                                                                                                                                                              
		</div>
		
		<div class ="span9">
         	<div class="control-group">
				<div class="controls">
					<legend> 
						<i class="icon-calendar"></i>
						请输入<s:text name="LOT_ID_TAG"/> 
					</legend>
				</div>
         	</div>
         </div> 
       	<div class="row ">
       		<form id ="boxForm" class="row form-horizontal"> 
       			<div class="control-group span4">                          
			      	<label class="control-label " for="lotidTxt">
			      		<s:text name="LOT_ID_TAG"/>                      
			      	</label>                                         
			      	<div class="controls">                           
			        	<input type="text" id="lotidTxt" class="span3"/>
				    </div>                                             
		   	 	</div>
			</form>
		</div>	
		
		<div class="span9 row">
			<!-- 已下线产品详细信息Div -->
			<!-- 应该保持Div ==>
			form ==> Div ....的好习惯 -->
			<form class="form-inline span8" >
				<div class="row">
					<div id ="shtListDiv" class="controls span7">
						<table id="shtListGrd"></table>
						<div id="shtListPg"></div>
					</div>
				</div>
			</form>
		</div>
	</div>
	
</body>
	<%@ include file="/page/comPage/comJS.html"%>
	<script type="text/javascript" src="js/page/C000/C200.js" ></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
	<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/com//printl.js"></script>
</html>