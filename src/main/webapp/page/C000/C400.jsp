<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%

String path = request.getContextPath();

String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>">

<title><s:text name="M_C400_TITLE_TAG" /></title>
<!-- Bootstrap -->

<script type="text/javascript">
		var basePath = "<%=basePath%>";
	</script>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

<style>
.color {
	color: red;
}

.right {
	margin: 0 50px 0 0;
}

.left {
	margin: 0 0 0 90px;
}
</style>
</head>

<body>
<div class="container-fluid">
	<div class="row col-lg-12">
		<button class="btn btn-info" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="print_btn">
			<s:text name="PRINT_TAG" />
		</button>
	</div>

	<div class="row col-lg-12">
	<div class = "row">
		<fieldset>
			<legend>补印信息</legend>
			<form class="form-horizontal">
				<%-- <label class="control-label- col-md-1" for="woIdTxt"> <s:text name="WORDER_ID_TAG" />
				</label> <input id="woIdTxt" type="text" class="col-md-2 form-control"> --%>
				<div class = "row">
				<div class="form-group col-lg-4 ">
				<label class="control-label col-lg-5" for="boxIdTxt"> <s:text name="BOX_ID_TAG" /></label> 
				<div class="col-lg-7">
				<input id="boxIdTxt" type="text" class="form-control">
				</div>
				</div>
				<div class="form-group col-lg-4 "> 
				<label class="control-label col-lg-5" for="proc_id"> <s:text name="站点" /></label> 
				<div class="col-lg-7">
				<select id="proc_id" class="form-control">
					<option value="QP02">抛光包装QA</option>
					<option value="QD02">镀膜包装QA</option>
					<option value="P001">抛光工艺</option>
					<option value="DMBZ">镀膜包装</option>
				</select>
				</div>
				</div>
				<div class="form-group col-lg-4 ">
				 <label class="control-label col-lg-5" for="print1Select"> <s:text name="打印机" /></label> 
				 <div class="col-lg-7">
				 <select id="print1Select" class="form-control"></select>
				 </div>
				</div>
				</div>
			</form>

		</fieldset>
		</div>
	</div>
				<div id="boxInfDiv" class="controls col-lg-12 col-lg-offset-1">
					<table id="boxInfGrd"></table>
					<div id="boxInfPg"></div>
				</div>
</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/C000/C400.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script type="text/javascript" src="<%=basePath%>js/com/printl.js"></script>
<script type="text/javascript" src="js/com/DestDesc.js"></script>
<applet alt="Printers" name="Printers" mayscript="mayscript" codebase="." code="com.print.PrinterApplet.class"
	archive="<%=basePath%>jar/print.jar, <%=basePath%>jar/barcode4j.jar, <%=basePath%>jar/jackson-all-1.9.11.jar" width="0" height="0"></applet>
<script type="text/javascript">
      try{
	      var applet = document.Printers;
	      var printers = applet.findAllPrinters();
	      printers = eval(printers);
	      console.log(printers);
      }catch(ex){
      	 console.info(ex);
      }
	</script>
</html>