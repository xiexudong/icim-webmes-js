<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_3300_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<link href="lib/datetimepicker/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" media="screen">

<%@ include file="/page/comPage/navbar.html"%>
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#toolSmListDiv{
   width:700px;
}
.form-group {
	margin: 15px 0px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row col-lg-12">
			<button class="btn btn-primary" id="f1_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="f8_btn">
				<s:text name="F8_REGIST_TAG" />
			</button>
		</div>
		<div class="row">&nbsp</div>
		<div class="row col-lg-12">
			<form id="toolDetailForm" class="row form-horizontal">
				<div class="form-group col-lg-3">
					<label class="control-label col-lg-5" for="opeSelect"><s:text
							name="OPE_ID_TAG" /></label>
					<div class="col-lg-7">
						<select id="opeSelect" class="form-control"></select>
					</div>
				</div>
				<div class="form-group col-lg-3">
					<label class="control-label col-lg-5" for="toolSelect"> <s:text
							name="TOOL_ID_TAG" />
					</label>
					<div class="col-lg-7">
						<select id="toolSelect" class="form-control"></select>
					</div>
				</div>
				<div class="form-group col-lg-3">
					<label class="control-label col-lg-5" for="pmCateSel"> <s:text
							name="保养类型" />
					</label>
					<div class="col-lg-7">
						<select id="pmCateSel" class="form-control"></select>
					</div>
				</div>
			</form>
		</div>
		<div class="row col-lg-12">
			<div id="tabDiv" class="col-lg-10">
				<ul id="mytab" class="nav nav-tabs">
					<li class="active"><a id="tab1" href="#tabPane_toolPm"
						data-toggle="tab"> <s:text name="设备保养维护" />
					</a></li>
					<li><a id="tab2" href="#tabPane_toolSm" data-toggle="tab">
							<s:text name="设备保养周期模拟" />
					</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="tabPane_toolPm">
						<div class="col-lg-6">
							<fieldset>
								<legend>选择信息</legend>
								<table>
									<tr>
									<div class="form-group">
										<label class="control-label col-lg-3" for="beginTimeDatepicker">
											<s:text name="BEGIN_TIME_TAG" />
										</label>
										<div class="col-lg-7">
											<div id="beginTimeDatepicker" class="input-append">
												<input data-format="yyyy-MM-dd hh:mm:ss" type="text"
													class="form-control"> <span class="add-on"> <i
													data-time-icon="icon-time" data-date-icon="icon-calendar"
													class="icon-calendar"></i>
												</span>
											</div>
										</div>
										</div>
									</tr>
									<div class="form-group">
									<label class="control-label col-lg-3" for="endTimeDatepicker"> <s:text
											name="END_TIME_TAG" />
									</label>
									<div class="col-lg-7 ">
										<div id="endTimeDatepicker" class="input-append">
											<input data-format="yyyy-MM-dd hh:mm:ss" type="text"
												class="form-control"> <span class="add-on"> <i
												data-time-icon="icon-time" data-date-icon="icon-calendar"
												class="icon-calendar"></i>
											</span>
										</div>
									</div>
									</div>
									<tr>
									</tr>

									<tr>
									<div class="form-group">
										<label class="control-label col-lg-3" for="remarkTxta"><s:text
												name="保养说明" /></label>
										<div class="col-lg-7">
											<textarea class="input form-control" id="remarkTxta" rows="3"></textarea>
										</div>
										</div>
									</tr>
								</table>
								<%-- 							<form class="form-horizontal" id="conditionform">
								<label class="control-label" for="beginTimeDatepicker">
									<s:text name="BEGIN_TIME_TAG" />
								</label>
								<div class="controls ">
									<div id="beginTimeDatepicker" class="input-append">
										<input data-format="yyyy-MM-dd hh:mm:ss" type="text"
											class="span2"> <span class="add-on"> <i
											data-time-icon="icon-time" data-date-icon="icon-calendar"
											class="icon-calendar"></i>
										</span>
									</div>
								</div>
								<label class="control-label" for="endTimeDatepicker"> <s:text
										name="END_TIME_TAG" />
								</label>
								<div class="controls ">
									<div id="endTimeDatepicker" class="input-append">
										<input data-format="yyyy-MM-dd hh:mm:ss" type="text"
											class="span2"> <span class="add-on"> <i
											data-time-icon="icon-time" data-date-icon="icon-calendar"
											class="icon-calendar"></i>
										</span>
									</div>
								</div>
								<label class="control-label" for="remarkTxta"><s:text
										name="保养说明" /></label>
								<div class="controls">
									<textarea class="input span2" id="remarkTxta" rows="3"></textarea>
								</div>
							</form> --%>
							</fieldset>
						</div>
						<div class="col-lg-5">
							<fieldset>
								<legend>保养项目列表</legend>
								<div id="dataItemEditDiv"></div>
							</fieldset>
						</div>
					</div>
					<div class="tab-pane" id="tabPane_toolSm">
						<div id="toolSmListDiv" class="row col-lg-6">
							<%-- 		<button id="toolSmBtn" class="btn btn-primary">
								<s:text name="保养模拟" />
							</button> --%>
							<fieldset>
								<legend>未来保养周期列表</legend>
								<div id="toolSmtListDiv" class="row col-lg-12">
									<table id="toolSmListGrd" ></table>
									<div id="toolSmListPg"></div>
								</div>
							</fieldset>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Le javascript
    ================================================== -->
	<%@ include file="/page/comPage/comJS.html"%>
	<!-- datetimepicker -->
	<script src="lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script
		src="lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
	<script src="js/dialog/showSimpleInputDialog.js"></script>
	<script src="js/page/3000/3300.js"></script>

</body>
</html>
