<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
   <head>
      <base href="<%=basePath%>">
      <title><s:text name="M_3200_TITLE_TAG"/></title>
      <%@ include file="/page/comPage/comCSS.html"%>
      <link href="lib/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

      <%@ include file="/page/comPage/navbar.html"%>
   </head>
   <body> 
    <div class="container-fluid">
         <div class="col-lg-12">
             <button class="btn btn-primary" id="f1_btn"><s:text name="F1_QUERY_TAG"/></button>
            <!--  <button class="btn btn-primary" id="f4_btn"><s:text name="F4_DELETE_TAG"/></button>
             <button class="btn btn-primary" id="f5_btn"><s:text name="F5_UPDATE_TAG"/></button> -->
             <button class="btn btn-primary" id="f8_btn"><s:text name="F8_REGIST_TAG"/></button>
         </div>
         <div class="row">
          <div class="col-lg-4">
            <fieldset>
                <legend><s:text name="WORK_ZONE_TAG"/></legend>
                <div class="row">
                  <form class="form-horizontal">
                  <div class="form-group">
                    <label class="control-label col-lg-3" for="baySelect"><s:text name="BAY_AREA_TAG"/></label>
                    <div class="col-lg-5">
                      <select id="baySelect" class="form-control"></select>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label keyCss col-lg-3" for="toolSelect"><s:text name="EQPT_TAG"/></label>
                    <div class="col-lg-5">
                      <select id="toolSelect" class="form-control"></select>
                    </div>
                    </div>
                  </form>
                </div>
              </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset>
                <legend><s:text name="CHANGE_STATUS_TAG"/></legend>
                <div class="row control-group">
                  <form class="form-horizontal">
                   <div class="form-group">
                    <label class="control-label keyCss col-lg-3" for="beginTimeDatepicker">
                      <s:text name="BEGIN_TIME_TAG"/>
                    </label>
                    <div class="col-lg-5 ">
                      <div id="beginTimeDatepicker" class="input-append">
                        <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class="form-control" >
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                        </span>
                      </div>
                    </div>
                    </div>
                     <div class="form-group">
                    <label class="control-label keyCss col-lg-3" for="endTimeDatepicker">
                      <s:text name="END_TIME_TAG"/>
                    </label>
                    <div class="col-lg-5 ">
                      <div id="endTimeDatepicker" class="input-append">
                        <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class="form-control" >
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                        </span>
                      </div>
                    </div>
                    </div>
                  </form>
                </div>
              </fieldset>
          </div>
         </div>			

         <div class="col-lg-8" id="toolCheckInfoDiv">
           <fieldset>
             <legend>
               <s:text name="TOOL_INSPECTION_INFO_TAG"/>(双击输入)         
             </legend>
             <table id="toolCheckInfoGrd"></table>
             <div id="toolCheckInfoPg"></div>
           </fieldset>
         </div>
   </div> 
    
    <!-- Le javascript
    ================================================== -->
    <%@ include file="/page/comPage/comJS.html"%> 
    <!-- datetimepicker -->
    <script src="lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="js/dialog/showSimpleInputDialog.js" ></script>

    <script src="js/page/3000/3200.js" ></script>
 
   </body>
</html>
