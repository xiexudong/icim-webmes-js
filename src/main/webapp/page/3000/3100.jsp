<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
   <head>
      <base href="<%=basePath%>">
      <title><s:text name="M_3100_TITLE_TAG"/></title>
      <%@ include file="/page/comPage/comCSS.html"%>
      <link href="lib/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

      <%@ include file="/page/comPage/navbar.html"%>
   </head>
   <body>
    
    <div class="container-fluid">
         <div class="col-lg-12">
             <button class="btn btn-primary" id="f1_btn"><s:text name="F1_QUERY_TAG"/></button>
             <button class="btn btn-primary" id="f8_btn"><s:text name="F8_REGIST_TAG"/></button>
         </div>
         <div class="row">
          <div class="col-lg-4">
            <fieldset>
                <legend><s:text name="WORK_ZONE_TAG"/></legend>
                <div class="row ">
                  <form class="form-horizontal">
                  <div class="form-group">
                    <label class="control-label col-lg-3" for="opeSelect"><s:text name="OPE_ID_TAG"/></label>
                    <div class="col-lg-5">
                        <select id="opeSelect" class="form-control"></select>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label keyCss col-lg-3" for="toolSelect"><s:text name="EQPT_TAG"/></label>
                    <div class="col-lg-5">
                        <select id="toolSelect" class="form-control"></select>
                    </div>
                    </div>
                  </form>
                </div>
              </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset>
                <legend><s:text name="CHANGE_STATUS_TAG"/></legend>
                <div class="row control-group">
                  <form class="form-horizontal">
                  <div class="form-group">
                    <label class="control-label keyCss col-lg-4" for="toolStatusSelect"><s:text name="NEXT_STATUS_TAG"/></label>
                    <div class="col-lg-5">
                      <select id="toolStatusSelect" class="form-control"></select>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label keyCss col-lg-4" for="statusChangeDatepicker"><s:text name="STATUS_CHANGE_TIME_TAG"/></label>
                    <div class="col-lg-5">
                      <div id="statusChangeDatepicker" class="input-append">
                        <input data-format="yyyy-MM-dd" type="text" class="form-control" >
                        <span class="add-on">
                            <i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar"></i>
                        </span>
                      </div>
                    </div>
                    </div>
                    <div class="form-group">
                      <div id="statusChangeTimepicker" class="input-append col-lg-5 col-lg-offset-4">
                        <input data-format="hh:mm:ss" type="text" class="form-control">
                        <span class="add-on">
                            <i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-time"></i>
                        </span>
                      </div>
                    </div>
                  </form>
                </div>
              </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset>
                <legend><s:text name="REMARK_TAG"/></legend>
                <div class="control-group">
                    <div class="col-lg-8">
                        <textarea id="remarkTxta" class="input-xlarge form-control" id="textarea" rows="3" maxlength="100"></textarea>
                    </div>
                </div>
              </fieldset>
          </div>
         </div>			

         <div class="col-lg-12 tabbable" id="toolStatusInfoDiv">
            <ul class="nav nav-tabs" id="toolTab">
                <li class="active"><a href="#toolStatusT" data-toggle="tab"><s:text name="TOOL_STATUS_INFO_TAG"/></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="toolStatusT">
                    <table id="toolStatusInfoGrd"></table>
                    <div id="toolStatusInfoPg"></div>
                </div>
            </div>

         </div>
   </div> 
    
    <!-- Le javascript
    ================================================== -->
    <%@ include file="/page/comPage/comJS.html"%> 
    <!-- datetimepicker -->
    <script src="lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>

    <script src="js/page/3000/3100.js" ></script>
 
   </body>
</html>
