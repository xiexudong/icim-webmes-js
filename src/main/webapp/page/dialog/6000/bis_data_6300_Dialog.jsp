<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
</head>
<style>
/*当Modal的width需要自定义时，需要同时设置margin-left和left。
      以保证Modal显示在屏幕中间
    */
#bis_data_6100_Dialog {
	margin-left: 10%;
	width: 70%;
	left: 10%;
}

#dataForm .form-horizontal .controls {
	margin-bottom: 0px;
}

#dataForm .control-group {
	margin-bottom: 0px;
}

#dataCateForm .form-horizontal .controls {
	margin-bottom: 0px;
}

#dataCateForm .control-group {
	margin-bottom: 0px;
}

.keyClass {
	color: blue;
	font-weight: bold;
}
</style>
<body>
	<!--  BayArea dialogs-->
	<div id="bis_data_6100_Dialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="UPDATE_BIS_DATA_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div>
						<form id="dataCateForm" class="form-horizontal row">
							<div class="control-group ">
								<label class="control-label col-xs-3" for="dia_dataCateSel"> <s:text name="DATA_CATE_TAG" />
								</label>
								<div class="col-xs-4">
									<select id="dia_dataCateSel" class="form-control"></select>
								</div>
								<div class="col-xs-5">
									<label id="dia_cateDescSpn" class="help-inline"></label>
								</div>
							</div>
						</form>
						<hr class="row" />
					</div>

					<div class="row">

						<form id="dataForm" class="form-horizontal">
							<div class="row">
								<div class="control-group col-md-6">
									<label id="dia_dataIDLbl" class="control-label col-xs-5 keyClass" for="dia_dataIdTxt"> <s:text name="DATA_ID_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_dataIdTxt" maxlength="4" class="form-control" rel="tooltip" data-original-title="必须输入不超过4位的非中文字符"
											onKeyUp="value=value.replace(/[\W]/g,''); this.value=this.value.toLocaleUpperCase()">
									</div>
								</div>
								<div class="control-group col-md-6">
									<label id="dia_dataExtLbl" class="control-label col-xs-5 keyClass" for="dia_dataExtTxt"> <s:text name="DATA_EXT_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_dataExtTxt" maxlength="50" class="form-control" rel="tooltip" data-original-title="必须输入不超过50位的字符">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="control-group col-md-6">
									<label id="dia_dataItemLbl" class="control-label col-xs-5" for="dia_dataItemTxt"> <s:text name="DATA_ITEM_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_dataItemTxt" class="form-control" maxlength="10" />
									</div>
								</div>
								<div class="control-group col-md-6">
									<label id="dia_dataDescLbl" class="control-label col-xs-5" for="dia_dataDescTxt"> <s:text name="DATA_DESC_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_dataDescTxt" class="form-control" maxlength="60" />
									</div>
								</div>

							</div>
							<div class="row">
								<!-- TR -->
								<div class="control-group col-md-6">
									<label id="dia_ext1Lbl" class="control-label col-xs-5" for="dia_ext1Txt"> <s:text name="EXT_1_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_ext1Txt" class="form-control" maxlength="32" />
									</div>
								</div>
								<div class="control-group col-md-6">
									<label id="dia_ext2Lbl" class="control-label col-xs-5" for="dia_ext2Txt"> <s:text name="EXT_2_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_ext2Txt" class="form-control" maxlength="32" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="control-group col-md-6">
									<label id="dia_ext3Lbl" class="control-label col-xs-5" for="dia_ext3Txt"> <s:text name="EXT_3_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_ext3Txt" class="form-control" maxlength="32" />
									</div>
								</div>
								<div class="control-group col-md-6">
									<label id="dia_ext4Lbl" class="control-label col-xs-5" for="dia_ext4Txt"> <s:text name="EXT_4_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_ext4Txt" class="form-control" maxlength="32" />
									</div>
								</div>

							</div>
							<div class="row">
								<div class="control-group col-md-6">
									<label id="dia_ext5Lbl" class="control-label col-xs-5" for="dia_ext5Txt"> <s:text name="EXT_5_TAG" />
									</label>
									<div class="col-xs-7">
										<input type="text" id="dia_ext5Txt" class="form-control" maxlength="32" />
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="bisDataDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="bisDataDialog_f5update_Btn" class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>
	<!--Modal-->

</body>
</html>