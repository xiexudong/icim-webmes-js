<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
  <base href="<%=basePath%>"></head>
<body>
  <!--  BayArea dialogs-->
  <div id="selectBayDialog" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>
        <s:text name="SELECT_BAYAREA_TAG"/>
      </h3>
    </div>
    <!--Modal header-->
    <div class="modal-body">
      <div class="row">
        <div class="control-group">
          <div class="controls">
            <center>
              <legend> <i class="icon-th"></i>
                <s:text name="ORDER_LIST_TAG"/>
              </legend>
            </center>
          </div>
        </div>
          <div class ="span12">
    <div class="row span12">
      <!-- 产品提配Div -->
      <div class="control-group">
        <div class="controls">
          <center>
            <legend> 
              <i class="icon-calendar"></i>
              <s:text name="ORDER_INFO_TAG"/> 
            </legend>
          </center>
        </div>
      </div>
      <div class="row ">
        <form id ="orderForm" class="form-horizontal"> 
          <div class="control-group span4">
                  <label class="control-label" for="appendedPrependedInput"><s:text name="ORDER_ID_TAG"/>  </label>
                  <div class="controls">
                    <div class="input-append">
                      <input class="span2" id="orderIDTxt" size="16" type="text">
                      <button  id="autoGeneratorBtn" class="btn" type="button"><s:text name="AUTO_GENERATOR_TAG"/></button>
                    </div>
                  </div>
              </div>
            <div class="control-group span3">                          
                <label class="control-label" for="orderDscTxt">    
                  <s:text name="ORDER_DSC_TAG"/>                      
                </label>                                         
                <div class="controls">                           
                  <input type="text" id="orderDscTxt" class="span2"\>
              </div>                                             
            </div>
            <div class="control-group span3">                          
                <label class="control-label" for="orderCateSel">    
                  <s:text name="ORDER_CATE_TAG"/>                      
                </label>                                         
                <div class="controls">                           
                  <select id="orderCateSel" class="span2"></select>
              </div>                                             
            </div>
            <!--TR -->
            <div class="control-group span4">                          
                <label class="control-label" for="plnStbDateTxt">    
                  <s:text name="PLAN_STB_DATE_TAG"/>
                </label>                                         
                <div class="controls">                           
                  <input type="text" id="plnStbDateTxt" class="uneditable-input span2" />
              </div>                                             
            </div>
            <div class="control-group span3">                          
                <label class="control-label" for="plnCmpDateTxt">    
                  <s:text name="PLAN_CMP_DATE_TAG"/>
                </label>                                         
                <div class="controls">                           
                  <input type="text" id="plnCmpDateTxt" class="uneditable-input  span2" />
              </div>                                             
            </div>
            <div class="control-group span3">                          
                <label class="control-label" for="pritySel">    
                  <s:text name="PRIORITY_TAG"/>
                </label>                                         
                <div class="controls">                           
                  <select id="pritySel" class="span2"></select>
              </div>                                             
            </div>
            <!--TR -->
            <div class="control-group span4">                          
                <label class="control-label" for="destShopSel">    
                  <s:text name="DEST_SHOP_TAG"/>
                </label>                                         
                <div class="controls">                           
                  <select id="destShopSel" class="span2"></select>
              </div>                                             
            </div>
            <div class="control-group span3">                          
                <label class="control-label" for="mdlIDSel">    
                  <s:text name="MDL_ID_TAG"/>
                </label>                                         
                <div class="controls">                           
                  <select id="mdlIDSel" class="span2"></select>
              </div>                                             
            </div>
            <div class="control-group span3">                          
                <label class="control-label" for="pnlStbQtyTxt">    
                  <s:text name="PLN_STB_QTY_TAG"/>
                </label>                                         
                <div class="controls">                           
                  <input type="text" id="pnlStbQtyTxt" class="span2" />
              </div>                                             
            </div>
        </form>
      </div>  
    </div>
  </div>
      </div>
    </div>
    <!--Modal body-->
    <div class="modal-footer">
      <a  id="selectBayAreaDialog_cancelBtn"        class="btn" data-dismiss="modal" >
        <s:text name="CANCEL_TAG" />
      </a>
      <a  id="selectBayAreaDialog_selectBayAreaBtn" class="btn btn-primary">
        <s:text name="SELECT_BAYAREA_TAG" />
      </a>
    </div>
    <!--Modal footer--> </div>

  <!--Modal-->
</body>
</html>