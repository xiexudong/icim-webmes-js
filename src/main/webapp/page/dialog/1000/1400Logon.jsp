<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
  <base href="<%=basePath%>">
  <script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
</head>
  <style>
    /*当Modal的width需要自定义时，需要同时设置margin-left和left。
      以保证Modal显示在屏幕中间
    */
      #Dialog_1400Logon {
        margin-left: auto;
        width: 85%;
        left: 7%;
      }
      #boxInfoDiv{
      	width:97%;
      }
    </style>
<body>
  <!--  BayArea dialogs-->
<div class="modal fade" id="Dialog_1400Logon" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">
           WAIT箱号查询并批量入账
        </h3>
      </center>
    </div>
    <!--Modal header-->
    <div class="modal-button" >
      <button id="Dialog_cancel_btn"  class="btn" data-dismiss="modal" >
        <s:text name="CANCEL_TAG" />
      </button>
      <button id="Dialog_move_in_btn" class="btn btn-primary">
        <s:text name="MOVE_IN_TAG" />
      </button>
    </div>
    <!--Modal button--> 
	<div class="modal-body">
		<div class ="row span9">
			<div class ="span4 row">
				<div class="row control-group ">
					<form class="row form-horizontal">
						<label class="control-label" for="1400Logon_boxIdInput"><s:text name="BOX_ID_TAG"/></label>
	                    <div class="controls">
	                    	<input type="text" class="span2" id="1400Logon_boxIdInput">
	                    </div> 
					</form>
				</div>
			</div>
		</div>
	    <div class="span9" id="boxInfoDiv">
	        <table id="boxInfoGrd"></table>
	        <div id="boxInfoPg"></div>
	    </div>  
	</div>
	<!--Modal body-->
</div>
</div>
</div>
<!--Modal-->

</body>
</html>