<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
  <base href="<%=basePath%>">
  <script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
</head>
  <style>
    /*当Modal的width需要自定义时，需要同时设置margin-left和left。
      以保证Modal显示在屏幕中间
    */
      #wip_retain_Dialog {
        margin-left: auto;
        margin-top: auto;
        width: 80%;
        left: 10%;
        height:70%;
      }
      #modalBodyDiv{
      	height:90%;
      }
      #retainBoxInfoDiv,#wipRetainDetailInfoDiv{
      	height:100%;
      }
     /*  #wipRetainInfoGrd,#wipRetainDetailInfoGrd{
      	width:100%;
      	height:100%;
      } */
    </style>
<body>
  <!--  BayArea dialogs-->
	<div id="wip_retain_Dialog" class="modal hide fade dialogMarginCss">
	    <div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    <center>
		        <h3>
		          <s:text name="WIP_RETAIN_CANCEL_TAG"/>
		        </h3>
		    </center>
	    </div>
	    <!--Modal header-->
	    <div class="modal-button" >
		    <button id="Dialog_cancel_btn"  class="btn" data-dismiss="modal" >
		       	关闭
		    </button>
		    <button id="Dialog_cancel_retain_btn" class="btn btn-primary">
		      <s:text name="WIP_RETAIN_CANCEL_TAG" />
		    </button>
		    <input type="checkbox" id="cancel_sc_chk" hidden>
		    <button id="okng_query_btn" class="btn btn-primary">查询</button>
		    <button id="sc_query_btn" class="btn btn-info">SC查询</button>
	    </div>
	    <!--Modal button--> 
		<div class="modal-body" id="modalBodyDiv">
			<div id="buffInfoDiv2" class ="row span10">
				<input type="text" id="boxIdInput" class="span1"  
						placeholder="箱号" style="float:left;width:120px;">
				<div class ="span3 row">
					<div class="row control-group ">
						<form class="row form-horizontal">
						<label class="control-label " for="prdSeqIdInput"><s:text name="PRODUCT_ID_TAG"/></label>
						<div class="controls">
						<input type="text" id="prdSeqIdInput"  class="span2" >
						</div>
						</form>
					</div>
			    </div>
				<div class ="span3 row">
					<div class="row control-group ">
						<form class="row form-horizontal">
							<label class="control-label keyCss" for="lotId2Input"><s:text name="LOT_ID_TAG"/></label>
							<div class="controls">
								<input type="text" id="lotId2Input"  class="span2">
							</div>
						</form>
					</div>
				</div>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				整箱<input type="checkbox" id="wholeFlg">
				<button class="btn btn-primary" id="regist_lot_btn">登记批次号</button>
			</div>
			<div class="span11">
				<div class="span3" id="retainBoxInfoDiv">
					<legend><s:text name="WIP_RETAIN_BOX_TAG"/></legend>
					<table id="wipRetainInfoGrd"></table>
					<div id="wipRetainInfoPg"></div>
				</div>
				<div class="span7" id="wipRetainDetailInfoDiv">
					<legend><s:text name="WIP_RETAIN_PRD_DETAIL_TAG"/></legend>
					<table id="wipRetainDetailInfoGrd"></table>
					<div id="wipRetainDetailInfoPg"></div>
				</div>
			</div>
			
		</div>
		<!--Modal body-->
	</div>
<!--Modal-->

</body>
</html>