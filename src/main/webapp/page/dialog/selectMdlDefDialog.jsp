<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
	<html>
   <head>
      <base href="<%=basePath%>">
    </head>
    <style>
    /*当Modal的width需要自定义时，需要同时设置margin-left和left。
      以保证Modal显示在屏幕中间
    */
      #selectMdlDefDialog {
        margin-left: 10%;
        width: 70%;
        left: 10%;
      }
    </style>
        <body>
          <!--  BayArea dialogs-->
          <div id="selectMdlDefDialog" class="modal hide fade dialogMarginCss">
          	<div class="modal-header">
           		 <button type="button" class="close" data-dismiss="modal">&times;</button>
           		 <center><h3>选择产品ID</h3></center>
            </div>
               <!--Modal header-->
          <div class="modal-body">
         		<div class="row">
                <div>
                  <form id ="orderForm" class="row form-horizontal"> 
                    <div class="control-group span6">
                      <label class="control-label" for="appendedPrependedInput">
                        <i class="icon-search"></i><s:text name="WORDER_ID_TAG"/></label>
                      <div class="controls">
                        <div class="input-append">
                          <input class="span2" id="appendedPrependedInput" size="16" type="text">
                        </div>
                      </div>
                    </div>
                  </form>  
                </div>
          		 	<div id="selectMdlDefDialog_mdlDefDiv"class="span10 ">
            	 			<table id="selectMdlDefDialog_mdlDefGrd"></table>
            	 			<div id="selectMdlDefDialog_mdlDeferPg"></div>
            	  </div>
         		</div>
          </div>
             <!--Modal body-->
          <div class="modal-footer" >
            		<a  id="selectMdlDefDialog_cancelBtn"  class="btn" data-dismiss="modal" >"取消"</a>
            		<a  id="selectMdlDefDialog_selectMdlDefBtn" class="btn btn-primary">"选择"</a>
              </div><!--Modal footer-->
         </div> <!--Modal-->
         
  </body>
</html>
   
