<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
  <html>
   <head>
      <base href="<%=basePath%>">
    </head>
    <body>

<div class="modal fade" id="showErrorDialog" tabindex="-1" role="dialog"
   aria-labelledby="errLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title" id="errLabel"><s:text name="SHOW_ERROR_DIALOG_TAG"/></h4></center>  
    	</div><!--Modal header-->
    	<div class="modal-body">
          <form class="well form-search">

            <div class="control-group error">
              <div class="controls" >
                <button class="btn-danger btn-large disabled ">错误代码</button>
                <input type="text" id="showErrorDialog_inputErrorCodeTxt" class="input-medium search-query ">
              </div>
            </div>
          </form>
          <form class="well form-search">
            <div class="control-group error">
              <div class="controls" >
                <button class="btn-danger btn-large disabled">错误信息</button>
                <input type="text" id="showErrorDialog_inputErrorMsgTxt" class="input-medium search-query span3 ">
              </div>
            </div>

          </form>

    	</div><!--Modal body-->
      <div class="modal-footer">
      		<a  id="showErrorDialog_SureBtn"    class="btn btn-danger"><s:text name="SURE_TAG" /></a>
      </div><!--Modal footer-->
   </div> <!--Modal-->
   
   
   
