<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
	<html>
   <head>
      <base href="<%=basePath%>">
    </head>
    <style>
    /*当Modal的width需要自定义时，需要同时设置margin-left和left。
      以保证Modal显示在屏幕中间
    */
      #selectOpeDialog {
        margin-left: 10%;
        width: 70%;
        left: 10%;
      }
    </style>
        <body>
          <!--  BayArea dialogs-->
          <div id="selectOpeDialog" class="modal hide fade">
          	<div class="modal-header">
           		 <button type="button" class="close" data-dismiss="modal">&times;</button>
           		 <center><h3><span id="titleSpn"><s:text name="SELECT_OPE_TAG"/></span></h3></center>
            </div>
               <!--Modal header-->
          <div class="modal-body">
         		<div class="row">
          		 	<div id="selectOpeDialog_opeDiv"class="span10 ">
            	 			<table id="selectOpeDialog_opeGrd"></table>
            	 			<div id="selectOpeDialog_opePg"></div>
            	  </div>
         		</div>
          </div>
             <!--Modal body-->
          <div class="modal-footer" >
            		<a  id="selectOpeDialog_cancelBtn"  class="btn" data-dismiss="modal" ><s:text name="CANCEL_TAG" /></a>
            		<a  id="selectOpeDialog_selectOpeBtn" class="btn btn-primary"><s:text name="SURE_TAG" /></a>
              </div><!--Modal footer-->
         </div> <!--Modal-->
         
  </body>
</html>
   
