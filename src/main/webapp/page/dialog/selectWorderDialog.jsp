<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
	<html>
   <head>
      <base href="<%=basePath%>">
    </head>
    <style>
    /*当Modal的width需要自定义时，需要同时设置margin-left和left。
      以保证Modal显示在屏幕中间
    */
      #selectWorderDialog {
        margin-left: 10%;
        width: 70%;
        left: 10%;
      }
    </style>
        <body>
          <!--  BayArea dialogs-->
          <div id="selectWorderDialog" class="modal hide fade">
          	<div class="modal-header">
           		 <button type="button" class="close" data-dismiss="modal">&times;</button>
           		 <center><h3><span id="titleSpn"><s:text name="SELECT_WORDER_TAG"/></span></h3></center>
            </div>
               <!--Modal header-->
          <div class="modal-body">
         		<div class="row">
          		 	<div id="selectWorderDialog_worderDiv"class="span10 ">
            	 			<table id="selectWorderDialog_worderGrd"></table>
            	 			<div id="selectWorderDialog_worderPg"></div>
            	  </div>
         		</div>
          </div>
             <!--Modal body-->
          <div class="modal-footer" >
            		<a  id="selectWorderDialog_cancelBtn"  class="btn" data-dismiss="modal" ><s:text name="CANCEL_TAG" /></a>
            		<a  id="selectWorderDialog_selectWorderBtn" class="btn btn-primary"><s:text name="SELECT_BAYAREA_TAG" /></a>
              </div><!--Modal footer-->
         </div> <!--Modal-->
         
  </body>
</html>
   
