<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
	<html>
   <head>
      <base href="<%=basePath%>">
    </head>
    <style>
    /*当Modal的width需要自定义时，需要同时设置margin-left和left。
      以保证Modal显示在屏幕中间
    */
      #comKeyBoardDialog {
        margin-left: 10%;
        width: 30%;
        left: 10%;
      }
    </style>
        <body>
          <!--  BayArea dialogs-->
          <div id="comKeyBoardDialog" class="modal hide fade dialogMarginCss">
          	<div class="modal-header">
           		 <button type="button" class="close" data-dismiss="modal">&times;</button>
           		 <!-- <center><h3><s:text name="SELECT_WORDER_TAG"/></h3></center> -->
               <center><h3>模拟键盘</h3></center>
            </div>
               <!--Modal header-->
          <div class="modal-body">
         		<div id="comKeyBoardDiv" class="row span3">

         		</div>
          </div>
             <!--Modal body-->
          <div class="modal-footer" >
            		<a  id="comKeyBoardDialog_cancelBtn"  class="btn" data-dismiss="modal" >
                  <!-- <s:text name="CANCEL_TAG" /> -->
                  取消
                </a>
            		<a  id="comKeyBoardDialog_keyInBtn" class="btn btn-primary">
                  <!-- <s:text name="SELECT_BAYAREA_TAG" /> -->
                  输入
                </a>
              </div><!--Modal footer-->
         </div> <!--Modal-->
         
  </body>
</html>
   
