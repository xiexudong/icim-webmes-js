<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true" isELIgnored="false"%>
<%@ page import="com.model.UserPO"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">

<title>iMES</title>
<%@ include file="/page/comPage/comCSS.html"%>
<link href="css/page/main.css" rel="stylesheet">
<%@ include file="/page/comPage/navbar.html"%>
</head>

<body>

	<%
		UserPO userPo = (UserPO) session.getAttribute("userInfo");
	%>

	<div class="navbar navbar-default"  role="navigation">
			<div class="container-fuid">
				<a class="brand" href="#"></a>
				<div class=" collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">首页</a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_1000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=1100" target="_blank">
										<s:text name="M_1100_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=1200" target="_blank">
										<s:text name="M_1200_TITLE_TAG" />
								</a></li>
							<%-- 	<li><a href="com/goto.action?opeID=7200" target="_blank">
										<s:text name="M_1300_TITLE_TAG" />
								</a></li> --%>
								<li><a href="com/goto.action?opeID=1400" target="_blank">
										<s:text name="M_1400_TITLE_TAG" />
								</a></li>
								
	           <%--    	<li><a href="com/goto.action?opeID=1600" target="_blank">
	              			<s:text name="M_1600_TITLE_TAG"/>
	              	</a></li> --%>
	              	
								<li><a href="com/goto.action?opeID=1700" target="_blank">
										<s:text name="M_1700_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=1800" target="_blank">
										<s:text name="M_1800_TITLE_TAG" />
								</a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_2000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=2100" target="_blank">
										<s:text name="M_2100_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=2300" target="_blank">
										<s:text name="M_2300_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=2500" target="_blank">
										<s:text name="M_2500_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=2600" target="_blank">
										<s:text name="M_2600_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=2700" target="_blank">
										<s:text name="M_2700_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=2800" target="_blank">
										<s:text name="M_2800_TITLE_TAG" />
								</a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_3000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=3100" target="_blank">
										<s:text name="M_3100_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=3200" target="_blank">
										<s:text name="M_3200_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=3300" target="_blank">
										<s:text name="M_3300_TITLE_TAG" />
								</a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_4000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=4100" target="_blank"><s:text
											name="M_4100_TITLE_TAG" /></a></li>
								<li><a href="com/goto.action?opeID=4200" target="_blank"><s:text
											name="M_4200_TITLE_TAG" /></a></li>
								<li><a href="com/goto.action?opeID=4300" target="_blank"><s:text
											name="M_4300_TITLE_TAG" /></a></li>
								<li><a href="com/goto.action?opeID=4400" target="_blank"><s:text
											name="M_4400_TITLE_TAG" /></a></li>
								<li><a href="com/goto.action?opeID=4500" target="_blank"><s:text
											name="M_4500_TITLE_TAG" /></a></li>
								<li><a href="com/goto.action?opeID=4A00" target="_blank"><s:text
											name="M_4A00_TITLE_TAG" /></a></li>
								<li><a href="com/goto.action?opeID=4B00" target="_blank"><s:text
											name="M_4B00_TITLE_TAG" /></a></li>
								<li><a href="com/goto.action?opeID=4C00" target="_blank"><s:text
											name="M_4C00_TITLE_TAG" /></a></li>
							<%-- 	<li><a href="com/goto.action?opeID=4D00" target="_blank"><s:text
											name="M_4D00_TITLE_TAG" /></a></li> --%>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_5000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=5100" target="_blank">5100:订单管理</a></li>
								<li><a href="com/goto.action?opeID=5200" target="_blank">5200:工单管理</a></li>
								<li><a href="com/goto.action?opeID=5400" target="_blank"><s:text
											name="M_5400_TAG" /></a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_6000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=6100" target="_blank">
										<s:text name="M6100_BIS_PATH_MANAGEMENT_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6200" target="_blank">
										<s:text name="M6200_LAYOUT_MANAGEMENT_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6300" target="_blank">
										<s:text name="M_6300_BIS_DATA_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6400" target="_blank">
										<s:text name="M_6400_BIS_MDL_DEF_MANAGEMENT_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6500" target="_blank">
										<s:text name="M_6500_OPE_MANAGEMENT_TAG" />
								</a></li>
								<!-- <li><a href="com/goto.action?opeID=6600" target="_blank">
	                	<s:text name="M_6600_BIS_QRS_MANAGEMENT_TAG"/>
	                	</a></li> -->
								<li><a href="com/goto.action?opeID=6700" target="_blank">
										<s:text name="M_6700_BIS_MTRL_MANAGEMENT_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6800" target="_blank">
										<s:text name="M6800_TOOL_MANAGEMENT_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6900" target="_blank">
										<s:text name="M6900_PARAM_MANAGEMENT_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6A00" target="_blank">
										<s:text name="M_6A00_MEASUREMENT_MANAGEMENT_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6B00" target="_blank">
										<s:text name="M_6B00_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6C00" target="_blank">
										<s:text name="M_6C00_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=6D00" target="_blank">
										<s:text name="M_6D00_TITLE_TAG" />
								</a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_7000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=7100" target="_blank">
										<s:text name="M_7100_SPC_CHART_SETTING_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=7200" target="_blank">
										<s:text name="M_7200_REPORT_SPC_TAG" />
								</a></li>
								<li><a id="spcPage" href="#" target="_blank"> <s:text
											name="M_7300_SPC_SHOW_CHART_TAG" />
								</a></li>
								<%--<li><a href="com/goto.action?opeID=7300" target="_blank"> <s:text
										name="M_7300_SPC_SHOW_CHART_TAG" />
								</a></li>--%>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_8000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=8100" target="_blank">8100</a></li>
								<!--  <li><a href="#">8200</a></li> 
	                <li><a href="#">8300</a></li>  -->
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_9000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=9100" target="_blank">
										<s:text name="M_9100_TITLE_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9200" target="_blank">
										<s:text name="M_9200_TITLE_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9300" target="_blank">
										<s:text name="M_9300_PNL_INFO_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9400" target="_blank">
										<s:text name="M_9400_TITLE_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9500" target="_blank">
										<s:text name="M_9500_QUERY_WO_INFO_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9600" target="_blank">
										<s:text name="M_9600_QUERY_SO_INFO_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9700" target="_blank">
										<s:text name="M_9700_QUERY_MDL_INFO_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9800" target="_blank">
										<s:text name="M_9800_QUERY_SO_INFO_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9A00" target="_blank">
										<s:text name="M_9A00_QUERY_DEFECT_TITLE_TAG" /><b
										class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9B00" target="_blank">
										<s:text name="M_9B00_QUERY_DN_INFO_TAG" /><b class="caret"></b>
								</a></li>
								<li><a href="com/goto.action?opeID=9C00" target="_blank">
										<s:text name="M_9C00_QUERY_OPE_INFO_TAG" /><b class="caret"></b>
								</a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_A000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a href="com/goto.action?opeID=A100" target="_blank">
										<s:text name="M_A100_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=A200" target="_blank">
										<s:text name="M_A200_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=A300" target="_blank">
										<s:text name="M_A300_TITLE_TAG" />
								</a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <s:text name="M_C000_TITLE_TAG" /><b
								class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<%-- <li><a href="com/goto.action?opeID=C100" target="_blank">
										<s:text name="M_C100_TITLE_TAG" />
								</a></li>--%>
								<li><a href="com/goto.action?opeID=C200" target="_blank">
										<s:text name="M_C200_TITLE_TAG" />
								</a></li>
								<%-- <li><a href="com/goto.action?opeID=C300" target="_blank">
										<s:text name="M_C300_TITLE_TAG" />
								</a></li> --%>
								<li><a href="com/goto.action?opeID=C400" target="_blank">
										<s:text name="M_C400_TITLE_TAG" />
								</a></li>
								<li><a href="com/goto.action?opeID=C500" target="_blank">
										<s:text name="C500:栈板标签补印" />
								</a></li>
							</ul></li>
					</ul>
					<!-- <form class="navbar-search pull-left" action="">
	            <input type="text" class="search-query span1" placeholder="搜索">
	          </form>
	          <ul class="nav pull-right">
	            <li><a href="#">链接</a></li> 
	          </ul> -->
				</div>
				<!-- /.nav-collapse -->
			</div>
		<!-- /navbar-inner -->
	</div>
	<!-- end of navbar -->
	<div class="row-fluid">
		<div class="col-lg-12">
			<fieldset>
				<legend>Info</legend>
				<div class="row">
					<textarea id="mainInfoTxt" class="col-lg-10 col-lg-offset-1 h160"></textarea>
					<button id="updateAnnounce1Btn"
						class="btn btn-primary btn-large col-lg-offset-10">
						<s:text name="UPDATE_ANNOUNCEMENT" />
					</button>
				</div>
			</fieldset>
		</div>
	</div>
	<div class="row-fluid">
		<div class="col-lg-6">
			<fieldset>
				<legend>Left</legend>
				<textarea id="leftInfoTxt" class="col-lg-10 col-lg-offset-1 h160"></textarea>
				<button id="updateAnnounce2Btn"
					class="btn btn-primary btn-large col-lg-offset-9">
					<s:text name="UPDATE_ANNOUNCEMENT" />
				</button>
			</fieldset>
		</div>
		<div class="col-lg-6">
			<fieldset>
				<legend>Right</legend>
				<textarea id="rightInfoTxt" class="col-lg-10 col-lg-offset-1 h160"></textarea>
				<button id="updateAnnounce3Btn"
					class="btn btn-primary btn-large col-lg-offset-9">
					<s:text name="UPDATE_ANNOUNCEMENT" />
				</button>
			</fieldset>
		</div>
	</div>
	
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/main.js"></script>
</body>
</html>
