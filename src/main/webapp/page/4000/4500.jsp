<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_4500_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<style type="text/css">
#ppboxInfoGrdDiv {
	width: 96%;
}
.form-group {
	margin-bottom: 0px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="container-fluid">
			<button class="btn btn-primary" id="f1_btn">F1查询</button>
			<button class="btn btn-primary" id="f2_btn">
				F2
				<s:text name="F2_CLEAR_TAG" />
			</button>
			<button class="btn btn-danger" id="f4_btn">
				F3
				<s:text name="DELETE_TAG" />
			</button>
			<button class="btn btn-primary" id="f5_btn">
				F4
				<s:text name="ADD_TAG" />
			</button>
			<button class="btn btn-primary" id="f8_btn">
				F5
				<s:text name="REGIST_TAG" />
			</button>
			<button class="btn btn-primary" id="f10_btn">
				F6
				<s:text name="待收货箱" />
			</button>
			<button class="btn btn-primary" id="f9_btn">
				F7
				<s:text name="收货" />
			</button>
		</div>
		<div class="container-fluid">
			<fieldset>
				<legend></legend>
				<div class="row">
					<form class="form-horizontal col-sm-5">
						<div class="form-group"><label class="col-xs-5 control-label" for="vdrIDSelect"><s:text name="CUS_ID_TAG" /></label>
						<div class="col-xs-5">
							<select id="vdrIDSelect" class="form-control"></select>
						</div>
						</div><div class="form-group"><label class="col-xs-5 control-label" for="mdlIdSel">产品型号</label>
						<div class="col-xs-5">
							<select id="mdlIdSel" class="form-control"></select>
						</div>
						</div><div class="form-group"><label class="col-xs-5 control-label" for="destShopCate">仓别种类</label>
						<div class="col-xs-5">
							<select id="destShopCate" class="form-control"></select>
						</div>
						</div><div class="form-group"><label class="col-xs-5 control-label" for="fromDestShopSelect">当前仓别</label>
						<div class="col-xs-5">
							<select id="fromDestShopSelect" class="form-control"></select>
						</div>
						</div><div class="form-group"><label class="col-xs-5 control-label" for="woIdSelect"><s:text name="WORDER_ID_TAG" /></label>
						<div class="col-xs-5">
							<select id="woIdSelect" class="form-control"></select>
						</div>
						</div>
					</form>
					<div class="col-sm-6">
						<form class="form-horizontal">
							<div class="form-group"><label class="col-xs-5 control-label keyCss" for="ppBoxIDInput"><s:text name="BOX_ID_TAG" /></label>
							<div class="col-xs-5">
								<input id="ppBoxIDInput" class="form-control" type="text">
							</div>
							</div><div class="form-group"><label class="col-xs-5 control-label keyCss" for="shipboxInput">出货箱号</label>
							<div class="col-xs-5">
								<input id="shipboxInput" class="form-control" type="text">
							</div>
							</div><div class="form-group"><label class="col-xs-5 control-label keyCss" for="destShopSelect"><s:text name="TARGET_DEST" /></label>
							<div class="col-xs-5">
								<select id="destShopSelect" class="form-control"></select>
							</div>
</div>
						</form>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
	<div class="container-fluid">
		<fieldset>
			<legend>
				<s:text name="PPBOX_INFO" />
			</legend>
		</fieldset>
	</div>
	<div class="container-fluid" id="ppboxInfoGrdDiv">
		<table id="ppboxInfoGrd"></table>
		<div id="ppboxInfoPg"></div>
	</div>


	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/4000/4500.js"></script>
	<script type="text/javascript" src="js/com/DestDesc.js"></script>
	<script src="js/com/SelectDom.js"></script>
</body>
</html>
