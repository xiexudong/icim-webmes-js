<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_4400_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<link href="lib/HubSpot-messenger/build/css/messenger-theme-future.css" rel="stylesheet" media="screen" />
<link href="lib/HubSpot-messenger/build/css/messenger.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
		var basePath = "<%=basePath%>";
</script>
<style type="text/css">
.keyCss:after {
	color: #FF0000;
	content: "*";
	float: none;
}

.mln80 {
	margin-left: -80px;
}

#ppboxInfoGrdDiv {
	width: 97%;
}

.form-group {
	margin-bottom: 0px;
}
</style>

</head>
<body>
	<div class="container-fluid">
		<div class="container-fluid">
			<button class="btn btn-primary" id="query_btn">
				F1
				<s:text name="QUERY_BTN_TAG" />
			</button>
			<button class="btn btn-primary" id="clear_btn">
				F2
				<s:text name="F2_CLEAR_TAG" />
			</button>
			<button class="btn btn-primary" id="wh_out_btn">
				F3
				<s:text name="WH_OUT" />
			</button>
			<button class="btn btn-primary" id="del_wo_btn">
				F4
				<s:text name="F4_DELETE_TAG" />
			</button>
			<button class="btn btn-primary" id="pack_btn">F5打包</button>
			<button class="btn btn-primary" id="unpack_btn">F6取消打包</button>
			<button class="btn btn-primary" id="exportCsotShippingDataBtn">补印华星光电栈板出货资料</button>
			<button class="btn btn-primary" id="exportXmtmShippingDataBtn">补印厦门天马栈板出货资料</button>
			<!-- <button class="btn btn-primary" id="testBtn">补印栈板出货资料</button> -->
		</div>

		<div class="container-fluid">
			<div class="col-sm-3">
				<fieldset>
					<legend>
						<s:text name="QUERY_CONDITION_TAG" />
					</legend>
					<form class="form-horizontal">
<%-- 						<div class="form-group radio">
							<div class="col-xs-4"></div>
							<div class="col-xs-8">
								<label class="control-label" for="woPriorityRadio"><input type="radio" name="dnwoPriorityRadios" id="dnPriorityRadio" value="dn" checked> 
								<s:text name="DN_PRIORITY_TAG" /></label> 
								<label class="control-label" for="woPriorityRadio"><input type="radio" name="dnwoPriorityRadios" id="woPriorityRadio" value="wo">
								<s:text name="WO_PRIORITY_TAG" /></label>
							</div>
						</div> --%>
						<div class="form-group">
							<label class="col-xs-5 control-label keyCss" for="vdrIDSelect"><s:text name="CUS_ID_TAG" /></label>
							<div class="col-xs-7">
								<select id="vdrIDSelect" class="form-control"></select>
							</div>
							<!-- <div id="soSIDDiv"></div> -->
						</div>
<%-- 						<div class="form-group">
							<label class="col-xs-4 control-label" for="soSIdSelect">商务订单号</label>
							<div class="col-xs-8">
								<select id="soSIdSelect" class="form-control"></select>
							</div>
						</div> --%>
						<div class="form-group">
							<label class="col-xs-5 control-label keyCss" for="DNNoSelect"><s:text name="DELIVERY_ORDER_ID_TAG" /></label>
							<div class="col-xs-7">
								<select id="DNNoSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-5 control-label keyCss" for="woIdSelect"><s:text name="WORDER_ID_TAG" /></label>
							<div class="col-xs-7">
								<select id="woIdSelect" class="form-control"></select>
								<button class="btn btn-primary" id="addWoBtn">添加内部订单</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-5 control-label" for="destShopSelect"><s:text name="DEST_SHOP_TAG" /></label>
							<div class="col-xs-7">
								<select id="destShopSelect" class="form-control"></select>
							</div>
						</div>
					</form>
				</fieldset>
			</div>
			<div class="col-sm-3">
				<fieldset>
					<legend>
						<s:text name="WH_OUT_INFO_INPUT" />
					</legend>
					<form class="form-horizontal " id="formDiv">
						<div class="form-group" id="sohideDiv">
							<label class="col-xs-4 control-label" for="DNNoSp">订单号</label>
							<div class="col-xs-8">
								<input id="DNNoSp" type="text" class="uneditable-input form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label keyCss" for="ppBoxIDInput"><s:text name="BOX_ID_TAG" /></label>
							<div class="col-xs-8">
								<input id="ppBoxIDInput" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label keyCss" for="shipboxInput"><s:text name="出货箱号" /></label>
							<div class="col-xs-8">
								<input id="shipboxInput" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label keyCss" for="palletIDInput"><s:text name="PALLET_ID_TAG" /></label>
							<div class="col-xs-8">
								<input id="palletIDInput" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label keyCss" for="phypalletIDInput">物理栈板号</label>
							<div class="col-xs-8">
								<input id="phypalletIDInput" type="text" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label keyCss" for="trxUserInput">承运人</label>
							<div class="col-xs-8">
								<%--<select id="trxUserInput" class="form-control"></select>--%>
								<input id="trxUserInput" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label keyCss" for="palteNOInput"><s:text name="PLATE_NUMBER" /></label>
							<div class="col-xs-8">
								<%--<select id="palteNOInput" class="form-control"></select>--%>
								<input id="palteNOInput" class="form-control"/>
							</div>
						</div>
 						<div class="form-group">
							<label class="col-xs-4 control-label keyCss" for="palteNOInput">打印机</label>
							<div class="col-xs-8">
								<select id="print1Select" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="remarkInput"><s:text name="REMARK_TAG" /></label>
							<div class="col-xs-8">
								<textarea id="remarkInput" rows="1" class="form-control"></textarea>
							</div>
						</div>
					</form>
				</fieldset>
			</div>
			<div class="col-sm-6">
				<fieldset>
					<legend>
						<s:text name="WORDER_DETAIL_TAG" />
					</legend>

					<div id="woInfoGrdDiv">
						<table id="woInfoGrd"></table>
						<div id="woInfoPg"></div>
					</div>
<%-- 					<div>
						<form class="form-horizontal " id="formDiv1" action="dummy.action">
							<div class="form-group">
								<label class="col-xs-2 control-label" for="palletIDInput"><s:text name="PALLET_ID_TAG" /></label>
								<div class="col-xs-4">
									<input id="palletIDInput" type="text" class="form-control">
								</div>
						 		<div class="col-xs-2">
									<button class="btn btn-primary" id="autobtn">自动生成</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-2 control-label keyCss" for="trxUserInput">物理栈板号</label>
								<div class="col-xs-4">
									<input id="phypalletIDInput" type="text" class="form-control" />
								</div>
							</div>
 							<div class="form-group">
								<label class="col-xs-2 control-label keyCss" for="palletInput">出货栈板</label>
								<div class="col-xs-4">
									<input id="palletInput" type="text" class="form-control">
								</div>
							</div>
						</form>
					</div> --%>
				</fieldset>
			</div>
		</div>
		<div id="ppboxInfoGrdDiv" class="col-sm-12">
			<table id="ppboxInfoGrd"></table>
			<div id="ppboxInfoPg"></div>
		</div>
	</div>

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script type="text/javascript" src="js/com/DestListTool.js"></script>
	<script src="lib/HubSpot-messenger/build/js/messenger.min.js"></script>
	<script src="js/dialog/showMessenger.js"></script>
	<script src="js/page/4000/4400.js"></script>
 	<script type="text/javascript" src="js/com//printl.js"></script>
 	<applet alt="Printers" name="Printers" mayscript="mayscript" codebase="." code="com.print.PrinterApplet.class"
		archive="<%=basePath%>jar/print.jar, <%=basePath%>jar/barcode4j.jar, <%=basePath%>jar/jackson-all-1.9.11.jar" width="0" height="0"></applet>
	<script type="text/javascript">
		try {
			var applet = document.Printers;
			var printers = applet.findAllPrinters();
			printers = eval(printers);
			console.log(printers);
		} catch (ex) {
			console.info(ex);
		}
	</script>
</body>
</html>
