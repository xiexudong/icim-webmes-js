<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_4100_TITLE_TAG" /></title>
<script type="text/javascript">
		var basePath = "<%=basePath%>";
</script>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<style>
#bindWODialog {
	margin-left: 2%;
	width: 90%;
	left: 4%;
}

#cusPrdDialog {
	margin-left: 2%;
	width: 90%;
	left: 4%;
}

.color {
	color: red;
}

#panelMaterialDeliveryInfoDiv {
	margin-top: 10px;
}
</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<button class="btn btn-primary" id="f1_btn">
				F1
				<s:text name="QUERY_BTN_TAG" />
			</button>
			<button class="btn btn-danger" id="f4_btn">
				F2
				<s:text name="DELETE_TAG" />
			</button>
			<button class="btn btn-warning" id="f5_btn">
				F3
				<s:text name="UPDATE_TAG" />
			</button>
			<button class="btn btn-primary" id="f8_btn">
				F4
				<s:text name="REGIST_TAG" />
			</button>
			<button class="btn btn-primary " id="f9_btn">
				<s:text name="CANCEL_TAG" />
				<s:text name="UPDATE_TAG" />
			</button>
			<button class="btn btn-primary" id="f11_bind_btn">F5:绑定商务订单</button>
			<button class="btn btn-primary" id="importCusPrdBtn">
				导入武汉华星资料
			</button>
			<button class="btn btn-primary" id="importTmCusPrdBtn">
				导入厦门天马资料
			</button>
			<!-- <button class="btn btn-primary" id="print_btn">
				<s:text name="打印" />
			</button>
			<button class="btn btn-primary" id="printNew_btn">
				<s:text name="新打印" />
			</button>
			<button class="btn btn-primary" id="importCusPrdBtn">
				<s:text name="IMPORT_CUS_PRD" />
			</button>-->
		</div>

		<fieldset>
			<legend>
				<s:text name="MTRL_PANEL_INFO" />
			</legend>

			<div class="col-lg-4">
				<form class="form-horizontal">
					<label class="col-sm-5 control-label input-sm keyCss" for="vdrIDSelect"><s:text name="CUS_ID_TAG" /></label>
					<div class="col-sm-7">
						<select id="vdrIDSelect" class="form-control input-sm"></select>
					</div>
					<label class="col-sm-5 control-label input-sm keyCss" for="mtrlTypeSelect"><s:text name="MTRL_TYPE" /></label>
					<div class="col-sm-7">
						<select id="mtrlTypeSelect" class="form-control input-sm"></select>
						<button class="btn hide" id="addNewMtrlTypeBtn">
							<s:text name="ADD_TAG" />
						</button>
					</div>
					<label class="col-sm-5 control-label input-sm keyCss" for="destShopSelect"><s:text name="DEST_SHOP_TAG" /></label>
					<div class="col-sm-7">
						<select id="destShopSelect" class="form-control input-sm"></select>
					</div>
					<label class="col-sm-5 control-label input-sm keyCss" for="woIDInput"><s:text name="WORDER_ID_TAG" /></label>
					<div class="col-sm-7">
						<select id="woIdSelect" class="form-control input-sm"></select>
					</div>
					<label class="col-sm-5 control-label" for="woCountInput">内部订单数量</label>
					<div class="col-sm-7">
						<input id="woCountInput" type="text" class="disabled form-control input-sm" disabled>
					</div>
					<label class="col-sm-5 control-label" for="stdThicknessTxt">标准<s:text name="TO_THICKNESS_TAG" /></label>
					<div class="col-sm-7">
						<input type="text" id="stdThicknessTxt" class="disabled form-control input-sm" disabled>
					</div>
					<label class="col-sm-5 control-label" for="getCountInput">商务订单数量</label>
					<div class="col-sm-7">
						<input id="getCountInput" type="text" class="disabled form-control input-sm" disabled>
					</div>
					<label class="col-sm-5 control-label" for="mtrlBoxInput">来料原箱号</label>
					<div class="col-sm-7">
						<input id="mtrlBoxInput" type="text" class="form-control input-sm" />
					</div>
					<label class="col-sm-5 control-label input-sm keyCss" for="rawBoxIDInput"><s:text name="PPBOX_ID_TAG" /></label>
					<div class="col-sm-7">
						<input id="rawBoxIDInput" type="text" class="form-control input-sm">
						<%--<button class="btn btn-primary" id="autoCreateBoxBtn"><s:text name="AUTO_GENERATOR_TAG"/></button> 
                        --%>
					</div>
					<label class="col-sm-5 control-label input-sm keyCss" for="rawBoxStdQtyInput"><s:text name="BOX_STD_QTY_TAG" /></label>
					<div class="col-sm-7">
						<input type="text" id="rawBoxStdQtyInput" class="form-control input-sm">
					</div>
					<label class="col-sm-5 control-label" for="rawWeightInput"><s:text name="RAW_WEIGHT_TAG" /></label>
					<div class="col-sm-7">
						<input id="rawWeightInput" type="text" class="form-control input-sm">
					</div>
					<label class="col-sm-5 control-label input-sm keyCss" for="rawCountInput"><s:text name="QTY_TAG" /></label>
					<div class="col-sm-7">
						<input type="text" id="rawCountInput" class="form-control input-sm">
					</div>
					<label class="col-sm-5 control-label" for="thicknessTxt"><s:text name="TO_THICKNESS_TAG" /></label>
					<div class="col-sm-7">
						<input type="text" id="thicknessTxt" class="form-control input-sm">
					</div>
					<!-- <label class="col-sm-4 control-label" for="customerSupplyCountInput"><s:text name="CUSTOM_QTY_TAG"/></label>
                        <div class="col-sm-8">
                          <input  id="customerSupplyCountInput" type="text" class="disabled" disabled>
                        </div> 
					<label class="col-sm-4 control-label color" for="woBoxTotalCntInput"><s:text name="WO_BOX_COUNT_TAG" /></label>
					<div class="col-sm-8">
						<input id="woBoxTotalCntInput" type="text" class="form-control input-sm">
					</div>
					<label class="col-sm-4 control-label color" for="prtBoxRuleInput"><s:text name="PRINT_BOX_RULE_TAG" /></label>
					<div class="col-sm-8">
						<input id="prtBoxRuleInput" type="text" class="form-control input-sm">
					</div>
					<label class="col-sm-4 control-label color" for="mtrlIdInput">料号</label>
					<div class="col-sm-8">
						<input id="mtrlIdInput" type="text" class="form-control input-sm">
					</div>
					<label class="col-sm-4 control-label color" for="hdIdInput">手册号</label>
					<div class="col-sm-8">
						<input id="hdIdInput" type="text" class="form-control input-sm">
					</div>-->
				</form>
			</div>
			<div class="col-lg-8" id="panelMaterialDeliveryInfoDiv">
				<table id="panelMaterialDeliveryInfoGrd"></table>
				<div id="panelMaterialDeliveryInfoPg"></div>
			</div>
		</fieldset>
	</div>

	<div id="bindWODialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog" style="width:1200px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">绑定商务订单</h4>
				</div>
				<div class="modal-body" id="bindWoDialogDiv">
					<div class="row">
						<form id="orderForm" class="form-horizontal">
							<div class="control-group">
								<label class="col-sm-4 control-label" for="bindWODialog_worderIDTxt"> <s:text name="WORDER_ID_TAG" />
								</label>
								<div class="col-sm-4">
									<input id="bindWODialog_worderIDTxt" class="form-control input-sm uneditable-input" type="text">
								</div>
							</div>
						</form>
					</div>

					<div id="bindWoDiv" class="row">
						<table id="bindWoDialog_woGrd"></table>
						<div id="bindWoDialog_woGrdPg"></div>
					</div>
				</div>
				<div class="modal-footer">
					<a id="bindWODialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="bindWODialog_bindWOBtn" class="btn btn-primary"> <s:text name="F11_BIND_WO_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>

	<div id="cusPrdDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog" style="width: 700px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">客户来料信息</h4>
				</div>
				<div class="modal-body" id="cusPrdDialogDiv">
					<div class="container-fluid">
					<div id="cusPrdDiv" class="col-lg-12 col-md-12 col-sm-12">
						<table id="cusPrdGrd"></table>
						<div id="cusPrdGrdPg"></div>
					</div>
					</div>
				</div>
				<div class="modal-footer">
					<a id="cusPrdDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="cusPrdDialog_selectPrdBtn" class="btn btn-primary"> 选择来料箱 </a>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div class="modal fade" id="tmCusPrdDialog"  tabindex="-1" role="dialog" aria-hidden="true">
	  	 <div class="modal-dialog" style="width:700px">
	      	<div class="modal-content">
			         <div class="modal-header">
			            <button type="button" class="close"
			               data-dismiss="modal" aria-hidden="true">
			                  &times;
			            </button>
			            <center><h4 class="modal-title">客户来料信息</h4></center>
			      	</div>
			      <div class="modal-body" id="bindWoDialogDiv" style="height:400px">
			      	  <div id="cusPrdDiv" class ="col-lg-10">
						      <table id="tmCusPrdGrd"></table>
						      <div id="tmCusPrdGrdPg"></div>
			      	  </div>
				      
			      </div>
			      <div class="modal-footer" >
				      <a  id="tmCusPrdDialog_cancelBtn"  class="btn" data-dismiss="modal" >
				      	  <s:text name="CANCEL_TAG" />
				      </a>
				      <a  id="tmCusPrdDialog_selectPrdBtn" class="btn btn-primary">
				      	   选择来料箱
				      </a>
			      </div>
	      	</div>
	      </div>
     </div>
	
<%-- 	 <div id="tmCusPrdDialog" class="modal hide fade">
	      <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal">&times;</button>
		      <center><h3>客户来料信息</h3></center>
	      </div>
	      <div class="modal-body" id="tmCusPrdDialogDiv">
		      <div id="cusPrdDiv">
			      <table id="tmCusPrdGrd"></table>
			      <div id="tmCusPrdGrdPg"></div>
		      </div>  
	      </div>
	      <div class="modal-footer" >
		      <a  id="tmCusPrdDialog_cancelBtn"  class="btn" data-dismiss="modal" >
		      	  <s:text name="CANCEL_TAG" />
		      </a>
		      <a  id="tmCusPrdDialog_selectPrdBtn" class="btn btn-primary">
		      	  选择来料箱
		      </a>
	      </div>
     </div> --%>
	<!-- Le javascript
    ================================================== -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/4000/4100.js"></script>
	<%-- <script type="text/javascript" src="<%=basePath%>js/com/printl.js"></script> --%>
	<script type="text/javascript" src="js/com/DestDesc.js"></script>
</body>
</html>
