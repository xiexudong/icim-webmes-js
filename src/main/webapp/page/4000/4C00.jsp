<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_4C00_TITLE_TAG" /></title>
<script type="text/javascript">
var basePath = "<%=basePath%>";
</script>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<style>
.form-group {
	margin-bottom: 0px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="container-fluid">
			<button class="btn btn-primary" id="query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="rtn_btn">
				<s:text name="WH_RETURN_TO_LINE_TAG" />
			</button>
		</div>
		<div class="container-fluid">
			<div class="col-sm-5" id="boxDiv">
				<fieldset>
					<legend>
						<i class="glyphicon glyphicon-th"></i>
						<s:text name="PLEASE_INOUT_BOX_TAG" />
					</legend>
				</fieldset>
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-xs-5 control-label" for="boxIDTxt"><s:text name="BOX_ID_TAG" /></label>
						<div class="col-xs-6">
							<input id="boxIDTxt" class="form-control" type="text">
						</div>
					</div>
<!-- 					<div class="form-group">
						<label class="col-xs-5 control-label" for="shipboxInput">出货箱号</label>
						<div class="col-xs-6">
							<input id="shipboxInput" class="form-control" type="text">
						</div>
					</div> -->
					<div class="form-group">
						<label class="col-xs-5 control-label" for="woIdSelect"><s:text name="WORDER_ID_TAG" /></label>
						<div class="col-xs-6">
							<select id="woIdSelect" class="form-control"></select>
						</div>
					</div>
				</form>


			</div>
			<div class="col-sm-5" id="opeDiv">
				<fieldset>
					<legend>
						<i class="glyphicon glyphicon-th"></i> 选择
						<s:text name="RETURN_OPERATION_TAG" />
					</legend>
				</fieldset>
				<form class="form-horizontal">
					<div class="form-group">
						<label class="control-label hide" for="pathIDSel"><s:text name="PATH_ID_TAG" /></label>
						<div class="col-xs-4 hide">
							<select id="pathIDSel" class="form-control"></select>
						</div>
					</div>
					<div class="form-inline">
						<input type="checkbox" id="bx1" name="cpkexcl1" class="form-control">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<!-- </div><div class="form-group"><label class="col-xs-3 control-label" for="opeSel"> -->
						<s:text name="OPE_ID_TAG" />
						<!-- <div class="col-xs-4"> -->
						<select id="opeSel" class="form-control" class="form-control" style="width: 180px;"></select>
					</div>
					<div class="form-inline">
						<input type="checkbox" id="bx2" name="cpkexcl1" class="form-control">&nbsp;
						<!-- </div><div class="form-group"><label class="col-xs-3 control-label" for="newWoIdSelect"> -->
						新
						<s:text name="WORDER_ID_TAG" />
						<!-- <div class="col-xs-4"> -->
						<select id="newWoIdSelect" class="form-control" style="width: 180px;"></select>
					</div>
				</form>
			</div>
		</div>

	</div>

	<div class="container-fluid">
		<fieldset>
			<legend>
				<i class="glyphicon glyphicon-th"></i>
				<s:text name="PNL_INFO_TAG" />
			</legend>
		</fieldset>
	</div>
	<div id="shtListDiv" class="container-fluid">
		<table id="shtListGrd"></table>
		<div id="shtListPg"></div>
	</div>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/4000/4C00.js"></script>
</body>
</html>