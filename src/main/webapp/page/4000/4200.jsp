<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_4200_TITLE_TAG" /></title>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<link href="lib/HubSpot-messenger/build/css/messenger-theme-future.css" rel="stylesheet" media="screen" />
<link href="lib/HubSpot-messenger/build/css/messenger.css" rel="stylesheet" media="screen" />
<style type="text/css">
.form-group{
	margin-bottom: 3px;
}
</style>
</head>
<body>
	<div class="container col-lg-12">
		<div class="row col-lg-12">
			<button class="btn btn-primary" id="f1_btn">
				F1
				<s:text name="QUERY_WAIT_BOX" />
			</button>
			<button class="btn btn-primary" id="f2_btn">
				F2
				<s:text name="QUERY_WFRL_BOX" />
			</button>
			<!-- <button class="btn btn-danger" id="f4_btn">
              F4:<s:text name="DELETE_TAG"/>
            </button> -->
			<button class="btn btn-primary" id="f8_btn">
				F3
				<s:text name="REGIST_TAG" />
			</button>
			<button class="btn btn-danger" id="f5_btn">F5已发料退料</button>
		</div>
		<fieldset class="col-lg-12">
			<legend>
				
			</legend>		
		<div class=" col-lg-12">
			<div class="col-lg-4">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-xs-4 control-label" for="vdrIDSelect"> <s:text name="CUS_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<select id="vdrIDSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="mtrlTypeSelect"> <s:text name="MTRL_TYPE" />
							</label>
							<div class="col-xs-6">
								<select id="mtrlTypeSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="woSelect"> <s:text name="WORDER_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<select id="woSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="destShopSelect"> <s:text name="DEST_SHOP_TAG" />
							</label>
							<div class="col-xs-6">
								<select id="destShopSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="rawBoxIDInput"> <s:text name="PPBOX_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<input id="rawBoxIDInput" class="form-control" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label keyCss" for="useUserInput"> <s:text name="USE_USER" />
							</label>
							<div class="col-xs-6">
								<input id="useUserInput" class="form-control" type="text">
							</div>
						</div>
					</form>
					<form  id="woInfoForm" class="form-horizontal">
						<div  class="form-group">
							<label class="col-xs-4 control-label" for="woIdSp"><s:text name="WORDER_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="woIdSp" class="form-control "/>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-xs-4 control-label" for="woCntSp"> <s:text name="WORDER_COUNT_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="woCntSp" class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="wfrlCntSp"> <s:text name="WFRL_BOX_CNT" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="wfrlCntSp" class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="waitCntSp"> <s:text name="WAIT_BOX_CNT" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="waitCntSp" class="form-control "/>
							</div>

						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="mdlTypeSp"> <s:text name="MDL_TYPE_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="mdlTypeSp" class="form-control "/>
							</div>

						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="mtrlTypeSp"> <s:text name="MTRL_TYPE" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="mtrlTypeSp" class="form-control "/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label" for="cusIdSp"> <s:text name="CUS_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="cusIdSp" class="form-control "/>
							</div>
						</div>
					</form>
			</div>
		
			<div class="col-lg-8" id="paneluseDiv">
			   <table id="paneluseGrd"></table>
			   <div id="panelusePg"></div>
		    </div>
		</div>
		
		</fieldset>
		
	    
	</div>




	<!-- Le javascript
    ================================================== -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="lib/underscore/underscore-min.js"></script>
	<script src="lib/HubSpot-messenger/build/js/messenger.min.js"></script>
	<script src="js/dialog/showMessenger.js"></script>
	<script src="js/page/4000/4200.js"></script>
	<script type="text/javascript" src="js/com/DestDesc.js"></script>
</body>
</html>
