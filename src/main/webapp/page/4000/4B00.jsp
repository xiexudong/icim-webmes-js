<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_4B00_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<style type="text/css">
#ppboxInfoGrdDiv {
	width: 97%;
}

.form-group {
	margin-bottom: 0px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="container-fluid">
			<button class="btn btn-primary" id="query_btn">
				F1
				<s:text name="QUERY_BTN_TAG" />
			</button>
			<button class="btn btn-danger" id="delete_btn">
				F2
				<s:text name="DELETE_TAG" />
			</button>
			<button class="btn btn-primary" id="clear_btn">
				F3
				<s:text name="F2_CLEAR_TAG" />
			</button>
			<button class="btn btn-warning" id="add_btn">
				F4
				<s:text name="ADD_TAG" />
			</button>
			<button class="btn btn-primary" id="regist_btn">
				F5
				<s:text name="REGIST_TAG" />
			</button>
			<button class="btn btn-warning" id="change_prd_admin_flg_btn">F6变更出货标识</button>
		</div>
		<div class="container-fluid">
			<fieldset>
				<legend>
					<s:text name="WIP_BANK_OUT_INFO_INPUT" />
				</legend>
				<div class="row">
					<div class="col-md-6">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="destShopSelect"><s:text name="DEST_SHOP_TAG" /></label>
								<div class="col-xs-4">
									<select id="destShopSelect" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label" for="opeSelect"><s:text name="OPE_ID_TAG" /></label>
								<div class="col-xs-4">
									<select id="opeSelect" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label" for="woIdSelect"><s:text name="WORDER_ID_TAG" /></label>
								<div class="col-xs-4">
									<select id="woIdSelect" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label keyCss" for="ppBoxIDInput"><s:text name="BOX_ID_TAG" /></label>
								<div class="col-xs-4">
									<input id="ppBoxIDInput" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label keyCss" for="shipUserInput"><s:text name="GET_WIP_USER_TAG" /></label>
								<div class="col-xs-4">
									<input id="shipUserInput" class="form-control" type="text">
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-6">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="woPlnQtyTxt"><s:text name="WO_PLN_QTY_TAG" /></label>
								<div class="col-xs-4">
									<input type="text" class="form-control" id="woPlnQtyTxt" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label" for="woWhinQtyTxt"><s:text name="WO_WHIN_QTY_TAG" />(半成品)</label>
								<div class="col-xs-4">
									<input type="text" class="form-control" id="woWhinQtyTxt" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label" for="woDiffQtyTxt"><s:text name="DIFF_QTY_TAG" /></label>
								<div class="col-xs-4">
									<input type="text" class="form-control" id="woDiffQtyTxt" disabled>
								</div>
							</div>
						</form>
					</div>
				</div>
			</fieldset>
		</div>
	</div>

	<div class="container-fluid">
		<fieldset>
			<legend>
				<s:text name="PPBOX_INFO" />
			</legend>
		</fieldset>
	</div>
	<div class="container-fluid" id="ppboxInfoGrdDiv">
		<table id="ppboxInfoGrd"></table>
		<div id="ppboxInfoPg"></div>
	</div>


	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script type="text/javascript" src="js/com/DestListTool.js"></script>
	<script src="js/page/4000/4B00.js?ver=20140828"></script>
	<script type="text/javascript" src="js/com/DestDesc.js"></script>

</body>
</html>
