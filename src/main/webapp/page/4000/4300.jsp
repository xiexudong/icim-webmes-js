<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_4300_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<link href="lib/HubSpot-messenger/build/css/messenger-theme-future.css" rel="stylesheet" media="screen" />
<link href="lib/HubSpot-messenger/build/css/messenger.css" rel="stylesheet" media="screen" />
<style>
#ppboxInfoGrdDiv {
	width: 97%;
}
.form-group{
	margin-bottom: 0px;
}
</style>
</head>
<body>

	<div class="container">
		<div class="container-fluid">
			<button class="btn btn-primary" id="f1_btn">
				F1
				<s:text name="QUERY_BTN_TAG" />
			</button>
			<button class="btn btn-primary" id="f2_btn">
				F2
				<s:text name="F2_CLEAR_TAG" />
			</button>
			<button class="btn btn-danger hide" id="f4_btn">
				<s:text name="DELETE_TAG" />
			</button>
			<button class="btn btn-primary" id="f5_btn">
				F3
				<s:text name="WEIGHING_TAG" />
			</button>
			<button class="btn btn-primary" id="f8_btn">
				F4
				<s:text name="REGIST_TAG" />
			</button>
			<button class="btn btn-primary" id="queryBox_btn">
				F5
				<s:text name="QUERY_BTN_TAG" />
				<s:text name="BOX_ID_TAG" />
			</button>
		</div>
		<div class="container-fluid">
			<fieldset>
				<legend>
					<s:text name="SHIP_INFO_INPUT" />
				</legend>
				<div class="row">
					<div class="col-md-5">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-xs-4 control-label keyCss" for="destShopSelect"><s:text name="DEST_SHOP_TAG" /></label>
								<div class="col-xs-6">
									<select id="destShopSelect" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 control-label keyCss" for="shipUserInput"><s:text name="SHIP_USER" /></label>
								<div class="col-xs-6">
									<input id="shipUserInput" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 control-label keyCss" for="ppBoxIDInput"><s:text name="BOX_ID_TAG" /></label>
								<div class="col-xs-6">
									<input id="ppBoxIDInput" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 control-label keyCss" for="shipboxInput">出货箱号</label>
								<div class="col-xs-6">
									<input id="shipboxInput" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 control-label keyCss" for="ppBoxWeightInput"><s:text name="WEIGHT_TAG" /></label>
								<div class="col-xs-6">
									<input id="ppBoxWeightInput" type="text" class="form-control">
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-5">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-xs-4 control-label" for="woIdSelect"><s:text name="WORDER_ID_TAG" /></label>
								<div class="col-xs-6">
									<select id="woIdSelect" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 control-label" for="woPlnQtyTxt"><s:text name="WO_PLN_QTY_TAG" /></label>
								<div class="col-xs-6">
									<input type="text" class="form-control" id="woPlnQtyTxt" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 control-label" for="woWhinQtyTxt"><s:text name="WO_WHIN_QTY_TAG" /></label>
								<div class="col-xs-6">
									<input type="text" class="form-control" id="woWhinQtyTxt" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 control-label" for="woWhinDiffQtyTxt"><s:text name="DIFF_QTY_TAG" /></label>
								<div class="col-xs-6">
									<input type="text" class="form-control" id="woWhinDiffQtyTxt" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 control-label" for="weighingBoxIDInput"><s:text name="BOX_TO_GET_WEIGHING_TAG" /></label>
								<div class="col-xs-6">
									<input id="weighingBoxIDInput" type="text" class="form-control disabled" disabled> <input id="selectGridIdHide" type="text" class="hide">
								</div>
							</div>
						</form>
					</div>
				</div>
			</fieldset>
		</div>
		<fieldset class="container-fluid">
			<legend>
				<s:text name="PPBOX_INFO" />
			</legend>
			<div class="container-fluid" id="ppboxInfoGrdDiv">
				<table id="ppboxInfoGrd"></table>
				<div id="ppboxInfoPg"></div>
			</div>
		</fieldset>
	</div>


	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="lib/HubSpot-messenger/build/js/messenger.min.js"></script>
	<script src="js/dialog/showMessenger.js"></script>
	<script src="js/page/4000/4300.js"></script>

</body>
</html>
