<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_5400_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.displayBlockCss {
	display: none;
}

#orderForm .control-group {
	margin-bottom: 8px;
}

#bindWODialog {
	margin-left: 10%;
	width: 70%;
	left: 10%;
}

.keyCss:after {
	float: right;
	content: '*';
	color: red;
}

.twoRowCss {
	height: 60px;
}

#dnListDiv {
	width: 97%;
}

form .form-group {
	margin-bottom: 1px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<link href="lib/datetimepicker/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" media="screen">
</head>
<body>
	<div class="container-fluid">
		<div class="col-lg-12">
			<button class="btn btn-primary" id="f1_query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="f4_del_btn">
				<s:text name="F4_DELETE_TAG" />
			</button>
			<button class="btn btn-primary" id="f5_upd_btn">
				<s:text name="F5_UPDATE_TAG" />
			</button>
			<button class="btn btn-primary" id="f8_regist_btn">
				<s:text name="F8_REGIST_TAG" />
			</button>
			<button class="btn btn-primary" id="f10_clear_btn">
				<s:text name="F10_CLEAR_TAG" />
			</button>
			<button class="btn btn-primary" id="f9_cancel_update_btn">
				<s:text name="CANCEL_UPDATE_BTN_TAG" />
			</button>
			<button class="btn btn-primary" id="f6_close_btn">
				<s:text name="F6_CLOSE_TAG" />
			</button>
			<button class="btn btn-primary" id="f7_open_btn">
				<s:text name="F7_OPEN_TAG" />
			</button>
			<button class="btn btn-primary" id="export_btn">
				<s:text name="EXPORT_FILE_TAG" />
			</button>
			<input id="evt_seq_id" type="text" class="hidden"
				style="float: right" />
		</div>

		<!-- 输入区--开始 -->
		<div class="col-lg-12">
			<div class="row col-lg-12">
				<div class="form-group">
					<div class="controls"></div>
				</div>
				<div class="row ">
					<form id="orderForm" class="form-horizontal">
						<div class="form-group col-lg-4 twoRowCss">
							<label class="control-label keyCss col-lg-5" for="dnNOTxt">
								<s:text name="DELIVERY_ORDER_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<input id="dnNOTxt" class="form-control" type="text" />
								<%-- 		<button id="autoDnBtn" class="btn" type="button">
								<s:text name="AUTO_GENERATOR_TAG" />
							</button> --%>
							</div>
						</div>
						<!-- <div class="control-group span3 offset1">
						<label class="control-label keyCss" for="shipDateTxt"> <s:text name="DELIVERY_DATE_TAG" /> </label>
						<div class="controls">
							<input type="text" id ="shipDateTxt" class = "span2"/>
						</div>
					</div> -->
						<!-- <div class="control-group span3 offset1">
						<label class="control-label" for="shipTimeTxt"> <s:text name="DELIVERY_TIME_TAG" /> </label>
						<div class="controls">
							<input id="shipTimeTxt" class="span2" type="text" />
						</div>
					</div> -->
						<div class="form-group col-lg-4  twoRowCss">
							<label class="control-label keyCss col-lg-5" for="shipQtyTxt">
								交货数量</label>
							<div class="col-lg-6">
								<input id="shipQtyTxt" class="form-control intCss" type="text" />
							</div>
						</div>
						<div id="shipDateDiv" class="form-group col-lg-4">
							<label class="control-label keyCss col-lg-4" for="shipDateTxt">
								<s:text name="计划交货日期" />
							</label>
							<div class="col-lg-6 ">
								<div id="shipDatepicker" class="input-group input-append">
									<input data-format="yyyy-MM-dd" type="text"
										class="form-control"> <span class="input-group-addon add-on"> <i
										data-date-icon="glyphicon-calendar" data-time-icon="glyphicon-time"
										class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-lg-6 col-lg-offset-4">
								<div id="shipTimepicker" class="input-group input-append">
									<input data-format="hh:mm:ss" type="text" class="form-control">
									<span class="input-group-addon add-on"> <i data-date-icon="glyphicon-calendar"
										data-time-icon="glyphicon-time" class="glyphicon glyphicon-time"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label keyCss col-lg-5" for="cusIDSel">
								<s:text name="CUS_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<select id="cusIDSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label keyCss col-lg-5" for="shipCusIDSel">
								交货客户代码</label>
							<div class="col-lg-6">
								<select id="shipCusIDSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group col-lg-4 ">
							<label class="control-label col-lg-4" for="addressSequeceSel">
								<s:text name="ADDRESS_SEQUENCE_TAG" />
							</label>
							<div class="col-lg-6">
								<select id="addressSequeceSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group col-lg-12">
							<label class="control-label col-lg-1" for="shipAddressTxt">
								<s:text name="DELIVERY_ADDRESS_TAG" />
							</label>
							<div class="col-lg-4">
								<input id="shipAddressTxt" class="form-control" type="text" />
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label keyCss col-lg-5" for="woIDSel">
								<s:text name="WORDER_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<select id="woIDSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label col-lg-5" for="mdlIdSel">
								产品名称 </label>
							<div class="col-lg-6">
								<select id="mdlIdSel" class="form-control"></select>
							</div>
						</div>
						<div class="form-group col-lg-4 offset1 hide">
							<label class="control-label col-lg-5" for="totalQtyTxt">
								订单总数量 </label>
							<div class="col-lg-6">
								<span id="totalQtyTxt" class="form-control uneditable-input"></span>
							</div>
						</div>
						<div class="form-group col-lg-4 ">
							<label class="control-label col-lg-4" for="rlPrdQtyTxt">
								<s:text name="SO_IN_PRD_QTY_TAG" />
							</label>
							<div class="col-lg-6">
								<span id="rlPrdQtyTxt" class="form-control uneditable-input"></span>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label col-lg-5" for="wipQtyTxt"> <s:text
									name="CREATING_COUNT_TAG" />
							</label>
							<div class="col-lg-6">
								<span id="wipQtyTxt" class="form-control uneditable-input"></span>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label col-lg-5" for="whInQtyTxt"> <s:text
									name="INVENTORY_COUNT_TAG" />
							</label>
							<div class="col-lg-6">
								<span id="whInQtyTxt" class="form-control uneditable-input"></span>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<label class="control-label col-lg-4" for="whOutQtyTxt">
								已出货数量 </label>
							<div class="col-lg-6">
								<span id="whOutQtyTxt" class="form-control  uneditable-input"></span>
							</div>
						</div>
						<div class="form-group col-lg-12">
							<label class="control-label col-lg-1" for="shipNoteTxt">
								<s:text name="REMARK_TAG" />
							</label>
							<div class="col-lg-4">
								<input id="shipNoteTxt" class="form-control" type="text" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- 输入区--结束 -->

		<!-- 列表区--开始 -->
		<div class="controls col-lg-12">
			<center>
				<legend id="legendDiv">
					<i class="icon-th"></i>
					<s:text name="INFO_BROWSE_TAG" />
				</legend>
			</center>
		</div>

		<div id="dnListDiv" class="controls col-lg-12">
			<table id="dnListGrd"></table>
			<div id="dnListPg"></div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script
	src="lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="js/com/SelectDom.js"></script>
<script type="text/javascript" src="js/com/CheckBoxDom.js"></script>
<script type="text/javascript" src="js/com/Regex.js"></script>
<script type="text/javascript" src="js/com/TimestampUtil.js"></script>
<script type="text/javascript" src="js/page/5000/5400.js"></script>

<!-- <script type="text/javascript" src="/lib/jquery-ui-1.9.2.custom/js/jquery-ui-timepicker-addon.js"></script> -->
</html>