<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_5100_SALES_ORDER_MANAGEMENT_TAG" />
</title>
<base href="<%=basePath%>">

<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
div .form-width{

	padding-left:0px;
	padding-right:0px;
}
.displayBlockCss {
	display: none;
}

#bindWODialog {
	margin-left: 10%;
	width: 70%;
	left: 10%;
}
#cusSoDialog {
	margin-left: 2%;
	width: 90%;
	left: 4%;
}
.keyCss:after {
	float: right;
	content: '*';
	color: red;
}
#orderListDiv{
	width:97%;
}
#orderDetailListDiv{
	width:97%;
}
#legendDiv1{
	width:96%;
}
#legendDiv2{
	width:96%;
}
#orderIDTxt{
    width:70%;
    float:left;
}
#autoGeneratorBtn{
    width:28%;
    float:right;
}
form .form-group {
	margin-bottom: 1px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

</head>
<body>
<div class="container-fluid">
	<div class="row col-lg-12">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="f4_del_btn">
			<s:text name="F4_DELETE_TAG" />
		</button>
		<button class="btn btn-primary" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG" />
		</button>
		<button class="btn btn-primary" id="f8_add_btn">
			<s:text name="F8_REGIST_TAG" />
		</button>
		<button class="btn btn-primary" id="f12_show_wo_btn">
			显示已绑定工单
		</button>
		<button class="btn btn-primary" id="f11_bind_btn">
			更改为实际的订单
		</button>
		<button class="btn btn-primary" id="f9_cancel_update_btn">
			<s:text name="CANCEL_UPDATE_BTN_TAG" />
		</button>
		<button class="btn btn-primary" id="f10_clear_btn">
			<s:text name="F10_CLEAR_TAG" />
		</button>
		<button class="btn btn-primary" id="f6_close_btn">
			<s:text name="F6_CLOSE_TAG" />
		</button>
		<button class="btn btn-primary" id="f7_open_btn">
			<s:text name="F7_OPEN_TAG" />
		</button>
		<button class="btn btn-primary" id="exportBtn">导出到文件</button>
		<button class="btn btn-primary" id="importCusSoBtn">
			导入华星光电客户订单
		</button>
		<%-- <button class="btn btn-primary" id="importCusSoBtn">
			导入武汉天马客户订单
		</button>--%>
	</div>

	<!-- 输入区--开始 -->
	<div class="col-lg-12">
		<div class="row col-lg-12">
			<div class="form-group">
				<div></div>
			</div>
			<div class="row">
				<form id="orderForm" class="form-horizontal">
				<div class="row">
					<div class="form-group col-lg-4 ">
						<label class="control-label keyCss col-lg-5" for="orderIDTxt"> <s:text
								name="CUS_ORDER_ID_TAG" /> </label>
						<div class="col-lg-7">
							<input id="orderIDTxt" name="wo_id" class="form-control" type="text">
						<%-- 	<button id="autoGeneratorBtn" class="btn form-width" type="button">
								<s:text name="AUTO_GENERATOR_TAG" />
							</button> --%>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="cusIDSel"> <s:text
								name="CUS_ID_TAG" /> </label>
						<div class="col-lg-7">
							<select id="cusIDSel" class="form-control"></select>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label keyCss col-lg-5" for="woCateSel"> <s:text
								name="WORDER_CATE_TAG" /> </label>
						<div class="col-lg-7">
							<select id="woCateSel" class="form-control"></select>
						</div>
					</div>
					</div>
					<div class="row">
					<div class="form-group col-lg-4">
						<label class="control-label keyCss col-lg-5" for="mtrlIDSel">来料型号
						</label>
						<div class="col-lg-7">
							<select id="mtrlIDSel" class="form-control"></select>
						</div>
					</div>

					<!--TR  -->
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="mdlIDDiv"><s:text
								name="TH_MDL_ID_TAG" /> </label>
						<div class="col-lg-7">
							<select id="thMdlIdSel" class="form-control"></select>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="tThicknessTxt">T侧厚度</label>
						<div class="col-lg-7">
							<input type="text" id="tThicknessTxt" class="form-control"/>
						</div>
					</div>
					</div>
					<div class="row">
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="fmMdlIDSel"> <s:text
								name="FM_MDL_ID_TAG" /> </label>
						<div class="col-lg-7">
							<select id="fmMdlIDSel" class="form-control"></select>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="cutMdlIDSel"> <s:text
								name="CUT_MDL_ID_TAG" /> </label>
						<div class="col-lg-7">
							<select id="cutMdlIDSel" class="form-control"></select>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="cThicknessTxt">C侧厚度</label>
						<div class="col-lg-7">
							<input type="text" id="cThicknessTxt" class="form-control"/>
						</div>
					</div>
					</div>
					<div class="row">
					<div class="form-group col-lg-4">
						<label class="control-label keyCss col-lg-5" for="pathChooseSel">工艺路线设定
						</label>
						<div class="col-lg-7">
							<select id="pathChooseSel" class="form-control"></select>
						</div>
					</div>
                    
					<div class="form-group col-lg-3">
						<label class="control-label col-lg-4" for="inlineCheckboxes">选择项</label>
						<div class="col-lg-8">
							<label class="checkbox inline"> <input type="checkbox"
								id="opeEffFlgChk" value="option1"> <s:text
									name="是否原箱原位" /> </label>
						    <label class="checkbox inline"> <input type="checkbox"
								id="vcrFlgChk" value="option2"> <s:text
									name="是否原箱返" /> </label>
						</div>
					</div>
                    </div>
                    <div class="row">
					<div class="form-group col-lg-3">
						<label class="control-label keyCss col-lg-5" for="plnStbDateTxt"> <s:text
								name="FROM_DATE_TAG" /> </label>
						<div class="col-lg-7">
							<input id="plnStbDateTxt" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group col-lg-3">
						<label class="control-label keyCss col-lg-5" for="plnCmpDateTxt"> <s:text
								name="DELIVERY_DATE_TAG" /> </label>
						<div class="col-lg-7">
							<input id="plnCmpDateTxt" type="text" class="form-control" />
						</div>
					</div>

					<div class="form-group col-lg-3">
						<label class="control-label keyCss col-lg-5" for="plnPrdQtyTxt">订单数量
						</label>
						<div class="col-lg-7">
							<input id="plnPrdQtyTxt" class="form-control intCss" type="text" />
						</div>
					</div>
					<div class="form-group col-lg-3">
						<label class="control-label col-lg-5" for="mtrlPartSel"> <s:text
								name="MTRL_PART_TAG" /> </label>
						<div class="col-lg-7">
							<select id="mtrlPartSel" class="form-control"></select>
						</div>
					</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-lg-1" for="woNoteTxt"> <s:text
								name="REMARK_TAG" /> </label>
						<div class="col-lg-9">
							<input id="woNoteTxt" class="form-control" type="text" />
						</div>
					</div>
					<div class="row">
					<div class="form-group col-lg-3">
						<label class="control-label keyCss col-lg-5"  for="fromThicknessTxt">
							<s:text name="FROM_THICKNESS_TAG" /> </label>
						<div class="col-lg-7">
							<input id="fromThicknessTxt" class="form-control numberCss" type="text" />
						</div>
					</div>
					<div class="form-group col-lg-3">
						<label class="control-label keyCss col-lg-5" for="toThicknessTxt">目标厚度
						</label>
						<div class="col-lg-7">
							<input id="toThicknessTxt" class="form-control numberCss" type="text" />
						</div>
					</div>
					<div class="form-group col-lg-3">
						<label class="control-label col-lg-5" for="cusInfoFstTxt">委外工单号
						</label>
						<div class="col-lg-7">
							<input id="cusInfoFstTxt" class="form-control" type="text" />
						</div>
					</div>
					<div class="form-group col-lg-3">
						<label class="control-label col-lg-5" for="cusInfoSndTxt">客户工单号
						</label>
						<div class="col-lg-7">
							<input id="cusInfoSndTxt" class="form-control" type="text" />
						</div>
					</div>
					</div>
					<!-- 003 -->
<!-- 					<div class="row">
					<div class="form-group col-lg-3">
						<label class="control-label col-lg-5" for="manualTxt">手册
						</label>
						<div class="col-lg-7">
							<input id="manualTxt" class="form-control" type="text" />
						</div>
					</div>
					<div class="form-group col-lg-3">
						<label class="control-label col-lg-5" for="cusMtrlTxt">客户料号
						</label>
						<div class="col-lg-7">
							<input id="cusMtrlTxt" class="form-control" type="text" />
						</div>
					</div>
					<div class="form-group col-lg-3">
						<label class="control-label col-lg-5" for="glassformTxt">基板来源
						</label>
						<div class="col-lg-7">
							<input id="glassformTxt" class="form-control" type="text" />
						</div>
					</div>
					</div> -->
				</form>
			</div>
		</div>
	</div>
	<!-- 输入区--结束 -->

	<!-- 列表区--开始 -->
	<div class="controls span12">
		<center>
			<legend id="legendDiv1">
				<i class="icon-th"></i>
				<s:text name="INFO_BROWSE_TAG" />
			</legend>
		</center>
	</div>

	<div id="orderListDiv" class="controls col-lg-12">
		<table id="orderListGrd"></table>
		<div id="orderListPg"></div>
	</div>
	<!-- 列表区--结束 -->

	<!-- 列表区2--开始 -->
	<div class="controls col-lg-12">
		<center>
			<legend id="legendDiv2">
				<i class="icon-th"></i>
				<s:text name="INFO_DETAIL_BROWSE_TAG" />
			</legend>
		</center>
	</div>

	<div id="orderDetailListDiv" class="controls col-lg-12">
		<table id="orderDetailListGrd"></table>
		<div id="orderDetailListPg"></div>
	</div>

<div class="modal fade" id="bindWODialog" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog " style="width:90%">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">更改为实际的订单</h4>
			</center>
		</div>
		<!--Modal header-->
		<div class="modal-body" style="height:90px">
			<form id="orderForm" class="form-horizontal">
				<div class="control-group col-lg-4">
					<label class="control-label col-lg-5" for="bindWODialog_orderIDTxt">
						<s:text name="CUS_ORDER_ID_TAG" /> </label>
					<div class="col-lg-7">
						<input id="bindWODialog_orderIDTxt" class="form-control uneditable-input"
							type="text">
					</div>
				</div>
				<div class="control-group col-lg-4">
					<label class="control-label col-lg-5" for="bindWODialog_plnPrdQtyTxt">
						<s:text name="FROM_COUNT_TAG" /> </label>
					<div class="col-lg-7">
						<input id="bindWODialog_plnPrdQtyTxt"
							class="form-control uneditable-input" type="text" />
					</div>
				</div>
				<div class="control-group col-lg-4">
					<label class="control-label col-lg-5" for="bindWODialog_soIDTxt"> <s:text
							name="RELEALITY_ORDER_TAG" /> </label>
					<div class="col-lg-7">
						<input id="bindWODialog_soIDTxt" class="form-control" type="text">
					</div>
				</div>
			</form>
		</div>
		<!--Modal body-->
		<div class="modal-footer">
			<a id="bindWODialog_cancelBtn" class="btn" data-dismiss="modal">
				<s:text name="CANCEL_TAG" /> </a> <a id="bindWODialog_bindWOBtn"
				class="btn btn-primary"> <s:text name="F11_BIND_WO_TAG" /> </a>
		</div>
		</div>
		</div>
		<!--Modal footer-->
	</div>
	
<div class="modal fade" id="cusSoDialog" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog" style="width: 600px">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">客户订单信息</h4></center>
      </div>
      <div class="modal-body" id="cusSoDialogDiv">
	      <div id="cusSoDiv">
		      <table id="cusSoGrd"></table>
		      <div id="cusSoGrdPg"></div>
	      </div>  
      </div>
      <div class="modal-footer" >
	      <a  id="cusSoDialog_cancelBtn"  class="btn" data-dismiss="modal" >
	      	  <s:text name="CANCEL_TAG" />
	      </a>
	      <a  id="cusSoDialog_selectSoBtn" class="btn btn-primary">
	      	  选择订单
	      </a>
      </div>
     </div>
     </div>
     </div>
     </div>
   
     
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="js/com/SelectDom.js"></script>
<script src="js/com/CheckBoxDom.js"></script>
<script src="js/com/Regex.js"></script>
<script src="js/com/TransUtil.js"></script>
<script type="text/javascript" src="js/page/5000/5100.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>

</html>