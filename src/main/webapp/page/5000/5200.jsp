<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_5200_WORDER_MANAGEMENT_TAG" /></title>
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
#bindWODialog {
	margin-left: 2%;
	width: 90%;
	left: 4%;
}
#worderListDiv{
	width:97%;
}


</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<link href="lib/datetimepicker/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" media="screen">
<jsp:include page="/page/dialog/selectWorderDialog.jsp"></jsp:include>
</head>
<body>
<div class="container-fluid">
	<div class="col-lg-12">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-primary" id="f4_del_btn">
			<s:text name="F4_DELETE_TAG" />
		</button>
		<button class="btn btn-primary" id="f8_add_btn">
			<s:text name="F8_REGIST_TAG" />
		</button>
		<button class="btn btn-primary" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG" />
		</button>
		<button class="btn btn-primary" id="commit_upd_btn">
			提交修改
		</button>
		<button class="btn btn-primary" id="cancel_upd_btn">
			取消修改
		</button>
		<button class="btn btn-primary" id="f6_close_btn">
			<s:text name="F6_CLOSE_TAG" />
		</button>
		<button class="btn btn-primary" id="f7_open_btn">
			<s:text name="F7_OPEN_TAG" />
		</button>
		<button class="btn btn-primary" id="f10_clear_btn">
			<s:text name="F10_CLEAR_TAG" />
		</button>
		<button class="btn btn-primary" id="f11_bind_btn">绑定商务订单</button>
		<!-- <button class="btn btn-primary" id="f11_export_btn">导出文件</button> -->
	</div>
	<div class="col-lg-12">
		<div class="row col-lg-12">
			<!-- 产品提配Div -->
			<div class="form-group">
				<div class="controls">
					<center>
						<legend>
							<i class="icon-calendar"></i>
							<s:text name="WORDER_INFO_TAG" />
						</legend>
					</center>
				</div>
			</div>
			<div class="row ">
				<form id="orderForm" class="row form-horizontal">
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="cusIdSel">客户代码</label>
						<div class="col-lg-5">
							<select id="cusIdSel" class="form-control"></select>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="mtrlProdIdSel">来料型号</label>
						<div class="col-lg-5">
							<select id="mtrlProdIdSel" class="form-control"></select>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="woderDiv"><s:text
								name="WORDER_ID_TAG" /> </label>
						<div class="col-lg-5">
							<div id="woderDiv" class="input-append">
								<input class="form-control" id="woIdTxt" type="text">
							</div>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="plnStbTimestampTxt">
							来料时间 </label>
						<!-- <div class="controls">                           
				        	<input type="text" id="plnStbTimestampTxt" class="span2"/>
					    </div>  -->
						<div class="col-lg-5 ">
							<div id="plnStbDatepicker" class="input-group input-append">
								<input data-format="yyyy-MM-dd" type="text" class="form-control">
								<span class="input-group-addon add-on"> <i data-date-icon="glyphicon-calendar"
									data-time-icon="glyphicon-time" class="glyphicon glyphicon-calendar"></i> </span>
							</div>
						</div>
						<div class="col-lg-5 col-lg-offset-5">
							<div id="plnStbTimepicker" class="input-group input-append">
								<input data-format="hh:mm:ss" type="text" class="form-control">
								<span class="input-group-addon add-on"> <i data-date-icon="glyphicon-calendar"
									data-time-icon="glyphicon-time" class="glyphicon glyphicon-time"></i> </span>
							</div>
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="plnCompTimestampTxt">
							交货时间 </label>
						<!--  	<div class="controls">                           
				        	<input type="text" id="plnCompTimestampTxt" class="span2"/>
					    </div>  -->
						<div class="col-lg-5 ">
							<div id="plnCompDatepicker" class="input-group input-append">
								<input data-format="yyyy-MM-dd" type="text" class="form-control">
								<span class="input-group-addon add-on"> <i data-date-icon="icon-calendar"
									data-time-icon="icon-time" class="glyphicon glyphicon-calendar"></i> </span>
							</div>
						</div>
						<div class="col-lg-5 col-lg-offset-5">
							<div id="plnCompTimepicker" class="input-group input-append">
								<input data-format="hh:mm:ss" type="text" class="form-control">
								<span class="input-group-addon add-on"> <i data-date-icon="icon-calendar"
									data-time-icon="icon-time" class="glyphicon glyphicon-time"></i> </span>
							</div>
						</div>
					</div>
					<div class="form-group col-lg-4 rsvPlnPrdQtyDiv">
						<label class="control-label keyCss col-lg-5" for="rsvPlnPrdQtyTxt">
							计划来料数量</label>
						<div class="col-lg-5">
							<input type="text" id="rsvPlnPrdQtyTxt" class="form-control intCss" />
						</div>
					</div>
					<div class="form-group col-lg-4">
						<label class="control-label col-lg-5" for="woCateSel"> <s:text
								name="WORDER_CATE_TAG" /> </label>
						<div class="col-lg-5">
							<select id="woCateSel" class="form-control"></select>
						</div>
					</div>
					
					
					<div class="form-group col-lg-4">
					
						<label class="control-label col-lg-5" for="plantSel"> 产品类型</label>
						<div class="col-lg-5">
							<select id="plantSel" class="form-control ">
							 <!--  <option value=''></option>
							    <option value="G5">G5</option>
							    <option value="G6">G6</option> -->
							</select>
						</div>
						
					</div>

					<div class="form-group col-lg-8">
					
						<label class="control-label col-lg-1" for="woDscTxt"> 描述 </label>
						<div class="col-lg-5">
							<input type="text" id="woDscTxt" class="form-control" />
						</div>
						<div id="0581">
						<label class="control-label col-lg-1" for="mainTitleTxt"> 主标签Title </label>
						<div class="col-lg-5">
							<input type="text" id="mainTitleTxt" class="form-control" />
						</div>
						</div>
					</div>
					
					<div class="form-group col-lg-12" id="0582">
					<label class="control-label col-lg-1" for="shtidTitleTxt"> 信息表Title </label>
						<div class="col-lg-5">
							<input type="text" id="shtidTitleTxt" class="form-control" />
						</div>
							<label class="control-label col-lg-1" for="jgyqSel"> 加工要求 </label>
						<div class="col-lg-5">
							<select id="jgyqSel" class="form-control" ></select>
						</div>
					</div>
					<div class="form-group col-lg-12" id="0583">
						<label class="control-label col-lg-1" for="fhSel"> 发货地址 </label>
						<div class="col-lg-5">
							<select id="fhSel" class="form-control" ></select>
						</div>
					</div>
					<!--TR -->

					<!-- <div class="control-group span4">
						<label class="control-label" for="soIdTxt"> <s:text
								name="ORDER_ID_TAG" /> </label>
						<div class="controls">
							<input id="soIdTxt" type="text" class="span2" /> 
							<select id="soIdSel" class="span2"></select>
						</div>
					</div> -->
					<!-- <div class="control-group span4">
						<label class="control-label" for="woPrtySel"> <s:text
								name="PRIORITY_TAG" /> </label>
						<div class="controls">
							<select id="woPrtySel" class="span2"></select>
						</div>
					</div> -->
				</form>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<center>
			<legend id = "legendDiv">
				<i class="icon-th"></i>
				<s:text name="WORDER_LIST_TAG" />
			</legend>
		</center>
	</div>

	<div id="worderListDiv" class="col-lg-12">
		<table id="worderListGrd"></table>
		<div id="worderListPg"></div>
	</div>
<div class="modal fade" id="bindWODialog"  tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog" style="width:1200px">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title">
		   	       	绑定商务订单
		      </h4>
	      </center>
      </div>
      <div class="modal-body" id="bindWoDialogDiv" style="height:400px">
      	  <div class ="col-lg-10">
      	  		<form id="orderForm" class="form-horizontal">
				      <div class="control-group col-lg-12">
					      <label class="control-label  col-lg-2" for="bindWODialog_worderIDTxt" > <s:text name="WORDER_ID_TAG" /> </label>
					      <div class="col-lg-4">
				      	  	   <input id="bindWODialog_worderIDTxt" class="form-control uneditable-input" type="text">
					      </div>
				      </div>
				 </form>
      	  </div>
	      
	      <div id="bindWoDiv" class="col-lg-10">
		      <table id="bindWoDialog_woGrd"></table>
		      <div id="bindWoDialog_woGrdPg"></div>
	      </div>  
      </div>
      <div class="modal-footer" >
	      <a  id="bindWODialog_cancelBtn"  class="btn" data-dismiss="modal" >
	      	  <s:text name="CANCEL_TAG" />
	      </a>
	      <a  id="bindWODialog_bindWOBtn" class="btn btn-primary">
	      	  <s:text name="F11_BIND_WO_TAG" />
	      </a>
      </div>
      </div>
      </div>
     </div>
     </div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="js/dialog/selectWorderDialog.js"></script>
<script src="lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="js/com/SelectDom.js"></script>
<script src="js/com/Regex.js"></script>
<script src="js/page/5000/5200.js"></script>
</html>