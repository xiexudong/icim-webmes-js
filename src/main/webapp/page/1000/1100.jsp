<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_1100_TITLE_TAG" /></title>
<script type="text/javascript">
		var basePath = "<%=basePath%>";
</script>
	<applet alt="Printers" name="Printers" mayscript="mayscript"
			codebase="." code="com.print.PrinterApplet.class"
			archive="<%=basePath%>jar/print.jar, <%=basePath%>jar/barcode4j.jar, <%=basePath%>jar/jackson-all-1.9.11.jar"
			width="0" height="0"></applet>

	<script type="text/javascript">
        try {
            var applet = document.Printers;
            var printers = applet.findAllPrinters();
            printers = eval(printers);
            console.log(printers);
        } catch (ex) {
            console.info(ex);
        }
	</script>
<style type="text/css">
#btnDiv, #queryBtnDiv {
	position: relative;
	top: -5px;
}

div .form-width{

	padding-left:0px;
	padding-right:0px;
}
#spcDialog {
	margin-left: 2%;
	width: 90%;
	height: 80%;
	left: 4%;
}

#spcDialogDiv {
	height: 75%;
}

form .form-group {
	margin-bottom: 1px;
}
#releasedDetailDiv{
   height:300px;
}
#defectDetailDiv{
   height:220px;
}
</style>
<%@ include file="/page/comPage/comCSS.html"%>
<link href="lib/HubSpot-messenger/build/css/messenger-theme-future.css"
	rel="stylesheet" media="screen" />
<link href="lib/HubSpot-messenger/build/css/messenger.css"
	rel="stylesheet" media="screen" />
<%@ include file="/page/comPage/navbar.html"%>

<link rel="stylesheet" media="screen"
	href="css/dialog/1000/defectJudge2.css">
</head>
<body>

	<div class="container-fluid">
		<div class="col-lg-12" id="btnDiv">
			<button class="btn btn-primary hide" id="f1_btn">
				F1
				<s:text name="F1_QUERY_TAG" />
				<s:text name="WORDER_ID_TAG" />
			</button>
			<%--<button class="btn btn-primary" id="f2_btn">
				F2
				<s:text name="REGIST_TAG" />
			</button>--%>
			<button class="btn btn-primary hide" id="f5_btn">
				<s:text name="F5_ProdIDRead_TAG" />
			</button>
			<button class="btn btn-primary" id="f6_btn">
				F3
				<s:text name="Release_TAG" />
			</button>
			<button class="btn btn-primary" id="f9_cancel_unpack_btn">
				F5
				<s:text name="CANCEL_UNPACK_TAG" />
			</button>
			<button class="btn btn-primary" id="f8_btn">
				F12
				<s:text name="UNPACK_TAG" />
			</button>
			<button class="btn btn-primary hide" id="wip_retain_btn">
				F7
				<s:text name="WIP_RETAIN_TAG" />
			</button>
			<button class="btn btn-primary hide" id="wip_retain_cancel_btn">
				F8
				<s:text name="WIP_RETAIN_CANCEL_TAG" />
			</button>

		</div>

		<div id="inputInfoSpan4Div" class="col-lg-3 form-width">

			<form class="form-horizontal">

				<div class="form-group">
					<label for="woTypeSelect" class="col-lg-5 control-label">订单类型</label>
					<div class="col-lg-7">
						<select id="woTypeSelect" class="form-control"></select>
					</div>
				</div>

				<div class="form-group">
					<label for="woIDSelect" class="col-lg-5 control-label"><s:text
							name="WORDER_ID_TAG" /></label>
					<div class="col-lg-7">
						<select id="woIDSelect" class="form-control"></select>
					</div>
				</div>

				<div class="form-group">
					<label for="opeSelect" class="col-lg-5 control-label"><s:text
							name="OPE_ID_TAG" /></label>
					<div class="col-lg-7">
						<select id="opeSelect" class="form-control"></select>
					</div>
				</div>

				<div class="form-group">
					<label for="toolSelect" class="col-lg-5 control-label"><s:text
							name="EQPT_TAG" /></label>
					<div class="col-lg-7">
						<select id="toolSelect" class="form-control"></select>
					</div>
				</div>
				<div class="form-group">
					<label for="layoutIdSelect" class="col-lg-5 control-label"><s:text
							name="LAYOT_ID_TAG" /></label>
					<div class="col-lg-7">
						<select id="layoutIdSelect" class="form-control"></select>
					</div>
				</div>
				<div class="form-group hide">
					<label for="to_thicknessTxt" class="col-lg-5 control-label"><s:text
							name="TO_THICKNESS_TAG" /></label>
					<div class="col-lg-7">
						<input type="text" id="to_thicknessTxt" class="form-control"/>
					</div>
				</div>

				<div class="form-group hide">
					<label for="opeSelect" class="col-lg-5 control-label"><s:text
							name="DEFECT_OUT" /></label>
					<div class="control-label hide">
						<input type="checkbox" id="defectOutChk">
					</div>
				</div>
			</form>
		</div>

<div class="col-lg-3 form-width">
			<form class="form-horizontal">
			  <div class="form-group">
					<label for="destShopSelect" class="col-lg-5 control-label"><s:text
							name="DEST_SHOP_TAG" /></label>
					<div class="col-lg-7">
						<select type="text" id="destShopSelect" class="form-control"></select>
					</div>
				</div>
                <div class="form-group">
					<label for="ppboxIdTxt" class="col-lg-5 control-label"><s:text
							name="PPBOX_ID_TAG" /></label>
					<div class="col-lg-7">
						<input type="text" id="ppboxIdTxt" class="form-control"/>
					</div>
				</div>
				  <%--<div class="form-group">--%>
					<%--<label for="ppboxTxt" class="col-lg-5 control-label"><s:text--%>
							<%--name="来料箱体" /></label>--%>
					<%--<div class="col-lg-7">--%>
						<%--<input type="text" id="ppboxTxt" class="form-control"/>--%>
					<%--</div>--%>
				<%--</div>--%>
				<div class="form-group">
					<label for="rawBoxStdQtyInput" class="col-lg-5 control-label keyCss"><s:text
							name="BOX_STD_QTY_TAG" /></label>
					<div class="col-lg-7">
						<input type="text" id="rawBoxStdQtyInput" class="form-control"/>
					</div>
				</div>

				<div class="form-group">
					<label for="rawCountInput" class="col-lg-5 control-label keyCss"><s:text
							name="QTY_TAG" /></label>
					<div class="col-lg-7">
						<input type="text" id="rawCountInput" class="form-control"/>
					</div>
				</div>


			</form>
		</div>
        
		<div class="col-lg-3 form-width">
			<form class="form-horizontal">



				<div class="form-group">
					<label for="prdIDTxt" class="col-lg-4 control-label"><s:text
							name="PRODUCT_ID_TAG" /></label>
					<div class="col-lg-7">
						<input type="text" id="prdIDTxt" class="form-control"/>
					</div>
				</div>

				<div class="form-group">
					<label for="slotIDTxt" class="col-lg-4 control-label"><s:text
							name="SLOT_NO_TAG" /></label>
					<div class="col-lg-7">
						<input type="text" id="slotIDTxt" class="form-control"
							maxlength="4">
					</div>
				</div>

				<div class="form-group">
					<label for="lotIdTxt" class="col-lg-4 control-label"><s:text
							name="LOT_ID_TAG" /></label>
					<div class="col-lg-7">
						<input type="text" id="lotIdTxt" class="form-control"
							maxlength="25">
					</div>
				</div>
				<div class="form-group">
					<button id="okOutBtn" class="btn btn-primary col-lg-offset-2 col-lg-2">箱号</button>
					<div class="col-lg-7">
						<input id="okOutCrrTxt" type="text" class="form-control"
							placeholder="出账箱号">
					</div>
				</div>
				<div class="form-group">
					<label for="print1Select" class="col-lg-4 control-label">打印机</label>
					<div class="col-lg-7">
					<select type="text" id="print1Select" class="form-control"></select>
					</div>
				</div>
			</form>
		</div>
			<div class="col-lg-3 form-width">
				<div id="defectDetailDiv">
					<table id="defectDetailGrd"></table>
					<div id="defectDetailPg"></div>
				</div>
			</div>
		<div class="col-lg-12">
				<div id="releasedDetailDiv">
					<table id="releasedDetailGrd"></table>
					<div id="releasedDetailPg"></div>
				</div>


		</div>

	</div>
	<!-- Dialog-->
	
	<div class="modal fade" id="spcDialog" tabindex="-1" role="dialog"
   aria-labelledby="spcLabel" aria-hidden="true">
   <div class="modal-dialog" style="width:100%;height:100%">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title" id="spcLabel">
						<span
							style="float: left; font-size: 12px; font-family: sans-serif; font-weight: normal">
							面板ID<input id="prd_seq_id" type="text" disabled
							style="width: 100px"> 站点<input id="ope_id" type="text"
							disabled style="width: 100px">
						</span> <span>SPC数据查询及上报</span>
					</h4>
				</center>
			</div>
			<div class="modal-body" id="spcDialogDiv">
				<div class="row">
					<div id="dataItemEditDiv" class="col-lg-6"></div>
					<div id="spcDiv" class="col-lg-5">
						<table id="spcDialog_woGrd"></table>
						<div id="spcDialog_woGrdPg"></div>
						<button id="spcDialog_reportBtn" class="btn btn-info"> 上报数据</button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				 <a id="spcDialog_cancelBtn"
					class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
				</a>
			</div>
		</div>
     </div>
</div>

	<%@ include file="/page/dialog/1000/defectJudge2.html"%>
	<!-- Le javascript
    ================================================== -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="lib/HubSpot-messenger/build/js/messenger.min.js"></script>
	<script src="js/dialog/showMessenger.js"></script>
	<script src="js/dialog/1000/defectJudge2.js?v=201407011420"></script>
	<script src="js/dialog/comKeyBoardDialog.js"></script>
	<script src="js/dialog/showSimpleInputDialog.js"></script>
	<script src="js/page/1000/1100.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/com/printl.js"></script>
	<script type="text/javascript" src="js/dialog/1000/wip_retain_Dialog.js"></script>
	<script type="text/javascript" src="js/com/Regex.js"></script>
	<script src="js/com/spcThicknessRule.js"></script>
	<jsp:include page="/page/dialog/1000/wip_retain_Dialog.jsp"></jsp:include>
</body>
</html>
