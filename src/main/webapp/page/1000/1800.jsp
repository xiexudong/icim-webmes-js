<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>
	">
	<title><s:text name="M_1800_TITLE_TAG"/></title> 
	<%@ include file="/page/comPage/comCSS.html"%>
	<%@ include file="/page/comPage/navbar.html"%></head>
	<style>
		#holdDetailControlDiv,#holdDetailDiv{
			width:97%;
		}
	</style>
<body>
<div class="container-fluid">
	<div class="col-lg-12">
		<button class="btn btn-primary" id="f1_btn">
			<s:text name="F1_QUERY_TAG"/>
		</button>
		<button class="btn btn-primary" id="f7_btn">
			<s:text name="HOLD_RELEASE_TAG"/>
		</button>
	</div>

	<div class ="row col-lg-12">
		<div class ="col-lg-12">
			<fieldset>
				<legend>
					<s:text name="PPBOX_INFO"/>
				</legend>
					<form class="form-horizontal">
						<label class="control-label keyCss col-lg-2" for="ppBoxIDInput">
							<s:text name="BOX_ID_TAG"/>
						</label>
						<div class="controls col-lg-3">
							<input id="ppBoxIDInput" type="text" class="form-control" style="width: 200px">
						</div>
						<!-- <label class="control-label" for="holdReasonSelect">
							<s:text name="HOLD_REASON_TAG"/>
						</label>
						<div class="controls">
							<select id="holdReasonSelect" class="span2"></select>
						</div> -->

					</form>
			</fieldset>
		</div>
		<!-- <div class="span5">
			<fieldset>
				<legend>
					<s:text name="REMARK_TAG"/>
				</legend>
				<div class="controls">
					<textarea class="input-xlarge" id="remarkInput" rows="3"></textarea>
				</div>
			</fieldset>

		</div> -->
	</div>
		<div class="col-lg-12">
			<legend>
				<s:text name="PRODUCT_ID_TAG"/>
			</legend>
		</div>
	<div id="holdDetailControlDiv">
		<div class="col-lg-12" id="holdDetailDiv">
			<table id="holdDetailGrd"></table>
			<div id="holdDetailPg"></div>
		</div>
	</div>
</div>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/1000/1800.js" ></script>

</body>
</html>