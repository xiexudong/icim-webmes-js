<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_1400_TITLE_TAG" /></title>
<script type="text/javascript">
			var basePath = "<%=basePath%>";

</script>
		<applet alt="Printers" name="Printers" mayscript="mayscript"
			codebase="." code="com.print.PrinterApplet.class"
			archive="<%=basePath%>jar/print.jar, <%=basePath%>jar/barcode4j.jar, <%=basePath%>jar/jackson-all-1.9.11.jar"
			width="0" height="0"></applet>

		<script type="text/javascript">
			try {
				var applet = document.Printers;
				var printers = applet.findAllPrinters();
				printers = eval(printers);
				console.log(printers);
			} catch (ex) {
				console.info(ex);
			}
		</script>
<style>
#wipInfoDiv {
	width: 100%;
}

#btnDiv {
	position: relative;
	top: -5px;
}

#remarkTxt {
	height: 80px;
	float: right;
	position: relative;
	right: 30px;
	resize: none;
}

#spcDialog {
	margin-left: 2%;
	width: 90%;

	left: 4%;
}

#spcDialogDiv {
	height: 75%;
}

.form-horizontal .form-group {
	margin: 1px 0px;
}

#remarkDtlDialog {
	margin-left: 2%;
	width: 100%;
	height: 80%;
	left: 4%;
}

#remarkDtlDialogDiv {
    width:80%;
	height: 700px;
}

form .form-group {
	margin-bottom: 1px;
}
div .form-width{

	padding-left:0px;
	padding-right:0px;
}
#Dialog_1400Logon {
	margin-left: auto;
	width: 85%; 
	left: 7%;
}

#dialog_logon_dialog{
	margin-left: auto;
	width: 85%; 
	left: 7%;
}

#swhOutOpeSelectLbl{
	margin-left: 29px;
}

#boxInfoDiv {
	width: 97%;
}
</style>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<link rel="stylesheet" media="screen"
	href="css/dialog/1000/defectJudge2.css">
<link href="lib/HubSpot-messenger/build/css/messenger-theme-future.css"
	rel="stylesheet" media="screen" />
<link href="lib/HubSpot-messenger/build/css/messenger.css"
	rel="stylesheet" media="screen" />
</head>
<body>
	<div class="container-fluid">
		<div class="row" id="btnDiv">
			<div class="clo-md-12">
				<button class="btn btn-primary" id="f1_btn">
					F1
					<s:text name="F1_QUERY_TAG" />
				</button>
				<button class="btn btn-primary" id="f7_btn">
					F7
					<s:text name="MOVE_IN_TAG" />
				</button>
				<%-- 		<button class="btn btn-primary" id="process_start_btn">
                            F2
                            <s:text name="START_TAG" />
                        </button> --%>
				<button class="btn btn-primary" id="f6_btn">
					F3
					<s:text name="PRD_MOVE_IN_BOX_TAG" />
				</button>
				<button class="btn btn-primary" id="f8_btn">
					F12
					<s:text name="MOVE_OUT_TAG" />
				</button>
				<button class="btn btn-primary" id="save_buff_time_btn">
					F6
					<s:text name="SAVE_BUFF_INFO_TAG" />
				</button>
				<button class="btn btn-primary" id="qa_btn">
					F4
					<s:text name="INSPECTION_TAG" />
				</button>
				<%-- 			<button class="btn btn-primary" id="issue_move_out_btn">
                                F8
                                <s:text name="ISSUE_MOVE_OUT_TAG" />
                            </button> --%>
				<button class="btn btn-primary" id="f9_btn">
					F9
					<s:text name="PRD_CANCEL_MOVE_IN_BOX_TAG" />
				</button>
				<%-- 			<button class="btn btn-primary" id="wip_retain_btn">
                                <s:text name="WIP_RETAIN_TAG" />
                            </button>
                            <button class="btn btn-primary" id="wip_retain_cancel_btn">
                                <s:text name="WIP_RETAIN_CANCEL_TAG" />
                            </button> --%>
			</div>

		</div>

		<div class="row" id="infoDiv">
			<div class="col-lg-3 col-md-3 col-sm-6">
				<fieldset>
					<div class="form-group">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="control-label col-lg-5" for="opeSelect"><s:text
										name="OPE_ID_TAG" /></label>
								<div class="col-lg-7">
									<select id="opeSelect" class="form-control"></select>
								</div>
							</div>
							<div class="form-group" id="pg">
								<label class="control-label col-lg-5" for="toolSelect"
									id="toolSelectLb"><s:text name="EQPT_TAG" /></label>
								<div class="col-lg-7">
									<select id="toolSelect" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-5" for="woIdSelect"><s:text
										name="WORDER_ID_TAG" /></label>
								<div class="col-lg-7">
									<select id="woIdSelect" class="form-control"></select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label hide" for="ope_eff_flgTxt">原箱返回</label>
								<div class="col-lg-7 hide">
									<input type="text" class="form-control" id="ope_eff_flgTxt">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-5" for="lotIdInput"><s:text
										name="LOT_ID_TAG" /></label>
								<div class="col-lg-7">
									<input type="text" class="form-control" id="lotIdInput">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-5" for="ppboxIdTxt">来料箱号</label>
								<div class="col-lg-7">
									<input type="text" class="form-control" id="ppboxIdTxt">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-5" for="prdIdInput"><s:text
										name="PRODUCT_ID_TAG" /></label>
								<div class="col-lg-7">
									<input type="text" class="form-control" id="prdIdInput">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-5" for="prdInBoxTxt">转入箱号</label>
								<div class="col-lg-7">
									<input type="text" class="form-control" id="prdInBoxTxt">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-5" for="moveInBoxTxt">入账箱号</label>
								<div class="col-lg-7">
									<input type="text" class="form-control" id="moveInBoxTxt">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-5" for="gradeSelect"><s:text
										name="产品等级" /></label>
								<div class="col-lg-7">
									<select id="gradeSelect" class="form-control">
									  <option value="OK">正常品</option>
					                  <option value="FX">风险品</option>
					                  <option value="GK">管控品</option>
					                  <option value="MR">MRB品</option>
					                  <option value="HG">合格品</option>
					                  <option value="LW">裂纹品</option>
					                  <option value="PP">破片管控</option>
					                </select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-5" for="judgeUserID">检验人员</label>
								<div class="col-lg-7">
									<input type="text" class="form-control" id="judgeUserID">
								</div>
							</div>
							<%--
	                    <label class="control-label" for="BoxStatusTxt">箱子状态</label>
	                    <div class="controls">
	                      <input type="text" class="span2" id="BoxStatusTxt" disabled>
	                    </div>
	                  --%>
						</form>
					</div>
				</fieldset>
			</div>
			<div class="col-lg-5 col-md-4 col-sm-6">
				<fieldset>
			<!-- 		<div class="form-inline col-lg-offset-1" style="float: left"
						id="wip_wait_div">
						<input id="wip_wait_chk" type="checkbox"> 在制等待
					</div> -->
					<div class="col-lg-12">
						<label class="col-lg-2 form-width col-lg-offset-6"> <s:text
								name="MAX_COUNT_TAG" /> &nbsp;&nbsp;
						</label> 
						<label class="col-lg-2 form-width"> <s:text name="CUR_COUNT_TAG" />
							&nbsp;&nbsp;
						</label>
						 <label class="col-lg-2 form-width"> <s:text name="TOTAL_MODE_CNT" />
						</label>
					</div>
 					<div class="col-lg-12">

						<button id="okOutBtn" class="btn btn-primary col-lg-1">箱号</button>
						<div class="form-width col-lg-4 ">
							<input id="okOutCrrTxt" type="text"
								class=" form-control" placeholder="出账箱号"/>
						</div>
						<div class="form-width col-lg-2 ">
							<input id="OkBoxMaxCntTxt" type="text"
								class=" form-control" placeholder="最大数量"/>
						</div>
						<div class="form-width col-lg-2">
							<input id="OkBoxCurCntTxt" type="text"
								class=" form-control" placeholder="当前数量"/>
						</div>
						<div class="form-width col-lg-2 ">
							<input id="OkBoxModeCntTxt" type="text"
								class=" form-control" placeholder="总粒数"/>
						</div>
					</div>  
					<div class="form-inline" id="gradeSelectDiv">
						<div class="btn-group">
							<button class="btn btn-success hide" id="gradeSelectBtnOK">正常品</button>
							<button class="btn btn-warning hide" id="gradeSelectBtnGK">管控品</button>
							<button class="btn btn-danger hide" id="gradeSelectBtnLZ">留滞品</button>
							<button class="btn btn-info hide" id="gradeSelectBtnNG">不良品</button>
							<button class="btn btn-inverse hide" id="gradeSelectBtnSC">报废品</button>
							<button class="btn btn-primary hide" id="gradeSelectBtnRM">RM</button>
							<button class="btn btn-primary hide" id="gradeSelectBtnRW">RW</button>
						</div>
					</div>
					<div class="col-lg-7">



						<div class="col-lg-12">
							<div class="col-lg-1">
								<input type="checkbox" id="swhOpeChk"/>
							</div>

							<label class="control-label col-lg-5 " for="swhPathSelect">异转站点
							</label>
							<div class="col-lg-5">
								<select id="swhPathSelect" class="form-control"></select>
							</div>
						</div>
						
						<div class="col-lg-12 hide">
							<label id="swhOutOpeSelectLbl" class="control-label col-lg-5 col-lg-offset-1 " for="swhOutOpeSelect">返回站点</label>&nbsp; 
							<div class="col-lg-5">
	                   	 		<select id="swhOutOpeSelect" class="form-control"></select>
	                   	 	</div>  
						</div>
					</div>


					<textarea id="remarkTxt" class="col-lg-5" rows="2"
						placeholder="返工原因"></textarea>
					<div id="buffInfoDiv">
						<div class="col-lg-12 row" id="toolGrpDiv">
							<label class="control-label col-lg-3" for="buffToolGrpSelect"><s:text
									name="抛光信息" /></label>
									<%-- <div class="col-lg-5">
									 <select id="buffToolGrpSelect" 
								class="form-control "></select>
						</div> --%>
						</div>
						<div id="tftToolDiv" class="row">
							<label class="control-label col-lg-3 form-width" for="tftBuffTxt"> <s:text
									name="TFT_BUFF_TIME_TAG" /> (<s:text name="MINUTE_TAG" />)
							</label> 
							<div class="col-lg-3">
							<input type="text" id="tftBuffTxt" class="form-control " maxlength="4">
							</div>
							<span> <label class="control-label  col-lg-2"
								for="tftToolSelect"><s:text name="EQPT_TAG" /></label> 
								<div class="col-lg-4">
								<select id="tftToolSelect" class="form-control "></select>
								</div>
							</span>
						</div>
						<div id="cfToolDiv" class="row">
							<label class="control-label col-lg-3 form-width" for="cfBuffTxt"> <s:text
									name="CF_BUFF_TIME_TAG" /> (<s:text name="MINUTE_TAG" />)&nbsp;&nbsp;
							</label> 
							<div class="col-lg-3">
							<input type="text" id="cfBuffTxt" class="form-control " maxlength="4">
							</div>
							<span > <label class="control-label col-lg-2"
								for="cfToolSelect"><s:text name="EQPT_TAG" /></label> 
								<div class="col-lg-4">
								<select id="cfToolSelect" class="form-control"></select>
								</div>
							</span>
						</div>
					</div>
					<div id="yieldInfoDiv">
						<div class="form-inline">
							<label class="control-label" for="procDefTxt"> <s:text
									name="PROCESS_DEFECT_TAG" />
							</label> <input type="text" id="procDefTxt" class="form-control " >
							</div>
							<div class="form-inline">
							<label class="control-label" for="cusDefTxt"> <s:text
									name="CUS_MTRL_DEFECT_TAG" />
							</label> <input type="text" id="cusDefTxt" class="form-control " >
						</div>
					<%-- 	<div class="form-inline">
							<label class="control-label" for="procYieldTxt"> <s:text
									name="PROCESS_YIELD_TAG" />
							</label> <input type="text" id="procYieldTxt" class="form-control " >
							<label class="control-label" for="wholeYieldTxt"> <s:text
									name="WHOLE_YIELD_TAG" />
							</label> <input type="text" id="wholeYieldTxt" class="form-control " >
						</div> --%>
					</div>
					<%-- <div id ="remarkDiv">
						<div class="form-inline">
							<label class="control-label" for="remarkTxt">
								123<s:text name="REMARK_TAG"/>
							</label>
                    		<!-- <textarea id="remarkTxt" rows="1"></textarea> -->
						</div>
					</div> --%>

				</fieldset>
			</div>
			<div class="col-lg-4 col-md-5 col-sm-6">
				<fieldset>
					<div class="col-lg-12 col-sm-6">
						<div class="form-group">
							<label class="control-label col-lg-5" for="woPlnQtyTxt"><s:text
									name="WO_PLN_QTY_TAG" /></label>
							<div class="col-lg-7">
								<input type="text" class="form-control" id="woPlnQtyTxt"
								>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="woRlQtyTxt"><s:text
									name="WO_RL_QTY_TAG" /></label>
							<div class="col-lg-7">
								<input type="text" class="form-control" id="woRlQtyTxt"
								>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="woLevQtyTxt"><s:text
									name="WO_LEVEL_QTY_TAG" /></label>
							<div class="col-lg-7">
								<input type="text" class="form-control" id="woLevQtyTxt"
								>
							</div>
						</div>
						<div class= "form-inline hide" id="hideDiv">
							<div class="form-inline">
								<div class="form-group">
									<label class="control-label" for="defectPrdSeqIDTxt"> <s:text
											name="PRODUCT_ID_TAG" /></label> <input type="text"
																					class="form-control" id="defectPrdSeqIDTxt"
																					style="width: 70px">
								</div>
								<div class="form-group">
									<label class="control-label" for="from_thicknessTxt"><s:text
											name="FROM_THICKNESS_TAG" /></label> <input type="text"
																						class="form-control" id="from_thicknessTxt"  style="width: 70px">
								</div>
								<div class="form-group">
									<label class="control-label" for="t_thicknessTxt">T侧厚度</label> <input
										type="text" class="form-control" id="t_thicknessTxt"  style="width: 70px">
								</div>
							</div>
							<div class="form-inline">
								<div class="form-group">
									<label class="control-label" for="defectCntSumTxt">不良总数</label> <input
										type="text" class="form-control" id="defectCntSumTxt"
										style="width: 70px">
								</div>
								<div class="form-group">

									<label class="control-label" for="to_thicknessTxt"><s:text
											name="TO_THICKNESS_TAG" /></label> <input type="text"
																					  class="form-control" id="to_thicknessTxt"  style="width: 70px">
								</div>
								<div class="form-group">
									<label class="control-label" for="c_thicknessTxt">C侧厚度</label> <input
										type="text" class="form-control" id="c_thicknessTxt"  style="width: 70px">
								</div>
							</div>
						</div>
						<div class="form-inline">
							<label class="control-label" for="defOpeSelect"><s:text
									name="OPE_ID_TAG" /></label> <select id="defOpeSelect"
																		 style="width: 100px"></select> <select id="print1Select"
																												style="width: 100px"></select> <select id="print2Select"
																																					   style="width: 100px"></select>
						</div>
					</div>


				</fieldset>
			</div>
			<div class="col-lg-4 col-sm-12">
				<div id="defectInfoDiv" class="form-inline">
					<table id="defectInfoGrd"></table>
				</div>
			</div>
		</div>

		<div class="row" id="wipHightDiv" style="height: 400px;">
			<div class="col-lg-12 col-md-12 col-sm-12" id="wipInfoDiv">
				<table id="wipInfoGrd"></table>
				<div id="wipInfoPg"></div>
			</div>
		</div>
		</div>
<div class="modal fade" id="spcDialog" tabindex="-1" role="dialog"
   aria-labelledby="spcLabel" aria-hidden="true">
   <div class="modal-dialog" style="width:100%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title" id="spcLabel">
						<span
							style="float: left; font-size: 12px; font-family: sans-serif; font-weight: normal">
							面板ID<input id="prd_seq_id" type="text" disabled
							style="width: 100px"> 站点<input id="ope_id" type="text"
							disabled style="width: 100px">
						</span> <span>SPC数据查询及上报</span>
					</h4>
				</center>
			</div>
			<div class="modal-body" id="spcDialogDiv">
				<div class="row">
					<div id="dataItemEditDiv" class="col-lg-6"></div>
					<div id="spcDiv" class="col-lg-5">
						<table id="spcDialog_woGrd"></table>
						<div id="spcDialog_woGrdPg"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a id="spcDialog_reportBtn" class="btn btn-info"
					data-dismiss="modal"> 上报数据 </a> <a id="spcDialog_cancelBtn"
					class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
				</a>
			</div>
		</div>
     </div>
</div>
<div class="modal fade" id="remarkDtlDialog" tabindex="-1" role="dialog"
   aria-labelledby="remarkLabel" aria-hidden="true">
   <div class="modal-dialog" style="width:80%">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <center>
            <h4 class="modal-title" id="remarkLabel">
						<span>备注信息查看</span>
					</h4>
				</center>
			</div>
			<div class="modal-body" id="remarkDtlDialogDiv">
				<div id="remarkDtlDiv" class="col-lg-12">
					<table id="remarkDtlGrd"></table>
					<div id="remarkDtlGrdPg"></div>
				</div>
			</div>
			<div class="modal-footer">
				<a id="remarkDtl_cancelBtn" class="btn" data-dismiss="modal">
					关闭 </a>
			</div>
		</div>
		</div>
		</div>
		
		<!-- </div>  -->
		<div class="modal fade" id="Dialog_1400Logon" tabindex="-1" role="dialog" aria-hidden="true" >
		   <div id="dialog_logon_dialog" class="modal-dialog">
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close"
		               data-dismiss="modal" aria-hidden="true">
		                  &times;
		            </button>
		            <center>
		            <h4 class="modal-title">
		           WAIT箱号查询并批量入账
		        </h4>
		      </center>
		    </div>
		    <!--Modal header-->
		    <div class="modal-button" >
		      <button id="Dialog_cancel_btn"  class="btn" data-dismiss="modal" >
		        <s:text name="CANCEL_TAG" />
		      </button>
		      <button id="Dialog_move_in_btn" class="btn btn-primary">
		        <s:text name="MOVE_IN_TAG" />
		      </button>
		    </div>
		    <!--Modal button--> 
			<div class="modal-body">
				<form class="row form-horizontal">
                   	<div class="form-group">
						<label for="to_thicknessTxt" class="col-lg-2 control-label"><s:text name="BOX_ID_TAG"/></label>
						<div class="col-lg-7">
							<input type="text" id="1400Logon_boxIdInput" class="form-control"/>
						</div>
					</div>
				   	<div class="col-lg-9" id="boxInfoDiv">
			        	<table id="boxInfoGrd"></table>
		           		<div id="boxInfoPg"></div>
			    	</div>  
				</form>
			 
			</div>
			<!--Modal body-->
		</div>
		</div>
		</div>
		
		</body>

		<!-- Dialog-->
		<%@ include file="/page/dialog/1000/defectJudge2.html"%>
		<!-- Le javascript
    ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<%@ include file="/page/comPage/comJS.html"%>
		<script src="lib/underscore/underscore-min.js"></script>
		<script src="js/dialog/showSimpleInputDialog.js"></script>
		<script src="js/dialog/1000/defectJudge2.js"></script>
		<script src="lib/HubSpot-messenger/build/js/messenger.min.js"></script>
		<script src="js/dialog/showMessenger.js"></script>
		<script src="js/page/1000/1400.js"></script>
		<script type="text/javascript"
			src="js/dialog/1000/wip_retain_Dialog.js"></script>
		<jsp:include page="/page/dialog/1000/wip_retain_Dialog.jsp"></jsp:include>
		<jsp:include page="/page/dialog/1000/1400Logon.jsp"></jsp:include>
		<script type="text/javascript" src="js/dialog/1000/1400Logon.js"></script>
		<script type="text/javascript" src="<%=basePath%>js/com/printl.js"></script>
		<script type="text/javascript"
			src="js/dialog/showFabSnDetailInfoDialog.js"></script>
		<script type="text/javascript" src="js/com/Regex.js"></script>
		<script src="js/com/spcThicknessRule.js"></script>

</html>
