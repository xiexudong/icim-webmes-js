<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
   <head>
      <base href="<%=basePath%>">
      <title><s:text name="M_1600_TITLE_TAG"/></title>
	  <%@ include file="/page/comPage/comCSS.html"%>
      <%@ include file="/page/comPage/navbar.html"%>
      <style>
      	#pnlInfoControlDiv,#pnlInfoGrdDiv{
      		width:97%;
      	}
      	#remarkInput{
      		height:50px;
      	}
      	#btnDiv {
	       position: relative;
	       top: -5px;
         }
      </style>
	      
   </head>
   <body>
   <div class="container-fluid">
   <div class="row" id="btnDiv">
       <button class="btn btn-danger" id="delete_btn"><s:text name="DELETE_TAG"/> </button>
       <button class="btn btn-primary" id="clear_btn"><s:text name="F2_CLEAR_TAG"/></button>
       <button class="btn btn-warning" id="add_btn"><s:text name="ADD_TAG"/> </button>
       <button class="btn btn-info" id="scrp_btn"><s:text name="SCRP_TAG"/> </button>
       <button class="btn btn-info" id="scrp_cancel_btn"><s:text name="SCRP_CANCEL_TAG"/></button>
       <button class="btn btn-warning" id="query_all_btn"><s:text name="QUERY_ALL_TAG"/></button>
   </div>
   <div class="col-lg-12">
   	   <legend><s:text name="SCRP_INFO_TAG"/></legend>
   </div>
   <div class="row">
      <fieldset>
        <div class="col-lg-12">
            <form class="form-horizontal">
            	<div class="col-lg-3">
            		<label class="control-label keyCss" for="pnlIDInput"><s:text name="PRD_SEQ_ID_TAG"/></label>
	               <div class="controls">
	                 <input id="pnlIDInput" type="text">
	               </div>
	               
	               <label class="control-label" for="boxIDInput"><s:text name="BOX_ID_TAG"/></label>
	               <div class="controls">
	                 <input id="boxIDInput" type="text">
	               </div>
            	</div>
            	<div class="col-lg-3">
            		<label class="control-label" for="remarkInput"><s:text name="REMARK_TAG"/></label>
                <div class="controls">
                   <textarea class="input-xlarge" id="remarkInput" rows="3"></textarea>
                </div>
            	</div>
            </form>
        </div>
      </fieldset>
   </div>
    <div class="col-lg-12">
   		<legend><s:text name="PNL_INFO_TAG"/></legend>
    </div>
    <div id="pnlInfoControlDiv" class="col-lg-12">
    	<div id="pnlInfoGrdDiv" class="col-lg-12">
	       <table id="pnlInfoGrd"></table>
	       <div id="pnlInfoPg"></div>
	    </div>	
    </div>
    </div>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <%@ include file="/page/comPage/comJS.html"%> 
    <script src="js/page/1000/1600.js" ></script>
 
   </body>
</html>
