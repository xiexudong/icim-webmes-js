<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_1700_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%></head>
<style>
#boxDetailControlDiv, #boxDetailDiv {
	width: 97%;
}
#remarkInput{
   
}
</style>
<body>
	<div class="container-fluid">
		<div class="col-lg-12">
			<button class="btn btn-primary" id="f1_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="f7_btn">
				<s:text name="HOLD_TAG" />
			</button>
		</div>

		<div class="row">
			<div class="col-lg-3">
				<fieldset>
					<legend>
						<s:text name="PPBOX_INFO" />
					</legend>
					<!-- 	<div class="row control-group"> -->
					<form class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-lg-5 keyCss" for="ppBoxIDInput"> <s:text
									name="BOX_ID_TAG" />
							</label>
							<div class="controls col-lg-7">
								<input id="ppBoxIDInput" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="holdReasonSelect"> <s:text
									name="HOLD_REASON_TAG" />
							</label>
							<div class="controls col-lg-7">
								<select id="holdReasonSelect" class="form-control"></select>
							</div>
						</div>
					</form>
					<!-- </div> -->
				</fieldset>
			</div>
			<div class="col-lg-5">
				<fieldset>
					<legend>
						<s:text name="REMARK_TAG" />
					</legend>
					<div class="controls">
						<textarea id="remarkInput" class="col-lg-6" rows="3"></textarea>
					</div>
				</fieldset>

			</div>
		</div>

		<div id="boxDetailControlDiv" class="col-lg-12">
			<legend>
				<s:text name="PRODUCT_ID_TAG" />
			</legend>
			<div class="col-lg-12 row" id="boxDetailDiv">
				<table id="boxDetailGrd"></table>
				<div id="boxDetailPg"></div>
			</div>
		</div>
	</div>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/1000/1700.js"></script>

</body>
</html>