<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_1200_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<style>
#boxControlsDiv {
	width: 97%;
}

#boxInfoDiv {
	width: 97%;
}

div .form-width {
	padding-left: 0px;
	padding-right: 0px;
}

form .form-group {
	margin: 5px 0px;
}
</style>

</head>
<body>
	<div class="container">
		<div class="container-fluid">
			<button class="btn btn-primary" id="f1_btn">
				F1
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="f8_btn">
				F2
				<s:text name="MOVE_IN_TAG" />
			</button>
		</div>

		<div class="container-fluid">
			<div class="col-lg-4">
				<fieldset>
					<legend>
						<s:text name="WORK_ZONE_TAG" />
					</legend>
					<form class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-lg-5" for="opeSelect"><s:text name="OPE_ID_TAG" /> </label>
							<div class="controls col-lg-7">
								<select id="opeSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="lotIdInput"><s:text name="LOT_ID_TAG" /> </label>


							<div class="controls col-lg-7">
								<input type="text" class="form-control" id="lotIdInput">
							</div>
						</div>
						<!-- <label class="control-label keyCss" for="userIdInput"><s:text name="USER_ID_TAG"/></label>
	                    <div class="controls">
	                    	<input type="text" class="span2" id="userIdInput">
	                    </div> -->
						<div class="form-group">
							<label class="control-label col-lg-5" for="boxIdInput"><s:text name="BOX_ID_TAG" /> </label>
							<div class="controls col-lg-7">
								<input type="text" class="form-control" id="boxIdInput">
							</div>
						</div>
					</form>
				</fieldset>
			</div>
			<div class="form-horizontal col-md-4">
				<fieldset>
					<legend>
						<s:text name="BOX_ID_TAG" />
					</legend>
					<form class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-lg-5" for="woIdSelect"><s:text name="WORDER_ID_TAG" /> </label>
							<div class="controls col-lg-7">
								<select id="woIdSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="from_thicknessTxt"><s:text name="FROM_THICKNESS_TAG" /> </label>
							<div class="controls col-lg-7">
								<input type="text" class="form-control" id="from_thicknessTxt" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="to_thicknessTxt"><s:text name="TO_THICKNESS_TAG" /> </label>
							<div class="controls col-lg-7">
								<input type="text" class="form-control" id="to_thicknessTxt" disabled>
							</div>
						</div>
						<div class="form-group" hidden="true">
							<label class="control-label col-lg-5" for="woPlnQtyTxt"><s:text name="WO_PLN_QTY_TAG" /> </label>
							<div class="controls col-lg-7">
								<input type="text" class="form-control" id="woPlnQtyTxt" disabled>
							</div>
						</div>
						<div class="form-group" hidden="true">
							<label class="control-label col-lg-5" for="woRlQtyTxt"><s:text name="WO_RL_QTY_TAG" /> </label>
							<div class="controls col-lg-7">
								<input type="text" class="form-control" id="woRlQtyTxt" disabled>
							</div>
						</div>
						<div class="form-group" hidden="true">
							<label class="control-label col-lg-5" for="woLevQtyTxt"><s:text name="WO_LEVEL_QTY_TAG" /> </label>
							<div class="controls col-lg-7">
								<input type="text" class="form-control" id="woLevQtyTxt" disabled>
							</div>
						</div>
					</form>
				</fieldset>
			</div>
		</div>
		<div id="boxControlsDiv" class="container-fluid">
			<fieldset>
				<legend>
					<s:text name="PPBOX_INFO" />
				</legend>
				<div class="span12" id="boxInfoDiv">

					<table id="boxInfoGrd"></table>
					<div id="boxInfoPg"></div>
				</div>
			</fieldset>
		</div>

	</div>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/1000/1200.js"></script>
</body>
</html>
