<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>

	">

<title><s:text name="M_6A00_MEASUREMENT_MANAGEMENT_TAG" /></title>

<!-- Bootstrap -->

<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#measureListDiv {
	position: absolute;
	height: 70%;
}

#measureConditionForm .control-group {
	margin-bottom: 3px;
}

#measureItemListDiv {
	position: absolute;
	height: 55%;
}

.keyCss:after {
	float: right;
	content: '*';
	color: red;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

</head>

<body>

	<div class="container">

		<button class="btn btn-info" id="f1_query_btn">

			<s:text name="F1_QUERY_TAG" />

		</button>

		<button class="btn btn-danger" id="f4_del_btn">

			<s:text name="F4_DELETE_TAG" />

		</button>

		<!-- <button class="btn btn-warning" id="f5_upd_btn">

			<s:text name="F5_UPDATE_TAG"/>

		</button> -->

		<button class="btn btn-primary" id="f6_add_btn">

			<s:text name="F6_ADD_TAG" />

		</button>

		<button class="btn btn-info" id="f9_save_btn">
			<s:text name="F9_SAVE_BTN" />
		</button>

		<button class="btn btn-warning" id="f10_clear_btn">

			<s:text name="F10_CLEAR_TAG" />

		</button>



	</div>

	<div class="container">

		<div class="container-fluid">

			<div class="row-fluid">

				<div class="span12">

					<h3>

						<s:text name="MEASUREMENT_INFO_TAG" />

					</h3>

				</div>

			</div>

		</div>

		<div id="measureListDiv" class="col-md-3">

			<table id="measureListGrd"></table>

			<div id="measureListPg"></div>

		</div>


		<div class="col-md-offset-4 col-md-8">

			<form id="measureConditionForm" class="form-horizontal">
				<div class="row">
					<div class="control-group col-sm-6">
						<label class="control-label col-sm-4 keyCss " for="toolIDSel"> <s:text name="TOOL_ID_TAG" />
						</label>
						<div class="col-sm-6">
							<select id="toolIDSel" class="form-control"></select>
						</div>
					</div>
					<div class="control-group col-sm-6">
						<label class="control-label col-sm-4 keyCss" for="repUnitSel"> <s:text name="REP_UNIT_TAG" />
						</label>
						<div class="col-sm-6">
							<select id="repUnitSel" class="form-control"></select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="control-group col-sm-6">
						<label class="control-label col-sm-4 keyCss" for="dataPatSel"> <s:text name="DATA_PAT_TAG" />
						</label>
						<div class="col-sm-6">
							<select id="dataPatSel" class="form-control"></select>
						</div>
					</div>

					<div class="control-group col-sm-6">
						<label class="control-label col-sm-4 keyCss" for="mesIDSel"> <s:text name="MES_ID_TAG" />
						</label>
						<div class="col-sm-6">
							<select id="mesIDSel" class="form-control"></select>
						</div>
					</div>
				</div>
			</form>
			<button class="btn btn-primary col-md-offset-1" id="add_measureItem_btn">
				<s:text name="ADD_TAG" />
			</button>
			<button class="btn btn-primary " id="del_measureItem_btn">
				<s:text name="DELETE_TAG" />
			</button>
			<div id="measureItemListDiv" class="col-sm-12">
				<table id="measureItemListGrd"></table>
				<div id="measureItemListPg"></div>
			</div>
		</div>



	</div>



	<!-- 查询对话框 -->
	<div id="queryMeasureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">

						<s:text name="QUERY_CONDITION_TAG" />

					</h4>

				</div>

				<!--Modal header-->

				<div class="modal-body">

					<div class="row">
						<form id="queryMeasureDialog_MeasureForm" class="form-horizontal">

							<div class="control-group ">
								<label class="control-label col-xs-4" for="queryMeasureDialog_ToolIDTxt"> <s:text name="TOOL_ID_TAG" />
								</label>
								<div class="col-xs-7 form-inline">
									<input id="queryMeasureDialog_ToolIDTxt" class="form-control" /> <span class="help-inline"> 模糊查询请使用<span style="color: red; font-weight: bold;">*</span>
									</span>
								</div>
							</div>
							<div class="control-group ">
								<label class="control-label col-xs-4" for="queryMeasureDialog_repUnitTxt"> <s:text name="REP_UNIT_TAG" />
								</label>
								<div class="col-xs-7 form-inline">
									<input id="queryMeasureDialog_repUnitTxt" class="form-control" /> <span class="help-inline"> 模糊查询请使用<span style="color: red; font-weight: bold;">*</span>
									</span>
								</div>
							</div>
							<div class="control-group ">
								<label class="control-label col-xs-4" for="queryMeasureDialog_dataPatTxt"> <s:text name="DATA_PAT_TAG" />
								</label>
								<div class="col-xs-7 form-inline">
									<input id="queryMeasureDialog_dataPatTxt" class="form-control" /> <span class="help-inline"> 模糊查询请使用<span style="color: red; font-weight: bold;">*</span>
									</span>
								</div>
							</div>
							<div class="control-group ">
								<label class="control-label col-xs-4" for="queryMeasureDialog_mesIDTxt"> <s:text name="MES_ID_TAG" />
								</label>
								<div class="col-xs-7 form-inline">
									<input id="queryMeasureDialog_mesIDTxt" class="form-control" /> <span class="help-inline"> 模糊查询请使用<span style="color: red; font-weight: bold;">*</span>
									</span>
								</div>
							</div>

						</form>

					</div>

				</div>

				<!--Modal body-->

				<div class="modal-footer">

					<a id="queryMeasureDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />

					</a> <a id="queryMeasureDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />

					</a>

				</div>
			</div>
		</div>

	</div>
	<!-- 添加或修改Q-time子项目-->
	<div id="measureItemDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="ADD_MEASUREMENT_ITEM_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div class="row">
						<form id="dialogMeasurementItemForm" class="form-horizontal">
							<div class="control-group">
								<label class="control-label col-xs-4 keyCss" for="dataGroupTxt"> <s:text name="DATA_GROUP_TAG" />
								</label>
								<div class="col-xs-6">
									<select id="dataGroupSel"></select>
								</div>
								<div class="col-xs-2">
									<span id="dataGroupSpn" class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="dataDscTxt"> <s:text name="DATA_DSC_TAG" />
								</label>
								<div class="col-xs-6">
									<input type="text" id="dataDscTxt" class="form-control" />
								</div>
							</div>

							<div class="control-group">
								<label class="control-label col-xs-4" for="dataGroupSeqTxt"> <s:text name="DATA_GROUP_SEQ_TAG" />
								</label>
								<div class="col-xs-6">
									<input type="text" id="dataGroupSeqTxt" class="form-control" />
								</div>
							</div>
							<div class="control-group ">
								<label class="control-label col-xs-4 keyCss" for="dataAttrSel"> <s:text name="DATA_ATTR_TAG" />
								</label>
								<div class="col-xs-6">
									<select id="dataAttrSel"></select>
								</div>
							</div>
							<div class="control-group ">
								<label class="control-label col-xs-4" for="inlineCheckboxes">选择项</label>
								<div class="col-xs-offset-1 col-xs-3">
									<label class="checkbox"> <input type="checkbox" id="spcModeChk" value="option1">是否上报SPC
									</label>
								</div>
								<div class="col-xs-3">
									<label class="checkbox"> <input type="checkbox" id="specChkFlgChk" value="option1"> <s:text name="SPEC_CHK_FLG_TAG" />
									</label>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="uSpecTxt"> <s:text name="U_SPEC_TAG" />
								</label>
								<div class="col-xs-6">
									<input type="text" id="uSpecTxt" class="form-control" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="lSpecTxt"> <s:text name="L_SPEC_TAG" />
								</label>
								<div class="col-xs-6">
									<input type="text" id="lSpecTxt" class="form-control" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="ruleSel"> 规格标准 </label>
								<div class="col-xs-6">
									<select id="ruleSel">
										<option value="0">绝对值</option>
										<option value="1">来料厚度的相对值</option>
										<option value='2'>产出厚度的相对值</option>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="measureMentItemDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="measureMentItemDialog_addMeasureItemBtn" class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>


</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6A00.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>

<!-- <script type="text/javascript" src="js/dialog/6000/bis_data_6400_Dialog.js" ></script> -->

</html>