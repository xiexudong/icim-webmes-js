<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>

<%@taglib prefix="s" uri="/struts-tags" %>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>

<html>
<head>

	<base href="<%=basePath%>
	">
	<title><s:text name="M6200_LAYOUT_MANAGEMENT_TAG"/></title>

	<!-- Bootstrap -->

	<style>
		body {
			padding-top: 60px;
			/* 60px to make the container go all the way to the bottom of the topbar */
		}

		/* .keyClass {
            color: blue;
            font-weight: bold;
        } */
		.keyCss:after {
			content: '*';
			color: red;
			/* margin-left: 2px; */
		}

		#layotListDiv {
			height: 360px;
		}

		#layotConditionForm .control-group {
			margin-bottom: 3px;
		}

		#layotLCConditionForm .control-group {
			margin-bottom: 3px;
		}

	</style>

	<%@ include file="/page/comPage/comCSS.html" %>
	<%@ include file="/page/comPage/navbar.html" %>
</head>

<body>

<div class="container" id="button-group">

	<button class="btn btn-primary" id="f1_query_btn">
		<s:text name="F1_QUERY_TAG"/>
	</button>

	<button class="btn btn-danger" id="f4_del_btn">
		<s:text name="F4_DELETE_TAG"/>
	</button>

	<button class="btn btn-warning" id="f5_upd_btn">
		<s:text name="F5_UPDATE_TAG"/>
	</button>

	<button class="btn btn-primary" id="f6_add_btn">
		<s:text name="F6_ADD_TAG"/>
	</button>
	<button class="btn btn-primary" id="f9_save_btn">
		<s:text name="F9_SAVE_BTN"/>
	</button>
	<button class="btn btn-warning" id="f10_clear_btn">
		<s:text name="F10_CLEAR_TAG"/>
	</button>
	<button id="showBtn" class="btn btn-primary">显示实物图</button>
	<button id="trunBtn" class="btn btn-primary">旋转90度</button>

</div>

<div class="container">
	<div class="container-fluid" id="tittle">
		<h3>
			<s:text name="MDL_DEF_INFO_TAG"/>
		</h3>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3" id="layotListDiv">
				<table id="layotListGrd"></table>
				<div id="layotListPg"></div>
			</div>

			<div class="col-md-7">
				<form id="layotConditionForm" class="form-horizontal">
					<div class="control-group">
						<label class="col-md-4 control-label keyCss" for="layotIDTxt"> <s:text name="LAYOUT_ID_TAG"/>
						</label>
						<div class="col-md-5">
							<input type="text" id="layotIDTxt" class="form-control">
						</div>
					</div>
					<div class="control-group">
						<label class="col-md-4 control-label" for="layotDscTxt"> <s:text name="LAYOUT_DSC_TAG"/>
						</label>
						<div class="col-sm-8">
							<input type="text" id="layotDscTxt" class="form-control">
						</div>
					</div>
					<div class="control-group">
						<label class="col-md-4 control-label keyCss" for="layotCateSel"> <s:text name="LAYOT_CATE_TAG"/>
						</label>
						<div class="col-md-5">
							<select id="layotCateSel" class="form-control"></select>
						</div>
					</div>
					<div class="control-group">
						<label class="col-md-4 control-label keyCss" for="xAxisCntTxt"> <s:text name="X_AXIS_CNT_TAG"/>
						</label>
						<div class="col-md-5">
							<input type="text" id="xAxisCntTxt" class="form-control">
						</div>
					</div>
					<div class="control-group">
						<label class="col-md-4 control-label keyCss" for="xAxisLenTxt"> <s:text name="X_AXIS_LEN_TAG"/>
						</label>
						<div class="col-md-5">
							<input type="text" id="xAxisLenTxt" class="form-control">
						</div>
					</div>
					<div class="control-group">
						<label class="col-md-4 control-label keyCss" for="yAxisCntTxt"> <s:text name="Y_AXIS_CNT_TAG"/>
						</label>
						<div class="col-md-5">
							<input type="text" id="yAxisCntTxt" class="form-control">
						</div>
					</div>
					<div class="control-group">
						<label class="col-md-4 control-label keyCss" for="yAxisLenTxt"> <s:text name="Y_AXIS_LEN_TAG"/>
						</label>
						<div class="col-md-5">
							<input type="text" id="yAxisLenTxt" class="form-control">
						</div>
					</div>

				</form>


			</div>

			<div class="col-md-7">
				<form>
					<div id="layoutDiv"></div>
				</form>
				<form>
					<div id="trunedlayoutDiv"></div>
				</form>
			</div>

		</div>
	</div>
</div>

<!-- 查询对话框 -->
<div id="querylayotDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="title">
					<s:text name="QUERY_CONDITION_TAG"/>
				</h4>
			</div>
			<!--Modal header-->

			<div class="modal-body" style="margin-bottom: 25px">
				<form id="querylayotDialog_layotLCForm" class="form-horizontal">
					<div class="control-group">
						<label class="col-sm-3 control-label" for="querylayotDialog_layotIDTxt"> <s:text
								name="MDL_ID_TAG"/>
						</label>
						<div class="col-sm-3">
							<input type="text" id="querylayotDialog_layotIDTxt" class="form-control"/>
						</div>
						<div class="col-sm-3">
								<span id="querylayotDialog_helpLineSpn" class="help-inline"> 模糊查询请使用 <span
										style="color: red; font-weight: bold;">*</span>
								</span>
						</div>
					</div>
				</form>

			</div>

			<!--Modal body-->

			<div class="modal-footer">

				<a id="querylayotDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG"/>
				</a> <a id="querylayotDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG"/>
			</a>

			</div>
		</div>
	</div>
</div>
</body>
<%@ include file="/page/comPage/comJS.html" %>
<script type="text/javascript" src="js/page/6000/6200.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>

</html>