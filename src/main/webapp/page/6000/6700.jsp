<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>

	">

<title><s:text name="M_6700_BIS_MTRL_MANAGEMENT_TAG" /></title>

<!-- Bootstrap -->

<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#mtrlListDiv {
	height: 400px;
}

#mtrlListGrd {
	position: absolute;
	height: 70%;
}

.keyCss:after {
	float: right;
	content: '*';
	color: red;
}

.row {
	margin-top: 10px;
}
#selectCusDialog_cusListDiv{
	margin-left: 90px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

</head>

<body>

	<div class="container-fluid">

		<button class="btn btn-primary" id="f1_query_btn">

			<s:text name="F1_QUERY_TAG" />

		</button>

		<button class="btn btn-danger" id="f4_del_btn">

			<s:text name="F4_DELETE_TAG" />

		</button>

		<button class="btn btn-warning" id="f5_upd_btn">

			<s:text name="F5_UPDATE_TAG" />

		</button>

		<button class="btn btn-primary" id="f6_add_btn">

			<s:text name="F6_ADD_TAG" />

		</button>

		<button class="btn btn-primary" id="f9_save_btn">
			<s:text name="F9_SAVE_BTN" />
		</button>

		<button class="btn btn-warning" id="f10_clear_btn">

			<s:text name="F10_CLEAR_TAG" />

		</button>



	</div>
	<div class="container-fluid">

		<h3>

			<s:text name="MTRL_INFO_TAG" />

		</h3>

	</div>
	<div class="container-fluid">


		<div id="mtrlListDiv" class="col-md-4">

			<table id="mtrlListGrd"></table>

			<div id="mtrlListPg"></div>

		</div>

		<div class="col-sm-4">
			<form id="mdlDefConditionForm" class="form-horizontal">
				<div class="control-group">
					<label class="control-label col-sm-5 keyCss" for="mtrlProdIDTxt"> <s:text name="MTRL_PRODUCT_ID_TAG" />
					</label>
					<div class="col-sm-7">
						<input type="text" id="mtrlProdIDTxt" class="form-control" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-sm-5 keyCss" for="mtrlCateSel"> <s:text name="MTRL_CATE_TAG" />
					</label>
					<div class="col-sm-7">
						<select id="mtrlCateSel" class="form-control"></select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-sm-5" for="mtrlProdDscTxt"> <s:text name="MTRL_PROD_DSC_TAG" />
					</label>
					<div class="col-sm-7">
						<input type="text" id="mtrlProdDscTxt" class="form-control">
					</div>
				</div>
	<%-- 			<div class="control-group">
					<label class="control-label col-sm-5" for="mtrlChgCateSel"> <s:text name="MTRL_CHANGE_CATE_TAG" />
					</label>
					<div class="col-sm-7">
						<select id="mtrlChgCateSel" class="form-control"></select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-sm-5" for="mtrlUseModeSel"> <s:text name="MTRL_USE_MODE_TAG" />
					</label>
					<div class="col-sm-7">
						<select id="mtrlUseModeSel" class="form-control"></select>
					</div>
				</div> --%>
				<div class="control-group">
					<label class="control-label col-sm-5" for="mtrlPrtSel"> <s:text name="MTRL_PART_TAG" />
					</label>
					<div class="col-sm-7">
						<select id="mtrlPrtSel" class="form-control"></select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-sm-5 keyCss" for="mtrlMkrSel"> <s:text name="MTRL_MKR_TAG" />
					</label>
					<div class="col-sm-7">
						<!-- <select id="mtrlMkrSel" class="span2"></select> -->
						<button id="addMtrlMkrBtn" class="btn btn-primary">新增</button>
						<button id="deleteMtrlMkrBtn" class="btn btn-primary">删除</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-4">
			<div id="cusListDiv">
				<table id="cusListGrd"></table>
				<div id="cusListPg"></div>
			</div>
		</div>
	</div>



	<!-- 查询对话框 -->

	<div id="queryMtrlDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>

				<!--Modal header-->

				<div class="modal-body">
					<div class="row">
						<form id="queryMtrlDialog_mtrlForm" class="form-horizontal">
							<div class="control-group ">
								<label class="control-label col-sm-4" for="queryMtrlDialog_mtrlProdIDTxt"> <s:text name="MTRL_PRODUCT_ID_TAG" />
								</label>
								<div class="col-sm-4">
									<input id="queryMtrlDialog_mtrlProdIDTxt" class="form-control" />
								</div>
								<div class="col-sm-4">
									<span id="queryMtrlDialog_helpLineSpn" class="help-inline"> 模糊查询请使用<span style="color: red; font-weight: bold;">*</span>
									</span>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="queryMtrlDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryMtrlDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>

	<div id="selectCusDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">选择客户</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div id="selectCusDialog_cusListDiv" class="row">
						<table id="selectCusDialog_cusListGrd"></table>
						<div id="selectCusDialog_cusListPg"></div>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="selectCusDialog_cancelBtn" class="btn" data-dismiss="modal"> 取消 </a> <a id="selectCusDialog_selectCusBtn" class="btn btn-primary"> 选择客户 </a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>

</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6700.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
</html>