<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>

<html>
<head>

<base href="<%=basePath%>
	">
<title><s:text name="M_6D00_TITLE_TAG" /></title>

<!-- Bootstrap -->

<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyCss:after {
	content: '*';
	color: red;
}

#layotgListDiv {
	position: absolute;
	height: 70%;
}

#layotgListGrd {
	position: absolute;
	height: 70%;
}

.control-group {
	padding-top: 40px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%></head>

<body>

	<div class="container">

		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>

		<button class="btn btn-danger" id="f4_del_btn">
			<s:text name="F4_DELETE_TAG" />
		</button>

		<button class="btn btn-warning" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG" />
		</button>

		<button class="btn btn-primary" id="f6_add_btn">
			<s:text name="F6_ADD_TAG" />
		</button>
		<button class="btn btn-primary" id="f9_save_btn">
			<s:text name="F9_SAVE_BTN" />
		</button>
		<button class="btn btn-warning" id="f10_clear_btn">
			<s:text name="F10_CLEAR_TAG" />
		</button>

	</div>

	<div class="container">
		<div class="row-fluid">
			<div class="container-fluid">
				<h3>
					<s:text name="LAYOUT_GROUP_INFO_TAG" />
				</h3>
			</div>
		</div>
	</div>
	<div class="container">

		<div id="layotgListDiv" class="col-md-3">
			<table id="layotgListGrd"></table>
			<div id="layotgListPg"></div>
		</div>

		<div class="col-md-offset-3 col-md-7">
			<form id="layotgConditionForm" class="form-horizontal">
				<div class="control-group">
					<label class="control-label col-xs-4 keyCss" for="layotgIDTxt"> <s:text name="LAYOUT_G_ID_TAG" />
					</label>
					<div class="col-xs-6">
						<input type="text" id="layotgIDTxt" class="form-control input" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-xs-4" for="layotgDscTxt"> <s:text name="LAYOUT_G_DESC_TAG" />
					</label>
					<div class="col-xs-6">
						<input type="text" id="layotgDscTxt" class="form-control input" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-xs-4 keyCss " for="layotgCateSel"> <s:text name="LAYOUT_G_CATE_TAG" />
					</label>
					<div class="col-xs-6">
						<select id="layotgCateSel" class="form-control"></select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-xs-4 keyCss" for="layotIDSel"> <s:text name="LAYOUT_ID_TAG" />
					</label>
					<div class="col-xs-6">
						<select id="layotIDSel" class="form-control"></select>
					</div>
				</div>
				<div class="control-group">
					<div class="col-xs-offset-2 col-xs-6">
						<button class="btn btn-primary" id="add_layot_btn">
							<s:text name="ADD_TAG" />
						</button>
						<button class="btn btn-primary" id="delete_layot_btn">
							<s:text name="DELETE_TAG" />
						</button>
						<span class="label label-warning">对版式的所有更改都要==>保存</span>
					</div>
				</div>
			</form>
		</div>
		<div id="layotListDiv" class="col-xs-offset-4 col-xs-7">
			<table id="layotListGrd"></table>
			<div id="layotListPg"></div>
		</div>
	</div>
	<!-- 查询对话框 -->
	<div id="querylayotgDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>

				<!--Modal header-->

				<div class="modal-body">
					<div class="row">
						<form id="querylayotgDialog_layotgForm" class="row form-horizontal">
							<div class="control-group ">
								<label class="control-label col-xs-4" for="querylayotgDialog_layotgIDTxt"> <s:text name="LAYOUT_G_ID_TAG" />
								</label>
								<div class="col-xs-6">
									<input type="text" id="querylayotgDialog_layotgIDTxt" class="form-control" /> <span id="querylayotDialog_helpLineSpn" class="help-inline"> </span>
								</div>
							</div>
						</form>
					</div>
				</div>

				<!--Modal body-->

				<div class="modal-footer">
					<a id="querylayotgDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="querylayotgDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>					
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6D00.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>

</html>