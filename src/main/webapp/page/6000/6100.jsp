<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>

	">

<title><s:text name="M6100_BIS_PATH_MANAGEMENT_TAG" /></title>

<!-- Bootstrap -->

<style>
.left{
  float:left;
}
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#pathListDiv {
	position: absolute;
	height: 75%;
}

#pathListGrd {
	position: absolute;
	height: 75%;
}

#pathItemListDiv {
	position: absolute;
	height: 30%;
}

#pathItemListGrd { /*position:absolute;*/
	height: 30%;
}

#pathItemForm {
	position: absolute;
	/*margin-top: 502px;*/
}

#pathConditionForm .control-group {
	margin-bottom: 5px;
}

#queryPamForm .control-group {
	margin-bottom: 3px;
}

#dialogpathItemForm .control-group {
	margin-bottom: 3px;
}

#qrsListForm .control-group {
	margin-bottom: 3px;
}

#dialogPamItemForm .control-group {
	margin-bottom: 3px;
}
/* #queryPathDialog {
		    margin-left: 10%;
	        left: 10%;
		  }*/
#pathItemDialog {
	margin-left: 10%;
	left: 10%;
}


#branchPathDialog, #branchPathModalDialog {
	margin-left: 10%;
	left: 4%;
	width: 85%;
}

.keyCss:after {
	float: right;
	content: '*';
	color: red;
	/* margin-left: 2px; */
}
</style>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

</head>

<body>

	<div class="container">

		<button class="btn btn-info" id="f1_query_btn">

			<s:text name="F1_QUERY_TAG" />

		</button>

		<button class="btn btn-danger" id="f4_del_btn">

			<s:text name="F4_DELETE_TAG" />

		</button>

		<button class="btn btn-primary" id="f6_add_btn">

			<s:text name="F6_ADD_TAG" />

		</button>

		<button class="btn btn-primary" id="f8_regist_btn">

			<s:text name="SAVE_TAG" />

		</button>

		<button class="btn btn-warning" id="f10_clear_btn">

			<s:text name="F10_CLEAR_TAG" />

		</button>
	</div>

	<div class="container">

		<div class="container-fluid">
			<div id="tittle">
				<h3>
					<s:text name="PATH_INFO_TAG" />
				</h3>
			</div>
		</div>

		<div class="col-md-4">

			<!-- <div class="control-group span3 row">    -->

			<div id="pathListDiv" class="col-lg-12">

				<table id="pathListGrd"></table>

				<div id="pathListPg"></div>

			</div>

			<!-- </div> -->
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<form id="pathConditionForm" class="row col-lg-12 form-horizontal">

					<div class="control-group col-lg-6">
						<label class="control-label col-lg-6 keyCss" for="pathIDTxt"> <s:text name="PATH_ID_TAG" />
						</label>
						<div class="col-lg-6">
							<input type="text" id="pathIDTxt" class="form-control" />
						</div>
					</div>
					<div class="control-group col-lg-6">
						<label class="control-label col-lg-6 keyCss" for="pathVerTxt"> <s:text name="PATH_VER_TAG" />
						</label>
						<div class="col-lg-6">
							<input type="text" id="pathVerTxt" class="form-control" />
						</div>
					</div>

					<div class="control-group col-lg-12">
						<label class="control-label col-lg-3" for="pathDscTxt"> <s:text name="PATH_DSC_TAG" />
						</label>
						<div class="col-lg-9">
							<input type="text" id="pathDscTxt" class="form-control" />
						</div>
					</div>

					<div class="control-group col-lg-6">
						<label class="control-label col-lg-6 keyCss" for="pathCateSel"> <s:text name="PATH_CATE_TAG" />
						</label>
						<div class="col-lg-6">
							<select id="pathCateSel" class="form-control"></select>
						</div>
					</div>
					<div class="control-group col-lg-6">
						<label class="control-label col-lg-6 keyCss" for="mdlCateSel">工艺属性 </label>
						<div class="col-lg-6">
							<select id="mdlCateSel" class="form-control"></select>
						</div>
					</div>
					<div class="control-group col-lg-6">
						<label class="control-label col-lg-6 keyCss" for="startBankSel"> <s:text name="START_BANK_ID_TAG" />
						</label>
						<div class="col-lg-6">
							<select id="startBankSel" class="form-control"></select>
						</div>
					</div>

					<div class="control-group col-lg-6">
						<label class="control-label col-lg-6 keyCss" for="endBankSel"> <s:text name="END_BANK_ID_TAG" />
						</label>
						<div class="col-lg-6">
							<select id="endBankSel" class="form-control"></select>
						</div>
					</div>


					<div class="control-group col-lg-6">
						<label class="control-label col-lg-6" for="rmaSel">RMA返工标识</label>
						<div class="col-lg-6">
							<select id="rmaSel" class="form-control">
								<option value="2">正常</option>
								<option value="1">返工</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="form-group row form-horizontal">
				<div class="container-fluid">
					<button class="btn btn-primary col-lg-offset-1" id="add_pathItem_btn">
						<s:text name="ADD_TAG" />
					</button>
					<button class="btn btn-primary " id="upd_pathItem_btn">
						<s:text name="UPDATE_TAG" />
					</button>
					<button class="btn btn-primary " id="del_pathItem_btn">
						<s:text name="DELETE_TAG" />
					</button>
					<button class="btn btn-primary " id="branch_pathItem_btn">
						<s:text name="BRANCH_BTN_TAG" />
					</button>
					<span class="label label-warning">对工艺路线的所有更改都要==>保存</span>
				</div>
				<div id="pathItemListDiv" class="col-lg-12">
					<table id="pathItemListGrd"></table>
					<div id="pathItemListPg"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- 查询Path -->
	<div id="queryPathDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<!-- <div class="row"> -->
					<form id="queryPathForm" class="row form-horizontal">
						<div class="control-group ">
							<label class="control-label col-lg-4" for="queryPathDialog_pathCateTxt"> <s:text name="PATH_CATE_TAG" />
							</label>
							<div class="col-lg-6">
								<input type="text" id="queryPathDialog_pathCateTxt" class="form-control" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label col-lg-4" for="queryPathDialog_pathIDTxt"> <s:text name="PATH_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<input type="text" id="queryPathDialog_pathIDTxt" class="form-control" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-lg-4" for="queryPathDialog_pathVerTxt"> <s:text name="PATH_VER_TAG" />
							</label>
							<div class="col-lg-6">
								<input type="text" id="queryPathDialog_pathVerTxt" class="form-control" />
							</div>
						</div>
					</form>
					<!-- </div> -->
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="queryPathDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryPathDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>

	<!-- 添加或修改path子项目-->
	<div id="pathItemDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="ADD_UPDATE_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<!-- <div class="row"> -->
					<form id="dialogPathItemForm" class="row form-horizontal">
						<div class="control-group">
							<label class="control-label col-lg-4 keyCss" for="pathItemDialog_crOpeNOTxt"> <s:text name="OPE_NO_TAG" />
							</label>
							<div class="col-lg-8">
								<input type="text" id="pathItemDialog_crOpeNOTxt" class="form-control">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-lg-4 keyCss" for="pathItemDialog_crOpeIDSel"> <s:text name="OPE_ID_TAG" />
							</label>
							<div class="col-sm-8 col-md-8 col-lg-8">
								<select id="pathItemDialog_crOpeIDSel" class="form-control"></select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label col-lg-4 keyCss" for="pathItemDialog_crOpeVerSel"> <s:text name="OPE_VER_TAG" />
							</label>
							<div class="col-lg-8">
								<select id="pathItemDialog_crOpeVerSel" class="form-control"></select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label col-lg-4" for="inlineCheckboxes">选择项</label>
							<div class="col-lg-3">
								<label class="checkbox-inline"> <input type="checkbox" id="pathItemDialog_swhAvlFlgChk" value="option1"> <s:text name="SWH_AVL_FLG_TAG" />
								</label>
							</div>
							<div class="col-lg-3 hidden">
								<label class="checkbox-inline"> <input type="checkbox" id="pathItemDialog_wipBankFlgChk" value="option2"> <s:text name="WIP_BANK_FLG_TAG" />
								</label>
							</div>
						</div>
						<div class="control-group hidden">
							<label class="control-label col-lg-4 keyCss" for="pathItemDialog_wipBnkIdSel"> <s:text name="WIP_BANK_ID_TAG" />
							</label>
							<div class="col-lg-5">
								<select id="pathItemDialog_wipBnkIdSel" class="form-control"></select>
							</div>
						</div>

					</form>
					<!-- </div> -->
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="pathItemDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="pathItemDialog_savePathItemBtn" class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>

	<!-- 添加分支工艺项目-->
	<div id="branchPathDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div id="branchPathModalDialog" class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="ADD_UPDATE_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">

					<div class="row">
						<div class="col-md-4">
							<form class="form-horizontal">
								<div class="control-group">
									<label class="control-label col-lg-6 keyCss" for="fr_path_id_fk"> 工艺线路</label>
									<div class="col-lg-6">
										<input type="text" id="fr_path_id_fk" class="form-control" />
									</div>
									<label class="control-label col-lg-6 keyCss" for="fr_path_ver_fk"> 工艺版本</label>
									<div class="col-lg-6">
										<input type="text" id="fr_path_ver_fk" class="form-control" />
									</div>
									<label class="control-label col-lg-6 keyCss" for="fr_ope_no_fk"> 转入站点</label>
									<div class="col-lg-6">
										<input type="text" id="fr_ope_no_fk" class="form-control" />
									</div>
								</div>
								<div class="control-group" id="pathInput">
									<label class="control-label col-lg-6 keyCss" for="swh_id"> 分支号</label>
									<div class="col-lg-6">
										<input type="text" id="swh_id" class="form-control" />
									</div>
									<label class="control-label col-lg-6 keyCss" for="swh_path_id_fk"> 分支工艺</label>
									<div class="col-lg-6">
										<input type="text" id="swh_path_id_fk" class="form-control" />
									</div>
									<label class="control-label col-lg-6 keyCss" for="swh_path_ver_fk"> 分支工艺版本</label>
									<div class="col-lg-6">
										<input type="text" id="swh_path_ver_fk" class="form-control" />
									</div>
									<label class="control-label col-lg-6 keyCss" for="swh_dsc"> 工艺描述</label>
									<div class="col-lg-6">
										<input type="text" id="swh_dsc" class="form-control" />
									</div>
									<label class="control-label col-lg-6 keyCss" for="out_ope_no_fk"> 转出站点</label>
									<div class="col-lg-6">
										<input type="text" id="out_ope_no_fk" class="form-control" />
									</div>
								</div>

							</form>
						</div>
						<div class="col-md-8">
							<form id="branchPathDialogForm" class="form-horizontal">
								<%-- <button class="btn btn-primary"
								id="branchPathDialog_add_branchItem_btn">
								<s:text name="ADD_TAG" />
							</button> --%>
								<button class="btn btn-primary" id="branchPathDialog_del_branchItem_btn">
									<s:text name="DELETE_TAG" />
								</button>
								<button class="btn btn-info" id="refreshGrid">刷新</button>
								<div id="branchPathListDiv" class="row">

									<table id="branchPathListGrd"></table>

									<div id="branchPathListPg"></div>

								</div>
							</form>
						</div>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">

					<a id="branchPathDialog_add_branchItem_btn" class="btn btn-primary left"> <s:text name="ADD_TAG" />
					</a> <a id="branchPathDialog_branch_regist_btn" class="btn btn-primary left"> <s:text name="F8_REGIST_TAG" />
					</a> <a id="branchPathDialog_cancelBtn" class="btn left" data-dismiss="modal"> 关闭 </a>
					<%-- <a id="branchPathDialog_saveBranchBtn" class="btn btn-primary"> 
					<s:text name="F5_UPATE_COMMIT_TAG" /> 
				</a> --%>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>

	<!-- 添加分支工艺项目-->
	<div id="branchItemDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="ADD_UPDATE_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<!-- <div class="row"> -->
					<form id="dialogBranchItemForm" class="row form-horizontal">
						<div class="control-group">
							<label class="control-label col-lg-4" for="branchItemDialog_swhIDTxt"> <s:text name="OPE_NO_TAG" />
							</label>
							<div class="col-lg-5">
								<input type="text" id="branchItemDialog_swhIDTxt" class="form-control" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-lg-4" for="branchItemDialog_swhPathIDSel"> <s:text name="OPE_ID_TAG" />
							</label>
							<div class="col-lg-5">
								<select id="branchItemDialog_swhPathIDSel" class="form-control"></select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label col-lg-4" for="branchItemDialog_swhPathVerSel"> <s:text name="OPE_VER_TAG" />
							</label>
							<div class="col-lg-5">
								<select id="branchItemDialog_swhPathVerSel" class="form-control"></select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label col-lg-4" for="branchItemDialog_swhDscTxt"> <s:text name="OPE_VER_TAG" />
							</label>
							<div class="col-lg-5">
								<select id="branchItemDialog_swhDscTxt" class="form-control"></select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label col-lg-4" for="branchItemDialog_outOpeNoTxt"> <s:text name="OPE_VER_TAG" />
							</label>
							<div class="col-lg-5">
								<select id="branchItemDialog_outOpeNoTxt" class="form-control"></select>
							</div>
						</div>

					</form>
					<!-- </div> -->
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="branchItemDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="branchItemDialog_saveItemBtn" class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="js/com/SelectDom.js"></script>
<script type="text/javascript" src="js/page/6000/6100.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<!-- <script type="text/javascript" src="js/dialog/6000/bis_data_6400_Dialog.js" ></script> -->

</html>