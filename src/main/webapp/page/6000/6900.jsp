<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>

	">

<title><s:text name="M6900_PARAM_MANAGEMENT_TAG" /></title>


<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#pamListDiv {
	/* position:absolute; */
	height: 70%;
}

#pamItemListDiv {
	/* position:absolute; */
	height: 30%;
}

#pamItemListGrd {
	/*position:absolute;*/
	height: 30%;
}

#pamItemForm {
	/* position:absolute; */
	/*margin-top: 502px;*/
	
}

#queryPamForm .control-group {
	margin-bottom: 3px;
}

#dialogpamItemForm .control-group {
	margin-bottom: 3px;
}

#qrsListForm .control-group {
	margin-bottom: 3px;
}

#dialogPamItemForm .control-group {
	margin-bottom: 3px;
}

#pamItemDialog {
	margin-left: 10%;
	left: 10%;
}

.keyCss:after {
	float: right;
	content: '*';
	color: red;
}

#add_pamItem_btn, #upd_pamItem_btn, #del_pamItem_btn {
	position: relative;
	top: -10px;
}
</style>
</head>

<body>

	<div class="container-fluid">


		<button class="btn btn-primary" id="f1_query_btn">

			<s:text name="F1_QUERY_TAG" />

		</button>

		<button class="btn btn-danger" id="f4_del_btn">

			<s:text name="F4_DELETE_TAG" />

		</button>

		<button class="btn btn-primary" id="f6_add_btn">

			<s:text name="F6_ADD_TAG" />

		</button>

		<button class="btn btn-warning" id="f10_clear_btn">

			<s:text name="F10_CLEAR_TAG" />

		</button>
	</div>
	<div class="container-fluid">
		<h3>
			<s:text name="PAM_INFO_TAG" />
		</h3>
	</div>
	<div class="container-fluid">
		<div id="pamListDiv" class="col-md-2">
			<table id="pamListGrd"></table>
			<div id="pamListPg"></div>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-9 row">
			<div class="col-md-12">
				<form id="pamConditionForm" class="form-horizontal">
					<label class="control-label col-xs-2 keyCss" for="mdlIDSel"> <s:text name="MDL_ID_TAG" />
					</label>
					<div class="col-xs-6">
						<select id="mdlIDSel" class="form-control"></select>
					</div>
				</form>
			</div>
			<div class="col-md-6">
				<form id="pamConditionForm" class="row form-horizontal">

					<label class="control-label col-sm-4 keyCss" for="pathIDSel"> <s:text name="PATH_ID_TAG" />
					</label>
					<div class="col-sm-8">
						<select id="pathIDSel" class="form-control"></select>
					</div>

					<label class="control-label col-sm-4 keyCss" for="pathVerSel"> <s:text name="PATH_VER_TAG" />
					</label>
					<div class="col-sm-8">
						<select id="pathVerSel" class="form-control"></select>
					</div>

					<label class="control-label col-sm-4 keyCss" for="opeNOSel"> <s:text name="OPE_NO_TAG" />
					</label>
					<div class="col-sm-8">
						<select id="opeNOSel" class="form-control"></select>
					</div>
				</form>
			</div>
			<div class="col-md-6">
				<form id="pamConditionForm" class="row form-horizontal">
					<label class="control-label col-sm-4 keyCss" for="opeIDSel"> <s:text name="OPE_ID_TAG" />
					</label>
					<div class="col-sm-8">
						<select id="opeIDSel" class="form-control"></select>
					</div>

					<label class="control-label col-sm-4 keyCss" for="opeVerSel"> <s:text name="OPE_VER_TAG" />
					</label>
					<div class="col-sm-8">
						<select id="opeVerSel" class="form-control"></select>
					</div>
				</form>
			</div>

			<div class="control-group col-md-12" style="margin-top: 25px">
				<button class="btn btn-primary offset1" id="add_pamItem_btn" value="6900-01">
					<s:text name="ADD_TAG" />
				</button>
				<button class="btn btn-primary offset1" id="upd_pamItem_btn">
					<s:text name="UPDATE_TAG" />
				</button>
				<button class="btn btn-primary offset1" id="del_pamItem_btn">
					<s:text name="DELETE_TAG" />
				</button>
				<div id="pamItemListDiv" class="col-sm-12">

					<table id="pamItemListGrd"></table>

					<div id="pamItemListPg"></div>

				</div>
			</div>
		</div>
	</div>
	<!-- 查询Q-time -->
	<div id="queryPamDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<!-- <div class="row"> -->
					<form id="queryPamForm" class="row form-horizontal">
						<div class="control-group">
							<label class="control-label col-xs-4" for="queryPamDialog_mdlIDTxt"> <s:text name="MDL_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="queryPamDialog_mdlIDTxt" class="form-control" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-xs-4" for="queryPamDialog_pathIDTxt"> <s:text name="PATH_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="queryPamDialog_pathIDTxt" class="form-control" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-xs-4" for="queryPamDialog_pathVerTxt"> <s:text name="PATH_VER_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="queryPamDialog_pathVerTxt" class="form-control" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label col-xs-4" for="queryPamDialog_opeNOTxt"> <s:text name="OPE_NO_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="queryPamDialog_opeNOTxt" class="form-control" />
							</div>
						</div>
						<div class="control-group ">
							<label class="control-label col-xs-4" for="queryPamDialog_opeIDTxt"> <s:text name="OPE_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="queryPamDialog_opeIDTxt" class="form-control" />
							</div>
						</div>

						<div class="control-group ">
							<label class="control-label col-xs-4" for="queryPamDialog_opeVerTxt"> <s:text name="OPE_VER_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="queryPamDialog_opeVerTxt" class="form-control" />
							</div>
						</div>
						<!-- FROM HERE -->
						<div class="control-group ">
							<label class="control-label col-xs-4" for="queryPamDialog_toolIDTxt"> <s:text name="TOOL_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="queryPamDialog_toolIDTxt" class="form-control" />
							</div>
						</div>
					</form>
					<!-- </div> -->
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="queryPamDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryPamDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>

	<!-- 添加或修改Q-time子项目-->
	<div id="pamItemDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="ADD_UPDATE_TAG" />
					</h4>

				</div>
				<!--Modal header-->
				<div class="modal-body">
					<!-- <div class="row"> -->
					<form id="dialogPamItemForm" class="row form-horizontal">
						<div class="control-group">
							<label class="control-label col-xs-4 keyCss" for="pamItemDialog_toolIDSel"> <s:text name="TOOL_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<select id="pamItemDialog_toolIDSel" class="form-control"></select>
							</div>
						</div>
			<%-- 			<div class="control-group">
							<label class="control-label col-xs-4" for="pamItemDialog_recipeTxt"> <s:text name="RECIPE_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="pamItemDialog_recipeTxt" class="form-control" />
							</div>
						</div> --%>

						<div class="control-group">
							<label class="control-label col-xs-4 keyCss" for="pamItemDialog_mesIDSel"> <s:text name="MES_ID_TAG" />
							</label>
							<div class="col-xs-6">
								<select id="pamItemDialog_mesIDSel" class="form-control"></select>
							</div>
						</div>
			<%-- 			<div class="control-group ">
							<label class="control-label col-xs-4" for="pamItemDialog_boxSetCodeSel"> <s:text name="BOX_SET_CODE_TAG" />
							</label>
							<div class="col-xs-6">
								<select id="pamItemDialog_boxSetCodeSel" class="form-control"></select>
							</div>
						</div> --%>

			<%-- 			<div class="control-group">
							<label class="control-label col-xs-4" for="pamItemDialog_swhCntPosSel"> <s:text name="SWH_CNT_ADD_POS_TAG" />
							</label>
							<div class="col-xs-6">
								<select id="pamItemDialog_swhCntPosSel" class="form-control"></select>
							</div>
						</div> --%>

		<%-- 				<div class="control-group">
							<label class="control-label col-xs-4" for="pamItemDialog_swhMaxCntTxt"> <s:text name="SWH_MAX_CNT_TAG" />
							</label>
							<div class="col-xs-6">
								<input type="text" id="pamItemDialog_swhMaxCntTxt" class="form-control" />
							</div>
						</div> --%>
<%-- 						<div class="control-group">
							<label class="control-label col-xs-4" for="inlineCheckboxes">选择项</label>

							<div class="col-xs-6">
								<label class="checkbox"> <input type="checkbox" id="pamItemDialog_activeFlgChk" value="option1"> <s:text name="ACTIVE_FLG_TAG" />
								</label> <label class="checkbox"> <input type="checkbox" id="pamItemDialog_skipFlgChk" value="option2"> <s:text name="SKIP_FLG_TAG" />
								</label> <label class="checkbox"> <input type="checkbox" id="pamItemDialog_gdJudgeFLgChk" value="option3"> <s:text name="GD_JUDEG_FLG_TAG" />
								</label> <label class="checkbox"> <input type="checkbox" id="pamItemDialog_defJudgeFlgChk" value="option4"> <s:text name="DEF_JUDGE_FLG_TAG" />
								</label> <label class="checkbox"> <input type="checkbox" id="pamItemDialog_swhCntAddFlgChk" value="option5"> <s:text name="SWH_CNT_ADD_FLG_TAG" />
								</label>
							</div>
						</div> --%>


					</form>
					<!-- </div> -->
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="pamItemDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="pamItemDialog_addPamItemBtn" class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>

</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6900.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<!-- <script type="text/javascript" src="js/dialog/6000/bis_data_6400_Dialog.js" ></script> -->

</html>