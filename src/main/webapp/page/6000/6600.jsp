<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>

	">

<title><s:text name="M_6600_BIS_QRS_MANAGEMENT_TAG" /></title>

<!-- Bootstrap -->

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
</head>

<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#qrsListDiv {
	height: 400px;
}

#qrsListGrd {
	position: absolute;
	height: 100%;
}

#qrsItemListDiv {
	position: absolute;
	height: 100%;
}

#qrsItemListGrd {
	/*position:absolute;*/
	height: 30%;
}

#qrsItemForm {
	position: absolute;
	/*margin-top: 502px;*/
}

#dialogQrsItemForm .form-horizontal .col-sm-6 {
	margin-bottom: 3px;
}

#dialogQrsItemForm .control-group {
	margin-bottom: 3px;
}

#qrsListForm .control-group {
	margin-bottom: 3px;
}
/*  #qrsItemDialog{
		  	margin-left: 10%;
	        left: 10%;
		  }*/
/*  #qrsItemDialog {
	        margin-left: 10%;
	        width: 70%;
	        left: 10%;
	      }*/
</style>
<body>

	<div class="container-fluid">


		<button class="btn btn-primary" id="f1_query_btn">

			<s:text name="F1_QUERY_TAG" />

		</button>

		<button class="btn btn-danger" id="f4_del_btn">

			<s:text name="F4_DELETE_TAG" />

		</button>

		<button class="btn btn-primary" id="f6_add_btn">

			<s:text name="F6_ADD_TAG" />

		</button>

		<button class="btn btn-warning" id="f10_clear_btn">

			<s:text name="F10_CLEAR_TAG" />

		</button>
	</div>
	<div class="container-fluid">
		<h3>
			<s:text name="QRS_INFO_TAG" />
		</h3>
	</div>
	<div class="container-fluid">
		<div id="qrsListDiv" class="col-md-3">
			<table id="qrsListGrd"></table>
			<div id="qrsListPg"></div>
		</div>
		<div class="col-md-9">
			<form id="qrsConditionForm" class="form-horizontal">
				<div class="row">
					<div class="col-sm-6 control-group">
						<label class="control-label col-sm-4" for="mdlIDSel"> <s:text name="MDL_ID_TAG" />
						</label>
						<div class="col-sm-8">
							<select id="mdlIDSel" class="form-control"></select>
						</div>
					</div>
					<div class="col-sm-6 control-group">
						<label class="control-label col-sm-4" for="toolIDSel"> <s:text name="TOOL_ID_TAG" />
						</label>
						<div class="col-sm-8">
							<select id="toolIDSel" class="form-control"></select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 control-group">
						<label class="control-label col-sm-4" for="pathIDSel"> <s:text name="PATH_ID_TAG" />
						</label>
						<div class="col-sm-8">
							<select id="pathIDSel" class="form-control"></select>
						</div>
					</div>
					<div class="col-sm-6 control-group">
						<label class="control-label col-sm-4" for="pathVerSel"> <s:text name="PATH_VER_TAG" />
						</label>
						<div class="col-sm-8">
							<select id="pathVerSel" class="form-control"></select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 control-group">
						<label class="control-label col-sm-4" for="opeNOSel"> <s:text name="START_OPE_NO_TAG" />
						</label>
						<div class="col-sm-8">
							<select id="opeNOSel" class="form-control"></select>
						</div>
					</div>
					<div class="col-sm-6 control-group">
						<label class="control-label col-sm-4" for="opeIDSel"> <s:text name="START_OPE_ID_TAG" />
						</label>
						<div class="col-sm-8">
							<select id="opeIDSel" class="form-control"></select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 control-group">
						<label class="control-label col-sm-4" for="opeVerSel"> <s:text name="START_OPE_VER_TAG" />
						</label>
						<div class="col-sm-8">
							<select id="opeVerSel" class="form-control"></select>
						</div>
					</div>
				</div>
			</form>
			<div class="form-horizontal">
				<div class="control-group">
					<button class="btn btn-primary" id="f8_add_qrsItem_btn" style="margin-left: 25px;">
						<s:text name="F8_ADD_QRS_BTN_TAG" />
					</button>
				</div>
				<div id="qrsItemListDiv" class="col-sm-12">
					<table id="qrsItemListGrd"></table>
					<div id="qrsItemListPg"></div>
				</div>
			</div>
		</div>
		<!-- 查询Q-time -->
		<div id="qrsListDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">
							<s:text name="QUERY_CONDITION_TAG" />
						</h4>
					</div>
					<!--Modal header-->
					<div class="modal-body">
						<div class="row">
							<form id="qrsListForm" class="form-horizontal">
								<div class="control-group ">
									<label class="control-label col-sm-4" for="qrsListDialog_mdlIDTxt"> <s:text name="MDL_ID_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsListDialog_mdlIDTxt" class="form-control" />
									</div>
								</div>
								<div class="control-group ">
									<label class="control-label col-sm-4" for="qrsListDialog_mdlTypeTxt"> <s:text name="MDL_TYPE_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsListDialog_mdlTypeTxt" class="form-control" />
									</div>
								</div>

								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsListDialog_pathIDTxt"> <s:text name="PATH_ID_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsListDialog_pathIDTxt" class="form-control" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsListDialog_pathVerTxt"> <s:text name="PATH_VER_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsListDialog_pathVerTxt" class="form-control" />
									</div>
								</div>

								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsListDialog_opeNOTxt"> <s:text name="START_OPE_NO_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsListDialog_opeNOTxt" class="form-control" />
									</div>
								</div>
								<div class="control-group ">
									<label class="control-label col-sm-4" for="qrsListDialog_opeIDTxt"> <s:text name="START_OPE_ID_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsListDialog_opeIDTxt" class="form-control" />
									</div>
								</div>

								<div class="control-group ">
									<label class="control-label col-sm-4" for="qrsListDialog_opeVerTxt"> <s:text name="START_OPE_VER_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsListDialog_opeVerTxt" class="form-control" />
									</div>
								</div>
								<!-- FROM HERE -->
								<div class="control-group ">
									<label class="control-label col-sm-4" for="qrsListDialog_toolIDTxt"> <s:text name="TOOL_ID_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsListDialog_toolIDTxt" class="form-control" />
									</div>
								</div>
							</form>
						</div>
					</div>
					<!--Modal body-->
					<div class="modal-footer">
						<a id="qrsListDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
						</a> <a id="qrsListDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
						</a>
					</div>
					<!--Modal footer-->
				</div>
			</div>
		</div>

		<!-- 添加或修改Q-time子项目-->
		<div id="qrsItemDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">
							<s:text name="ADD_QRS_TAG" />
						</h4>
					</div>
					<!--Modal header-->
					<div class="modal-body">
						<div class="row">
							<form id="dialogQrsItemForm" class="form-horizontal">
								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsIDTxt"> <s:text name="QRS_ID_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsIDTxt" class="form-control" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsTypeSel"> <s:text name="QRS_TYPE_TAG" />
									</label>
									<div class="col-sm-6">
										<select id="qrsTypeSel" class="form-control"></select>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsPathIDSel"> <s:text name="QRS_PATH_ID_TAG" />
									</label>
									<div class="col-sm-6">
										<select id="qrsPathIDSel" class="form-control"></select>
									</div>
								</div>
								<div class="control-group ">
									<label class="control-label col-sm-4" for="qrsPathVerSel"> <s:text name="QRS_PATH_VER_TAG" />
									</label>
									<div class="col-sm-6">
										<select id="qrsPathVerSel" class="form-control"></select>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsOpeNOSel"> <s:text name="QRS_OPE_NO_TAG" />
									</label>
									<div class="col-sm-6">
										<select id="qrsOpeNOSel" class="form-control"></select>
									</div>
								</div>
								<div class="control-group ">
									<label class="control-label col-sm-4" for="qrsOpeIDSel"> <s:text name="QRS_OPE_ID_TAG" />
									</label>
									<div class="col-sm-6">
										<select id="qrsOpeIDSel" class="form-control"></select>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsOpeVerSel"> <s:text name="QRS_OPE_VER_TAG" />
									</label>
									<div class="col-sm-6">
										<select id="qrsOpeVerSel" class="form-control"></select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label col-sm-4" for="qrsTimeTxt"> <s:text name="QRS_TIME_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrsTimeTxt" class="form-control" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label col-sm-4" for="qrkTimeTxt"> <s:text name="QRK_TIME_TAG" />
									</label>
									<div class="col-sm-6">
										<input type="text" id="qrkTimeTxt" class="form-control" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label col-sm-4" for="swhPathIDSel"> <s:text name="SWH_PATH_ID_TAG" />
									</label>
									<div class="col-sm-6">
										<select id="swhPathIDSel" class="form-control"></select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label col-sm-4" for="swhPathVerSel"> <s:text name="SWH_PATH_VER_TAG" />
									</label>
									<div class="col-sm-6">
										<select id="swhPathVerSel" class="form-control"></select>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!--Modal body-->
					<div class="modal-footer">
						<a id="qrsItemDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
						</a> <a id="qrsItemDialog_addQrsItemBtn" class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
						</a>
					</div>
					<!--Modal footer-->
				</div>
			</div>
		</div>

	</div>

</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6600.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<!-- <script type="text/javascript" src="js/dialog/6000/bis_data_6400_Dialog.js" ></script> -->

</html>