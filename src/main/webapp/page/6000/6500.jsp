<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%> ">

<title><s:text name="M_6500_OPE_MANAGEMENT_TAG" /></title>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#opeListDiv {
	height: 400px;
}

#opeListGrd {
	height: 74%;
}

.keyCss:after {
	float: right;
	/* font-size: 20px; */
	content: '*';
	color: red;
	/* margin-left: 2px; */
}

select input {
	margin-bottom: 10px;
}
/*#opeConditionForm .selection{
	margin-top: 10px;
} */
</style>
</head>

<body>

	<div class="container-fluid">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>

		<button class="btn btn-danger" id="f4_del_btn">
			<s:text name="F4_DELETE_TAG" />
		</button>

		<button class="btn btn-warning" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG" />
		</button>

		<button class="btn btn-primary" id="f6_add_btn">
			<s:text name="F6_ADD_TAG" />
		</button>

		<button class="btn btn-primary" id="f9_save_btn">
			<s:text name="F9_SAVE_BTN" />
		</button>
		<button class="btn btn-warning" id="f10_clear_btn">
			<s:text name="F10_CLEAR_TAG" />
		</button>
	</div>
	<div class="container-fluid">
		<h3>
			<s:text name="OPE_INFO_TAG" />
		</h3>
	</div>

	<div class="container-fluid">
		<div id="opeListDiv" class="col-md-3">
			<table id="opeListGrd"></table>
			<div id="opeListPg"></div>
		</div>
		<div class="col-md-8">
			<form id="opeConditionForm" class="form-horizontal">
				<div class="control-group">
					<label class="control-label col-xs-2 keyCss" for="opeIDTxt"> <s:text name="OPE_ID_TAG" />
					</label>
					<div class="col-xs-4">
						<input type="text" id="opeIDTxt" class="form-control input-sm">
					</div>
					<label class="control-label col-xs-2 keyCss" for="opeVerTxt"> <s:text name="OPE_VER_TAG" />
					</label>
					<div class="col-xs-4">
						<input type="text" id="opeVerTxt" class="form-control input-sm">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-xs-2" for="opeDscTxt"> <s:text name="OPE_DSC_TAG" />
					</label>
					<div class="col-xs-10">
						<input type="text" id="opeDscTxt" class="form-control">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-xs-2 keyCss" for="procIDSel"> <s:text name="PROC_ID_TAG" />
					</label>
					<div class="col-xs-4">
						<select id="procIDSel" class="form-control input-sm"></select>
					</div>
					<label class="control-label col-xs-2 keyCss" for="toolgIDSel"> <s:text name="TOOLG_ID_TAG" />
					</label>
					<div class="col-xs-4">
						<select id="toolgIDSel" class="form-control input-sm"></select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-xs-2" for="pepLvlTxt"> <s:text name="PEP_LVL_TAG" />
					</label>
					<div class="col-xs-4">
						<input type="text" id="pepLvlTxt" class="form-control">
					</div>
					<label class="control-label col-xs-2" for="deptIDSel"> <s:text name="DEPT_ID_TAG" />
					</label>
					<div class="col-xs-4">
						<select id="deptIDSel" class="form-control input-sm"></select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label col-xs-2" for="stbOpeTimeTxt"> <s:text name="STD_OPE_TIME_TAG" />
					</label>
					<div class="col-xs-4">
						<input type="text" id="stbOpeTimeTxt" class="form-control input-sm">
					</div>
				</div>
			</form>
		</div>
	</div>



	<!-- 查询对话框 -->

	<div id="queryOpeDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>

				<!--Modal header-->

				<div class="modal-body">
					<div class="row">
						<form id="queryOpeDialog_mdlDefForm" class="row form-horizontal">
							<div class="control-group ">
								<label class="control-label col-xs-4" for="queryOpeDialog_opeIDTxt"> <s:text name="OPE_ID_TAG" />
								</label>
								<div class="col-xs-5">
									<input id="queryOpeDialog_opeIDTxt" class="form-control input-sm">
								</div>
							</div>
							<div class="control-group ">
								<label class="control-label col-xs-4" for="queryOpeDialog_opeVerTxt"> <s:text name="OPE_DSC_TAG" />
								</label>
								<div class="col-xs-5">
									<input id="queryOpeDialog_opeVerTxt" class="form-control input-sm">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<a id="queryOpeDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryOpeDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6500.js"></script>

<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>

<!-- <script type="text/javascript" src="js/dialog/6000/bis_data_6400_Dialog.js" ></script> -->

</html>