<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%

String path = request.getContextPath();

String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>">

<title><s:text name="M_6C00_TITLE_TAG" /></title>
<!-- Bootstrap -->

<style>
body {
	padding-top: 60px;
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#customerListDiv {
	height: 70%;
}

#locListDiv {
	height: 90%;
}

.keyCss:after {
	float: right;
	content: '*';
	color: red;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

</head>

<body>

	<div class="container">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-danger" id="f4_del_btn">
			<s:text name="F4_DELETE_TAG" />
		</button>
		<button class="btn btn-warning" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG" />
		</button>
		<button class="btn btn-primary" id="f6_add_btn">
			<s:text name="ADD_TAG" />
		</button>
		<button class="btn btn-primary" id="f8_register_btn">
			<s:text name="F8_REGIST_TAG" />
		</button>

	</div>

	<div class="container">
		<div class="container">
			<h3>
				<s:text name="CUSTOMER_LIST_TAG" />
			</h3>
		</div>

		<div id="customerListDiv" class="col-md-4">
			<table id="customerListGrd"></table>
			<div id="customerListPg"></div>
		</div>

		<div id="tabDiv" class="col-md-8">
			<ul id="mytab" class="nav nav-tabs">
				<li class="active"><a id="tab1" href="#tabPane_user" data-toggle="tab"> <s:text name="CUSTOMER_BASE_INFO_TAG" />
				</a></li>
				<li><a id="tab2" href="#tabPane_dept" data-toggle="tab"> <s:text name="CUSTOMER_ADDRESS_INFO_TAG" />
				</a></li>
				<!-- <li><a id="tab4" href="#settings" data-toggle="tab">其他</a></li> -->
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tabPane_user">
					<!-- <div class="row span8"> -->
					<form id="CustForm" class="form-horizontal col-sm-8">
						<div class="control-group">
							<label class="control-label col-sm-4 keyCss" for="customerIDTxt"> <s:text name="CUS_ID_TAG" />
							</label>
							<div class="col-sm-7">
								<input type="text" id="customerIDTxt" class="form-control input-sm">
							</div>
						</div>

						<div class="control-group">
							<label class="control-label col-sm-4 keyCss" for="customerNameTxt"> <s:text name="CUSTOMER_NAME_TAG" />
							</label>
							<div class="col-sm-7">
								<input type="text" id="customerNameTxt" class="form-control input-sm">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-sm-4" for="customerDscTxt"> <s:text name="CUSTOMER_DSC_TAG" />
							</label>
							<div class="col-sm-7">
								<input type="text" id="customerDscTxt" class="form-control input-sm">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-sm-4" for="customerTypSel"> 客户或厂商 </label>
							<div class="col-sm-7">
								<select id="customerTypSel" class="form-control input-sm">
								</select>
							</div>
						</div>
						<div class="control-group hide">
							<label class="control-label col-sm-4" for="data_seq_id"> 主键:data_seq_id </label>
							<div class="col-sm-7">
								<input type="text" id="dataSeqIdTxt" class="form-control input-sm">
							</div>
						</div>

					</form>
					<!-- </div> -->
				</div>
				<div class="tab-pane" id="tabPane_dept">
					<div id="locListDiv" class="col-xs-11">
						<button id="addloc" class="btn btn-primary span1">新增</button>
						<div class="control-group row" id="adressDiv">
							<div id="locListDiv" class="row">
								<table id="locListGrd"></table>
								<div id="locListPg"></div>
							</div>

						</div>

					</div>

				</div>
			<!-- 	<div class="tab-pane" id="settings">other</div> -->
			</div>
		</div>
	</div>
	<!-- 更新对话框 -->

	<div id="updateLocDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="UPDATE_INFO_TAG" />

					</h4>

				</div>

				<!--Modal header-->

				<div class="modal-body">

					<div class="row">
						<form id="updateLocDialog_locForm" class="row form-horizontal">

							<div class="control-group ">

								<label class="control-label col-xs-4 keyCss" for="updateLocDialog_locTypSp"> <s:text name="ADRESS_TYPE_TAG" />

								</label>

								<div class="col-xs-6">
									<select id="updateLocDialog_locTypSel">
									</select> <span id="locTypSelDesSpn" class="help-inline"></span>
								</div>

							</div>
							<div class="control-group ">

								<label class="control-label col-xs-4 keyCss" for="updateLocDialog_locIDTxt"> <s:text name="LOC_INFO_TAG" />

								</label>
								<div class="col-xs-7 form-inline">
									<input type="text" id="updateLocDialog_locIDTxt" class="form-control" /> <span id="updateLocDialog_helpLineSpn" class="help-inline"> 请输入收货/送货地址 </span>

								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->

				<div class="modal-footer">
					<a id="updateLocDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="updateLocDialog_queryBtn" class="btn btn-primary"> <s:text name="F8_EXECUTE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>

</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="js/com/SelectDom.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script type="text/javascript" src="js/page/6000/6C00.js"></script>
</html>