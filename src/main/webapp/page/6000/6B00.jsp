<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_6B00_TITLE_TAG" />
</title>
<script type="text/javascript">
		var basePath = "<%=basePath%>";
	</script>
<!-- Bootstrap -->
<style>
	      body {
	        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
	      }
	      .displayBlockCss{
	      	display: none ;
	      }
	      #orderForm .control-group{
		     margin-bottom: 8px;
		  }
		   #bindWODialog {
	        margin-left: 10%;
	        width: 70%;
	        left: 10%;
	      }
	       .keyCss:after {
			float:right;
			content: '*';
			color: red; 
		 }
	</style>

	<%@ include file="/page/comPage/comCSS.html"%> 
	<%@ include file="/page/comPage/navbar.html"%>
</head>
<body>
	<div class="container-fluid">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-danger" id="f4_del_btn">
			<s:text name="F4_DELETE_TAG" />
		</button>
		<!-- <button class="btn btn-warning" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG" />
		</button> -->
		<button class="btn btn-primary" id="f8_regist_btn">
			<s:text name="F8_REGIST_TAG" />
		</button>
		<button class="btn btn-primary" id="f9_cancel_update_btn">
			<s:text name="CANCEL_UPDATE_BTN_TAG" />
		</button>
		<button class="btn btn-warning" id="f11_save_Changes_Btn">
			<s:text name="SAVE_CHANGES_TAG" />
		</button>

	</div>
	<div class="container-fluid">
	<!-- 输入区--开始 -->
	<div class="col-md-4">
		<div class="col-xm-12">
			<div class="control-group">
				<div class="controls">
					<center>
						<legend>
							<i class="icon-th"></i>
							<s:text name="BASE_SETTING_TAG"/>
						</legend>
					</center>
				</div>
			</div>
			<div class="col-xm-12">
				<form id="boxSettingForm" class="form-horizontal">
					<div class="control-group">
						<label class="control-label col-xs-4 keyCss" for="boxIDTxt"> <s:text name="BOX_ID_TAG" /> </label>
						<div class="col-xs-8">
							<input id="boxIDTxt" class="form-control" type="text" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-xs-4 keyCss" for="boxCateSel"> <s:text name="BOX_CATE_TAG" /> </label>
						<div class="col-xs-8">
							<select id="boxCateSel" class="form-control"></select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-xs-4 keyCss" for="boxSizeSel"> <s:text name="BOX_SIZE_TAG" /> </label>
						<div class="col-xs-8">
							<select id="boxSizeSel" class="form-control"></select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-xs-4 keyCss" for="boxSetCodeSel"> 箱子属性 </label>
						<div class="col-xs-8">
							<select id="boxSetCodeSel" class="form-control"></select>
						</div>
					</div>
		 			<div class="control-group">
						<label class="control-label col-xs-4 keyCss" for="boxMkrSel"> <s:text name="BOX_MKR_TAG" /> </label>
						<div class="col-xs-8">
							<select id="boxMkrSel" class="form-control"></select>
						</div>
					</div>
 					<div class="control-group">
						<label class="control-label col-xs-4" for="hxEmptyBoxChk">007客户预置空箱</label>
						<div class="col-xs-8">
							<input id="hxEmptyBoxChk" type="checkbox" class="form-control"/>
						</div>
					</div>
					<div id="boxPhyIdGroup" class="control-group">
						<label class="control-label col-xs-4" for="boxPhyIdTxt"> 来料箱号</label>
						<div class="col-xs-8">
							<input id="boxPhyIdTxt" type="text" class="form-control">
						</div>
					</div>
					<div id="mtrlPalletCapacityGroup" class="control-group">
						<label class="control-label col-xs-4" for="mtrlPalletCapacityTxt">来料栈板容量 </label>
						<div class="col-xs-8">
							<input id="mtrlPalletCapacityTxt" type="text" class="form-control">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 输入区--结束 -->

	<!-- 列表区--开始 -->
	<div class="col-md-8">
		<div class="row">
			<div class="control-group">
				<div class="controls">
					<center>
						<legend>
							<i class="icon-th"></i>
							<s:text name="INFO_BROWSE_TAG" />
						</legend>
					</center>
				</div>
			</div>

			<div class="row container-fluid">
				<form class="form-inline span7">
					<div class="row">
						<div id="boxListDiv" class="controls container-fluid">
							<table id="boxListGrd"></table>
							<div id="boxListPg"></div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6B00.js" ></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<%-- <script type="text/javascript" src="<%=basePath%>js/com/printl.js"></script> --%>

<script type="text/javascript" src="js/com/DestDesc.js"></script>
<%-- <applet alt="Printers" name="Printers" mayscript="mayscript" codebase="." code="com.print.PrinterApplet.class"
	archive="<%=basePath%>jar/print.jar, <%=basePath%>jar/barcode4j.jar, <%=basePath%>jar/jackson-all-1.9.11.jar" width="0" height="0"></applet>
<script type="text/javascript">
      try{
	      var applet = document.Printers;
	      var printers = applet.findAllPrinters();
	      printers = eval(printers);
	      console.log(printers);
      }catch(ex){
      	 console.info(ex);
      }
	</script> --%>

<!-- <script type="text/javascript" src="/lib/jquery-ui-1.9.2.custom/js/jquery-ui-timepicker-addon.js"></script> -->
</html>