<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>">

<title><s:text name="M_6400_BIS_MDL_DEF_MANAGEMENT_TAG" /></title>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#pathListDiv {
	margin-top: 300px;
}

#mdlDefListDiv {
	height: 400px;
}

#bomListDiv {
	margin-left: 0px;
}
/* #mdlDefListGrd{
	      	position:absolute;
	      	height:90%;
	      }*/
/* #bomListDiv {
	position: absolute;
	height: 90%;
}

*/
#bomEditDialog {
	margin-left: 20%;
	left: 10%;
}

#mdlDefConditionForm .control-group {
	margin-top: 5px;
}

#selectPathDialog_pathListDiv {
	margin-left: 90px;
}
</style>

</head>

<body>

	<div class="container">

		<button class="btn btn-primary" id="f1_query_btn">

			<s:text name="F1_QUERY_TAG" />

		</button>

		<button class="btn btn-danger" id="f4_del_btn">

			<s:text name="F4_DELETE_TAG" />

		</button>

		<button class="btn btn-warning" id="f5_upd_btn">

			<s:text name="F5_UPDATE_TAG" />

		</button>

		<button class="btn btn-primary" id="f6_add_btn">

			<s:text name="F6_ADD_TAG" />

		</button>

		<button class="btn btn-primary" id="f9_save_btn">
			<s:text name="F9_SAVE_BTN" />
		</button>
		<button class="btn btn-warning" id="f10_clear_btn">

			<s:text name="F10_CLEAR_TAG" />

		</button>

	</div>
	<div class="container">
		<h3>
			<s:text name="MDL_DEF_INFO_TAG" />
		</h3>
	</div>
	<div class="container">



		<div id="mdlDefListDiv" class="col-md-3">

			<table id="mdlDefListGrd"></table>

			<div id="mdlDefListPg"></div>

		</div>

		<div id="tabDiv" class="col-md-9">
			<ul id="mytab" class="nav nav-tabs">
				<li class="active"><a id="tab1" href="#tabPane_mdlDef" data-toggle="tab"> <s:text name="MDL_DEF_BASE_INFO_TAG" />
				</a></li>
				<li><a id="tab2" href="#tabPane_bom" data-toggle="tab"> <s:text name="MTRL_INFO_TAG" />
				</a></li>
				<!-- <li><a id="tab3" href="#tabPane_grade" data-toggle="tab">等级</a></li>
				<li><a id="tab4" href="#settings" data-toggle="tab">其他</a></li> -->
			</ul>

			<div class="tab-content">
				<div class="tab-pane active row" id="tabPane_mdlDef">
					<!-- <div class="row span8"> -->

					<form id="mdlDefConditionForm" class="form-horizontal">
						<div class="control-group col-sm-12">
							<div class="col-sm-5">
								<label class="control-label col-sm-5 keyCss" for="mdlTypeSel"> 产品规格</label>
								<div class="col-sm-7">
									<select id="mdlTypeSel" class="form-control input-sm"></select>
								</div>
							</div>
							<div class="col-sm-7">
								<label class="control-label col-sm-4 keyCss" for="mdlIdTxt"> <s:text name="MDL_ID_TAG" />
								</label>
								<div class="col-sm-8">
									<input type="text" id="mdlIdTxt" class="form-control input-sm">
								</div>
							</div>
						</div>
						<div class="control-group col-sm-12">
							<label class="control-label col-sm-2" for="mdlDscTxt" style="margin-left: 8px"> <s:text name="MDL_DSC_TAG" />
							</label>
							<div class="col-sm-9">
								<input type="text" id="mdlDscTxt" class="form-control  input-sm">
							</div>
						</div>
						<div class="control-group col-sm-12">
							<div class="col-sm-5">
								<label class="control-label col-sm-5 keyCss" for="mdlCodeSel"> <s:text name="MDL_CODE_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="mdlCodeSel" class="form-control input-sm"></select>
								</div>
							</div>
							<div class="col-sm-7">
								<label class="control-label col-sm-4 keyCss" for="layoutIDSel"> <s:text name="LAYOT_ID_TAG" />
								</label>
								<div class="col-sm-6">
									<select id="layoutIDSel" class="form-control input-sm"></select>
								</div>

								<div class="col-sm-2">
									<label class="checkbox inline control-label"> <input id="dqrLayoutIDFlgChk" type="checkbox" />ABN
									</label>
								</div>
							</div>
						</div>
						<div class="control-group col-sm-12">
							<div class="col-sm-5">
								<label class="control-label col-sm-5 keyCss" for="mdlCateSel"> <s:text name="MDL_CATE_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="mdlCateSel" class="form-control input-sm"></select>
								</div>
							</div>

							<div class="col-sm-7">
								<label class="control-label col-sm-4 keyCss" for="mdlSizeSel"> <s:text name="MDL_SIZE_TAG" />
								</label>
								<div class="col-sm-8">
									<select id="mdlSizeSel" class="form-control input-sm"></select>
								</div>
							</div>
						</div>
						<div class="control-group col-sm-12">
							<div class="col-sm-5">
								<label class="control-label col-sm-5" for="okRadioTxt"> <s:text name="OK_RADIO_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="okRadioTxt" class="form-control input-sm numberCss">
								</div>
							</div>
							<div class="col-sm-7">
								<label class="control-label col-sm-4" for="boxCntTxt"> <s:text name="BOX_CNT_TAG" />
								</label>
								<div class="col-sm-8">
									<input type="text" id="boxCntTxt" class="form-control input-sm numberCss">
								</div>
							</div>
						</div>
						<div class="control-group col-sm-12">
							<div class="col-sm-5">
								<label class="control-label col-sm-5 keyCss" for="fromThicknessTxt"> <s:text name="FROM_THICKNESS_TAG" />
								</label>
								<div class="col-sm-5">
									<input type="text" id="fromThicknessTxt" class="form-control input-sm numberCss">
								</div>
								<div class="col-sm-2">
									<label class="checkbox inline"><input id="dqrFromThicknessFlgChk" type="checkbox" />ABN</label>
								</div>
							</div>
							<div class="col-sm-7">
								<label class="control-label col-sm-5 keyCss" for="toThicknessTxt"> <s:text name="TO_THICKNESS_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="toThicknessTxt" class="form-control input-sm numberCss">
								</div>
							</div>
						</div>
					<%--	<div class="control-group col-sm-12">
							<div class="col-sm-5">
								<label class="control-label col-sm-5 keyCss" for="paramSel"> <s:text name="玻璃卡控规则" />
								</label>
								<div class="col-sm-7">
									<select id="paramSel" class="form-control"></select>
								</div>
							</div>
						</div>--%>
						<div class="control-group col-sm-12">
							<label class="control-label col-sm-2 keyCss" for="addPathBtn"> 工艺路线设定 </label>
							<div class="col-sm-4">
								<button id="addPathBtn" class="btn btn-primary">新增</button>
								<button id="deletePathBtn" class="btn btn-primary">删除</button>
							</div>
						</div>
					</form>
					<div id="pathListDiv" class="container-fluid">
						<table id="pathListGrd"></table>
						<div id="pathListPg"></div>
					</div>
					<!-- </div> -->
				</div>
				<div class="tab-pane" id="tabPane_bom">

					<!-- -->
					<button id="addBomMtrl" class="btn btn-primary offset1">添加物料</button>
					<button id="updateBomMtrl" class="btn btn-warning">修改物料</button>
					<button id="deleteBomMtrl" class="btn btn-danger">删除物料</button>
					<br /> <br />
					<div id="bomListDiv" class="row">
						<table id="bomListGrd"></table>
						<div id="bomListPg"></div>
					</div>
				</div>
		<!-- 		<div class="tab-pane" id="tabPane_grade">等级</div>
				<div class="tab-pane" id="settings">other</div> -->
			</div>

		</div>

	</div>



	<!-- 查询对话框 -->

	<div id="queryMdlDefDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div class="row">
						<form id="queryMdlDefDialog_mdlDefForm" class="row form-horizontal">
							<div class="control-group ">
								<label class="control-label col-lg-4" for="queryMdlDefDialog_mdlIdTxt"> <s:text name="MDL_ID_TAG" />
								</label>
								<div class="col-lg-3">
									<input type="text" id="queryMdlDefDialog_mdlIdTxt" class="form-control" />
								</div>
								<div class="col-lg-3">
									<label id="queryMdlDefDialog_helpLineSpn" class="help-inline control-label"> 模糊查询请使用<span style="color: red; font-weight: bold;">*</span>
									</label>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="queryMdlDefDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryMdlDefDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>

	<!-- BOM 维护对话框 -->
	<div id="bomEditDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="BOM_EDIT_DIALOG_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div class="row">
						<form id="bomEditDialogForm" class="row form-horizontal">
							<input type="text" id="bomEditDialog_mdlMtrlSeqIDTxt" class="form-control hide" />
							<div class="control-group">
								<label class="control-label col-lg-4 keyCss" for="bomEditDialog_mdlIdTxt"> <s:text name="MDL_ID_TAG" />
								</label>
								<div class="col-lg-6">
									<input type="text" id="bomEditDialog_mdlIdTxt" class="form-control" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-lg-4 keyCss" for="bomEditDialog_mtrlCatesel"> <s:text name="MTRL_CATE_TAG" />
								</label>
								<div class="col-lg-6">
									<select id="bomEditDialog_mtrlCatesel" class="form-control input-sm"></select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-lg-4 keyCss" for="bomEditDialog_mtrlProdIDSel"> <s:text name="MTRL_PRODUCT_ID_TAG" />
								</label>
								<div class="col-lg-6">
									<select id="bomEditDialog_mtrlProdIDSel" class="form-control input-sm"></select>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label col-lg-4 keyCss" for="bomEditDialog_OpeIDSel"> <s:text name="OPE_ID_TAG" />
								</label>
								<div class="col-lg-6">
									<select id="bomEditDialog_OpeIDSel" class="form-control input-sm"></select>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="bomEditDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="bomEditDialog_saveBtn" class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>

	<div id="selectPathDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">选择工艺路线</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div id="selectPathDialog_pathListDiv" class="row">
						<table id="selectPathDialog_pathListGrd"></table>
						<div id="selectPathDialog_pathListPg"></div>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="selectPathDialog_cancelBtn" class="btn" data-dismiss="modal"> 取消 </a> <a id="selectPathDialog_selectPathBtn" class="btn btn-primary"> 选择工艺路线 </a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>


</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6400.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script type="text/javascript" src="js/com/SelectDom.js"></script>
<script type="text/javascript" src="js/com/CheckBoxDom.js"></script>
<script type="text/javascript" src="js/com/Regex.js"></script>
<!-- <script type="text/javascript" src="js/dialog/6000/bis_data_6400_Dialog.js" ></script> -->

</html>