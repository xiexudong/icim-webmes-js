<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_6300_BIS_DATA_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#dataListDiv {
	position: absolute;
	height: 65%;
}

.keyClass {
	color: blue;
	font-weight: bold;
}

.keyClass:after {
	content: '*';
	color: red;
	/* margin-left: 2px; */
}
</style>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
</head>
<body>
	<div class="container">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<!-- <button class="btn btn-danger"  id="f4_del_btn">
			<s:text name="F4_DELETE_TAG"/>
		</button>
		<button class="btn btn-warning" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG"/>
		</button> -->
		<!-- <button class="btn btn-primary" id="f8_add_btn">
			<s:text name="F8_REGIST_TAG"/>
		</button> -->
		<button class="btn btn-primary" id="f6_add_btn">
			<s:text name="F6_ADD_TAG" />
		</button>
		<button class="btn btn-warning" id="f8_addDataCate_btn">
			<s:text name="F8_ADD_DATA_CATE_TAG" />
		</button>
		<button class="btn btn-danger" id="f9_deleteDataCate_btn">
			<s:text name="F9_DELELE_DATA_CATE_TAG" />
		</button>
		<button class="btn btn-warning" id="f10_clear_btn">
			<s:text name="F10_CLEAR_TAG" />
		</button>
	</div>

	<div class="container">
		<!-- 产品提配Div -->
		<div class="container-fluid">
			<center>
				<legend>
					<i class="icon-calendar"></i>
					<s:text name="DATA_INFO_TAG" />
				</legend>
			</center>
		</div>
		<div class="container-fluid">
			<form id="dataForm" class="form-horizontal">
				<div class="form-group">
					<label class="control-label col-sm-2" for="dataCateSel"> <s:text name="DATA_CATE_TAG" />
					</label>
					<div class="col-sm-2">
						<select id="dataCateSel" class="form-control"></select>
					</div>
					<div class="col-sm-4">
						<span id="cateDesSpn" class="help-inline"></span>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="container">
		<div id="dataListDiv" class="col-sm-10">
			<table id="dataListGrd"></table>
			<div id="dataListPg"></div>
		</div>
	</div>

	<!-- 新增Data Cate Dialog -->
	<div id="addDataCateDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="ADD_DATA_CATE_DIALOG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div class="row">
						<form id="addDatacateDialog_dataCateForm" class="form-horizontal row">
							<div class="control-group">
								<label class="control-label col-xs-4 keyClass" for="addDataCateDialog_dataCateTxt"> <s:text name="DATA_CATE_TAG" />
								</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dataCateTxt" class="form-control" maxlength="4" rel="tooltip" data-original-title="必须输入不超过4位的非中文字符"
										onKeyUp="value=value.replace(/[\W]/g,''); this.value=this.value.toLocaleUpperCase() ">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4 " for="addDataCateDialog_dataDescTxt"> <s:text name="DATA_DESC_TAG" />
								</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dataDescTxt" class="form-control" maxlength="60" />
								</div>

							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_data_IdTxt">第一列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_data_IdTxt" class="form-control" maxlength="20" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_data_ExtTxt">第二列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_data_ExtTxt" class="form-control" maxlength="20" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_data_ItemTxt">第三列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_data_ItemTxt" class="form-control" maxlength="20" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_ext_1Txt">第四列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_ext_1Txt" class="form-control" maxlength="20" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_ext_2Txt">第五列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_ext_2Txt" class="form-control" maxlength="20" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_ext_3Txt">第六列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_ext_3Txt" class="form-control" maxlength="20" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_ext_4Txt">第七列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_ext_4Txt" class="form-control" maxlength="20" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_ext_5Txt">第八列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_ext_5Txt" class="form-control" maxlength="20" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label col-xs-4" for="addDataCateDialog_dsc_data_DscTxt">第九列含义</label>
								<div class="col-xs-6">
									<input type="text" id="addDataCateDialog_dsc_data_DscTxt" class="form-control" maxlength="20" />
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="addDatacateDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="addDatacateDialog_addDateCateBtn" class="btn btn-primary"> <s:text name="F5_UPATE_COMMIT_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>
	<!-- 删除Data Cate Dialog -->
	<div id="deleteDataCateDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<s:text name="DELETE_DATA_CATE_DIALOG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div class="row">

						<%-- <form id="deleteDataCateDialog_dataForm" class="row form-inline"">
							<div class="control-group">
								<label class="control-label col-md-3" for="deleteDataCateDialog_dataCateSel"> <s:text name="DATA_CATE_TAG" />
								</label>
								<div class="col-md-9">
									<select id="deleteDataCateDialog_dataCateSel" class="form-control col-lg-4"></select> 
									<span id="deleteDataCateDialog_dataDescSpn" class="help-inline col-lg-5"></span>
								</div>
							</div>
						</form> --%>
						
						
						<form id="deleteDataCateDialog_dataForm" class="form-inline">
						
						<div class="form-group col-lg-offset-1">
							<label class="control-label" for="deleteDataCateDialog_dataCateSel"> <s:text name="DATA_CATE_TAG" />
						</div>
						
					    <div class="input-group col-lg-offset-1">
						    <select id="deleteDataCateDialog_dataCateSel" class="form-control"></select> 
						    <div class="input-group-addon" id="deleteDataCateDialog_dataDescSpn">.00</div>
						  </div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="deleteDatacateDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="deleteDatacateDialog_deleteDateCateBtn" class="btn btn-danger"> <s:text name="F6_DELETE_CATE_TAG" />
					</a>
				</div>
				<!--Modal footer-->
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script type="text/javascript" src="js/page/6000/6300.js"></script>
<script type="text/javascript" src="js/dialog/6000/bis_data_6300_Dialog.js"></script>
<jsp:include page="/page/dialog/6000/bis_data_6300_Dialog.jsp"></jsp:include>
</html>