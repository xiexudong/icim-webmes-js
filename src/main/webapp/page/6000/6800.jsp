<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();

	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>



<!DOCTYPE html>

<html>

<head>

<base href="<%=basePath%>

	">

<title><s:text name="M6800_TOOL_MANAGEMENT_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.keyClass {
	color: blue;
	font-weight: bold;
}

#toolListDiv {
	position: absolute;
	height: 70%;
}

#toolListGrd {
	position: absolute;
	height: 70%;
}

#toolPortListDiv {
	position: absolute;
	height: 90%;
}

#toolPmListDiv {
	position: absolute;
	height: 90%;
}

#toolCheckInfoDiv {
	position: absolute;
	height: 90%;
}

.keyCss:after {
	float: right;
	content: '*';
	color: red;
}

#pmItemDialog {
	margin-left: 10%;
	left: 20%;
}

.row {
	margin-top: 10px;
}
</style>

</head>

<body>

	<div class="container-fluid">
		<button class="btn btn-primary" id="f1_query_btn">
			<s:text name="F1_QUERY_TAG" />
		</button>
		<button class="btn btn-danger" id="f4_del_btn">
			<s:text name="F4_DELETE_TAG" />
		</button>
		<button class="btn btn-warning" id="f5_upd_btn">
			<s:text name="F5_UPDATE_TAG" />
		</button>
		<button class="btn btn-primary" id="f6_add_btn">
			<s:text name="F6_ADD_TAG" />
		</button>
		<button class="btn btn-primary" id="f9_save_btn">
			<s:text name="F9_SAVE_BTN" />
		</button>
		<button class="btn btn-warning" id="f10_clear_btn">
			<s:text name="F10_CLEAR_TAG" />
		</button>
	</div>
	<div class="container-fluid">
		<h3>
			<s:text name="TOOL_INFO_TAG" />
		</h3>
	</div>
	<div class="container-fluid">
		<div id="toolListDiv" class="col-md-3">
			<table id="toolListGrd"></table>
			<div id="toolListPg"></div>
		</div>
		<div id="tabDiv" class="col-md-offset-3 col-md-9">
			<ul id="mytab" class="nav nav-tabs">
				<li class="active"><a id="tab1" href="#tabPane_toolDetail" data-toggle="tab"> <s:text name="TOOL_DETAIL_INFO_TAG" />
				</a></li>
			<%-- 	<li><a id="tab2" href="#tabPane_toolPort" data-toggle="tab"> <s:text name="TOOL_PORT_INFO_TAG" />
				</a></li> --%>
				<li><a id="tab3" href="#tabPane_toolPm" data-toggle="tab"> <s:text name="TOOL_PM_INFO" />
				</a></li>
				<li><a id="tab4" href="#tabPane_toolInsp" data-toggle="tab"> <s:text name="TOOL_INSPECTION_INFO_TAG" />
				</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tabPane_toolDetail">
					<form id="toolDetailForm" class="form-horizontal">
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4 keyCss" for="toolIDTxt"> <s:text name="TOOL_ID_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="toolIDTxt" class="form-control">
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4 keyCss" for="UnitTypeSel"> <s:text name="UNIT_TYPE_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="UnitTypeSel" class="form-control"></select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4" for="toolDscTxt"> <s:text name="TOOL_DSC_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="toolDscTxt" class="form-control" />
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4" for="toolTrnsCateSel"> <s:text name="TOOL_TRNS_CATE_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="toolTrnsCateSel" class="form-control"></select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4" for="rootToolIDSel"> <s:text name="ROOT_TOOL_ID_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="rootToolIDSel" class="form-control"></select>
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4 keyCss" for="toolCateSel"> <s:text name="TOOL_CATE_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="toolCateSel" class="form-control"></select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4" for="bayIDSel"> <s:text name="BAY_ID_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="bayIDSel" class="form-control"></select>
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4 keyCss" for="toolGroupSel"> <s:text name="TOOL_GROUP_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="toolGroupSel" class="form-control"></select>
								</div>
							</div>
						</div>
				<%-- 		<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4 keyCss" for="fabIdSel"> <s:text name="PLANT_ID_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="fabIdSel" class="form-control"></select>
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4 keyCss" for="floorCodeSel"> <s:text name="FLOOR_CODE_TAG" />
								</label>
								<div class="col-sm-7">
									<select id="floorCodeSel" class="form-control"></select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4" for="maxShtCntTxt"> <s:text name="MAX_SHT_CNT_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="maxShtCntTxt" class="form-control">
								</div>
							</div>
							<div class="col-sm-6 control-group">
								<label class="control-label col-sm-4" for="maxShtTimeTxt"> <s:text name="MAX_SHT_TIME_TAG" />
								</label>
								<div class="col-sm-7">
									<input type="text" id="maxShtTimeTxt" class="form-control">
								</div>
							</div>
						</div> --%>
					</form>
				</div>
	<%-- 			<div class="tab-pane row" id="tabPane_toolPort">
					<div id="toolPortListDiv" class="row" style="margin-left: 15px;">
						<button id="toolPort_addBtn" class="btn btn-primary">
							<s:text name="ADD_TAG" />
						</button>
						<button id="toolPort_updBtn" class="btn btn-warning">
							<s:text name="UPDATE_TAG" />
						</button>
						<button id="toolPort_delBtn" class="btn btn-danger">
							<s:text name="DELETE_TAG" />
						</button>

						<div id="toolPortListDiv" class="row col-md-12">
							<table id="toolPortListGrd" class="col-md-12"></table>
							<div id="toolPortListPg" class="col-md-12"></div>
						</div>
					</div>
				</div> --%>
				<div class="tab-pane" id="tabPane_toolPm">
					<div id="toolPmListDiv" class="row" style="margin-left: 5px;">
						<button id="toolPm_addBtn" class="btn btn-primary">
							<s:text name="ADD_TAG" />
						</button>
						<button id="toolm_delBtn" class="btn btn-danger">
							<s:text name="DELETE_TAG" />
						</button>
						<div id="toolPmListDiv" class="row col-md-12">
							<table id="toolPmListGrd"></table>
							<div id="toolPmListPg"></div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="tabPane_toolInsp">
					<div id="toolCheckInfoDiv" class="row" style="margin-left: 5px;">
						<table id="toolCheckInfoGrd"></table>
						<div id="toolCheckInfoPg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 添加或修改pm项目-->
	<div id="pmItemDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="title">
						<s:text name="ADD_UPDATE_TAG" />
					</h4>
				</div>
				<div class="modal-body">
					<form id="dialogPmItemForm" class="row form-horizontal">
						<div class="control-group">
							<label class="control-label col-sm-4 keyCss" for="pmCateSel"> <s:text name="保养类型" />
							</label>
							<div class="col-sm-5">
								<select id="pmCateSel" class="from-control"></select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-sm-4" for="beginTimeTxt"><s:text name="开始日期" /></label>
							<div class="col-sm-5">
								<input type="text" id="beginTimeTxt" class="form-control">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-sm-4" for="endTimeTxt"><s:text name="结束日期" /></label>
							<div class="col-sm-5">
								<input type="text" id="endTimeTxt" class="form-control">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label col-sm-4 keyCss" for="pmIdSel"> <s:text name="保养项目" />
							</label>
							<div class="col-sm-5">
								<select id="pmIdSel" class="from-control"></select>
							</div>
						</div>

					</form>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="pmItemDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="pmItemDialog_saveBtn" class="btn btn-primary"> <s:text name="SURE_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 查询对话框 -->
	<div id="queryToolDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="title">
						<s:text name="QUERY_CONDITION_TAG" />
					</h4>
				</div>
				<!--Modal header-->
				<div class="modal-body">
					<div class="row">
						<form id="queryToolDialog_mdlDefForm" class="row form-horizontal">
							<div class="control-group ">
								<label class="control-label col-sm-3" for="queryToolDialog_toolIDTxt"> <s:text name="TOOL_ID_TAG" />
								</label>
								<div class="col-sm-4">
									<input type="text" id="queryToolDialog_toolIDTxt" class="form-control">
								</div>
								<div class="col-sm-5">
									<label id="queryToolDialog_helpLineSpn" class="help-inline control-label"> 模糊查询请使用<span style="color: red; font-weight: bold;">*</span>
									</label>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--Modal body-->
				<div class="modal-footer">
					<a id="queryToolDialog_cancelBtn" class="btn" data-dismiss="modal"> <s:text name="CANCEL_TAG" />
					</a> <a id="queryToolDialog_queryBtn" class="btn btn-primary"> <s:text name="QUERY_BTN_TAG" />
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/com/SelectDom.js"></script>
<%-- <script src="lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script
	src="lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script> --%>
<script src="js/com/TimestampUtil.js"></script>
<script type="text/javascript" src="js/page/6000/6800.js"></script>
</html>