<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_2600_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%></head>
<style>
#boxDetailDiv {
	width: 97%;
}

#grdLegendDiv {
	width: 96%;
}

.form-horizontal .form-group {
	margin: 5px 0px;
}
</style>
<body>
	<div class="container">
		<div class="col-lg-12">
			<button class="btn btn-primary" id="query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="bank_out_btn">
				F1
				<s:text name="WIP_BANK_OUT_TAG" />
			</button>
			<button class="btn btn-primary" id="bank_in_btn">
				F2
				<s:text name="WIP_BANK_IN_TAG" />
			</button>
		</div>

		<div class="col-lg-5">
			<fieldset>
				<legend>
					<s:text name="PPBOX_INFO" />
				</legend>
				<div class="row control-group">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="control-label keyCss col-lg-5 " for="ppBoxIDInput"> <s:text name="BOX_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<input id="ppBoxIDInput" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label keyCss col-lg-5" for="wipBankSelect"> <s:text name="DFT_WIP_BANK_TOOL_ID_TAG" />
							</label>
							<div class="col-lg-6">
								<select id="wipBankSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label keyCss col-lg-5" for="ppBoxIDInput"> 批次号 </label>
							<div class="col-lg-6">
								<input id="lotIdInput" type="text" placeholder="模糊查询" class="form-control">
							</div>
						</div>
					</form>
				</div>
			</fieldset>
		</div>

		<div class="col-lg-12" id="grdLegendDiv">
			<fieldset>
				<legend>
					<s:text name="PRODUCT_ID_TAG" />
				</legend>
				<div id="boxDetailDiv">
					<table id="boxDetailGrd"></table>
					<div id="boxDetailPg"></div>
				</div>
			</fieldset>
		</div>
	</div>



	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/2000/2600.js"></script>

</body>
</html>