<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_2100_TITLE_TAG" /></title>
<style type="text/css">
#centerBtnDiv {
	amrgin:0px auto;
}
div .form-width{

	padding-left:0px;
	padding-right:0px;
}
#btnDiv {
	position: relative;
	top: -5px;
}
#lrBtnDiv{
    margin-top:400px;
}
.mid{
  vertical-align: middle;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%></head>
<body>
	<div class="container-fluid">
		<div class="col-lg-12" id="btnDiv">
			<button class="btn btn-primary" id="query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="change_btn">
				<s:text name="CHANGE_BOX_TAG" />
			</button>
			<button class="btn btn-primary" id="wh_change_btn">
				<s:text name="IN_BANK_BOX_CHANGE" />
			</button>
		</div>
		<div class="row col-lg-12 form-horizontal">
			<div class="row control-group">
				<label class="control-label col-lg-1" for="opeSelect"> <s:text
						name="OPE_ID_TAG" />
				</label>
				<div class="col-lg-2">
					<select id="opeSelect" class="form-control"></select>
				</div>
			</div>
		</div>

		<div id="grdDiv" class="row col-lg-12">
			<div class="col-lg-5 form-width">
				<fieldset>
					<legend>
						A
						<s:text name="PPBOX_INFO" />
					</legend>
					<form class="form-horizontal">
						<div class="control-group col-lg-12">
							<label class="control-label keyCss col-lg-2" for="ppBoxIDInputL">
								<s:text name="BOX_ID_TAG" />
							</label>
							<div class="col-lg-4">
								<input id="ppBoxIDInputL" type="text" class="form-control">
							</div>
						</div>
						<legend>
							<s:text name="PRODUCT_ID_TAG" />
						</legend>
						<div id="boxDetailDivL">
							<table id="boxDetailGrdL"></table>
							<div id="boxDetailPgL"></div>
						</div>
					</form>
				</fieldset>
			</div>
			<div  id="lrBtnDiv" class="col-lg-2 form-width">
			
				<button id="chgBtnL2R" class="btn btn-info">
					<i class="glyphicon-arrow-right glyphicon"></i>
				</button>
				<br>
				<button id="chgBtnR2L" class="btn btn-info">
					<i class="glyphicon-arrow-left glyphicon"></i>
				</button>
				
			</div>
			<div class="col-lg-5 form-width">
				<fieldset>
					<legend>
						B
						<s:text name="PPBOX_INFO" />
					</legend>
					<form class="form-horizontal">
						<div class="control-group col-lg-12">
							<label class="control-label keyCss col-lg-2" for="ppBoxIDInputR">
								<s:text name="BOX_ID_TAG" />
							</label>
							<div class="col-lg-4">
								<input id="ppBoxIDInputR" type="text" class="form-control">
							</div>
						</div>
						<legend>
							<s:text name="PRODUCT_ID_TAG" />
						</legend>
						<div id="boxDetailDivR">
							<table id="boxDetailGrdR"></table>
							<div id="boxDetailPgR"></div>
						</div>

					</form>
				</fieldset>
			</div>

		</div>
	</div>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/2000/2100.js?v=20140821"></script>

</body>
</html>