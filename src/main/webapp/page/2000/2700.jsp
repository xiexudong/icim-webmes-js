<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_2700_TITLE_TAG" /></title>
<style type="text/css">
.form-horizontal .form-group {
	margin: 5px 0px;
}
div .form-width{

	padding-left:0px;
	padding-right:0px;
}

form .form-group {
	margin: 1px 0px;
}

#lrBtnDiv{
	width:220px;
}


</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%></head>
<body>
	<div class="container-fluid">
		<div class="col-lg-12" id="btnDiv">
			<button class="btn btn-primary" id="query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="change_btn">
				<s:text name="WO_CHANGE_WO" />
			</button>
		</div>

		<div class="row col-lg-12 form-horizontal">
			<div class="col-lg-4">
				<fieldset>
					<div class="row control-group">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="control-label keyCss col-lg-4" for="woIdSelectL"><s:text
										name="WORDER_ID_TAG" /></label>
								<div class="col-lg-6">
									<select id="woIdSelectL" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-4" for="opeSelect"><s:text
										name="OPE_ID_TAG" /></label>
								<div class="col-lg-6">
									<select id="opeSelect" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label keyCss col-lg-4" for="ppBoxIDInputL"><s:text
										name="BOX_ID_TAG" /></label>
								<div class="col-lg-6">
									<input id="ppBoxIDInputL" type="text" class="form-control">
								</div>
							</div>
						</form>
					</div>
				</fieldset>
			</div>

			<div class="col-lg-4">
				<fieldset>
					<div class="row control-group">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="control-label keyCss col-lg-4" for="woIdSelectR"><s:text
										name="WORDER_ID_TAG" /></label>
								<div class="col-lg-6">
									<select id="woIdSelectR" class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-4" for="opeR"><s:text
										name="OPE_ID_TAG" /></label>
								<div class="col-lg-6">
									<input id="opeR" type="text" class="disabled form-control"
										disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label keyCss col-lg-4" for="ppBoxIDInputR"><s:text
										name="BOX_ID_TAG" /></label>
								<div class="col-lg-6">
									<input id="ppBoxIDInputR" type="text" class="form-control">
								</div>
							</div>
						</form>
					</div>
				</fieldset>
			</div>
		</div>

		<!-- <div id="grdDiv">
		<div id="boxDetailDivL" class="span7">
			<table id="boxDetailGrdL"></table>
			<div id="boxDetailPgL"></div>
		</div>
		
		<div class ="span1" id="lrBtnDiv">
			<button id="chgBtnL2R" class="btn btn-info">
	        	<i class="icon-arrow-right icon-white"></i>
	        </button>
	        <button id="chgBtnR2L" class="btn btn-info">
	        	<i class="icon-arrow-left icon-white"></i>
	        </button>
		</div>
	
	<div id="boxDetailDivR" class="span6">
		<table id="boxDetailGrdR"></table>
		<div id="boxDetailPgR"></div>
	</div>
	</div> -->
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-5  form-width" id="boxDetailDivL">
				<table id="boxDetailGrdL"></table>
				<div id="boxDetailPgL"></div>
			</div>
			<div class="col-lg-2 form-width " id="lrBtnDiv">
				<button id="chgBtnL2R" class="btn btn-info" >
					<i class="glyphicon-arrow-right glyphicon"></i>
				</button>
				<br>
				<button id="chgBtnR2L" class="btn btn-info">
					<i class="glyphicon-arrow-left glyphicon"></i>
				</button>
			</div>
			<div class="col-lg-5  form-width" id="boxDetailDivR">
				<table id="boxDetailGrdR"></table>
				<div id="boxDetailPgR"></div>
			</div>
		</div>
	</div>

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/2000/2700.js?v=20140821"></script>

</body>
</html>