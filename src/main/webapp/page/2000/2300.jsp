<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_2300_TITLE_TAG" /></title>
<style type="text/css">
#ipqcDetailDiv {
	width: 97%;
}
.top{
   margin-top:30px;
}
form .form-group {
margin:5px 0px;
}
</style>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%></head>
<body>
	<div class="container">
		<div class="col-lg-12">
			<button class="btn btn-primary" id="f1_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="ipqc_btn">
				<s:text name="IPQC_TAG" />
			</button>
			<button class="btn btn-info" id="update_lot_btn">修改批次</button>
			<button class="btn btn-info" id="update_group_id_btn">修改组代码
			</button>
<!-- 			<button class="btn btn-info" id="wip_reserve_btn">批量在制保留</button>
			<button class="btn btn-info" id="wip_reserve_cancel_btn">
				取消在制保留</button> -->
		</div>
		<div class="row">
			<fieldset>
				<legend>
					<s:text name="PNL_INFO_TAG" />
				</legend>
				<div class="col-lg-4">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="control-label keyCss col-lg-5" for="cusIDSelect"><s:text
									name="CUS_ID_TAG" /></label>
							<div class="col-lg-7">
								<select id="cusIDSelect" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label keyCss col-lg-5" for="woIdSelect"><s:text
									name="WORDER_ID_TAG" /></label>
							<div class="col-lg-7">
								<select id="woIdSelect" class="form-control"></select>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-lg-5" for="lotIDInput">批次号</label>
							<div class="col-lg-7">
								<input id="lotIDInput" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="mtrlBoxInput">来料箱号</label>
							<div class="col-lg-7">
								<input id="mtrlBoxInput" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="boxIDInput">产品当前箱号</label>
							<div class="col-lg-7">
								<input id="boxIDInput" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="pnlIDInput">产品唯一码</label>
							<div class="col-lg-7">
								<input id="pnlIDInput" type="text" class="form-control">
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-4" >
					<form class="form-inline">
						<div style="position: relative;left:100px">
							<label class="radio"> <input type="radio"
								name="judgeRadios" id="judgeRadioOK" value="OK" checked>OK
							</label> <label class="radio"> <input type="radio"
								name="judgeRadios" id="judgeRadioNG" value="NG">NG
							</label> <label class="radio"> <input type="radio"
								name="judgeRadios" id="judgeRadioSC" value="SC">SC
							</label> <label class="radio"> <input type="radio"
								name="judgeRadios" id="judgeRadioGK" value="GK">GK
							</label> <label class="radio"> <input type="radio"
								name="judgeRadios" id="judgeRadioLZ" value="LZ">LZ
							</label> <label class="radio"> <input type="radio"
								name="judgeRadios" id="judgeRadioRM" value="RM">RM
							</label> <label class="radio"> <input type="radio"
								name="judgeRadios" id="judgeRadioRW" value="RW">RW
							</label>
						</div>
					</form>
					<form class="form-horizontal top">
						<label class="control-label keyCss col-lg-3 " for="remarkInput"><s:text
								name="REMARK_TAG" /></label>
						<div class="col-lg-9">
							<textarea class="input-xlarge col-lg-12" id="remarkInput" rows="3"></textarea>
						</div>
					</form>
				</div>
				<div class="col-lg-4">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-lg-5" for="lotIdOld"> 原批次 </label>
							<div class="col-lg-7">
								<input id="lotIdOld" type="text" class="form-control" disabled>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-lg-5" for="lotIdNew"> 新批次 </label>
							<div class="col-lg-7">
								<input id="lotIdNew" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-5" for="groupIdNew"> 新组代码 </label>
							<div class="col-lg-7">
								<input id="groupIdNew" type="text" class="form-control">
							</div>
						</div>
					</form>
				</div>
			</fieldset>
		</div>
	</div>
	<div class="col-lg-12" id="ipqcDetailDiv">
		<table id="ipqcDetailGrd"></table>
		<div id="ipqcDetailPg"></div>
	</div>


	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/2000/2300.js"></script>
	<script type="text/javascript"
		src="js/dialog/showFabSnDetailInfoDialog.js"></script>

</body>
</html>