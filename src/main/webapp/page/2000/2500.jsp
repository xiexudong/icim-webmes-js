<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>
	">
<title><s:text name="M_2500_TITLE_TAG" /></title>
<!-- Bootstrap -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

#pnlInfoListDiv {
	height: 100%;
}

/* #queryForm{
        margin-top:10px;   
    } */
form .form-group {
	margin-bottom: 1px;
}
</style>

<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>


</head>
<body>
	<div class="container">
		<div class="col-lg-12">
			<button class="btn btn-primary" id="f1_query_btn">
				<s:text name="F1_QUERY_TAG" />
			</button>
			<button class="btn btn-primary" id="f2_skip_btn">
				<s:text name="F2_SKIP_OPE_TAG" />
			</button>
			<button class="btn btn-primary" id="f3_lotchg_btn">
				<s:text name="F3_LOT_CHANGE_TAG" />
			</button>
			<button class="btn btn-primary" id="f4_rwk_btn">自由重工</button>
		</div>

		<!-- 输入区--开始 -->

		<div class="col-lg-12" style="margin-top: 5px;">
			<form id="queryForm" class="form-horizontal">
				<div class="control-group col-lg-4">
					<label class="control-label keyCss col-lg-5" for="boxIDTxt"> <s:text name="BOX_ID_TAG" />
					</label>
					<div class="col-lg-6">
						<input id="boxIDTxt" class="form-control" type="text">
					</div>
				</div>
			</form>
		</div>

		<!-- 输入区--结束 -->

		<!-- 列表区--开始 -->

		<!-- 列表区--结束 -->
		<div id="tabDiv">
			<div class="row col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						<fieldset>
							<legend>
								<s:text name="PPBOX_INFO" />
							</legend>
							<div class="row control-group">
								<form class="form-horizontal">
									<div class="form-group">
										<label class="control-label col-lg-4" for="boxID2Txt"><s:text name="BOX_ID_TAG" /></label>
										<div class="col-lg-6">
											<input type="text" id="boxID2Txt" class="form-control" disabled />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-4" for="boxStatTxt"><s:text name="BOX_STAT_TAG" /></label>
										<div class="col-lg-6">
											<input type="text" id="boxStatTxt" class="form-control" disabled />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-4" for="pathIdTxt"><s:text name="PATH_ID_TAG" /></label>
										<div class="col-lg-6">
											<input type="text" id="pathIdTxt" class="form-control" disabled />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-4" for="pathVerTxt"><s:text name="PATH_VER_TAG" /></label>
										<div class="col-lg-6">
											<input type="text" id="pathVerTxt" class="form-control" disabled />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-4" for="crOpeDscTxt"><s:text name="CR_OPE_ID_TAG" /></label>
										<div class="col-lg-6">
											<input type="text" id="crOpeDscTxt" class="form-control" disabled />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-4" for="skipOpeIdSel" id="skip_ope_tag"><s:text name="SKIP_OPE_ID_TAG" /></label>
										<div class="col-lg-6">
											<select id="skipOpeIdSel" class="form-control"></select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-4" for="oLotTxt"><s:text name="Old_Lot_id_TAG" /></label>
										<div class="col-lg-6">
											<input type="text" id="oLotTxt" class="form-control" disabled />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-4" for="nLotTxt"><s:text name="New_Lot_id_TAG" /></label>
										<div class="col-lg-6">
											<input type="text" id="nLotTxt" class="form-control" />
										</div>
									</div>
									<br>
									<div class="form-group">
										<label class="control-label col-lg-3" for="rwkPathSel">重工线路</label>
										<div class="col-lg-1">
											<input type="checkbox" id="rwkPathFlg" />
										</div>
										<div class="col-lg-6">
											<select id="rwkPathSel" class="form-control" disabled></select>
										</div>
									</div>
								</form>
							</div>
						</fieldset>
					</div>
					<div class="col-lg-8" id="pnlInfoListDiv">
						<fieldset>
							<legend>
								<s:text name="PANEL_INFO_TAG" />
							</legend>
							<div class="col-lg-12" id="pnlInfoListDiv1">
								<table id="pnlInfoListGrd"></table>
								<div id="pnlInfoListPg"></div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="/page/comPage/comJS.html"%>
<script src="lib/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="js/page/2000/2500.js"></script>
</html>