<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title><s:text name="M_2800_TITLE_TAG" /></title>
<%@ include file="/page/comPage/comCSS.html"%>
<%@ include file="/page/comPage/navbar.html"%>

</head>
<body>

	<div class="container-fluid">
		<div class="col-lg-12">
			<button class="btn btn-primary" id="query_btn">
				<s:text name="QUERY_BTN_TAG" />
			</button>
			<!-- <button class="btn btn-primary" id="upload_btn">上传客户资料</button> -->
			<button class="btn btn-primary" id="export_btn">导出客户资料</button>
			<button class="btn btn-primary" id="clear_btn">
				<s:text name="F2_CLEAR_TAG" />
			</button>
		</div>
		<div class="col-lg-12 row">
			<fieldset>
				<legend>
					<s:text name="WIP_BANK_OUT_INFO_INPUT" />
				</legend>
				<div class="col-lg-12">
					<form class="form-horizontal">
						<div class="control-group">
							<label class="control-label" for="boxIDInput"><s:text
									name="BOX_ID_TAG" /></label>
							<div class="controls">
								<input id="boxIDInput" type="text">
							</div>
						</div>
					</form>
				</div>
			</fieldset>
		</div>

		<div class="col-lg-12" id="boxInfoGrdDiv">
			<fieldset>
				<legend>
					<s:text name="PPBOX_INFO" />
				</legend>
				<table id="boxInfoGrd"></table>
				<div id="boxInfoPg"></div>
			</fieldset>
		</div>
	</div>

	<!-- Placed at the end of the document so the pages load faster -->
	<%@ include file="/page/comPage/comJS.html"%>
	<script src="js/page/2000/2800.js"></script>

</body>
</html>
