<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <title>用户登录</title>
    <!-- Bootstrap -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <style>
        *{margin:0;padding: 0;}
        body{background: #444 }
        .loginBox{
            width:230px;height:210px;
            padding:0 20px;
            border:1px solid #fff;
            color:#000;
            border-radius:8px;
            background: white;
            box-shadow:0 0 15px #222;
            background: -moz-linear-gradient(top, #fff, #efefef 8%);
            background: -webkit-gradient(linear, 0 0, 0 100%, from(#f6f6f6), to(#f4f4f4));
            font:11px/1.5em 'Microsoft YaHei' ;
            position: absolute;left:50%;top:50%;
            margin-left:-115px;margin-top:-115px;
        }
        .loginBox h2{height:45px;font-size:20px;font-weight:normal;}
    </style>
</head>
<body>
    <s:if test="#session.userInfo.isLogin == 1">
        <jsp:forward page="/page/main.jsp" />
    </s:if>
    <div class="container">
        <div class="loginBox row-fluid">
            <div class="span7">
                <form id ="loginForm" method="post" action="com/login.action">
                    <h2>用户登录</h2>
                    <p>
                        <label>
                            <input type="text" id="u" name="userName" maxlength="8" placeholder="用户名">
                        </label>
                    </p>
                    <p>
                        <label>
                            <input type="password" id="p" name="passWord" maxlength="8" placeholder="密码">
                        </label>
                    </p>
                    <div class="row-fluid">
                        <div class="span1">
                            <input type="submit" id="loginBtn" value=" 登录 " class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /loginBox -->
    </div> <!-- /container -->
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<!--     <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script> -->
    <%@ include file="/page/comPage/comJS.html"%>
    <script src="js/page/login.js"></script>
    <script src="js/com/SelectDom.js"></script>
</body>
</html>