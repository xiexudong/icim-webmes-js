/**
 * This tiny script just helps us demonstrate
 * what the various example callbacks are doing
 */

 function showSuccessDialog(str){
	 bootbox.dialog({
		 message : "<center><h3>" + str + "</h3></center>",
		 buttons:{
			 success:{
				 label:"Success!",
				 className:"btn-success",
			 }
		 }
	 })
  }

  function showErrorDialog( errCode , errMsg, cb){
  	var str = "<center><h3 class='errorAlert'>"+ errCode +"</h3></center>"+"<br/>"+
        "<center><h3 class='errorAlert'>"+ errMsg +"</h3></center>";
  
     bootbox.alert(str,cb);
  }
    function showWarnningDialog(options){
        var defaults = {
            callbackFn : null
        };
        var config = $.extend(defaults, options || {});
        var resultObj={
            result:false
        };
        bootbox.confirm("<p><h3>"+ options.errMsg+"</h3></p>", function(result) {
            resultObj.result = result ;
            if ( typeof config.callbackFn == 'function') {
                config.callbackFn(resultObj);
            }
        });
    }
  (function($){
    $.fn.extend({
    	"showCallBackWarnningDialog" : function(options) {
    		var defaults = {
		           callbackFn : null
		        }
	     	var config = $.extend(defaults, options || {});
	        var resultObj={
	        		  result:false
	        }
	        bootbox.confirm("<p><center><h3>"+ options.errMsg+"</h3></center></p>", function(result) {
			  	resultObj.result = result ;
			  	if ( typeof config.callbackFn == 'function') { 
                	config.callbackFn(resultObj); 
            	}
			}); 
			
		}
	})
   })(jQuery);
