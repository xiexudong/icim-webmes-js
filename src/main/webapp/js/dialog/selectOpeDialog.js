(function($){
    $.fn.extend({
    	"showSelectOpeDialog" : function(options) {
        //定义返回类型和返回json格式
	      var defaults = {
		           callbackFn : null
		        }
	     	var config = $.extend(defaults, options || {});
        var resultObj={
            ope_id:null,
            ope_ver:null
        };
	        
  		  /***如果show:false 则需要执行modal('show')方法***/
  //		  $('#selectBayToolDialog').modal('show');
  		  /************************Bay Select ***********************/
  		  function  opeSelFun(){
  			  var selectedId = $("#selectOpeDialog_opeGrd").getGridParam("selrow");
  			  if(selectedId==null ||selectedId==undefined){
  				  showErrorDialog("003","请选择站点");
  				  return false
  			  }
          resultObj.ope_id  = $("#selectOpeDialog_opeGrd").getCell( selectedId , 'ope_id');
          resultObj.ope_ver = $("#selectOpeDialog_opeGrd").getCell( selectedId , 'ope_ver');
                  //将结果返回
          if (typeof config.callbackFn == 'function') { 
              config.callbackFn(resultObj); 
          }
          $('#selectOpeDialog').modal('hide');
  		  }

        function selectOpeShowFnc(){
            $("#selectOpeDialog_opeGrd").jqGrid({
               datatype    : "local",
               mtype       : "POST",
               autowidth   : true,//宽度根据父元素自适应
               resizable   : true,
               shrinkToFit : true,//自动填充
               scroll      : true,
               ondblClickRow : opeSelFun,
               colModel    : [
                   { name : 'ope_id'             , index : 'ope_id'            , label: OPE_ID_TAG         , width : 80 , align : "center" },
                   { name : 'ope_ver'            , index : 'ope_ver'           , label: OPE_VER_TAG        , width : 120 , align : "center" },
                   { name : 'ope_desc'           , index : 'ope_desc'          , label: OPE_DSC_TAG       , width : 60 , align : "center" },
                   { name : 'proc_id'            , index : 'proc_id'           , label: PROC_ID_TAG       , width : 60 , align : "center" },
                   { name : 'toolg_id'           , index : 'toolg_id'          , label: TOOLG_ID_TAG      , width : 60 , align : "center" }
                           ],
              viewrecords : true, 
              pager       : '#selectOpeDialog_opePg',
              sortname    : 'wo_id',
              sortorder   : "asc",
              loadonce    : true,
              rownumbers  :true ,
              rownumWidth : 20,
              jsonReader  : {
                  repeatitems : false
              },
               onSelectRow : function (id) {
                         opeSelFun();
               }
            });
            var iary = {

            };
            var inTrxObj  = {
                 trx_id      : "XPBISOPE",
                 action_flg  : 'Q',
                 iary        : iary
            };
              
            var outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if (outTrxObj.rtn_code != "0000000") {
               return false;
            }
            setGridInfo(outTrxObj.oary,"#selectOpeDialog_opeGrd");
            var resetJqgrid = function(){
              $(window).unbind("onresize"); 
              $("#selectOpeDialog_opeGrd").setGridWidth($("#selectOpeDialog_opeDiv").width()*0.90); 
              $("#selectOpeDialog_opeGrd").setGridHeight($("#selectOpeDialog_opeDiv").height()*0.80);     
              $(window).bind("onresize", this);  
            }  
            resetJqgrid();
            $(window).resize(function(){   
              resetJqgrid();
            });
        }


        $('#selectOpeDialog').unbind("shown");
        $('#selectOpeDialog').bind("shown",selectOpeShowFnc);
        $("#selectOpeDialog_selectOpeBtn").unbind("click");
        $("#selectOpeDialog_selectOpeBtn").bind("click",opeSelFun);

        /*** 将Div实例化为modal窗体 ***/
        $('#selectOpeDialog').modal({
            backdrop:true,
            keyboard:false,
            show:true
        });
        // $('#selectBayToolDialog').modal('show');
    	}
    });
})(jQuery);