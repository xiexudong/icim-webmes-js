  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Bis_data Add Data_cate                                */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/04/28 N0.00   Lin.Xin      Initial release                        */
  /*                                                                        */
  /**************************************************************************/


(function($){
    $.fn.extend({
  /**************************************
    ******* 当调用dialog时，需要方式事件被多次执行 ==>使用unbind,bind方法
    ******* shown方法也要unbind ==> bind ==> modal("show"),防止options的值没有传入
    ******* 
  **************************************/
    	"showBisDataDialog" : function(options) {
        //定义返回类型和返回json格式
        var action_flg = options.action_flg;
        var data_cate  = options.data_cate;
        var data_id    = options.data_id;
        var data_ext   = options.data_ext;
        var data_item  = options.data_item;
        var ext_1   = options.ext_1;
        var ext_2   = options.ext_2;
        var ext_3   = options.ext_3;
        var ext_4   = options.ext_4;
        var ext_5   = options.ext_5;
        var data_desc = options.data_desc;
        var _NORMAL = "0000000";

	      var defaults = {
		        callbackFn : null
		    };
	     	var config = $.extend(defaults, options || {});
	      var resultObj={
	      	  result: false
	      };
	        
    	  /*** 将Div实例化为modal窗体 ***/
  		  $('#bis_data_6100_Dialog').modal({
  			    backdrop:true,
  			    keyboard:false,
  			    show:false
  		  });
  		  /***
          (1)show:false 则需要执行modal('show')方法
          (2)需要unbind('show')，避免 未获取options传过来的数据之前,就showDialog 

        ***/
  		  // $('#bis_data_6100_Dialog').modal('show');

        function dialogShowFnc(){
            if(action_flg=="A"){
              $("#dia_dataIdTxt").attr({'disabled':false});
              $("#dia_dataExtTxt").attr({'disabled':false});

              $("#dia_dataIdTxt").val("");
              $("#dia_dataExtTxt").val("");
              $("#dia_dataItemTxt").val("");
              $("#dia_ext1Txt").val("");
              $("#dia_ext2Txt").val("");
              $("#dia_ext3Txt").val("");
              $("#dia_ext4Txt").val("");
              $("#dia_ext5Txt").val("");
              $("#dia_dataDescTxt").val("");
              $("#dia_dataCateSel").empty();
              $("#dia_dataCateSel").append("<option value='Value'>"+ data_cate +"</option>");
            }else if(action_flg=="U"){
                $("#dia_dataCateSel").empty();
                $("#dia_dataCateSel").append("<option value='Value'>"+ data_cate +"</option>");
                diaLogDataCateSelFnc();
                $("#dia_dataIdTxt").attr({'disabled':true});
                $("#dia_dataExtTxt").attr({'disabled':true});

                $("#dia_dataIdTxt").val(data_id);
                $("#dia_dataExtTxt").val(data_ext);
                $("#dia_dataItemTxt").val(data_item);
                $("#dia_ext1Txt").val(ext_1);
                $("#dia_ext2Txt").val(ext_2);
                $("#dia_ext3Txt").val(ext_3);
                $("#dia_ext4Txt").val(ext_4);
                $("#dia_ext5Txt").val(ext_5);
                $("#dia_dataDescTxt").val(data_desc);
            }else{

            }
        }
        function getDialogCATE(){
          $("#dia_dataCateSel").empty();
          var iary = {
            data_cate : 'CATE'
          }
          var inTrxObj = {
             trx_id     : 'XPLSTDAT' ,
             action_flg : 'Q'        ,
             iary       : iary 
          };
          var outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if( outTrxObj.rtn_code == _NORMAL ) {
              var dataCnt = comParseInt( outTrxObj.tbl_cnt);
              if( dataCnt ==1 ){
                $("#dia_dataCateSel").append("<option value='Value'>"+ outTrxObj.oary.data_id +"</option>");
              }else{
                for(var i=0;i<dataCnt;i++){
                  $("#dia_dataCateSel").append("<option value='Value'>"+ outTrxObj.oary[i].data_id +"</option>");
                } 
              }
              diaLogDataCateSelFnc();
          }
        }
        function diaLogDataCateSelFnc(){
           $("#dia_cateDescSpn").text("");
           var iary = {
            data_cate : 'CATE'  ,
            data_id   : $("#dia_dataCateSel").find("option:selected").text()
           };
           var inTrxObj = {
               trx_id     : 'XPLSTDAT' ,
               action_flg : 'Q'        ,
               iary       : iary
           };
           var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
           if(  outTrxObj.rtn_code == _NORMAL ) {
                var dataCnt = comParseInt( outTrxObj.tbl_cnt);
                if( dataCnt == 1 ){
                  $("#dia_cateDescSpn").text(outTrxObj.oary.data_desc);
                }else if(data_cnt>1){
                  $("#dia_cateDescSpn").text(outTrxObj.oary[0].data_desc);
                }
           }
        }
        function diaLogf5UpdateFnc(){
           if($("#dia_dataIdTxt").val()==""){
              showErrorDialog("003","参数代码不能为空");
              $("#dia_dataIdTxt").focus();
              return false;
            }
            if($("#dia_dataExtTxt").val()==""){
              showErrorDialog("003","参数拓展码不能为空");
              $("#dia_dataExtTxt").focus();
              return false;
            }

            var iary = {
              data_cate : $("#dia_dataCateSel").find("option:selected").text(),
              data_id   : $("#dia_dataIdTxt").val(),
              data_ext  : $("#dia_dataExtTxt").val()
            };
            if($("#dia_dataItemTxt").val()!=""){
                iary.data_item = $("#dia_dataItemTxt").val();
            }
            if($("#dia_ext1Txt").val()!=""){
                iary.ext_1 = $("#dia_ext1Txt").val();
            }
            if($("#dia_ext2Txt").val()!=""){
                iary.ext_2 = $("#dia_ext2Txt").val();
            }
            if($("#dia_ext3Txt").val()!=""){
                iary.ext_3 = $("#dia_ext3Txt").val();
            }
            if($("#dia_ext4Txt").val()!=""){
                iary.ext_4 = $("#dia_ext4Txt").val();
            }
            if($("#dia_ext5Txt").val()!=""){
                iary.ext_5 = $("#dia_ext5Txt").val();
            }
            if($("#dia_dataDescTxt").val()!=""){
                iary.data_desc = $("#dia_dataDescTxt").val();
            }
            var inTrxObj = {
               trx_id     : 'XPLSTDAT' ,
               action_flg : action_flg ,
               iary       : iary       ,
               tbl_cnt    : 1
            };
            var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if(  outTrxObj.rtn_code == _NORMAL ) {
              resultObj.result = true ;
              if (typeof config.callbackFn == 'function') { 
                   config.callbackFn(resultObj); 
              }
              if(action_flg=="A"){
                $("#dia_dataIdTxt").val("");
                $("#dia_dataExtTxt").val("");
                $("#dia_dataItemTxt").val("");
                $("#dia_ext1Txt").val("");
                $("#dia_ext2Txt").val("");
                $("#dia_ext3Txt").val("");
                $("#dia_ext4Txt").val("");
                $("#dia_ext5Txt").val("");
                $("#dia_dataDescTxt").val("");
              }else if(action_flg=="U"){
                $('#bis_data_6100_Dialog').modal("hide");
              }
            }
        }
        /*****
            当多次弹出对话框的同时也会多次绑定click事件。
            解决办法: 在show Dialog时将click unbind全部掉
                      然后再绑定，这样就只剩下一个click事件了。
          **********/
        $('#bis_data_6100_Dialog').unbind('shown.bs.modal');
        $("#bisDataDialog_f5update_Btn").unbind('click');
        $("#dia_dataCateSel").unbind('change');
        // $("#dia_dataIdTxt").unbind('tooltip');

        $('#bis_data_6100_Dialog').bind('shown.bs.modal',dialogShowFnc);
        $('#bis_data_6100_Dialog').modal("show");
        $("#bisDataDialog_f5update_Btn").bind('click',diaLogf5UpdateFnc);
        $("#dia_dataCateSel").bind('change',diaLogDataCateSelFnc());
        /*****
          弹出提示,当获取焦点时
          需要包括: bootstrap-tooltip.js
        ******/
        $('[rel=tooltip]').tooltip({
          placement:"bottom",
          trigger:"focus",
          animation:true,

        })


    	}
    });
})(jQuery);