(function($){
    $.fn.extend({
    	"showKeyBoardDialog" : function(options) {
        //定义返回类型和返回json格式
	      var defaults = {
		           callbackFn : null
		        }
	     	var config = $.extend(defaults, options || {});
	      var resultObj={
	        		  keyInResult:null
	      };

        if($('#comKeyBoardDialog').length>0){
           $('#comKeyBoardDialog').remove();
        }
        var str1='<div id="comKeyBoardDialog" class="span2">'
        // style="background:#1e90ff"
          // + '<div id="comKeyBoardDiv" class="span3"></div>'
          + '</div>';
        $(str1).appendTo('body');
        $("#comKeyBoardDialog").dialog({
            autoOpen : false,
            modal : true, //  
            bgiframe : true, // ie6select
            show: "clip",
            hide: "clip",
            // buttons : {
            //   "OK" : function() {
            //     $(this).dialog("close");
            //   }
            // },
            open : function(event, ui) {
              keyBoardShowFnc();
            }
        });
        $("#comKeyBoardDialog").dialog("open");
        function keyBoardShowFnc(){
            var KEYINBTN_ID    = "comKeyBtn_11" ;
            var KEYINBTN_INDEX = 11;
            function comKeyBtnClickFnc(obj){
              if(obj.id!=KEYINBTN_ID){
                $("#comKeyResultTxt").val($("#comKeyResultTxt").val()+obj.textContent);  
              }else{
                // alert($("#comKeyResultTxt").val());
                resultObj.keyInResult = $("#comKeyResultTxt").val() ;
                //将结果返回

                if (typeof config.callbackFn == 'function') { 
                    config.callbackFn(resultObj); 
                }
                $("#comKeyResultTxt").val("");
                $("#comKeyBoardDialog").dialog("close");
              }
            }
            var x_axis_cnt = 4 ;
            var y_axis_cnt = 3 ;
            var k;
            var index;
            var pos_location = 0;
            $("#comKeyBoardDialog").empty();
            var btnValue = ['1','2','3','4','5','6','7','8','9','0','*','输入'];
            var str ='<input type="text" id="comKeyResultTxt" style="width:140px;margin-left:10px;"></input>';
            str = str + '<button id="clearKeyRusultBtn">清空</button>';
            str = str + '<table class="table table-bordered table-condensed">';
            for( var i=0;i<x_axis_cnt;i++){
                 str = str + '<tr>';
                 for(var j=0;j<y_axis_cnt;j++){
                 if(j!=KEYINBTN_INDEX){
                    str = str +'<td id="comKeyBtn_'+ pos_location +'" class="dialog_comKeyNumberCss">' 
                    /***
                      行间样式优先级是最高的,要直接写在HTML中，
                      否则会被Dialog的.table th,.table td
                    *****/
                    +'<center>'+ btnValue[pos_location] +'</center>' + '</td>';
                 }else{
                    str = str +'<td id="comKeyBtn_'+ pos_location +'" class="dialog_comKeyInBtnCss">' 
                    +'<center>'+ btnValue[pos_location] +'</center>' + '</td>';
                 }
                 if(j==y_axis_cnt-1){
                    str = str + '</tr>' ;
                 }
                 pos_location = pos_location+1;
              }
            }
            str = str +'</table>';
        
            $(str).appendTo($("#comKeyBoardDialog"));
            $("#clearKeyRusultBtn").button();
            $("#clearKeyRusultBtn").css({"margin-left":"10px"});
            // $("#clearKeyRusultBtn").addClass("btn");
            // $("#clearKeyRusultBtn").addClass("btn-warning");
            $("#clearKeyRusultBtn").css({"background":"#ff7f50"});
            
            for(var i=0;i<pos_location;i++){
              // $("#comKeyBtn_"+i).click( test( this ) );
              $("#comKeyBtn_"+i).click(function(){
                comKeyBtnClickFnc(this);
              })
            }
            $("#clearKeyRusultBtn").click(function(){
              $("#comKeyResultTxt").val("");
            })
            
        }
      }   
    });
})(jQuery);