(function($) {
	$.fn.extend({
		"showSelectWorderDialog" : function(options) {
			// 定义返回类型和返回json格式
			var wo_type = options.wo_type;
			var defaults = {
				callbackFn : null
			}
			var config = $.extend(defaults, options || {});
			var resultObj = {
				wo_id : null
			}

			/** *如果show:false 则需要执行modal('show')方法** */
			// $('#selectBayToolDialog').modal('show');
			/** **********************Bay Select ********************** */
			function worderSelFun() {
				var selectedId = $("#selectWorderDialog_worderGrd").getGridParam("selrow");
				if (selectedId == null || selectedId == undefined) {
					showErrorDialog("003", "请选择工单");
					return false
				}
				wo_id = $("#selectWorderDialog_worderGrd").getCell(selectedId, 'wo_id');
				resultObj.wo_id = wo_id;
				// 将结果返回
				if (typeof config.callbackFn == 'function') {
					config.callbackFn(resultObj);
				}
				$('#selectWorderDialog').modal('hide');
			}

			function selectWorderShowFnc() {
				$("#selectWorderDialog_worderGrd").jqGrid({
					datatype : "local",
					mtype : "POST",
					autowidth : true,// 宽度根据父元素自适应
					resizable : true,
					shrinkToFit : false,
					scroll : true,
					ondblClickRow : worderSelFun,
					colModel : [ {
						name : 'wo_id',
						index : 'wo_id',
						label : WO_ID_TAG,
						width : 80,
						align : "center"
					}, {
						name : 'wo_dsc',
						index : 'wo_dsc',
						label : WO_DSC_TAG,
						width : 120,
						align : "center"
					}, {
						name : 'wo_cate',
						index : 'wo_cate',
						label : WO_CATE_TAG,
						width : 60,
						align : "center"
					}, {
						name : 'wo_stat',
						index : 'wo_stat',
						label : WO_STAT_TAG,
						width : 60,
						align : "center"
					}, {
						name : 'so_id',
						index : 'so_id',
						label : SO_ID_TAG,
						width : 80,
						align : "center"
					}, {
						name : 'mdl_id_fk',
						index : 'mdl_id_fk',
						label : MDL_ID_TAG,
						width : 90,
						align : "center"
					}, {
						name : 'pln_stb_date',
						index : 'pln_stb_date',
						label : PLN_STB_DATE_TAG,
						width : 80,
						align : "center"
					}, {
						name : 'pln_cmp_date',
						index : 'pln_cmp_date',
						label : PLAN_CMP_DATE_TAG,
						width : 80,
						align : "center"
					}, {
						name : 'pln_prd_qty',
						index : 'pln_prd_qty',
						label : PLN_PRD_QTY_TAG,
						width : 80,
						align : "center"
					}, {
						name : 'rl_prd_qty',
						index : 'rl_prd_qty',
						label : RL_PRD_QTY_TAG,
						width : 80,
						align : "center"
					}, {
						name : 'dest_shop',
						index : 'dest_shop',
						label : DEST_SHOP_TAG,
						width : 60,
						align : "center"
					}, {
						name : 'plant_id',
						index : 'plant_id',
						label : PLANT_ID_TAG,
						width : 60,
						align : "center"
					}, {
						name : 'vcr_flg_fk',
						index : 'vcr_flg_fk',
						label : VCR_FLG_TAG,
						width : 80,
						align : "center"
					}, ],
					viewrecords : true,
					pager : '#selectWorderDialog_worderPg',
					sortname : 'wo_id',
					sortorder : "asc",
					loadonce : true,
					rownumbers : true,
					rownumWidth : 20,
					jsonReader : {
						repeatitems : false
					},
					onSelectRow : function(id) {
						worderSelFun();
					}
				});

				var inTrxObj = {};
				if (wo_type == "W") {
					$("#titleSpn").text("选择工单");
					inTrxObj = {
						trx_id : "XPAPLYWO",
						action_flg : 'W'
					}
				} else if (wo_type == "S") {
					$("#titleSpn").text("选择订单");
					inTrxObj = {
						trx_id : "XPAPLYSO",
						action_flg : 'S',
						iary : {}
					}
				} else {
					showErrorDialog("003", "wo_type没有赋值，请检查程式调用函数之前是否写了wo_type:W");
					return false;
				}

				var outTrxObj = comTrxSubSendPostJson(inTrxObj);
				if (outTrxObj.rtn_code != "0000000") {
					return false;
				}
				setGridInfo(outTrxObj.oary, "#selectWorderDialog_worderGrd");
				var resetJqgrid = function() {
					$(window).unbind("onresize");
					$("#selectWorderDialog_worderGrd").setGridWidth($("#selectWorderDialog_worderDiv").width() * 0.90);
					$("#selectWorderDialog_worderGrd").setGridHeight($("#selectWorderDialog_worderDiv").height() * 0.80);
					$(window).bind("onresize", this);
				}
				resetJqgrid();
				$(window).resize(function() {
					resetJqgrid();
				});
			}

			$('#selectWorderDialog').unbind("shown");
			$('#selectWorderDialog').bind("shown", selectWorderShowFnc);
			$("#selectWorderDialog_selectWorderBtn").unbind("click");
			$("#selectWorderDialog_selectWorderBtn").bind("click", worderSelFun);

			/** * 将Div实例化为modal窗体 ** */
			$('#selectWorderDialog').modal({
				backdrop : true,
				keyboard : false,
				show : true
			});
			// $('#selectBayToolDialog').modal('show');
		}
	});
})(jQuery);