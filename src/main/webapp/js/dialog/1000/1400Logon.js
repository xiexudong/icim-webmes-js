  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  在制保留取消，1100需绑定批次号，1400不需要绑定                                */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2014/07/31 N0.00   vincent.M      Initial release                        */
  /*                                                                        */
  /**************************************************************************/


(function($){
    $.fn.extend({
  /**************************************
    ******* 当调用dialog时，需要方式事件被多次执行 ==>使用unbind,bind方法
    ******* shown方法也要unbind ==> bind ==> modal("show"),防止options的值没有传入
    ******* 
  **************************************/
    	"showWaitBoxDialog" : function(options) {
	        //定义返回类型和返回json格式
    		var VAL ={
    		        NORMAL     : "0000000"  ,
    		        T_XPMOVEIN  : "XPMOVEIN"  ,
    		        ope_id     : options.ope_id,
    		        ope_ver    : options.ope_ver,
    				user_id    : options.user_id,
    				boxOary    : options.boxOary
    		}
    		var controlsQuery2 = {
    				Dialog_1400Logon : $("#Dialog_1400Logon"),
    				boxIdInput : $("#1400Logon_boxIdInput"),
    				mainGrd   :{
    					boxInfoGrd : $("#boxInfoGrd")   ,
    					boxInfoPg : "#boxInfoPg"       
		            }
    		}
    		var btnQuery = {
    				Dialog_move_in_btn : $("#Dialog_move_in_btn"),
    		}
			/*** 将Div实例化为modal窗体 ***/
    		controlsQuery2.Dialog_1400Logon.modal({
				backdrop:true,
				keyboard:false,
				show:false
			});
	  		/*************
	          (1)show:false 则需要执行modal('show')方法
	          (2)需要unbind('show')，避免 未获取options传过来的数据之前,就showDialog 
	        ***/
	        function boxMoveInFnc(){
	            var iRow,
	                selectRowIds,
	                selectRowIdsLen,
	                selectRow,
	                movein_rc,
	                inObj,
	                outObj;
	
	            selectRowIds = controlsQuery2.mainGrd.boxInfoGrd.jqGrid('getGridParam','selarrrow');
	            selectRowIdsLen = selectRowIds.length;
	            if( selectRowIdsLen <= 0 ){
	                showErrorDialog("01","请选择要释放的箱号！");
	                return false;
	            }
	
	            for( iRow = 0; iRow<selectRowIdsLen; iRow++){
	                selectRow = controlsQuery2.mainGrd.boxInfoGrd.jqGrid("getRowData", selectRowIds[iRow]);
	
	                box_id = selectRow['box_id'];
	                if(!box_id){
	                    showErrorDialog("02","请选择要释放的箱号！");
	                    return false;
	                }
	
	                movein_rc = selectRow['rc'];
	                if("Y" === movein_rc){
	                    showErrorDialog("", box_id + "已经入账，请勿重复操作！");
	                    return false;
	                }
	                
	                if (VAL.user_id == "" || VAL.user_id == null || VAL.user_id == undefined){
	                	showErrorDialog("","请重新登录！");
	                	return false;
	                }
	                
	                inObj = {
	                    trx_id     : VAL.T_XPMOVEIN,
	                    evt_usr    : VAL.user_id  ,
	                    box_id     : box_id        ,
	                    cr_ope_id  : VAL.ope_id        ,
	                    cr_ope_ver : VAL.ope_ver
	                };
	                outObj = comTrxSubSendPostJson(inObj);
	                if(outObj.rtn_code == VAL.NORMAL) {
	                    selectRow['rc'] = "Y";
	                    controlsQuery2.mainGrd.boxInfoGrd.jqGrid("setRowData", selectRowIds[iRow], selectRow);
	                }else{
	                    selectRow['rc'] = "N";
	                    controlsQuery2.mainGrd.boxInfoGrd.jqGrid("setRowData", selectRowIds[iRow], selectRow);
	                }
	            }
	        }
	        /**
	         * grid  initialization
	         */
	        var iniFunc = function(){
	        	 var grdInfoCM = [
	                  {name: 'rc',        index: 'rc',       label: RELEASE_RESULT_TAG    , width: RC_CLM},
	                  {name: 'isJumpOpe', index: 'isJumpOpe',label: '返品'    , width: REWK_FLAG_CLM},
	                  {name: 'lot_id',    index: 'lot_id',   label: LOT_ID_TAG  , width: LOT_ID_CLM},
	                  {name: 'box_id',    index: 'box_id',   label: CRR_ID_TAG  , width: BOX_ID_CLM},
	                  {name: 'mtrl_prod_id',   index: 'mtrl_prod_id',   label: MTRL_PROD_ID_TAG   , width: MTRL_PROD_ID_CLM},
	                  {name: 'mdl_id',         index: 'mdl_id',         label: MDL_ID_TAG         , width: MDL_ID_CLM},
	                  {name: 'fm_mdl_id_fk',   index: 'fm_mdl_id_fk',   label: FM_MDL_ID_TAG      , width: MDL_ID_CLM},
	                  /*{name: 'cut_mdl_id_fk',  index: 'cut_mdl_id_fk',  label: CUT_MDL_ID_TAG     , width: MDL_ID_CLM},*/
//	                  {name: 'mode_cnt',     index: 'mode_cnt',     label: MODE_CNT_TAG      , width: MODE_CNT_CLM},
//	                  {name: 'ok_grade_cnt', index: 'ok_grade_cnt', label: OK_CNT_TAG        , width: QTY_CLM},
//	                  {name: 'ng_grade_cnt', index: 'ng_grade_cnt', label: NG_CNT_TAG        , width: QTY_CLM},
//	                  {name: 'gk_grade_cnt', index: 'gk_grade_cnt', label: GRADE_CONTROL_TAG , width: QTY_CLM},
//	                  {name: 'lz_grade_cnt', index: 'lz_grade_cnt', label: GRADE_SCRP_TAG    , width: QTY_CLM},
//	                  {name: 'from_thickness', index: 'from_thickness', label: FROM_THICKNESS_TAG , width: QTY_CLM},
//	                  {name: 'to_thickness',   index: 'to_thickness',   label: TO_THICKNESS_TAG   , width: QTY_CLM},
//	                  {name: 'qty',       index: 'qty',      label: QTY_TAG     , width: QTY_CLM},
	                  {name: 'path_id'  , index: 'path_id',  label: PATH_ID_TAG , width: PATH_ID_CLM},
	                  {name: 'ope_id'   , index: 'ope_id',   label: OPE_ID_TAG  , width: OPE_ID_CLM},
	                  {name: 'wo_id'   ,  index: 'wo_id',    label: WO_ID_TAG   , width: WO_ID_CLM},
	                  {name: 'priority',  index: 'priority', label: PRTY_TAG    , width: PRIORITY_CLM}
	              ];
	              controlsQuery2.mainGrd.boxInfoGrd.jqGrid({
						url:"",
						datatype : "local",
						mtype : "POST",
						/*autowidth : true,*/
						width:$("#Dialog_1400Logon").width()*0.82,
						rownumbers : true,
						scroll : true,
						resizable : true,
						viewrecords:true,
						height:200,
						rownumWidth : 20,
						multiselect : true,
						pager : controlsQuery2.mainGrd.boxInfoPg,
						colModel: grdInfoCM
	              });
	            
	            controlsQuery2.boxIdInput.val("");
	        	controlsQuery2.mainGrd.boxInfoGrd.jqGrid("clearGridData");
	        	setGridInfo(VAL.boxOary, "#boxInfoGrd",true);
	        };
	        
        	iniFunc();
	        /*************
	            当多次弹出对话框的同时也会多次绑定click事件。
	            解决办法: 在show Dialog时将click unbind全部掉
	                      然后再绑定，这样就只剩下一个click事件了。
	          **********/
	        controlsQuery2.Dialog_1400Logon.unbind('shown.bs.modal');
	        btnQuery.Dialog_move_in_btn.unbind('click');
	        controlsQuery2.boxIdInput.unbind('keydown');
	        
	        controlsQuery2.Dialog_1400Logon.bind('shown.bs.modal');
	        controlsQuery2.Dialog_1400Logon.modal("show");
	        btnQuery.Dialog_move_in_btn.bind('click',boxMoveInFnc);
	        controlsQuery2.boxIdInput.bind('keydown',function(event){
	        	if(event.keyCode == 13){
	        		var box_id = $.trim(controlsQuery2.boxIdInput.val());
	        		_findRowInGrid('boxInfoDiv', $("#boxInfoGrd"), 'box_id', box_id);
	        	}
	        });
        	
        	controlsQuery2.Dialog_1400Logon.modal("show");
    	}
    });
})(jQuery);