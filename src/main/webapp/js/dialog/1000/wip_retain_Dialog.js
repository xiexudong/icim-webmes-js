  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  在制保留取消，1100需绑定批次号，1400不需要绑定                                */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2014/07/31 N0.00   vincent.M      Initial release                        */
  /*                                                                        */
  /**************************************************************************/


(function($){
    $.fn.extend({
  /**************************************
    ******* 当调用dialog时，需要方式事件被多次执行 ==>使用unbind,bind方法
    ******* shown方法也要unbind ==> bind ==> modal("show"),防止options的值没有传入
    ******* 
  **************************************/
    	"showWipRetainCancelDialog" : function(options) {
	        //定义返回类型和返回json格式
    		var VAL ={
    		        NORMAL        : "0000000"  ,
    		        T_XPINQBOX    : "XPINQBOX" ,
    		        T_XPWIPRETAIN : "XPWIPRETAIN" ,
    		        action_flg    : options.action_flg,
    		        ope_id        : options.ope_id,
    		        ope_ver       : options.ope_ver,
    				wo_id         : options.wo_id,
    				tool_id       : options.tool_id,
    				user_id       : options.user_id
    		};
    		var controlsQuery = {
    				wip_retain_Dialog : $("#wip_retain_Dialog"),
    				buffInfoDiv2 : $("#buffInfoDiv2"),
    				prdSeqIdInput : $("#prdSeqIdInput"),
    				lotId2Input : $("#lotId2Input"),
    				mainGrd   :{
	    				wipRetainInfoGrd : $("#wipRetainInfoGrd")   ,
	    				wipRetainInfoPg : "#wipRetainInfoPg"       ,
	    				wipRetainDetailInfoGrd : $("#wipRetainDetailInfoGrd")   ,
	    				wipRetainDetailInfoPg : "#wipRetainDetailInfoPg"
		            }
    		};
    		var btnQuery = {
    				Dialog_cancel_retain_btn : $("#Dialog_cancel_retain_btn"),
    				regist_lot_btn : $("#regist_lot_btn"),
    				sc : $("#cancel_sc_chk"),
    				sc_query_btn : $("#sc_query_btn"),
    				okng_query_btn : $("#okng_query_btn")
    		};
			/*** 将Div实例化为modal窗体 ***/
    		controlsQuery.wip_retain_Dialog.modal({
				backdrop:true,
				keyboard:false,
				show:false
			});
	  		/*************
	          (1)show:false 则需要执行modal('show')方法
	          (2)需要unbind('show')，避免 未获取options传过来的数据之前,就showDialog 
	        ***/
	        function queryRetainBoxFnc(){
	        	var inObj,outObj,box_cnt,boxAry;
	        	
	        	boxAry = [];
	        	
	        	inObj = {
	        			trx_id      : VAL.T_XPINQBOX,
	                    action_flg  : 'R'           ,
	                    wo_id       : VAL.wo_id,
	                    ope_id      : VAL.ope_id,
	                    ope_ver     : VAL.ope_ver
    	        };
    	        outObj = comTrxSubSendPostJson(inObj);
    	        if (outObj.rtn_code == VAL.NORMAL) {
    	        	box_cnt = parseInt(outObj.box_cnt, 10);
                    if(0 < box_cnt){
                        if(1 == box_cnt){
                        	boxAry.push(outObj.table);
                        }else{
                        	boxAry = outObj.table;
                        }
                        setGridInfo(boxAry, "#wipRetainInfoGrd",true);
                    }else{
        	        	controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("clearGridData");
        	        	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
                    }
    	        }else{
    	        	controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("clearGridData");
    	        	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
    	        }

	        }
            
	        function queryRetainShtSCFnc(){
	        	var inObj,outObj,prdAry;
	        	
	        	boxAry = [];
	        	
	        	inObj = {
	        			trx_id      : "XPINQBOX",
	                    action_flg  : 'E'       ,
	                    ope_id      : VAL.ope_id,
	                    ope_ver     : VAL.ope_ver
    	        };
    	        outObj = comTrxSubSendPostJson(inObj);
    	        if(outObj.rtn_code == VAL.NORMAL){
	   	    		var prdAry = [];
//	   	    		var prd_cnt = outObj.table.length;
//	   	    		if(1 == prd_cnt){
//	   	    			prdAry.push(outObj.table);
//	   	    		}else{
	                    prdAry = outObj.table;
//	                }
	   	    		setGridInfo(outObj.table, "#wipRetainDetailInfoGrd",true);
	   	    	}else{
	   	    		controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
	   	    	}
	        }
	        
	        function cancelRetainFnc(){
	        	var inObj,outObj,selectIds,ids,iary = [];
	        	
	        	if (btnQuery.sc[0].checked){
	        		ids = controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid('getGridParam','selarrrow');
	        		if(ids.length > 0){
	        			for(var i=0;i < ids.length; i++){
				        	var selectRowData = controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("getRowData",ids[i]);
				        	var prd_seq_id = selectRowData['prd_seq_id'];
				            var tmpIary = {
			            		prd_seq_id : prd_seq_id
			                };
				            iary.push(tmpIary);
			        	}
		        		inObj = {
			                    trx_id     : VAL.T_XPWIPRETAIN,
			                    action_flg : "E",
			                    evt_usr    : VAL.user_id  ,
			                    box_cnt    : iary.length  ,
			                    iary       : iary
			                };
			            outObj = comTrxSubSendPostJson(inObj);
			            if(outObj.rtn_code == VAL.NORMAL) {
			            	showSuccessDialog("成功取消在制保留！");
//			            	queryRetainBoxFnc();
			            	return;
			            }else {
			            	showErrorDialog("001","取消在制保留失败！");
			        		return;
			            }
		        	}
	        	}
	        	
	        	selectIds = controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid('getGridParam','selarrrow');
	        	if(selectIds.length == 0){
	        		return;
	        	}
	        	
	        	for(var i=0;i < selectIds.length; i++){
		        	var selectRowData = controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("getRowData",selectIds[i]);
		        	var box_id = selectRowData['box_id'];
		            var tmpIary = {
	            		box_id : box_id
	                };
		            iary.push(tmpIary);
	        	}
                
	            inObj = {
	                    trx_id     : VAL.T_XPWIPRETAIN,
	                    action_flg : "C",
	                    evt_usr    : VAL.user_id  ,
	                    box_cnt    : iary.length  ,
	                    iary       : iary
	                };
	            outObj = comTrxSubSendPostJson(inObj);
	            if(outObj.rtn_code == VAL.NORMAL) {
	            	showSuccessDialog("成功取消在制保留！");
	            	queryRetainBoxFnc();
	            	return;
	            }
	        }
	        function autoCreateLotIdFunc(){
	        	var inTrx, outTrx;
				inTrx = {
					trx_id : "XPINQLOT",
					action_flg : "C",
					wo_id : VAL.wo_id,
					tool_id : VAL.tool_id,
					evt_usr : VAL.user_id
				};
				outTrx = comTrxSubSendPostJson(inTrx);
				if (outTrx.rtn_code == "0000000") {
					controlsQuery.lotId2Input.val(outTrx.lot_id);
				}
	        }
	        function registLotFnc(){
	        	
	        	if($("#wholeFlg")[0].checked){
	        		var box_id = $("#boxIdInput").val();
		        	var lot_id = controlsQuery.lotId2Input.val();
		        	if(!box_id){
		        		showErrorDialog("001","请选择需要绑定批次的箱子！");
		        		return;
		        	}
		        	if(!lot_id){
		        		autoCreateLotIdFunc();
		        		lot_id = controlsQuery.lotId2Input.val();
		        	}
		            var inObj = {
		                    trx_id     : VAL.T_XPWIPRETAIN,
		                    action_flg : "F",
		                    evt_usr    : VAL.user_id  ,
		                    box_id  : box_id,
		                    lot_id     : lot_id
		                };
		            var outObj = comTrxSubSendPostJson(inObj);
		            if(outObj.rtn_code == VAL.NORMAL) {
		        	     $("#boxIdInput").val(box_id);
		        	     controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
		        	     if(box_id){
		        	    	 var inObj = {
		        	                 trx_id      : VAL.T_XPINQBOX,
		        	                 action_flg  : 'P'           ,
		        	                 order_slot_flg : 'Y'        ,
		        	                 box_id      : box_id
		        	             };
		        	    	 var outObj = comTrxSubSendPostJson(inObj); 
		        	    	 if(outObj.rtn_code == VAL.NORMAL){
		        	    		 var prdAry = [];
		        	    		 var prd_cnt = outObj.prd_qty;
		        	    		 if(1 == prd_cnt){
		        	    			 prdAry.push(outObj.table);
		        	    		 }else{
                                   prdAry = outObj.table;
                                }
		        	    		 setGridInfo(prdAry, "#wipRetainDetailInfoGrd",true);
		        	    	 }else{
		        	    		 controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
		        	    	 }
		        	     }
		            }
	        	}else {
	        		var prd_seq_id = controlsQuery.prdSeqIdInput.val();
		        	var lot_id = controlsQuery.lotId2Input.val();
		        	if(!prd_seq_id){
		        		showErrorDialog("001","请选择需要绑定批次的产品！");
		        		return;
		        	}
		        	if(!lot_id){
		        		autoCreateLotIdFunc();
		        		lot_id = controlsQuery.lotId2Input.val();
		        	}
		            var inObj = {
		                    trx_id     : VAL.T_XPWIPRETAIN,
		                    action_flg : "A",
		                    evt_usr    : VAL.user_id  ,
		                    prd_seq_id : prd_seq_id,
		                    lot_id     : lot_id
		                };
		            var outObj = comTrxSubSendPostJson(inObj);
		            if(outObj.rtn_code == VAL.NORMAL) {
		            	var rowId = controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid('getGridParam','selrow');
		            	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid('setRowData', rowId, {lot_id: lot_id});
		            	controlsQuery.prdSeqIdInput.val("");
		            }
	        	}
	        }
	        /**
	         * grid  initialization
	         */
	        var iniFunc = function(){
	            var grdInfoCM = [
	                {name: 'box_id',index: 'box_id',label: '箱号' , width: BOX_ID_CLM},
	                {name: 'prd_qty',index: 'box_id',label:'数量' , width: QTY_CLM}
	            ];
	            controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid({
	                url:"",
	                datatype:"local",
	                mtype:"POST",
	                height:200,
	                width:280,
	                shrinkToFit:false,
	                scroll:true,
	                rownumWidth : true,
	                resizable : true,
	                rowNum:40,
	                loadonce:true,
	                fixed:true,
	                viewrecords:true,
	                multiselect : true,
	                pager : controlsQuery.mainGrd.wipRetainInfoPg,
	                onSelectRow : function(id,status){
			       	     if(status){
			        	     var selectRowData =  controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("getRowData",id);
			        	     var box_id = selectRowData['box_id'];
			        	     $("#boxIdInput").val(box_id);
			        	     if(box_id){
			        	    	 var inObj = {
			        	                 trx_id      : VAL.T_XPINQBOX,
			        	                 action_flg  : 'P'           ,
			        	                 order_slot_flg : 'Y'        ,
			        	                 box_id      : box_id
			        	             };
			        	    	 var outObj = comTrxSubSendPostJson(inObj); 
			        	    	 if(outObj.rtn_code == VAL.NORMAL){
			        	    		 var prdAry = [];
			        	    		 var prd_cnt = outObj.prd_qty;
			        	    		 if(1 == prd_cnt){
			        	    			 prdAry.push(outObj.table);
			        	    		 }else{
                                        prdAry = outObj.table;
                                     }
			        	    		 setGridInfo(prdAry, "#wipRetainDetailInfoGrd",true);
			        	    	 }else{
			        	    		 controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
			        	    	 }
			        	     }
			    	     }else{
			    	    	 controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
			    	    	 $("#boxIdInput").val("");
			    	     }
	            	},
	                colModel: grdInfoCM
	            });

	            var grdInfoCMDetail= [
	                {name: 'slot_no',        index: 'slot_no',     label: SLOT_NO_TAG,    width: SLOT_NO_CLM,hidden:true},
	                {name: 'prd_seq_id',     index: 'prd_seq_id',  label: PRD_SEQ_ID_TAG,  width: PRD_SEQ_ID_CLM},
	                {name: 'lot_id',         index: 'lot_id',        label: LOT_ID_TAG  ,   width: LOT_ID_CLM},
	                {name: 'mtrl_box_id_fk', index: 'mtrl_box_id_fk', label: '来料箱号',  width: BOX_ID_CLM},
	                {name: 'wo_id_fk',       index: 'wo_id_fk',      label: WO_ID_TAG,  width: WO_ID_CLM}
	            ];
	            controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid({
	                url:"",
	                datatype:"local",
	                mtype:"POST",
	                height:200,
	                width:600,
	                shrinkToFit:false,
	                scroll:true,
	                rownumWidth : true,
	                resizable : true,
	                rowNum:40,
	                loadonce:true,
	                fixed:true,
	                viewrecords:true,
	                multiselect : true,
	                pager : controlsQuery.mainGrd.wipRetainDetailInfoPg,
	                onSelectRow : function(id){
		        	     var selectRowData =  controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("getRowData",id);
		        	     var prd_seq_id = selectRowData['prd_seq_id'];
		        	     if(prd_seq_id){
		        	    	 controlsQuery.prdSeqIdInput.val(prd_seq_id);
		        	     }
		            },
	                colModel: grdInfoCMDetail
	            });
	            
	            if(VAL.action_flg == "S"){
	            	controlsQuery.buffInfoDiv2.show();
	            }else if(VAL.action_flg == "H"){
	            	controlsQuery.buffInfoDiv2.hide();
	            }else{
	            	return;
	            }
	            controlsQuery.prdSeqIdInput.val("");
	            controlsQuery.lotId2Input.val("");
	        	controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("clearGridData");
	        	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
	        };
	        
        	iniFunc();
	        /*************
	            当多次弹出对话框的同时也会多次绑定click事件。
	            解决办法: 在show Dialog时将click unbind全部掉
	                      然后再绑定，这样就只剩下一个click事件了。
	          **********/
	        controlsQuery.wip_retain_Dialog.unbind('shown.bs.modal');
	        btnQuery.Dialog_cancel_retain_btn.unbind('click');
	        btnQuery.regist_lot_btn.unbind('click');
	
	        controlsQuery.wip_retain_Dialog.bind('shown.bs.modal',queryRetainBoxFnc);
	        controlsQuery.wip_retain_Dialog.modal("show");
	        
	        var ope_id = VAL.ope_id;
	        var ope_ver = VAL.ope_ver;
	        
	        btnQuery.Dialog_cancel_retain_btn.bind('click',cancelRetainFnc);
	        btnQuery.regist_lot_btn.bind('click',registLotFnc);
	        $("#wholeFlg")[0].checked = false;
	        $("#boxIdInput").val("");
	        /*****
	          弹出提示,当获取焦点时
	          需要包括: bootstrap-tooltip.js
	        ******/
	        $('[rel=tooltip]').tooltip({
	          placement:"bottom",
	          trigger:"focus",
	          animation:true,
	        });
	        btnQuery.sc[0].checked = false;
	        
	        btnQuery.sc_query_btn.click (function (){
	        	btnQuery.sc[0].checked = true;
	        	if (btnQuery.sc[0].checked){
	        		controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("clearGridData");
		        	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
	        		queryRetainShtSCFnc();
	        	}else {
	        		controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("clearGridData");
		        	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
	        	}
	        });
	        btnQuery.okng_query_btn.click (function (){
	        	controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("clearGridData");
	        	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
	        	queryRetainBoxFnc();
	        	btnQuery.sc[0].checked = false;
	        });
//	        btnQuery.sc.click(function (){
//	        	if (btnQuery.sc[0].checked){
//	        		controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("clearGridData");
//		        	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
//	        		queryRetainShtSCFnc();
//	        	}else {
//	        		controlsQuery.mainGrd.wipRetainInfoGrd.jqGrid("clearGridData");
//		        	controlsQuery.mainGrd.wipRetainDetailInfoGrd.jqGrid("clearGridData");
//	        	}
//	        });
    	}
    });
})(jQuery);