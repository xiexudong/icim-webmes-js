(function($){
    $.fn.extend({
    	"showSelectBayToolDialog" : function(options) {
        //定义返回类型和返回json格式
	      var defaults = {
		           callbackFn : null
		        }
	     	var config = $.extend(defaults, options || {});
	        var resultObj={
	        		  bayID:null,
	        		  toolID:null
	        }
	        
    	  /*** 将Div实例化为modal窗体 ***/
		  $('#selectBayDialog').modal({
			    backdrop:true,
			    keyboard:true,
			    show:true
		    });
		  $('#selectToolDialog').modal({
			    backdrop:true,
			    keyboard:true,
			    show:false
		    });
		  /***如果show:false 则需要执行modal('show')方法***/
//		  $('#selectBayToolDialog').modal('show');
		  /************************Bay Select ***********************/
		  function  baySelFun(){
			  var selectedId = $("#selectBayAreaDialog_bayAreaGrd").getGridParam("selrow");
			  if(selectedId==null ||selectedId==undefined){
				  alert("请选择区域");//TODO:改成统一报错对话框
				  return false
			  }
              bayID = $("#selectBayAreaDialog_bayAreaGrd").getCell( selectedId , 'bay_id');
              resultObj.bayID = bayID ;
              $('#selectBayDialog').modal('hide');
              $('#selectToolDialog').modal('show');
		  }
		  $('#selectBayDialog').on('shown.bs.modal',function (e){
              $("#selectBayAreaDialog_bayAreaGrd").jqGrid({
                 datatype    : "local",
                 mtype       : "POST",
                 width       : 320,
                 height      : 240,
                 resizable   : true,
                 shrinkToFit : false,
                 scroll      : true,
                 ondblClickRow : baySelFun,
                 colModel    : [
                             { name : 'bay_id'  , index : 'bay_id'  , label: BAY_AREA_TAG  , width : 120 , align : "center" },
                             { name : 'bay_dsc' , index : 'bay_dsc' , label: BAY_DSC_TAG   , width : 190 , align : "center" , sortable : "true"} 
                             ],
                 viewrecords : true, // 
                 pager       : '#selectBayAreaDialog_bayAreaPg',
                 sortname    : 'bay_id',
                 sortorder   : "desc",
                 loadonce    : true,
                 jsonReader  : {
                    repeatitems : false
                 },
                 onSelectRow : function (id) {
                           baySelFun();
                 }
              });
              var inTrxObj = {
                 trx_id      : "XPLSTBAY",
                 action_flg  : 'I'
              }
              var outTrxObj = comTrxSubSendPostJson(inTrxObj);
              if (outTrxObj.rtn_code != "0000000") {
                 return false;
              }
              setGridInfo(outTrxObj.oary,"#selectBayAreaDialog_bayAreaGrd");
              $("#selectBayDialog_selectBayAreaBtn").click(function(){
            	  baySelFun();
              })

              	var resetJqgrid = function(){
	              $(window).unbind("onresize"); 
	              $("#selectBayAreaDialog_bayAreaGrd").setGridWidth($("#selectBayAreaDialog_bayAreaDiv").width()*0.90); 
	              $("#selectBayAreaDialog_bayAreaGrd").setGridHeight($("#selectBayAreaDialog_bayAreaDiv").height()*0.80);     
	              $(window).bind("onresize", this);  
               }  
            	resetJqgrid();
	            $(window).resize(function(){   
	              resetJqgrid();
	           	});
               
		    })
		    /************************Tool Select Fnc***********************/
		    function eqptSelFun(){
                var selectedId = $("#selectToolDialog_toolGrd").getGridParam("selrow");
                if(selectedId==null ||selectedId==undefined){
  				  alert("请选择机台");//TODO:改成统一报错对话框
  				  return false
  			    }
                toolID = $("#selectToolDialog_toolGrd").getCell( selectedId , 'tool_id');
                resultObj.toolID = toolID ;
                //将结果返回
	            if (typeof config.callbackFn == 'function') { 
	                config.callbackFn(resultObj); 
	            }
	    		$("#selectToolDialog").modal('hide');
		    }
		    $('#selectToolDialog').on('shown.bs.modal',function (e){
	            $("#selectToolDialog_toolGrd").jqGrid({
	               datatype    : "local",
	               mtype       : "POST",
	               width       : 320,
	               height      : 240,
	               resizable   : true,
	               shrinkToFit : false,
	               scroll      : true,
	               ondblClickRow : eqptSelFun,
	               colModel    : [
	                           { name : 'tool_id'  , index : 'tool_id'  , label: TOOL_ID_TAG   , width : 120 , align : "center" },
	                           { name : 'tool_dsc' , index : 'tool_dsc' , label: TOOL_DSC_TAG , width : 190 , align : "center" , sortable : "true" }
	                           ],
	               viewrecords : true, // 
	               pager       : '#selectToolDialog_toolPg',
	               sortname    : 'tool_id',
	               sortorder   : "desc",
	               loadonce    : true,
	               jsonReader  : {
	                  repeatitems : false
	               },
	               onSelectRow : function (id) {
	            	   eqptSelFun();
	               }
	            });
	            var inTrxObj1 = {
	               trx_id       : "XPLSTEQP",
	               action_flg   : 'I'       ,
	               bay_id       : resultObj.bayID   
	            }
	            var outTrxObj1 = comTrxSubSendPostJson(inTrxObj1);
	            if (outTrxObj1.rtn_code != "0000000") {
	               return false;
	            }
	            setGridInfo(outTrxObj1.oary,"#selectToolDialog_toolGrd");
	            $("#selectToolDialog_selectToolBtn").click(function(){
	            	eqptSelFun();
	            })
	            var resetJqgrid = function(){
	              $(window).unbind("onresize"); 
	              $("#selectToolDialog_toolGrd").setGridWidth($("#selectToolDialog_toolDiv").width()*0.90); 
	              $("#selectToolDialog_toolGrd").setGridHeight($("#selectToolDialog_toolDiv").height()*0.80);     
	              $(window).bind("onresize", this);  
            	}  
            	resetJqgrid();
	            $(window).resize(function(){   
	              resetJqgrid();
	           	});
		    })
    	}
    });
})(jQuery);