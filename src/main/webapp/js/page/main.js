$(document).ready(function() { 
  var TRX_XPANNOUN = 'XPANNOUN',
      EVT_USER = $("#userId").text();
 
  var initializationFunc = function(){
    listAnnounceFunc();
  };

  /**
   * List all announcement info 
   * @return {[type]} [description]
   */
  var listAnnounceFunc = function(){
      var inTrxObj = {
        trx_id: TRX_XPANNOUN,
        type: 'L'
      };
      var outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code == "0000000") {
        var announceCount = outTrxObj.count;
        if(1 == announceCount){
          setAnnounceFunc(outTrxObj.oary.announce_no, outTrxObj.oary.announce_text);
        }else{
          for (var i = 0; i < announceCount; i++){
            setAnnounceFunc(outTrxObj.oary[i].announce_no, outTrxObj.oary[i].announce_text);
          }
        }

      }
      inTrxObj = {
    	    	 trx_id       : "XPLSTDAT",
    	    	 action_flg   : "Q",
    	    	 iary         : {
    	    		 data_cate: "SPCL"
    	    	 }
    	      };
      outTrxObj = comTrxSubSendPostJson(inTrxObj);
      var href = outTrxObj.oary ? outTrxObj.oary.data_desc : ""; 
      if(outTrxObj.rtn_code === "0000000" && href){
    	  $("#spcPage").attr("href",href);
      }
  };

  /**
   * setAnnounceFunc
   * @param {[type]} announce_no   [description]
   * @param {[type]} announce_text [description]
   */
  var setAnnounceFunc = function(announce_no, announce_text){
      if('01' == announce_no){
        $('#mainInfoTxt').val(announce_text);
      }else if('02' == announce_no){
        $('#leftInfoTxt').val(announce_text);
      }else if('03' == announce_no){
        $('#rightInfoTxt').val(announce_text);
      }
  };

  /**
   * Add/Update announcement info 
   * @param  {[type]} announce_no   [textarea no]
   * @param  {[type]} announce_text [announcement text]
   * @return {[type]}               [description]
   */
  var updateAnounceFunc = function(announce_no, announce_text){
      var inTrxObj = {
        trx_id        : TRX_XPANNOUN ,
        type          : 'U'          ,
        announce_no   : announce_no  ,
        announce_text : announce_text,
        evt_usr       : EVT_USER
      };
      var outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code == "0000000") {
          showSuccessDialog("更新成功！")
      }else{
          showErrorDialog("","公告更新失败！");
      }
  };

  $('#updateAnnounce1Btn').click(function(){
    updateAnounceFunc("01", $('#mainInfoTxt').val());
  });
  $('#updateAnnounce2Btn').click(function(){
    updateAnounceFunc("02", $('#leftInfoTxt').val());
  });
  $('#updateAnnounce3Btn').click(function(){
    updateAnounceFunc("03", $('#rightInfoTxt').val());
  });    
  initializationFunc();
});