$(document).ready(function() {
    var VAL ={
        NORMAL     : "0000000"  ,
        EVT_USER   : $("#userId").text(),
        T_XPBANKIO : "XPBANKIO" ,
        T_XPINQBOX : "XPINQBOX"
    };

    /**
     * All controls's jquery object/text
     * @type {Object}
     */
    var controlsQuery = {
        W              : $(window)           ,
        boxIDInput     : $("#boxIDInput")    ,
        mainGrd   :{
            grdId     : $("#boxInfoGrd")   ,
            grdPgText : "#boxInfoPg"       ,
            fatherDiv : $("#boxInfoGrdDiv")
        }
    };

    /**
     * All button's jquery object
     * @type {Object}
     */
    var btnQuery = {
        queryBtn : $('#query_btn'),
        clearBtn : $('#clear_btn'),
        exportBtn :$('#export_btn'),
        uploadBtn :$('#upload_btn')
    };

    /**
     * All tool functions
     * @type {Object}
     */
    var toolFunc = {
        resetJqgrid :function(){
            controlsQuery.W.unbind("onresize");
            controlsQuery.mainGrd.grdId.setGridWidth(controlsQuery.mainGrd.fatherDiv.width()*0.80);
            controlsQuery.W.bind("onresize", this);
        },
        clearInput : function(){
            controlsQuery.boxIDInput.val("");
//            controlsQuery.shipUserInput.val("");
//            controlsQuery.pathID.attr("disabled",true);
//            controlsQuery.pathChk.attr("checked",false);
//            toolFunc.setPathProc();
        }
    };

    /**
     * All button click function
     * @type {Object}
     */
    var btnFunc = {
    	query_func:function(){
    		var inObj,outObj,boxId;
    		boxId = controlsQuery.boxIDInput.val();
        	if(!boxId){
        		showErrorDialog("","箱子代码不能为空,请输入箱子代码");
                return false;
        	}
    		inObj = {
    			trx_id:"XPINQBOX",
    			action_flg:"S",
    			box_id: controlsQuery.boxIDInput.val()
    		};
    		outObj = comTrxSubSendPostJson(inObj);
    		if(outObj.rtn_code=="0000000"){
    			setGridInfo(outObj.table,"#boxInfoGrd")
    		}
    	},
        //Clear
        clear_func : function(){
            controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
            $("#xmlTxt").val("");
            controlsQuery.boxIDInput.val("");
        },
        //Delete
        del_func : function(){
            var i, j,
                curGrid = controlsQuery.mainGrd.grdId,
                ids = curGrid.jqGrid("getGridParam","selarrrow");
            for ( i = 0, j = ids.length; i < j; i++) {
                curGrid.jqGrid('delRowData', ids[0]);
            }
        },
        export_func : function(){
        	var boxId = controlsQuery.boxIDInput.val();
        	
        	 window.location.href = "jcom/exportFile.action?boxId="+boxId;
//        	if(!boxId){
//        		showErrorDialog("","箱子代码不能为空,请输入箱子代码");
//                return false;
//        	}
//        	$.ajax({
//				type : "post",
//				url : "jcom/exportCdtmXml.action",
//				timeout : 60000, // TODO 1min
//				data : {
//					boxId : boxId
//				},
//				async : false, // false代表等待ajax执行完毕后才执行alert("ajax执行完毕")语句;
//				beforeSend : function() {
//					// $("#loadingImgDiv").show();
//				},
//				complete : function() {// ajaxStop改为ajaxComplete也是一样的
//					// $("#loadingImgDiv").hide();
//				},
//				success : function(data) {
//					var outObj = data.cdtmOutO;
//					if(outObj.rtn_code===0){
//						$("#xmlTxt").val(outObj.xml)
//						showSuccessDialog("导出信息成功");
//					}else{
//						showErrorDialog("导出信息失败",outObj.rtn_mesg);
//		                return false;
//					}
////					$("#xmlTxt").val(data.cdtmOutO.xml);
////					showSuccessDialog("导出信息成功");
//				},
//				error : function(xhr, stat, e) {
//					console.error(xhr);
//
//				}
//
//			});
        	
        },
        upload_func : function(){
        	var boxId = controlsQuery.boxIDInput.val();
        	if(!boxId){
        		showErrorDialog("","箱子代码不能为空,请输入箱子代码");
                return false;
        	}
        	$.ajax({
				type : "post",
				url : "jcom/updateCdtmXml.action",
				timeout : 60000, // TODO 1min
				data : {
					boxId : boxId
				},
				async : false, // false代表等待ajax执行完毕后才执行alert("ajax执行完毕")语句;
				beforeSend : function() {
					// $("#loadingImgDiv").show();
				},
				complete : function() {// ajaxStop改为ajaxComplete也是一样的
					// $("#loadingImgDiv").hide();
				},
				success : function(data) {
					var outObj = data.cdtmOutO;
					if(outObj.rtn_code===0){
						$("#xmlTxt").val(outObj.xml)
						showSuccessDialog("上传资料成功");
					}else{
						showErrorDialog("上传资料失败",outObj.rtn_mesg);
		                return false;
					}
				},
				error : function(xhr, stat, e) {
					console.error(xhr);

				}

			});
        }
    };

    /**
     * Bind button click action
     */
    var iniButtonAction = function(){
        btnQuery.clearBtn.click(function(){
            btnFunc.clear_func();
        });
        btnQuery.queryBtn.click(function(){
            btnFunc.query_func();
        });
        btnQuery.exportBtn.click(function(){
        	btnFunc.export_func();
        });
        btnQuery.uploadBtn.click(function(){
        	btnFunc.upload_func();
        })
        
    };

    /**
     * grid  initialization
     */
    var iniGridInfo = function(){
        var infoCM = [
            {name: 'box_id_fk',   index: 'box_id_fk',  label: CRR_ID_TAG       , width: 150},
            {name: 'prd_stat'   , index: 'prd_stat',   label: STATUS_TAG       , width: 60},
            {name: 'lot_id',      index: 'lot_id',     label: "厂内批次号"       , width: 100},
            {name: 'mdl_id_fk',   index: 'mdl_id_fk',  label: MDL_ID_TAG       , width: 120},
            {name: 'wo_id_fk',    index: 'wo_id_fk',   label: WO_ID_TAG        , width: 100},
            {name: 'so_id_fk',    index: 'so_id_fk',   label: SO_NO_TAG        , width: 100}
        ];
        controlsQuery.mainGrd.grdId.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height: 360,
            width: 700,
            shrinkToFit:false,
            scroll:true,
            rownumWidth : true,
            resizable : true,
            rowNum:40,
            loadonce:true,
            multiselect : true,
            viewrecords:true,
            pager : controlsQuery.mainGrd.grdPgText,
            fixed: true,
            colModel: infoCM
        });
    };
    
    /**
     * Other action bind
     */
    var otherActionBind = function(){
        // Reset size of jqgrid when window resize
        controlsQuery.W.resize(function(){
            toolFunc.resetJqgrid();
        });

        controlsQuery.boxIDInput.keydown(function(event){
            if(event.keyCode === 13){
//                btnFunc.add_func();
            }
        });
    };
    
    /**
     * Ini view and data
     */
    var initializationFunc = function(){
        iniGridInfo();
        iniButtonAction();
        otherActionBind();
        toolFunc.clearInput();
    };

    
    initializationFunc();
    
    $("form").submit(function(){
    	return false;
    })
});