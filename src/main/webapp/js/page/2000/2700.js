$(document).ready(function() {
    var VAL ={
        NORMAL      : "0000000"  ,
        EVT_USER    : $("#userId").text(),
        T_XPINQBOX  : "XPINQBOX",
        T_XPINQSHT  : "XPINQSHT",
        T_XPBISOPE  : "XPBISOPE",
        T_XPAPLYWO  : "XPAPLYWO",
        T_XPINQCOD  : "XPINQCOD" ,
        T_XPWOCHG   : "XPWOCHG"
    };

    var gVal = {
        boxIdL    : '', //Old box
        boxIdR    : '', //New box
        ope_id    : '',
        woIdL     : '',
        woIdR     : '',
        prdAryL   : [],  //prds in left
        prdAryR   : [],  //prds in right
        prdAryMV  : [],  //最终被移动的玻璃
        inObjL    : [],
        inObjR    : []
    };
    var gSwhOpe = "";

    /**
     * All controls's jquery object/text
     * @type {Object}
     */
    var controlsQuery = {
        W                 : $(window)          ,
        woIdSelectL       : $("#woIdSelectL")  ,
        woIdSelectR       : $("#woIdSelectR")  ,
        opeSelect         : $("#opeSelect")    ,
        opeR              : $("#opeR")         ,
        ppBoxIDInputL     : $("#ppBoxIDInputL"),
        ppBoxIDInputR     : $("#ppBoxIDInputR"),
        grdDiv            : $("#grdDiv")       ,
        lrBtnDiv          : $("#lrBtnDiv")     ,

        mainGrdL : {
            grdId     : $("#boxDetailGrdL")   ,
            grdPgText : "#boxDetailPgL"       ,
            fatherDiv : $("#boxDetailDivL")
        },
        mainGrdR : {
            grdId     : $("#boxDetailGrdR")   ,
            grdPgText : "#boxDetailPgR"       ,
            fatherDiv : $("#boxDetailDivR")
        }
    };

    /**
     * All button's jquery object
     * @type {Object}
     */
    var btnQuery = {
        query_btn : $("#query_btn"),
        change_btn : $("#change_btn"),
        chgBtnL2R : $("#chgBtnL2R"),
        chgBtnR2L : $("#chgBtnR2L")
    };

    /**
     * All tool functions
     * @type {Object}
     */
    var toolFunc = {
        resetJqgrid :function(){
            controlsQuery.W.unbind("onresize");
            controlsQuery.mainGrdL.grdId.setGridWidth(controlsQuery.mainGrdL.fatherDiv.width()*0.90);
            controlsQuery.mainGrdR.grdId.setGridWidth(controlsQuery.mainGrdR.fatherDiv.width()*0.90);
            controlsQuery.W.bind("onresize", this);
        },
        clearInput : function(){
            controlsQuery.ppBoxIDInputL.val('');
            controlsQuery.ppBoxIDInputR.val('');
            controlsQuery.opeR.val('');
        },
        
        iniWoIdSelect : function(){
            var inObj,
                outObj;
            inObj = {
                trx_id      : VAL.T_XPAPLYWO,
                action_flg  : 'Q'           ,
                iary : {
                    wo_stat : "WAIT"
                }
            };

            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woIdSelectL", false);
                _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woIdSelectR", false);
            }
        },
        iniOpeSelect : function(){
            var inObj, outObj;
            var user_id = VAL.EVT_USER;
            var iaryB = {
                user_id     : user_id
            };
            inObj = {
                trx_id      : VAL.T_XPBISOPE,
                action_flg  : 'S',
                iaryB       : iaryB
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                _setOpeSelectDate(outObj.tbl_cnt, outObj.oary,
                    "ope_id", "ope_ver", "ope_dsc", controlsQuery.opeSelect, true);
            }
        },
        getCurOpe : function(){
            var ope_info,
                ope_info_ary,
                ope_id;

            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];

            return ope_id;
        },
        setOutTrx2Grid : function(outObj, gPrdAry, grdId){
            "use strict";
            var i,
                pnl_cnt,
                prdAry = [];

            pnl_cnt = parseInt(outObj.tbl_cnt, 10);
            if(pnl_cnt > 0){
                if(pnl_cnt === 1){
                    prdAry.push(outObj.table);
                }else{
                    prdAry = outObj.table;
                }
            }

            for( i = 0; i < pnl_cnt; i++ ){
                gPrdAry.push({
                    prd_seq_id : prdAry[i].prd_seq_id,
                    box_id_fk  : prdAry[i].box_id_fk,
                    wo_id_fk   : prdAry[i].wo_id_fk,
                    prd_stat   : prdAry[i].prd_stat,
                    fab_sn     : prdAry[i].fab_sn,
                    change_wo  : 'N'
                });
            }

            setGridInfo(gPrdAry, grdId, true);
        },
        /**
         * 两个grid移动玻璃信息
         * @param srcGrdId 起始grid
         * @param tarGrdId 目标grid
         * @param scrGPrdAry 起始grid的数据源
         * @param tarGPrdAry 目标grid的数据源
         * @param mvRowIds 要移动的行的行号数组
         * @param tarBoxId 目标侧的箱号
         * @param tarWoId 目标侧的内部订单号
         * @returns {boolean}
         */
        mvPrd : function(srcGrdId, tarGrdId, scrGPrdAry, tarGPrdAry, mvRowIds, tarBoxId, tarWoId){
            "use strict";
            var i,
                mvRowIndex,
                scrGPrdAryIndex,
                tmpRowData,
                prdMv = gVal.prdAryMV;

            for(mvRowIndex = mvRowIds.length - 1; mvRowIndex >= 0; mvRowIndex--){
                tmpRowData = srcGrdId.jqGrid('getRowData', mvRowIds[mvRowIndex]);
                for(scrGPrdAryIndex = scrGPrdAry.length - 1; scrGPrdAryIndex >= 0; scrGPrdAryIndex--){
                    if(tmpRowData['prd_seq_id'] === scrGPrdAry[scrGPrdAryIndex]['prd_seq_id']){
                        if(tarBoxId){
                            tmpRowData['box_id_fk'] = tarBoxId;
                        }
                        if(tarWoId){
                            tmpRowData['wo_id_fk'] = tarWoId;
                        }

                        if('N' === tmpRowData['change_wo']){
                            tmpRowData['change_wo'] = 'Y';
                            prdMv.push(tmpRowData);
                        }else if('Y' === tmpRowData['change_wo']){
                            for(i = prdMv.length - 1; i >= 0; i--){
                                if(prdMv[i]['prd_seq_id'] === tmpRowData['prd_seq_id']){
                                    prdMv.splice(i, 1);
                                    break;
                                }
                            }
                            tmpRowData['change_wo'] = 'N';
                        }else{
                            console.error('Invalid change_wo [%s] for [%s]',
                                tmpRowData['change_wo'], tmpRowData['prd_seq_id']);
                            return false;
                        }

                        tarGPrdAry.push(tmpRowData);
                        scrGPrdAry.splice(scrGPrdAryIndex, 1);
                        break;
                    }
                }
            }

            setGridInfo(scrGPrdAry, srcGrdId, true);
            setGridInfo(tarGPrdAry, tarGrdId, true);

            return true;
        },
        getRetBoxInfo : function(box_id){
            "use strict";
            var inObj,
                outObj;

            inObj = {
                trx_id     : VAL.T_XPINQBOX,
                action_flg : "S",
                box_id     : box_id
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
            	if (outObj.box_stat == "EMPT"){
            		if (outObj.bnk_flg == "3") {
                		showErrorDialog("","箱子["+outObj.box_id+"]当前未在制保留，不可操作！");
                        return null;
            		}
            	}
        		return outObj;
            }

            return null;
        }
    };

    /**
     * All button click function
     * @type {Object}
     */
    var btnFunc = {
        //Query
        query_func : function(){
            "use strict";
            var inObjL,
                inObjR,
                outObjL,
                outObjR,
                wo_id_l,
                wo_id_r,
                box_id_l,
                box_id_r,
                ret_box_l,
                ret_box_r,
                ope_id;

            gVal.boxIdL = '';
            gVal.boxIdR = '';
            gVal.woIdL = '';
            gVal.woIdR = '';
            gVal.prdAryL = [];
            gVal.prdAryR = [];
            gVal.prdAryMV = [];

            //inTrx
            inObjL = {
                trx_id     : VAL.T_XPINQSHT,
                action_flg : "C"
            };

            inObjR = {
                trx_id     : VAL.T_XPINQSHT,
                action_flg : "C"
            };

            //WO
            wo_id_l = controlsQuery.woIdSelectL.val();
            if(!wo_id_l){
                showErrorDialog('P2700-101','请选择左侧内部订单!');
                return false;
            }
            wo_id_r = controlsQuery.woIdSelectR.val();
            if(!wo_id_r){
                showErrorDialog('P2700-102','请选择右侧内部订单!');
                return false;
            }

            if(wo_id_l == wo_id_r){
                showErrorDialog('P2700-103','两侧内部订单不能相同！');
                return false;
            }

            inObjL.wo_id = wo_id_l;
            inObjR.wo_id = wo_id_r;
            gVal.woIdL = wo_id_l;
            gVal.woIdR = wo_id_r;

            //box
            box_id_l = controlsQuery.ppBoxIDInputL.val().trim();
            box_id_r = controlsQuery.ppBoxIDInputR.val().trim();
            if( box_id_l && box_id_r && box_id_l === box_id_r){
                showErrorDialog("P2700-104","箱号不能相同！");
                return false;
            }

            //Ope
            ope_id = toolFunc.getCurOpe();

            //Query by box_id or ope
            if(ope_id){
                //by ope
                if(box_id_l || box_id_r){
                    showErrorDialog("P2700-105","站点仅供查询入账后尚未转入箱子的玻璃，查询时请勿输入箱号！");
                    return false;
                }
                if(ope_id == "DM-05" || ope_id == "DM-06" || ope_id == "JB-10" || ope_id == "JB-11"){
                	showErrorDialog("","终检站点后不能做交换订单！");
                	return false;
                }
                gVal.ope_id = ope_id;

                inObjL.ope_id = ope_id;
                inObjL.prd_stat = 'INPR';
                inObjL.space_box_id = 'Y';

                inObjR.ope_id = ope_id;
                inObjR.prd_stat = 'INPR';
                inObjR.space_box_id = 'Y';
            }else{
                //box box_id
                if( !box_id_l || !box_id_r ){
                    showErrorDialog( "P2700-106","请输入两侧箱号！" );
                    return false;
                }

                inObjL.box_id = box_id_l;
                inObjL.prd_stats = "'WAIT','COMP','SHIP'";
                inObjR.box_id = box_id_r;
                inObjR.prd_stats = "'WAIT','COMP','SHIP'";

                gVal.boxIdL = box_id_l;
                gVal.boxIdR = box_id_r;
                ret_box_l = toolFunc.getRetBoxInfo(box_id_l);
//                ret_box_l = prdAryL.box_id;
                if(!ret_box_l){
 //               	showErrorDialog('get box [%s] info failed!', box_id_l);
                    return false;
                }
                ret_box_r = toolFunc.getRetBoxInfo(box_id_r);
//                ret_box_r = prdAryR.box_id;
                if(!ret_box_r){
//                	showErrorDialog('get box [%s] info failed!', box_id_r);
                    return false;
                }

                if(ret_box_l.box_set_code != "JBIC" &&
                    ret_box_r.box_set_code != "JBIC" &&
                    ret_box_l.box_set_code != ret_box_r.box_set_code){
                    showErrorDialog('P2700-107',
                        "箱号["+ box_id_l + "]的设定代码[" + ret_box_l.box_set_code +"] " +
                            "VS 箱号[" + box_id_r + "]的设定代码[" + ret_box_r.box_set_code +"]");
                    return false;
                }
                if(ret_box_l.nx_ope_id_fk == "DM-05" || ret_box_l.nx_ope_id_fk == "DM-06" || ret_box_l.nx_ope_id_fk == "JB-10" || ret_box_l.nx_ope_id_fk == "JB-11"){
                	showErrorDialog("","终检站点后不能做交换订单！");
                	return false;
                }
                if(ret_box_r.nx_ope_id_fk == "DM-05" || ret_box_r.nx_ope_id_fk == "DM-06" || ret_box_r.nx_ope_id_fk == "JB-10" || ret_box_r.nx_ope_id_fk == "JB-11"){
                	showErrorDialog("","终检站点后不能做交换订单！");
                	return false;
                }
                if (ret_box_r.box_stat != "EMPT") {
                    if (ret_box_l.nx_ope_id_fk != ret_box_r.nx_ope_id_fk) {
                        showErrorDialog('P2700-107',
                                "箱号["+ box_id_l + "]的站点[" + ret_box_l.nx_ope_id_fk +"] " +
                                    "VS 箱号[" + box_id_r + "]的站点[" + ret_box_r.nx_ope_id_fk +"]不一致，不能转");
                            return false;
                    }
                }
            	gSwhOpe = ret_box_l.nx_ope_id_fk;
            }

            controlsQuery.mainGrdL.grdId.jqGrid("clearGridData");
            controlsQuery.mainGrdR.grdId.jqGrid("clearGridData");

            //Send
            outObjL = comTrxSubSendPostJson(inObjL);
            if (outObjL.rtn_code == VAL.NORMAL) {
                toolFunc.setOutTrx2Grid(outObjL, gVal.prdAryL, controlsQuery.mainGrdL.grdId);
                if ( outObjL.tbl_cnt == 0 ) {
                    showErrorDialog("","转出WO查询无数据！");
                    return false;                	
                }
            }else{
                return false;
            }

            outObjR = comTrxSubSendPostJson(inObjR);
            if (outObjR.rtn_code == VAL.NORMAL) {
                toolFunc.setOutTrx2Grid(outObjR, gVal.prdAryR, controlsQuery.mainGrdR.grdId);
//                if ( outObjR.tbl_cnt == 0 ) {
//                    showErrorDialog("","查询无数据！");
//                    return false;                	
//                }
            }else{
                return false;
            }
            gVal.inObjL = inObjL;
            gVal.inObjR = inObjR;
            return true;
        },
        
        query1_func : function(){
            "use strict";
            var outObjL,
	            outObjR;
	           
            gVal.prdAryL = [];
            gVal.prdAryR = [];
            gVal.prdAryMV = [];

            controlsQuery.mainGrdL.grdId.jqGrid("clearGridData");
            controlsQuery.mainGrdR.grdId.jqGrid("clearGridData");

            //Send
            outObjL = comTrxSubSendPostJson(gVal.inObjL);
            if (outObjL.rtn_code == VAL.NORMAL) {
                toolFunc.setOutTrx2Grid(outObjL, gVal.prdAryL, controlsQuery.mainGrdL.grdId);
            }else{
                return false;
            }

            outObjR = comTrxSubSendPostJson(gVal.inObjR);
            if (outObjR.rtn_code == VAL.NORMAL) {
                toolFunc.setOutTrx2Grid(outObjR, gVal.prdAryR, controlsQuery.mainGrdR.grdId);
            }else{
                return false;
            }

            return true;
        },
        //Change
        change_func : function(){
            "use strict";
            var i,
                inObj,
                outObj,
                left_wo_id,
                right_wo_id,
                left_mdl_id="",
                right_mdl_id="",
                to_left_cnt,
                to_right_cnt,
                iary_2left = [],
                iary_2right = [],
                prdAryMv = gVal.prdAryMV,
                has_box_id,
            	inObjWO,
            	outObjWO;
      	  	
            if(gVal.boxIdL && gVal.boxIdR){
                has_box_id = 'Y';
            }else{
                has_box_id = 'N';
            }

            left_wo_id = gVal.woIdL;
            right_wo_id = gVal.woIdR;
            for(i = prdAryMv.length - 1; i >= 0; i--){
                if(prdAryMv[i]['wo_id_fk'] === left_wo_id){
                    iary_2left.push({
                        prd_seq_id : prdAryMv[i].prd_seq_id,
                        box_id_fk  : prdAryMv[i].box_id_fk,
                        wo_id_fk   : prdAryMv[i].wo_id_fk,
                        prd_stat   : prdAryMv[i].prd_stat
                    });
                }else{
                    iary_2right.push({
                        prd_seq_id : prdAryMv[i].prd_seq_id,
                        box_id_fk  : prdAryMv[i].box_id_fk,
                        wo_id_fk   : prdAryMv[i].wo_id_fk,
                        prd_stat   : prdAryMv[i].prd_stat
                    });
                }
            }

            to_left_cnt = iary_2left.length;
            to_right_cnt = iary_2right.length;

            inObj = {
                trx_id       : VAL.T_XPWOCHG ,
                action_flg   : "C",
                left_wo_id   : left_wo_id,
                right_wo_id  : right_wo_id,
                has_box_id   : has_box_id,
                evt_user     : VAL.EVT_USER,

                to_left_cnt  : to_left_cnt,
                to_right_cnt : to_right_cnt,
                iary_2left   : iary_2left,
                iary_2right  : iary_2right
            };
            if(gVal.boxIdL){
                inObj.left_box_id = gVal.boxIdL;
            }
            if(gVal.boxIdR){
                inObj.right_box_id = gVal.boxIdR;
            }
            
	        inObjWO = {
		            trx_id      : VAL.T_XPAPLYWO,
		            action_flg  : 'Q'           ,
		            iary        : { wo_id : left_wo_id }
		    };
		
	        outObjWO = comTrxSubSendPostJson(inObjWO);
	        if(outObjWO.rtn_code == VAL.NORMAL) {
	        	left_mdl_id = outObjWO.oary.mdl_id;
	        }
	        
	        inObjWO = {
		            trx_id      : VAL.T_XPAPLYWO,
		            action_flg  : 'Q'           ,
		            iary        : { wo_id : right_wo_id }
		    };
		
	        outObjWO = comTrxSubSendPostJson(inObjWO);
	        if(outObjWO.rtn_code == VAL.NORMAL) {
	        	right_mdl_id = outObjWO.oary.mdl_id;
	        }
	        
	        if (left_mdl_id != right_mdl_id) {
	        	btnQuery.change_btn.showCallBackWarnningDialog({
					errMsg : "工单: ["+left_wo_id+"]产品型号:["+left_mdl_id+"]与工单:["+right_wo_id+"]产品型号:["+right_mdl_id+"]不一致，是否变更?",
					callbackFn : function(data) {
						if (data.result == true) {
				            outObj = comTrxSubSendPostJson(inObj);
				            if (outObj.rtn_code == VAL.NORMAL) {
				                showSuccessDialog('交换内部订单成功！');
				                gVal.prdAryMV = [];
				                btnFunc.query1_func();
				                return true;
				            }else {
				            	btnFunc.query1_func();
				            }
				        }else {
			            	btnFunc.query1_func();
				        	return false;
				        }						
					}	        		
	        	});
	        }else{
	            outObj = comTrxSubSendPostJson(inObj);
	            if (outObj.rtn_code == VAL.NORMAL) {
	                showSuccessDialog('交换内部订单成功！');
	                gVal.prdAryMV = [];
	                btnFunc.query1_func();
	                return true;
	            }else {
	            	btnFunc.query1_func();
	            }
	        }
        },
        //Left to right
        l2rMv_func : function(){
            "use strict";
            var grdL,
                grdR,
                selRowIdL;

            grdL = controlsQuery.mainGrdL.grdId;
            grdR = controlsQuery.mainGrdR.grdId;

            selRowIdL = grdL.jqGrid('getGridParam','selarrrow');
            if(selRowIdL.length <= 0){
                showErrorDialog("","请选择左侧的产品ID！");
                return false;
            }

            toolFunc.mvPrd(grdL, grdR, gVal.prdAryL, gVal.prdAryR, selRowIdL, gVal.boxIdR, gVal.woIdR);

            return true;
        },
        //Right to left
        r2lMv_func : function(){
            "use strict";
            var grdL,
                grdR,
                selRowIdR;

            grdL = controlsQuery.mainGrdL.grdId;
            grdR = controlsQuery.mainGrdR.grdId;

            selRowIdR = grdR.jqGrid('getGridParam','selarrrow');
            if(selRowIdR.length <= 0){
                showErrorDialog("","请选择右侧的产品ID！");
                return false;
            }

            toolFunc.mvPrd(grdR, grdL, gVal.prdAryR, gVal.prdAryL, selRowIdR, gVal.boxIdL, gVal.woIdL);

            return true;
        }
    };

    /**
     * Bind button click action
     */
    var iniButtonAction = function(){
        btnQuery.query_btn.click(function(){
            btnFunc.query_func();
        });
        btnQuery.change_btn.click(function(){
            btnFunc.change_func();
        });
        btnQuery.chgBtnL2R.click(function(){
            btnFunc.l2rMv_func();
        });
        btnQuery.chgBtnR2L.click(function(){
            btnFunc.r2lMv_func();
        });
    };

    /**
     * grid  initialization
     */
    var iniGridInfo = function(){
        var grdInfoCM = [
            {name: 'prd_seq_id', index: 'prd_seq_id', label: PRD_SEQ_ID_TAG  ,   width: PRD_SEQ_ID_CLM ,hidden:true},
            {name: 'fab_sn'    , index: 'fab_sn'    , label: PRD_SEQ_ID_TAG  ,   width: PRD_SEQ_ID_CLM},
            {name: 'box_id_fk',  index: 'box_id_fk',  label: BOX_ID_TAG  ,       width: BOX_ID_CLM},
//          {name: 'slot_no',    index: 'slot_no',    label: PPBOX_SLOT_NO_TAG,  width: 55},
            {name: 'wo_id_fk',   index: 'wo_id_fk',   label: WO_ID_TAG,          width: WO_ID_CLM},
            {name: 'prd_stat',   index: 'prd_stat',   label: STATUS_TAG ,        width: PRD_STAT_CLM},
//            {name: 'change_wo',  index: 'change_wo',  label: 'change_wo' ,         width: 10}
            {name: 'change_wo',  index: 'change_wo',  label: '' ,  width: 1,     hidden: true}
        ];
        controlsQuery.mainGrdL.grdId.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:400,
            width:380,
            shrinkToFit:false,
            scroll:true,
            rownumWidth : 25,
            resizable : true,
            rowNum:40,
            loadonce:true,
            fixed:true,
            viewrecords:true,
            rownumbers : true,
            sortable:false,
            multiselect : true,
            cmTemplate: {sortable:false},
            pager : controlsQuery.mainGrdL.grdPgText,
            colModel: grdInfoCM
        });
        controlsQuery.mainGrdR.grdId.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:400,
            width:380,
            shrinkToFit:false,
            scroll:true,
            rownumWidth : 25,
            resizable : true,
            rowNum:40,
            loadonce:true,
            fixed:true,
            viewrecords:true,
            rownumbers : true,
            sortable:false,
            multiselect : true,
            cmTemplate: {sortable:false},
            pager : controlsQuery.mainGrdR.grdPgText,
            colModel: grdInfoCM
        });
    };

    /**
     * Other action bind
     */
    var otherActionBind = function(){
        // Reset size of jqgrid when window resize
        controlsQuery.W.resize(function(){
            toolFunc.resetJqgrid();
        });

        //Stop from auto commit
        $("form").submit(function(){
            return false;
        });

        //Ope select change
        controlsQuery.opeSelect.change(function(){
            var ope_info,
            ope_info_ary;

	        ope_info = $.trim(controlsQuery.opeSelect.val());
	
	        if(!ope_info){
	            return false;
	        }
	
	        ope_info_ary = ope_info.split("@");
	        gSwhOpe = ope_info_ary[0];
	        controlsQuery.opeR.val(controlsQuery.opeSelect.find("option:selected").text());
        });

        controlsQuery.ppBoxIDInputL.keydown(function(event){
            if(event.keyCode === 13){
                btnFunc.query_func();
            }
        });
        controlsQuery.ppBoxIDInputR.keydown(function(event){
            if(event.keyCode === 13){
                btnFunc.query_func();
            }
        });
    };

    /**
     * Ini contorl's data
     */
    var iniContorlData = function(){
        gVal.boxIdL = "";
        gVal.boxIdR = "";
        gVal.prdAryL = [];
        gVal.prdAryR = [];

        toolFunc.clearInput();
        toolFunc.iniWoIdSelect();
        toolFunc.iniOpeSelect();
    };

    /**
     * Ini view, data and action bind
     */
    var initializationFunc = function(){
        iniGridInfo();
        iniButtonAction();
        otherActionBind();

        toolFunc.resetJqgrid();

        iniContorlData();
    };

    initializationFunc();
    function resizeFnc(){  
    	comResize(controlsQuery.W,controlsQuery.mainGrdL.fatherDiv,controlsQuery.mainGrdL.grdId);
    	comResize(controlsQuery.W,controlsQuery.mainGrdR.fatherDiv,controlsQuery.mainGrdR.grdId);
    	var offsetBottom, divWidth;                                                              
        
		divWidth = controlsQuery.mainGrdL.fatherDiv.width();                                   
		offsetBottom = controlsQuery.W.height() - controlsQuery.mainGrdL.fatherDiv.offset().top;
		controlsQuery.mainGrdL.fatherDiv.height(offsetBottom * 0.95);                          
		controlsQuery.mainGrdL.grdId.setGridWidth(divWidth * 1.0);                   
		controlsQuery.mainGrdL.grdId.setGridHeight(offsetBottom * 0.99 - 51); 
		
		divWidth = controlsQuery.mainGrdR.fatherDiv.width();                                   
		offsetBottom = controlsQuery.W.height() - controlsQuery.mainGrdR.fatherDiv.offset().top;
		controlsQuery.mainGrdR.fatherDiv.height(offsetBottom * 0.95);                          
		controlsQuery.mainGrdR.grdId.setGridWidth(divWidth * 1.0);                   
		controlsQuery.mainGrdR.grdId.setGridHeight(offsetBottom * 0.99 - 51);
    	controlsQuery.lrBtnDiv.offset({
    		top:controlsQuery.mainGrdL.fatherDiv.height()/2 + controlsQuery.mainGrdL.fatherDiv.offset().top,
//    		left:(controlsQuery.mainGrdR.fatherDiv.offset().left - controlsQuery.lrBtnDiv.offset().left)/2 + controlsQuery.lrBtnDiv.offset().left
    		left:controlsQuery.W.width()/2-20
    	});
    };                                                                                         
    resizeFnc();                                                                               
    controlsQuery.W.resize(function() {                                                         
    	resizeFnc();                                                                             
	});       
});