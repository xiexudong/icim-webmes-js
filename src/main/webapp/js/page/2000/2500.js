$(document).ready(function() {
  $("form").submit(function(){
      return false;
  });
  var domObj = {
			$window 		 : $(window),
			$pnlInfoListDiv1 : $("#pnlInfoListDiv1"),
			$pnlInfoListGrd  : $("#pnlInfoListGrd"),
			$rwkPathSel      : $("#rwkPathSel"),
			$rwkPathFlg      : $("#rwkPathFlg"),
			$f4              : $("#f4_rwk_btn"),
			$skipOpeIdSel    : $("#skipOpeIdSel"),
			$skip_ope_tag    : $("#skip_ope_tag")
  };
  var VAL = {
			T_XPBISOPE  : "XPBISOPE",
	        T_XPINQCOD  : "XPINQCOD" ,
			T_XPINQBOX  : "XPINQBOX",
			T_XPSKIPOPE : "XPSKIPOPE",
			T_XPRWKPATH	: "XPRWKPATH",
	        EVT_USER  	: $("#userId").text(),
			NORMAL		: "0000000"
		};
  
  var globalBean = {
			opeObjs : {},
			getOpeDsc : function(opeID, opeVer) {
				var i, oary;
				for (i = 0; i < this.opeObjs.length; i++) {
					oary = this.opeObjs[i];
					if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
						return oary.ope_dsc;
					}
				}
			}
		};
  var gSwhOpe = "";

  /**
   * All tool functions
   * @type {Object}
   */
  var toolFunc = {
	  setOpeSelectDate : function(dataCnt, arr, selVal, selVal2, selVal3,selVal4,selVal5, queryObj){
	      var i, realCnt,oary;
	
	      queryObj.empty();
          queryObj.append("<option value=''></option>");
	      realCnt = parseInt( dataCnt, 10);
          for( i = 0; i < realCnt; i++ ){
          	oary = realCnt > 1 ? arr[i] : arr;
          	queryObj.append("<option value="+ oary[selVal] + "@" + oary[selVal2] + "@" + oary[selVal3]+ "@" + oary[selVal4] +">"+ oary[selVal5] +"</option>");
          };
          queryObj.select2({
  	    	theme : "bootstrap"
  	    });
	  },
	  setOpebyPathSelectDate : function(dataCnt, arr, selVal, selVal2, selVal3,selVal4,selVal5,selVal6, queryObj){
	      var i, realCnt,oary;
	
	      queryObj.empty();
          queryObj.append("<option value=''></option>");
	      realCnt = parseInt( dataCnt, 10);
          for( i = 0; i < realCnt; i++ ){
          	oary = realCnt > 1 ? arr[i] : arr;
          	queryObj.append("<option value="+ oary[selVal] + "@" + oary[selVal2] + "@" + oary[selVal3]+ "@" + oary[selVal4] + "@" + oary[selVal6] +">"+ oary[selVal5] +"</option>");
          };
          queryObj.select2({
  	    	theme : "bootstrap"
  	    });
	  },
	  setRwkPathData : function (dataCnt, arr, selVal, selVal2, selVal3, selVal4, queryObj){
		  var i, realCnt,oary;
			
	      queryObj.empty();
          queryObj.append("<option value=''></option>");
	      realCnt = parseInt( dataCnt, 10);
          for( i = 0; i < realCnt; i++ ){
          	oary = realCnt > 1 ? arr[i] : arr;
          	queryObj.append("<option value="+ oary[selVal] + "@" + oary[selVal2] + "@"+oary[selVal4]+">"+ oary[selVal3] +"</option>");
          };
          queryObj.select2({
  	    	theme : "bootstrap"
  	    });
	  },
	  queryRwkPathFnc : function (){
		  var inObj,outObj;
	        inObj = {
	    	  trx_id     : "XPBISPTH",
	          action_flg : "S"
	        };
	        outObj = comTrxSubSendPostJson(inObj);
	        if(outObj.rtn_code === VAL.NORMAL){
	        	toolFunc.setRwkPathData(outObj.tbl_cnt_a, outObj.oaryA, "path_id", "path_ver", "path_dsc","first_ope_no",domObj.$rwkPathSel);
	        }
	  },
	  queryBoxInfo : function(){
		  var box_id,
			  opeID,
			  opeVer,
			  opeDsc,
			  inTrx,
			  outTrx;
		  box_id = $("#boxIDTxt").val();
		  if(box_id==""){
		    showErrorDialog("003","请输入箱号");
		    return false;
		  }
		  inTrx = {
		    trx_id     : VAL.T_XPINQBOX,
		    action_flg : "S"    ,
		    box_id     : box_id
		  };
		  outTrx = comTrxSubSendPostJson(inTrx);
		  if(outTrx.rtn_code==VAL.NORMAL){
		    $("#boxID2Txt").val(outTrx.box_id);
		    $("#boxStatTxt").val(outTrx.box_stat);
		    if(outTrx.box_stat != "WAIT"){
			    showErrorDialog("", "箱子状态必须为WAIT才能跳站！");
			    disPlay();
			    return false; 
		    }
    		
		    $("#pathIdTxt").val(outTrx.nx_path_id_fk);
		    $("#pathVerTxt").val(outTrx.nx_path_ver_fk);
		    $("#oLotTxt").val(outTrx.lot_id_fk);
		    opeID  = outTrx.nx_ope_id_fk;
		    opeVer = outTrx.nx_ope_ver_fk;
		    opeDsc = globalBean.getOpeDsc(opeID, opeVer);
		    $("#crOpeDscTxt").val(opeDsc);
		    $("#nLotTxt").val("");
		    $("#skipOpeIdSel").empty();
		    toolFunc.iniOpeSelect();
		    if(outTrx.table.length>1){
		       outTrx.table.sort(function(a,b){return a.slot_no-b.slot_no;});
		    }
		    setGridInfo(outTrx.table,"#pnlInfoListGrd",true);
//		    /**
//		     * 检查用户是否有权限操作这个箱子.
//		     * @author CMJ
//		     */
//		    for(var i=0;i<OpeoutObj.tbl_cnt;i++){
//		    	var oary = OpeoutObj.tbl_cnt>1 ? OpeoutObj.oary[i] : OpeoutObj.oary;
//		    	if($.trim(opeID) == oary.ope_id && 
//		    	   $.trim(opeVer) == oary.ope_ver){
//		    		$("#pathIdTxt").val(outTrx.nx_path_id_fk);
//				    $("#pathVerTxt").val(outTrx.nx_path_ver_fk);
//				    $("#crOpeDscTxt").val(opeDsc);
//				    setGridInfo(outTrx.table,"#pnlInfoListGrd",true);
//				    break;
//		    	}
//		    }
//		    if(OpeoutObj.tbl_cnt ==0 || i>=OpeoutObj.tbl_cnt){
//		    	opeDsc = opeDsc ? opeDsc : opeID;
//		    	showErrorDialog("", "此用户没有权限操作站点["+opeDsc+"]的箱子！");
//			    disPlay();
//			    return false; 
//		    }
		  }else{
			  disPlay();
		  }
	  },
	  iniOpeSelect : function(){
		  var inObj, outObj;
		  inObj = {
		      trx_id      : VAL.T_XPBISOPE,
		      action_flg  : 'L'
		  };
		  outObj = comTrxSubSendPostJson(inObj);
		  if (outObj.rtn_code == VAL.NORMAL) {
			  globalBean.opeObjs = outObj.oary;
		      toolFunc.setOpeSelectDate(outObj.tbl_cnt, outObj.oary, "ope_id", "ope_ver", "proc_id","toolg_id", "ope_dsc", $("#skipOpeIdSel"));
		  }
      },
	  iniOpeSelectByPath : function(){
          var inObj, outObj,pathId,pathVer;
          pathId = $("#pathIdTxt").val();
          pathVer = $("#pathVerTxt").val();
          if(pathId == "" || pathVer == ""){
        	  $("#skipOpeIdSel").empty();
        	  return;
          }
          inObj = {
              trx_id      : VAL.T_XPBISOPE,
              action_flg  : 'P',
              path_id_fk  : pathId,
              path_ver_fk : pathVer
            	  
          };
          outObj = comTrxSubSendPostJson(inObj);
          if (outObj.rtn_code == VAL.NORMAL) {
              toolFunc.setOpebyPathSelectDate(outObj.tbl_cnt, outObj.oary, "ope_id", "ope_ver", "proc_id","toolg_id", "ope_dsc", "ope_no",$("#skipOpeIdSel"));
          }else{
        	  $("#skipOpeIdSel").empty();
          }
      },
      
      skipOpeFnc : function(){
          var inObj,
              outObj,
              boxId,
              pathId,
              pathVer,
              skipToOpe,
              skipToOpeVer,
              skipToOpeNo,
              procId,
              toolgId,
              ope_dsc,
              ope_info, ope_info_ary;


	 	 if (domObj.$rwkPathFlg[0].checked) {
	   		  showErrorDialog("", "跳站时候重工路线选择标识不可勾选");
	          return false;
          }
		  if($("#boxIDTxt").val()==""){
		    showErrorDialog("003","请输入箱号");
      	    $("#skipOpeIdSel").empty();
		    return false;
		  }
          boxId = $("#boxID2Txt").val();
          pathId = $("#pathIdTxt").val();
          pathVer = $("#pathVerTxt").val();
          ope_info = $.trim($("#skipOpeIdSel").val());
          ope_info_ary = ope_info.split("@");
          skipToOpe = ope_info_ary[0];
          skipToOpeVer = ope_info_ary[1];
          procId = ope_info_ary[2];
          toolgId = ope_info_ary[3];
          skipToOpeNo = ope_info_ary[4];
          ope_dsc = $.trim($("#skipOpeIdSel").find("option:selected").text());
          if(pathId == "" || pathVer == ""){
        	  $("#skipOpeIdSel").empty();
        	  return;
          }
          if(!toolgId || toolgId ==="undefined" || $("#boxIDTxt").val()!== $("#boxID2Txt").val()){
        	  showErrorDialog("","请查询后获取箱号信息后再跳站");
        	  return false;
          }
          if(skipToOpe == ""){
        	  return;
          }
          if(skipToOpeNo == ""){
        	  return;
          }
//          olotid = $("#oLotTxt").val();
          nlotid = $("#nLotTxt").val();
//          if(ope_dsc == $.trim($("#crOpeDscTxt").val())&&olotid==nlotid){
//        	  return;
//          }
//          if(ope_dsc == $.trim($("#crOpeDscTxt").val())&&nlotid==""){
//        	  return;
//          }
          if(nlotid!=""){
        	  return;
          }

          inObj = {
              trx_id          : VAL.T_XPSKIPOPE,
              box_id          : boxId,
              path_id_fk      : pathId,
              path_ver_fk     : pathVer,
              skip_to_ope     : skipToOpe,
              skip_to_ope_no  : skipToOpeNo,
              skip_to_ope_ver : skipToOpeVer,
              proc_id         : procId,
              toolg_id        : toolgId,
              evt_usr         : VAL.EVT_USER
          };
          outObj = comTrxSubSendPostJson(inObj);
          if (outObj.rtn_code == VAL.NORMAL) {
        	  showSuccessDialog("调整成功！");
	    	  disPlay();
//        	  f1QueryFnc();
          }
      },
      
      lotChgFnc : function(){
          var inObj,
              outObj,
              boxId,
              pathId,
              pathVer,
              skipToOpe,
              skipToOpeVer,
              procId,
              toolgId,
              ope_dsc,
              ope_info, ope_info_ary;

		  if($("#boxIDTxt").val()==""){
		    showErrorDialog("003","请输入箱号");
      	    $("#skipOpeIdSel").empty();
		    return false;
		  }
          boxId = $("#boxID2Txt").val();
          pathId = $("#pathIdTxt").val();
          pathVer = $("#pathVerTxt").val();
          ope_info = $.trim($("#skipOpeIdSel").val());
          ope_info_ary = ope_info.split("@");
          skipToOpe = ope_info_ary[0];
//          skipToOpeVer = ope_info_ary[1];
//          procId = ope_info_ary[2];
//          toolgId = ope_info_ary[3];
          //ope_dsc = $.trim($("#skipOpeIdSel").find("option:selected").text());
//          if(pathId == "" || pathVer == ""){
//        	  $("#skipOpeIdSel").empty();
//        	  return;
//          }
          if(skipToOpe != ""){
        	  return;
          }
          olotid = $("#oLotTxt").val();
          nlotid = $("#nLotTxt").val();
          if(nlotid==""){
        	  return;
          }
//          if(ope_dsc == $.trim($("#crOpeDscTxt").val())&&olotid==nlotid){
//        	  return;
//          }
//          if(ope_dsc == $.trim($("#crOpeDscTxt").val())&&nlotid==""){
//        	  return;
//          }

          inObj = {
              trx_id          : VAL.T_XPSKIPOPE,
              box_id          : boxId,
              evt_usr         : VAL.EVT_USER,
              olotid          : olotid,
              nlotid          : nlotid
          };
          outObj = comTrxSubSendPostJson(inObj);
          if (outObj.rtn_code == VAL.NORMAL) {
        	  showSuccessDialog("调整成功！");
        	  f1QueryFnc();
          }
      },
      
      rwkFnc : function (){
    	  var pathVal,pathArr,path_id,path_ver,first_ope_no,out_ope_no_info,out_ope_no_arr,out_ope_no_fk,out_ope_id_fk;
    	  
    	  pathVal = $.trim(domObj.$rwkPathSel.val());
    	  
    	  if(!pathVal){
    		  showErrorDialog("", "请选择重工线路");
              return false;
          }
    	  pathArr = pathVal.split("@");
    	  
    	  path_id = pathArr[0];
    	  path_ver = pathArr[1];
    	  first_ope_no = pathArr[2];
    	  
    	  if (path_id === "" || path_ver === "" || first_ope_no === "") {
    		  showErrorDialog("", "重工线路维护有错误，请检查");
              return false;
    	  }
    	  
    	  out_ope_no_info = $.trim(domObj.$skipOpeIdSel.val());
    	  if (!out_ope_no_info){
    		  showErrorDialog("", "请选择返回站点");
              return false;
    	  }
    	  
    	  out_ope_no_arr = out_ope_no_info.split("@");
    	  out_ope_no_fk = out_ope_no_arr[4];
    	  out_ope_id_fk = out_ope_no_arr[0];
    	  
    	  var inObj,
	          outObj,
	          boxId;
	
		  if($("#boxIDTxt").val()==""){
			  showErrorDialog("003","请输入箱号");
		  	  $("#skipOpeIdSel").empty();
			  return false;
		  }
	      boxId = $("#boxID2Txt").val();
	
	      inObj = {
	          trx_id          : VAL.T_XPRWKPATH,
	          action_flg      : "S",
	          box_id          : boxId,
	          evt_usr         : VAL.EVT_USER,
	          path_id         : path_id,
	          path_ver        : path_ver,
	          first_ope_no    : first_ope_no,
	          out_ope_no_fk   : out_ope_no_fk,
	          out_ope_id_fk   : out_ope_id_fk
	      };
	      
	      
	      outObj = comTrxSubSendPostJson(inObj);
	      if (outObj.rtn_code == VAL.NORMAL) {
	    	  showSuccessDialog("重工线路成功！");
	    	  disPlay();
	    	  //f1QueryFnc();
	      }
    	  
    	  
      }
  };
  
  function disPlay(){
	  $("input").val("");
	  $("#boxIDTxt").focus();
	  $("#pnlInfoListGrd").jqGrid("clearGridData");
	  toolFunc.iniOpeSelect();
	 // toolFunc.iniUserOpeId();
  }
  
  var itemInfoCM = [
        {name: 'slot_no'       , index: 'slot_no'       , label: SLOT_NO_TAG,      width: SLOT_NO_CLM},
        {name: 'prd_seq_id'    , index: 'prd_seq_id'    , label: PRD_SEQ_ID_TAG,   width: PRD_SEQ_ID_CLM ,hidden : true},
        {name: 'fab_sn'        , index: 'fab_sn'        , label: PRD_SEQ_ID_TAG,   width: PRD_SEQ_ID_CLM},
        {name: 'lot_id'        , index: 'lot_id'        , label: LOT_ID_TAG,       width: LOT_ID_CLM},
        {name: 'wo_id_fk'      , index: 'wo_id_fk'      , label: WO_ID_TAG,        width: WO_ID_CLM},
        {name: 'nx_path_id_fk' , index: 'nx_path_id_fk' , label: PATH_ID_TAG,      width: PATH_ID_CLM},
        {name: 'nx_ope_no_fk'  , index: 'nx_ope_no_fk'  , label: CR_OPE_ID_TAG,    width: OPE_ID_CLM}
      ];
  
  $("#pnlInfoListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:400,//TODO:高度如何调整
      autowidth: true,
      shrinkToFit:false,
      autoScroll: true, 
      scroll:false,//如需要分页控件,必须将此项置为false
      loadonce:true,
      viewrecords : true, //显示总记录数
      emptyrecords :true ,
      colModel: itemInfoCM
  });
  
  var resetJqgrid = function(){
      $(window).unbind("onresize");      
      $(window).bind("onresize", this);  
  };
  
  function initFnc(){
  	resetJqgrid();
  }
  
  $(window).resize(function(){  
  	resetJqgrid();
  });
  
  function f1QueryFnc(){
	  toolFunc.queryBoxInfo();
      toolFunc.iniOpeSelectByPath();
  }
  
  function f2SkipOpeFnc(){
//		var func_code = "F_2500_SKIP_OPE";
//		if(checkUserFunc(func_code) == false){
//			showErrorDialog("","你没有权限跳站！");
//			return;
//		}
	  toolFunc.skipOpeFnc();
  }
  
  function f3LotChgFnc(){
//		var func_code = "F_2500_CHANGE_LOT";
//		if(checkUserFunc(func_code) == false){
//			showErrorDialog("","你没有权限调整批次！");
//			return;
//		}
	  toolFunc.lotChgFnc();
  }
  
  initFnc();
  disPlay();
  $("#f1_query_btn").click(f1QueryFnc);
  $("#f2_skip_btn").click(f2SkipOpeFnc);
  $("#f3_lotchg_btn").click(f3LotChgFnc);
 // $("#skipOpeIdSel").change(f2SkipOpeFnc);
  $("#boxIDTxt").keyup(function(e) {
	    $(this).val($(this).val().toUpperCase());
	    if (e.keyCode == 13) {
	      f1QueryFnc();
	    }
	  });
  domObj.$rwkPathFlg.click(function (){
	 if (domObj.$rwkPathFlg[0].checked) {
		toolFunc.queryRwkPathFnc();
		domObj.$rwkPathSel[0].disabled = false;
		domObj.$skip_ope_tag[0].textContent = "返回站点";
  	  }else {
		domObj.$rwkPathSel[0].disabled = true;
		domObj.$skip_ope_tag[0].textContent = "转入站点";
      }
  });
  domObj.$f4.click(function (){
	 toolFunc.rwkFnc(); 
  });
  $("#rwkPathSel").change(function(){
	  var pathVal, pathArr;
	  pathVal = $.trim(domObj.$rwkPathSel.val());
	  pathArr = pathVal.split("@");  
	  gSwhOpe = pathArr[2];	  
//	  showErrorDialog(gSwhOpe,"！");
  });
  $("#skipOpeIdSel").change(function(){
	  var ope_info, ope_info_ary;
      ope_info = $.trim($("#skipOpeIdSel").val());
      ope_info_ary = ope_info.split("@");
 	 if (!domObj.$rwkPathFlg[0].checked) {
 	      gSwhOpe = ope_info_ary[0];	  
 	 }
//	  showErrorDialog(gSwhOpe,"！");
  });
 
  function resizeFnc(){                                                                      
  		var offsetBottom, divWidth;                                                              

  		divWidth = domObj.$pnlInfoListDiv1.width();                                   
		offsetBottom = domObj.$window.height() - domObj.$pnlInfoListDiv1.offset().top;
		domObj.$pnlInfoListDiv1.height(offsetBottom * 0.95);                          
		domObj.$pnlInfoListGrd.setGridWidth(divWidth * 0.99);                   
		domObj.$pnlInfoListGrd.setGridHeight(offsetBottom * 0.99 - 51);               
  };                                                                                         
  resizeFnc();                                                                               
  domObj.$window.resize(function() {                                                         
  	resizeFnc();                                                                             
  });  
});