$(document).ready(function() {
    var VAL ={
        NORMAL      : "0000000"  ,
        EVT_USER    : $("#userId").text(),
        T_XPINQBOX  : "XPINQBOX",
        T_XPBOXCHG  : "XPBOXCHG",
        T_XPINQCOD  : "XPINQCOD",
        T_XPBISOPE  : "XPBISOPE"
    };
    var domObj = {
			$window : $(window),
			$boxDetailDivL : $("#boxDetailDivL"),
			$boxDetailGrdL : $("#boxDetailGrdL"),
			
			$boxDetailDivR : $("#boxDetailDivR"),
			$boxDetailGrdR : $("#boxDetailGrdR"),
			$grdDiv      : $("#grdDiv"),
			$lrBtnDiv      : $("#lrBtnDiv")
	};
    var gVal = {};
    gVal.boxIdL = ""; //Old box
    gVal.boxIdR = ""; //New box
    gVal.boxStatL = "";
    gVal.boxStatR = "";
    gVal.prdAryL = [];  //prds in left
    gVal.prdAryR = [];  //prds in right
    gVal.prdAryMV = [];  //最终被移动的玻璃

    /**
     * All controls's jquery object/text
     * @type {Object}
     */
    var controlsQuery = {
        W                 : $(window)             ,
        ppBoxIDInputL     : $("#ppBoxIDInputL")   ,
        ppBoxIDInputR     : $("#ppBoxIDInputR")   ,
        opeSelect         : $("#opeSelect")       ,
        mainGrdL   :{
            grdId     : $("#boxDetailGrdL")   ,
            grdPgText : "#boxDetailPgL"       ,
            fatherDiv : $("#boxDetailDivL")
        },
        mainGrdR   :{
            grdId     : $("#boxDetailGrdR")   ,
            grdPgText : "#boxDetailPgR"       ,
            fatherDiv : $("#boxDetailDivR")
        }
    };

    /**
     * All button's jquery object
     * @type {Object}
     */
    var btnQuery = {
        query_btn : $("#query_btn"),
        change_btn : $("#change_btn"),
        wh_change_btn : $("#wh_change_btn"),
        chgBtnL2R : $("#chgBtnL2R"),
        chgBtnR2L : $("#chgBtnR2L")
    };

    /**
     * All tool functions
     * @type {Object}
     */
    var toolFunc = {
        resetJqgrid :function(){
//            controlsQuery.W.unbind("onresize");
//            controlsQuery.mainGrdL.grdId.setGridWidth(controlsQuery.mainGrdL.fatherDiv.width()*0.90);
//            controlsQuery.mainGrdR.grdId.setGridWidth(controlsQuery.mainGrdR.fatherDiv.width()*0.90);
//            controlsQuery.W.bind("onresize", this);
        },
      clearInput : function(){
            controlsQuery.ppBoxIDInputL.val("");
            controlsQuery.ppBoxIDInputR.val("");
        },
        /**
         * 根据人员，厂别，站点的关系，筛选出可以使用的站点。By CMJ
         */
        iniOpeSelect : function(){
            var inObj, outObj;
            var user_id = VAL.EVT_USER;
            var iaryB = {
                user_id     : user_id
            };
            inObj = {
                trx_id      : VAL.T_XPBISOPE,
                action_flg  : 'S',
                iaryB       : iaryB
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                _setOpeSelectDate(outObj.tbl_cnt, outObj.oary, "ope_id", "ope_ver", "ope_dsc", controlsQuery.opeSelect);
            }
        },
        
        //批量执行
        mvPrd : function(srcGrdId, tarGrdId, scrGPrdAry, tarGPrdAry, mvRowIds, tarBoxId){
            "use strict";
            var mvRowIndex,
                scrGPrdAryIndex,
                tmpRowData,
                prdMv = gVal.prdAryMV;
            var slotNotInTar = function(id){
            	for(var j=0; j<tarGPrdAry.length; j++){
            		if(tarGPrdAry[j]['slot_no'] == id){
            			return false;
            		}else{
            			continue;
            		}
            	}
                return true;
            }
            for(mvRowIndex = mvRowIds.length - 1; mvRowIndex >= 0; mvRowIndex--){
                tmpRowData = srcGrdId.jqGrid('getRowData', mvRowIds[mvRowIndex]);
                for(scrGPrdAryIndex = scrGPrdAry.length - 1; scrGPrdAryIndex >= 0; scrGPrdAryIndex--){
                    if(tmpRowData['prd_seq_id'] === scrGPrdAry[scrGPrdAryIndex]['prd_seq_id']){
                        if(tarBoxId){
                            tmpRowData['box_id_fk'] = tarBoxId;
                        }
                        
//                        if('N' === tmpRowData['change_wo']){
//                            tmpRowData['change_wo'] = 'Y';
//                            prdMv.push(tmpRowData);
//                        }else if('Y' === tmpRowData['change_wo']){
//                            for(i = prdMv.length - 1; i >= 0; i--){
//                                if(prdMv[i]['prd_seq_id'] === tmpRowData['prd_seq_id']){
//                                    prdMv.splice(i, 1);
//                                    break;
//                                }
//                            }
//                            
//                        }
                        for(var i = 1;;i++){
                        	if(slotNotInTar(i)){
                        	   tmpRowData['slot_no'] = i;
                        	   break;                           		
                        	}
                        }
                        prdMv.push(tmpRowData);
                        tarGPrdAry.push(tmpRowData);
                        scrGPrdAry.splice(scrGPrdAryIndex, 1);
                        break;
                    }
                }
            }

            setGridInfo(scrGPrdAry, srcGrdId, true);
            setGridInfo(tarGPrdAry, tarGrdId, true);

            return true;
        },
        /**
         * 检查用户是否有权限操作这个箱子
         * @param outObjL
         * @returns {Boolean}
         * @author CMJ
         */
        checkAutr4Oper:function(outObjL,outObjR){
        	var autr =true;
        	var opeIndex,ope_id,ope_ver;
        	opeIndex = controlsQuery.opeSelect.val().indexOf("@");
            ope_id = controlsQuery.opeSelect.val().substr(0, opeIndex);
            ope_ver = controlsQuery.opeSelect.val().substr(opeIndex+1);
        	if(outObjL.box_stat != "WAIT" && outObjL.box_stat != "COMP" && outObjL.box_stat != "SHIP"){
        		showErrorDialog("","箱子["+outObjL.box_id+"]状态["+outObjL.box_stat+"]不符合条件，必须是等待，完成，在库！");
        		autr = false;
                return autr;
        	}
        	if(outObjL.box_stat == "WAIT"){
        		if($.trim(outObjL.nx_ope_id_fk) != $.trim(ope_id)){
        			showErrorDialog("","箱子["+outObjL.box_id+"]所在的站点["+outObjL.nx_ope_id_fk+"]与界面上所选站点["+ope_id+"]不符!");
        			autr = false;
                    return autr;
        		}
        		
        	}
        }
    };

    /**
     * All button click function
     * @type {Object}
     */
    var btnFunc = {
        //Query
        query_func : function(){
            var inObjL,
                inObjR,
                outObjL,
                outObjR,
                box_id_l,
                box_id_r,
                prdAryL = [],
                prdAryR = [],
                l, r,
                prd_qtyL = 0,
                prd_qtyR = 0,
                std_qtyL = 0,
                std_qtyR = 0;

            gVal.boxIdL = "";
            gVal.boxIdR = "";
            gVal.prdAryL.length = 0;
            gVal.prdAryR.length = 0;
            gVal.prdAryMV.length = 0;

            box_id_l = controlsQuery.ppBoxIDInputL.val().trim();
            box_id_r = controlsQuery.ppBoxIDInputR.val().trim();
            if(!box_id_l){
                showErrorDialog("","请输入左侧箱号！");
                return false;
            }
            if(!box_id_r){
                showErrorDialog("","请输入右侧箱号！");
                return false;
            }

            if(box_id_l === box_id_r){
                showErrorDialog("","箱号不能相同！");
                return false;
            }

            inObjL = {
                trx_id : VAL.T_XPINQBOX,
                action_flg : "S",
                box_id : box_id_l
            };
            outObjL = comTrxSubSendPostJson(inObjL);
            if (outObjL.rtn_code == VAL.NORMAL) {
                controlsQuery.mainGrdL.grdId.jqGrid("clearGridData");
                if(outObjL.hasOwnProperty("table")){
                    if(outObjL.table.length){
                        prdAryL = outObjL.table;
                    }else{
                        prdAryL[0] = outObjL.table;
                    }
                }
                gVal.boxStatL = outObjL.box_stat;

                std_qtyL = parseInt( outObjL.std_qty, 10);
                prd_qtyL = parseInt( outObjL.prd_qty, 10);

                for( l = 0; l < std_qtyL; l++ ){
                    controlsQuery.mainGrdL.grdId.jqGrid('addRowData', l, {});
                }
                for( l = 0; l < prd_qtyL; l++ ){
                    controlsQuery.mainGrdL.grdId.jqGrid('setRowData', l, {
                            prd_seq_id : prdAryL[l].prd_seq_id,
                            box_id_fk  : prdAryL[l].box_id_fk,
                            slot_no    : prdAryL[l].slot_no,
                            prd_stat   : prdAryL[l].prd_stat,
                            fab_sn     : prdAryL[l].fab_sn
                        });
                    gVal.prdAryL.push({
                        prd_seq_id : prdAryL[l].prd_seq_id,
                        box_id_fk  : prdAryL[l].box_id_fk,
                        slot_no    : prdAryL[l].slot_no,
                        prd_stat   : prdAryL[l].prd_stat,
                        fab_sn     : prdAryL[l].fab_sn
                    });
                }
                gVal.boxIdL = box_id_l;
            }else{
                return false;
            }

            inObjR = {
                trx_id : VAL.T_XPINQBOX,
                action_flg : "S",
                box_id : box_id_r
            };
            outObjR = comTrxSubSendPostJson(inObjR);
            if (outObjR.rtn_code == VAL.NORMAL) {
                controlsQuery.mainGrdR.grdId.jqGrid("clearGridData");
                if(outObjL.box_set_code != "JBIC" &&
                	outObjR.box_set_code != "JBIC" && 
                	outObjL.box_set_code != outObjR.box_set_code){
                	showErrorDialog("","箱号["+ box_id_l + "]的设定代码[" +
                        outObjL.box_set_code +"] VS 箱号["
                        + box_id_r + "]的设定代码[" + outObjR.box_set_code +"]");
                    return false;
                }
                /**
                 * 检查箱子是否在可以操作的站点
                 * @author CMJ
                 */
                var autr = true;
                if(outObjL.box_stat =="EMPT" || outObjR.box_stat == "EMPT"){
                	if(outObjL.box_stat =="EMPT"){
                		autr = toolFunc.checkAutr4Oper(outObjR,outObjL);
                		if(autr==false){
                			controlsQuery.mainGrdL.grdId.jqGrid("clearGridData");
                			return false;
                		}
                	}else{
                		autr =toolFunc.checkAutr4Oper(outObjL,outObjR);
                		if(autr==false){
                			controlsQuery.mainGrdL.grdId.jqGrid("clearGridData");
                			return false;
                		}
                	}
                }else{
                	if(outObjL.box_stat != outObjR.box_stat){
                    	showErrorDialog("","两个箱子状态不相同！");
                        return false;
                    }else if(outObjL.dest_shop != outObjR.dest_shop){
                    	if(outObjL.box_stat!='WAIT'&&outObjR.box_stat!='WAIT'){
                    	showErrorDialog("","左侧箱子仓位["+outObjL.dest_shop+"]与右侧箱子仓位["+outObjR.dest_shop+"]不相同！");
                        return false;
                    	}
                    }
                	else{
                    	autr =toolFunc.checkAutr4Oper(outObjL,outObjR);
                    	if(autr==false){
                    		controlsQuery.mainGrdL.grdId.jqGrid("clearGridData");
                			return false;
                		}
                    	autr =toolFunc.checkAutr4Oper(outObjR,outObjL);
                    	if(autr==false){
                    		controlsQuery.mainGrdL.grdId.jqGrid("clearGridData");
                			return false;
                		}
                    }
                }
                //<--End
                if(outObjR.hasOwnProperty("table")){
                    if(outObjR.table.length){
                        prdAryR = outObjR.table;
                    }else{
                        prdAryR[0] = outObjR.table;
                    }
                }

                gVal.boxStatR = outObjR.box_stat;
                std_qtyR = parseInt( outObjR.std_qty, 10);
                prd_qtyR = parseInt( outObjR.prd_qty, 10);

                for( r = 0; r < std_qtyR; r++ ){
                    controlsQuery.mainGrdR.grdId.jqGrid('addRowData', r, {});
                }
                for( r = 0; r < prd_qtyR; r++ ){
                    controlsQuery.mainGrdR.grdId.jqGrid('setRowData', r, {
                            prd_seq_id : prdAryR[r].prd_seq_id,
                            box_id_fk  : prdAryR[r].box_id_fk,
                            slot_no    : prdAryR[r].slot_no,
                            prd_stat   : prdAryR[r].prd_stat,
                            fab_sn     : prdAryR[r].fab_sn
                     });
                    gVal.prdAryR.push({
                        prd_seq_id : prdAryR[r].prd_seq_id,
                        box_id_fk  : prdAryR[r].box_id_fk,
                        slot_no    : prdAryR[r].slot_no,
                        prd_stat   : prdAryR[r].prd_stat,
                        fab_sn     : prdAryR[r].fab_sn
                    });
                }
                gVal.boxIdR = box_id_r;
            }else{
                return false;
            }

            return true;
        },
        //Change
        /**
         * 交换
         * @param isWh 是否为在库品交换
         */
        change_func : function(isWh){
            var inObj,
                outObj,
                grdL,
                grdR,
                rowIdsL,
                rowIdsR,
                rowData,
                new_box_cnt = 0 ,
                old_box_cnt = 0 ,
                iary_new = [] ,
                iary_old = [] ;
            grdL = controlsQuery.mainGrdL.grdId;
            grdR = controlsQuery.mainGrdR.grdId;
            
            //Left
            rowIdsL = grdL.jqGrid('getDataIDs');
            $.each(rowIdsL, function(i,val){
                rowData = grdL.jqGrid('getRowData', val);
                if("" !== rowData["prd_seq_id"]){
                    iary_old.push({
                        prd_seq_id : rowData["prd_seq_id"],
                        slot_no : rowData["slot_no"]
                    });
                    old_box_cnt++;
                }
            });

            //Right
            rowIdsR = grdR.jqGrid('getDataIDs');
            $.each(rowIdsR, function(i,val){
                rowData = grdR.jqGrid('getRowData', val);
                if("" !== rowData["prd_seq_id"]){
                    iary_new.push({
                        prd_seq_id : rowData["prd_seq_id"],
                        slot_no : rowData["slot_no"]
                    });
                    new_box_cnt++;
                }
            });

            inObj = {
                trx_id : VAL.T_XPBOXCHG ,
                action_flg  : "C",
                new_box_id  : gVal.boxIdR,
                old_box_id  : gVal.boxIdL,
                new_box_cnt : new_box_cnt,
                old_box_cnt : old_box_cnt,
                evt_user    : VAL.EVT_USER,
                remark      : "",
                iary_new    : iary_new,
                iary_old    : iary_old
            };
            if(gVal.boxStatL === "SHIP"){
                inObj.in_bank = 'Y';
            }
            if(gVal.boxStatR === "SHIP"){
                inObj.in_bank = 'Y';
            }            
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                showSuccessDialog("交换成功");
                controlsQuery.ppBoxIDInputL.val(gVal.boxIdL);
                controlsQuery.ppBoxIDInputR.val(gVal.boxIdR);
                btnFunc.query_func();
            }
        },
        //Left to right
        l2rMv_func : function(){
            "use strict";
            var grdL,
                grdR,
                selRowIdL;

            grdL = controlsQuery.mainGrdL.grdId;
            grdR = controlsQuery.mainGrdR.grdId;

            selRowIdL = grdL.jqGrid('getGridParam','selarrrow');
            if(selRowIdL.length <= 0){
                showErrorDialog("","请选择左侧的产品ID！");
                return false;
            }

            toolFunc.mvPrd(grdL, grdR, gVal.prdAryL, gVal.prdAryR, selRowIdL, gVal.boxIdR);

            return true;
        },
        //Right to left
        r2lMv_func : function(){
            "use strict";
            var grdL,
                grdR,
                selRowIdR;

            grdL = controlsQuery.mainGrdL.grdId;
            grdR = controlsQuery.mainGrdR.grdId;

            selRowIdR = grdR.jqGrid('getGridParam','selarrrow');
            if(selRowIdR.length <= 0){
                showErrorDialog("","请选择右侧的产品ID！");
                return false;
            }

            toolFunc.mvPrd(grdR, grdL, gVal.prdAryR, gVal.prdAryL, selRowIdR, gVal.boxIdL);

            return true;
        }
//        //Left to right
//        l2rMv_func : function(){
//            var grdL,
//                grdR,
//                selRowDataL,
//                selRowIdL,
//                selRowDataR,
//                selRowIdR,
//                rowIdsL,
//                rowIdsR;
//
//            grdL = controlsQuery.mainGrdL.grdId;
//            grdR = controlsQuery.mainGrdR.grdId;
//
//            rowIdsL = grdL.jqGrid('getDataIDs');
//            rowIdsR = grdR.jqGrid('getDataIDs');
//            if(rowIdsL.length === 0 ||
//                rowIdsR.length === 0 ){
//                showErrorDialog("","请先输入箱号查询！");
//                return false;
//            }
//
//            selRowIdL = grdL.jqGrid('getGridParam','selrow');
//            if(!selRowIdL){
//                showErrorDialog("","请选择左侧的产品ID！");
//                return false;
//            }
//
//            selRowIdR = grdR.jqGrid('getGridParam','selrow');
//            if(!selRowIdR){
//                showErrorDialog("","请选择右侧空行！");
//                return false;
//            }
//
//            selRowDataL = grdL.jqGrid('getRowData', selRowIdL);
//            if(!selRowDataL.prd_seq_id){
//                showErrorDialog("","请选择左侧的产品ID！");
//                return false;
//            }
//            selRowDataR = grdR.jqGrid('getRowData', selRowIdR);
//            if(selRowDataR.prd_seq_id){
//                showErrorDialog("","右侧此子格已经有产品的产品ID！");
//                return false;
//            }
//
//            grdR.jqGrid('setRowData', selRowIdR,{
//                prd_seq_id : selRowDataL.prd_seq_id,
//                box_id_fk  : gVal.boxIdR,
//                slot_no    : selRowDataL.slot_no,
//                prd_stat   : selRowDataL.prd_stat
//            });
//            grdL.jqGrid('setRowData', selRowIdL, {
//                prd_seq_id : "",
//                box_id_fk  : "",
//                slot_no    : "",
//                prd_stat   : ""
//            });
//        },
//        //Right to left
//        r2lMv_func : function(){
//            var grdL,
//                grdR,
//                selRowDataL,
//                selRowIdL,
//                selRowDataR,
//                selRowIdR,
//                rowIdsL,
//                rowIdsR;
//
//            grdL = controlsQuery.mainGrdL.grdId;
//            grdR = controlsQuery.mainGrdR.grdId;
//
//            rowIdsL = grdL.jqGrid('getDataIDs');
//            rowIdsR = grdR.jqGrid('getDataIDs');
//            if(rowIdsL.length === 0 ||
//                rowIdsR.length === 0 ){
//                showErrorDialog("","请先输入箱号查询！");
//                return false;
//            }
//
//            selRowIdR = grdR.jqGrid('getGridParam','selrow');
//            if(!selRowIdR){
//                showErrorDialog("","请选择右侧的产品ID！");
//                return false;
//            }
//
//            selRowIdL = grdL.jqGrid('getGridParam','selrow');
//            if(!selRowIdL){
//                showErrorDialog("","请选择左侧空行！");
//                return false;
//            }
//
//            selRowDataR = grdR.jqGrid('getRowData', selRowIdR);
//            if(!selRowDataR.prd_seq_id){
//                showErrorDialog("","请选择右侧的产品ID！");
//                return false;
//            }
//            selRowDataL = grdL.jqGrid('getRowData', selRowIdL);
//            if(selRowDataL.prd_seq_id){
//                showErrorDialog("","左侧此子格已经有产品的产品ID！");
//                return false;
//            }
//
//            grdL.jqGrid('setRowData', selRowIdL,{
//                prd_seq_id : selRowDataR.prd_seq_id,
//                box_id_fk  : gVal.boxIdL,
//                slot_no    : selRowDataR.slot_no ,
//                prd_stat   : selRowDataR.prd_stat
//            });
//            grdR.jqGrid('setRowData', selRowIdR, {
//                prd_seq_id : "",
//                box_id_fk  : "",
//                slot_no    : "",
//                prd_stat   : ""
//            });
//        }
    };

    /**
     * Bind button click action
     */
    var iniButtonAction = function(){
        btnQuery.query_btn.click(function(){
            btnFunc.query_func();
        });
        btnQuery.change_btn.click(function(){
            btnFunc.change_func();
        });
        btnQuery.wh_change_btn.click(function(){
            btnFunc.change_func(true);
        });
        btnQuery.chgBtnL2R.click(function(){
            btnFunc.l2rMv_func();
        });
        btnQuery.chgBtnR2L.click(function(){
            btnFunc.r2lMv_func();
        });
    };

    /**
     * grid  initialization
     */
    var iniGridInfo = function(){
        var grdInfoCM = [
            {name: 'prd_seq_id',   index: 'prd_seq_id',   label: PRD_SEQ_ID_TAG  ,   width: PRD_SEQ_ID_CLM ,hidden : true},
            {name: 'fab_sn',       index: 'fab_sn',       label: PRD_SEQ_ID_TAG  ,   width: PRD_SEQ_ID_CLM },
            {name: 'box_id_fk',    index: 'box_id_fk',    label: BOX_ID_TAG  ,       width: BOX_ID_CLM},
            {name: 'slot_no',      index: 'slot_no',      label: SLOT_NO_TAG     ,  width: SLOT_NO_CLM},
            {name: 'prd_stat',     index: 'prd_stat',     label: STATUS_TAG ,        width: PRD_STAT_CLM}
          
       ];
        controlsQuery.mainGrdL.grdId.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:500,
            width:380,
            shrinkToFit:false,
            scroll:true,
            rownumWidth : 25,
            resizable : true,
            rowNum:40,
            loadonce:true,
            fixed:true,
            viewrecords:true,
            rownumbers : true,
            sortable:false,
            multiselect : true,
            cmTemplate: {sortable:false},
            pager : controlsQuery.mainGrdL.grdPgText,
            colModel: grdInfoCM
        });
        controlsQuery.mainGrdR.grdId.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:500,
            width:380,
            shrinkToFit:false,
            scroll:true,
            rownumWidth : 25,
            resizable : true,
            rowNum:40,
            loadonce:true,
            fixed:true,
            viewrecords:true,
            rownumbers : true,
            sortable:false,
            multiselect : true,
            cmTemplate: {sortable:false},
            pager : controlsQuery.mainGrdR.grdPgText,
            colModel: grdInfoCM
        });
    };

    /**
     * Other action bind
     */
    var otherActionBind = function(){
        // Reset size of jqgrid when window resize
        controlsQuery.W.resize(function(){
            toolFunc.resetJqgrid();
        });

        //Stop from auto commit
        $("form").submit(function(){
            return false;
        });

        controlsQuery.ppBoxIDInputL.keydown(function(event){
            if(event.keyCode === 13){
                btnFunc.query_func();
            }
        });
        controlsQuery.ppBoxIDInputR.keydown(function(event){
            if(event.keyCode === 13){
                btnFunc.query_func();
            }
        });
    };

    /**
     * Ini contorl's data
     */
    var iniContorlData = function(){
        gVal.boxIdL = "";
        gVal.boxIdR = "";

        toolFunc.clearInput();
        toolFunc.iniOpeSelect();
    };

    /**
     * Ini view, data and action bind
     */
    var initializationFunc = function(){
    	btnQuery.wh_change_btn.hide();
        iniGridInfo();
        iniButtonAction();
        otherActionBind();

        toolFunc.resetJqgrid();
        iniContorlData();
    };

    initializationFunc();
    function resizeFnc(){                                                                      
    	var offsetBottom, divWidth;                                                              
    	
    	grdDivWidth = domObj.$grdDiv.width();
		divWidth = domObj.$boxDetailDivL.width();                                   
		offsetBottom = domObj.$window.height() - domObj.$boxDetailDivL.offset().top;
		domObj.$boxDetailDivL.height(offsetBottom * 0.95);                          
		domObj.$boxDetailGrdL.setGridWidth(grdDivWidth * 0.45);                   
		domObj.$boxDetailGrdL.setGridHeight(offsetBottom * 0.99 - 51);     
		
		domObj.$lrBtnDiv.offset({left:domObj.$window.width()/2-30});
		divWidth = domObj.$boxDetailDivR.width();                                   
		offsetBottom = domObj.$window.height() - domObj.$boxDetailDivR.offset().top;
		domObj.$boxDetailDivR.height(offsetBottom * 0.95);                          
		domObj.$boxDetailGrdR.setGridWidth(grdDivWidth * 0.45);                   
		domObj.$boxDetailGrdR.setGridHeight(offsetBottom * 0.99 - 51);  
    };                                                                                         
    resizeFnc();                                                                               
    domObj.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	}); 
});

