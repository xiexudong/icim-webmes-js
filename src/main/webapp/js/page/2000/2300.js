$(document).ready(function() {
    var VAL ={
        NORMAL     : "0000000"  ,
        T_XPQCOPE  : "XPQCOPE"  ,
        T_XPWIPRETAIN : "XPWIPRETAIN" ,
        EVT_USER   : $("#userId").text(),
    };
    var gFab_sn = "";
    var domObj = {
			$window : $(window),
			$ipqcDetailDiv : $("#ipqcDetailDiv"),
			$ipqcDetailGrd : $("#ipqcDetailGrd")
	};
    /**
     * All controls's jquery object/text
     * @type {Object}
     */
    var controlsQuery = {
        W                  : $(window)          ,
        pnlIDInput         : $("#pnlIDInput")     ,
//        ipqcUserInput  : $("#ipqcUserInput") ,
        remarkInput        : $("#remarkInput")   ,
        judgeRadioOK       : $("#judgeRadioOK")   ,
        judgeRadioNG       : $("#judgeRadioNG")   ,
        lotIdOld           : $("#lotIdOld")       ,
        lotIdNew           : $("#lotIdNew")       ,
        updateLotBtn       : $("#update_lot_btn") ,
        cusIDSelect        : $("#cusIDSelect")              ,
        woIdSelect         : $("#woIdSelect")              ,
        lotIDInput         : $("#lotIDInput")       ,
        boxIDInput         : $("#boxIDInput")       ,
        groupIdNew         : $("#groupIdNew")       ,
        mtrlBoxInput       : $("#mtrlBoxInput"),
        mainGrd   :{
            grdId     : $("#ipqcDetailGrd")   ,
            grdPgText : "#ipqcDetailPg"       ,
            fatherDiv : $("#ipqcDetailDiv")
        }
    };

    /**
     * All button's jquery object
     * @type {Object}
     */
    var btnQuery = {
        f1                       : $("#f1_btn"),
        ipqc_btn                 : $("#ipqc_btn"),
        update_group_id_btn      : $("#update_group_id_btn"),
        update_Lot_Btn           : $("#update_lot_btn") ,
      //  wip_reserve_btn          : $("#wip_reserve_btn") , 
       // wip_reserve_cancel_btn   : $("#wip_reserve_cancel_btn") 
    };

    /**
     * All tool functions
     * @type {Object}
     */
    var toolFunc = {
        resetJqgrid :function(){
//            controlsQuery.W.unbind("onresize");
//            controlsQuery.mainGrd.grdId.setGridWidth(controlsQuery.mainGrd.fatherDiv.width()*0.90);
//            controlsQuery.W.bind("onresize", this);
        },
        iniWoIdSelect : function(){
            var inObj,
                outObj;
            var cus_id = $.trim(controlsQuery.cusIDSelect.val()); 
            inObj = {
                    trx_id      : "XPAPLYWO",
                    action_flg  : 'Q'           ,
            		iary       : {
            			wo_stat: "WAIT"
            		}

                };

            if( cus_id ){
        		inObj.iary.cus_id = cus_id;        		
            }

            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woIdSelect", true);
            }
        },
        
        clearInput : function(){
            controlsQuery.pnlIDInput.val("");
            controlsQuery.remarkInput.val("");
            controlsQuery.judgeRadioOK.attr("checked", "checked");
        }
    };

    /**
     * All button click function
     * @type {Object}
     */
    var btnFunc = {
    	query_main_ipqc_btn_func : function (){
    		var prd_seq_id = $.trim(controlsQuery.pnlIDInput.val());
        	if(prd_seq_id){
        		inObj = {
        			trx_id : "XPINQSHT",
        			action_flg : "F",
        			fab_sn : prd_seq_id
        		};
        		outObj = comTrxSubSendPostJson(inObj);
        		if (outObj.rtn_code == VAL.NORMAL) {
        			if(outObj.has_more_flg === "Y"){
        				FabSn.showSelectWorderDialog(prd_seq_id, outObj.oary3, function (data){
        					gFab_sn = prd_seq_id;
            				controlsQuery.pnlIDInput.val(data.prd_seq_id);
            				btnFunc.ipqc_btn_func();
            			});
        			}else{
        				gFab_sn = prd_seq_id;
        				btnFunc.ipqc_btn_func();
        			}
        		}
        	}
    	},
        //IPQC
        ipqc_btn_func : function(){
            var iary=[],
                inObj,
                outObj,
                sht_id,
                ipqc_note,rowIds, i,rowData,
                judge_grade;

            ipqc_note = $.trim(controlsQuery.remarkInput.val());
            if( !ipqc_note ){
                showErrorDialog("","请输入备注");
                return false;
            }
            
            judge_grade = $("input[name='judgeRadios']:checked").val();
            if("OK" === judge_grade){
                judge_grade = "OK";
            }else if ("SC" === judge_grade){
            	judge_grade = "SC";
            }else if ("NG" === judge_grade) {
                judge_grade = "NG";
            }else if ("RW" === judge_grade) {
                judge_grade = "RW";
            }else if ("GK" === judge_grade) {
                judge_grade = "GK";
            }else if ("LZ" === judge_grade) {
                judge_grade = "LZ";
            }else{
            	 judge_grade = "RM";
            }
            
            rowIds = controlsQuery.mainGrd.grdId.jqGrid('getGridParam','selarrrow');
            if(parseInt(rowIds.length) != 0){
            	for (i = 0; i < rowIds.length; i ++){
                    rowData = controlsQuery.mainGrd.grdId.jqGrid("getRowData", rowIds[i]);
            		iary.push({
                        sht_id      : rowData["prd_seq_id"],
                        judge_grade : judge_grade,
                        judge_note  : ipqc_note,
                        evt_usr     : VAL.EVT_USER           			
            		});
            	}
            }else{
                showErrorDialog("","请选择需要判定的产品！");
                return false;
            }
            
            inObj = {
                trx_id   : VAL.T_XPQCOPE ,
                act_flg  : "A",
                iary_cnt : rowIds.length,
                iary     : iary
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
//                showMessengerSuccessDialog("IPQC判定成功!",1);
            	btnFunc.query_func();
            
            }
        },
        
        query_main : function (){
        	var prd_seq_id = $.trim(controlsQuery.pnlIDInput.val());
        	if(prd_seq_id){
        		inObj = {
        			trx_id : "XPINQSHT",
        			action_flg : "F",
        			fab_sn : prd_seq_id
        		};
        		outObj = comTrxSubSendPostJson(inObj);
        		if (outObj.rtn_code == VAL.NORMAL) {
        			if(outObj.has_more_flg === "Y"){
        				FabSn.showSelectWorderDialog(prd_seq_id, outObj.oary3, function (data){
            				controlsQuery.pnlIDInput.val(data.prd_seq_id);
            				btnFunc.f1_func();
            			});
        			}else{
        				btnFunc.f1_func();
        			}
        		}
        	}
        },
        
        //query lot id
        f1_func : function (){
        	var prd_seq_id = $.trim(controlsQuery.pnlIDInput.val());
        	
        	if("" === prd_seq_id){
        		showErrorDialog("","产品唯一码不能为空");
        		return false;
        	}
        	
        	var iary,
	            inObj,
	            outObj;
        	
        	iary = {
                sht_id : prd_seq_id,
            };

            inObj = {
                trx_id : VAL.T_XPQCOPE ,
                act_flg : "B",
                iary : iary
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
            	controlsQuery.lotIdOld.val(outObj.lot_id);
            }
        },

        //query fucntion
        query_func : function (){
        	var cus_id     = $.trim(controlsQuery.cusIDSelect.val());
        	var wo_id      = $.trim(controlsQuery.woIdSelect.val());
        	var lot_id     = $.trim(controlsQuery.lotIDInput.val());
        	var mtrl_box_id = $.trim(controlsQuery.mtrlBoxInput.val());
        	var box_id     = $.trim(controlsQuery.boxIDInput.val());
        	var prd_seq_id = $.trim(controlsQuery.pnlIDInput.val());
        	
        	if(!cus_id){
        		showErrorDialog("","客户代码不能为空");
        		return false;
        	}
        	if(!wo_id){
        		showErrorDialog("","工单号不能为空");
        		return false;
        	}        
        	
        	var inObj, outObj;
        	
            inObj = {
                trx_id     : "XPINQSHT" ,
                action_flg : "C",
    			wo_id  : wo_id,
    			lot_id : lot_id,
    			mtrl_box_id : mtrl_box_id,
    			box_id : box_id,
    			fab_sn : prd_seq_id
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {            	
            	controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
                setGridInfo(outObj.table, controlsQuery.mainGrd.grdId);
            }
        },
        
//        query_main_update_lot_id_func : function (){
//        	var prd_seq_id = $.trim(controlsQuery.pnlIDInput.val());
//        	if(prd_seq_id){
//        		inObj = {
//        			trx_id     : "XPINQSHT",
//        			action_flg : "F",
//        			fab_sn     : prd_seq_id
//        		};
//        		outObj = comTrxSubSendPostJson(inObj);
//        		if (outObj.rtn_code == VAL.NORMAL) {
//        			if(outObj.has_more_flg === "Y"){
//        				FabSn.showSelectWorderDialog(prd_seq_id, outObj.oary3, function (data){
//            				controlsQuery.pnlIDInput.val(data.prd_seq_id);
//            				btnFunc.update_lot_id_func();
//            			});
//        			}else{
//        				btnFunc.update_lot_id_func();
//        			}
//        		}
//        	}
//        },
        
        // update lot id
        update_lot_id_func : function (){
        	var iary=[], inObj, outObj, rowIds, i, rowData, lot_id, lot_note;
        	lot_note = $.trim(controlsQuery.remarkInput.val());
        	lot_id = $.trim(controlsQuery.lotIdNew.val());        	
        	if(!lot_id){
        		showErrorDialog("","新批次不能为空");
        		return false;
        	}
        	
            rowIds = controlsQuery.mainGrd.grdId.jqGrid('getGridParam','selarrrow');
            if(parseInt(rowIds.length) != 0){
            	for (i = 0; i < rowIds.length; i ++){
                    rowData = controlsQuery.mainGrd.grdId.jqGrid("getRowData", rowIds[i]);
            		iary.push({
                        sht_id      : rowData["prd_seq_id"],
                        lot_id      : lot_id,
                        judge_note  : lot_note,
                        evt_usr     : VAL.EVT_USER           			
            		});
            	}
            }else{
                showErrorDialog("","请选择需要更改批次的产品！");
                return false;
            }  
            
            inObj = {
                trx_id   : VAL.T_XPQCOPE ,
                act_flg  : "C",
                iary_cnt : rowIds.length,                
                iary     : iary
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
//                showMessengerSuccessDialog("批次更改成功!",1);
            	btnFunc.query_func();
            }
        },
        //Group ID change funciton
        group_btn_func : function(){
            var iary =[],
                inObj,
                outObj,
                ipqc_note,rowIds, i,rowData,
                group_id;

            ipqc_note = $.trim(controlsQuery.remarkInput.val());
            if( !ipqc_note ){
                showErrorDialog("","请输入备注");
                return false;
            }
            
            group_id = $.trim(controlsQuery.groupIdNew.val());
//            if ( !group_id ){
//                showErrorDialog("","请输入组代码！");
//                return false;
//            }
            
            rowIds = controlsQuery.mainGrd.grdId.jqGrid('getGridParam','selarrrow');
            if(parseInt(rowIds.length) != 0){
            	for (i = 0; i < rowIds.length; i ++){
                    rowData = controlsQuery.mainGrd.grdId.jqGrid("getRowData", rowIds[i]);
            		iary.push({
                        sht_id      : rowData["prd_seq_id"],
                        group_id    : group_id,
                        judge_note  : ipqc_note,
                        evt_usr     : VAL.EVT_USER           			
            		});
            	}
            }else{
                showErrorDialog("","请选择需要更改组代码的产品！");
                return false;
            }
            
            inObj = {
                trx_id   : VAL.T_XPQCOPE ,
                act_flg  : "D",
                iary_cnt : rowIds.length,
                iary     : iary
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
//                showMessengerSuccessDialog("批量更改组代码成功!",1);
            	btnFunc.query_func();

            }
        }
        //WIP Reserve funciton
//        wip_reserve_btn_func : function(){
//            var iary_box =[], iary_sc = [],
//                inObj_box, outObj_box, inObj_sc, outObj_sc,
//                rowIds, i, rowData ,box_has = "N", sc_has = "N"  ,box_same_has ,j    ;
//            
//            rowIds = controlsQuery.mainGrd.grdId.jqGrid('getGridParam','selarrrow');
//            if(parseInt(rowIds.length) != 0){
//            	for (i = 0; i < rowIds.length; i ++){
//                    rowData = controlsQuery.mainGrd.grdId.jqGrid("getRowData", rowIds[i]);
//                    if (parseInt(rowData["bnk_flg"]) != 0){
//                        showErrorDialog("","只有bnk_flg = 0的产品才可以在制保留！");
//                        return false;
//                    }
//                    if (rowData["prd_stat"] != "INPR"){
//                        showErrorDialog("","只有INPR状态的产品才可以在制保留！");
//                        return false;
//                    }
//                    if ( rowData["prd_grade"] == "SC" || !rowData["box_id_fk"] ){
//                    	iary_sc.push({	prd_seq_id : rowData['prd_seq_id'] });                   	
//                    	sc_has = "Y";
//                    }else{
////                    	if (!rowData["box_id_fk"]){
////                            showErrorDialog("","只有INPR状态且已经转入箱子的产品才可以在制保留！");
////                            return false;
////                        }
//                    	if (iary_box.length == 0){
//                    		iary_box.push({ box_id  : rowData["box_id_fk"] });
//                    	}else{
//                    		box_same_has = "N";
//                    		for ( j = 0 ; j < iary_box.length; j ++ ){
//                    			if (rowData["box_id_fk"] == iary_box[j].box_id){
//                    				box_same_has = "Y";
//                    				break;
//                    			}
//                    		}
//                    		if (box_same_has == "N"){
//                        		iary_box.push({ box_id  : rowData["box_id_fk"] });
//                    		}
//                    	}
//                    	box_has = "Y";
//                    }
//
//            	}
//            }else{
//                showErrorDialog("","请选择需要在制保留的产品！");
//                return false;
//            }
//            if (sc_has == "Y"){
//            	inObj_sc = {
//                        trx_id     : VAL.T_XPWIPRETAIN,
//                        action_flg : "S",
//                        evt_usr    : VAL.EVT_USER  ,
//                        iary       : iary_sc
//                    };
//            	outObj_sc = comTrxSubSendPostJson(inObj_sc);
//            }
//            
//            if (box_has == "Y") {
//            	inObj_box = {
//                        trx_id     : VAL.T_XPWIPRETAIN,
//                        action_flg : "R",
//                        evt_usr    : VAL.EVT_USER  ,
//                        box_cnt    : iary_box.length  ,
//                        iary       : iary_box
//                    };           	
//            	outObj_box = comTrxSubSendPostJson(inObj_box);
//            }
//           	btnFunc.query_func();
//        },
        //WIP reserver cancel function
//        wip_reserve_cancel_btn_func : function(){
//            var iary_box =[], iary_sc = [],
//            inObj_box, outObj_box, inObj_sc, outObj_sc,
//            rowIds, i, rowData ,box_has = "N", sc_has = "N"  ,box_same_has ,j    ;
//        
//        rowIds = controlsQuery.mainGrd.grdId.jqGrid('getGridParam','selarrrow');
//        if(parseInt(rowIds.length) != 0){
//        	for (i = 0; i < rowIds.length; i ++){
//                rowData = controlsQuery.mainGrd.grdId.jqGrid("getRowData", rowIds[i]);
//                if (parseInt(rowData["bnk_flg"]) != 3){
//                    showErrorDialog("","只有bnk_flg = 3的产品才可以取消在制保留！");
//                    return false;
//                }
//                if (rowData["prd_stat"] != "INPR"){
//                    showErrorDialog("","只有INPR状态的产品才可以取消在制保留！");
//                    return false;
//                }
//                if ( rowData["prd_grade"] == "SC" || !rowData["box_id_fk"] ){
//                	iary_sc.push({	prd_seq_id : rowData['prd_seq_id'] });                   	
//                	sc_has = "Y";
//                }else{
////                	if (!rowData["box_id_fk"]){
////                        showErrorDialog("","只有INPR状态且已经转入箱子的产品才可以在制保留！");
////                        return false;
////                    }
//                	if (iary_box.length == 0){
//                		iary_box.push({ box_id  : rowData["box_id_fk"] });
//                	}else{
//                		box_same_has = "N";
//                		for ( j = 0 ; j < iary_box.length; j ++ ){
//                			if (rowData["box_id_fk"] == iary_box[j].box_id){
//                				box_same_has = "Y";
//                				break;
//                			}
//                		}
//                		if (box_same_has == "N"){
//                    		iary_box.push({ box_id  : rowData["box_id_fk"] });
//                		}
//                	}
//                	box_has = "Y";
//                }
//
//        	}
//        }else{
//            showErrorDialog("","请选择需要进行在制保留取消的产品！");
//            return false;
//        }
//        if (sc_has == "Y"){
//        	inObj_sc = {
//                    trx_id     : VAL.T_XPWIPRETAIN,
//                    action_flg : "E",
//                    evt_usr    : VAL.EVT_USER  ,
//                    iary       : iary_sc
//                };
//        	outObj_sc = comTrxSubSendPostJson(inObj_sc);
//        }
//        
//        if (box_has == "Y") {
//        	inObj_box = {
//                    trx_id     : VAL.T_XPWIPRETAIN,
//                    action_flg : "C",
//                    evt_usr    : VAL.EVT_USER  ,
//                    box_cnt    : iary_box.length  ,
//                    iary       : iary_box
//                };           	
//        	outObj_box = comTrxSubSendPostJson(inObj_box);
//        }
//       	btnFunc.query_func();
//    }        
        
    };

    /**
     * Bind button click action
     */
    var iniButtonAction = function(){
        btnQuery.ipqc_btn.click(function(){
//            btnFunc.query_main_ipqc_btn_func();
        	btnFunc.ipqc_btn_func();
        });
        btnQuery.f1.click(function(){
        	btnFunc.query_func();
        });
        btnQuery.update_group_id_btn.click(function(){
        	btnFunc.group_btn_func();
        });
//        btnQuery.wip_reserve_btn.click(function(){
//        	btnFunc.wip_reserve_btn_func();
//        });
//        btnQuery.wip_reserve_cancel_btn.click(function(){
//        	btnFunc.wip_reserve_cancel_btn_func();
//        });
    };

    /**
     * grid  initialization
     */
    var iniGridInfo = function(){
        var grdInfoCM = [
            {name: 'prd_seq_id',          index: 'prd_seq_id',          label: PRD_SEQ_ID_TAG  ,   width: SHT_ID_CLM  , hidden : true},
            {name: 'fab_sn',              index: 'fab_sn',              label: PRD_SEQ_ID_TAG  ,   width: SHT_ID_CLM},
            {name: 'prd_grade',           index: 'prd_grade',           label: JUDGE_TAG       ,   width: JUDGE_GRADE_CLM},
            {name: 'evt_usr',             index: 'evt_usr',             label: EVT_USR         ,   width: EVT_USR_CLM},
            {name: 'wo_id_fk',            index: 'wo_id_fk',            label: WO_ID_TAG       ,   width: WO_ID_FK_CLM},
            {name: 'lot_id',              index: 'lot_id',              label: LOT_ID_TAG      ,   width: SHT_ID_CLM},
            {name: 'box_id_fk',           index: 'box_id_fk',           label: BOX_ID_TAG      ,   width: SHT_ID_CLM},
            {name: 'mtrl_box_id_fk',      index: 'mtrl_box_id_fk',      label: "来料箱号"       ,   width: SHT_ID_CLM},
            {name: 'prd_stat',            index: 'prd_stat',            label: PRD_STAT_TAG    ,   width: PRD_STAT_CLM},
            {name: 'group_id',            index: 'group_id',            label: GROUP_ID_TAG    ,   width: SHT_ID_CLM},
            {name: 'bnk_flg',              index: 'bnk_flg',            label: "在制保留"         ,   width: QTY_CLM},
            {name: 'cr_ope_id_fk',        index: 'cr_ope_id_fk',        label: OPE_NO_TAG      ,   width: CR_OPE_ID_FK_CLM, hidden : true},
            {name: 'fst_judge_timestamp', index: 'fst_judge_timestamp', label: "判定时间"         ,   width: JUDGE_TIMESTAMP_CLM},
            {name: 'sht_ope_msg',          index: 'sht_ope_msg',          label: REMARK_TAG      ,   width: JUDGE_NOTE_CLM}
        ];
        controlsQuery.mainGrd.grdId.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:300,
            width:600,
            shrinkToFit:false,
            scroll:true,
            rownumWidth : true,
            resizable : true,
            rowNum:40,
            loadonce:true,
            fixed:true,
            viewrecords:true,
            multiselect : true,
            pager : controlsQuery.mainGrd.grdPgText,
            colModel: grdInfoCM
        });
     };

    /**
     * Other action bind
     */
    var otherActionBind = function(){
        // Reset size of jqgrid when window resize
        controlsQuery.W.resize(function(){
            toolFunc.resetJqgrid();
        });

        //Stop from auto commit
        $("form").submit(function(){
            return false;
        });

//        controlsQuery.ppBoxIDInput.keydown(function(event){
//            if(event.keyCode === 13){
//                btnFunc.f1_func();
//            }
//        });pnlIDInput
        
        controlsQuery.pnlIDInput.keydown(function(event){
        	if(event.keyCode === 13){
        		btnFunc.query_main();
        	}
        });
        
        controlsQuery.updateLotBtn.click(function (){
//    		var func_code = "F_2300_CHANGE_LOT";
//    		if(checkUserFunc(func_code) == false){
//    			showErrorDialog("","你没有权限调整批次！");
//    			return;
//    		}
        	btnFunc.update_lot_id_func();
        });
        
        controlsQuery.lotIdNew.keydown(function(event){
//        	if(event.keyCode === 13){
//        		var func_code = "F_2300_CHANGE_LOT";
//        		if(checkUserFunc(func_code) == false){
//        			showErrorDialog("","你没有权限调整批次！");
//        			return;
//        		}
//            	btnFunc.query_main_update_lot_id_func();
//        	}
        });
        
        //Set
        controlsQuery.cusIDSelect.change(function(){
            toolFunc.iniWoIdSelect();
        });       
    };

    /**
     * Ini contorl's data
     */
    var iniContorlData = function(){
        toolFunc.clearInput();

        //Tool info
//        toolFunc.iniBySelect();
    };
    /**
     * Ini view and data
     */
    var setcusIDSelect = function(){
        var inTrxObj,
            outTrxObj;

        inTrxObj = {
            trx_id      : 'XPLSTDAT' ,
            action_flg  : 'Q'        ,
            iary        : {
                data_cate : "CUSD"
            }
        };
        outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if( outTrxObj.rtn_code == _NORMAL ) {
            if(1 < parseInt(outTrxObj.tbl_cnt, 10)){
                outTrxObj.oary.sort(function(x, y){
                    if(x.data_item < y.data_item){
                        return -1;
                    }else if(x.data_item > y.data_item){
                        return 1;
                    }else{
                        return 0;
                    }
                });
            }
            _setSelectDate(outTrxObj.tbl_cnt, outTrxObj.oary,
                "data_item", "data_item", "#cusIDSelect", true);
        }
    };
    /**
     * Ini view, data and action bind
     */
    var initializationFunc = function(){
        iniGridInfo();
        iniButtonAction();
        otherActionBind();

        toolFunc.resetJqgrid();

        iniContorlData();
        setcusIDSelect();
    };

    initializationFunc();
    function resizeFnc(){                                                                      
    	var offsetBottom, divWidth;                                                              
		     
		divWidth = domObj.$ipqcDetailDiv.width();                                   
		offsetBottom = domObj.$window.height() - domObj.$ipqcDetailDiv.offset().top;
		domObj.$ipqcDetailDiv.height(offsetBottom * 0.95);                          
		domObj.$ipqcDetailGrd.setGridWidth(divWidth * 0.99);                   
		domObj.$ipqcDetailGrd.setGridHeight(offsetBottom * 0.99 - 51);               
    };                                                                                         
    resizeFnc();                                                                               
    domObj.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});  
});

