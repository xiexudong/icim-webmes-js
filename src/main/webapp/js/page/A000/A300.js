  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  权限特殊参数维护                                                                                                        */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2014/08/28 N0.00   meijiao.C      Initial release                      */
  /*                                                                        */
  /**************************************************************************/
var fab_id_sum="";
var eqptOutObj="";
var destOutObj="";
var useropeGrdData;
$(document).ready(function() {
	$("#tabDiv").tab('show');
	var VAL ={
        NORMAL     : "0000000"  ,
        EVT_USER   : $("#userId").text(),
        T_XPUASGRP : "XPUASGRP",
        T_XPBISDEPT: "XPBISDEPT",
        T_XPLSTDAT : "XPLSTDAT",
        T_XPBISTOL : "XPBISTOL",
        T_XPBISOPE : "XPBISOPE"
	}
	var controlsQuery = {
	        W                  : $(window)             ,
	        ope_userIdSelect   : $("#ope_userIdTxt"),
	        eqpt_userIdSelect  : $("#eqpt_userIdTxt"),
	        dest_userIdSelect  : $("#dest_userIdTxt"),
	        line_userIdSelect  : $("#line_userIdTxt"),
	        fabIdSelect        : $("#fabIdSelect"),
	        fabIdSelect1        : $("#fabIdSelect1"),
	        opeInfoDialog      : $("#opeInfoDialog"),
	        eqptInfoDialog     : $("#eqptInfoDialog"),
	        eqptInfoDialog2     : $("#eqptInfoDialog2"),
	        fabOpeInfoDialog   : $("#fabOpeInfoDialog"),
	        destInfoDialog     : $("#destInfoDialog"),
	        lineInfoDialog     : $("#lineInfoDialog"),
	        mainGrd   :{
	            grdId     : $("#userListGrd")   ,
	            grdPgText : "#userListPg"       ,
	            fatherDiv : $("#userListDiv")
	        },
	        opeGrd : {
	        	grdId     : $("#opeListGrd"),
	        	grdPgText : "#opeListPg"    ,
	        	fatherDiv : $("#opeListDiv")
	        },
	        ope2Grd : {
	        	grdId     : $("#opeList2Grd"),
	        	grdPgText : "#opeList2Pg"    ,
	        	fatherDiv : $("#opeList2Div")
	        },
	        eqptGrd : {
	        	grdId     : $("#eqptListGrd"),
	        	grdPgText : "#eqptListPg"    ,
	        	fatherDiv : $("#eqptListDiv")
	        },
	        eqpt2Grd : {
	        	grdId     : $("#eqptList2Grd"),
	        	grdPgText : "#eqptList2Pg"    ,
	        	fatherDiv : $("#eqptList2Div")
	        },
	        destGrd : {
	        	grdId     : $("#destListGrd"),
	        	grdPgText : "#destListPg"    ,
	        	fatherDiv : $("#destListDiv")
	        },
	        dest2Grd : {
	        	grdId     : $("#destList2Grd"),
	        	grdPgText : "#destList2Pg"    ,
	        	fatherDiv : $("#destList2Div")
	        },
	        fabOpeGrd : {
	        	grdId     : $("#fabOpeListGrd"),
	        	grdPgText : "#fabOpeListPg"    ,
	        	fatherDiv : $("#fabOpeListDiv")
	        },
	        fabOpe2Grd : {
	        	grdId     : $("#fabOpeList2Grd"),
	        	grdPgText : "#fabOpeList2Pg"    ,
	        	fatherDiv : $("#fabOpeList2Div")
	        },
	        fabEqptGrd : {
	        	grdId     : $("#fabEqptListGrd"),
	        	grdPgText : "#fabEqptListPg"    ,
	        	fatherDiv : $("#fabEqptListDiv")
	        },
	        fabEqpt2Grd: {
	        	grdId     : $("#fabEqptList2Grd"),
	        	grdPgText : "#fabEqptList2Pg"    ,
	        	fatherDiv : $("#fabEqptList2Div")
	        },
	        opeuserGrd : {
	        	grdId     : $("#opeuserListGrd"),
	        	grdPgText : "#opeuserListPg" ,
	        	fatherDiv : $("#opeuserListGrddiv")
	        },
	        lineGrd : {
	        	grdId     : $("#lineListGrd"),
	        	grdPgText : "#lineListPg"    ,
	        	fatherDiv : $("#lineListDiv")
	        },
	        line2Grd : {
	        	grdId     : $("#lineList2Grd"),
	        	grdPgText : "#lineList2Pg"    ,
	        	fatherDiv : $("#lineList2Div")
	        }
		}
    /**
     * All button's jquery object
     * @type {Object}
     */
    var btnQuery = {
        f1 : $("#f1_query_btn"),
        sure_ope : $("#opeInfoDialog_queryBtn"),
        sure_eqpt: $("#eqptInfoDialog_queryBtn"),
        sure_dest: $("#destInfoDialog_queryBtn"),
        sure_fabOpe: $("#fabOpeInfoDialog_queryBtn"),
        sure_eqpt2: $("#eqptInfoDialog2_queryBtn"),
        sure_line: $("#lineInfoDialog_queryBtn"),
        add_ope  : $("#addOpe"),
        del_ope  : $("#deleteOpe"),
        save_ope : $("#saveOpe"),
        add_eqpt : $("#addEqpt"),
        del_eqpt : $("#deleteEqpt"),
        save_eqpt: $("#saveEqpt"),
        add_dest : $("#addDest"),
        del_dest : $("#deleteDest"),
        save_dest: $("#saveDest"),
        add_fabOpe : $("#addFabOpe"),
        del_fabOpe : $("#deleteFabOpe"),
        save_fabOpe: $("#savedFabOpe"),
        add_fabEqpt : $("#addFabEqpt"),
        del_fabEqpt : $("#deleteFabEqpt"),
        save_fabEqpt: $("#savedFabEqpt"),
        add_line : $("#addLine"),
        del_line : $("#deleteLine"),
        save_line: $("#saveLine")
    };
	var toolFunc = {
    	initFnc : function(){
    		controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
    		controlsQuery.opeGrd.grdId.jqGrid("clearGridData");
    		controlsQuery.eqptGrd.grdId.jqGrid("clearGridData");
    		controlsQuery.destGrd.grdId.jqGrid("clearGridData");
    		controlsQuery.lineGrd.grdId.jqGrid("clearGridData");
    		controlsQuery.fabOpeGrd.grdId.jqGrid("clearGridData");
    		controlsQuery.fabEqptGrd.grdId.jqGrid("clearGridData");
    		toolFunc.ListUserID();
    		//toolFunc.listFabIDs();
    		//toolFunc.QueryInfo(controlsQuery.eqpt_userIdSelect.val(),"UREQ","#eqptListGrd");
    		//toolFunc.QueryInfo(controlsQuery.dest_userIdSelect.val(), "URDT", "#destListGrd");
    		//toolFunc.QueryInfo(controlsQuery.fabIdSelect.val(), "FBOP", "#fabOpeListGrd");
    		//toolFunc.QueryInfo(controlsQuery.fabIdSelect1.val(), "FBEQ", "#fabEqptListGrd");
    		//toolFunc.QueryInfo(controlsQuery.ope_userIdSelect.val(), "UROP", "#opeListGrd");
//    		/**
//    		 * 获取机台信息
//    		 */
    		var inObj,
      		inObj = {
      			trx_id : VAL.T_XPBISTOL,
      			action_flg : 'Q',
      			iary : {
      				unit_typ : "MAIN"
      			}
      		}
      		eqptOutObj = comTrxSubSendPostJson(inObj);
//      		/**
//    		 * 获取站点信息
//    		 */
//      		var inObj;
//      		inObj = {
//      			trx_id : VAL.T_XPBISOPE,
//      			action_flg : 'L'
//      		}
//      		fabOpeOutObj = comTrxSubSendPostJson(inObj);
      		
    	},
    	userDialogShow:function(){
	       	controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
    	},
        ListUserID:function(){
          var inObj,
              outObj;
          inObj ={
     	           trx_id     : VAL.T_XPUASGRP ,
     	           action_flg : 'Q'           
          }
          outObj = comTrxSubSendPostJson(inObj);
          if( outObj.rtn_code == VAL.NORMAL){
//        	  _setSelectDate(outObj.tbl_cnt, outObj.oary, "usr_id","usr_name", controlsQuery.ope_userIdSelect);
//        	  _setSelectDate(outObj.tbl_cnt, outObj.oary, "usr_id","usr_name", controlsQuery.eqpt_userIdSelect);
//        	  _setSelectDate(outObj.tbl_cnt, outObj.oary, "usr_id","usr_name", controlsQuery.dest_userIdSelect);
       		  setGridInfo(outObj.oary, "#opeuserListGrd" );
       		  useropeGrdData=outObj.oary;
       		}
        },
        QueryInfo:function(user_id,data_cate,comGrid){
          	var inObj,
                outObj;
            var iary = {};
            iary.data_ext = user_id;
            iary.data_cate = data_cate;
            inObj ={
   		         trx_id     : VAL.T_XPLSTDAT,
   		         action_flg : 'O' ,
   		         iary       : iary
   			};
   			outObj = comTrxSubSendPostJson(inObj);
            if( outObj.rtn_code == VAL.NORMAL){
            	$(comGrid).jqGrid("clearGridData");
            	setGridInfo(outObj.oary, comGrid );
            }
          },
          listFabIDs:function(){
        		var inObj,outObj;
        		var iary={
     				data_cate : 'FBID'
     			}
     			inObj ={
     		         trx_id     : VAL.T_XPLSTDAT,
     		         action_flg : 'Q' ,
     		         iary : iary
     			};
     			outObj = comTrxSubSendPostJson(inObj);
     			if(outObj.rtn_code == VAL.NORMAL){
     				_setSelectDate(outObj.tbl_cnt, outObj.oary, "data_id","data_desc", controlsQuery.fabIdSelect);
     				_setSelectDate(outObj.tbl_cnt, outObj.oary, "data_id","data_desc", controlsQuery.fabIdSelect1);
     			}
         },
          query_ope_list:function(){
        	    var inObj,outObj;
//        	    var iary = {};
//        	    var data_ext="";
//                iary.usr_id = controlsQuery.ope_userIdSelect.val(); 
//                inObj ={
//                   trx_id     : VAL.T_XPUASGRP ,
//                   action_flg : 'Q'        ,   
//                   iary       : iary       ,
//                   tbl_cnt    : 1
//                };
//                outObj = comTrxSubSendPostJson(inObj);
//	   			if(outObj.rtn_code == VAL.NORMAL){
//	   				fab_id_sum = outObj.oary.fab_id_sum==undefined?"":outObj.oary.fab_id_sum;
//	   			}
//	      		
//	      		for(i=0;i<fab_id_sum.length;i++){
//	   				if(fab_id_sum.substring(i,i+1)== "Y"){
//	   					break;
//	   				}
//	   			}
//	      		if(fab_id_sum.length ==0 || i>=fab_id_sum.length){
//	      			controlsQuery.opeInfoDialog.modal("hide");
//	   				showErrorDialog("","此用户没有绑定厂别，因此没有可操作的站点!");
//	   				return false;
//	   			}
//	   			for(i=0;i<fab_id_sum.length;i++){
//	   				if(fab_id_sum.substring(i,i+1)=="Y"){
//	   					if(data_ext == ""){
//	   						data_ext = "'"+(i+1)+"'";
//	   					}else{
//	   						data_ext += ",'"+(i+1)+"'";
//	   					}
//	   				}
//	   			}
//	   			if(data_ext==""){
//	   				return false;
//	   			}
//	   			var iary={
//	   				data_cate : 'FBOP',
//	   				data_ext  : data_ext
//	   			}
//	   			inObj ={
//	   		         trx_id     : VAL.T_XPLSTDAT,
//	   		         action_flg : 'I' ,
//	   		         iary : iary
//	   			};
//	   			outObj = comTrxSubSendPostJson(inObj);
//	   			if(outObj.rtn_code == VAL.NORMAL){
//	   				setGridInfo(outObj.oary, "#opeList2Grd" );
//	   			}
      		inObj = {
  			    trx_id : VAL.T_XPBISOPE,
  			    action_flg : 'L'
  		    }
	   			outObj = comTrxSubSendPostJson(inObj);
	   			if(outObj.rtn_code == VAL.NORMAL){
	   				setGridInfo(outObj.oary, "#opeList2Grd" );
	   			}
      	},
      	query_eqpt_list : function(){
   			if(eqptOutObj.rtn_code == VAL.NORMAL){
   				setGridInfo(eqptOutObj.oary, "#eqptList2Grd" );
   			}
      	},
      	query_eqpt_list2: function(){
   			if(eqptOutObj.rtn_code == VAL.NORMAL){
   				setGridInfo(eqptOutObj.oary, "#fabEqptList2Grd" );
   			}
      	},
      	query_dest_list : function(){
      		var inObj,
      			outObj;
      		var iary={
   				data_cate : "DEST"
   			}
   			inObj ={
   		         trx_id     : VAL.T_XPLSTDAT,
   		         action_flg : 'Q' ,
   		         iary : iary
   			};
   			outObj = comTrxSubSendPostJson(inObj);
   			if(outObj.rtn_code == VAL.NORMAL){
   				setGridInfo(outObj.oary, "#destList2Grd" );
   			}
      	},
      	query_line_list : function(){
      		var inObj,
      			outObj;
      		var iary={
   				data_cate : "LNID"
   			}
   			inObj ={
   		         trx_id     : VAL.T_XPLSTDAT,
   		         action_flg : 'Q' ,
   		         iary : iary
   			};
   			outObj = comTrxSubSendPostJson(inObj);
   			if(outObj.rtn_code == VAL.NORMAL){
   				setGridInfo(outObj.oary, "#lineList2Grd" );
   			}
      	},
      	list_ope_func : function(){
      		if(fabOpeOutObj.rtn_code == VAL.NORMAL){
   				setGridInfo(fabOpeOutObj.oary, "#fabOpeList2Grd" );
   			}
      	},
      	addOpeId : function(){
    		var crGrid,
    			rowIds;
			var OpecrGrid,
				OperowIds;
			OpecrGrid = controlsQuery.ope2Grd.grdId;
			OperowIds = OpecrGrid.jqGrid('getGridParam','selarrrow');
			var user_id = controlsQuery.ope_userIdSelect.val();
			for(var j=0;j<OperowIds.length;j++){
				crGrid = controlsQuery.opeGrd.grdId;
				rowIds = crGrid.jqGrid('getDataIDs');
				var OperowData = OpecrGrid.jqGrid('getRowData',OperowIds[j]);
				var dataRow={
					data_ext : user_id,
					ext_1    : OperowData['ope_id'],
					data_desc: OperowData['ope_dsc'],
					flag     : 'Y'
				}
				if(rowIds.length==0){
					crGrid.jqGrid("addRowData",1,dataRow);
				}else{
					for(var i=0;i<rowIds.length;i++){
						var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
						if(rowData['ext_1'] == OperowData['ope_id']){
							break;
						}
					}
					if(i==rowIds.length){
						var rowID = getGridNewRowIDInt("#opeListGrd");
						crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
					}
				}
				controlsQuery.opeInfoDialog.modal("hide");
			}
    	},
    	addEqptId : function(){
    		var crGrid,
    			rowIds;
			var EqptcrGrid,
				EqptrowIds;
			EqptcrGrid = controlsQuery.eqpt2Grd.grdId;
			EqptrowIds = EqptcrGrid.jqGrid('getGridParam','selarrrow');
			var user_id = controlsQuery.eqpt_userIdSelect.val();
			for(var j=0;j<EqptrowIds.length;j++){
				crGrid = controlsQuery.eqptGrd.grdId;
				rowIds = crGrid.jqGrid('getDataIDs');
				var EqptrowData = EqptcrGrid.jqGrid('getRowData',EqptrowIds[j]);
				var dataRow={
					data_ext : user_id,
					ext_1    : EqptrowData['tool_id'],
					data_desc: EqptrowData['tool_dsc'],
					flag     : 'Y'
				}
				if(rowIds.length==0){
					crGrid.jqGrid("addRowData",1,dataRow);
				}else{
					for(var i=0;i<rowIds.length;i++){
						var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
						if(rowData['ext_1'] == EqptrowData['tool_id']){
							break;
						}
					}
					if(i==rowIds.length){
						var rowID = getGridNewRowIDInt("#eqptListGrd");
						crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
					}
				}
				controlsQuery.eqptInfoDialog.modal("hide");
			}
    	},
    	addDestId : function(){
    		var crGrid,
    			rowIds;
			var DestcrGrid,
				DestrowIds;
			var user_id = controlsQuery.dest_userIdSelect.val();
			DestcrGrid = controlsQuery.dest2Grd.grdId;
			DestrowIds = DestcrGrid.jqGrid('getGridParam','selarrrow');
			for(var j=0;j<DestrowIds.length;j++){
				crGrid = controlsQuery.destGrd.grdId;
				rowIds = crGrid.jqGrid('getDataIDs');
				var DestrowData = DestcrGrid.jqGrid('getRowData',DestrowIds[j]);
				var dataRow={
					data_ext : user_id,
					ext_1    : DestrowData['data_id'],
					data_desc: DestrowData['data_desc'],
					flag     : 'Y'
				}
				if(rowIds.length==0){
					crGrid.jqGrid("addRowData",1,dataRow);
				}else{
					for(var i=0;i<rowIds.length;i++){
						var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
						if(rowData['ext_1'] == DestrowData['data_id']){
							break;
						}
					}
					if(i==rowIds.length){
						var rowID = getGridNewRowIDInt("#destListGrd");
						crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
					}
				}
				controlsQuery.destInfoDialog.modal("hide");
			}
    	},
    	addfabOpe : function(){
    		var crGrid,
    			rowIds;
			var crGrid,
				DestrowIds;
			var fab_id = controlsQuery.fabIdSelect.val();
			fabOpecrGrid = controlsQuery.fabOpe2Grd.grdId;
			fabOperowIds = fabOpecrGrid.jqGrid('getGridParam','selarrrow');
			for(var j=0;j<fabOperowIds.length;j++){
				crGrid = controlsQuery.fabOpeGrd.grdId;
				rowIds = crGrid.jqGrid('getDataIDs');
				var fabOperowData = fabOpecrGrid.jqGrid('getRowData',fabOperowIds[j]);
				var dataRow={
					data_ext : fab_id,
					ext_1    : fabOperowData['ope_id'],
					data_desc: "厂别"+fab_id+"-"+fabOperowData['ope_dsc'],
					flag     : 'Y'
				}
				if(rowIds.length==0){
					crGrid.jqGrid("addRowData",1,dataRow);
				}else{
					for(var i=0;i<rowIds.length;i++){
						var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
						if(rowData['ext_1'] == fabOperowData['ope_id']){
							break;
						}
					}
					if(i==rowIds.length){
						var rowID = getGridNewRowIDInt("#fabOpeListGrd");
						crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
					}
				}
				controlsQuery.fabOpeInfoDialog.modal("hide");
			}
    	},
    	addfabEqpt : function(){
    		var crGrid,
			rowIds;
		var EqptcrGrid,
			EqptrowIds;
		EqptcrGrid = controlsQuery.fabEqpt2Grd.grdId;
		EqptrowIds = EqptcrGrid.jqGrid('getGridParam','selarrrow');
		var fab_id = controlsQuery.fabIdSelect1.val();
		for(var j=0;j<EqptrowIds.length;j++){
			crGrid = controlsQuery.fabEqptGrd.grdId;
			rowIds = crGrid.jqGrid('getDataIDs');
			var EqptrowData = EqptcrGrid.jqGrid('getRowData',EqptrowIds[j]);
			var dataRow={
				data_ext : fab_id,
				ext_1    : EqptrowData['tool_id'],
				data_desc: EqptrowData['tool_dsc'],
				flag     : 'Y'
			}
			if(rowIds.length==0){
				crGrid.jqGrid("addRowData",1,dataRow);
			}else{
				for(var i=0;i<rowIds.length;i++){
					var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
					if(rowData['ext_1'] == EqptrowData['tool_id']){
						break;
					}
				}
				if(i==rowIds.length){
					var rowID = getGridNewRowIDInt("#fabEqptListGrd");
					crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
				}
			}
			controlsQuery.eqptInfoDialog2.modal("hide");
		}
    	},
      	del_data_func : function(data_cate,data_ext,ext_1,crGrid,rowid){
      		var inObj,outObj;
			var iary ={
				data_cate: data_cate,
				data_ext : data_ext,
				ext_1    : ext_1
			};
			inObj={
				trx_id : VAL.T_XPLSTDAT,
				action_flg : 'T',
				tbl_cnt : 1,
				iary : iary
			}
			outObj = comTrxSubSendPostJson(inObj);
			if( outObj.rtn_code == VAL.NORMAL){
				crGrid.jqGrid('delRowData', rowid);
				showSuccessDialog("删除站点信息成功");
				return;
			}
      	},
      	save_data_func:function(crGrid,rowIds,data_cate){
      		var inObj,outObj;
      		var iary = new Array();
		    if(rowIds.length==0){
				return false;
		    }
		    for(var i=0;i<rowIds.length;i++){
				var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
				var opeInf={
					data_cate: data_cate,
					data_ext : rowData['data_ext'],
					ext_1    : rowData['ext_1'],
					ext_2    : (i+1),
					data_desc: rowData['data_desc']
				}
				iary.push(opeInf);
		    }
		    inObj={
				trx_id : VAL.T_XPLSTDAT,
				action_flg : 'W',
				tbl_cnt : iary.length,
				iary : iary
			}
		    outObj = comTrxSubSendPostJson(inObj);
		    if(outObj.rtn_code == VAL.NORMAL){
		    	showSuccessDialog("信息维护成功");
		    }
      	},
    	addLineId : function(){
    		var crGrid,
    			rowIds;
			var LinecrGrid,
				LinerowIds;
			var user_id = controlsQuery.line_userIdSelect.val();
			if(!user_id){
				showErrorDialog("","用户不能空!");
				return false;
			}
			LinecrGrid = controlsQuery.line2Grd.grdId;
			LinerowIds = LinecrGrid.jqGrid('getGridParam','selarrrow');
			for(var j=0;j<LinerowIds.length;j++){
				crGrid = controlsQuery.lineGrd.grdId;
				rowIds = crGrid.jqGrid('getDataIDs');
				var LinerowData = LinecrGrid.jqGrid('getRowData',LinerowIds[j]);
				var dataRow={
					data_ext : user_id,
					ext_1    : LinerowData['data_id'],
					data_desc: LinerowData['data_item'],
					flag     : 'Y'
				}
				if(rowIds.length==0){
					crGrid.jqGrid("addRowData",1,dataRow);
				}else{
					for(var i=0;i<rowIds.length;i++){
						var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
						if(rowData['ext_1'] == LinerowData['data_id']){
							break;
						}
					}
					if(i==rowIds.length){
						var rowID = getGridNewRowIDInt("#lineListGrd");
						crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
					}
				}
				controlsQuery.lineInfoDialog.modal("hide");
			}
    	},
	}
	var btnFunc = {
    	add_ope_func : function(){
    		controlsQuery.opeInfoDialog.modal({
                backdrop:true,
                keyboard:false,
                show:false
             });
    		toolFunc.query_ope_list();
            controlsQuery.opeInfoDialog.modal("show");
            btnQuery.sure_ope.bind('click',toolFunc.addOpeId);
    	},
    	del_ope_func:function(){
      		var crGrid,
				dataRow;
			crGrid = controlsQuery.opeGrd.grdId;
			var rowid  = crGrid.jqGrid('getGridParam','selrow');
			if( rowid == null){
				showErrorDialog("","请选择需要删除的站点!");
				return false;
			}
			dataRow = crGrid.jqGrid('getRowData',rowid);
			data_ext = dataRow['data_ext'];
			ext_1 = dataRow['ext_1'];
			btnQuery.del_ope.showCallBackWarnningDialog({
				errMsg:"是否删除选中的站点?",
				callbackFn : function(data) {
					if( data.result){
						if(dataRow['flag']=="Y"){
							crGrid.jqGrid('delRowData', rowid);
						}else{
							toolFunc.del_data_func("UROP",data_ext,ext_1,crGrid,rowid);
						}
					}else{
						return ;
					}
				}
			})
      	},
      	save_ope_func:function(){
      		//UROP
      		var crGrd,rowIds;
      		crGrid = controlsQuery.opeGrd.grdId;
		    rowIds = crGrid.jqGrid('getDataIDs');
		    toolFunc.save_data_func(crGrid,rowIds,"UROP");
      	},
    	add_eqpt_func : function(){
    		controlsQuery.eqptInfoDialog.modal({
                backdrop:true,
                keyboard:false,
                show:false
             });
    		toolFunc.query_eqpt_list();
            controlsQuery.eqptInfoDialog.modal("show");
            btnQuery.sure_eqpt.bind('click',toolFunc.addEqptId);
    	},
    	del_eqpt_func:function(){
      		var crGrid,
				dataRow;
			crGrid = controlsQuery.eqptGrd.grdId;
			var rowid  = crGrid.jqGrid('getGridParam','selrow');
			if( rowid == null){
				showErrorDialog("","请选择需要删除的机台!");
				return false;
			}
			dataRow = crGrid.jqGrid('getRowData',rowid);
			data_ext = dataRow['data_ext'];
			ext_1 = dataRow['ext_1'];
			btnQuery.del_eqpt.showCallBackWarnningDialog({
				errMsg:"是否删除选中的机台?",
				callbackFn : function(data) {
					if( data.result){
						if(dataRow['flag']=="Y"){
							crGrid.jqGrid('delRowData', rowid);
						}else{
							toolFunc.del_data_func("UREQ",data_ext,ext_1,crGrid,rowid);
						}
					}else{
						return ;
					}
				}
			})
      	},
      	save_eqpt_func:function(){
      		//UROP
      		var crGrd,rowIds;
      		crGrid = controlsQuery.eqptGrd.grdId;
		    rowIds = crGrid.jqGrid('getDataIDs');
		    toolFunc.save_data_func(crGrid,rowIds,"UREQ");
      	},
    	add_dest_func : function(){
    		toolFunc.query_dest_list;
    		controlsQuery.destInfoDialog.modal({
                backdrop:true,
                keyboard:false,
                show:false
             });
    		toolFunc.query_dest_list();
            controlsQuery.destInfoDialog.modal("show");
            btnQuery.sure_dest.bind('click',toolFunc.addDestId);
    	},
    	del_dest_func:function(){
      		var crGrid,
				dataRow;
			crGrid = controlsQuery.destGrd.grdId;
			var rowid  = crGrid.jqGrid('getGridParam','selrow');
			if( rowid == null){
				showErrorDialog("","请选择需要删除的仓位!");
				return false;
			}
			dataRow = crGrid.jqGrid('getRowData',rowid);
			data_ext = dataRow['data_ext'];
			ext_1 = dataRow['ext_1'];
			btnQuery.del_dest.showCallBackWarnningDialog({
				errMsg:"是否删除选中的仓位?",
				callbackFn : function(data) {
					if( data.result){
						if(dataRow['flag']=="Y"){
							crGrid.jqGrid('delRowData', rowid);
						}else{
							toolFunc.del_data_func("URDT",data_ext,ext_1,crGrid,rowid);
						}
					}else{
						return ;
					}
				}
			})
      	},
      	save_dest_func:function(){
      		var crGrd,rowIds;
      		crGrid = controlsQuery.destGrd.grdId;
		    rowIds = crGrid.jqGrid('getDataIDs');
		    toolFunc.save_data_func(crGrid,rowIds,"URDT");
      	},
    	add_fabOpe_func : function(){
    		controlsQuery.fabOpeInfoDialog.modal({
                backdrop:true,
                keyboard:false,
                show:false
             });
            controlsQuery.fabOpeInfoDialog.modal("show");
            btnQuery.sure_fabOpe.bind('click',toolFunc.addfabOpe);
    	},
    	del_fabOpe_func:function(){
      		var crGrid,
				dataRow;
			crGrid = controlsQuery.fabOpeGrd.grdId;
			var rowid  = crGrid.jqGrid('getGridParam','selrow');
			if( rowid == null){
				showErrorDialog("","请选择需要删除的站点!");
				return false;
			}
			dataRow = crGrid.jqGrid('getRowData',rowid);
			data_ext = dataRow['data_ext'];
			ext_1 = dataRow['ext_1'];
			btnQuery.del_fabOpe.showCallBackWarnningDialog({
				errMsg:"是否删除选中的站点?",
				callbackFn : function(data) {
					if( data.result){
						if(dataRow['flag']=="Y"){
							crGrid.jqGrid('delRowData', rowid);
						}else{
							toolFunc.del_data_func("FBOP",data_ext,ext_1,crGrid,rowid);
						}
					}else{
						return ;
					}
				}
			})
      	},
      	save_fabOpe_func:function(){
      		//UROP
      		var crGrd,rowIds;
      		crGrid = controlsQuery.fabOpeGrd.grdId;
		    rowIds = crGrid.jqGrid('getDataIDs');
		    toolFunc.save_data_func(crGrid,rowIds,"FBOP");
      	},
    	add_fabEqpt_func : function(){
    		controlsQuery.eqptInfoDialog2.modal({
                backdrop:true,
                keyboard:false,
                show:false
             });
            controlsQuery.eqptInfoDialog2.modal("show");
            btnQuery.sure_eqpt2.bind('click',toolFunc.addfabEqpt);
    	},
    	del_fabEqpt_func:function(){
      		var crGrid,
				dataRow;
			crGrid = controlsQuery.fabEqptGrd.grdId;
			var rowid  = crGrid.jqGrid('getGridParam','selrow');
			if( rowid == null){
				showErrorDialog("","请选择需要删除的机台!");
				return false;
			}
			dataRow = crGrid.jqGrid('getRowData',rowid);
			data_ext = dataRow['data_ext'];
			ext_1 = dataRow['ext_1'];
			btnQuery.del_fabEqpt.showCallBackWarnningDialog({
				errMsg:"是否删除选中的机台?",
				callbackFn : function(data) {
					if( data.result){
						if(dataRow['flag']=="Y"){
							crGrid.jqGrid('delRowData', rowid);
						}else{
							toolFunc.del_data_func("FBEQ",data_ext,ext_1,crGrid,rowid);
						}
					}else{
						return ;
					}
				}
			})
      	},
      	save_fabEqpt_func:function(){
      		//FBEQ
      		var crGrd,rowIds;
      		crGrid = controlsQuery.fabEqptGrd.grdId;
		    rowIds = crGrid.jqGrid('getDataIDs');
		    toolFunc.save_data_func(crGrid,rowIds,"FBEQ");
      	},
    	add_line_func : function(){
			var user_id = controlsQuery.line_userIdSelect.val();
			if(!user_id){
				showErrorDialog("","用户不能空!");
				return false;
			}
    		toolFunc.query_line_list;
    		controlsQuery.lineInfoDialog.modal({
                backdrop:true,
                keyboard:false,
                show:false
             });
            controlsQuery.lineInfoDialog.modal("show");
            btnQuery.sure_line.bind('click',toolFunc.addLineId);
    	},
    	del_line_func:function(){
      		var crGrid,
				dataRow;
			crGrid = controlsQuery.lineGrd.grdId;
			var rowid  = crGrid.jqGrid('getGridParam','selrow');
			if( rowid == null){
				showErrorDialog("","请选择需要删除的线别!");
				return false;
			}
			dataRow = crGrid.jqGrid('getRowData',rowid);
			data_ext = dataRow['data_ext'];
			ext_1 = dataRow['ext_1'];
			btnQuery.del_dest.showCallBackWarnningDialog({
				errMsg:"是否删除选中的线别?",
				callbackFn : function(data) {
					if( data.result){
						if(dataRow['flag']=="Y"){
							crGrid.jqGrid('delRowData', rowid);
						}else{
							toolFunc.del_data_func("URLN",data_ext,ext_1,crGrid,rowid);
						}
					}else{
						return ;
					}
				}
			})
      	},
      	save_line_func:function(){
      		var crGrd,rowIds;
      		crGrid = controlsQuery.lineGrd.grdId;
		    rowIds = crGrid.jqGrid('getDataIDs');
		    toolFunc.save_data_func(crGrid,rowIds,"URLN");
      	}
	}
	var iniGridInfo = { 
    	iniUserGridInfo:function(){
    		var grdInfoCM = [
 	            {name: 'usr_id'        , index: 'usr_id'     , label: USER_ID_TAG     , width: 100 },
     			{name: 'usr_name'      , index: 'usr_name'   , label: USER_NAME_TAG   , width: 100 }
     		];
 	        controlsQuery.mainGrd.grdId.jqGrid({
 	              url:"",
 			      datatype:"local",
 			      mtype:"POST",
 			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
 			      height:500,
 			      shrinkToFit:false,
 			      scroll:false,
 			      resizable : true,
 			      loadonce:true,
 			      fixed: true,
 			      // hidedlg:true,
 			      jsonReader : {
 			            // repeatitems: false
 			          },
 			      viewrecords : true, //显示总记录数
 			      pager : '#userListPg',
 			      rownumbers  :true ,//显示行号
 			      rowNum:25,         //每页多少行，用于分页
 			      rownumWidth : 20,  //行号列宽度
 			      emptyrecords :true ,
 			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
 			      colModel: grdInfoCM,
 			      gridComplete:function(){
 			          var gridPager,
 			              pageLen;
 			          gridPager = $(this).jqGrid("getGridParam","pager");
 			          if(gridPager.length<2){
 			             return false;
 			          }
 			          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
 			          $(".ui-pg-input").hide();
 			          $('td[dir="ltr"]').hide(); 
 			          $(gridPager+"_left").hide();
 			          $(gridPager+"_center").css({width:0});
 			      },
 		          onSelectRow:function(id){
 			         $("input").attr({"disabled":true});
 			         $("select").attr({"disabled":true});
 			         var rowData = $(this).jqGrid("getRowData",id);
 			           toolFunc.QueryUserInfo(rowData['usr_id']);
 		          }
 	        });
    	},
    	iniOpeGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'data_ext'        , index: 'data_ext'      , label: USER_ID_TAG     , width: 150 },
      			{name: 'ext_1'           , index: 'ext_1'         , label: OPE_ID_TAG      , width: 150 },
      			{name: 'data_desc'       , index: 'data_desc'     , label: OPE_DSC_TAG    , width: 150 },
      			{name: 'flag'            , index: 'flag'          , label: "标记"      , width: 20 }
      		];
  	        controlsQuery.opeGrd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
  			      height:350,
  			      shrinkToFit:false,
  			      scroll:false,
  			      resizable : true,
  			      loadonce:true,
  			      fixed: true,
  			      viewrecords : true, //显示总记录数
  			      pager : '#userListPg',
  			      rownumbers  :true ,//显示行号
  			      rownumWidth : 20,  //行号列宽度
  			      rowNum:500,         //每页多少行，用于分页
  			      emptyrecords :true ,
  			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
  			      colModel: grdInfoCM
  	        });
    	},
    	iniOpe2GridInfo:function(){
    		var grdInfoCM = [
      			{name: 'ope_id'           , index: 'ope_id'         , label: OPE_ID_TAG      , width: 150 },
      			{name: 'ope_dsc'       , index: 'ope_dsc'     , label: OPE_DSC_TAG     , width: 180 }
      		];
  	        controlsQuery.ope2Grd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
  			      height:200,
  			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      multiselect : true,
			      jsonReader : {
		          },
			      rownumbers  :true ,//显示行号
			      rowNum:500,         //每页多少行，用于分页
		          colModel: grdInfoCM
  	        });
    	},
    	iniEqptGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'data_ext'        , index: 'data_ext'      , label: USER_ID_TAG     , width: 150 },
      			{name: 'ext_1'           , index: 'ext_1'         , label: TOOL_ID_TAG     , width: 150 },
      			{name: 'data_desc'       , index: 'data_desc'     , label: OPE_DSC_TAG    , width: 150 },
      			{name: 'flag'            , index: 'flag'          , label: "标记"      , width: 20 }
      		];
  	        controlsQuery.eqptGrd.grdId.jqGrid({
  	        	url:"",
			      datatype:"local",
			      mtype:"POST",
  			      width:$("#eqptListDiv").width()*0.99,
			      autowidth:false,//宽度根据父元素自适应
			      height:350,
			      shrinkToFit:false,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, //显示总记录数
			      pager : '#userListPg',
			      rownumbers  :true ,//显示行号
			      rownumWidth : 20,  //行号列宽度
			      rowNum:500,         //每页多少行，用于分页
			      emptyrecords :true ,
			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
			      colModel: grdInfoCM
  	        });
    	},
    	iniEqp2GridInfo:function(){
    		var grdInfoCM = [
      			{name: 'tool_id'         , index: 'tool_id'     , label: TOOL_ID_TAG      , width: 150 },
      			{name: 'tool_dsc'        , index: 'tool_dsc'    , label: TOOL_DSC_TAG     , width: 180 }
      		];
  	        controlsQuery.eqpt2Grd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
  			      height:230,
  			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      multiselect : true,
			      jsonReader : {
		          },
			      rownumbers  :true ,//显示行号
			      rowNum:500,         //每页多少行，用于分页
		          colModel: grdInfoCM
  	        });
    	},
    	inifabEqpt2GridInfo:function(){
    		var grdInfoCM = [
      			{name: 'tool_id'         , index: 'tool_id'     , label: TOOL_ID_TAG      , width: 150 },
      			{name: 'tool_dsc'        , index: 'tool_dsc'    , label: TOOL_DSC_TAG     , width: 180 }
      		];
  	        controlsQuery.fabEqpt2Grd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
  			      height:230,
  			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      multiselect : true,
			      jsonReader : {
		          },
			      rownumbers  :true ,//显示行号
			      rowNum:500,         //每页多少行，用于分页
		          colModel: grdInfoCM
  	        });
    	},
    	iniDestGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'data_ext'        , index: 'data_ext'      , label: USER_ID_TAG     , width: 150 },
      			{name: 'ext_1'           , index: 'ext_1'         , label: DEST_SHOP_TAG   , width: 150 },
      			{name: 'data_desc'       , index: 'data_desc'     , label: OPE_DSC_TAG    , width: 150 },
      			{name: 'flag'            , index: 'flag'          , label: "标记"      , width: 20 }
      		];
  	        controlsQuery.destGrd.grdId.jqGrid({
  	        	url:"",
			      datatype:"local",
			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
			      height:350,
			      shrinkToFit:false,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, //显示总记录数
			      pager : '#userListPg',
			      rownumbers  :true ,//显示行号
			      rownumWidth : 20,  //行号列宽度
			      rowNum:500,         //每页多少行，用于分页
			      emptyrecords :true ,
			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
			      colModel: grdInfoCM
  	        });
    	},
    	iniDest2GridInfo:function(){
    		var grdInfoCM = [
      			{name: 'data_id'         , index: 'data_id'     , label: DEST_SHOP_TAG     , width: 150 },
      			{name: 'data_desc'       , index: 'data_desc'   , label: DEST_DESC_TAG     , width: 180 }
      		];
  	        controlsQuery.dest2Grd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
  			      height:200,
  			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      multiselect : true,
			      jsonReader : {
		          },
			      rownumbers  :true ,//显示行号
			      rowNum:500,         //每页多少行，用于分页
		          colModel: grdInfoCM
  	        });
    	},
    	iniFabOpeGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'data_ext'        , index: 'data_ext'      , label: FAB_ID_TAG     , width: 150 },
      			{name: 'ext_1'           , index: 'ext_1'         , label: OPE_ID_TAG     , width: 150 },
      			{name: 'data_desc'       , index: 'data_desc'     , label: OPE_DSC_TAG    , width: 150 },
      			{name: 'flag'            , index: 'flag'          , label: "标记"      , width: 20 }
      		];
  	        controlsQuery.fabOpeGrd.grdId.jqGrid({
  	        	url:"",
			      datatype:"local",
			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
			      height:350,
			      shrinkToFit:false,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, //显示总记录数
			      pager : '#userListPg',
			      rownumbers  :true ,//显示行号
			      rownumWidth : 20,  //行号列宽度
			      rowNum:500,         //每页多少行，用于分页
			      emptyrecords :true ,
			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
			      colModel: grdInfoCM
  	        });
    	},
    	inifabOpe2GridInfo:function(){
    		var grdInfoCM = [
      			{name: 'ope_id'        , index: 'ope_id'    , label: OPE_ID_TAG     , width: 150 },
      			{name: 'ope_dsc'       , index: 'ope_dsc'   , label: OPE_DSC_TAG    , width: 180 }
      		];
  	        controlsQuery.fabOpe2Grd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
  			      height:200,
  			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      multiselect : true,
			      jsonReader : {
		          },
			      rownumbers  :true ,//显示行号
			      rowNum:500,         //每页多少行，用于分页
		          colModel: grdInfoCM
  	        });
    	},
    	iniFabEqptGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'data_ext'        , index: 'data_ext'      , label: FAB_ID_TAG     , width: 150 },
      			{name: 'ext_1'           , index: 'ext_1'         , label: TOOL_ID_TAG     , width: 150 },
      			{name: 'data_desc'       , index: 'data_desc'     , label: OPE_DSC_TAG    , width: 150 },
      			{name: 'flag'            , index: 'flag'          , label: "标记"      , width: 20 }
      		];
  	        controlsQuery.fabEqptGrd.grdId.jqGrid({
  	        	url:"",
			      datatype:"local",
			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
			      height:350,
			      shrinkToFit:false,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, //显示总记录数
			      pager : '#userListPg',
			      rownumbers  :true ,//显示行号
			      rownumWidth : 20,  //行号列宽度
			      rowNum:500,         //每页多少行，用于分页
			      emptyrecords :true ,
			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
			      colModel: grdInfoCM
  	        });
    	},
    	iniopeuserListGrdInfo:function(){
    		var grdInfoCM = [
      			{name: 'usr_id'        , index: 'usr_id'    , label: USER_ID_TAG     , width: 150 },
      			{name: 'usr_name'       , index: 'usr_name'   , label: USER_NAME_TAG    , width: 180 }
      		];
  	        controlsQuery.opeuserGrd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
  			      height:200,
  			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      jsonReader : {
		          },
			      rownumbers  :true ,//显示行号
			      rowNum:500,         //每页多少行，用于分页
			      pager : controlsQuery.opeuserGrd.grdPgText,
		          colModel: grdInfoCM,
                 onSelectRow : function(id) {
          			var crmGrid,
    				data,
    				user_id,
        			user_name;
    			crmGrid = controlsQuery.opeuserGrd.grdId;
    			var rowid  = crmGrid.jqGrid('getGridParam','selrow');
    			data = crmGrid.jqGrid('getRowData',rowid);
    			user_id = data['usr_id'];
    			user_name = data['usr_name'];
    			controlsQuery.ope_userIdSelect.val(user_id);
    			controlsQuery.eqpt_userIdSelect.val(user_id);
    			controlsQuery.dest_userIdSelect.val(user_id);
    			controlsQuery.line_userIdSelect.val(user_id);
    			toolFunc.QueryInfo(user_id, "UROP", "#opeListGrd");
    			toolFunc.QueryInfo(user_id, "UREQ", "#eqptListGrd");
    			toolFunc.QueryInfo(user_id, "URDT", "#destListGrd");
    			//toolFunc.QueryInfo(user_id, "URLN", "#lineListGrd");
              }
  	        });
    	},
    	iniLineGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'data_ext'        , index: 'data_ext'      , label: USER_ID_TAG     , width: 150 },
      			{name: 'ext_1'           , index: 'ext_1'         , label: LINE_ID_TAG   , width: 150 },
      			{name: 'data_desc'       , index: 'data_desc'     , label: "线别描述"    , width: 150 },
      			{name: 'flag'            , index: 'flag'          , label: "标记"      , width: 20 }
      		];
  	        controlsQuery.lineGrd.grdId.jqGrid({
  	        	  url:"",
			      datatype:"local",
			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
			      height:350,
			      shrinkToFit:false,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, //显示总记录数
			      pager : '#userListPg',
			      rownumbers  :true ,//显示行号
			      rownumWidth : 20,  //行号列宽度
			      rowNum:500,         //每页多少行，用于分页
			      emptyrecords :true ,
			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
			      colModel: grdInfoCM
  	        });
    	},
    	iniLine2GridInfo:function(){
    		var grdInfoCM = [
      			{name: 'data_id'         , index: 'data_id'     , label: LINE_ID_TAG     , width: 150 },
      			{name: 'data_item'       , index: 'data_item'   , label: "线别描述"     , width: 180 }
      		];
  	        controlsQuery.line2Grd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width:"99%",
			      autowidth:false,//宽度根据父元素自适应
  			      height:200,
  			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      multiselect : true,
			      jsonReader : {
		          },
			      rownumbers  :true ,//显示行号
			      rowNum:500,         //每页多少行，用于分页
		          colModel: grdInfoCM
  	        });
    	}
	};
//	controlsQuery.eqpt_userIdSelect.change(function(){
//		toolFunc.QueryInfo(controlsQuery.eqpt_userIdSelect.val(),"UREQ","#eqptListGrd");
//	});
//	controlsQuery.dest_userIdSelect.change(function(){
//		toolFunc.QueryInfo(controlsQuery.dest_userIdSelect.val(),"URDT","#destListGrd");
//	});
//	controlsQuery.fabIdSelect.change(function(){
//		toolFunc.QueryInfo(controlsQuery.fabIdSelect.val(), "FBOP", "#fabOpeListGrd");
//	});
//	controlsQuery.fabIdSelect1.change(function(){
//		toolFunc.QueryInfo(controlsQuery.fabIdSelect1.val(), "FBEQ", "#fabEqptListGrd");
//	});
//	controlsQuery.ope_userIdSelect.change(function(){
//		toolFunc.QueryInfo(controlsQuery.ope_userIdSelect.val(), "UROP", "#opeListGrd");
//	});
	
    /**
     * Bind button click action
     */
    var iniButtonAction = function(){
    	$("body").on("shown","#opeInfoDialog",toolFunc.query_ope_list);
    	$("#opeInfoDiv").on("click","#addOpe",btnFunc.add_ope_func);
    	
    	$("body").on("shown","#eqptInfoDialog",toolFunc.query_eqpt_list);
    	$("#eqptInfoDiv").on("click","#addEqpt",btnFunc.add_eqpt_func);    	
    	
    	$("body").on("shown","#destInfoDialog",toolFunc.query_dest_list);
    	$("#destInfoDiv").on("click","#addDest",btnFunc.add_dest_func);
    	
//    	$("body").on("shown","#fabOpeInfoDialog",toolFunc.list_ope_func);
//    	$("#fabOpeInfoDiv").on("click","#addFabOpe",btnFunc.add_fabOpe_func);
//    	
//    	$("body").on("shown","#eqptInfoDialog2",toolFunc.query_eqpt_list2);
//    	$("#fabEqptInfoDiv").on("click","#addFabEqpt",btnFunc.add_fabEqpt_func);
//    	
//       	$("body").on("shown","#lineInfoDialog",toolFunc.query_line_list);
//    	$("#lineInfoDiv").on("click","#addLine",btnFunc.add_line_func);
    	
        btnQuery.del_ope.click(function(){
        	btnFunc.del_ope_func();
        });
        
        btnQuery.save_ope.click(function(){
        	btnFunc.save_ope_func();
        });
        
        btnQuery.del_eqpt.click(function(){
        	btnFunc.del_eqpt_func();
        });
        btnQuery.save_eqpt.click(function(){
        	btnFunc.save_eqpt_func();
        });
        
        btnQuery.del_dest.click(function(){
        	btnFunc.del_dest_func();
        });
        btnQuery.save_dest.click(function(){
        	btnFunc.save_dest_func();
        });
        
        btnQuery.del_fabOpe.click(function(){
        	btnFunc.del_fabOpe_func();
        });
        btnQuery.save_fabOpe.click(function(){
        	btnFunc.save_fabOpe_func();
        });
        
        btnQuery.del_fabEqpt.click(function(){
        	btnFunc.del_fabEqpt_func();
        });
        btnQuery.save_fabEqpt.click(function(){
        	btnFunc.save_fabEqpt_func();
        });
        
        btnQuery.del_line.click(function(){
        	btnFunc.del_line_func();
        });
        btnQuery.save_line.click(function(){
        	btnFunc.save_line_func();
        });
    };
	var initializationFunc = function(){
        iniGridInfo.iniUserGridInfo();
        iniGridInfo.iniOpeGridInfo();
        iniGridInfo.iniOpe2GridInfo();
        iniGridInfo.iniEqptGridInfo();
        iniGridInfo.iniEqp2GridInfo();
        iniGridInfo.iniDest2GridInfo();
        iniGridInfo.iniDestGridInfo();
       // iniGridInfo.iniLine2GridInfo();
       // iniGridInfo.iniLineGridInfo();
        //iniGridInfo.iniFabOpeGridInfo();
        //iniGridInfo.inifabOpe2GridInfo();
        //iniGridInfo.iniFabEqptGridInfo();
        iniGridInfo.iniopeuserListGrdInfo();
       // iniGridInfo.inifabEqpt2GridInfo();
        iniButtonAction();
        toolFunc.initFnc();
    };
    initializationFunc();
    $("#search_userid").keyup(onSearchInfo);
    function onSearchInfo()
    {
 	   var searchText = $.trim($("#search_userid").val());
 	   var regex = new RegExp("^" + searchText + ".*", "i");
 	   var _useropeGrdData = [];
 	   for (var i = 0; i < useropeGrdData.length; i++) {
 		   if (regex.test($.trim(useropeGrdData[i].usr_id))) {
 			   _useropeGrdData.push(useropeGrdData[i]);
 		   }
 	   }
 	   setGridInfo(_useropeGrdData, "#opeuserListGrd");
    }
    $("#search_username").keyup(onSearchInfo1);
    function onSearchInfo1()
    {
 	   var searchText = $.trim($("#search_username").val());
 	   var regex = new RegExp("^" + searchText + ".*", "i");
 	   var _useropeGrdData = [];
 	   for (var i = 0; i < useropeGrdData.length; i++) {
 		   if (regex.test($.trim(useropeGrdData[i].usr_name))) {
 			   _useropeGrdData.push(useropeGrdData[i]);
 		   }
 	   }
 	   setGridInfo(_useropeGrdData, "#opeuserListGrd");
    }
    function resizeFnc(){        
    	comResize(controlsQuery.W,controlsQuery.fabOpeGrd.fatherDiv,controlsQuery.fabOpeGrd.grdId);
    	comResize(controlsQuery.W,controlsQuery.opeGrd.fatherDiv,controlsQuery.opeGrd.grdId);
    	comResize(controlsQuery.W,controlsQuery.eqptGrd.fatherDiv,controlsQuery.eqptGrd.grdId);
    	comResize(controlsQuery.W,controlsQuery.destGrd.fatherDiv,controlsQuery.destGrd.grdId);
    	comResize(controlsQuery.W,controlsQuery.lineGrd.fatherDiv,controlsQuery.lineGrd.grdId);
    	comResize(controlsQuery.W,controlsQuery.opeuserGrd.fatherDiv,controlsQuery.opeuserGrd.grdId);
    	comResize(controlsQuery.W,controlsQuery.fabEqptGrd.fatherDiv,controlsQuery.fabEqptGrd.grdId);
    	$("#opeuserListGrd").setGridWidth((controlsQuery.W.width()-controlsQuery.opeuserGrd.grdId.offset().left)*0.95);
    };                                                                                         
    resizeFnc();                                                                               
    controlsQuery.W.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
    
    $('a[data-toggle="tab"]').on('shown.bs.modal', function (e) {
    	  e.target // activated tab
    	  e.relatedTarget // previous tab
//    	  console.info($(this));
    	  resizeFnc();
    	})
});