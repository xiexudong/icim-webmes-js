  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Bis_data Management                                   */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/08/19 N0.00   meijiao.C      Initial release                      */
  /*                                                                        */
  /**************************************************************************/
var setting = {
	async: {
		enable: true,
		url:"jcom/ztree.action",
		autoParam:["id=nodeId", "name=nodeName", "level=nodeLevel"]
	}
};
var fab_cnt=0;
var fab_id_sum="";
$(document).ready(function() {
	$("#tabDiv").tab('show');
	var VAL ={
        NORMAL     : "0000000"  ,
        EVT_USER   : $("#userId").text(),
        T_XPUASGRP : "XPUASGRP",
        T_XPBISDEPT: "XPBISDEPT",
        T_XPLSTDAT : "XPLSTDAT"
	};
	$.fn.zTree.init($("#tree"), setting);
	var controlsQuery = {
        W                  : $(window)             ,
        userID             : $("#userIDTxt"),
        userName           : $("#userNameTxt"),
        userEName          : $("#userENameTxt"),
        //userDeptID         : $("#userDeptIDSel"),
        //userLevel          : $("#userLevelTxt"),
        userCate           : $("#userCateSel"),
        //userPost           : $("#userPostTxt"),
        //userWorkBay        : $("#userWorkBayTxt"),
        userPhone          : $("#userPhoneTxt"),
        userTel            : $("#userTelTxt"),
        userMail           : $("#userMailTxt"),
        //userFrom           : $("#userFromTxt"),
        userRegTime        : $("#userRegisterTimeTxt"),
        //officerFalg        : $("#officerChk"),
        validFlag          : $("#validFlgChk"),
        lockFlag           : $("#lockFlgChk"),
        deptID             : $("#DeptIDTxt"),
        deptName           : $("#DeptNameTxt"),
        deptShortName      : $("#DeptShortNameTxt"),
        deptHead           : $("#DeptOffcierTxt"),
        deptLevel          : $("#DeptLevelSel"),
        superDept          : $("#DeptSuperSel"),
        deptValidFalg      : $("#deptvalidFlgChk"),
        queryUserDialog    : $("#queryUserDialog"),
        queryUserID        : $("#queryUserDialog_userIDTxt"),
        fabInfoDialog      : $("#fabInfoDialog"),
        fabIdSelect        : $("#fabInfoDialog_fabIdSelect"),
        opeInfoDialog      : $("#opeInfoDialog"),
        opeIdSelect        : $("#opeInfoDialog_opeIdSelect"),
        inputCom           : $("input"),
        selectCom          : $("select"),
        mainGrd   :{
            grdId     : $("#userListGrd")   ,
            grdPgText : "#userListPg"       ,
            fatherDiv : $("#userListDiv")
        },
        deptGrpGrd:{
        	grdId     : $("#deptListGrd"),
        	grdPgText : "#deptListPg"    ,
        	fatherDiv : $("#deptListDiv")
        },
        fabGrd : {
        	grdId     : $("#fabListGrd"),
        	grdPgText : "#fabListPg"    ,
        	fatherDiv : $("#fabListDiv")
        },
        opeGrd : {
        	grdId     : $("#opeListGrd"),
        	grdPgText : "#opeListPg"    ,
        	fatherDiv : $("#opeListDiv")
        },
        ope2Grd : {
        	grdId     : $("#opeList2Grd"),
        	grdPgText : "#opeList2Pg"    ,
        	fatherDiv : $("#opeList2Div")
        }
	};
	        /**
     * All button's jquery object
     * @type {Object}
     */
    var btnQuery = {
        f1 : $("#f1_query_btn"),
        f4 : $("#f4_del_btn"),
        f5 : $("#f5_upd_btn"),
        f6 : $("#f6_add_btn"),
        f9 : $("#f9_save_btn"),
        f10: $("#f10_clear_btn"),
        listDept : $("#listDept"),
        addDept: $("#addDept"),
        updateDept : $("#updateDept"),
        deleteDept : $("#deleteDept"),
        saveDept   : $("#saveDept"),
        query: $("#queryMdlDefDialog_queryBtn"),
        query_fab:$("#fabInfoDialog_queryBtn"),
        query_ope:$("#opeInfoDialog_queryBtn"),
        add_fab  : $("#addFab"),
        del_fab  : $("#deleteFab"),
        add_ope  : $("#addOpe"),
        del_ope  : $("#deleteOpe"),
        save_ope : $("#saveOpe")
    };
    var toolFunc = {
    	initFnc : function(){
    		var strLvl="";
    		var strDept="";
    		controlsQuery.inputCom.attr({'disabled':true});
    		controlsQuery.selectCom.attr({'disabled':true});
    		controlsQuery.inputCom.val("");
    		controlsQuery.selectCom.empty();
    		controlsQuery.deptValidFalg.attr("checked",false);
    		var deptLvl= ['1','2','3','4','5','6','7'];
    		for( var i=0;i<deptLvl.length;i++){
    			strLvl +="<option value=" + deptLvl[i] + ">" + deptLvl[i] + "</option>";
    		}
    		controlsQuery.deptLevel.append(strLvl);
    		controlsQuery.deptLevel.select2({
	 	    	theme : "bootstrap"
	 	    });
    		btnQuery.add_fab.attr("disabled",true);
    		btnQuery.add_fab.fadeTo("fast",0.25);
    		btnQuery.del_fab.attr("disabled",true);
    		btnQuery.del_fab.fadeTo("fast",0.25);
    		toolFunc.fabDialogShow();
    		toolFunc.iniDeptId();
    	},
    	iniDeptId:function(){
    		var inObj,
		        outObj;
	        inObj ={
	           trx_id     : VAL.T_XPBISDEPT ,
	           action_flg : 'Q'        
	        };
	        outObj = comTrxSubSendPostJson(inObj);
	        //_setSelectDate(outObj.tbl_cnt, outObj.oary, "dept_id","dept_name", controlsQuery.userDeptID);
    	},
    	
    	userDialogShow:function(){
//    		controlsQuery.inputCom.val("");
//	       	controlsQuery.selectCom.empty();
    		$(".user").val("");
	       	controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
    	},
      ListUserID:function(){
        var inObj,
            outObj;
        var iary = {};
        var QuserID = controlsQuery.queryUserID.val();
        if( QuserID != "" ){
          iary.usr_id = QuserID ;
          inObj ={
	           trx_id     : VAL.T_XPUASGRP ,
	           action_flg : 'Q'        ,   
	           iary       : iary       ,
	           tbl_cnt    : 1
        	};
        }else{
        	inObj ={
	           trx_id     : VAL.T_XPUASGRP ,
	           action_flg : 'Q'           
        	};
        }
        
        outObj = comTrxSubSendPostJson(inObj);
        if( outObj.rtn_code == VAL.NORMAL){
        	setGridInfo(outObj.oary, "#userListGrd" );
        	var flag = btnQuery.f1.data("flag");
        	if( flag == "H"){
        		controlsQuery.queryUserDialog.modal("hide");
        	}
        }
      },
      QueryUserInfo:function(user_id){
      	var inObj,
            outObj;
        var iary = {};
        iary.usr_id = user_id; 
        inObj ={
           trx_id     : VAL.T_XPUASGRP ,
           action_flg : 'Q'        ,   
           iary       : iary       ,
           tbl_cnt    : 1
        };
        outObj = comTrxSubSendPostJson(inObj);
        if( outObj.rtn_code == VAL.NORMAL){
        	toolFunc.setUserInfo(outObj);
        }
      },
      setUserInfo:function(outObj){
         controlsQuery.userID.val(outObj.oary.usr_id);
         controlsQuery.userName.val(outObj.oary.usr_name);
         //controlsQuery.userDeptID.val(outObj.oary.dept_id_fk);
         //controlsQuery.userLevel.val(outObj.oary.usr_level);
         controlsQuery.userEName.val(outObj.oary.usr_e_name);
         controlsQuery.userTel.val(outObj.oary.usr_phs);
         controlsQuery.userMail.val(outObj.oary.usr_mail);
         //controlsQuery.userPost.val(outObj.oary.usr_job);
         //controlsQuery.userWorkBay.val(outObj.oary.usr_job_loc);
         controlsQuery.userCate.val(outObj.oary.usr_typ);
         controlsQuery.userPhone.val(outObj.oary.usr_sms);
         //controlsQuery.userFrom.val(outObj.oary.usr_loc);
         controlsQuery.userRegTime.val(outObj.oary.reg_timestamp);
/*         if( outObj.oary.usr_head_flg == 'Y'){
            controlsQuery.officerFalg.attr("checked",true);
         }else{
            controlsQuery.officerFalg.attr("checked",false);
         }*/
         if( outObj.oary.valid_flg == 'Y' ){
            controlsQuery.validFlag.attr("checked",true);
         }else{
            controlsQuery.validFlag.attr("checked",false);
         }
         if( outObj.oary.lock_flg == 'Y'){
            controlsQuery.lockFlag.attr("checked",true);
         }else{
            controlsQuery.lockFlag.attr("checked",false);
         }
         /**
          * 厂别信息
          */
         fab_id_sum = outObj.oary.fab_id_sum==undefined?"":outObj.oary.fab_id_sum;
         var crGrid = controlsQuery.fabGrd.grdId;
         crGrid.jqGrid("clearGridData");
         if(fab_id_sum !=undefined){
        	 for(var i=0;i<fab_id_sum.length;i++){
            	 if(fab_id_sum.substring(i,i+1)=="Y"){
            		 var dataRow={
    					usr_id : outObj.oary.usr_id,
    					fab_id : i+1
    				 };
            		 var rowID = getGridNewRowIDInt("#fabListGrd");
    				 crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
            	 }
             }
         }
         toolFunc.listUserOper();
         
      },DleteUserInfoFnc:function(rowId,user_id){
			var inObj,
			outObj,
			month;
			var date = new Date();
			if( date.getMonth()+1 <10){
				month = "0"+(date.getMonth()+1);
			}else{
				month = date.getMonth()+1;
			}
			var strDate = date.getFullYear()+"-"+ month +"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
			
			var iary={
				usr_id:user_id,
				evt_usr:VAL.EVT_USER,
				evt_timestamp: strDate
			};
			var inObj={
				trx_id    :VAL.T_XPUASGRP,
				action_flg:'D',
				tbl_cnt:1,
				iary : iary
			};
			outObj = comTrxSubSendPostJson(inObj);
	        if(outObj.rtn_code == VAL.NORMAL){
	            showSuccessDialog("删除成功");
	            $("#userListGrd").jqGrid('delRowData', rowId);
//	            toolFunc.ListUserID();
	        }else{
	            return null;
	        }
		},
      	listDept_func:function(){
  	    	  var inObj,
  		          outObj;
  	    	  var iary=[];
  		      inObj ={
  		         trx_id     : VAL.T_XPBISDEPT ,
  		         action_flg : 'Q'        
  		      };
  		      outObj = comTrxSubSendPostJson(inObj);
  		      if( outObj.rtn_code == VAL.NORMAL){
  		    	  $(".dept").val("");
  		    	  setGridInfo(outObj.oary, "#deptListGrd" );
  		      }
      	},
		setDeptInf:function(rowData){
			controlsQuery.deptID.val(rowData.dept_id);
			controlsQuery.deptName.val(rowData.dept_name);
			controlsQuery.deptLevel.val(rowData.dept_lvl);
			controlsQuery.superDept.val(rowData.up_dept_id);
			controlsQuery.deptShortName.val(rowData.dept_s_name);
			controlsQuery.deptHead.val(rowData.lead_usr_id_fk);
			if( rowData.valid_flg =="Y"){
				controlsQuery.deptValidFalg.attr("checked",true);
			}else{
				controlsQuery.deptValidFalg.attr("checked",false);
			}
		},
		fabDialogShow : function(){
			var inObj,outObj;
			var iary={
				data_cate : 'FBID'
			};
			inObj ={
		         trx_id     : VAL.T_XPLSTDAT,
		         action_flg : 'Q' ,
		         iary : iary
			};
			outObj = comTrxSubSendPostJson(inObj);
			if(outObj.rtn_code == VAL.NORMAL){
				fab_cnt = outObj.tbl_cnt;
				_setSelectDate(outObj.tbl_cnt, outObj.oary, "data_id", "data_desc", "#fabInfoDialog_fabIdSelect", false);
			}
		},
		addFabId : function(){
			var crGrid,rowIds;
			crGrid = controlsQuery.fabGrd.grdId;
			rowIds = crGrid.jqGrid('getDataIDs');
			var fab_id = controlsQuery.fabIdSelect.val();
			var user_id = controlsQuery.userID.val();
			if(!user_id){
				showErrorDialog("","请选择用户代码!");
				controlsQuery.fabInfoDialog.modal("hide");
				return false;
			}
			var dataRow={
				usr_id : user_id,
				fab_id : fab_id
			};
			if(rowIds.length==0){
				crGrid.jqGrid("addRowData",1,dataRow);
			}else{
				for(var i=0;i<rowIds.length;i++){
					var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
					if(rowData['fab_id'] == fab_id){
						controlsQuery.fabInfoDialog.modal("hide");
						return;
					}
				}
				var rowID = getGridNewRowIDInt("#fabListGrd");
				crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
			}
			controlsQuery.fabInfoDialog.modal("hide");
		},
		opeListFunc : function(){
			var user_id = controlsQuery.userID.val();
			var i=0;
			var data_ext="";
			var inObj,outObj;
			if(!user_id){
				showErrorDialog("","请选择用户代码!");
				controlsQuery.opeInfoDialog.modal("hide");
				return false;
			}
			for(i=0;i<fab_id_sum.length;i++){
				var fab_flg = fab_id_sum.substring(i,i+1);
				if(fab_flg== "Y"){
					break;
				}
			}
			if(fab_id_sum.length ==0 || i>=fab_id_sum.length){
				showErrorDialog("","此用户没有绑定厂别，因此没有可操作的站点!");
				controlsQuery.opeInfoDialog.modal("hide");
				return false;
			}
			for(i=0;i<fab_id_sum.length;i++){
				if(fab_id_sum.substring(i,i+1)=="Y"){
					if(data_ext == ""){
						data_ext = "'"+(i+1)+"'";
					}else{
						data_ext += ",'"+(i+1)+"'";
					}
				}
			}
			var iary={
				data_cate : 'FBOP',
				data_ext  : data_ext
			};
			inObj ={
		         trx_id     : VAL.T_XPLSTDAT,
		         action_flg : 'I' ,
		         iary : iary
			};
			outObj = comTrxSubSendPostJson(inObj);
			if(outObj.rtn_code == VAL.NORMAL){
//				_setSelectDate(outObj.tbl_cnt, outObj.oary, "ext_1", "data_desc", "#opeInfoDialog_opeIdSelect", false);
				setGridInfo(outObj.oary, "#opeList2Grd" );
			}
		},
		addOpeId : function(){
			var crGrid,rowIds;
			var OpecrGrid,OperowIds;
			crGrid = controlsQuery.opeGrd.grdId;
			rowIds = crGrid.jqGrid('getDataIDs');
			OpecrGrid = controlsQuery.ope2Grd.grdId;
			OperowIds = OpecrGrid.jqGrid('getGridParam','selarrrow');
//			var ope_id = controlsQuery.opeIdSelect.val();
			var user_id = controlsQuery.userID.val();
			for(var j=0;j<OperowIds.length;j++){
				var OperowData = OpecrGrid.jqGrid('getRowData',OperowIds[j]);
				var dataRow={
					data_ext : user_id,
					ext_1    : OperowData['ext_1'],
					flag     : 'Y'
				};
				if(rowIds.length==0){
					crGrid.jqGrid("addRowData",1,dataRow);
				}else{
					for(var i=0;i<rowIds.length;i++){
						var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
						if(rowData['ext_1'] == OperowData['ext_1']){
							break;
						}
					}
					if(i==rowIds.length){
						var rowID = getGridNewRowIDInt("#opeListGrd");
						crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
					}
				}
				controlsQuery.opeInfoDialog.modal("hide");
			}
			
//			if(rowIds.length==0){
//				crGrid.jqGrid("addRowData",1,dataRow);
//			}else{
//				for(var i=0;i<rowIds.length;i++){
//					var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
//					if(rowData['ext_1'] == ope_id){
//						controlsQuery.opeInfoDialog.modal("hide");
//						return;
//					}
//				}
//				var rowID = getGridNewRowIDInt("#opeListGrd");
//				crGrid.jqGrid("addRowData",parseInt( rowID ),dataRow);
//			}
//			controlsQuery.opeInfoDialog.modal("hide");
		},
		listUserOper:function(){
			var user_id = controlsQuery.userID.val();
			if(!user_id){
				showErrorDialog("","请选择用户代码!");
				controlsQuery.opeInfoDialog.modal("hide");
				return false;
			}
			var iary={
				data_cate : 'UROP',
				data_ext  : "'"+user_id+"'"
			};
			inObj ={
		         trx_id     : VAL.T_XPLSTDAT,
		         action_flg : 'I' ,
		         iary : iary
			};
			outObj = comTrxSubSendPostJson(inObj);
			if(outObj.rtn_code == VAL.NORMAL){
				setGridInfo(outObj.oary, "#opeListGrd" );
			}
		}
    };
     var btnFunc = {
    	query_func : function(){
    	   toolFunc.userDialogShow();
    	   btnQuery.f1.data("flag","H");
           controlsQuery.queryUserDialog.modal({
              backdrop:true,
              keyboard:false,
              show:false
           });
           controlsQuery.queryUserDialog.unbind('shown.bs.modal');
           btnQuery.query.unbind('click');
           $('[rel=tooltip]').tooltip({
               placement:"bottom",
               trigger:"focus",
               animation:true
           });
           controlsQuery.queryUserDialog.bind('shown.bs.modal',toolFunc.userDialogShow);
           controlsQuery.queryUserDialog.modal("show");
           btnQuery.query.bind('click',toolFunc.ListUserID);
           controlsQuery.queryUserID.attr({'disabled':false});
           controlsQuery.queryUserID.focus();
    	},update_func:function(){
    		$(".user").attr({'disabled':false});
    		controlsQuery.userID.attr({'disabled':true});
//    		$.post('jcom/deptInfo', {
//			}, function(data) {
//				_setOpeSelectDate(outObj.tbl_cnt, outObj.oary, "ope_id", "ope_ver", "ope_dsc", controlsQuery.opeSelect);
//				for (var i = 0; i < data.length; i++) {
//					controlsQuery.userDeptID.append('<option role="option" value="'
//							+ data[i].dept_id + '">' + data[i].dept_id
//							+ "</option>");
//				}
//			});
    		btnQuery.f9.data("save_flg","U");
    		btnQuery.add_fab.attr("disabled",false);
    		btnQuery.add_fab.fadeTo("fast",1.0);
    		btnQuery.del_fab.attr("disabled",false);
    		btnQuery.del_fab.fadeTo("fast",1.0);
    	},add_func:function(){
    		$(".user").attr({'disabled':false});
    		$(".user").val("");
    		var type = ['G'];
    		for( var i =0;i<type.length;i++){
    			controlsQuery.userCate.append("<option value=>" + type[i] + "</option>");
    		}

    		controlsQuery.userCate.select2({
	 	    	theme : "bootstrap"
	 	    });
            //controlsQuery.officerFalg.attr("checked",false);
            controlsQuery.validFlag.attr("checked",false);
            controlsQuery.lockFlag.attr("checked",false);
    		btnQuery.f9.data("save_flg","A");
    		btnQuery.add_fab.attr("disabled",false);
    		btnQuery.add_fab.fadeTo("fast",1.0);
    		btnQuery.del_fab.attr("disabled",false);
    		btnQuery.del_fab.fadeTo("fast",1.0);
      	},
      	save_func:function(){
      		var usrID,
	            usrName,
	            usreName,
	            usrDeptID,
	            usrFrom,
	            usrType,
	            usrPhone,
	            usrTel,
	            usrMail,
	            usrPost,
	            usrLock,
	            headFlag,
	            usrValid,
	            usrWorkBay,
	            usrLevel,
	            usrRegTime,
	            inObj,
	            outObj,
	            strDate,
	            regDate;
	      	  var iary = new Array();
	          var action_flg = btnQuery.f9.data("save_flg");
	          var crGrid,rowIds;
	          var fab_id_sum_save="";
	          for(var j=0;j<fab_cnt;j++){
	        	  fab_id_sum_save += 'N';
	          }
	          if(action_flg==undefined){
	            return false;
	          }
		        usrID = controlsQuery.userID.val();
		        usrName = controlsQuery.userName.val();
		        usrDeptID = "DEPT";
		        //usrFrom = controlsQuery.userFrom.val();
		        //usrLevel = controlsQuery.userLevel.val();
		        userEName = controlsQuery.userEName.val();
		        //usrPost = controlsQuery.userPost.val();
		        usrPhone = controlsQuery.userPhone.val();
		        usrType = controlsQuery.userCate.find("option:selected").text();
		        usrTel = controlsQuery.userTel.val();
		        usrMail = controlsQuery.userMail.val();
		        usrRegTime = controlsQuery.userRegTime.val();
		        //usrWorkBay = controlsQuery.userWorkBay.val();
		        if(controlsQuery.lockFlag.attr("checked")=="checked"){
		          usrLock ="Y";
		        }else{
		          usrLock ="N";
		        }
		        if( controlsQuery.validFlag.attr("checked") =="checked"){
		          usrValid ="Y";
		        }else{
		          usrValid ="N";
		        }
/*		        if( controlsQuery.officerFalg.attr("checked") =="checked"){
		          headFlag ="Y";
		        }else{
		          headFlag ="N";
		        }*/
		        if( usrID == ""){
		        	showErrorDialog("","用户代码不可为空!");
		        	return false;
		        }
		        if( usrName == ""){
		        	showErrorDialog("","用户姓名不可为空!");
		        	return false;
		        }
		        if( usrDeptID == ""){
		        	showErrorDialog("","部门代码不可为空!");
		        	return false;
		        }
		        if( usrRegTime == ""){
		        	showErrorDialog("","入职日期不可为空!");
		        	return false;
		        }
	          var date = new Date();
	          if( date.getMonth()+1 <10){
	            month = "0"+(date.getMonth()+1);
	          }else{
	            month = date.getMonth()+1;
	          }
			  crGrid = controlsQuery.fabGrd.grdId;
			  rowIds = crGrid.jqGrid('getDataIDs');
			  for(var i=0;i<rowIds.length;i++){
					var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
					var fab_id = rowData['fab_id'];
					if(fab_id==1){
						fab_id_sum_save = "Y"+fab_id_sum_save.substring(1, fab_id_sum_save.length);
					}else if(fab_id == fab_id_sum_save.length){
						fab_id_sum_save = fab_id_sum_save.substring(0, fab_id_sum_save.length-1)+"Y";
					}else{
						fab_id_sum_save = fab_id_sum_save.substring(0, fab_id-1)+"Y"+fab_id_sum_save.substring(fab_id,fab_id_sum_save.length);
					}
			  }
//	          strDate = date.getFullYear()+"-"+ month +"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
//	          regDate = usrRegTime + " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
		        iary = {
			        	usr_id       : usrID,
			        	usr_name     : usrName,                   
			            usr_e_name   : userEName,   
			            dept_id_fk   : usrDeptID,
			            usr_typ      : usrType ,    
			            usr_level    : usrLevel,  
			            usr_job      : usrPost,
			            usr_job_loc  : usrWorkBay, 
			            usr_head_flg : headFlag,
			            valid_flg    : usrValid,
			            lock_flg     : usrLock, 
			            usr_mail     : usrMail,    
			            usr_sms      : usrPhone,   
			            usr_phs      : usrTel,
			            usr_loc      : usrFrom,
			            reg_timestamp: usrRegTime,
			            evt_usr      : VAL.EVT_USER,
			            fab_id_sum   : fab_id_sum_save
			        };
	          inObj = {
	            trx_id    : VAL.T_XPUASGRP,
	            action_flg: action_flg,
	            iary      : iary,
	            tbl_cnt   : 1
	          };
	          outObj = comTrxSubSendPostJson(inObj);
	          if( outObj.rtn_code == VAL.NORMAL){
	            if( action_flg == 'A'){
	              showSuccessDialog("添加用户信息成功");
	            }else{
	              showSuccessDialog("更新用户信息成功");
	            }
	            $(".user").attr("disabled",true);
	            controlsQuery.queryUserID.val("");
	            toolFunc.initFnc();
	            toolFunc.ListUserID();
	            
	          }
      	},
      	del_func:function(){
      		var dept_id,
				crGrid,
				data;
			crGrid = controlsQuery.mainGrd.grdId;
			var rowid  = crGrid.jqGrid('getGridParam','selrow');
			if( rowid == null){
				controlsQuery.userInfoDialog.modal("hide");
				showErrorDialog("请选择需要删除的用户!");
				return false;
			}
			var rowData = crGrid.jqGrid('getRowData',rowid);
			user_id = rowData['usr_id'];
			btnQuery.f4.showCallBackWarnningDialog({
				errMsg:"是否删除选中用户的信息?",
				callbackFn : function(data) {
					if( data.result){
						toolFunc.DleteUserInfoFnc( rowid,user_id );
					}else{
						return ;
					}
				}
			});
//			$.post('jcom/checkUserAuthority', {
//				function_code:"L_A100_USER_",
//				check_admin_flg :"Y"
//			}, function(data) {
//				if( data== false){
//					showErrorDialog("你没有权限执行这个操作!");
//					return false;
//				}else{
//					var rowData = crGrid.jqGrid('getRowData',rowid);
//					user_id = rowData['usr_id'];
//					btnQuery.f4.showCallBackWarnningDialog({
//						errMsg:"是否删除选中用户的信息?",
//						callbackFn : function(data) {
//							if( data.result){
//								toolFunc.DleteUserInfoFnc( rowid,user_id );
//							}else{
//								return ;
//							}
//						}
//					});
//				}
//			});
			
      	},
      	clear_func:function(){
      		controlsQuery.inputCom.val("");
      		controlsQuery.selectCom.empty();
      		controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
      	},
      	saveDept_func:function(){
      	   var deptIDval,
  			   deptNameval,
  			   headDeptVal,
               ShortName,
               deptLev,
               superDp,
               deptValid,
               inObj,
               outObj,
               strDate;
	          var iary = new Array();
	          var date = new Date();
	          if( date.getMonth()+1 <10){
	            month = "0"+(date.getMonth()+1);
	          }else{
	            month = date.getMonth()+1;
	          }
	          var action_flg = btnQuery.addDept.data("save_flg");
	          if(action_flg==undefined){
	            return false;
	          }
//	          strDate = date.getFullYear()+"-"+ month +"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
	          deptIDval = $.trim(controlsQuery.deptID.val());
	          deptNameval = $.trim(controlsQuery.deptName.val());
	          headDept = $.trim(controlsQuery.deptHead.val());
	          ShortName = $.trim(controlsQuery.deptShortName.val());
	          deptLev = $.trim(controlsQuery.deptLevel.find("option:selected").text());
	          superDp = $.trim(controlsQuery.superDept.find("option:selected").text());
	          if( controlsQuery.deptValidFalg.attr("checked")=="checked" ){
	            deptValid = 'Y';
	          }else{
	            deptValid = 'N';
	          }
	          if( deptIDval =="" ){
	        	  showErrorDialog("","部门代码不可为空!");
					return false;
	          }
	          if( deptNameval =="" ){
	        	  showErrorDialog("","部门名称不可为空!");
					return false;
	          }
/*	          if( deptLev == ""){
	        	  showErrorDialog("","请选择部门级别!");
					return false;
	          }*/
/*	          if( deptIDval != "1000"){
	        	  if( superDp == ""){
	        		  showErrorDialog("","请选择上级部门代码!");
					  return false;
	        	  }
	        	  
	          }*/
	          iary = {
	            dept_id        : deptIDval,
	            dept_name      : deptNameval,
	            dept_s_name    : ShortName,
	            lead_usr_id_fk : headDept,
	            up_dept_id     : superDp,
	            dept_lvl       : deptLev,
	            valid_flg      : deptValid,
	            evt_usr        : VAL.EVT_USER
	          }; 
	          inObj = {
	            trx_id    : VAL.T_XPBISDEPT,
	            action_flg: action_flg,
	            tbl_cnt   : 1,
	            iary      : iary
	          };
	          outObj = comTrxSubSendPostJson(inObj);
	          if( outObj.rtn_code == VAL.NORMAL){
	        	  if( action_flg == 'A'){
		              showSuccessDialog("部门信息添加成功");
	              }else{
	                 showSuccessDialog("部门信息更新成功");
	              }
	        	  toolFunc.listDept_func();
	          }
	      	},
	      	addDept_func:function(){
	      		$(".dept").attr({'disabled':false});
	      		$(".dept").val("");
	      		controlsQuery.deptValidFalg.attr({'disabled':false});
	    		controlsQuery.deptValidFalg.attr({"checked":false});
	    		
	      		btnQuery.addDept.data("save_flg","A");
	      	},
	      	updateDept_func:function(){
	      		btnQuery.addDept.data("save_flg","U");
	      		$(".dept").attr({'disabled':false});
	      		$(".sel").attr({'disabled':false});
	      		controlsQuery.deptID.attr("disabled",true);
	      	},
	      	deleteDept_func:function(){
	      		var user_id,
					crGrid,
					data;
				crGrid = controlsQuery.deptGrpGrd.grdId;
				var rowid  = crGrid.jqGrid('getGridParam','selrow');
				if( rowid == null){
					showErrorDialog("请选择需要删除的部门!");
					return false;
				}
				$.post('jcom/checkUserAuthority', {
					function_code:"L_A200_USER_"
				}, function(data) {
					if( data== false){
						showErrorDialog("你没有权限执行这个操作!");
						return false;
					}else{
						data = crGrid.jqGrid('getRowData',rowid);
						dept_id = data['dept_id'];
						btnQuery.f4.showCallBackWarnningDialog({
							errMsg:"是否删除选中部门的信息?",
							callbackFn : function(data) {
								if( data.result){
									var inObj,outObj;
									var iary ={};
									iary.dept_id = dept_id;
									inObj={
										trx_id : VAL.T_XPBISDEPT,
										action_flg : 'D',
										tbl_cnt : 1,
										iary : iary
									};
									outObj = comTrxSubSendPostJson(inObj);
									if( outObj.rtn_code == VAL.NORMAL){
										showSuccessDialog("删除部门信息成功");
										toolFunc.listDept_func();
										return;
									}
								}else{
									return ;
								}
							}
						});
					}
				});
	      	},
	      	add_fab_func : function(){
	    		controlsQuery.fabInfoDialog.modal({
	                backdrop:true,
	                keyboard:false,
	                show:false
	             });
	    		controlsQuery.fabInfoDialog.unbind('shown.bs.modal');
	    		btnQuery.query_fab.unbind('click');
	            $('[rel=tooltip]').tooltip({
	                placement:"bottom",
	                trigger:"focus",
	                animation:true
	            });
	            controlsQuery.fabInfoDialog.modal("show");
	            btnQuery.query_fab.bind('click',toolFunc.addFabId);
	            controlsQuery.fabIdSelect.attr({'disabled':false});
	    	},
	    	del_fab_func:function(){
	    		var user_id,
					crGrid,
					data;
				crGrid = controlsQuery.fabGrd.grdId;
				var rowid  = crGrid.jqGrid('getGridParam','selrow');
				if( rowid == null){
					showErrorDialog("请选择需要删除的厂别!");
					return false;
				}
				crGrid.jqGrid('delRowData', rowid);
	    	},
	      	add_ope_func : function(){
	    		controlsQuery.opeInfoDialog.modal({
	                backdrop:true,
	                keyboard:false,
	                show:false
	             });
	    		controlsQuery.opeInfoDialog.unbind('shown.bs.modal');
	    		btnQuery.query_fab.unbind('click');
	            $('[rel=tooltip]').tooltip({
	                placement:"bottom",
	                trigger:"focus",
	                animation:true
	            });
	            controlsQuery.opeInfoDialog.bind('shown.bs.modal',toolFunc.opeListFunc);
	            controlsQuery.opeInfoDialog.modal("show");
	            btnQuery.query_ope.bind('click',toolFunc.addOpeId);
	            controlsQuery.opeIdSelect.attr({'disabled':false});
	    	},
	    	del_ope_func:function(){
	      		var crGrid,
					dataRow;
				crGrid = controlsQuery.opeGrd.grdId;
				var rowid  = crGrid.jqGrid('getGridParam','selrow');
				if( rowid == null){
					showErrorDialog("请选择需要删除的站点!");
					return false;
				}
				dataRow = crGrid.jqGrid('getRowData',rowid);
				data_ext = dataRow['data_ext'];
				ext_1 = dataRow['ext_1'];
				btnQuery.del_ope.showCallBackWarnningDialog({
					errMsg:"是否删除选中的站点?",
					callbackFn : function(data) {
						if( data.result){
							if(dataRow['flag']=="Y"){
								crGrid.jqGrid('delRowData', rowid);
							}else{
								var inObj,outObj;
								var iary ={
									data_cate: "UROP",
									data_ext : data_ext,
									ext_1    : ext_1
								};
								inObj={
									trx_id : VAL.T_XPLSTDAT,
									action_flg : 'T',
									tbl_cnt : 1,
									iary : iary
								};
								outObj = comTrxSubSendPostJson(inObj);
								if( outObj.rtn_code == VAL.NORMAL){
									showSuccessDialog("删除站点信息成功");
									toolFunc.listUserOper();
									return;
								}
							}
						}else{
							return ;
						}
					}
				});
	      	},
	      	save_ope_func : function(){
	      		//UROP
	      		var crGrd,rowIds;
	      		var inObj,outObj;
	      		var iary = new Array();
	      		crGrid = controlsQuery.opeGrd.grdId;
			    rowIds = crGrid.jqGrid('getDataIDs');
			    if(rowIds.length==0){
			    	showErrorDialog("","请选择站点!");
					return false;
			    }
			    for(var i=0;i<rowIds.length;i++){
					var rowData = crGrid.jqGrid('getRowData',rowIds[i]);
					var opeInf={
						data_cate: "UROP",
						data_ext : rowData['data_ext'],
						ext_1    : rowData['ext_1']
					};
					iary.push(opeInf);
			    }
			    inObj={
					trx_id : VAL.T_XPLSTDAT,
					action_flg : 'W',
					tbl_cnt : iary.length,
					iary : iary
				};
			    outObj = comTrxSubSendPostJson(inObj);
			    if(outObj.rtn_code == VAL.NORMAL){
			    	toolFunc.listUserOper();
			    	showSuccessDialog("人员与站点信息维护成功");
			    }
	      	}
     };
    var iniGridInfo = { 
    	iniUserGridInfo:function(){
    		var grdInfoCM = [
 	            {name: 'usr_id'        , index: 'usr_id'     , label: USER_ID_TAG     , width: 100 },
     			{name: 'usr_name'      , index: 'usr_name'   , label: USER_NAME_TAG   , width: 100 }
     		];
 	        controlsQuery.mainGrd.grdId.jqGrid({
 	              url:"",
 			      datatype:"local",
 			      mtype:"POST",
 			      // height:400,//TODO:需要根据页面自适应，要相对很高
 			      // width:"99%",
 			      autowidth:true,//宽度根据父元素自适应
 			      height:500,
 			      shrinkToFit:false,
 			      scroll:false,
 			      resizable : true,
 			      loadonce:true,
 			      fixed: true,
 			      // hidedlg:true,
 			      jsonReader : {
 			            // repeatitems: false
 			          },
 			      viewrecords : true, //显示总记录数
 			      pager : '#userListPg',
 			      rownumbers  :true ,//显示行号
 			      rowNum:25,         //每页多少行，用于分页
 			      rownumWidth : 20,  //行号列宽度
 			      emptyrecords :true ,
 			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
 			      colModel: grdInfoCM,
 			      gridComplete:function(){
 			          var gridPager,
 			              pageLen;
 			          gridPager = $(this).jqGrid("getGridParam","pager");
 			          if(gridPager.length<2){
 			             return false;
 			          }
 			          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
 			          $(".ui-pg-input").hide();
 			          $('td[dir="ltr"]').hide(); 
 			          $(gridPager+"_left").hide();
 			          $(gridPager+"_center").css({width:0});
 			      },
 		          onSelectRow:function(id){
 			         $("input").attr({"disabled":true});
 			         $("select").attr({"disabled":true});
 			         var rowData = $(this).jqGrid("getRowData",id);
 			           toolFunc.QueryUserInfo(rowData['usr_id']);
 		          }
 	        });
    	},
    	iniDeptGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'dept_id'        , index: 'dept_id'       , label: DEPT_ID_TAG     , width: 100 },
      			{name: 'dept_name'      , index: 'dept_name'     , label: DEPT_NAME_TAG   , width: 100 },
  	            {name: 'dept_s_name'    , index: 'dept_s_name'   , label: "部门简称"       , width: 100 },
  	            {name: 'up_dept_id'     , index: 'up_dept_id'    , label: "上级部门代码"   , width: 100 },
  	            {name: 'lead_usr_id_fk' , index: 'lead_usr_id_fk', label: "部门主管"       , width: 100 },
  	            {name: 'dept_lvl'       , index: 'dept_lvl'      , label: "部门级别"       , width: 60 },
  	            {name: 'valid_flg'      , index: 'valid_flg'     , label: "有效标记"       , width: 60 },
      		];
  	        controlsQuery.deptGrpGrd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
//  			      autowidth:true,//宽度根据父元素自适应
  			      width : 700,
  			      height:200,
  			      shrinkToFit:false,
  			      scroll:false,
  			      resizable : true,
  			      loadonce:true,
  			      fixed: true,
  			      // hidedlg:true,
  			      jsonReader : {
  			            // repeatitems: false
  			          },
  			      viewrecords : true, //显示总记录数
  			      pager : '#userListPg',
  			      rownumbers  :true ,//显示行号
  			      rownumWidth : 20,  //行号列宽度
  			      rowNum:500,         //每页多少行，用于分页
  			      emptyrecords :true ,
  			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
  			      colModel: grdInfoCM,
  		          onSelectRow:function(id){
  			         $("input").attr({"disabled":true});
  			         $("select").attr({"disabled":true});
  			         var rowData = $(this).jqGrid("getRowData",id);
  			         toolFunc.setDeptInf(rowData);
  		          }
  	        });
    	},
    	iniFabGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'usr_id'        , index: 'usr_id'      , label: USER_ID_TAG     , width: 150 },
      			{name: 'fab_id'        , index: 'fab_id'      , label: FAB_ID_TAG      , width: 150 },
      		];
  	        controlsQuery.fabGrd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width : 400,
  			      height:200,
  			      shrinkToFit:false,
  			      scroll:false,
  			      resizable : true,
  			      loadonce:true,
  			      fixed: true,
  			      viewrecords : true, //显示总记录数
  			      pager : '#userListPg',
  			      rownumbers  :true ,//显示行号
  			      rownumWidth : 20,  //行号列宽度
  			      rowNum:500,         //每页多少行，用于分页
  			      emptyrecords :true ,
  			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
  			      colModel: grdInfoCM
  	        });
    	},
    	iniOpeGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'data_ext'        , index: 'data_ext'      , label: USER_ID_TAG     , width: 150 },
      			{name: 'ext_1'           , index: 'ext_1'         , label: OPE_ID_TAG      , width: 150 },
      			{name: 'flag'            , index: 'flag'          , label: "标记"      , width: 10 },
      		];
  	        controlsQuery.opeGrd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width : 400,
  			      height:200,
  			      shrinkToFit:false,
  			      scroll:false,
  			      resizable : true,
  			      loadonce:true,
  			      fixed: true,
  			      viewrecords : true, //显示总记录数
  			      pager : '#userListPg',
  			      rownumbers  :true ,//显示行号
  			      rownumWidth : 20,  //行号列宽度
  			      rowNum:500,         //每页多少行，用于分页
  			      emptyrecords :true ,
  			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
  			      colModel: grdInfoCM
  	        });
    	},
    	iniOpe2GridInfo:function(){
    		var grdInfoCM = [
      			{name: 'ext_1'           , index: 'ext_1'         , label: OPE_ID_TAG      , width: 150 },
      			{name: 'data_desc'       , index: 'data_desc'     , label: OPE_DSC_TAG     , width: 180 },
      		];
  	        controlsQuery.ope2Grd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
  			      width : 400,
  			      height:200,
  			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      // toppager: true , 翻页控件是否在上层显示
			      fixed: true,
			      multiselect : true,
			      jsonReader : {
		            // repeatitems: false
		          },
			      rownumbers  :true ,//显示行号
			      rowNum:500,         //每页多少行，用于分页
		          colModel: grdInfoCM
  	        });
    	}
    };

    $("#userRegisterTimeTxt" ).datepicker({
        defaultDate    : "",
        dateFormat     : 'yy-mm-dd 00:00:00.0',
        changeMonth    : true,
        changeYear     : true,
        numberOfMonths : 1
    });
                     /**
     * Bind button click action
     */
    var iniButtonAction = function(){
        btnQuery.f1.click(function(){
            btnFunc.query_func();
        });
        btnQuery.f4.click(function(){
            btnFunc.del_func();
        });
        btnQuery.f5.click(function(){
            btnFunc.update_func();
        });
        btnQuery.f6.click(function(){
            btnFunc.add_func();
        });
        btnQuery.f9.click(function(){
            btnFunc.save_func();
        });
        btnQuery.f10.click(function(){
            btnFunc.clear_func();
        });
        btnQuery.listDept.click(function(){
            toolFunc.listDept_func();
        });
        btnQuery.addDept.click(function(){
            btnFunc.addDept_func();
        });
        btnQuery.updateDept.click(function(){
            btnFunc.updateDept_func();
        });
        btnQuery.deleteDept.click(function(){
            btnFunc.deleteDept_func();
        });
        btnQuery.saveDept.click(function(){
            btnFunc.saveDept_func();
        });
        btnQuery.add_fab.click(function(){
        	btnFunc.add_fab_func();
        });
        btnQuery.del_fab.click(function(){
        	btnFunc.del_fab_func();
        });
        btnQuery.add_ope.click(function(){
        	btnFunc.add_ope_func();
        });
        btnQuery.del_ope.click(function(){
        	btnFunc.del_ope_func();
        });
        btnQuery.save_ope.click(function(){
        	btnFunc.save_ope_func();
        });
    };
    var initializationFunc = function(){
        iniGridInfo.iniDeptGridInfo();
        iniGridInfo.iniUserGridInfo();
        iniGridInfo.iniFabGridInfo();
        iniGridInfo.iniOpeGridInfo();
        iniGridInfo.iniOpe2GridInfo();
        iniButtonAction();
        toolFunc.initFnc();
    };
    initializationFunc();
    function resizeFnc(){                                                                      
      	var offsetBottom, divWidth;                                                              

    	offsetBottom = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDiv.offset().top;
    	controlsQuery.mainGrd.fatherDiv.height(offsetBottom * 0.95);                          
    	controlsQuery.mainGrd.grdId.setGridHeight(offsetBottom * 0.99 - 100);  
    	
//    	offsetBottom = controlsQuery.W.height() - controlsQuery.deptGrpGrd.fatherDiv.offset().top;
//    	controlsQuery.deptGrpGrd.fatherDiv.height(offsetBottom * 0.95);                          
//    	controlsQuery.deptGrpGrd.grdId.setGridHeight(offsetBottom * 0.99 - 100); 
      };                                                                                         
      resizeFnc();                                                                               
      controlsQuery.W.resize(function() {                                                         
      	resizeFnc();                                                                             
   });
    
});
