$(document).ready(function() { 

  var XPALMINF_Q = 'XPALMINF';

  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#alarmInfoGrd").setGridWidth($("#alarmInfoDiv").width()*0.95); 
      $("#alarmInfoGrd").setGridHeight($("#alarmInfoDiv").height()*0.80);      
      $(window).bind("onresize", this);  
  }

  /**
   * Reset size of jqgrid when window resize
   * @param  {[type]} )
   * @param  {[type]} this);
   * @return {[type]}
   */
  $(window).resize(function(){
      resetJqgrid();
  });

  var initializationFunc = function(){
    resetJqgrid();
    iniAlarmCate();
    $('#alarmCateSlt').change(function(){
      var inTrxObj = {
        trx_id: XPALMINF_Q,
        action_flg: 'A',
        alm_cate:$(this).val()
      };
      var outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code == "0000000") {
        setGridInfo(outTrxObj.oary,"#alarmInfoGrd");
        $('#updateTimeSpan').text(outTrxObj.oary[0].evt_timestamp);
      } 
    });
    //setTimeout
    var timer = setTimeout(function(){
      getNewAlmRecord();
      setTimeout(arguments.callee, 1000);
    }, 1000);
  }

  //Query alarm code cate
  var iniAlarmCate = function(){
    var inTrxObj = {
      trx_id: XPALMINF_Q,
      action_flg: 'C'
    };
    var outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if(outTrxObj.rtn_code == "0000000") {
      var alarmCateSelector=$('#alarmCateSlt');
      if(outTrxObj.alm_cate_cnt>0){
        var almCateCnt = outTrxObj.alm_cate_cnt;
        for (var i = 0; i < almCateCnt; i++){
          alarmCateSelector.append('<option value="'+outTrxObj.oary2[i].alm_cate+'">'+outTrxObj.oary2[i].alm_dec+'</option>');
        }
        alarmCateSelector.select2({
 	    	theme : "bootstrap"
 	    });
      }
    }
  }

  //Query Alarm info
  var getNewAlmRecord = function(){
    var almCate = $('#alarmCateSlt').val();
    console.info('In getNewAlmRecord almCate: '+almCate);
    if(almCate){
      var inTrxObj = {
        trx_id: XPALMINF_Q,
        action_flg: 'T',
        alm_cate: almCate,
        begin_timestamp: $('#updateTimeSpan').text()
      };
      var outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code == "0000000") {
        if(outTrxObj.record_cnt){
          var alarmRecordCnt = outTrxObj.record_cnt;  
          var currentCount = $('#alarmInfoGrd').getDataIDs.length;
          for (var i = alarmRecordCnt-1; i >=0; i--) {
            $('#alarmInfoGrd').addRowData( ++currentCount , outTrxObj.oary[i], "first" );
          };
        }
        $('#updateTimeSpan').text(outTrxObj.oary[0].evt_timestamp);//TODO undefine
      } 
    }    
  }


    /**
     * grid  initialization
     */
  var alarmInfoCM = [
        // {name: 'alm_cate',      index: 'alm_cate',       label: '订单号码', width: 80  },
        {name: 'evt_timestamp', index: 'evt_timestamp',  label: '报警时间', width: 150 },
        {name: 'alm_msg',       index: 'alm_msg',        label: '报警信息', width: 850 },
  ];
  $('#alarmInfoGrd').jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:200,
      width:1000,
      shrinkToFit:false,
      scroll:true,
      rownumWidth : true,
      resizable : true,
      rowNum:40,
      loadonce:true,
      fixed:true,
      viewrecords:true,
      pager : 'alarmInfoPg',
      fixed: true,
      colModel: alarmInfoCM  
  }); 

  initializationFunc();


  $('#f1_btn').click(function(){
    // iniAlmRecore();
  });
 
});