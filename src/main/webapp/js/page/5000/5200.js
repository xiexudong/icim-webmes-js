$(document).ready(function() {
	$("form").submit(function() {
				return false;
			});
	var VAL = {
		T_XPBMDLDF : "XPBMDLDF",
		EVT_USR : $("#userId").text(),
		NORMAL : "0000000",
		DISABLED_ATTR : {
			"disabled" : true
		},
		ENABLED_ATTR : {
			"disabled" : false
		}
	};
	var domObj = {
		$window : $(window),
		$cusIdSel : $("#cusIdSel"),
		$fhSel : $("#fhSel"),
		$mtrlProdIdSel : $("#mtrlProdIdSel"),
		$woIdTxt : $("#woIdTxt"),
		$plnStbDatepicker : $("#plnStbDatepicker"),
		$plnStbTimepicker : $("#plnStbTimepicker"),
		$plnCompDatepicker : $("#plnCompDatepicker"),
		$plnCompTimepicker : $("#plnCompTimepicker"),
		$rsvPlnPrdQtyTxt : $("#rsvPlnPrdQtyTxt"),
		$woCateSel : $("#woCateSel"),
		$plantSel : $("#plantSel"),
		$woDscTxt : $("#woDscTxt"),
		$mainTitleTxt : $("#mainTitleTxt"),
		$shtidTitleTxt : $("#shtidTitleTxt"),
		$jgyqSel : $("#jgyqSel"),
		$plnStbDatepicker : $("#plnStbDatepicker"),
		$plnStbTimepicker : $("#plnStbTimepicker"),
		$plnCompDatepicker : $("#plnCompDatepicker"),
		$plnCompTimepicker : $("#plnCompTimepicker"),
		grids : {
			$worderListDiv : $("#worderListDiv"),
			$worderListGrd : $("#worderListGrd")
		},
		$bindWoDialog_woGrd : $("#bindWoDialog_woGrd"),
		buttons : {
			$f1_query_btn : $("#f1_query_btn"),
			$f4_del_btn : $("#f4_del_btn"),
			$f5_upd_btn : $("#f5_upd_btn"),
			$commit_upd_btn : $("#commit_upd_btn"),
			$cancel_upd_btn : $("#cancel_upd_btn"),
			$f6_close_btn : $("#f6_close_btn"),
			$f7_open_btn : $("#f7_open_btn"),
			$f8_add_btn : $("#f8_add_btn"),
			$f10_clear_btn : $("#f10_clear_btn"),
			$f11_export_btn : $("#f11_export_btn"),
	        $f11_bind_btn: $("#f11_bind_btn")
		},
		dialogs : {

		}
	};
	var baseFnc = {
		replaceSpace:function(str){
			if(!str){
				return str;
			}
			return str.replace(/[ ]/g,"@");	
		},
		returnSpace:function(str){
			if(!str){
				return str;
			}
			return str.replace(/[@]/g," ");
		},
		sendXplstData : function(dataCate) {
			var inObj = {
				trx_id : "XPLSTDAT",
				action_flg : "Q",
				iary : {
					data_cate : dataCate
				}
			};
			outObj = comTrxSubSendPostJson(inObj);
			return outObj.rtn_code === VAL.NORMAL ? outObj : false;
		},
		convertTimestamp2Date : function(timestamp) {
			var date = new Date(timestamp.substring(0, 10));
			date.setHours(timestamp.substring(11, 13));
			date.setMinutes(timestamp.substring(14, 16));
			date.setSeconds(timestamp.substring(17, 19));
			return date;
		}
	};
	var controlsFunc = {
		iniClean : function(){
			domObj.buttons.$commit_upd_btn.hide();
			domObj.buttons.$cancel_upd_btn.hide();
			domObj.$woIdTxt.val("");
			domObj.$rsvPlnPrdQtyTxt.val("");
			domObj.$woDscTxt.val("");
			domObj.$mainTitleTxt.val("");
			domObj.$shtidTitleTxt.val("");
			domObj.$jgyqSel.val("");
			domObj.$fhSel.val("");
		},
		initCusSel : function() {
			var inObj, outObj, tblCnt, oaryArr;

			SelectDom.initWithSpace(domObj.$cusIdSel);

			outObj = baseFnc.sendXplstData("CUSD");
			if (!outObj) {
				return false;
			}
			tblCnt = outObj.tbl_cnt;
			oaryArr = tblCnt > 1 ? outObj.oary : [outObj.oary];
			SelectDom.addSelectArr(domObj.$cusIdSel, oaryArr,
					"data_item");
			// SelectDom.addSelectArr(domObj.$cusIdSel, tblCnt, oaryArr,
			// "data_item", "data_ext");
		},
		initplantSel : function() {
			SelectDom.initWithSpace(domObj.$plantSel);
			var plantInfo =[{data_item:"G5"},{data_item:"G6"}];
			SelectDom.addSelectArr(domObj.$plantSel, plantInfo,
			"data_item");
		},
		initMtrlSel : function() {
		 var crSelect = domObj.$mtrlProdIdSel,
                vdrID,
                i,
                tbl_cnt,
                inTrxObj,
                outTrxObj,
                inObj,
                outObj,
                oary,
                oaryArr;

			SelectDom.initWithSpace(crSelect);
            vdrID = domObj.$cusIdSel.val();
            if(!vdrID){
                console.log('客户代码为空。');
                return false;
            }

            if(vdrID=="058"){
            	$("#0581").show();
    			$("#0582").show();
    			$("#0583").show();
        		
        		iarydir = {
    				cus_id_fk : vdrID
    			};
        		inObj = {
    				trx_id : "XPBISCUS",
    				action_flg:'Q',
    				iary: iarydir
    			};
        		outObj = comTrxSubSendPostJson(inObj);
        	 
           	 if(outObj.rtn_code==VAL.NORMAL && outObj.oary !=undefined){
           		for(i=0;i<outObj.tbl_cnt;i++){
           			oary = outObj.tbl_cnt>1 ?outObj.oary[i]:outObj.oary;
           			if(oary.loc_type=="R"){
           				SelectDom.addSelect(domObj.$fhSel, oary.loc_info, oary.loc_info);
           			}
           		}
             }
 			outObjgy = baseFnc.sendXplstData("JGYQ");
			if (!outObjgy) {
				return false;
			}
			tblCnt = outObjgy.tbl_cnt;
			oaryArr = tblCnt > 1 ? outObjgy.oary : [outObjgy.oary];
			SelectDom.addSelectArr(domObj.$jgyqSel, oaryArr,
					"data_item");
            }else{
            	$("#0581").hide();
    			$("#0582").hide();
    			$("#0583").hide();
            }
            inTrxObj = {
                trx_id     : 'XPLSTDAT' ,
                action_flg : 'H' ,
                iary       : {
                    data_cate : 'CSMT',
                    data_item : vdrID
                }
            };
		
            outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if( outTrxObj.rtn_code == VAL.NORMAL ) {
                tbl_cnt = parseInt( outTrxObj.tbl_cnt, 10);
                for(i=0;i<tbl_cnt;i++){
                	oary = tbl_cnt > 1 ? outTrxObj.oary[i] : outTrxObj.oary;
                    crSelect.append('<option value='+ baseFnc.replaceSpace(oary['ext_1']) +'>'+ oary['ext_1'] +'</option>');
                }
                crSelect.select2({
        	    	theme : "bootstrap"
        	    });
            }
		},
		
		initWoCateSel : function() {
			var outObj, tblCnt, oaryArr;

			SelectDom.initWithSpace(domObj.$woCateSel);

			outObj = baseFnc.sendXplstData("WOAB");
			if (outObj) {
				tblCnt = outObj.tbl_cnt;
				oaryArr = tblCnt > 1 ? outObj.oary : [outObj.oary];
				SelectDom.addSelectArr(domObj.$woCateSel,  oaryArr,
						"data_ext", "data_desc");
			}
		},
		initPlantSel : function() {
			var outObj, tblCnt, oaryArr;
			SelectDom.initWithSpace(domObj.$plantSel);
			oaryArr=[{data_ext:"G5",data_desc:"G5"},{data_ext:"G6",data_desc:"G6"}]
			SelectDom.addSelectArr(domObj.$plantSel,  oaryArr,
						"data_ext", "data_desc");
			
		},
		iniDateTimePicker : function() {
			var datepickerData, datepickerTime;

			domObj.$plnStbDatepicker.datetimepicker({
						language : 'zh-CN',
						autoclose: true,
						pickTime : false
					});
			domObj.$plnStbDatepicker.datetimepicker().on('changeDate', function(ev){
				domObj.$plnStbDatepicker.datetimepicker('hide');
			});

			domObj.$plnCompDatepicker.datetimepicker({
						language : 'zh-CN',
						autoclose: true,
						pickTime : false
					});
			domObj.$plnCompDatepicker.datetimepicker().on('changeDate', function(ev){
				domObj.$plnCompDatepicker.datetimepicker('hide');
			});

			
			domObj.$plnStbTimepicker.datetimepicker({
						language : 'zh-CN',
						autoclose: true,
						pickDate : false
					});

			domObj.$plnCompTimepicker.datetimepicker({
						language : 'zh-CN',
						autoclose: true,
						pickDate : false
					});

			datepickerData = domObj.$plnStbDatepicker.data("datetimepicker");
			datepickerData.setLocalDate(datepickerData.getLocalDate());
			datepickerData = domObj.$plnCompDatepicker.data("datetimepicker");
			datepickerData.setLocalDate(datepickerData.getLocalDate());

			datepickerTime = domObj.$plnStbTimepicker.data("datetimepicker");
			datepickerTime.setLocalDate(new Date());
			datepickerTime = domObj.$plnCompTimepicker.data("datetimepicker");
			datepickerTime.setLocalDate(new Date());
		},
		initGirdInfo : function() {
			domObj.grids.$worderListGrd.jqGrid({
						url : "",
						datatype : "local",
						width : $("#worderListDiv").width(),
						mtype : "POST",
						shrinkToFit : true,
						scroll : true,
						viewrecords : true,
						rownumbers : true,
						rowNum : 20,
						rownumWidth : 20,
						resizable : true,
						loadonce : true,
//						toppager : true,
						fixed : true,
						pager : '#worderListPg',
						pginput : true,
						multiselect : false,
						cellsubmit : "clientArray",
						emptyrecords : true,
						ondblClickRow : function(id){
							btnsFnc.f5_upd(id);
						},
						colModel : [{
									name : 'wo_id',
									index : 'wo_id',
									label : WO_ID_TAG,
									width : WO_ID_CLM
								}, {
									name : 'crt_timestamp',
									index : 'crt_timestamp',
									label : "创建时间",
									width : TIMESTAMP_CLM
								}, {
									name : 'crt_usr',
									index : 'crt_usr',
									label : "制定人",
									width : USER_CLM
								}, {
									name : 'cus_id_fk',
									index : 'cus_id_fk',
									label : CUS_ID_TAG,
									width : CUS_ID_CLM
								}, {
									name : 'wo_stat',
									index : 'wo_stat',
									label : "状态",
									width : PRD_STAT_CLM
								}, {
									name : 'wo_cate',
									index : 'wo_cate',
									label : SO_CATE_TAG,
									width : PRD_STAT_CLM
								}, {
									name : 'pln_stb_timestamp',
									index : 'pln_stb_timestamp',
									label : "来料时间",
									width : TIMESTAMP_CLM
								}, {
									name : 'pln_cmp_timestamp',
									index : 'pln_cmp_timestamp',
									label : "交货时间",
									width : TIMESTAMP_CLM
								}, {
									name : 'mtrl_prod_id_fk',
									index : 'mtrl_prod_id_fk',
									label : "来料型号",
									width : MTRL_PROD_ID_CLM
								}, {
									name : 'rsv_pln_prd_qty',
									index : 'rsv_pln_prd_qty',
									label : "计来料",
									width : QTY_CLM
								}, {
									name : 'pln_prd_qty',
									index : 'pln_prd_qty',
									label : "实来料",
									width : QTY_CLM
								},  {
									name : 'so_id',
									index : 'so_id',
									label : SO_ID_TAG,
									width : WO_ID_CLM
								},{
									name : 'evt_usr',
									index : 'evt_usr',
									label : SO_EVT_USR,
									width : USER_CLM
								}, 
								{
									name : 'plant_id',
									index : 'pant_id',
									label : "产品类型",
									width : MTRL_PROD_ID_CLM
								},{
									name : 'evt_timestamp',
									index : 'evt_timestamp',
									label : SO_EVT_TIMESTAMP,
									width : TIMESTAMP_CLM
								}, {
									name : 'wo_dsc',
									index : 'wo_dsc',
									label : SO_DSC_TAG,
									width : WO_DSC_CLM
								}, {
									name : 'so_id',
									index : 'so_id',
									label : "订单号码",
									width : 10,
									hidden : true
								}, {
									name : 'vcr_flg',
									index : 'vcr_flg',
									label : "VCR标识",
									width : 10,
									hidden : true
								}, {
									name : 'swh_cnt_flg',
									index : 'swh_cnt_flg',
									label : "站点绑定",
									width : 10,
									hidden : true
								}, {
									name : 'rsv_pln_prd_qty',
									index : 'rsv_pln_prd_qty',
									label : "工单计划数量",
									width : 10,
									hidden : true
								}, {
									name : 'pln_stb_timestamp',
									index : 'pln_stb_timestamp',
									label : "订单预计开始日期",
									width : 10,
									hidden : true
								}, {
									name : 'pln_cmp_timestamp',
									index : 'pln_cmp_timestamp',
									label : "工单预计结束时间",
									width : 10,
									hidden : true
								}, {
									name : 'ope_eff_flg',
									index : 'ope_eff_flg',
									label : "重工计数",
									width : 10,
									hidden : true
								},{
									name : 'mainT',
									index : 'mainT',
									label : "主标签title",
									width : 10,
									hidden : true
								},{
									name : 'shtT',
									index : 'shtT',
									label : "id信息表title",
									width : 10,
									hidden : true
								},{
									name : 'jgyq',
									index : 'jgyq',
									label : "加工要求",
									width : 10,
									hidden : true
								},{
									name : 'fhdir',
									index : 'fhdir',
									label : "发货地址",
									width : 10,
									hidden : true
								}]
					})
		}
	};
	var btnsFnc = {
		f1_query : function() {
			var cuId, mtrlId, woId, woCate, iary, inObj, outObj,plant_id;
            cusId = domObj.$cusIdSel.val();
            mtrlId = domObj.$mtrlProdIdSel.val();
			woId = domObj.$woIdTxt.val().trim();
			woCate = domObj.$woCateSel.val();
			plant_id = domObj.$plantSel.val();

			iary = {};
            if (cusId) {
            	iary.cus_id = cusId;
            }  
            if (mtrlId){
            	iary.mtrl_prod_id_fk = mtrlId;
            }
			if (woId) {
				iary.wo_id = woId;
			}
			if (woCate) {
				iary.wo_cate = woCate;
			}

			inObj = {
				trx_id : 'XPAPLYWO',
				action_flg : 'Q',
				tbl_cnt : 1,
				iary : iary
			};

			outObj = comTrxSubSendPostJson(inObj);

			if (outObj.rtn_code == VAL.NORMAL) {
				setGridInfo(outObj.oary, "#worderListGrd", true);
				$("#woIdTxt").attr(VAL.ENABLED_ATTR);
			}
		},
		f4_del : function() {
			var delRowIds, rowCnt, iary, i, inObj, outObj;
			var delRowID = $("#worderListGrd").jqGrid("getGridParam", "selrow");
			if (!delRowID) {
				showErrorDialog("001", "请选择需要删除的行");
				return false;
			}
			var iary = new Array();
			rowData = $("#worderListGrd").jqGrid("getRowData", delRowID);
			iary.push({
				wo_id : rowData.wo_id
			})
//			delRowIds = domObj.grids.$worderListGrd.jqGrid("getGridParam",
//					"selrow");
//			rowCnt = delRowIds.length;
//			if (rowCnt <= 0) {
//				showErrorDialog("", "请选择需要删除的行");
//				return false;
//			}
//			iary = [];
//			for (i = 0; i < rowCnt; i++) {
//				rowData = domObj.grids.$worderListGrd.jqGrid("getRowData",
//						delRowIds[i]);
//				iary.push({
//							wo_id : rowData.wo_id
//						})
//			}
			domObj.buttons.$f4_del_btn.showCallBackWarnningDialog({
						errMsg : "删除选中的工单，是否继续?",
						callbackFn : function(data) {
							if (data.result == true) {
								inObj = {
									trx_id : 'XPAPLYWO',
									action_flg : 'D',
									tbl_cnt : 1,
									iary : iary
								};

								outObj = comTrxSubSendPostJson(inObj);
								if (outObj.rtn_code == VAL.NORMAL) {
//									for (i = 0; i < rowCnt; i++) {
										domObj.grids.$worderListGrd.jqGrid(
												"delRowData", delRowID);
//									}
									showSuccessDialog("工单删除成功");
								}
							}
						}
					});
		},
		f8_add : function() {
			var woId, woCate, woDsc,  cusId, mtrlProdId, rsvPlnPrdQty, newRowId, plnStbDate, plnStbTime, 
			    plnStbTimestamp, plnCompDate, plnCompTime, plnCompTimestamp,mainT,shtT,jgyq,fhdir,plantId;

			woId = domObj.$woIdTxt.val();
			woCate = domObj.$woCateSel.val();
			woDsc = domObj.$woDscTxt.val();
			mainT = domObj.$mainTitleTxt.val();
			shtT = domObj.$shtidTitleTxt.val();
			jgyq = domObj.$jgyqSel.val();
			fhdir = domObj.$fhSel.val();
			plantId=$("#plantSel").val();
			if(jgyq == null){
				jgyq = "";
			}
			if(fhdir == null){
				fhdir = "";
			}
			mtrlProdId = baseFnc.returnSpace(domObj.$mtrlProdIdSel.val());
			cusId = domObj.$cusIdSel.val();
			rsvPlnPrdQty = domObj.$rsvPlnPrdQtyTxt.val().trim();

			if (!woId) {
				showErrorDialog("", "内部订单号不能为空");
				return false;
			}
			if (!woCate) {
				showErrorDialog("", "订单类别不能为空,请选择");
				return false;
			}
			if (!rsvPlnPrdQty) {
				showErrorDialog("", "计划来料不能为空");
				return false;
			}
			if (!mtrlProdId) {
				showErrorDialog("", "来料型号不能为空,请选择");
				return false;
			}
			if (!cusId) {
				showErrorDialog("", "客户ID不能为空,请选择");
				return false;
			}
			plnStbDate = domObj.$plnStbDatepicker.data("datetimepicker")
					.getLocalDate().format('yyyy-MM-dd');
			plnStbTime = domObj.$plnStbTimepicker.data("datetimepicker")
					.getLocalDate().format('hh:mm:ss');
			plnStbTimestamp = plnStbDate + " " + plnStbTime;

			plnCompDate = domObj.$plnCompDatepicker.data("datetimepicker")
					.getLocalDate().format('yyyy-MM-dd');
			plnCompTime = domObj.$plnCompTimepicker.data("datetimepicker")
					.getLocalDate().format('hh:mm:ss');
			plnCompTimestamp = plnCompDate + " " + plnCompTime;

			if (!plnStbTimestamp) {
				showErrorDialog("", "请选择来料时间");
				return false;
			}
			if (!plnCompTimestamp) {
				showErrorDialog("", "请选择交货时间");
				return false;
			}
			if (baseFnc.convertTimestamp2Date(plnStbTimestamp) > baseFnc
					.convertTimestamp2Date(plnCompTimestamp)) {
				showErrorDialog("", "来料时间不能在交货时间之后");
				return false;
			}

			iary = {
				wo_id : woId,
				wo_cate : woCate,
				wo_dsc : woDsc,
				mainT:mainT,
				shtT:shtT,
				jgyq:jgyq,
				fhdir:fhdir,
				cus_id : cusId,
				mtrl_prod_id_fk : mtrlProdId,
				pln_stb_timestamp : plnStbTimestamp,
				pln_cmp_timestamp : plnCompTimestamp,
				rsv_pln_prd_qty : rsvPlnPrdQty,
				plant_id: domObj.$plantSel.val(),
				evt_user : $("#userId").text()
			};
			inObj = {
				trx_id : 'XPAPLYWO',
				action_flg : 'A',
				tbl_cnt : 1,
				iary : iary
			};

			var outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code == VAL.NORMAL) {
				newRowId = getGridNewRowID("#worderListGrd")
				domObj.grids.$worderListGrd.jqGrid("addRowData", newRowId,
						outObj.oary);
				showSuccessDialog("工单新建成功");
			}
		},
		f5_upd : function(rowId) {
			var selRowIds, selRowId, rowData, rowId, plnStbDate, plnStbTime, plnStbTimestamp, plnStbDateDate, plnCmpDate, pnlCmpTime, plnCmpTimestamp, plnCmpDateDate;

//			selRowIds = domObj.grids.$worderListGrd.jqGrid("getGridParam",
//					"selarrrow");
//			if (selRowIds.length <= 0 && typeof(rowId) === "undefined") {
//				showErrorDialog("", "请在工单列表中选择需要更新的订单");
//				return false;
//			}
//			if (selRowIds.length > 1) {
//				showErrorDialog("", "每次只能更新一个内部订单,不能选多个");
//				return false;
//			}
			selRowId = rowId;

			rowData = domObj.grids.$worderListGrd
					.jqGrid("getRowData", selRowId);
			if (rowData.wo_stat === "关闭") {
				showErrorDialog("", "内部订单已经关闭,不能对其操作，只能先打开此内部订单");
				return false;
			}
			SelectDom.setSelect(domObj.$cusIdSel, rowData.cus_id_fk);
			SelectDom.setSelect(domObj.$mtrlProdIdSel, baseFnc.replaceSpace(rowData.mtrl_prod_id_fk),rowData.mtrl_prod_id_fk);

			domObj.$woIdTxt.val(rowData.wo_id);
			domObj.$woDscTxt.val(rowData.wo_dsc);
			domObj.$mainTitleTxt.val(rowData.mainT);
			domObj.$shtidTitleTxt.val(rowData.shtT);
			domObj.$jgyqSel.val(rowData.jgyq);
			domObj.$fhSel.val(rowData.fhdir);
			if(rowData.plant_id =="G5"||rowData.plant_id=="G6"){
				SelectDom.setSelect(domObj.$plantSel, rowData.plant_id);
			}
			/************************ set option selected by text in select *****************/ 
			
				
			
			SelectDom.setSelect(domObj.$woCateSel, rowData.wo_cate);
			//domObj.$woCateSel.val(rowData.wo_cate);
			domObj.$woIdTxt.attr(VAL.DISABLED_ATTR);
			if(rowData.so_id){
				domObj.$cusIdSel.attr(VAL.DISABLED_ATTR);
				domObj.$mtrlProdIdSel.attr(VAL.DISABLED_ATTR);
			}else{
				domObj.$cusIdSel.removeAttr("disabled");
				domObj.$mtrlProdIdSel.removeAttr("disabled");
				
			}
			domObj.$rsvPlnPrdQtyTxt.val(rowData.rsv_pln_prd_qty);

			plnStbDateDate = baseFnc
					.convertTimestamp2Date(rowData.pln_stb_timestamp);
			plnCmpDateDate = baseFnc
					.convertTimestamp2Date(rowData.pln_cmp_timestamp);

			domObj.$plnStbDatepicker.data("datetimepicker")
					.setLocalDate(plnStbDateDate);
			domObj.$plnStbTimepicker.data("datetimepicker")
					.setLocalDate(plnStbDateDate);

			domObj.$plnCompDatepicker.data("datetimepicker")
					.setLocalDate(plnCmpDateDate);
			domObj.$plnCompTimepicker.data("datetimepicker")
					.setLocalDate(plnCmpDateDate);

			$("button").hide();
			domObj.buttons.$commit_upd_btn.show();
			domObj.buttons.$cancel_upd_btn.show();

		},
		commit_upd : function() {

			var woId, woCate, woDsc, mdlId, cusId, rsvPlnPrdQty, plnStbDate, plnStbTime, plnStbTimestamp, 
			    plnCompDate, plnCompTime, plnCompTimestamp,mainT,shtT,jgyq,fhdir,plant_id;

			woId = domObj.$woIdTxt.val();
			woCate = domObj.$woCateSel.val();
			woDsc = domObj.$woDscTxt.val();
			mainT = domObj.$mainTitleTxt.val();
			shtT = domObj.$shtidTitleTxt.val();
			jgyq = domObj.$jgyqSel.val();
			fhdir = domObj.$fhSel.val();
			plant_id= domObj.$plantSel.val();
			if(jgyq == null){
				jgyq = "";
			}
			if(fhdir == null){
				fhdir = "";
			}
			rsvPlnPrdQty = domObj.$rsvPlnPrdQtyTxt.val().trim();
			mtrlProdId = baseFnc.returnSpace(domObj.$mtrlProdIdSel.val());
			cusId = domObj.$cusIdSel.val();
			if (!woId) {
				showErrorDialog("", "工单ID不能为空，请选择需要编辑的行");
				return false;
			}
			if (!woCate) {
				showErrorDialog("", "工单类型不能为空,请选择");
				return false;
			}
			if (!mtrlProdId) {
				showErrorDialog("", "产品名称不能为空,请选择");
				return false;
			}
			if (!cusId) {
				showErrorDialog("", "客户ID不能为空,请选择");
				return false;
			}
			if (!rsvPlnPrdQty) {
				showErrorDialog("", "计划来料不能为空,请选择");
				return false;
			}
			plnStbDate = domObj.$plnStbDatepicker.data("datetimepicker")
					.getLocalDate().format('yyyy-MM-dd');
			plnStbTime = domObj.$plnStbTimepicker.data("datetimepicker")
					.getLocalDate().format('hh:mm:ss');
			plnStbTimestamp = plnStbDate + " " + plnStbTime;

			plnCompDate = domObj.$plnCompDatepicker.data("datetimepicker")
					.getLocalDate().format('yyyy-MM-dd');
			plnCompTime = domObj.$plnCompTimepicker.data("datetimepicker")
					.getLocalDate().format('hh:mm:ss');
			plnCompTimestamp = plnCompDate + " " + plnCompTime;

			if (!plnStbTimestamp) {
				showErrorDialog("", "请选择来料时间");
				return false;
			}
			if (!plnCompTimestamp) {
				showErrorDialog("", "请选择交货时间");
				return false;
			}
			if (baseFnc.convertTimestamp2Date(plnStbTimestamp) > baseFnc
					.convertTimestamp2Date(plnCompTimestamp)) {
				showErrorDialog("", "来料时间不能在交货时间之后");
				return false;
			}

			iary = {
				wo_id : woId,
				wo_cate : woCate,
				wo_dsc : woDsc,
				mainT:mainT,
				shtT:shtT,
				jgyq:jgyq,
				fhdir:fhdir,
				cus_id : cusId,
				mtrl_prod_id_fk : mtrlProdId,
				pln_stb_timestamp : plnStbTimestamp,
				pln_cmp_timestamp : plnCompTimestamp,
				rsv_pln_prd_qty : rsvPlnPrdQty,
				plant_id : plant_id,
				evt_user : $("#userId").text()
			};
			inObj = {
				trx_id : 'XPAPLYWO',
				action_flg : 'U',
				tbl_cnt : 1,
				iary : iary
			};

			var outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code == VAL.NORMAL) {
				showSuccessDialog("工单修改成功");

				btnsFnc.f1_query();
				domObj.$woIdTxt.attr(VAL.ENABLED_ATTR);
				$("button").show();
				domObj.buttons.$commit_upd_btn.hide();
				domObj.buttons.$cancel_upd_btn.hide();
				domObj.grids.$worderListGrd.resetSelection();
			}

		},
		cancel_upd : function() {
//			btnsFnc.f10_clear();
			domObj.$woIdTxt.removeAttr("disabled");
			domObj.$cusIdSel.removeAttr("disabled");
			domObj.$mtrlProdIdSel.removeAttr("disabled");
			$("button").show();
			domObj.buttons.$commit_upd_btn.hide();
			domObj.buttons.$cancel_upd_btn.hide();
		},
		f6_close : function() {
			var rowIds, rowCnt, rowData, i, inObj, iary, outObj, useId,closeRowID;

			usrId = $("#userId").text();
			closeRowID = domObj.grids.$worderListGrd.jqGrid("getGridParam", "selrow");
			if (!closeRowID) {
				showErrorDialog("001", "选择需要关闭的内部订单");
				return false;
			}
			iary = [];
			rowData = domObj.grids.$worderListGrd.jqGrid("getRowData", closeRowID);
			iary.push({
				wo_id : rowData.wo_id,
				evt_user : usrId
			})
//			rowIds = domObj.grids.$worderListGrd.jqGrid("getGridParam",
//					"selarrrow").concat();
//			rowCnt = rowIds.length;
//			if (rowCnt <= 0) {
//				showErrorDialog("", "选择需要关闭的内部订单");
//				return false;
//			}
//			iary = [];
//			for (i = 0; i < rowCnt; i++) {
//				rowData = domObj.grids.$worderListGrd.jqGrid("getRowData",
//						rowIds[i]);
//				if (rowData.wo_stat == "关闭") {
//					showErrorDialog("", "此订单已经被关闭,不能对其操作,请重新打开此订单");
//					return false;
//				}
//				iary.push({
//							wo_id : rowData.wo_id,
//							evt_user : usrId
//						});
//			}

			domObj.buttons.$f6_close_btn.showCallBackWarnningDialog({
						errMsg : "关闭后订单[" + rowData.wo_id + "]将无法继续使用,是否继续?",
						callbackFn : function(data) {
							if (data.result == true) {
								inTrx = {
									trx_id : "XPAPLYWO",
									action_flg : "X",
									iary : iary
								};
								outObj = comTrxSubSendPostJson(inTrx);
								if (outObj.rtn_code == VAL.NORMAL) {
//									for (i = 0; i < rowCnt; i++) {
										domObj.grids.$worderListGrd.jqGrid(
												"setRowData", closeRowID,
												outObj.oary);
//									}
									showSuccessDialog("内部订单关闭成功");
									domObj.grids.$worderListGrd.resetSelection();
								}

							}
						}
					})
		},
		f7_open : function() {
			var rowIds, rowCnt, rowData, i, inObj, iary, outObj, useId,openRowID;

			usrId = $("#userId").text();
			openRowID = domObj.grids.$worderListGrd.jqGrid("getGridParam", "selrow");
			if (!openRowID) {
				showErrorDialog("001", "选择需要打开的内部订单");
				return false;
			}
			iary = [];
			rowData = domObj.grids.$worderListGrd.jqGrid("getRowData", openRowID);
			iary.push({
				wo_id : rowData.wo_id,
				evt_user : usrId
			})
//			rowIds = domObj.grids.$worderListGrd.jqGrid("getGridParam",
//					"selarrrow").concat();
//			rowCnt = rowIds.length;
//			if (rowCnt <= 0) {
//				showErrorDialog("", "选择需要打开的内部订单");
//				return false;
//			}
//			iary = [];
//			for (i = 0; i < rowCnt; i++) {
//				rowData = domObj.grids.$worderListGrd.jqGrid("getRowData",
//						rowIds[i]);
//				if (rowData.wo_stat !== "关闭") {
//					showErrorDialog("", "此订单没有被关闭,不能打开");
//					return false;
//				}
//				iary.push({
//							wo_id : rowData.wo_id,
//							evt_user : usrId
//						});
//			}

			inTrx = {
				trx_id : "XPAPLYWO",
				action_flg : "Y",
				iary : iary
			};
			outObj = comTrxSubSendPostJson(inTrx);
			if (outObj.rtn_code == VAL.NORMAL) {
//				for (i = 0; i < rowCnt; i++) {
					domObj.grids.$worderListGrd.jqGrid("setRowData", openRowID,
							outObj.oary);
//				}
				showSuccessDialog("内部订单打开成功");
			}
		},
		f10_clear : function() {
			$("input[type='text']").val("");
			SelectDom.setSelect($("select"), "", "");
			domObj.$woIdTxt.removeAttr("disabled");
			domObj.$cusIdSel.removeAttr("disabled");
			domObj.$mtrlProdIdSel.removeAttr("disabled");
			domObj.grids.$worderListGrd.jqGrid("clearGridData");
		},
		f11_export : function() {
			generateExcel("#worderListGrd");
		},
        f11_bind_func : function(){
            var woDialog = new WoDialog();
            woDialog.showDialogFnc();
        }
	};
	var pageFnc = {
		initData : function() {
			domObj.$woIdTxt.removeAttr("disabled");
			domObj.$cusIdSel.removeAttr("disabled");
			domObj.$mtrlProdIdSel.removeAttr("disabled");
			$("#0581").hide();
			$("#0582").hide();
			$("#0583").hide();
			controlsFunc.iniClean();
			controlsFunc.initGirdInfo();
			controlsFunc.initCusSel();
//			controlsFunc.initplantSel();
			controlsFunc.initMtrlSel();
			controlsFunc.iniDateTimePicker();
			controlsFunc.initWoCateSel();
			controlsFunc.initPlantSel();
		},
		initEvent : function() {
			domObj.$cusIdSel.change(controlsFunc.initMtrlSel);

			domObj.buttons.$f1_query_btn.click(btnsFnc.f1_query);

			domObj.buttons.$f4_del_btn.click(btnsFnc.f4_del);

			domObj.buttons.$f5_upd_btn.click(function(){
				var rowID = domObj.grids.$worderListGrd.jqGrid("getGridParam", "selrow");
				if (!rowID) {
					showErrorDialog("001", "选择需要修改的内部订单");
					return false;
				}
				btnsFnc.f5_upd(rowID);
			});

			domObj.buttons.$cancel_upd_btn.click(btnsFnc.cancel_upd);

			domObj.buttons.$commit_upd_btn.click(btnsFnc.commit_upd);

			domObj.buttons.$f6_close_btn.click(btnsFnc.f6_close);

			domObj.buttons.$f7_open_btn.click(btnsFnc.f7_open);

			domObj.buttons.$f8_add_btn.click(btnsFnc.f8_add);

			domObj.buttons.$f10_clear_btn.click(btnsFnc.f10_clear);

			domObj.buttons.$f11_export_btn.click(btnsFnc.f11_export);
			
			domObj.buttons.$f11_bind_btn.click(btnsFnc.f11_bind_func);
		}
	};
	
    var WoDialog = function(){
        this.showDialogFnc  = function(){
            var iary,
                inTrx,
                outTrx,
                rowId,
                rowData;

            rowId = domObj.grids.$worderListGrd.jqGrid("getGridParam", "selrow");
            if (!rowId) {
				showErrorDialog("001", "选择需要绑定商务订单的内部订单");
				return false;
			}
            rowData = domObj.grids.$worderListGrd.jqGrid("getRowData", rowId);
            iary = {
                wo_id : rowData['wo_id']
            };
            inTrx = {
                trx_id     : "XPAPLYWO" ,
                action_flg : 'Q'        ,
                tbl_cnt    :  1         ,
                iary       :  iary
            };
            outTrx = comTrxSubSendPostJson(inTrx);
            if (outTrx.rtn_code==VAL.NORMAL){
                if( outTrx.tbl_cnt<=0){
                    showErrorDialog("003","此内部订单还没有被创建，请先创建");
                    return false;
                }
                if( outTrx.oary.so_id!=undefined){
                    showErrorDialog("003","此内部订单已经绑定过商务订单,不可重复绑定");
                    return false;
                }
            }

            this.initialDialogFnc(rowData['wo_id']);

            iary = {
                mtrl_prod_id : baseFnc.returnSpace(outTrx.oary.mtrl_prod_id_fk),
                cus_id       : baseFnc.returnSpace(outTrx.oary.cus_id_fk),
                wo_cate      : baseFnc.returnSpace(outTrx.oary.wo_cate)
            };
            inTrx = {
                trx_id     : "XPAPLYSO",
                action_flg : "S",
                iary       : iary
            };
            outTrx = comTrxSubSendPostJson(inTrx);
            if(outTrx.rtn_code==VAL.NORMAL){
                setGridInfo(outTrx.oary,"#bindWoDialog_woGrd");
            }
        };
        this.bindWoClickFnc = function(){
            var selectRowID,
                iary = new Array(),
                rowData,
                inTrx,
                outTrx,
                woId;

            selectRowID = $("#bindWoDialog_woGrd").jqGrid("getGridParam","selrow");
            if( selectRowID == null || selectRowID ==""){
                showErrorDialog("003","选择需要绑定工单的订单");
                return false;
            }
            woId = $("#bindWODialog_worderIDTxt").val();
            iary={
                wo_id    : woId,
                so_id    : $("#bindWoDialog_woGrd").jqGrid("getRowData",selectRowID).wo_id,
                evt_user : VAL.EVT_USR
            };
            inTrx = {
                trx_id     : "XPAPLYSO" ,
                action_flg : 'B'        ,
                tbl_cnt    :  1         ,
                iary       :  iary
            };
            outTrx = comTrxSubSendPostJson(inTrx);
            if(outTrx.rtn_code=="0000000"){
                showSuccessDialog("订单绑定成功");
                $('#bindWODialog').modal("hide");
                btnsFnc.f1_query();
            }
        };
        this.initialDialogFnc = function(wo_id){
            $('#bindWODialog').modal({
                backdrop:true,
                keyboard:false,
                show:false
            });
            $('#bindWODialog').unbind('shown.bs.modal');
            $("#bindWODialog_bindWOBtn").unbind('click');

            $('#bindWODialog').modal("show");
            $("#bindWODialog_bindWOBtn").bind('click',this.bindWoClickFnc);
            $("#bindWODialog_worderIDTxt").attr({"disabled":true});
            $("#bindWODialog_worderIDTxt").val(wo_id);
            $("#bindWoDialog_woGrd").jqGrid({
                url:"",
                datatype:"local",
                mtype:"POST",
//                autowidth:true,//宽度根据父元素自适应
                height:280,
                width:1160,
                shrinkToFit:true,
                scroll:true,
                viewrecords:true,
                rownumbers  :true ,
                rowNum:20,
                rownumWidth : 20,
                resizable : true,
                loadonce:true,
                toppager: true ,
                fixed:true,
                pager : '#bindWoDialog_woPg',
                pginput :true,
                fixed: true,
                cellEdit : true,
                cellsubmit : "clientArray",
                emptyrecords :true ,
                colModel:  [
                    {name: 'wo_id'            , index: 'wo_id'           , label: SO_ID_TAG,          width: WO_ID_CLM},
                    {name: 'wo_cate'          , index: 'wo_cate'         , label: SO_CATE_TAG,        width: WO_CATE_CLM },
                    {name: 'mdl_id'           , index: 'mdl_id'          , label: MDL_ID_TAG,         width: MDL_ID_CLM},
                    {name: 'pln_prd_qty'      , index: 'pln_prd_qty'     , label: EXPECT_COUNT_TAG ,    width: PATH_ID_FK_CLM},
                    {name: 'path_id'          , index: 'path_id'         , label: PATH_ID_TAG,         width: MDL_ID_CLM},
                    {name: 'pln_stb_date'     , index: 'pln_stb_date'    , label: PLAN_FROM_DATE_TAG,   width: PLN_STB_DATE_CLM},
                    {name: 'pln_cmp_date'     , index: 'pln_cmp_date'    , label: PLN_CMP_DATE_TAG,     width: PLN_CMP_DATE_CLM},
                    {name: 'wo_note'          , index: 'wo_note'         , label: ORDER_INSTRUCTION_TAG,    width: WO_NOTE_CLM},
                    {name: 'cus_info_snd'            , index: 'cus_info_snd'           , label: "客户工单号",        width: SO_ID_CLM},
                    {name: 'cus_info_fst'            , index: 'cus_info_fst'           , label: "委外工单号",        width: SO_ID_CLM},
                    {name: 'evt_usr'          , index: 'wo_dsc'          , label: EVT_USR,         width: WO_DSC_CLM},
                    {name: 'plant_id'          , index: 'plant_id'          , label:"产品类型",         width: WO_DSC_CLM},
                    {name: 'evt_timestamp'    , index: 'evt_timestamp'   , label: EVT_TIMESTAMP,         width: EVT_TIMESTAMP_CLM}
                ]
            });
        }
    }
	pageFnc.initData();
	pageFnc.initEvent();
	function resizeFnc(){                                                                      
	  	var offsetBottom, divWidth;                                                              
	
	  	divWidth = domObj.grids.$worderListDiv.width();                                   
		offsetBottom = domObj.$window.height() - domObj.grids.$worderListDiv.offset().top;
		domObj.grids.$worderListDiv.height(offsetBottom * 0.95);                          
		domObj.grids.$worderListGrd.setGridWidth(divWidth * 0.99);                   
		domObj.grids.$worderListGrd.setGridHeight(offsetBottom * 0.95 - 51);   
		$("#legendDiv").width(divWidth*0.99);
		domObj.$bindWoDialog_woGrd.setGridWidth(divWidth * 0.85);
    };                                                                                         
    resizeFnc();                                                                               
    domObj.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
    });
})