/*******************************************************************************
 * NO.01 Lin.Xin add fm_mdl_id_fk,cut_ NO.02 Lin.Xin Add ope_eff_flg
 ******************************************************************************/
$(document).ready(function() {
	// 全局变量
	$("form").submit(function() {
		return false;
	});
	var GlobalBean = {
		autoGenSoFlg : false
	};
	var VAL = {
		NORMAL : "0000000",
		DISABLE_ATTR : {
			"disabled" : true
		},
		ENABLE_ATTR : {
			"disabled" : false
		},
		WOAB:{}
	};
	function getWoCateByChin(chinTxt){
		var prop,val;
		for(prop in VAL.WOAB){
			if(VAL.WOAB[prop] === chinTxt){
				return prop;
			}
		}
		return null;
	}
	var domObjs = {
		$window : $(window),
		$orderIDTxt : $("#orderIDTxt"),
		$cusIDSel : $("#cusIDSel"),
		$woCateSel : $("#woCateSel"),
		$mtrlIDSel : $("#mtrlIDSel"),
		$pathChooseSel : $("#pathChooseSel"),
		$thMdlIdSel : $("#thMdlIdSel"),
		$fmMdlIDSel : $("#fmMdlIDSel"),
		$cutMdlIDSel : $("#cutMdlIDSel"),
		$opeEffFlgChk : $("#opeEffFlgChk"),
		$vcrFlgChk : $("#vcrFlgChk"),
		$plnStbDateTxt : $("#plnStbDateTxt"),
		$plnCmpDateTxt : $("#plnCmpDateTxt"),
		$plnPrdQtyTxt : $("#plnPrdQtyTxt"),
		$woNoteTxt : $("#woNoteTxt"),
		$fromThicknessTxt : $("#fromThicknessTxt"),
		$toThicknessTxt : $("#toThicknessTxt"),
		$mtrlPartSel : $("#mtrlPartSel"),
		$cusInfoFstTxt : $("#cusInfoFstTxt"),
		$cusInfoSndTxt : $("#cusInfoSndTxt"),
		$tThicknessTxt : $("#tThicknessTxt"),
		$cThicknessTxt : $("#cThicknessTxt"),
		$manualTxt : $("#manualTxt"),
		$cusMtrlTxt : $("#cusMtrlTxt"),
		$glassFromTxt : $("#glassFromTxt"),
		grids : {
			$orderListDiv : $("#orderListDiv"),
			$orderListGrd : $("#orderListGrd"),
			$orderDetailListDiv : $("#orderDetailListDiv"),
			$orderDetailListGrd : $("#orderDetailListGrd"),
			$bindWoDialog_woGrd : $("#bindWoDialog_woGrd"),
			$cusSoGrd : $("#cusSoGrd")
		}
	};
	var baseFnc = {
		replaceSpace:function(str){
			if(!str){
				return str;
			}
			return str.replace(/[ ]/g,"@");	
		},
		returnSpace:function(str){
			if(!str){
				return str;
			}
			return str.replace(/[@]/g," ");
		},
		getMdlId:function(){
			var mdlId = baseFnc.returnSpace(domObjs.$thMdlIdSel.val().trim());
			if(!mdlId){
				mdlId = baseFnc.returnSpace(domObjs.$fmMdlIDSel.val().trim()); 
			}
			return mdlId;
		}
	};
	$("#plnStbDateTxt, #plnCmpDateTxt").datepicker({
		defaultDate : "",
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1
	});
	function setSelByCodeOutTrx(outObj, DocumentObj) {
		var dataCnt = comParseInt(outObj.tbl_cnt);
		if (dataCnt == 1) {
			DocumentObj.append("<option value='Value'>" + outObj.oary.data_id + "</option>");
		} else {
			for ( var i = 0; i < dataCnt; i++) {
				DocumentObj.append("<option value='Value'>" + outObj.oary[i].data_id + "</option>");
			}
		}
		DocumentObj.select2({
	    	theme : "bootstrap"
	    });
	}
	function comSetSelect($selecter, val, txt) {
		$selecter.empty();
		$selecter.append("<option value=''></option>");
		$selecter.append("<option value=" + val + ">" + txt + "</option>");
		$selecter.select2({
	    	theme : "bootstrap"
	    });
	}
	function comAddSelect($selecter, val, txt) {
		$selecter.append("<option value=" + val + ">" + txt + "</option>");
	}
	function cusIDChangeFnc() {
		var inObj, outObj, iary, i, tblCnt, cusId;
		cusId = $("#cusIDSel").val();
		if (!cusId) {
			return false;
		}
		iary = {
			data_cate : "CSMT",
			data_item : cusId
		};
		inObj = {
			trx_id : "XPLSTDAT",
			action_flg : "H",
			iary : iary
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code === VAL.NORMAL) {

			SelectDom.initWithSpace(domObjs.$mtrlIDSel);
			tblCnt = outObj.tbl_cnt;
			for (i = 0; i < tblCnt; i++) {
				oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
				SelectDom.addSelect(domObjs.$mtrlIDSel, baseFnc.replaceSpace(oary.ext_1),oary.ext_1);
			}
		}
		mtrlIDChangeFnc();
	}
	function mtrlIDChangeFnc() {
		var inObj, outObj, iary, oary, i, tblCnt, replacedMdlId,mdlId,mtrlId;
		mtrlId = baseFnc.returnSpace(domObjs.$mtrlIDSel.val());
		iary = {
			mtrl_prod_id_fk : mtrlId
		};
		inObj = {
			trx_id : 'XPBISBOM',
			action_flg : 'I',
			iary : iary
		};
		outObj = comTrxSubSendPostJson(inObj);
		tblCnt = outObj.tbl_cnt;
		SelectDom.initWithSpace($("#thMdlIdSel,#fmMdlIDSel,#cutMdlIDSel"));
		for (i = 0; i < tblCnt; i++) {
			oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
			mdlId = oary.mdl_id_fk;
			replacedMdlId = baseFnc.replaceSpace(mdlId);
			if(!SelectDom.hasValue(domObjs.$thMdlIdSel, replacedMdlId)){
				SelectDom.addSelect(domObjs.$thMdlIdSel, replacedMdlId, mdlId);
			}
			if(!SelectDom.hasValue(domObjs.$fmMdlIDSel, replacedMdlId)){
				SelectDom.addSelect(domObjs.$fmMdlIDSel, replacedMdlId, mdlId);
			}
			if(!SelectDom.hasValue(domObjs.$cutMdlIDSel, replacedMdlId)){
				SelectDom.addSelect(domObjs.$cutMdlIDSel, replacedMdlId, mdlId);
			}
		}
	}

	function setThickness() {
		var inObj, outObj,mdlId;
		mdlId = baseFnc.getMdlId();
		if (!mdlId) {
			domObjs.$fromThicknessTxt.val("");
			domObjs.$toThicknessTxt.val("");
			return false;
		}
		inObj = {
			trx_id : "XPBMDLDF",
			action_flg : "Q",
			iary : {
				mdl_id : mdlId
			}
		};
		outObj = comTrxSubSendPostJson(inObj);
		if(outObj.rtn_code === VAL.NORMAL && outObj.oary){
			domObjs.$fromThicknessTxt.val(outObj.oary.from_thickness);
			domObjs.$toThicknessTxt.val(outObj.oary.to_thickness);
		}
	}
	function setPath(){
		var inObj,outObj,iaryA,oary,tblCnt,i,mdlCate;
		mdlCate = ( domObjs.$thMdlIdSel.val() ? "A":"") + (domObjs.$fmMdlIDSel.val() ? "B":"") + (domObjs.$cutMdlIDSel.val() ? "C":"");
		domObjs.$pathChooseSel.empty();
		SelectDom.initWithSpace(domObjs.$pathChooseSel);
		if(!mdlCate){
			return false;
		}
		iaryA = {
			mdl_cate    : mdlCate 	,
			max_swh_cnt : 2         ,
			path_cate   : "MAIN"
		};
		inObj = {
			trx_id     : "XPBISPTH",
			action_flg : "Q",
			iaryA      : iaryA
		};
		outObj = comTrxSubSendPostJson(inObj);
		if(outObj.rtn_code === "0000000"){
			tblCnt = outObj.tbl_cnt_a;
			for(i=0;i<tblCnt;i++){
				oary = tblCnt > 1 ?  outObj.oaryA[i] : outObj.oaryA;
				SelectDom.addSelect(domObjs.$pathChooseSel,oary.path_id+"@"+oary.path_ver,oary.path_id);
			}
		}
	}
	domObjs.$mtrlIDSel.on("change", mtrlIDChangeFnc);
	domObjs.$cusIDSel.on("change", cusIDChangeFnc);
	$("#thMdlIdSel,#fmMdlIDSel,#cutMdlIDSel").on("change",setPath);
	$("#thMdlIdSel,#fmMdlIDSel,#cutMdlIDSel").on("change",setThickness);

	function getMtrlID() {
		var iary, inObj, outObj, tblCnt, i, oary;
		inObj = {
			trx_id : 'XPBISMTR',
			action_flg : 'Q',
			iary : {}
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code === VAL.NORMAL) {
			SelectDom.initWithSpace(domObjs.$mtrlIDSel);
			tblCnt = outObj.tbl_cnt;
			for (i = 0; i < tblCnt; i++) {
				oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
				SelectDom.addSelect(domObjs.$mtrlIDSel, baseFnc.replaceSpace(oary.mtrl_prod_id),oary.mtrl_prod_id);
			}
		}
	}
	function sendXplstdat(dataCate) {
		var inObj, outObj;
		inObj = {
			trx_id : 'XPLSTDAT',
			action_flg : 'Q',
			iary : {
				data_cate : dataCate
			}
		};
		outObj = comTrxSubSendPostJson(inObj);
		return outObj.rtn_code === VAL.NORMAL ? outObj : false;
	}
	function initCusIDSel() {
		var outObj, tblCnt, oary, i;
		outObj = sendXplstdat("CUSD");
		SelectDom.initWithSpace(domObjs.$cusIDSel);
		if (outObj) {
			tblCnt = outObj.tbl_cnt;
			for (i = 0; i < tblCnt; i++) {
				oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
				SelectDom.addSelect(domObjs.$cusIDSel, oary.data_item);
			}
		}
	}
	function initMtrlPartSel() {
		var outObj, tblCnt, oary, i;
		outObj = sendXplstdat("MTPT");
		SelectDom.initWithSpace(domObjs.$mtrlPartSel);
		if (outObj) {
			tblCnt = outObj.tbl_cnt;
			for (i = 0; i < tblCnt; i++) {
				oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
				SelectDom.addSelect(domObjs.$mtrlPartSel, oary.ext_1, oary.ext_1);
			}
		}
	}

	function initWoCate() {
		var outObj, tblCnt, oary, i;
		outObj = sendXplstdat("WOAB");
		SelectDom.initWithSpace(domObjs.$woCateSel);
		if (outObj) {
			tblCnt = outObj.tbl_cnt;
			for (i = 0; i < tblCnt; i++) {
				oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
				SelectDom.addSelect(domObjs.$woCateSel, oary.data_id, oary.data_desc);
			}
		}
	}

	var itemInfoCM = [ {
		name : 'crt_timestamp',
		index : 'crt_timestamp',
		label : ORDER_DATE_TAG,
		width : TIMESTAMP_CLM
	}, {
		name : 'wo_stat',
		index : 'wo_stat',
		label : "状态",
		width : WO_STAT_CLM
	}, {
		name : 'cus_id',
		index : 'cus_id',
		label : CUS_ID_TAG,
		width : CUS_ID_CLM
	}, {
		name : 'wo_id',
		index : 'wo_id',
		label : CUS_ORDER_ID_TAG,
		width : WO_ID_CLM
	}, {
		name : 'mtrl_prod_id',
		index : 'mtrl_prod_id',
		label : FROM_MTRL_ID_TAG,
		width : MTRL_PROD_ID_CLM
	}, {
		name : 'mtrl_part',
		index : 'mtrl_part',
		label : "材质",
		width : STAT_DSC_CLM
	}, {
		name : 'th_mdl_id_fk',
		index : 'th_mdl_id_fk',
		label : TO_MTRL_ID_TAG,
		width : MDL_ID_CLM
	}, {
		name : 'mdl_size',
		index : 'mdl_size',
		label : MDL_SIZE_TAG,
		width : MDL_SIZE_CLM
	}, {
		name : 'from_thickness',
		index : 'from_thickness',
		label : FROM_THICKNESS_TAG,
		width : QTY_CLM
	}, {
		name : 'to_thickness',
		index : 'to_thickness',
		label : TO_THICKNESS_TAG,
		width : QTY_CLM
	}, {
		name : 'fm_mdl_id_fk',
		index : 'fm_mdl_id_fk',
		label : FM_MDL_ID_TAG,
		width : MDL_ID_CLM
	}, {
		name : 'cut_mdl_id_fk',
		index : 'cut_mdl_id_fk',
		label : CUT_MDL_ID_TAG,
		width : MDL_ID_CLM
	}, {
		name : 'path_id',
		index : 'path_id',
		label : "工艺路线",
		width : PATH_ID_CLM
	}, {
		name : 'path_ver',
		index : 'path_ver',
		label : "版本",
		width : PATH_VER_CLM
	},{
		name : 'pln_cmp_date',
		index : 'pln_cmp_date',
		label : DELIVERY_DATE_TAG,
		width : DATE_CLM
	}, {
		name : 'mdl_dsc',
		index : 'mdl_dsc',
		label : MDL_DSC_TAG,
		width : MDL_DSC_CLM
	}, {
		name : 'layot_id_fk',
		index : 'layot_id_fk',
		label : LAYOUT_ID_TAG,
		width : LAYOT_ID_CLM
	},{
		name : 'pln_stb_date',
		index : 'pln_stb_date',
		label : FROM_DATE_TAG,
		width : DATE_CLM
	}, {
		name : 'pln_prd_qty',
		index : 'pln_prd_qty',
		label : EXPECT_COUNT_TAG,
		width : QTY_CLM
	}, {
		name : 'wo_note',
		index : 'wo_note',
		label : REMARK_TAG,
		width : WO_NOTE_CLM
	}, {
		name : 'wo_cate',
		index : 'wo_cate',
		label : "订单类别",
		width : WO_CATE_CLM
	},{
		name : 'evt_usr',
		index : 'evt_usr',
		label : EVT_USER_TAG,
		width : USER_CLM
	}, {
		name : 'bnd_wo',
		index : 'bnd_wo',
		label : '绑定工单',
		width : WO_ID_CLM
	},{
		name : 'so_id',
		index : 'so_id',
		label : '实际订单',
		width : WO_ID_CLM
	}, {
		name : 'ope_eff_flg',
		index : 'ope_eff_flg',
		label : "ope_eff_flg",
		width : OPE_EFF_FLG_CLM,
		hidden : true
	},{
		name : 'vcr_flg_fk',
		index : 'vcr_flg_fk',
		label : "vcr_flg_fk",
		width : OPE_EFF_FLG_CLM,
		hidden : true
	},{
		name : 'cus_info_fst',
		index : 'cus_info_fst',
		label : 'cus_info_fst',
		width : 100,
		hidden : true
	},{
		name : 'cus_info_snd',
		index : 'cus_info_snd',
		label : 'cus_info_snd',
		width : 100,
		hidden : true
	},{
		name : 'manual',
		index : 'manual',
		label : 'manual',
		width : 100,
		hidden : true
	},{
		name : 'cus_mtrl_id_fk',
		index : 'cus_mtrl_id_fk',
		label : 'cus_mtrl_id_fk',
		width : 100,
		hidden : true
	},{
		name : 'glass_from',
		index : 'glass_from',
		label : 'glass_from',
		width : 100,
		hidden : true
	},{
		name : 't_thickness',
		index : 't_thickness',
		label : 'T侧厚度',
		width : T_THICKNESS_CLM
	},{
		name : 'c_thickness',
		index : 'c_thickness',
		label : 'C侧厚度',
		width : C_THICKNESS_CLM
	}, {
		name : 'crt_usr',
		index : 'crt_usr',
		label : "制定人",
		width : USER_CLM
	}, {
		name : 'evt_timestamp',
		index : 'evt_timestamp',
		label : "制定时间",
		width : TIMESTAMP_CLM
	}];
	domObjs.grids.$orderListGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 300,
		width : 680,
		shrinkToFit : false,
		scroll : true,
		rownumWidth : true,
		resizable : true,
		rowNum : 40,
		loadonce : true,
		fixed : true,
		viewrecords : true,
		pager : '#orderListPg',
		pginput : true,
		colModel : itemInfoCM,
		ondblClickRow : function(id) {
			var rowData, inObj, outObj, iary, tblCnt, oary,woCate;

			domObjs.grids.$orderDetailListGrd.jqGrid("clearGridData");
			rowData = domObjs.grids.$orderListGrd.jqGrid("getRowData", id);

			CheckBoxDom.setCheckBox(domObjs.$opeEffFlgChk, rowData.ope_eff_flg === "Y" ? true : false);
			CheckBoxDom.setCheckBox(domObjs.$vcrFlgChk, rowData.vcr_flg_fk === "Y" ? true : false);

			SelectDom.setSelect(domObjs.$cusIDSel, rowData.cus_id);
			cusIDChangeFnc();
			SelectDom.setSelect(domObjs.$mtrlIDSel, baseFnc.replaceSpace(rowData.mtrl_prod_id), rowData.mtrl_prod_id);
			mtrlIDChangeFnc();

			SelectDom.setSelect(domObjs.$thMdlIdSel, baseFnc.replaceSpace(rowData.th_mdl_id_fk), rowData.th_mdl_id_fk);
			SelectDom.setSelect(domObjs.$fmMdlIDSel, baseFnc.replaceSpace(rowData.fm_mdl_id_fk), rowData.fm_mdl_id_fk);
			SelectDom.setSelect(domObjs.$cutMdlIDSel, baseFnc.replaceSpace(rowData.cut_mdl_id_fk), rowData.cut_mdl_id_fk);
			SelectDom.setSelect(domObjs.$mtrlPartSel, baseFnc.replaceSpace(rowData.mtrl_part), rowData.mtrl_part);
			domObjs.$orderIDTxt.val(rowData.wo_id);
			domObjs.$woCateSel.val(getWoCateByChin(rowData.wo_cate)).trigger("change");;
			domObjs.$plnStbDateTxt.val(rowData.pln_stb_date);
			domObjs.$plnCmpDateTxt.val(rowData.pln_cmp_date);
			domObjs.$plnPrdQtyTxt.val(rowData.pln_prd_qty);
			domObjs.$woNoteTxt.val(rowData.wo_note);
			SelectDom.setSelect(domObjs.$pathChooseSel, rowData.path_id + "@" + rowData.path_ver, rowData.path_id);
			domObjs.$fromThicknessTxt.val(rowData.from_thickness);
			domObjs.$toThicknessTxt.val(rowData.to_thickness);
			domObjs.$cusInfoSndTxt.val(rowData.cus_info_snd);
			domObjs.$cusInfoFstTxt.val(rowData.cus_info_fst);
			domObjs.$tThicknessTxt.val(rowData.t_thickness);
			domObjs.$cThicknessTxt.val(rowData.c_thickness);
			domObjs.$orderIDTxt.attr(VAL.DISABLE_ATTR);
			domObjs.$manualTxt.val(rowData.manual);
			domObjs.$cusMtrlTxt.val(rowData.cus_mtrl_id_fk);
			domObjs.$glassFromTxt.val(rowData.glass_from);
			$("#f9_cancel_update_btn").show();// hide
			$("#f4_del_btn").hide();
			$("#f10_clear_btn").hide();
			$("#f1_query_btn").hide();
			$("#f8_add_btn").hide();
			$("#f11_bind_btn").hide();
			$("#f12_show_wo_btn").hide();
			$("#importCusSoBtn").hide();
		}
	});
	var itemInfoCM2 = [ {
		name : 'wo_id',
		index : 'wo_id',
		label : WO_ID_TAG,
		width : WO_ID_CLM
	}, {
		name : 'wo_cate',
		index : 'wo_cate',
		label : SO_CATE_TAG,
		width : WO_CATE_CLM
	}, {
		name : 'pln_stb_date',
		index : 'pln_stb_date',
		label : REACH_DATE_TAG,
		width : DATE_CLM
	}, {
		name : 'pln_cmp_date',
		index : 'pln_cmp_date',
		label : EXPECT_END_DATE_TAG,
		width : DATE_CLM
	}, {
		name : 'mdl_id',
		index : 'mdl_id',
		label : MDL_ID_TAG,
		width : MDL_ID_CLM
	}, {
		name : 'pln_prd_qty',
		index : 'pln_prd_qty',
		label : EXPECT_COUNT_TAG,
		width : QTY_CLM
	}, {
		name : 'wo_note',
		index : 'wo_note',
		label : ORDER_INSTRUCTION_TAG,
		width : WO_NOTE_CLM
	}, {
		name : 'so_id',
		index : 'so_id',
		label : CUS_ORDER_ID_TAG,
		width : WO_ID_CLM
	}, {
		name : 'evt_usr',
		index : 'wo_dsc',
		label : EVT_USR,
		width : USER_CLM
	}, {
		name : 'evt_timestamp',
		index : 'evt_timestamp',
		label : EVT_TIMESTAMP,
		width : TIMESTAMP_CLM
	} ];
	domObjs.grids.$orderDetailListGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		autowidth : true,// 宽度根据父元素自适应
		shrinkToFit : true,
		scroll : true,
		viewrecords : true,
		rownumbers : true,
		rowNum : 20,
		rownumWidth : 20,
		resizable : true,
		loadonce : true,
		toppager : true,
		fixed : true,
		pager : '#orderDetailListPg',
		pginput : true,
		fixed : true,
		cellEdit : true,
		cellsubmit : "clientArray",
		emptyrecords : true,
		colModel : itemInfoCM2
	});
	function f12ShowWoFnc() {
		var inObj, outObj, rowData, selectRowId;
		domObjs.grids.$orderDetailListGrd.jqGrid("clearGridData");
		selectRowId = domObjs.grids.$orderListGrd.jqGrid("getGridParam", "selrow");
		if (!selectRowId) {
			showErrorDialog("", "选择需要查询的客户订单");
			return false;
		}
		rowData = domObjs.grids.$orderListGrd.jqGrid("getRowData", selectRowId);
		inObj = {
			trx_id : "XPAPLYSO",
			action_flg : "G",
			iary : {
			    so_id : rowData.wo_id,
				mtrl_prod_id : rowData.mtrl_prod_id
			}
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			setGridInfo(outObj.oary, "#orderDetailListGrd");
		}
	}
	$("#f12_show_wo_btn").click(f12ShowWoFnc);
	function worderSelectFnc() {
		var pln_prd_qty = 0;
		var selectAllRows = $("#bindWoDialog_woGrd").jqGrid("getGridParam", "selarrrow").concat();
		for ( var i = 0; i < selectAllRows.length; i++) {
			var rowData = $("#bindWoDialog_woGrd").jqGrid("getRowData", selectAllRows[i]);
			pln_prd_qty = pln_prd_qty + comParseInt(rowData.pln_prd_qty);
		}
		domObjs.$plnPrdQtyTxt.val(pln_prd_qty);
		$("#bindWODialog_plnPrdQtyTxt").val(pln_prd_qty);
	}
	$("#bindWoDialog_woGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		autowidth : true,// 宽度根据父元素自适应
		shrinkToFit : true,
		scroll : true,
		viewrecords : true,
		rownumbers : true,
		rowNum : 20,
		rownumWidth : 20,
		resizable : true,
		loadonce : true,
		toppager : true,
		fixed : true,
		pager : '#bindWoDialog_woPg',
		pginput : true,
		fixed : true,
		multiselect : true,
		cellEdit : true,
		cellsubmit : "clientArray",
		emptyrecords : true,
		colModel : itemInfoCM2,
		onSelectRow : function(id) {
			worderSelectFnc();
		},
		onSelectAll : function(rowIDs) {
			worderSelectFnc();
		}
	});

	function f1QueryFnc() {
		var inObj, outObj, iary, tblCnt , oary , i;
		var woId = domObjs.$orderIDTxt.val().trim();
		var woCate = domObjs.$woCateSel.val();
		var thMdlId = baseFnc.returnSpace(domObjs.$thMdlIdSel.val());
		var fmMdlId = baseFnc.returnSpace(domObjs.$fmMdlIDSel.val());
		var cutMdlId = baseFnc.returnSpace(domObjs.$cutMdlIDSel.val());
		var mtrlProdId = baseFnc.returnSpace(domObjs.$mtrlIDSel.val()); 
		var cusId = domObjs.$cusIDSel.val();
		var pln_stb_date = $("#plnStbDateTxt").val();
		iary = {};
		if (woId) {
			iary.wo_id = woId;
		} else {
			if (woCate) {
				iary.wo_cate = woCate;
			}
			if (thMdlId) {
				iary.th_mdl_id_fk = thMdlId;
			}
			if (fmMdlId) {
				iary.fm_mdl_id_fk = fmMdlId;
			}
			if (cutMdlId) {
				iary.cut_mdl_id_fk = cutMdlId;
			}
			if (mtrlProdId && mtrlProdId != " ") {
				iary.mtrl_prod_id = mtrlProdId;
			}
			if (cusId) {
				iary.cus_id = cusId;
			}
			if(pln_stb_date){
				iary.pln_stb_date = pln_stb_date;
			}
		}
		inObj = {
			trx_id : 'XPAPLYSO',
			action_flg : 'Q',
			tbl_cnt : 1,
			iary : iary
		};

		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code === VAL.NORMAL) {
			tblCnt = outObj.tbl_cnt ;
			for(i=0;i<tblCnt;i++){
				oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
				oary.wo_cate = VAL.WOAB[oary.wo_cate];
			}
			setGridInfo(outObj.oary, "#orderListGrd");
			domObjs.$orderIDTxt.removeClass("uneditable-input");
			domObjs.$orderIDTxt.attr(VAL.ENABLE_ATTR);
		}
		domObjs.$orderIDTxt.attr(VAL.ENABLE_ATTR);
	}
	$("#f1_query_btn").click(f1QueryFnc);

	$("#f8_add_btn").click(function() {
		var wo_id = $("#orderIDTxt").val().trim();
		var wo_cate = $("#woCateSel").val();
		var wo_dsc = $("#orderDscTxt").val();
		var pln_stb_date = $("#plnStbDateTxt").val();
		var pln_cmp_date = $("#plnCmpDateTxt").val();
		var dest_shop = $("#destShopSel").find("option:selected").html();
		var th_mdl_id_fk = baseFnc.returnSpace( $("#thMdlIdSel").val());
		var pln_prd_qty = $("#plnPrdQtyTxt").val();
		var mtrl_prod_id = $("#mtrlIDSel").find("option:selected").html();
		var wo_note = $("#woNoteTxt").val();
		var fm_mdl_id_fk = baseFnc.returnSpace($("#fmMdlIDSel").val());
		var cut_mdl_id_fk = baseFnc.returnSpace($("#cutMdlIDSel").val());
		var ope_eff_flg,vcr_flg_fk;
		var from_thickness = $("#fromThicknessTxt").val();
		var to_thickness = $("#toThicknessTxt").val();
		var cusId = $("#cusIDSel").val();
		var pathChoose = $("#pathChooseSel").val();
		var pathId, pathVer;
		var mdl_id = baseFnc.getMdlId();
		var mtrl_part = $("#mtrlPartSel").val();
		var cus_info_fst = $("#cusInfoFstTxt").val();
		var cus_info_snd = $("#cusInfoSndTxt").val();
		var manual = $("#manualTxt").val();
		var cusMtrl = $("#cusMtrlTxt").val();
		var glassFrom = $("#glassFromTxt").val();
		var t_thickness = $.trim(domObjs.$tThicknessTxt.val());
		var c_thickness = $.trim(domObjs.$cThicknessTxt.val());
		
		ope_eff_flg = CheckBoxDom.isChecked(domObjs.$opeEffFlgChk) ? "Y" : "N";
		vcr_flg_fk = CheckBoxDom.isChecked(domObjs.$vcrFlgChk) ? "Y" : "N";

		if (!wo_id) {
			showErrorDialog("002", "订单ID不能为空，请填写");
			return false;
		}
		if (!wo_cate) {
			showErrorDialog("003", "订单类型不能为空,请选择");
			return false;
		}
		if (!pln_stb_date) {
			showErrorDialog("003", "计划投产日期不能为空,请选择");
			return false;
		}
		if (!cusId) {
			showErrorDialog("", "请选择客户代码");
			return false;
		}
		if (!mdl_id) {
			showErrorDialog("003", "产品名称不能为空,请选择");
			return false;
		}
		if (!pln_prd_qty) {
			showErrorDialog("003", "来料数量不能为空,请选择");
			return false;
		}
		if (!mtrl_prod_id) {
			showErrorDialog("003", "来料型号不能为空,请选择");
			return false;
		}
		if (!from_thickness) {
			showErrorDialog("", "来料厚度不能为空,请填写");
			return false;
		}
		if (!to_thickness) {
			showErrorDialog("", "目标厚度不能为空,请填写");
			return false;
		}
		if (!t_thickness) {
			showErrorDialog("", "T侧厚度不能为空,请填写");
			return false;
		}
		if (!c_thickness) {
			showErrorDialog("", "C侧厚度不能为空,请填写");
			return false;
		}
		if( from_thickness < 0 || to_thickness < 0 || t_thickness < 0 || c_thickness < 0  ){
			showErrorDialog("", "厚度不能<0,请填写");
			return false;
		}
		if (!pathChoose) {
			showErrorDialog("", "必须选择工艺路线设定方式,请选择");
			return false;
		}
		if (from_thickness < to_thickness) {
			showErrorDialog("", "目标厚度不能大于来料厚度");
			return false;
		}
		if ((parseFloat(t_thickness) + parseFloat(c_thickness) ).toFixed(3)  != parseFloat(to_thickness).toFixed(3)) {
			showErrorDialog("", "T侧厚度+C侧厚度 不等于产出厚度");
			return false;
		}
		
		if(pln_cmp_date && new Date(pln_stb_date)>new Date(pln_cmp_date)){
			showErrorDialog("","来料日期不能在交货日期之后");
			return false;
		}
		
//		if (!mtrl_part) {
//			showErrorDialog("", "请选择材质");
//			return false;
//		}
		pathId = pathChoose.split("@")[0];
		pathVer = pathChoose.split("@")[1];
		var iary = {
			wo_id : wo_id,
			wo_cate : wo_cate,
			wo_dsc : wo_dsc,
			pln_stb_date : pln_stb_date,
			dest_shop : dest_shop,
			mdl_id : mdl_id,
			pln_prd_qty : pln_prd_qty,
			evt_user : $("#userId").text(),
			mtrl_prod_id : mtrl_prod_id,
			wo_note : wo_note,
			th_mdl_id_fk : th_mdl_id_fk,
			fm_mdl_id_fk : fm_mdl_id_fk,
			cut_mdl_id_fk : cut_mdl_id_fk,
			ope_eff_flg : ope_eff_flg,
			vcr_flg_fk : vcr_flg_fk,
			from_thickness : from_thickness,
			to_thickness : to_thickness,
			path_id : pathId,
			path_ver : pathVer,
			cus_id : cusId,
			mtrl_part : mtrl_part,
			cus_info_fst : cus_info_fst,
			cus_info_snd : cus_info_snd,
			t_thickness : t_thickness,
			c_thickness : c_thickness,
			manual : manual,
		    cusMtrl : cusMtrl,
		    glassFrom : glassFrom
		};
		if (GlobalBean.autoGenSoFlg == true) {
			iary.update_crt_no_flg = "Y";
		}
		if (pln_cmp_date) {
			iary.pln_cmp_date = pln_cmp_date;
		}
		var selectAllRows = domObjs.grids.$orderDetailListGrd.jqGrid('getGridParam', 'selarrrow');
		var iaryB = [];
		for ( var i = 0; i < selectAllRows.length; i++) {
			var rowData = domObjs.grids.$orderDetailListGrd.jqGrid("getRowData", selectAllRows[i]);
			iaryB.push({
				wo_id : rowData.wo_id
			});
		}
		iary.iaryB = iaryB;
		var inObj = {
			trx_id : 'XPAPLYSO',
			action_flg : 'A',
			tbl_cnt : 1,
			iary : iary
		};

		var outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			iary.evt_timestamp = getCurrentTime();
			var rowCnt = $("#orderListGrd").jqGrid("getGridParam", "reccount");
			var rowIDs = $("#orderListGrd").jqGrid("getDataIDs");
			var oary = outObj.oary;
			oary.wo_cate = VAL.WOAB[oary.wo_cate];
			$("#orderListGrd").jqGrid("addRowData", rowIDs[rowCnt - 1] + 1, outObj.oary);
			showSuccessDialog("订单新增成功");
		}
		domObjs.$orderIDTxt.removeClass("uneditable-input");
		domObjs.$orderIDTxt.attr(VAL.ENABLE_ATTR);
		domObjs.$orderIDTxt.val("");
		GlobalBean.autoGenSoFlg = false;
	});
	$("#f4_del_btn").click(function() {
		var delRowID = $("#orderListGrd").jqGrid("getGridParam", "selrow");
		if (!delRowID) {
			showErrorDialog("001", "请选择需要删除的行");
			return false;
		}
		var iary = new Array();
		rowData = $("#orderListGrd").jqGrid("getRowData", delRowID);
		iary.push({
			wo_id : rowData.wo_id
		});
		$("#f4_del_btn").showCallBackWarnningDialog({
			errMsg : "删除选中的订单，是否继续?",
			callbackFn : function(data) {
				if (data.result == true) {
					var inObj = {
						trx_id : 'XPAPLYSO',
						action_flg : 'D',
						tbl_cnt : 1,
						iary : iary
					};

					var outObj = comTrxSubSendPostJson(inObj);
					if (outObj.rtn_code == "0000000") {
						$("#orderListGrd").jqGrid("delRowData", delRowID);
						showSuccessDialog("订单删除成功");
						$("#orderIDTxt").attr({
							"disabled" : false
						});
					}
				}
			}
		});
		$("#orderIDTxt").removeClass("uneditable-input");
		$("#orderIDTxt").attr({
			"disabled" : false
		});
	});

	$("#f5_upd_btn").click(function() {
		if ($("#orderIDTxt").attr("disabled") != "disabled") {
			showErrorDialog("003", "修改请双击需要修改的行");
			return false;
		}
		var wo_id = $("#orderIDTxt").val().trim();
		var wo_cate = $("#woCateSel").val();
		var wo_dsc = $("#orderDscTxt").val();
		var pln_stb_date = $("#plnStbDateTxt").val();
		var pln_cmp_date = $("#plnCmpDateTxt").val();
		var dest_shop = $("#destShopSel").find("option:selected").html();
		var pln_prd_qty = $("#plnPrdQtyTxt").val();
		var mtrl_prod_id = $("#mtrlIDSel").find("option:selected").html();
		var wo_note = $("#woNoteTxt").val();
		var th_mdl_id_fk = baseFnc.returnSpace(domObjs.$thMdlIdSel.val());
		var fm_mdl_id_fk = baseFnc.returnSpace($("#fmMdlIDSel").val());
		var cut_mdl_id_fk = baseFnc.returnSpace($("#cutMdlIDSel").val());
		var mdl_id = baseFnc.getMdlId();
		var from_thickness = $("#fromThicknessTxt").val();
		var to_thickness = $("#toThicknessTxt").val();
		var pathChoose = $("#pathChooseSel").val();
		var cusId = $("#cusIDSel").val();
		var mtrl_part = $("#mtrlPartSel").val();
		var ope_eff_flg, vcr_flg_fk,pathId, pathVer;
		var cus_info_fst = $("#cusInfoFstTxt").val();
		var cus_info_snd = $("#cusInfoSndTxt").val();
		var manual = $("#manualTxt").val();
		var cusMtrl = $("#cusMtrlTxt").val();
		var glassFrom = $("#glassFromTxt").val();
		var t_thickness = $.trim(domObjs.$tThicknessTxt.val());
		var c_thickness = $.trim(domObjs.$cThicknessTxt.val());
		
		if (!cusId) {
			showErrorDialog("", "请选择客户");
			return false;
		}
		if (!from_thickness) {
			showErrorDialog("", "来料厚度不能为空,请填写");
			return false;
		}
		if (!to_thickness) {
			showErrorDialog("", "目标厚度不能为空,请填写");
			return false;
		}
		if (!t_thickness) {
			showErrorDialog("", "T侧厚度不能为空,请填写");
			return false;
		}
		if (!c_thickness) {
			showErrorDialog("", "C侧厚度不能为空,请填写");
			return false;
		}
		if( from_thickness < 0 || to_thickness < 0 || t_thickness < 0 || c_thickness < 0  ){
			showErrorDialog("", "厚度不能<0,请填写");
			return false;
		}
		if (from_thickness < to_thickness) {
			showErrorDialog("", "目标厚度不能大于来料厚度");
			return false;
		}
		if ((parseFloat(t_thickness) + parseFloat(c_thickness) ).toFixed(3) != parseFloat(to_thickness).toFixed(3)) {
			showErrorDialog("", "T侧厚度+C侧厚度 不等于产出厚度");
			return false;
		}
		if (wo_id == "") {
			showErrorDialog("002", "订单ID不能为空，请填写");
			return false;
		}
		if (wo_cate == "") {
			showErrorDialog("003", "订单类型不能为空,请选择");
			return false;
		}
		if (pln_stb_date == "") {
			showErrorDialog("003", "计划投产日期不能为空,请选择");
			return false;
		}
		if(pln_cmp_date && new Date(pln_stb_date)>new Date(pln_cmp_date)){
			showErrorDialog("","来料日期不能在交货日期之后");
			return false;
		}
		if(!mdl_id){
			showErrorDialog("", "产品名称不能为空,请选择");
			return false;
		}
		if (pln_prd_qty == "") {
			showErrorDialog("003", "来料数量不能为空,请选择");
			return false;
		}
		if (mtrl_prod_id == "") {
			showErrorDialog("003", "来料型号不能为空,请选择");
			return false;
		}
		if ($("#opeEffFlgChk").attr("checked") == "checked") {
			ope_eff_flg = "Y";
		} else {
			ope_eff_flg = "N";
		}
		if ($("#vcrFlgChk").attr("checked") == "checked") {
			vcr_flg_fk = "Y";
		} else {
			vcr_flg_fk = "N";
		}

		if (!pathChoose) {
			showErrorDialog("", "必须选择工艺路线设定,请选择");
			return false;
		}
		
//		if (!mtrl_part) {
//			showErrorDialog("", "请选择材质");
//			return false;
//		}
		pathId = pathChoose.split("@")[0];
		pathVer = pathChoose.split("@")[1];
		var iary = {
			wo_id : wo_id,
			wo_cate : wo_cate,
			wo_dsc : wo_dsc,
			pln_stb_date : pln_stb_date,
			pln_cmp_date : pln_cmp_date,
			dest_shop : dest_shop,
			mdl_id : mdl_id,
			pln_prd_qty : pln_prd_qty,
			evt_user : $("#userId").text(),
			mtrl_prod_id : mtrl_prod_id,
			wo_note : wo_note,
			fm_mdl_id_fk : fm_mdl_id_fk,
			cut_mdl_id_fk : cut_mdl_id_fk,
			ope_eff_flg : ope_eff_flg,
			vcr_flg_fk : vcr_flg_fk,
			th_mdl_id_fk: th_mdl_id_fk,
			from_thickness : from_thickness,
			to_thickness : to_thickness,
			path_id : pathId,
			path_ver : pathVer,
			cus_id : cusId,
			mtrl_part : mtrl_part,
			cus_info_fst : cus_info_fst,
			cus_info_snd : cus_info_snd,
			t_thickness : t_thickness,
			c_thickness : c_thickness,
			manual : manual,
		    cusMtrl : cusMtrl,
		    glassFrom : glassFrom
		};
		var selectAllRows = $("#orderDetailListGrd").jqGrid('getGridParam', 'selarrrow');// 获取所有选择行的IDs
		var iaryB = new Array();
		for ( var i = 0; i < selectAllRows.length; i++) {
			var rowData = $("#orderDetailListGrd").jqGrid("getRowData", selectAllRows[i]);
			iaryB.push({
				wo_id : rowData.wo_id
			});
		}
		iary.iaryB = iaryB;
		var inObj = {
			trx_id : 'XPAPLYSO',
			action_flg : 'U',
			tbl_cnt : 1,
			iary : iary
		};

		var outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == "0000000") {
			showSuccessDialog("订单修改成功");
			f1QueryFnc();
			$("#orderIDTxt").removeClass("uneditable-input");
			$("#orderIDTxt").attr({
				"disabled" : false
			});
			$("#f9_cancel_update_btn").hide();// hide
			$("#f4_del_btn").show();
			$("#f10_clear_btn").show();
			$("#f1_query_btn").show();
			$("#f8_add_btn").show();
			$("#f11_bind_btn").show();
			$("#f12_show_wo_btn").show();
			$("#importCusSoBtn").show();
		}
	});
	
	/**
	 * 新增客户订单信息Dialog
	 */
	var CusSoDialog = function() {
		this.showDialogFnc = function(){
			this.iniDialogFnc();
			var inObj = {
				trx_id : "XPAPLYSO",
				action_flg : "C",
			};
			var outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code == "0000000") {
				setGridInfo(outObj.table, "#cusSoGrd");
			}
		};
		this.selectClickFnc = function(){
			var selectedID = domObjs.grids.$cusSoGrd.jqGrid("getGridParam", "selrow");
			var rowData = domObjs.grids.$cusSoGrd.jqGrid("getRowData", selectedID);
			
			var so_cate = rowData['cus_so_cate'];
			if(so_cate == "PP"){
				SelectDom.setSelect(domObjs.$woCateSel, "PROD");
			}else{
				SelectDom.setSelect(domObjs.$woCateSel, "ENG");
			}
			
			var mtrl_prod_id ;
			var mdl_id = rowData['mdl_id_fk'];
			if (rowData['mtrl_prod_id_fk'] == rowData['mdl_id_fk']) {
				var inObj_BOM = {
						trx_id : 'XPBISBOM',
						action_flg : 'Q',
						iary : {
							mdl_id_fk : mdl_id
						}
					};
				var outObj_BOM = comTrxSubSendPostJson(inObj_BOM);
				if (outObj_BOM.rtn_code == "0000000" && outObj_BOM.tbl_cnt > 0) {
					if (parseInt(outObj_BOM.tbl_cnt) == 1){
						mtrl_prod_id = outObj_BOM.oary.mtrl_prod_id_fk;
					}else{
						mtrl_prod_id = outObj_BOM.oary[0].mtrl_prod_id_fk;
					}
				}				
			}else {
				mtrl_prod_id = rowData['mtrl_prod_id_fk'];
			}
			var inObj = {
				trx_id : 'XPLSTDAT',
				action_flg : 'Q',
				iary : {
					data_cate : "CSMT",
					ext_1 : mtrl_prod_id
				}
			};
			var outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code == "0000000" && outObj.tbl_cnt > 0) {
				var inTxObj = {
					trx_id : "XPBMDLDF",
					action_flg : "Q",
					iary : {
						mdl_id : mdl_id
					}
				};
				var outTxObj = comTrxSubSendPostJson(inTxObj);
				if(outTxObj.rtn_code == "0000000"){
					var oary = outObj.tbl_cnt > 1 ? outObj.oary : [outObj.oary];
					SelectDom.setSelect(domObjs.$cusIDSel, oary[0].data_item);
					cusIDChangeFnc();
					SelectDom.setSelect(domObjs.$mtrlIDSel, baseFnc.replaceSpace(mtrl_prod_id), mtrl_prod_id);
					mtrlIDChangeFnc();
					if(outTxObj.oary.mdl_cate == "A"){
						SelectDom.setSelect(domObjs.$thMdlIdSel, baseFnc.replaceSpace(mdl_id), mdl_id);
					}else if(outTxObj.oary.mdl_cate == "AB"){
						SelectDom.setSelect(domObjs.$thMdlIdSel, baseFnc.replaceSpace(mdl_id), mdl_id);
						SelectDom.setSelect(domObjs.$fmMdlIDSel, baseFnc.replaceSpace(mdl_id), mdl_id);
					}else{
						SelectDom.setSelect(domObjs.$thMdlIdSel, baseFnc.replaceSpace(mdl_id), mdl_id);
						SelectDom.setSelect(domObjs.$fmMdlIDSel, baseFnc.replaceSpace(mdl_id), mdl_id);
						SelectDom.setSelect(domObjs.$cutMdlIDSel, baseFnc.replaceSpace(mdl_id), mdl_id);
					}
					setPath();
					domObjs.$orderIDTxt.val(rowData['cus_so_id'].toUpperCase());
					domObjs.$plnStbDateTxt.val(rowData['pln_stb_timestamp'].substring(0,10));
					domObjs.$plnCmpDateTxt.val(rowData['pln_cmp_timestamp'].substring(0,10));
					domObjs.$plnPrdQtyTxt.val(rowData['pln_prd_qty']);
					domObjs.$woNoteTxt.val(rowData['cus_so_note'].toUpperCase() + ";" + rowData['cus_so_remark'].toUpperCase());
					domObjs.$fromThicknessTxt.val(outTxObj.oary.from_thickness);
					domObjs.$toThicknessTxt.val(outTxObj.oary.to_thickness);
					domObjs.$cusInfoSndTxt.val(rowData['cus_so_id'].toUpperCase());
					domObjs.$cusInfoFstTxt.val(rowData['cus_so_id'].toUpperCase());
				}else{
					showErrorDialog("002", "未维护产品[" + mdl_id + "]信息!");
					init();
				}
			}else{
				showErrorDialog("001", "此料号[" + mtrl_prod_id + "]未维护料号与客户的对应关系[CSMT]!");
				init();
			}
			$('#cusSoDialog').modal("hide");
		};
		this.iniDialogFnc = function(){
			$('#cusSoDialog').modal({
				backdrop : true,
				keyboard : false,
				show : false
			});
			$('#cusSoDialog').unbind('shown.bs.modal');
			$("#cusSoDialog_selectSoBtn").unbind('click');

			$('#cusSoDialog').modal("show");
			$("#cusSoDialog_selectSoBtn").bind('click', this.selectClickFnc);
			domObjs.grids.$cusSoGrd.jqGrid({
                url:"",
                datatype:"local",
                mtype:"POST",
                height:280,
                width:1180,
                shrinkToFit:true,
                scroll:true,
                viewrecords:true,
                rownumbers  :true ,
                rowNum:20,
                rownumWidth : 20,
                resizable : true,
                loadonce:true,
                toppager: true ,
                fixed:true,
                pager : '#cusSoGrdPg',
                pginput :true,
                fixed: true,
                cellEdit : true,
                cellsubmit : "clientArray",
                emptyrecords :true ,
                colModel:  [
                    {name: 'ope_fab_id'           , index: 'ope_fab_id'           , label: OPE_FAB_ID_TAG            , width: SO_ID_CLM},
                    {name: 'cus_so_id'            , index: 'cus_so_id'            , label: CUS_ORDER_ID_TAG          , width: COM_CLM},
                    {name: 'cus_so_cate'          , index: 'cus_so_cate'          , label: CUS_SO_CATE_TAG           , width: SO_ID_CLM},
                    {name: 'cus_so_stat'          , index: 'cus_so_stat'          , label: CUS_SO_STAT_TAG           , width: SO_ID_CLM},
                    {name: 'mdl_id_fk'            , index: 'mdl_id_fk'            , label: MDL_ID_TAG                , width: COM_CLM},
                    {name: 'mtrl_prod_id_fk'      , index: 'mtrl_prod_id_fk'      , label: MTRL_PROD_ID_TAG          , width: MTRL_PROD_ID_FK_CLM},
                    {name: 'pln_prd_qty'          , index: 'pln_prd_qty'          , label: "订单数量 "                , width: SO_ID_CLM},
                    {name: 'cus_so_grade'         , index: 'cus_so_grade'         , label: CUS_SO_GRADE_TAG          , width: SO_ID_CLM},
                    {name: 'pln_stb_timestamp'    , index: 'pln_stb_timestamp'    , label: PLN_STB_TIMESTAMP_TAG     , width: COM_CLM},
                    {name: 'pln_cmp_timestamp'    , index: 'pln_cmp_timestamp'    , label: PLN_CMP_TIMESTAMP_TAG     , width: COM_CLM},
                    {name: 'cus_po_rls_timestamp' , index: 'cus_po_rls_timestamp' , label: CUS_PO_RLS_TIMESTAMP_TAG  , width: COM_CLM},
                    {name: 'crt_usr'              , index: 'crt_usr'              , label: CRT_USR_TAG               , width: COM_CLM},
                    {name: 'crt_timestamp'        , index: 'crt_timestamp'        , label: SO_CRT_TIMESTAMP          , width: COM_CLM},
                    {name: 'cus_so_note'          , index: 'cus_so_note'          , label: NOTE_TAG                  , width: WO_NOTE_CLM},
                    {name: 'cus_so_remark'        , index: 'cus_so_remark'        , label: REMARK_TAG                , width: WO_NOTE_CLM}
                ]
            });
		}
	}
	
	var WoDialog = function() {
		this.showDialogFnc = function() {
			this.initialDialogFnc();
			var selectedID = $("#orderListGrd").jqGrid("getGridParam", "selrow");
			var rowData = $("#orderListGrd").jqGrid("getRowData", selectedID);
			$("#bindWODialog_orderIDTxt").val(rowData.wo_id);
			$("#bindWODialog_plnPrdQtyTxt").val(rowData.pln_prd_qty);
			$("#bindWODialog_soIDTxt").val("");
			$("#bindWODialog_soIDTxt").focus();
			var iary = {
				mtrl_prod_id : rowData.mtrl_prod_id
			};
			var inObj = {
				trx_id : "XPAPLYSO",
				action_flg : "G",
				iary : iary
			};
			var outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code == "0000000") {
				setGridInfo(outObj.oary, "#bindWoDialog_woGrd");
			}
		};
		this.bindWoClickFnc = function() {// N0.04
			var real_so_id = $.trim($("#bindWODialog_soIDTxt").val());
			if (real_so_id == "") {
				showErrorDialog("003", "请输入实际订单号!");
				return false;
			}

			var iary = {
				so_id : real_so_id,
				wo_id : $("#orderListGrd").jqGrid("getRowData", $("#orderListGrd").jqGrid("getGridParam", "selrow")).wo_id,
				evt_user : $("#userId").text()

			};
			var inObj = {
				trx_id : 'XPAPLYSO',
				action_flg : 'O',
				tbl_cnt : 1,
				iary : iary
			};
			var outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code == "0000000") {
				showSuccessDialog("订单绑定成功");
				$('#bindWODialog').modal("hide");
				f1QueryFnc();
			}
		};
		this.initialDialogFnc = function() {
			$('#bindWODialog').modal({
				backdrop : true,
				keyboard : false,
				show : false
			});
			$('#bindWODialog').unbind('shown.bs.modal');
			$("#bindWODialog_bindWOBtn").unbind('click');

			$('#bindWODialog').modal("show");
			$("#bindWODialog_bindWOBtn").bind('click', this.bindWoClickFnc);
			$("#bindWODialog_orderIDTxt").attr({
				"disabled" : true
			});
			$("#bindWODialog_plnPrdQtyTxt").attr({
				"disabled" : true
			});

		}

	}
	function f11BindWoFnc() {
		var selectedID = $("#orderListGrd").jqGrid("getGridParam", "selrow");
		if (selectedID == null) {
			showErrorDialog("003", "选择需要绑定工单的订单");
			return false;
		}
		var rowData = $("#orderListGrd").jqGrid("getRowData", selectedID);
		if (rowData.wo_stat == "关闭") {
			showErrorDialog("", "此订单已经被关闭,不能对其操作,请重新打开此订单");
			return false;
		}
		var woDialog = new WoDialog();
		woDialog.showDialogFnc();
	}
	function autoGenSoIDFnc() {
		var inTrx, outTrx, serialNO;
		inTrx = {
			trx_id : "XPCRTNO",
			action_flg : "Q",
			type : "S"
		};
		outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == "0000000") {
			$("#orderIDTxt").val(outTrx.serial_no);
			$("#orderIDTxt").attr({
				"disabled" : true
			});
			GlobalBean.autoGenSoFlg = true;
		}
	}
	function selectCusSoFnc(){
		var cusSoDialog = new CusSoDialog();
		cusSoDialog.showDialogFnc();
	}
    function selectCusSoFnc(){
        var cusSoDialog = new CusSoDialog();
        cusSoDialog.showDialogFnc();
    }
	//$("#autoGeneratorBtn").click(autoGenSoIDFnc);
	$("#f11_bind_btn").click(f11BindWoFnc);
	$("#importCusSoBtn").click(selectCusSoFnc);
	function cancelUpdateFnc() {
		$("#f9_cancel_update_btn").hide();
		$("#f4_del_btn").show();
		$("#f10_clear_btn").show();
		$("#f1_query_btn").show();
		$("#f8_add_btn").show();
		$("#f11_bind_btn").show();
	    $("#f12_show_wo_btn").show();
	    $("#importCusSoBtn").show();
		$("#orderIDTxt").attr(VAL.ENABLE_ATTR);
	}
	$("#f9_cancel_update_btn").click(cancelUpdateFnc);

	$("#f10_clear_btn").click(function() {
		$("#f10_clear_btn").showCallBackWarnningDialog({
			errMsg : "清空所有控件，是否继续?",
			callbackFn : function(data) {
				if (data.result == true) {
					init();
				}
			}
		})
	})
	$("#f6_close_btn").click(function() {
		var orderListGrd = $("#orderListGrd");
		var selRowID = orderListGrd.jqGrid("getGridParam", "selrow");
		if (!selRowID) {
			showErrorDialog("", "选择需要关闭的订单");
			return false;
		}
		var rowData = $("#orderListGrd").jqGrid("getRowData", selRowID);
		if (rowData.wo_stat == "关闭") {
			showErrorDialog("", "此订单已经被关闭,不能对其操作,请重新打开此订单");
			return false;
		}
		$("#f6_close_btn").showCallBackWarnningDialog({
			errMsg : "关闭后订单[" + rowData.wo_id + "]将无法继续使用,是否继续?",
			callbackFn : function(data) {
				if (data.result == true) {
					var inTrx, outTrx, iary, iaryList, usrID;
					usrID = $("#userId").text();
					iary = {
						wo_id : rowData.wo_id,
						evt_user : usrID
					};
					inTrx = {
						trx_id : "XPAPLYSO",
						action_flg : "X",
						iary : iary
					};
					var outObj = comTrxSubSendPostJson(inTrx);
					if (outObj.rtn_code == "0000000") {
						orderListGrd.jqGrid("setRowData", selRowID, outObj.oary);
						showSuccessDialog("订单关闭成功");
					}

				}
			}
		});
	});

	$("#f7_open_btn").click(function() {
		var selRowID = domObjs.grids.$orderListGrd.jqGrid("getGridParam", "selrow");
		var rowDatainTrx, outTrx, iary, iaryList, usrID;
		if (!selRowID) {
			showErrorDialog("", "选择需要重新打开的订单");
			return false;
		}
		usrID = $("#userId").text();
		rowData = domObjs.grids.$orderListGrd.jqGrid("getRowData", selRowID);
		iary = {
			wo_id : rowData.wo_id,
			evt_user : usrID
		};
		inTrx = {
			trx_id : "XPAPLYSO",
			action_flg : "Y",
			iary : iary
		};
		var outObj = comTrxSubSendPostJson(inTrx);
		if (outObj.rtn_code == "0000000") {
			domObjs.grids.$orderListGrd.jqGrid("setRowData", selRowID, outObj.oary);
			showSuccessDialog("订单打开成功");
		}
	});
	$("#exportBtn").click(function() {
		generateExcel("#orderListGrd");
	});
	function initChinWoab(){
		var inObj,outObj,tblCnt,oary,i;
		inObj = {
			trx_id     :  "XPLSTDAT" ,
			action_flg :  "Q",
			iary       : { data_cate : "WOAB" }
		};
		outObj = comTrxSubSendPostJson(inObj);
		if(outObj.rtn_code === VAL.NORMAL){
			tblCnt = outObj.tbl_cnt ;
			for(i=0;i<tblCnt;i++){
				oary = tblCnt > 1 ? outObj.oary[i]:outObj.oary;
				VAL.WOAB[oary.data_ext] = oary.data_desc;
			}
		}
	}
	function init() {
		$("input[type='text']").val("");
		$("select").empty();
		initCusIDSel();
		initMtrlPartSel();
		initWoCate();
		domObjs.$pathChooseSel.empty();
		CheckBoxDom.setCheckBox($("input[type='checkbox']"), false);
		domObjs.$orderIDTxt.attr(VAL.ENABLE_ATTR);
		domObjs.grids.$orderListGrd.jqGrid("clearGridData");
		domObjs.grids.$orderDetailListGrd.jqGrid("clearGridData");
		domObjs.grids.$bindWoDialog_woGrd.jqGrid("clearGridData");
		domObjs.grids.$cusSoGrd.jqGrid("clearGridData");
		initChinWoab();
		$("#f9_cancel_update_btn").hide();
	}
	init(); 
    function resizeFnc(){                                                                      
	  	var offsetBottom, divWidth;                                                              
	  	divWidth = domObjs.grids.$orderListDiv.width();                                   
		offsetBottom = domObjs.$window.height() - domObjs.grids.$orderListDiv.offset().top;
		domObjs.grids.$orderListDiv.height(offsetBottom * 0.95);                          
		domObjs.grids.$orderListGrd.setGridWidth(divWidth * 0.99);                   
		domObjs.grids.$orderListGrd.setGridHeight(offsetBottom * 0.95 - 51);   
		$("#legendDiv1").width(divWidth*0.99);
		
		divWidth = domObjs.grids.$orderDetailListDiv.width();                                   
		domObjs.grids.$orderDetailListDiv.height(offsetBottom * 0.95);                          
		domObjs.grids.$orderDetailListGrd.setGridWidth(divWidth * 0.99);                   
		domObjs.grids.$orderDetailListGrd.setGridHeight(offsetBottom * 0.95 - 51); 
		$("#legendDiv2").width(divWidth*0.99);
		
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
    }); 
})