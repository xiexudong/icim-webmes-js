  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  RePrint Management                                    */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2014/05/21 N0.00   meijiao.C      Initial release                      */
  /*                                                                        */
  /**************************************************************************/
  $(document).ready(function() {
	  var label = new Label();
	  var VAL ={
	        NORMAL     : "0000000"  ,
	        EVT_USER   : $("#userId").text(),
	        T_XPWHSOPE : "XPWHSOPE",
	        T_XPAPLYWO : "XPAPLYWO",
	        T_XPBMDLDF : "XPBMDLDF"
	  };
	  var controlsQuery = {
		    W                  : $(window)             ,
			box_id             : $("#boxidTxt"),
			woBoxTotalCntInput : $("#woBoxTotalCntInput"),
			cusIdSel           : $("#cusIdSel"),
			woIdTxt            : $("#woIdTxt"),
			boxIdTxt           : $("#boxIdTxt"),
			mtrlIdInput        : $("#mtrlIdInput"),
	        hdIdInput          : $("#hdIdInput"),
			mainGrd:{
				grdId          : $("#boxInfGrd"),
				grdPgText      : "#boxInfPg"       ,
	            fatherDiv      : $("#boxInfDiv")
			}
		};
	  var btnQuery ={
		  f1    : $("#f1_query_btn"),
		  print : $("#print_btn"),
		  printNew : $("#printNew_btn")
	  };
	  var toolFunc = {
		  iniClear:function(){
			controlsQuery.woBoxTotalCntInput.val("");
			controlsQuery.woIdTxt.val("");
			controlsQuery.mtrlIdInput.val("");
			controlsQuery.hdIdInput.val("");
			controlsQuery.boxIdTxt.val("");
			controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
			$("form").submit(function(){
                return false ;
             });
		  },
		  setVdrIDSelect : function(){
	            var inTrxObj,
	                outTrxObj;

	            inTrxObj = {
	                trx_id      : 'XPLSTDAT' ,
	                action_flg  : 'Q'        ,
	                iary        : {
	                    data_cate : "CUSD"
	                }
	            };
	            outTrxObj = comTrxSubSendPostJson(inTrxObj);
	            if( outTrxObj.rtn_code == VAL.NORMAL ) {
	            	if(1 < parseInt(outTrxObj.tbl_cnt, 10)){
	                    outTrxObj.oary.sort(function(x, y){
	                        if(x.data_item < y.data_item){
	                            return -1;
	                        }else if(x.data_item > y.data_item){
	                            return 1;
	                        }else{
	                            return 0;
	                        }
	                    });
	                }
	                _setSelectDate(outTrxObj.tbl_cnt, outTrxObj.oary, "data_item", "data_item", "#cusIdSel", false);
	            }
	        },
	        setDestShopSel : function(){
	            addValueByDataCateFnc("#destShopSel","DEST","data_id");
	        },
	  };
	  /**
	     * All button click function
	     * @type {Object}
	     */
	    var btnFunc = {
	        //Query
	        f1_func : function(){
	        	var inObj,outObj;
	        	var iary = new Array();
        		var cus_id,wo_id;
        		cus_id = controlsQuery.cusIdSel.val();
        		wo_id = controlsQuery.woIdTxt.val();
        		if(!cus_id){
        			showErrorDialog("","请选择客户代码！");
	                return false;
        		}
        		if(!wo_id){
        			showErrorDialog("","请输入内部订单号！");
	                return false;
        		}
        		var SearchInf={
        			vdr_id : cus_id,
        			wo_id : wo_id
        		}
        		iary.push(SearchInf);
        		inObj={
        			trx_id : VAL.T_XPWHSOPE,
        			action_flg : 'Q',
                    iary       : iary
        		}
        		outObj = comTrxSubSendPostJson(inObj);
                if(outObj.rtn_code == VAL.NORMAL) {
                	controlsQuery.woBoxTotalCntInput.val(outObj.box_cnt);
                	for(var i=0;i<outObj.box_cnt;i++){
                		var oary = outObj.box_cnt>1 ? outObj.oary[i] : outObj.oary;
                		var desc = DestTrans.getTran("'C','D'",oary.dest_shop_dsc);
                		oary.dest_shop_dsc = desc==undefined ? oary.dest_shop_dsc : desc;
                	}
                    setGridInfo(outObj.oary,"#boxInfGrd");
                }
	        },
	        print_func:function(){
	        	var crGrid,rowIds,rowData;
	        	var i,k,p;
	        	var vdr_id,
	        		box_id,
	        		mode_name,
	        		wo_id,
	        		totalCnt,
	        		prod_spec,
	        		mtrl_id,
	        		habk_id,
	        		wo_cnt,
	        		start,
	        		end,
	        		boxRule,
	        		right_cnt,
	        		left_cnt,
	        		layout_cnt;
	        	var inObj,outObj;
	        	var boxAry = [];
	        	var prdAry = [];
	        	var destAry = [];
	        	var numberAry = [];
	        	var iary = new Array();
	        	var date = new Date();
	        	str_date = date.getFullYear() + "-" + ( date.getMonth()+1 ) + "-"+date.getDate();
	        	crGrid = controlsQuery.mainGrd.grdId;
	        	rowIds = crGrid.jqGrid('getGridParam','selarrrow');
	        	if( rowIds.length == 0){
	        		showErrorDialog("","请先选择需补印的箱号，再打印！");
	                return false;
	        	}

	        	totalCnt= controlsQuery.woBoxTotalCntInput.val();
	        	if(!totalCnt){
	        		showErrorDialog("","请先输入订单来料总箱数！");
	                return false;
	        	}
	        	
	        	mtrl_id = $.trim(controlsQuery.mtrlIdInput.val());
	        	habk_id = $.trim(controlsQuery.hdIdInput.val());
	        	for(i=0;i<rowIds.length;i++){
	        		rowData = crGrid.jqGrid('getRowData',rowIds[i]);
	        		numberAry[i] = rowIds[i]; //每行的id和行号一致，所以用id号
	        		boxAry[i] = rowData.box_id;
		        	prdAry[i] = rowData.count;
		        	destAry[i] = rowData.dest_shop.substr(0,1);
		        	if(rowData.dest_shop.substr(0,1) == "D"){
		        		if(!mtrl_id){
			        		showErrorDialog("","有保税仓的箱子需补印，请输入料号！");
			                return false;
			        	}
			        	if(!habk_id){
			        		showErrorDialog("","有保税仓的箱子需补印，请输入手册号！");
			                return false;
			        	}
		        	}
	        	}
	        	vdr_id = rowData.vdr_id;
	        	wo_id = rowData.wo_id;
		  		var iary = {
		  			 wo_id : wo_id,
		  		};
		  		inObj = {
	  				trx_id : VAL.T_XPAPLYWO,
	  				action_flg : 'Q',
	  				iary : iary
		  		}
		  		outObj = comTrxSubSendPostJson( inObj );
		  		if( outObj.rtn_code == VAL.NORMAL){
		  			mode_name = outObj.oary.mtrl_prod_id_fk;
		  			prod_spec = outObj.oary.mdl_dsc;
					wo_cnt = outObj.oary.pln_prd_qty;
					/**
				   * 计算模数layout_cnt
				   */
				  var layout_id = outObj.oary.layout_id_fk;
				  for(k=0;k<layout_id.length;k++){
					  if(layout_id.substring(k, k+1) == '*'){
						  start = k;
						  break;
					  }
				  }
				  for(p=start;p<layout_id.length;p++){
					  if(layout_id.substring(p, p+1)=='_'){
						  break;
					  }
				  }
				  end = p;
				  left_cnt = layout_id.substring(0,start);
				  right_cnt = layout_id.substring(parseInt(start+1,10),end);
				  layout_cnt = left_cnt * right_cnt;
					if( !prod_spec ){
					    showErrorDialog("003","产品规格没有维护，请确认！");
			            return false;
				    }
					label.PrintWG4PPBOX(vdr_id,mode_name,str_date,prod_spec,wo_id,wo_cnt,JSON.stringify(prdAry),
							JSON.stringify(numberAry),totalCnt,JSON.stringify(boxAry),layout_cnt.toString(),mtrl_id,habk_id,JSON.stringify(destAry));
					showSuccessDialog("补印成功");
					toolFunc.iniClear();
		  		}
		  		
	        },
	        printNew_func:function(){
	        	var crGrid,rowIds,rowData;
	        	var i,k,p;
	        	var vdr_id,
	        		box_id,
	        		cuswo,
	        		mode_name,
	        		wo_id,
	        		totalCnt,
	        		prod_spec,
	        		mtrl_id,
	        		habk_id,
	        		wo_cnt,
	        		start,
	        		end,
	        		boxRule,
	        		right_cnt,
	        		left_cnt,
	        		layout_cnt;
	        	var inObj,outObj;
	        	var boxAry = [];
	        	var prdAry = [];
	        	var destAry = [];
	        	var numberAry = [];
	        	var iary = new Array();
	        	var date = new Date();
	        	str_date = date.getFullYear() + "-" + ( date.getMonth()+1 ) + "-"+date.getDate();
	        	crGrid = controlsQuery.mainGrd.grdId;
	        	rowIds = crGrid.jqGrid('getGridParam','selarrrow');
	        	if( rowIds.length == 0){
	        		showErrorDialog("","请先选择需补印的箱号，再打印！");
	                return false;
	        	}

	        	totalCnt= controlsQuery.woBoxTotalCntInput.val();
	        	if(!totalCnt){
	        		showErrorDialog("","请先输入订单来料总箱数！");
	                return false;
	        	}
	        	
	        	mtrl_id = $.trim(controlsQuery.mtrlIdInput.val());
	        	habk_id = $.trim(controlsQuery.hdIdInput.val());
	        	for(i=0;i<rowIds.length;i++){
	        		rowData = crGrid.jqGrid('getRowData',rowIds[i]);
	        		numberAry[i] = rowIds[i]; //每行的id和行号一致，所以用id号
	        		boxAry[i] = rowData.box_id;
		        	prdAry[i] = rowData.count;
		        	destAry[i] = rowData.dest_shop.substr(0,1);
		        	if(rowData.dest_shop.substr(0,1) == "D"){
		        		if(!mtrl_id){
			        		showErrorDialog("","有保税仓的箱子需补印，请输入料号！");
			                return false;
			        	}
			        	if(!habk_id){
			        		showErrorDialog("","有保税仓的箱子需补印，请输入手册号！");
			                return false;
			        	}
		        	}
	        	}
	        	vdr_id = rowData.vdr_id;
	        	wo_id = rowData.wo_id;
		  		var iary = {
		  			 wo_id : wo_id,
		  		};
		  		inObj = {
	  				trx_id : VAL.T_XPAPLYWO,
	  				action_flg : 'Q',
	  				iary : iary
		  		}
		  		outObj = comTrxSubSendPostJson( inObj );
		  		if( outObj.rtn_code == VAL.NORMAL){
		  		  var iarys = {
						  wo_id : outObj.oary.so_id,
						  wo_typ:'S'
				  };
				var inObjs = {
						trx_id : VAL.T_XPAPLYWO,
						action_flg : 'Q',
						iary : iarys
				  };
				var outObjs = comTrxSubSendPostJson( inObjs );
				  cuswo = outObjs.oary.cus_info_snd;
		  			mode_name = outObj.oary.mtrl_prod_id_fk;
		  			prod_spec = outObj.oary.mdl_guige;
					wo_cnt = outObj.oary.pln_prd_qty;
					/**
				   * 计算模数layout_cnt
				   */
				  var layout_id = outObj.oary.layout_id_fk;
				  for(k=0;k<layout_id.length;k++){
					  if(layout_id.substring(k, k+1) == '*'){
						  start = k;
						  break;
					  }
				  }
				  for(p=start;p<layout_id.length;p++){
					  if(layout_id.substring(p, p+1)=='_'){
						  break;
					  }
				  }
				  end = p;
				  left_cnt = layout_id.substring(0,start);
				  right_cnt = layout_id.substring(parseInt(start+1,10),end);
				  layout_cnt = left_cnt * right_cnt;
					if( !prod_spec ){
					    showErrorDialog("003","产品规格没有维护，请确认！");
			            return false;
				    }
					  label.printyl(vdr_id,mode_name,str_date,prod_spec,wo_id,wo_cnt,JSON.stringify(prdAry),
							  "1",totalCnt,JSON.stringify(boxAry),layout_cnt.toString(),mtrl_id,habk_id,cuswo);
					showSuccessDialog("补印成功");
					toolFunc.iniClear();
		  		}
	        },
	        ent_fnc : function(){
	  		  var box_id = $.trim(controlsQuery.boxIdTxt.val());
	  		  if( !box_id ){return false;}
	  		  _findRowInGrid('boxInfDiv', $("#boxInfGrd"), 'box_id', box_id);
	        }
	    };
	  var iniGridInfo = function(){
 			var grdInfoCM = [
	 				{name:"box_id"         , index:"box_id"          , label:PPBOX_ID_TAG   , width :BOX_ID_CLM        ,align:"left" ,sortable:false},
		            {name:"vdr_id"         , index:"vdr_id"          , label:CUS_ID_TAG     , width :CUS_ID_CLM        ,align:"left" ,sortable:false},
		            {name:"ppbox_stat"     , index:"ppbox_stat"      , label:STATUS_TAG     , width :PRD_STAT_CLM      ,align:"left" ,sortable:false},
					{name:"wo_id"          , index:"wo_id"           , label:WO_ID_TAG      , width :WO_ID_CLM         ,align:"left" ,sortable:false},
					{name:"mtrl_type"      , index:"mtrl_type"       , label:RAW_MTRL_ID_TAG, width :MTRL_PROD_ID_CLM  ,align:"left" ,sortable:false},
					{name:"dest_shop"      , index:"dest_shop"       , label: ''            , width: 1                 , hidden: true},
					{name:"dest_shop_dsc"  , index:"dest_shop_dsc"   , label:DEST_SHOP_TAG  , width :DEST_SHOP_CLM     ,align:"left" ,sortable:false},
					{name:"count"          , index:"count"           , label:FROM_COUNT_TAG , width :QTY_CLM           ,align:"left" ,sortable:false},
					{name:"rcv_timestamp"  , index:"rcv_timestamp"   , label:FROM_DATE_TAG  , width :TIMESTAMP_CLM     ,align:"left" ,sortable:false}
  		];
	        controlsQuery.mainGrd.grdId.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			     // autowidth:true,//宽度根据父元素自适应
			      width:1170,
			      height:400,
			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      multiselect : true,
			      viewrecords : true, //显示总记录数
			      rownumbers  :true ,//显示行号
			      rowNum:50,         //每页多少行，用于分页
			      rownumWidth : 40,  //行号列宽度
			      emptyrecords :true ,
			      sortname : "rcv_timestamp",
			      sortorder : "asc",
		          colModel: grdInfoCM
	        });
	  };
	  /**
	     * Bind button click action
	     */
	    var iniButtonAction = function(){
	        btnQuery.f1.click(function(){
	            btnFunc.f1_func();
	        });
	        btnQuery.print.click(function(){
	            btnFunc.print_func();
	        });
	        btnQuery.printNew.click(function(){
	            btnFunc.printNew_func();
	        });
	    };
	    var otherActionBind = function(){
	    	controlsQuery.boxIdTxt.keydown(function(event){
	            if(event.keyCode == ENT_KEY){
	                btnFunc.ent_fnc();
	            }
	        });
	    };
	  /**
	     * Ini view and data
	     */
	    var initializationFunc = function(){
	        iniGridInfo();
	        toolFunc.iniClear();
	        toolFunc.setVdrIDSelect();
	        iniButtonAction();
	        otherActionBind();
	        DestTrans.addTrans("'C','D'");
	    };
	    initializationFunc();
  });