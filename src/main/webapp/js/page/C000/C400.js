  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  RePrint Management                                    */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2016/04/25 N0.00   wfq      Initial release                      */
  /*                                                                        */
  /**************************************************************************/
  $(document).ready(function() {
	  var label = new Label();
	  var VAL ={
	        NORMAL     : "0000000"  ,
	        EVT_USER   : $("#userId").text(),
	        T_XPINQBOX : "XPINQBOX"
	  };
	  var controlsQuery = {
		    W                  : $(window),
			//cusIdSel           : $("#cusIdSel"),
			woIdTxt            : $("#woIdTxt"),
			boxIdTxt           : $("#boxIdTxt"),
			print1Select       : $("#print1Select"),
			proc_id            : $("#proc_id")        ,
			mainGrd:{
				grdId          : $("#boxInfGrd"),
				grdPgText      : "#boxInfPg",
	            fatherDiv      : $("#boxInfDiv")
			}
		};
	  var btnQuery ={
		  f1    : $("#f1_query_btn"),
		  print : $("#print_btn")
	  };
	  var toolFunc = {
		  iniClear:function(){
			
			controlsQuery.woIdTxt.val("");
			controlsQuery.boxIdTxt.val("");
			controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
			$("form").submit(function(){
                return false ;
             });
		  }
	  };
	  /**
	     * All button click function
	     * @type {Object}
	     */
	    var btnFunc = {
	        //Query
	        f1_func : function(){
	        	var inObj,outObj,wo_id,box_id;
        		wo_id = controlsQuery.woIdTxt.val();
        		box_id = controlsQuery.boxIdTxt.val();
        		if(wo_id==""&&box_id==""){
        			showErrorDialog("","请输入查询条件！");
	                return false;
        		}
        		inObj={
        			trx_id : VAL.T_XPINQBOX,
        			action_flg : 'A'
        		}
        		if(wo_id){
        			inObj.wo_id=wo_id;
        		}
        		if(box_id){
        			inObj.box_id=box_id;
        		}
        		outObj = comTrxSubSendPostJson(inObj);
                if(outObj.rtn_code == VAL.NORMAL) {
                    setGridInfo(outObj.oary4,"#boxInfGrd");
                }
	        },
	        print_func:function(){
	        	var proc_id = $("#proc_id").val();
	        	if(!proc_id){
	        		showErrorDialog("","请选择补印站点！");
	                return false;
	        	}
	        	var rowIds = controlsQuery.mainGrd.grdId.jqGrid('getGridParam','selarrrow');
	        	if( rowIds.length == 0){
	        		showErrorDialog("","请先选择需补印的箱号，再打印！");
	                return false;
	        	}
	        	for(i=0;i<rowIds.length;i++){
	        		var rowData = controlsQuery.mainGrd.grdId.jqGrid('getRowData',rowIds[i]);
	        		var boxinObj,
                    boxObj;
	        		boxinObj = {
                         trx_id      : VAL.T_XPINQBOX,
                         action_flg  : 'Q'           ,
                         box_id      : rowData.box_id
                    };
	        		boxObj = comTrxSubSendPostJson(boxinObj);
                    if(boxObj.rtn_code != VAL.NORMAL){
                	    return false;
                    }
                    var mtrlId = boxObj.fm_mdl_id_fk;
                    var mtrlBoxId = boxObj.box_phy_id;
                    //var mtrlBoxId = "201706";
                    var prdQty = boxObj.prd_qty;
                    var boxId = boxObj.ship_box_id;
                    var vender = "201706";
                    label.PrintboxforHX(controlsQuery.print1Select.find("option:selected").text(), mtrlId,mtrlBoxId, prdQty,boxId,vender);
                    // label.PrintLabelForDM007(controlsQuery.print1Select.find("option:selected").text(),mtrlId,mtrlBoxId, prdQty,boxId);
                    if(proc_id == "QP02"){
                    	if(boxObj.cus_id_fk=="058"){
                            var printer = controlsQuery.print1Select.find("option:selected").text();
                            var ps = boxObj.mainT;
                            var mkr = "湖北鸿创"; 
                            var woId = boxObj.wo_id_fk;
                            var wocate = "";
                            if(boxObj.wo_cate=="ENG"){
                            	wocate = "试验品";
                            }else if(boxObj.wo_cate=="ONCE"){
                            	wocate = "ON CELL品";
                            }else if(boxObj.wo_cate=="PROD"){
                            	wocate = "量产品";
                            }else if(boxObj.wo_cate=="RD"){
                            	wocate = "研发品";
                            }else if(boxObj.wo_cate=="SAMP"){
                            	wocate = "样品";
                            }
                            var mtrlId = boxObj.th_mdl_id_fk;
                            var qty = boxObj.prd_qty + "*" + boxObj.smode_cnt + "=" + boxObj.mode_cnt;
                            var jgyq = boxObj.jgyq;
                            var date = boxObj.pln_cmp_date;
                            var outdate = boxObj.comp_date;
                            var boxId = rowData.box_id;
                            var thickness = boxObj.to_thickness;
                            var dir = boxObj.fhdir;
                            var user = boxObj.evt_user;
                            var note = boxObj.wo_dsc;
                            label.PrintHCT058(printer, ps, mkr, woId, wocate, mtrlId, qty,jgyq, date,
                                     outdate, boxId, thickness, dir, user,note);
                            var title = boxObj.shtT;
                            var shtAry = [];
            			    for(j=0; j<20; j++){
          				        shtAry[j] = "";
          			        }
                    		if( boxObj.prd_qty == 1){
      						    shtAry[0] = boxObj.table.prd_seq_id;
      					    }else if(boxObj.table.length > 1){
      						   for( i=0;i<boxObj.table.length;i++){
      							   shtAry[i] = boxObj.table[i].prd_seq_id;
      						   }
      					    }
                            label.PrintForHCT058ShtList(printer,title,JSON.stringify(shtAry),boxId);
                            var inTrxCRT = {
    		        				trx_id : "XPCRTNO",
    		        				action_flg : "C",
    		        				key_id : mtrlId,
    		        				type : "S"
    		        			};
    		        		 var outTrxCRT = comTrxSubSendPostJson(inTrxCRT);
    		            	 var str = mtrlId + "H" + outTrxCRT.serial_no;
    		            	 label.PrintQR( printer, str);
    		            	 label.PrintQRhc( printer);
                    	}else if(boxObj.cus_id_fk=="018"||boxObj.cus_id_fk=="088"){
                            var printer = controlsQuery.print1Select.find("option:selected").text();
                            var thickness = boxObj.to_thickness/2;
                            var time = boxObj.comp_date_all;
                            var qty = boxObj.prd_qty;
                            var userID = boxObj.evt_user;
                            var product = boxObj.th_mdl_id_fk;
                            var boxId = boxObj.ship_box_id;
                    		label.PrintHCToncell088(printer, boxId,thickness,time,qty,userID,product);
                    		
                    		var printer = controlsQuery.print1Select.find("option:selected").text();
                    		var soId = boxObj.so_id_fk;
                            var dateinfo = new Array();
                    		    dateinfo = boxObj.overdue_date.split("-");
                    		var overDate =  dateinfo[1] + "月" + dateinfo[2] + "日"; 
                    		var outwoId = boxObj.cus_info_fst;
                    		var qty = boxObj.prd_qty;
                    		var mdltyp = boxObj.guige;
                    		var cusId = boxObj.cus_dsc;
                    		var boxId = rowData.box_id;
                    		var pm = "LCD-TFT";
                    		//var blqty = boxObj.bl_cnt + "cell";
                    		var user = boxObj.evt_user;
                    	    var dateinfo1 = new Array();
                 		        dateinfo1 = boxObj.comp_date.split("/");
                 		    var date =  dateinfo1[1] + "月" + dateinfo1[2] + "日";
                    		var mdlId = boxObj.th_mdl_id_fk;
                    		var woId = boxObj.wo_id_fk;
                    		//var okyield = boxObj.okyield + "%";
                    		var okyield = "";
                    		var qauser = "HC0025";
                    		var bt = "薄化产品出货标签";
                    		var blqty = "";
                    		var grade = boxObj.prd_grade_fk;
                    		if(grade=="OK"){
                    			blqty="正常品";
                    		}else if(grade=="FX"){
                    			blqty="风险品";
                    		}else if(grade=="GK"){
                    			blqty="管控品";
                    		}else if(grade=="MR"){
                    			blqty="MRB品";
                    		}else if(grade=="HG"){
                    			blqty="合格品";
                    		}else if(grade=="LW"){
                    			blqty="裂纹品";
                    		}else if(grade=="PP"){
                    			blqty="破片管控";
                    		}
                    		label.PrintHCT088(printer, soId,overDate, outwoId,qty, mdltyp,cusId,pm,boxId,
                    	             blqty, user, date, mdlId, woId,okyield, qauser,bt); 
                    	}else if(boxObj.cus_id_fk=="098"){
                    		var printer = controlsQuery.print1Select.find("option:selected").text();
                    		
                    		var gys = "HCT";
                    		var boxId = rowData.box_id;
                    		var cusName = "上海中航光电子有限公司";
                    		var mode = boxObj.th_mdl_id_fk;
                    		var date = boxObj.comp_date;
                    		var size = boxObj.guige;
                    		var indate = boxObj.overdue_date;
                    		var mtrllot = "";
                    		//var orderno = boxObj.so_id_fk;
                    		var orderno = "";
                    		var mode2 = boxObj.mtrl_prod_id_fk;
                    		var cnt = boxObj.prd_qty;
                    		var allcnt = boxObj.mode_cnt;
                    		var lotno = boxObj.wo_id_fk;
                            var procate = "";
                            if(boxObj.wo_cate=="ENG"){
                            	procate = "试验品";
                            }else if(boxObj.wo_cate=="ONCE"){
                            	procate = "ON CELL品";
                            }else if(boxObj.wo_cate=="PROD"){
                            	procate = "量产品";
                            }else if(boxObj.wo_cate=="RD"){
                            	procate = "研发品";
                            }else if(boxObj.wo_cate=="SAMP"){
                            	procate = "样品";
                            }
//                            var shtAry = [];
//            			    for(j=0; j<20; j++){
//          				        shtAry[j] = "";
//          			        }
//                    		if( boxObj.prd_qty == 1){
//      						    shtAry[0] = boxObj.table.prd_seq_id;
//      					    }else if(boxObj.table.length > 1){
//      						   for( i=0;i<boxObj.table.length;i++){
//      							   shtAry[i] = boxObj.table[i].prd_seq_id;
//      						   }
//      					    }
                    		label.PrintHCT0981(printer, gys,boxId, cusName,mode, date,size,indate,mtrllot, orderno, mode2, cnt, allcnt, lotno,procate);
                    		var thickness = boxObj.to_thickness/2;
                            var time = boxObj.comp_date_all;
                            var qty = boxObj.prd_qty;
                            var userID = boxObj.evt_user;
                            var product = boxObj.th_mdl_id_fk;
                            var shipboxId = boxObj.ship_box_id;
                    		label.PrintHCToncell088(printer, shipboxId,thickness,time,qty,userID,product);
                    	}else if(boxObj.cus_id_fk=="068"){
                    		var printer = controlsQuery.print1Select.find("option:selected").text();
                    		var soId = boxObj.so_id_fk;
                    		var cusId = "厦门天马";
                    		var compDate = boxObj.comp_date;
                    		var title = "薄化产品出货标签";
                    		var mdl_id = boxObj.th_mdl_id_fk;
                    		var toThickness = boxObj.to_thickness;
                    		var cusInfoFst = boxObj.cus_info_fst;
                    		var cusInfoSnd = boxObj.cus_info_snd;
                    		var prdQty = boxObj.prd_qty;
                    		var NgQty = "/";
                    		//var boxId = rowData.box_id;
                    		var boxId = boxObj.ship_box_id;
                    		if(boxId==null ||boxId ==""){
                    			boxId=rowData.box_id;
                    		}
                    		var grade = boxObj.prd_grade_fk;
                    		var blqty = "";
                    		if(grade=="OK"){
                    			blqty="正常品";
                    		}else if(grade=="FX"){
                    			blqty="风险品";
                    		}else if(grade=="GK"){
                    			blqty="管控品";
                    		}else if(grade=="MR"){
                    			blqty="MRB品";
                    		}else if(grade=="HG"){
                    			blqty="合格品";
                    		}else if(grade=="LW"){
                    			blqty="裂纹品";
                    		}else if(grade=="PP"){
                    			blqty="破片管控";
                    		}
                    		var userId = boxObj.evt_user;
                    		var woId = boxObj.wo_id_fk;
                    		//label.PrintHCT068(printer,soId, cusId, compDate,title, mdl_id, toThickness,cusInfoFst,cusInfoSnd,prdQty,NgQty,boxId, blqty,userId,woId);
                    		var cusName = "鸿创科技有限公司";
                    		var prodName = "1/4一次切割中片";
                    		var dueDate = boxObj.overdue_date;
                    		var reportFLg = "YES";
                    		var hightFlg = "";
                    		label.PrintHCT068N(printer, cusName, prodName,
                    				mdl_id, woId, compDate, dueDate, boxId, cusInfoFst,
                    				cusInfoSnd, prdQty, reportFLg, hightFlg, userId);
                    	}
                    }else if(proc_id == "QD02"){
                    	if(boxObj.cus_id_fk=="058"){
                            var printer = controlsQuery.print1Select.find("option:selected").text();
                            var ps = boxObj.mainT;
                            var mkr = "湖北鸿创"; 
                            var woId = boxObj.wo_id_fk;
                            var wocate = "";
                            if(boxObj.wo_cate=="ENG"){
                            	wocate = "试验品";
                            }else if(boxObj.wo_cate=="ONCE"){
                            	wocate = "ON CELL品";
                            }else if(boxObj.wo_cate=="PROD"){
                            	wocate = "量产品";
                            }else if(boxObj.wo_cate=="RD"){
                            	wocate = "研发品";
                            }else if(boxObj.wo_cate=="SAMP"){
                            	wocate = "样品";
                            }
                            var mtrlId = boxObj.fm_mdl_id_fk;
                            var qty = boxObj.prd_qty + "*" + boxObj.smode_cnt + "=" + boxObj.mode_cnt;
                            var jgyq = boxObj.jgyq;
                            var date = boxObj.pln_cmp_date;
                            var outdate = boxObj.comp_date;
                            var boxId = rowData.box_id;
                            var thickness = boxObj.to_thickness;
                            var dir = boxObj.fhdir;
                            var user = boxObj.evt_user;
                            var note = boxObj.wo_dsc;
                            label.PrintHCT058(printer, ps, mkr, woId, wocate, mtrlId, qty,jgyq, date,
                                     outdate, boxId, thickness, dir, user,note);
                            var title = boxObj.shtT;
                            var shtAry = [];
            			    for(j=0; j<20; j++){
          				        shtAry[j] = "";
          			        }
                    		if( boxObj.prd_qty == 1){
      						    shtAry[0] = boxObj.table.prd_seq_id;
      					    }else if(boxObj.table.length > 1){
      						   for( i=0;i<boxObj.table.length;i++){
      							   shtAry[i] = boxObj.table[i].prd_seq_id;
      						   }
      					    }
                            label.PrintForHCT058ShtList(printer,title,JSON.stringify(shtAry),boxId);
                            var inTrxCRT = {
    		        				trx_id : "XPCRTNO",
    		        				action_flg : "C",
    		        				key_id : mtrlId,
    		        				type : "S"
    		        			};
    		        		 var outTrxCRT = comTrxSubSendPostJson(inTrxCRT);
    		            	 var str = mtrlId + "H" + outTrxCRT.serial_no;
    		            	 label.PrintQR( printer, str);
    		            	 label.PrintQRhc( printer);
                    	}else if(boxObj.cus_id_fk=="018"||boxObj.cus_id_fk=="088"){
                            var printer = controlsQuery.print1Select.find("option:selected").text();
                            var thickness = boxObj.to_thickness/2;
                            var time = boxObj.comp_date_all;
                            var qty = boxObj.prd_qty;
                            var userID = boxObj.evt_user;
                            var product = boxObj.fm_mdl_id_fk;
                            var boxId = boxObj.ship_box_id;
                    		label.PrintHCToncell088(printer, boxId,thickness,time,qty,userID,product);
                    		var printer = controlsQuery.print1Select.find("option:selected").text();
                    		var soId = boxObj.so_id_fk;
                            var dateinfo = new Array();
                    		    dateinfo = boxObj.overdue_date.split("-");
                    		var overDate =  dateinfo[1] + "月" + dateinfo[2] + "日"; 
                    		var outwoId = boxObj.cus_info_fst;
                    		var qty = boxObj.prd_qty;
                    		var mdltyp = boxObj.guige;
                    		var cusId = boxObj.cus_dsc;
                    		var boxId = rowData.box_id;
                    		var pm = "LCD-TFT";
                    		//var blqty = boxObj.bl_cnt + "cell";
                    		var user = boxObj.evt_user;
                    	    var dateinfo1 = new Array();
                 		        dateinfo1 = boxObj.comp_date.split("/");
                 		    var date =  dateinfo1[1] + "月" + dateinfo1[2] + "日";
                    		var mdlId = boxObj.fm_mdl_id_fk;
                    		var woId = boxObj.wo_id_fk;
                    		//var okyield = boxObj.okyield + "%";
                    		var okyield ="";
                    		var qauser = "HC0025";
                    		var bt = "镀膜产品出货标签";
                    		var blqty = "";
                    		var grade = boxObj.prd_grade_fk;
                    		if(grade=="OK"){
                    			blqty="正常品";
                    		}else if(grade=="FX"){
                    			blqty="风险品";
                    		}else if(grade=="GK"){
                    			blqty="管控品";
                    		}else if(grade=="MR"){
                    			blqty="MRB品";
                    		}else if(grade=="HG"){
                    			blqty="合格品";
                    		}else if(grade=="LW"){
                    			blqty="裂纹品";
                    		}else if(grade=="PP"){
                    			blqty="破片管控";
                    		}
                    		label.PrintHCT088(printer, soId,overDate, outwoId,qty, mdltyp,cusId,pm,boxId,
                    	             blqty, user, date, mdlId, woId,okyield, qauser,bt); 
                    	}else if(boxObj.cus_id_fk=="098"){
                    		var printer = controlsQuery.print1Select.find("option:selected").text();
                    		
                    		var gys = "HCT";
                    		var boxId = rowData.box_id;
                    		var cusName = "上海中航光电子有限公司";
                    		var mode = boxObj.fm_mdl_id_fk;
                    		var date = boxObj.comp_date;
                    		var size = boxObj.guige;
                    		var indate = boxObj.overdue_date;
                    		var mtrllot = "";
                    		//var orderno = boxObj.so_id_fk;
                    		var orderno = "";
                    		var mode2 = boxObj.mtrl_prod_id_fk;
                    		var cnt = boxObj.prd_qty;
                    		var allcnt = boxObj.mode_cnt;
                    		var lotno = boxObj.wo_id_fk;
                            var procate = "";
                            if(boxObj.wo_cate=="ENG"){
                            	procate = "试验品";
                            }else if(boxObj.wo_cate=="ONCE"){
                            	procate = "ON CELL品";
                            }else if(boxObj.wo_cate=="PROD"){
                            	procate = "量产品";
                            }else if(boxObj.wo_cate=="RD"){
                            	procate = "研发品";
                            }else if(boxObj.wo_cate=="SAMP"){
                            	procate = "样品";
                            }
//                            var shtAry = [];
//            			    for(j=0; j<20; j++){
//          				        shtAry[j] = "";
//          			        }
//                    		if( boxObj.prd_qty == 1){
//      						    shtAry[0] = boxObj.table.prd_seq_id;
//      					    }else if(boxObj.table.length > 1){
//      						   for( i=0;i<boxObj.table.length;i++){
//      							   shtAry[i] = boxObj.table[i].prd_seq_id;
//      						   }
//      					    }
                    		label.PrintHCT0981(printer, gys,boxId, cusName,mode, date,size,indate,mtrllot, orderno, mode2, cnt, allcnt, lotno,procate);
                    		//label.PrintHCT0982(printer, boxId, JSON.stringify(shtAry));
                    		var thickness = boxObj.to_thickness/2;
                            var time = boxObj.comp_date_all;
                            var qty = boxObj.prd_qty;
                            var userID = boxObj.evt_user;
                            var product = boxObj.fm_mdl_id_fk;
                            var shipboxId = boxObj.ship_box_id;
                    		label.PrintHCToncell088(printer, shipboxId,thickness,time,qty,userID,product);
                    	}else if(boxObj.cus_id_fk=="068"){
                    		var printer = controlsQuery.print1Select.find("option:selected").text();
                    		var soId = boxObj.so_id_fk;
                    		var cusId = "厦门天马";
                    		var compDate = boxObj.comp_date;
                    		var title = "镀膜产品出货标签";
                    		var mdl_id = boxObj.fm_mdl_id_fk;
                    		var toThickness = boxObj.to_thickness;
                    		var cusInfoFst = boxObj.cus_info_fst;
                    		var cusInfoSnd = boxObj.cus_info_snd;
                    		var prdQty = boxObj.prd_qty;
                    		var NgQty = "/"
                    		//var boxId = rowData.box_id;
                    		var boxId =	boxObj.ship_box_id;
                    		if(boxId==null ||boxId ==""){
                    			boxId= rowData.box_id;
                    		}
                    		var grade = boxObj.prd_grade_fk;
                    		var blqty = "";
                    		if(grade=="OK"){
                    			blqty="正常品";
                    		}else if(grade=="FX"){
                    			blqty="风险品";
                    		}else if(grade=="GK"){
                    			blqty="管控品";
                    		}else if(grade=="MR"){
                    			blqty="MRB品";
                    		}else if(grade=="HG"){
                    			blqty="合格品";
                    		}else if(grade=="LW"){
                    			blqty="裂纹品";
                    		}else if(grade=="PP"){
                    			blqty="破片管控";
                    		}
                    		var userId = boxObj.evt_user;
                    		var woId = boxObj.wo_id_fk;
                    		//label.PrintHCT068(printer,soId, cusId, compDate,title, mdl_id, toThickness,cusInfoFst,cusInfoSnd,prdQty,NgQty,boxId, blqty,userId,woId);
                    		var cusName = "鸿创科技有限公司";
                    		var prodName = "1/4一次切割中片";
                    		var dueDate = boxObj.overdue_date;
                    		var reportFLg = "YES";
                    		var hightFLg = "";
                    		label.PrintHCT068N(printer, cusName, prodName,
                    				mdl_id, woId, compDate, dueDate, boxId, cusInfoFst,
                    				cusInfoSnd, prdQty, reportFLg, hightFlg, userId);
                    	}
                    }else if(proc_id == "P001"){
                    	if(boxObj.cus_id_fk=="098"){
                    		var printer = controlsQuery.print1Select.find("option:selected").text();
                    		var boxId = rowData.box_id;
                            var shtAry = [];
            			    for(j=0; j<20; j++){
          				        shtAry[j] = "";
          			        }
                    		if( boxObj.prd_qty == 1){
      						    shtAry[0] = boxObj.table.prd_seq_id;
      					    }else if(boxObj.table.length > 1){
      						   for( i=0;i<boxObj.table.length;i++){
      							   shtAry[i] = boxObj.table[i].prd_seq_id;
      						   }
      					    }
                    		label.PrintHCT0982(printer, boxId, JSON.stringify(shtAry));
                    	}
                    }else if(proc_id == "DMBZ" && (boxObj.mdl_cate_fk == "AB" || boxObj.mdl_cate_fk == "B")){//华星镀膜包装
                        if(boxObj.cus_id_fk=="007"){
                            var mtrlId = boxObj.fm_mdl_id_fk;
                            var mtrlBoxId = boxObj.box_phy_id;
                            //var mtrlBoxId = "201706";
                            var prdQty = boxObj.prd_qty;
                            var boxId = boxObj.ship_box_id;
                            var vender = "201706";
                            label.PrintboxforHX(controlsQuery.print1Select.find("option:selected").text(), mtrlId,mtrlBoxId, prdQty,boxId,vender);
                            // label.PrintLabelForDM007(controlsQuery.print1Select.find("option:selected").text(),mtrlId,mtrlBoxId, prdQty,boxId);
                        }

                    }


	        	}
	        }
	    };
	  var iniGridInfo = function(){
 			var grdInfoCM = [
	 				{name:"box_id"         , index:"box_id"          , label:BOX_ID_TAG   , width :BOX_ID_CLM        ,align:"left" ,sortable:false},
		            {name:"cus_id_fk"         , index:"cus_id_fk"          , label:CUS_ID_TAG     , width :CUS_ID_CLM        ,align:"left" ,sortable:false},
		            {name:"so_id_fk"     , index:"so_id_fk"      , label:SO_ID_TAG     , width :SO_ID_CLM      ,align:"left" ,sortable:false},
					{name:"fm_mdl_id_fk"          , index:"fm_mdl_id_fk"           , label:FM_MDL_ID_TAG      , width :FM_MDL_ID_CLM         ,align:"left" ,sortable:false},
					{name:"to_thickness"      , index:"to_thickness"       , label:TO_THICKNESS_TAG, width :MTRL_PROD_ID_CLM  ,align:"left" ,sortable:false},
					{name:"prd_grade_fk"  , index:"prd_grade_fk"   , label:PRD_GRADE_TAG  , width :DEST_SHOP_CLM     ,align:"left" ,sortable:false},
					{name:"cus_info_fst"  , index:"cus_info_fst"   , label:"委外工单号"  , width :DEST_SHOP_CLM     ,align:"left" ,sortable:false},
					{name:"cus_info_snd"  , index:"cus_info_snd"   , label:"客户工单号"  , width :DEST_SHOP_CLM     ,align:"left" ,sortable:false},
					{name:"prd_qty"          , index:"prd_qty"           , label:PRD_QTY_TAG , width :QTY_CLM           ,align:"left" ,sortable:false},
					{name:"wo_id"  , index:"wo_id"   , label:WO_ID_TAG  , width :TIMESTAMP_CLM     ,align:"left" ,sortable:false},
					{name:"dm_user"  , index:"dm_user"   , label:EVT_USR  , width :TIMESTAMP_CLM     ,align:"left" ,sortable:false},
					{name:"comp_date"  , index:"comp_date"   , label:EVT_TIMESTAMP  , width :TIMESTAMP_CLM     ,align:"left" ,sortable:false}
  		];
	        controlsQuery.mainGrd.grdId.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			     // autowidth:true,//宽度根据父元素自适应
			      width:1170,
			      height:400,
			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      multiselect : true,
			      viewrecords : true, //显示总记录数
			      rownumbers  :true ,//显示行号
			      rowNum:50,         //每页多少行，用于分页
			      rownumWidth : 40,  //行号列宽度
			      emptyrecords :true ,
			      sortname : "rcv_timestamp",
			      sortorder : "asc",
		          colModel: grdInfoCM
	        });
	  };
	  /**
	     * Bind button click action
	     */
	    var iniButtonAction = function(){
	        btnQuery.f1.click(function(){
	            btnFunc.f1_func();
	        });
	        btnQuery.print.click(function(){
	            btnFunc.print_func();
	        });
	    };
	    var otherActionBind = function(){
	    	controlsQuery.boxIdTxt.keydown(function(event){
	            if(event.keyCode == ENT_KEY){
	                btnFunc.f1_func();
	            }
	        });
	    };
	  /**
	     * Ini view and data
	     */
	    var initializationFunc = function(){
	        iniGridInfo();
	        toolFunc.iniClear();
	        //toolFunc.setVdrIDSelect();
	        iniButtonAction();
	        otherActionBind();
	        getPrinters();

	    };
	    initializationFunc();
		  /**
		   * 获取打印机
		   */
		  function getPrinters(){
			  controlsQuery.print1Select.append("<option ></option>");
			  if (printers !== undefined) {
				  for(var i=0; i<printers.length; i++){
					  controlsQuery.print1Select.append("<option>"+ printers[i] +"</option>");
				  }
			  }
		  }
  });