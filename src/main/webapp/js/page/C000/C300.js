  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  RePrint Lot Label Management                          */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2014/06/26 N0.00   meijiao.C      Initial release                      */
  /*                                                                        */
  /** *********************************************************************** */
  $(document).ready(function() {
	  var label = new Label();
	  var VAL ={
	        NORMAL     : "0000000"  ,
	        EVT_USER   : $("#userId").text(),
	        T_XPINQDEF : "XPINQDEF"
	  };
	  var controlsQuery = {
		    W                  : $(window)             ,
		    proc_id            : $("#proc_id")        ,
		    boxidTxt           : $("#boxidTxt")        ,
            print2Select       : $("#print2Select"),
			mainGrd:{
				grdId003          : $("#shtListGrd003"),
				grdPgText003      : "#shtListPg003"       ,
	            fatherDiv003     : $("#shtListDiv003"),
	            grdId006          : $("#shtListGrd006"),
				grdPgText006      : "#shtListPg006"       ,
	            fatherDiv006     : $("#shtListDiv006"),
	            grdId008          : $("#shtListGrd008"),
				grdPgText008      : "#shtListPg008"       ,
	            fatherDiv008     : $("#shtListDiv008"),
				grdId063          : $("#shtListGrd063"),
				grdPgText063      : "#shtListPg063"       ,
	            fatherDiv063     : $("#shtListDiv063"), 
				grdId115          : $("#shtListGrd115"),
				grdPgText115      : "#shtListPg115"       ,
	            fatherDiv115     : $("#shtListDiv115"), 
				grdIdTM          : $("#shtListGrdTM"),
				grdPgTextTM      : "#shtListPgTM"       ,
	            fatherDivTM      : $("#shtListDivTM"),
	            grdIdOther          : $("#shtListGrdOther"),
				grdPgTextOther      : "#shtListPgOther"       ,
	            fatherDivOther      : $("#shtListDivOther")
			}
		};
	  var btnQuery ={
		  f1    : $("#f1_query_btn")
	  };
	  var toolFunc = {
		  iniClear:function(){
			controlsQuery.boxidTxt.val("");
			controlsQuery.mainGrd.grdId003.jqGrid("clearGridData");
			controlsQuery.mainGrd.grdId006.jqGrid("clearGridData");
			controlsQuery.mainGrd.grdId008.jqGrid("clearGridData");
			controlsQuery.mainGrd.grdId063.jqGrid("clearGridData");
			controlsQuery.mainGrd.grdId115.jqGrid("clearGridData");
			controlsQuery.mainGrd.grdIdTM.jqGrid("clearGridData");
			controlsQuery.mainGrd.grdIdOther.jqGrid("clearGridData");
			$("form").submit(function(){
                return false ;
             });
		  }
	  };
	  /**
	   * 获取打印机
	   */
	  function getPrinters(){
		  controlsQuery.print2Select.append("<option ></option>");
		  if (printers !== undefined) {
			  for(var i=0; i<printers.length; i++){
				  controlsQuery.print2Select.append("<option>"+ printers[i] +"</option>");
			  }
			  controlsQuery.print2Select.select2({
		 	    	theme : "bootstrap"
		 	    });
		  }
	  }
	  /**
		 * All button click function
		 * 
		 * @type {Object}
		 */
	  var btnFunc = {
	        // Query
	        f1_func : function(){
		  		var printTyp = "B";//补印
	        	var inObj,outObj,mdlId,cusInfoFst,checkUser;
	        	var prd_id = [],mtrl_box_id = [],grade = [];
	        	var proc_id = $("#proc_id").val();
	        	var box_id = controlsQuery.boxidTxt.val();
	        	
	        	if(!proc_id){
	        		showErrorDialog("","请选择补印站点！");
	                return false;
	        	}
	        	if( !box_id){
	        		showErrorDialog("","请输入箱号！");
	                return false;
	        	}
	        	inObj={
         			   trx_id  : VAL.T_XPINQDEF,
        			   action_flg : "O"   ,
                       box_id  : box_id,
                       proc_id  : proc_id,
                       check_flg : 'Y'
	        	}
	        	outObj =  comTrxSubSendPostJson(inObj);
	        	if( outObj.rtn_code == VAL.NORMAL ){
           		   if(proc_id == "JBQA"){
        			 mdlId = outObj.mdl_id_fk;
        		   }else if(proc_id == "DMQA"){
        			 mdlId = outObj.fm_mdl_id_fk;
        		   }
				   if(outObj.cus_info_fst.length == 0){
					   cusInfoFst = "";
				   }else{
					   cusInfoFst = outObj.cus_info_fst;
				   }
				   if(outObj.check_user.length == 0){
					   checkUser = "";
				   }else{
					   checkUser = outObj.check_user;
				   }
           		   var totalModelCnt = parseInt(outObj.prd_cnt,10) * parseInt(outObj.model_cnt,10);
           		   if(outObj.cus_id == "003"){
           			   controlsQuery.mainGrd.fatherDiv003.show();
           			   controlsQuery.mainGrd.fatherDiv006.hide();
           			   controlsQuery.mainGrd.fatherDiv008.hide();
           			   controlsQuery.mainGrd.fatherDiv063.hide();
           			   controlsQuery.mainGrd.fatherDiv115.hide();
           			   controlsQuery.mainGrd.fatherDivTM.hide();
           			   controlsQuery.mainGrd.fatherDivOther.hide();
           			   resizeFnc();
           			   var oary = outObj.prd_cnt > 1 ? outObj.oary003 : [outObj.oary003];
           			   var r_yjyc_cnt = [],r_llhx_cnt = [],r_atd_cnt = [],r_pl_cnt = [],r_lh_cnt = [],
           			   		r_qj_cnt = [],r_hs_cnt = [],r_bw_cnt = [],r_other = [],r_cnt = [],
           			   		c_pl_cnt = [],c_lh_cnt = [],c_qj_cnt = [],c_cj_cnt = [],c_ly_cnt = [],c_lsw_cnt = [],
           			   		c_td_cnt = [],c_lhy_cnt = [],c_ad_cnt = [],c_hs_cnt = [],c_bw_cnt = [],c_other = [],c_cnt = [],
           			   		remark = [];
//           			   if(outObj.ope_eff_flg == "Y"){// 原箱
//              			   for(var i=0; i<outObj.std_qty ; i++ ){
//              				   for(var j=0; j<outObj.prd_cnt ; j++){
//              					   if((i+1) == oary[j].slot_no){
//              						   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
//              						   mtrl_box_id[i] = oary[j].mtrl_box_id_fk;
//              						   grade[i] = oary[j].prd_grade;
//              						   r_yjyc_cnt[i] = oary[j].r_yjyc_cnt;
//              						   r_llhx_cnt[i] = oary[j].r_llhx_cnt;
//              						   r_atd_cnt[i] = oary[j].r_atd_cnt;
//              						   r_pl_cnt[i] = oary[j].r_pl_cnt;
//              						   r_lh_cnt[i] = oary[j].r_lh_cnt;
//              						   r_qj_cnt[i] = oary[j].r_qj_cnt;
//              						   r_hs_cnt[i] = oary[j].r_hs_cnt;
//              						   r_bw_cnt[i] = oary[j].r_bw_cnt;
//              						   r_other[i] = oary[j].r_other;
//              						   r_cnt[i] = parseInt(oary[j].r_llhx_cnt,10) + parseInt(oary[j].r_atd_cnt,10) + parseInt(oary[j].r_pl_cnt,10)+ parseInt(oary[j].r_lh_cnt,10) +
//              						   		parseInt(oary[j].r_qj_cnt,10) + parseInt(oary[j].r_hs_cnt,10) + parseInt(oary[j].r_bw_cnt,10) + parseInt(oary[j].r_other,10);
//              						   c_pl_cnt[i] = oary[j].c_pl_cnt;
//              						   c_lh_cnt[i] = oary[j].c_lh_cnt;
//              						   c_qj_cnt[i] = oary[j].c_qj_cnt;
//              						   c_cj_cnt[i] = oary[j].c_cj_cnt;
//              						   c_ly_cnt[i] = oary[j].c_ly_cnt;
//              						   c_lsw_cnt[i] = oary[j].c_lsw_cnt;
//              						   c_td_cnt[i] = oary[j].c_td_cnt;
//              						   c_lhy_cnt[i] = oary[j].c_lhy_cnt;
//              						   c_ad_cnt[i] = oary[j].c_ad_cnt;
//              						   c_hs_cnt[i] = oary[j].c_hs_cnt;
//              						   c_bw_cnt[i] = oary[j].c_bw_cnt;
//              						   c_other[i] = oary[j].c_other;
//              						   c_cnt[i] = parseInt(oary[j].c_pl_cnt,10) + parseInt(oary[j].c_lh_cnt,10) + parseInt(oary[j].c_qj_cnt,10)+ parseInt(oary[j].c_cj_cnt,10) +
//              						   		parseInt(oary[j].c_ly_cnt,10) + parseInt(oary[j].c_lsw_cnt,10) + parseInt(oary[j].c_td_cnt,10) + parseInt(oary[j].c_lhy_cnt,10) +
//              						   		parseInt(oary[j].c_ad_cnt,10) + parseInt(oary[j].c_hs_cnt,10) + parseInt(oary[j].c_bw_cnt,10) + parseInt(oary[j].c_other,10);
//              						   if(oary[j].remark.length == 0){
//              							   remark[i] = "";
//              						   }else{
//              							   remark[i] = oary[j].remark;
//              						   }
//              						   break;
//              					   }
//              					   if(j == parseInt(outObj.prd_cnt,10) - 1){
//              						   prd_id[i] = mtrl_box_id[i] = grade[i] = "";
//              						   r_yjyc_cnt[i] = r_llhx_cnt[i] = r_atd_cnt[i] = r_pl_cnt[i] = "";
//              						   r_lh_cnt[i] = r_qj_cnt[i] = r_hs_cnt[i] = r_bw_cnt[i] = r_other[i] = "";
//              						   r_cnt[i] = "";
//              						   c_pl_cnt[i] = c_lh_cnt[i] = c_qj_cnt[i] = c_cj_cnt[i] = "";
//              						   c_ly_cnt[i] = c_lsw_cnt[i] = c_td_cnt[i] = c_lhy_cnt[i] = "";
//              						   c_ad_cnt[i] = c_hs_cnt[i] = c_bw_cnt[i] = c_other[i] = "";
//              						   c_cnt[i] = "";
//              						   remark[i] = "";
//              					   }
//              				   }
//              			   }
//           			   }else{// 非原箱，按照产品开始时间ASC
               			   for(var j=0; j<outObj.prd_cnt ; j++){
               				   prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
               				   mtrl_box_id[j] = oary[j].mtrl_box_id_fk;
               				   grade[j] = oary[j].prd_grade;
               				   r_yjyc_cnt[j] = oary[j].r_yjyc_cnt;
               				   r_llhx_cnt[j] = oary[j].r_llhx_cnt;
               				   r_atd_cnt[j] = oary[j].r_atd_cnt;
               				   r_pl_cnt[j] = oary[j].r_pl_cnt;
               				   r_lh_cnt[j] = oary[j].r_lh_cnt;
               				   r_qj_cnt[j] = oary[j].r_qj_cnt;
               				   r_hs_cnt[j] = oary[j].r_hs_cnt;
               				   r_bw_cnt[j] = oary[j].r_bw_cnt;
               				   r_other[j] = oary[j].r_other;
               				   r_cnt[j] = parseInt(oary[j].r_llhx_cnt,10) + parseInt(oary[j].r_atd_cnt,10) + parseInt(oary[j].r_pl_cnt,10)+ parseInt(oary[j].r_lh_cnt,10) +
               				   			parseInt(oary[j].r_qj_cnt,10) + parseInt(oary[j].r_hs_cnt,10) + parseInt(oary[j].r_bw_cnt,10) + parseInt(oary[j].r_other,10);
               				   c_pl_cnt[j] = oary[j].c_pl_cnt;
               				   c_lh_cnt[j] = oary[j].c_lh_cnt;
               				   c_qj_cnt[j] = oary[j].c_qj_cnt;
               				   c_cj_cnt[j] = oary[j].c_cj_cnt;
               				   c_ly_cnt[j] = oary[j].c_ly_cnt;
               				   c_lsw_cnt[j] = oary[j].c_lsw_cnt;
               				   c_td_cnt[j] = oary[j].c_td_cnt;
               				   c_lhy_cnt[j] = oary[j].c_lhy_cnt;
               				   c_ad_cnt[j] = oary[j].c_ad_cnt;
               				   c_hs_cnt[j] = oary[j].c_hs_cnt;
               				   c_bw_cnt[j] = oary[j].c_bw_cnt;
               				   c_other[j] = oary[j].c_other;
               				   c_cnt[j] = parseInt(oary[j].c_pl_cnt,10) + parseInt(oary[j].c_lh_cnt,10) + parseInt(oary[j].c_qj_cnt,10)+ parseInt(oary[j].c_cj_cnt,10) +
      				   				parseInt(oary[j].c_ly_cnt,10) + parseInt(oary[j].c_lsw_cnt,10) + parseInt(oary[j].c_td_cnt,10) + parseInt(oary[j].c_lhy_cnt,10) +
      				   				parseInt(oary[j].c_ad_cnt,10) + parseInt(oary[j].c_hs_cnt,10) + parseInt(oary[j].c_bw_cnt,10) + parseInt(oary[j].c_other,10);
               				   if(oary[j].remark.length == 0){
               					   remark[j] = "";
               				   }else{
               					   remark[j] = oary[j].remark; 
               				   }  
               			   }
               			   if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
    	           				for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
    	         					 prd_id[i] = mtrl_box_id[i] = grade[i] = "";
    	         					 r_yjyc_cnt[i] = r_llhx_cnt[i] = r_atd_cnt[i] = r_pl_cnt[i] = "";
    	         					 r_lh_cnt[i] = r_qj_cnt[i] = r_hs_cnt[i] = r_bw_cnt[i] = r_other[i] = "";
    	         					 r_cnt[i] = "";
    	         					 c_pl_cnt[i] = c_lh_cnt[i] = c_qj_cnt[i] = c_cj_cnt[i] = "";
    	         					 c_ly_cnt[i] = c_lsw_cnt[i] = c_td_cnt[i] = c_lhy_cnt[i] = "";
    	         					 c_ad_cnt[i] = c_hs_cnt[i] = c_bw_cnt[i] = c_other[i] = "";
    	         					 c_cnt[i] = "";
    	       						 remark[i] = "";
    	   					   }
               			   }
//           			   }
           			   setGridInfo(outObj.oary003,"#shtListGrd003",true);
           			   var printObj = {
           					printTyp : printTyp,
           					procId : proc_id,
           					stdQty : outObj.std_qty,
           					cusId : outObj.cus_id,
           					woId : outObj.wo_id, 
           					boxId : outObj.box_id,
           					shipBoxId : outObj.ship_box_id,
           					toThickness : outObj.to_thickness, 
           					prdCnt : outObj.prd_cnt,  
           					modelCnt : outObj.model_cnt,  
           					totalModelCnt : totalModelCnt,
           					mdlId : mdlId,
           					prdAry : prd_id,         
           					mtrlBoxAry : mtrl_box_id,
           					rYjycAry : r_yjyc_cnt,
           					rLlhxAry : r_llhx_cnt,
           					rAtdAry : r_atd_cnt,
           					rPlAry : r_pl_cnt,
           					rLhAry : r_lh_cnt,
           					rQjAry : r_qj_cnt,
           					rHsAry : r_hs_cnt,
           					rBwAry : r_bw_cnt,
           					rOtherAry : r_other,
           					rCntAry : r_cnt,
           					cPlAry : c_pl_cnt,
           					cLhAry : c_lh_cnt,
           					cQjAry : c_qj_cnt,
           					cCjAry : c_cj_cnt,
           					cLyAry : c_ly_cnt,
           					cLswAry : c_lsw_cnt,
           					cTdAry : c_td_cnt,
           					cLhyAry : c_lhy_cnt,
           					cAdAry : c_ad_cnt,
           					cHsAry : c_hs_cnt,
           					cBwAry : c_bw_cnt,
           					cOther : c_other,
           					cCntAry : c_cnt,
           					remarkAry : remark 
           			   }
           			   label.PrintJBorDMQA003(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
           		   }else if(outObj.cus_id == "006"){// 006客户
//           			   if(proc_id == "JBQA"){
//               			   controlsQuery.mainGrd.fatherDiv003.hide();
//               			   controlsQuery.mainGrd.fatherDiv006.show();
//               			   controlsQuery.mainGrd.fatherDiv008.hide();
//               			   controlsQuery.mainGrd.fatherDiv063.hide();
//               			   controlsQuery.mainGrd.fatherDiv115.hide();
//               			   controlsQuery.mainGrd.fatherDivTM.hide();
//               			   controlsQuery.mainGrd.fatherDivOther.hide();
//               			   resizeFnc();
//               			   var oary = outObj.prd_cnt > 1 ? outObj.oary006 : [outObj.oary006];
//               			   var C_z_pl_cnt = [],C_z_ly_cnt = [],C_z_zw_cnt = [],C_z_skbj_cnt = [],C_z_atd_cnt = [],C_z_hs_cnt = [],
//               			   		C_l_pl_cnt = [],C_l_yjyc_cnt = [],C_l_ad_cnt = [],C_l_mjad_cnt = [],C_l_hs_cnt = [],C_l_gly_cnt = [],
//               			   		T_z_zw_cnt = [],T_z_skbj_cnt = [],T_z_atd_cnt = [],T_z_hs_cnt = [],
//               			   		T_l_ad_cnt = [],T_l_mjad_cnt = [],T_l_hs_cnt = [],T_l_gly_cnt = [],
//               			   		totalCnt = [],
//               			   		status = [],remark = [];
//               			   if(outObj.ope_eff_flg == "Y"){// 原箱
//               				   for(var i=0; i<outObj.std_qty ; i++ ){
//               					   for(var j=0; j<outObj.prd_cnt ; j++){
//               						   if((i+1) == oary[j].slot_no){
//               							   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
//               							   mtrl_box_id[i] = oary[j].mtrl_box_id_fk;
//               							   C_z_pl_cnt[i] = oary[j].C_z_pl_cnt;
//               							   C_z_ly_cnt[i] = oary[j].C_z_ly_cnt;
//               							   C_z_zw_cnt[i] = oary[j].C_z_zw_cnt;
//               							   C_z_skbj_cnt[i] = oary[j].C_z_skbj_cnt;
//               							   C_z_atd_cnt[i] = oary[j].C_z_atd_cnt;
//               							   C_z_hs_cnt[i] = oary[j].C_z_hs_cnt;
//               							   C_l_pl_cnt[i] = oary[j].C_l_pl_cnt;
//               							   C_l_yjyc_cnt[i] = oary[j].C_l_yjyc_cnt;
//               							   C_l_ad_cnt[i] = oary[j].C_l_ad_cnt;
//               							   C_l_mjad_cnt[i] = oary[j].C_l_mjad_cnt;
//               							   C_l_hs_cnt[i] = oary[j].C_l_hs_cnt;
//               							   C_l_gly_cnt[i] = oary[j].C_l_gly_cnt;
//               							   T_z_zw_cnt[i] = oary[j].T_z_zw_cnt;
//               							   T_z_skbj_cnt[i] = oary[j].T_z_skbj_cnt;
//               							   T_z_atd_cnt[i] = oary[j].T_z_atd_cnt;
//               							   T_z_hs_cnt[i] = oary[j].T_z_hs_cnt;
//               							   T_l_ad_cnt[i] = oary[j].T_l_ad_cnt;
//               							   T_l_mjad_cnt[i] = oary[j].T_l_mjad_cnt;
//               							   T_l_hs_cnt[i] = oary[j].T_l_hs_cnt;
//               							   T_l_gly_cnt[i] = oary[j].T_l_gly_cnt;
//               							   totalCnt[i] = parseInt(oary[j].C_z_pl_cnt,10) + parseInt(oary[j].C_z_ly_cnt,10) + parseInt(oary[j].C_z_zw_cnt,10) + parseInt(oary[j].C_z_skbj_cnt,10)
//               							   		+ parseInt(oary[j].C_z_atd_cnt,10) + parseInt(oary[j].C_z_hs_cnt,10) + parseInt(oary[j].C_l_pl_cnt,10)
//               							   		+ parseInt(oary[j].C_l_ad_cnt,10) + parseInt(oary[j].C_l_mjad_cnt,10) + parseInt(oary[j].C_l_hs_cnt,10) + parseInt(oary[j].C_l_gly_cnt,10)
//               							   		+ parseInt(oary[j].T_z_zw_cnt,10) + parseInt(oary[j].T_z_skbj_cnt,10) + parseInt(oary[j].T_z_atd_cnt,10) + parseInt(oary[j].T_z_hs_cnt,10)
//               							   		+ parseInt(oary[j].T_l_ad_cnt,10) + parseInt(oary[j].T_l_mjad_cnt,10) + parseInt(oary[j].T_l_hs_cnt,10) + parseInt(oary[j].T_l_gly_cnt,10);
//               							   if(oary[j].remark.length == 0){
//               								   remark[i] = ""; 
//               							   }else{
//               								   remark[i] = oary[j].remark;  
//               							   }
//               							   status[i] = "已减薄";  
//               							   break;
//               						   }
//               						   if(j == parseInt(outObj.prd_cnt,10) - 1){
//               							   prd_id[i] = mtrl_box_id[i] = "";
//               							   C_z_pl_cnt[i] = C_z_ly_cnt[i] = C_z_zw_cnt[i] = C_z_skbj_cnt[i] = C_z_atd_cnt[i] = C_z_hs_cnt[i]= "";
//               							   C_l_pl_cnt[i] = C_l_yjyc_cnt[i] = C_l_ad_cnt[i] = C_l_mjad_cnt[i] = C_l_hs_cnt[i] = C_l_gly_cnt[i] = "";
//               							   T_z_zw_cnt[i] = T_z_skbj_cnt[i] = T_z_atd_cnt[i] = T_z_hs_cnt[i] = "";
//               							   T_l_ad_cnt[i] = T_l_mjad_cnt[i] = T_l_hs_cnt[i] = T_l_gly_cnt[i] = totalCnt[i] ="";
//               							   status[i] = remark[i] = "";
//               						   }
//               					   }
//               				   }
//               			   }else{// 非原箱
//                  			  for(var j=0; j<outObj.prd_cnt ; j++){
//                  				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
//                  				  mtrl_box_id[j] = oary[j].mtrl_box_id_fk;
//                  				  C_z_pl_cnt[j] = oary[j].C_z_pl_cnt;
//                  				  C_z_ly_cnt[j] = oary[j].C_z_ly_cnt;
//                  				  C_z_zw_cnt[j] = oary[j].C_z_zw_cnt;
//                  				  C_z_skbj_cnt[j] = oary[j].C_z_skbj_cnt;
//                  				  C_z_atd_cnt[j] = oary[j].C_z_atd_cnt;
//                  				  C_z_hs_cnt[j] = oary[j].C_z_hs_cnt;
//                  				  C_l_pl_cnt[j] = oary[j].C_l_pl_cnt;
//                  				  C_l_yjyc_cnt[j] = oary[j].C_l_yjyc_cnt;
//                  				  C_l_ad_cnt[j] = oary[j].C_l_ad_cnt;
//                  				  C_l_mjad_cnt[j] = oary[j].C_l_mjad_cnt;
//                  				  C_l_hs_cnt[j] = oary[j].C_l_hs_cnt;
//                  				  C_l_gly_cnt[j] = oary[j].C_l_gly_cnt;
//                  				  T_z_zw_cnt[j] = oary[j].T_z_zw_cnt;
//                  				  T_z_skbj_cnt[j] = oary[j].T_z_skbj_cnt;
//                  				  T_z_atd_cnt[j] = oary[j].T_z_atd_cnt;
//                  				  T_z_hs_cnt[j] = oary[j].T_z_hs_cnt;
//                  				  T_l_ad_cnt[j] = oary[j].T_l_ad_cnt;
//                  				  T_l_mjad_cnt[j] = oary[j].T_l_mjad_cnt;
//                  				  T_l_hs_cnt[j] = oary[j].T_l_hs_cnt;
//                  				  T_l_gly_cnt[j] = oary[j].T_l_gly_cnt;
//                  				  totalCnt[j] = parseInt(oary[j].C_z_pl_cnt,10) + parseInt(oary[j].C_z_ly_cnt,10) + parseInt(oary[j].C_z_zw_cnt,10) + parseInt(oary[j].C_z_skbj_cnt,10)
//                  				  		+ parseInt(oary[j].C_z_atd_cnt,10) + parseInt(oary[j].C_z_hs_cnt,10) + parseInt(oary[j].C_l_pl_cnt,10) + parseInt(oary[j].C_l_yjyc_cnt,10)
//                  				  		+ parseInt(oary[j].C_l_ad_cnt,10) + parseInt(oary[j].C_l_mjad_cnt,10) + parseInt(oary[j].C_l_hs_cnt,10) + parseInt(oary[j].C_l_gly_cnt,10)
//                  				  		+ parseInt(oary[j].T_z_zw_cnt,10) + parseInt(oary[j].T_z_skbj_cnt,10) + parseInt(oary[j].T_z_atd_cnt,10) + parseInt(oary[j].T_z_hs_cnt,10)
//                  				  		+ parseInt(oary[j].T_l_ad_cnt,10) + parseInt(oary[j].T_l_mjad_cnt,10) + parseInt(oary[j].T_l_hs_cnt,10) + parseInt(oary[j].T_l_gly_cnt,10);
//                  				  if(oary[j].remark.length == 0){
//                  					  remark[j] = ""; 
//                  				  }else{
//                  					  remark[j] = oary[j].remark;  
//                  				  }
//                  				  status[j] = "已减薄";  
//                  			  }
//                  			  if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
//                  				  for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
//          							   prd_id[i] = mtrl_box_id[i] = "";
//           							   C_z_pl_cnt[i] = C_z_ly_cnt[i] = C_z_zw_cnt[i] = C_z_skbj_cnt[i] = C_z_atd_cnt[i] = C_z_hs_cnt[i]= "";
//           							   C_l_pl_cnt[i] = C_l_yjyc_cnt[i] = C_l_ad_cnt[i] = C_l_mjad_cnt[i] = C_l_hs_cnt[i] = C_l_gly_cnt[i] = "";
//           							   T_z_zw_cnt[i] = T_z_skbj_cnt[i] = T_z_atd_cnt[i] = T_z_hs_cnt[i] = "";
//           							   T_l_ad_cnt[i] = T_l_mjad_cnt[i] = T_l_hs_cnt[i] = T_l_gly_cnt[i] = totalCnt[i] ="";
//           							   status[i] = remark[i] = "";
//                  				  }
//                  			  }
//               			   }
//               			   setGridInfo(outObj.oary006,"#shtListGrd006",true);
//               			   var printObj = {
//               				   printTyp : printTyp,
//               				   procId : proc_id,
//     						   stdQty : outObj.std_qty,
//     						   cusId : outObj.cus_id,
//     						   actSoId : outObj.act_so_id,
//     						   woId : outObj.wo_id,
//     						   boxId : outObj.box_id,
//     						   toThickness : outObj.to_thickness,
//     						   prdCnt : outObj.prd_cnt,
//     						   modelCnt : outObj.model_cnt,
//     						   totalModelCnt : totalModelCnt,
//     						   mdlId : mdlId,
//     						   prdAry : prd_id,
//     						   mtrlBoxAry : mtrl_box_id,
//     						   czPlAry : C_z_pl_cnt,
//     						   czLyAry : C_z_ly_cnt,
//     						   czZwAry : C_z_zw_cnt,
//     						   czSkbjAry : C_z_skbj_cnt,
//     						   czAtdAry : C_z_atd_cnt,
//     						   czHsAry : C_z_hs_cnt,
//     						   clPlAry : C_l_pl_cnt,
//     						   clYjycAry : C_l_yjyc_cnt,
//     						   clAdAry : C_l_ad_cnt,
//     						   clMjadAry : C_l_mjad_cnt,
//     						   clHsAry : C_l_hs_cnt,
//     						   clGlyAry : C_l_gly_cnt,
//     						   tzZwAry : T_z_zw_cnt,
//     						   tzSkbjAry : T_z_skbj_cnt,
//     						   tzAtdAry : T_z_atd_cnt,
//     						   tzHsAry : T_z_hs_cnt,
//     						   tlAdAry : T_l_ad_cnt,
//     						   tlMjadAry : T_l_mjad_cnt,
//     						   tlHsAry : T_l_hs_cnt,
//     						   tlGlyAry : T_l_gly_cnt,
//     						   totalAry : totalCnt,
//     						   statusAry : status,
//     						   remarkAry : remark
//               			   };
//               			   label.PrintJBQA006(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
//           			   }else if(proc_id == "DMQA"){// 006DMQA和028,071,088客户的出货信息表格式一致
               			   controlsQuery.mainGrd.fatherDiv003.hide();
               			   controlsQuery.mainGrd.fatherDiv006.hide();
               			   controlsQuery.mainGrd.fatherDiv008.hide();
               			   controlsQuery.mainGrd.fatherDiv063.hide();
               			   controlsQuery.mainGrd.fatherDiv115.hide();
               			   controlsQuery.mainGrd.fatherDivTM.show();
               			   controlsQuery.mainGrd.fatherDivOther.hide();
               			   resizeFnc();
               			   var oary = outObj.prd_cnt > 1 ? outObj.oaryC : [outObj.oaryC];
     					   var yjyc_cnt = [],sy_cnt = [],C_hs_cnt = [],C_ad_cnt = [],
     					   		C_td_cnt = [],C_shbj_cnt = [],C_ps_cnt = [],C_zw_cnt = [],C_ly_cnt = [],
     					   		C_other = [],T_hs_cnt = [],T_ad_cnt = [],T_td_cnt = [],
     					   		T_shbj_cnt = [],T_ps_cnt = [],T_zw_cnt = [],T_ly_cnt = [],T_other = [],
     					   		remark = [];
                  		   if(outObj.ope_eff_flg == "Y"){// 原箱
                  			   for(var i=0; i<outObj.std_qty ; i++ ){
                  				   for(var j=0; j<outObj.prd_cnt ; j++){
                  					   if((i+1) == oary[j].slot_no){
                  						   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
                  						   yjyc_cnt[i] = oary[j].yjyc_cnt;
                  						   sy_cnt[i] = oary[j].sy_cnt;
                  						   C_hs_cnt[i] = oary[j].C_hs_cnt;
                  						   C_ad_cnt[i] = oary[j].C_ad_cnt;
                  						   C_td_cnt[i] = oary[j].C_td_cnt;
                  						   C_shbj_cnt[i] = oary[j].C_shbj_cnt;
                  						   C_ps_cnt[i] = oary[j].C_ps_cnt;
                  						   C_zw_cnt[i] = oary[j].C_zw_cnt;
                  						   C_ly_cnt[i] = oary[j].C_ly_cnt;
                  						   C_other[i] = oary[j].C_other;
                  						   T_hs_cnt[i] = oary[j].T_hs_cnt;
                  						   T_ad_cnt[i] = oary[j].T_ad_cnt;
                  						   T_td_cnt[i] = oary[j].T_td_cnt;
                  						   T_shbj_cnt[i] = oary[j].T_shbj_cnt;
                  						   T_ps_cnt[i] = oary[j].T_ps_cnt;
                  						   T_zw_cnt[i] = oary[j].T_zw_cnt;
                  						   T_ly_cnt[i] = oary[j].T_ly_cnt;
                  						   T_other[i] = oary[j].T_other; 
                  						   if(oary[j].remark.length == 0){
                  							 remark[i] = "";
                  						   }else{
                      						 remark[i] = oary[j].remark;
                  						   }
                  						   break;
                  					   }
                  					   if(j == parseInt(outObj.prd_cnt,10) - 1){
                  						   prd_id[i] = "";
                  						   yjyc_cnt[i] = sy_cnt[i] = "";
                  						   C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] =  C_other[i] = "";
                  						   T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = "";
                  						   remark[i] = "";
                  					   }
                  				   }
                  			   }
                  		   }else{// 非原箱
                  			  for(var j=0; j<outObj.prd_cnt ; j++){
                  				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
                  				  yjyc_cnt[j] = oary[j].yjyc_cnt;
                  				  sy_cnt[j] = oary[j].sy_cnt;
                  				  C_hs_cnt[j] = oary[j].C_hs_cnt;
                  				  C_ad_cnt[j] = oary[j].C_ad_cnt;
                  				  C_td_cnt[j] = oary[j].C_td_cnt;
                  				  C_shbj_cnt[j] = oary[j].C_shbj_cnt;
                  				  C_ps_cnt[j] = oary[j].C_ps_cnt;
                  				  C_zw_cnt[j] = oary[j].C_zw_cnt;
                  				  C_ly_cnt[j] = oary[j].C_ly_cnt;
                  				  C_other[j] = oary[j].C_other;
                  				  T_hs_cnt[j] = oary[j].T_hs_cnt;
                  				  T_ad_cnt[j] = oary[j].T_ad_cnt;
                  				  T_td_cnt[j] = oary[j].T_td_cnt;
                  				  T_shbj_cnt[j] = oary[j].T_shbj_cnt;
                  				  T_ps_cnt[j] = oary[j].T_ps_cnt;
                  				  T_zw_cnt[j] = oary[j].T_zw_cnt;
                  				  T_ly_cnt[j] = oary[j].T_ly_cnt;
                  				  T_other[j] = oary[j].T_other;
                  				  if(oary[j].remark.length == 0){
              						 remark[j] = "";
                  				  }else{
              						 remark[j] = oary[j].remark;
                  				  }
                  			  }
                  			  if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
                  				  for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
                  					  prd_id[i] = "";
                  					  yjyc_cnt[i] = sy_cnt[i] = "";
                  					  C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] = C_other[i] = "";
                  					  T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = ""; 
                  					  remark[i] = "";
                  				  }
                  			  }
                  		   }

                  		   setGridInfo(outObj.oaryC,"#shtListGrdTM",true);
                  		   var printObj = {
                  			   printTyp : printTyp,
                  			   procId : proc_id,
    						   stdQty : outObj.std_qty,
    						   cusId : outObj.cus_id,
    						   woId : outObj.wo_id,
    						   boxId : outObj.box_id,
//    						   shipBoxId : outObj.ship_box_id,
    						   toThickness : outObj.to_thickness,
    						   prdCnt : outObj.prd_cnt,
    						   modelCnt : outObj.model_cnt,
    						   totalModelCnt : totalModelCnt,
    						   whole_def_cnt : parseInt(outObj.l_def_cnt,10) + parseInt(outObj.p_def_cnt,10),
    						   whole_yield : outObj.whole_yield,
    						   checkUser : checkUser,
    						   mdlId : mdlId,
    						   prdAry : prd_id,
    						   yjycAry : yjyc_cnt,
    						   syAry : sy_cnt,
    						   cHsAry : C_hs_cnt,
    						   cAdAry : C_ad_cnt,
    						   cTdAry : C_td_cnt,
    						   cShbjAry : C_shbj_cnt,
    						   cPsAry : C_ps_cnt,
    						   cZwAry : C_zw_cnt,
    						   cLyAry : C_ly_cnt,
    						   cOtherAry : C_other,
    						   tHsAry : T_hs_cnt,
    						   tAdAry : T_ad_cnt,
    						   tTdAry : T_td_cnt,
    						   tShbjAry : T_shbj_cnt,
    						   tPsAry : T_ps_cnt,
    						   tZwAry : T_zw_cnt,
    						   tLyAry : T_ly_cnt,
    						   tOtherAry : T_other,
    						   remarkAry : remark
           		        	};
           		        	label.PrintJBorDMQA2(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
//                	   }
           		   }else if(outObj.cus_id == "008"){//008 HSD客户
            		   controlsQuery.mainGrd.fatherDiv003.hide();
            		   controlsQuery.mainGrd.fatherDiv006.hide();
            		   controlsQuery.mainGrd.fatherDiv008.show();
            		   controlsQuery.mainGrd.fatherDiv063.hide();
            		   controlsQuery.mainGrd.fatherDiv115.hide();
           			   controlsQuery.mainGrd.fatherDivTM.hide();
           			   controlsQuery.mainGrd.fatherDivOther.hide();
           			   resizeFnc();
           			   var oary = outObj.prd_cnt > 1 ? outObj.oary008 : [outObj.oary008];
           			   var l_ly_cnt = [],l_llxo_cnt = [],l_hnbl_cnt = [],l_pl_cnt = [],
           			   		z_c_skbj_cnt = [],z_c_dqx_cnt = [],z_c_xqx_cnt = [],z_c_wj_cnt = [],
           			   		z_t_skbj_cnt = [],z_t_dqx_cnt = [],z_t_xqx_cnt = [],z_t_wj_cnt = [],z_t_pit_cnt = [],
           			   		jbpl_cnt = [],jbly_cnt = [],status = [],remark = [];
//           			   if(outObj.ope_eff_flg == "Y"){// 原箱
//           				   for(var i=0; i<outObj.std_qty ; i++ ){
//           					   for(var j=0; j<outObj.prd_cnt ; j++){
//           						   if((i+1) == oary[j].slot_no){
//           							   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
//           							   l_ly_cnt[i] = oary[j].l_ly_cnt;
//           							   l_llxo_cnt[i] = oary[j].l_llxo_cnt;
//           							   l_hnbl_cnt[i] = oary[j].l_hnbl_cnt;
//           							   l_pl_cnt[i] = oary[j].l_pl_cnt;
//           							   z_c_skbj_cnt[i] = oary[j].z_c_skbj_cnt;
//           							   z_c_dqx_cnt[i] = oary[j].z_c_dqx_cnt;
//           							   z_c_xqx_cnt[i] = oary[j].z_c_xqx_cnt;
//           							   z_c_wj_cnt[i] = oary[j].z_c_wj_cnt;
//           							   z_t_skbj_cnt[i] = oary[j].z_t_skbj_cnt;
//           							   z_t_dqx_cnt[i] = oary[j].z_t_dqx_cnt;
//           							   z_t_xqx_cnt[i] = oary[j].z_t_xqx_cnt;
//           							   z_t_wj_cnt[i] = oary[j].z_t_wj_cnt;
//           							   z_t_pit_cnt[i] = oary[j].z_t_pit_cnt;
//           							   jbpl_cnt[i] = oary[j].jbpl_cnt;
//           							   jbly_cnt[i] = oary[j].jbly_cnt;
//           							   if(oary[j].remark.length == 0){
//           								   remark[i] = "";
//           							   }else{
//           								   remark[i] = oary[j].remark;
//           							   }
//           							   if(proc_id == "JBQA"){
//         								   status[i] = "已减薄";  
//         							   }else if(proc_id == "DMQA"){
//         								   status[i] = "已镀膜"; 
//         							   }
//           							   break;
//           						   }
//           						   if(j == parseInt(outObj.prd_cnt,10) - 1){
//           							   prd_id[i] = "";
//           							   l_ly_cnt[i] = l_llxo_cnt[i] = l_hnbl_cnt[i] = l_pl_cnt[i] = "";
//           							   z_c_skbj_cnt[i] = z_c_dqx_cnt[i] = z_c_xqx_cnt[i] = z_c_wj_cnt[i] = "";
//           							   z_t_skbj_cnt[i] = z_t_dqx_cnt[i] = z_t_xqx_cnt[i] = z_t_wj_cnt[i] = z_t_pit_cnt[i] = "";
//           							   jbpl_cnt[i] = jbly_cnt[i] = "";
//           							   status[i] = remark[i] = "";
//           						   }
//           					   }
//           				   }
//           			   }else{// 非原箱
           				   for(var j=0; j<outObj.prd_cnt ; j++){
           					   prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
           					   l_ly_cnt[j] = oary[j].l_ly_cnt;
							   l_llxo_cnt[j] = oary[j].l_llxo_cnt;
							   l_hnbl_cnt[j] = oary[j].l_hnbl_cnt;
							   l_pl_cnt[j] = oary[j].l_pl_cnt;
							   z_c_skbj_cnt[j] = oary[j].z_c_skbj_cnt;
							   z_c_dqx_cnt[j] = oary[j].z_c_dqx_cnt;
							   z_c_xqx_cnt[j] = oary[j].z_c_xqx_cnt;
							   z_c_wj_cnt[j] = oary[j].z_c_wj_cnt;
							   z_t_skbj_cnt[j] = oary[j].z_t_skbj_cnt;
							   z_t_dqx_cnt[j] = oary[j].z_t_dqx_cnt;
							   z_t_xqx_cnt[j] = oary[j].z_t_xqx_cnt;
							   z_t_wj_cnt[j] = oary[j].z_t_wj_cnt;
							   z_t_pit_cnt[j] = oary[j].z_t_pit_cnt;
							   jbpl_cnt[j] = oary[j].jbpl_cnt;
							   jbly_cnt[j] = oary[j].jbly_cnt;
							   if(oary[j].remark.length == 0){
								   remark[j] = "";
							   }else{
								   remark[j] = oary[j].remark;
							   }
							   if(proc_id == "JBQA"){
   								   status[j] = "已减薄";  
   							   }else if(proc_id == "DMQA"){
   								   status[j] = "已镀膜"; 
   							   }
           				   }
           				   if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
           					   for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
           						   prd_id[i] = "";
           						   l_ly_cnt[i] = l_llxo_cnt[i] = l_hnbl_cnt[i] = l_pl_cnt[i] = "";
    							   z_c_skbj_cnt[i] = z_c_dqx_cnt[i] = z_c_xqx_cnt[i] = z_c_wj_cnt[i] = "";
    							   z_t_skbj_cnt[i] = z_t_dqx_cnt[i] = z_t_xqx_cnt[i] = z_t_wj_cnt[i] = z_t_pit_cnt[i] = "";
    							   jbpl_cnt[i] = jbly_cnt[i] = "";
    							   status[i] = remark[i] = "";
           					   }
           				   }
//           			   }
           			   setGridInfo(outObj.oary008,"#shtListGrd008",true);
           			   var printObj = {
           				   printTyp : printTyp,
           				   procId : proc_id,
       					   stdQty : outObj.std_qty,
       					   cusId : outObj.cus_id,
       					   woId : outObj.wo_id,
       					   soId : outObj.so_id,
       					   boxId : outObj.box_id,
       					   cusInfoFst : cusInfoFst,
       					   modelCnt : outObj.model_cnt,
						   totalModelCnt : totalModelCnt,
       					   prdCnt : outObj.prd_cnt,
       					   mdlId : mdlId,
       					   prdAry : prd_id,
						   lLyAry : l_ly_cnt,
						   lLlxoAry : l_llxo_cnt,
						   lHnblAry : l_hnbl_cnt,
						   lPlAry : l_pl_cnt,
						   zcSkbjAry : z_c_skbj_cnt,
						   zcDqxAry : z_c_dqx_cnt,
						   zcXqxAry : z_c_xqx_cnt,
						   zcWjAry : z_c_wj_cnt,
						   ztSkbjAry : z_t_skbj_cnt,
						   ztDqxAry : z_t_skbj_cnt,
						   ztXqxAry : z_t_xqx_cnt,
						   ztWjAry : z_t_wj_cnt,
						   ztPitAry : z_t_pit_cnt,
						   jbPlAry : jbpl_cnt,
						   jbLyAry : jbly_cnt,
						   statusAry : status,
       					   remarkAry : remark
           			   };
           			   label.PrintJBorDMQA008(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
           		   }else if(outObj.cus_id == "063"){// 063客户
           			   controlsQuery.mainGrd.fatherDiv003.hide();
           			   controlsQuery.mainGrd.fatherDiv006.hide();
           			   controlsQuery.mainGrd.fatherDiv008.hide();
           			   controlsQuery.mainGrd.fatherDiv063.show();
           			   controlsQuery.mainGrd.fatherDiv115.hide();
           			   controlsQuery.mainGrd.fatherDivTM.hide();
           			   controlsQuery.mainGrd.fatherDivOther.hide();
           			   resizeFnc();
           			   var oary = outObj.prd_cnt > 1 ? outObj.oary063 : [outObj.oary063];
           			   var status = [], remark = [];
//           			   if(outObj.ope_eff_flg == "Y"){// 原箱
//           				   for(var i=0; i<outObj.std_qty ; i++ ){
//           					   for(var j=0; j<outObj.prd_cnt ; j++){
//           						   if((i+1) == oary[j].slot_no){
//           							   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
//           							   if(proc_id == "JBQA"){
//           								   status[i] = "已减薄";  
//           							   }else if(proc_id == "DMQA"){
//           								   status[i] = "已镀膜"; 
//           							   }
//           							   remark[i] = oary[j].remark;
//           							   break;
//           						   }
//           						   if(j == parseInt(outObj.prd_cnt,10) - 1){
//           							   prd_id[i] = "";
//           							   status[i] = ""; 
//           							   remark[i] = "";
//             					   }
//           					   }
//           				   }
//           			   }else{// 非原箱
               			  for(var j=0; j<outObj.prd_cnt ; j++){
               				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
  							   if(proc_id == "JBQA"){
   								   status[j] = "已减薄";  
   							   }else if(proc_id == "DMQA"){
   								   status[j] = "已镀膜"; 
   							   }
  							 remark[j] = oary[j].remark;
               			  }
               			  if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
               				  for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
               					  prd_id[i] = "";
               					  status[i] = "";
               					  remark[i] = "";
           					   }
           				   }
//               		   }
           			   setGridInfo(outObj.oary063,"#shtListGrd063",true);
           			   var printObj = {
           				   printTyp : printTyp,
           				   procId : proc_id,
  						   stdQty : outObj.std_qty,
  						   cusId : outObj.cus_id,
  						   woId : outObj.wo_id,
  						   soId : outObj.so_id,
  						   boxId : outObj.box_id,
  						   prdCnt : outObj.prd_cnt,
  						   modelCnt : outObj.model_cnt,
  						   totalModelCnt : totalModelCnt,
  						   mdlId : mdlId,
  						   prdAry : prd_id,
  						   statusAry : status,
  						   remarkAry : remark
           			   };
           			   label.PrintJBorDMQA063(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
           		   }else if(outObj.cus_id == "028" ||
           				   outObj.cus_id == "071" ||
           				   outObj.cus_id == "088"){// 天马客户
           			   controlsQuery.mainGrd.fatherDiv003.hide();
           			   controlsQuery.mainGrd.fatherDiv006.hide();
           			   controlsQuery.mainGrd.fatherDiv008.hide();
           			   controlsQuery.mainGrd.fatherDiv063.hide();
           			   controlsQuery.mainGrd.fatherDiv115.hide();
           			   controlsQuery.mainGrd.fatherDivTM.show();
           			   controlsQuery.mainGrd.fatherDivOther.hide();
           			   resizeFnc();
           			   var oary = outObj.prd_cnt > 1 ? outObj.oaryC : [outObj.oaryC];
 					   var yjyc_cnt = [],sy_cnt = [],C_hs_cnt = [],C_ad_cnt = [],
 					   		C_td_cnt = [],C_shbj_cnt = [],C_ps_cnt = [],C_zw_cnt = [],C_ly_cnt = [],
 					   		C_other = [],T_hs_cnt = [],T_ad_cnt = [],T_td_cnt = [],
 					   		T_shbj_cnt = [],T_ps_cnt = [],T_zw_cnt = [],T_ly_cnt = [],T_other = [],
 					   		remark = [];
              		   if(outObj.ope_eff_flg == "Y"){// 原箱
              			   for(var i=0; i<outObj.std_qty ; i++ ){
              				   for(var j=0; j<outObj.prd_cnt ; j++){
              					   if((i+1) == oary[j].slot_no){
              						 prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
              						 yjyc_cnt[i] = oary[j].yjyc_cnt;
              						 sy_cnt[i] = oary[j].sy_cnt;
              						 C_hs_cnt[i] = oary[j].C_hs_cnt;
              						 C_ad_cnt[i] = oary[j].C_ad_cnt;
              						 C_td_cnt[i] = oary[j].C_td_cnt;
              						 C_shbj_cnt[i] = oary[j].C_shbj_cnt;
              						 C_ps_cnt[i] = oary[j].C_ps_cnt;
              						 C_zw_cnt[i] = oary[j].C_zw_cnt;
              						 C_ly_cnt[i] = oary[j].C_ly_cnt;
              						 C_other[i] = oary[j].C_other;
              						 T_hs_cnt[i] = oary[j].T_hs_cnt;
              						 T_ad_cnt[i] = oary[j].T_ad_cnt;
              						 T_td_cnt[i] = oary[j].T_td_cnt;
              						 T_shbj_cnt[i] = oary[j].T_shbj_cnt;
              						 T_ps_cnt[i] = oary[j].T_ps_cnt;
              						 T_zw_cnt[i] = oary[j].T_zw_cnt;
              						 T_ly_cnt[i] = oary[j].T_ly_cnt;
              						 T_other[i] = oary[j].T_other; 
              						 if(oary[j].remark.length == 0){
              							 remark[i] = "";
              						 }else{
                  						 remark[i] = oary[j].remark;
              						 }
              						 break;
              					   }
              					   if(j == parseInt(outObj.prd_cnt,10) - 1){
            						 prd_id[i] = "";
            						 yjyc_cnt[i] = sy_cnt[i] = "";
            						 C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] =  C_other[i] = "";
              						 T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = "";
              						 remark[i] = "";
              					   }
              				   }
              			   }
              		   }else{// 非原箱
              			  for(var j=0; j<outObj.prd_cnt ; j++){
      						 prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
      						 yjyc_cnt[j] = oary[j].yjyc_cnt;
      						 sy_cnt[j] = oary[j].sy_cnt;
      						 C_hs_cnt[j] = oary[j].C_hs_cnt;
      						 C_ad_cnt[j] = oary[j].C_ad_cnt;
      						 C_td_cnt[j] = oary[j].C_td_cnt;
      						 C_shbj_cnt[j] = oary[j].C_shbj_cnt;
      						 C_ps_cnt[j] = oary[j].C_ps_cnt;
      						 C_zw_cnt[j] = oary[j].C_zw_cnt;
      						 C_ly_cnt[j] = oary[j].C_ly_cnt;
      						 C_other[j] = oary[j].C_other;
      						 T_hs_cnt[j] = oary[j].T_hs_cnt;
      						 T_ad_cnt[j] = oary[j].T_ad_cnt;
      						 T_td_cnt[j] = oary[j].T_td_cnt;
      						 T_shbj_cnt[j] = oary[j].T_shbj_cnt;
      						 T_ps_cnt[j] = oary[j].T_ps_cnt;
      						 T_zw_cnt[j] = oary[j].T_zw_cnt;
      						 T_ly_cnt[j] = oary[j].T_ly_cnt;
      						 T_other[j] = oary[j].T_other;
      						 if(oary[j].remark.length == 0){
          						 remark[j] = "";
      						 }else{
          						 remark[j] = oary[j].remark;
      						 }
          				   }
          				   if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
          					   for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
                					 prd_id[i] = "";
            						 yjyc_cnt[i] = sy_cnt[i] = "";
            						 C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] = C_other[i] = "";
              						 T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = ""; 
              						 remark[i] = "";
          					   }
          				   }
              		   }

              		   setGridInfo(outObj.oaryC,"#shtListGrdTM",true);
              		   var printObj = {
              			   printTyp : printTyp,
              			   procId : proc_id,
						   stdQty : outObj.std_qty,
						   cusId : outObj.cus_id,
						   woId : outObj.wo_id,
						   boxId : outObj.box_id,
						   shipBoxId : outObj.ship_box_id,
						   toThickness : outObj.to_thickness,
						   prdCnt : outObj.prd_cnt,
						   modelCnt : outObj.model_cnt,
						   totalModelCnt : totalModelCnt,
						   whole_def_cnt : parseInt(outObj.l_def_cnt,10) + parseInt(outObj.p_def_cnt,10),
						   whole_yield : outObj.whole_yield,
						   checkUser : checkUser,
						   mdlId : mdlId,
						   prdAry : prd_id,
						   yjycAry : yjyc_cnt,
						   syAry : sy_cnt,
						   cHsAry : C_hs_cnt,
						   cAdAry : C_ad_cnt,
						   cTdAry : C_td_cnt,
						   cShbjAry : C_shbj_cnt,
						   cPsAry : C_ps_cnt,
						   cZwAry : C_zw_cnt,
						   cLyAry : C_ly_cnt,
						   cOtherAry : C_other,
						   tHsAry : T_hs_cnt,
						   tAdAry : T_ad_cnt,
						   tTdAry : T_td_cnt,
						   tShbjAry : T_shbj_cnt,
						   tPsAry : T_ps_cnt,
						   tZwAry : T_zw_cnt,
						   tLyAry : T_ly_cnt,
						   tOtherAry : T_other,
						   remarkAry : remark
       		        	};
       		        	label.PrintJBorDMQA2(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
            	   }else if (outObj.cus_id == "098"){// 098客户
           			   if(proc_id == "JBQA"){
               			   controlsQuery.mainGrd.fatherDiv003.hide();
               			   controlsQuery.mainGrd.fatherDiv006.show();
               			   controlsQuery.mainGrd.fatherDiv008.hide();
               			   controlsQuery.mainGrd.fatherDiv063.hide();
               			   controlsQuery.mainGrd.fatherDiv115.hide();
               			   controlsQuery.mainGrd.fatherDivTM.hide();
               			   controlsQuery.mainGrd.fatherDivOther.hide();
               			   resizeFnc();
               			   var oary = outObj.prd_cnt > 1 ? outObj.oary006 : [outObj.oary006];
               			   var C_z_pl_cnt = [],C_z_ly_cnt = [],C_z_zw_cnt = [],C_z_skbj_cnt = [],C_z_atd_cnt = [],C_z_hs_cnt = [],
               			   		C_l_pl_cnt = [],C_l_yjyc_cnt = [],C_l_ad_cnt = [],C_l_mjad_cnt = [],C_l_hs_cnt = [],C_l_gly_cnt = [],
               			   		T_z_zw_cnt = [],T_z_skbj_cnt = [],T_z_atd_cnt = [],T_z_hs_cnt = [],
               			   		T_l_ad_cnt = [],T_l_hs_cnt = [],T_l_gly_cnt = [],
               			   		totalCnt = [],
               			   		status = [],remark = [];
//               			   if(outObj.ope_eff_flg == "Y"){// 原箱
//               				   for(var i=0; i<outObj.std_qty ; i++ ){
//               					   for(var j=0; j<outObj.prd_cnt ; j++){
//               						   if((i+1) == oary[j].slot_no){
//               							   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
//               							   mtrl_box_id[i] = oary[j].mtrl_box_id_fk;
//               							   C_z_pl_cnt[i] = oary[j].C_z_pl_cnt;
//               							   C_z_ly_cnt[i] = oary[j].C_z_ly_cnt;
//               							   C_z_zw_cnt[i] = oary[j].C_z_zw_cnt;
//               							   C_z_skbj_cnt[i] = oary[j].C_z_skbj_cnt;
//               							   C_z_atd_cnt[i] = oary[j].C_z_atd_cnt;
//               							   C_z_hs_cnt[i] = oary[j].C_z_hs_cnt;
//               							   C_l_pl_cnt[i] = oary[j].C_l_pl_cnt;
//               							   C_l_yjyc_cnt[i] = oary[j].C_l_yjyc_cnt;
//               							   C_l_ad_cnt[i] = oary[j].C_l_ad_cnt;
//               							   C_l_mjad_cnt[i] = oary[j].C_l_mjad_cnt;
//               							   C_l_hs_cnt[i] = oary[j].C_l_hs_cnt;
//               							   C_l_gly_cnt[i] = oary[j].C_l_gly_cnt;
//               							   T_z_zw_cnt[i] = oary[j].T_z_zw_cnt;
//               							   T_z_skbj_cnt[i] = oary[j].T_z_skbj_cnt;
//               							   T_z_atd_cnt[i] = oary[j].T_z_atd_cnt;
//               							   T_z_hs_cnt[i] = oary[j].T_z_hs_cnt;
//               							   T_l_ad_cnt[i] = oary[j].T_l_ad_cnt;
//               							   T_l_hs_cnt[i] = oary[j].T_l_hs_cnt;
//               							   T_l_gly_cnt[i] = oary[j].T_l_gly_cnt;
//               							   totalCnt[i] = parseInt(oary[j].C_z_pl_cnt,10) + parseInt(oary[j].C_z_ly_cnt,10) + parseInt(oary[j].C_z_zw_cnt,10) + parseInt(oary[j].C_z_skbj_cnt,10)
//               							   		+ parseInt(oary[j].C_z_atd_cnt,10) + parseInt(oary[j].C_z_hs_cnt,10) + parseInt(oary[j].C_l_pl_cnt,10) + parseInt(oary[j].C_l_yjyc_cnt,10)
//               							   		+ parseInt(oary[j].C_l_ad_cnt,10) + parseInt(oary[j].C_l_mjad_cnt,10) + parseInt(oary[j].C_l_hs_cnt,10) + parseInt(oary[j].C_l_gly_cnt,10)
//               							   		+ parseInt(oary[j].T_z_zw_cnt,10) + parseInt(oary[j].T_z_skbj_cnt,10) + parseInt(oary[j].T_z_atd_cnt,10) + parseInt(oary[j].T_z_hs_cnt,10)
//               							   		+ parseInt(oary[j].T_l_ad_cnt,10) + parseInt(oary[j].T_l_hs_cnt,10) + parseInt(oary[j].T_l_gly_cnt,10);
//               							   if(oary[j].remark.length == 0){
//               								   remark[i] = ""; 
//               							   }else{
//               								   remark[i] = oary[j].remark;  
//               							   }
//               							   status[i] = "已减薄";  
//               							   break;
//               						   }
//               						   if(j == parseInt(outObj.prd_cnt,10) - 1){
//               							   prd_id[i] = mtrl_box_id[i] = "";
//               							   C_z_pl_cnt[i] = C_z_ly_cnt[i] = C_z_zw_cnt[i] = C_z_skbj_cnt[i] = C_z_atd_cnt[i] = C_z_hs_cnt[i]= "";
//               							   C_l_pl_cnt[i] = C_l_yjyc_cnt[i] = C_l_ad_cnt[i] = C_l_mjad_cnt[i] = C_l_hs_cnt[i] = C_l_gly_cnt[i] = "";
//               							   T_z_zw_cnt[i] = T_z_skbj_cnt[i] = T_z_atd_cnt[i] = T_z_hs_cnt[i] = "";
//               							   T_l_ad_cnt[i] = T_l_hs_cnt[i] = T_l_gly_cnt[i] = totalCnt[i] ="";
//               							   status[i] = remark[i] = "";
//               						   }
//               					   }
//               				   }
//               			   }else{// 非原箱
                  			  for(var j=0; j<outObj.prd_cnt ; j++){
                  				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
                  				  mtrl_box_id[j] = oary[j].mtrl_box_id_fk;
                  				  C_z_pl_cnt[j] = oary[j].C_z_pl_cnt;
                  				  C_z_ly_cnt[j] = oary[j].C_z_ly_cnt;
                  				  C_z_zw_cnt[j] = oary[j].C_z_zw_cnt;
                  				  C_z_skbj_cnt[j] = oary[j].C_z_skbj_cnt;
                  				  C_z_atd_cnt[j] = oary[j].C_z_atd_cnt;
                  				  C_z_hs_cnt[j] = oary[j].C_z_hs_cnt;
                  				  C_l_pl_cnt[j] = oary[j].C_l_pl_cnt;
                  				  C_l_yjyc_cnt[j] = oary[j].C_l_yjyc_cnt;
                  				  C_l_ad_cnt[j] = oary[j].C_l_ad_cnt;
                  				  C_l_mjad_cnt[j] = oary[j].C_l_mjad_cnt;
                  				  C_l_hs_cnt[j] = oary[j].C_l_hs_cnt;
                  				  C_l_gly_cnt[j] = oary[j].C_l_gly_cnt;
                  				  T_z_zw_cnt[j] = oary[j].T_z_zw_cnt;
                  				  T_z_skbj_cnt[j] = oary[j].T_z_skbj_cnt;
                  				  T_z_atd_cnt[j] = oary[j].T_z_atd_cnt;
                  				  T_z_hs_cnt[j] = oary[j].T_z_hs_cnt;
                  				  T_l_ad_cnt[j] = oary[j].T_l_ad_cnt;
                  				  T_l_hs_cnt[j] = oary[j].T_l_hs_cnt;
                  				  T_l_gly_cnt[j] = oary[j].T_l_gly_cnt;
                  				  totalCnt[j] = parseInt(oary[j].C_z_pl_cnt,10) + parseInt(oary[j].C_z_ly_cnt,10) + parseInt(oary[j].C_z_zw_cnt,10) + parseInt(oary[j].C_z_skbj_cnt,10)
                  				  		+ parseInt(oary[j].C_z_atd_cnt,10) + parseInt(oary[j].C_z_hs_cnt,10) + parseInt(oary[j].C_l_pl_cnt,10) + parseInt(oary[j].C_l_yjyc_cnt,10)
                  				  		+ parseInt(oary[j].C_l_ad_cnt,10) + parseInt(oary[j].C_l_mjad_cnt,10) + parseInt(oary[j].C_l_hs_cnt,10) + parseInt(oary[j].C_l_gly_cnt,10)
                  				  		+ parseInt(oary[j].T_z_zw_cnt,10) + parseInt(oary[j].T_z_skbj_cnt,10) + parseInt(oary[j].T_z_atd_cnt,10) + parseInt(oary[j].T_z_hs_cnt,10)
                  				  		+ parseInt(oary[j].T_l_ad_cnt,10) + parseInt(oary[j].T_l_hs_cnt,10) + parseInt(oary[j].T_l_gly_cnt,10);
                  				  if(oary[j].remark.length == 0){
                  					  remark[j] = ""; 
                  				  }else{
                  					  remark[j] = oary[j].remark;  
                  				  }
                  				  status[j] = "已减薄";  
                  			  }
                  			  if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
                  				  for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
          							   prd_id[i] = mtrl_box_id[i] = "";
           							   C_z_pl_cnt[i] = C_z_ly_cnt[i] = C_z_zw_cnt[i] = C_z_skbj_cnt[i] = C_z_atd_cnt[i] = C_z_hs_cnt[i]= "";
           							   C_l_pl_cnt[i] = C_l_yjyc_cnt[i] = C_l_ad_cnt[i] = C_l_mjad_cnt[i] = C_l_hs_cnt[i] = C_l_gly_cnt[i] = "";
           							   T_z_zw_cnt[i] = T_z_skbj_cnt[i] = T_z_atd_cnt[i] = T_z_hs_cnt[i] = "";
           							   T_l_ad_cnt[i] = T_l_hs_cnt[i] = T_l_gly_cnt[i] = totalCnt[i] ="";
           							   status[i] = remark[i] = "";
                  				  }
                  			  }
//               			   }
               			   setGridInfo(outObj.oary006,"#shtListGrd006",true);
               			   var printObj = {
               				   printTyp : printTyp,
               				   procId : proc_id,
     						   stdQty : outObj.std_qty,
     						   cusId : outObj.cus_id,
     						   soId : outObj.so_id,
     						   woId : outObj.wo_id,
     						   boxId : outObj.box_id,
     						   toThickness : outObj.to_thickness,
     						   prdCnt : outObj.prd_cnt,
     						   modelCnt : outObj.model_cnt,
     						   totalModelCnt : totalModelCnt,
     						   mdlId : mdlId,
     						   prdAry : prd_id,
     						   mtrlBoxAry : mtrl_box_id,
     						   czPlAry : C_z_pl_cnt,
     						   czLyAry : C_z_ly_cnt,
     						   czZwAry : C_z_zw_cnt,
     						   czSkbjAry : C_z_skbj_cnt,
     						   czAtdAry : C_z_atd_cnt,
     						   czHsAry : C_z_hs_cnt,
     						   clPlAry : C_l_pl_cnt,
     						   clYjycAry : C_l_yjyc_cnt,
     						   clAdAry : C_l_ad_cnt,
     						   clMjadAry : C_l_mjad_cnt,
     						   clHsAry : C_l_hs_cnt,
     						   clGlyAry : C_l_gly_cnt,
     						   tzZwAry : T_z_zw_cnt,
     						   tzSkbjAry : T_z_skbj_cnt,
     						   tzAtdAry : T_z_atd_cnt,
     						   tzHsAry : T_z_hs_cnt,
     						   tlAdAry : T_l_ad_cnt,
     						   tlHsAry : T_l_hs_cnt,
     						   tlGlyAry : T_l_gly_cnt,
     						   totalAry : totalCnt,
     						   statusAry : status,
     						   remarkAry : remark
               			   };
               			   label.PrintJBQA098(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
           			   }else if(proc_id == "DMQA"){// 098DMQA和028,071,088客户的出货信息表格式一致
               			   controlsQuery.mainGrd.fatherDiv003.hide();
               			   controlsQuery.mainGrd.fatherDiv006.hide();
               			   controlsQuery.mainGrd.fatherDiv008.hide();
               			   controlsQuery.mainGrd.fatherDiv063.hide();
               			   controlsQuery.mainGrd.fatherDiv115.hide();
               			   controlsQuery.mainGrd.fatherDivTM.show();
               			   controlsQuery.mainGrd.fatherDivOther.hide();
               			   resizeFnc();
               			   var oary = outObj.prd_cnt > 1 ? outObj.oaryC : [outObj.oaryC];
     					   var yjyc_cnt = [],sy_cnt = [],C_hs_cnt = [],C_ad_cnt = [],
     					   		C_td_cnt = [],C_shbj_cnt = [],C_ps_cnt = [],C_zw_cnt = [],C_ly_cnt = [],
     					   		C_other = [],T_hs_cnt = [],T_ad_cnt = [],T_td_cnt = [],
     					   		T_shbj_cnt = [],T_ps_cnt = [],T_zw_cnt = [],T_ly_cnt = [],T_other = [],
     					   		remark = [];
//                  		   if(outObj.ope_eff_flg == "Y"){// 原箱
//                  			   for(var i=0; i<outObj.std_qty ; i++ ){
//                  				   for(var j=0; j<outObj.prd_cnt ; j++){
//                  					   if((i+1) == oary[j].slot_no){
//                  						   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
//                  						   yjyc_cnt[i] = oary[j].yjyc_cnt;
//                  						   sy_cnt[i] = oary[j].sy_cnt;
//                  						   C_hs_cnt[i] = oary[j].C_hs_cnt;
//                  						   C_ad_cnt[i] = oary[j].C_ad_cnt;
//                  						   C_td_cnt[i] = oary[j].C_td_cnt;
//                  						   C_shbj_cnt[i] = oary[j].C_shbj_cnt;
//                  						   C_ps_cnt[i] = oary[j].C_ps_cnt;
//                  						   C_zw_cnt[i] = oary[j].C_zw_cnt;
//                  						   C_ly_cnt[i] = oary[j].C_ly_cnt;
//                  						   C_other[i] = oary[j].C_other;
//                  						   T_hs_cnt[i] = oary[j].T_hs_cnt;
//                  						   T_ad_cnt[i] = oary[j].T_ad_cnt;
//                  						   T_td_cnt[i] = oary[j].T_td_cnt;
//                  						   T_shbj_cnt[i] = oary[j].T_shbj_cnt;
//                  						   T_ps_cnt[i] = oary[j].T_ps_cnt;
//                  						   T_zw_cnt[i] = oary[j].T_zw_cnt;
//                  						   T_ly_cnt[i] = oary[j].T_ly_cnt;
//                  						   T_other[i] = oary[j].T_other; 
//                  						   if(oary[j].remark.length == 0){
//                  							 remark[i] = "";
//                  						   }else{
//                      						 remark[i] = oary[j].remark;
//                  						   }
//                  						   break;
//                  					   }
//                  					   if(j == parseInt(outObj.prd_cnt,10) - 1){
//                  						   prd_id[i] = "";
//                  						   yjyc_cnt[i] = sy_cnt[i] = "";
//                  						   C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] =  C_other[i] = "";
//                  						   T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = "";
//                  						   remark[i] = "";
//                  					   }
//                  				   }
//                  			   }
//                  		   }else{// 非原箱
                  			  for(var j=0; j<outObj.prd_cnt ; j++){
                  				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
                  				  yjyc_cnt[j] = oary[j].yjyc_cnt;
                  				  sy_cnt[j] = oary[j].sy_cnt;
                  				  C_hs_cnt[j] = oary[j].C_hs_cnt;
                  				  C_ad_cnt[j] = oary[j].C_ad_cnt;
                  				  C_td_cnt[j] = oary[j].C_td_cnt;
                  				  C_shbj_cnt[j] = oary[j].C_shbj_cnt;
                  				  C_ps_cnt[j] = oary[j].C_ps_cnt;
                  				  C_zw_cnt[j] = oary[j].C_zw_cnt;
                  				  C_ly_cnt[j] = oary[j].C_ly_cnt;
                  				  C_other[j] = oary[j].C_other;
                  				  T_hs_cnt[j] = oary[j].T_hs_cnt;
                  				  T_ad_cnt[j] = oary[j].T_ad_cnt;
                  				  T_td_cnt[j] = oary[j].T_td_cnt;
                  				  T_shbj_cnt[j] = oary[j].T_shbj_cnt;
                  				  T_ps_cnt[j] = oary[j].T_ps_cnt;
                  				  T_zw_cnt[j] = oary[j].T_zw_cnt;
                  				  T_ly_cnt[j] = oary[j].T_ly_cnt;
                  				  T_other[j] = oary[j].T_other;
                  				  if(oary[j].remark.length == 0){
              						 remark[j] = "";
                  				  }else{
              						 remark[j] = oary[j].remark;
                  				  }
                  			  }
                  			  if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
                  				  for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
                  					  prd_id[i] = "";
                  					  yjyc_cnt[i] = sy_cnt[i] = "";
                  					  C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] = C_other[i] = "";
                  					  T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = ""; 
                  					  remark[i] = "";
                  				  }
                  			  }
//                  		   }

                  		   setGridInfo(outObj.oaryC,"#shtListGrdTM",true);
                  		   var printObj = {
                  			   printTyp : printTyp,
                  			   procId : proc_id,
    						   stdQty : outObj.std_qty,
    						   cusId : outObj.cus_id,
    						   woId : outObj.wo_id,
    						   boxId : outObj.box_id,
    						   toThickness : outObj.to_thickness,
    						   prdCnt : outObj.prd_cnt,
    						   modelCnt : outObj.model_cnt,
    						   totalModelCnt : totalModelCnt,
    						   whole_def_cnt : parseInt(outObj.l_def_cnt,10) + parseInt(outObj.p_def_cnt,10) + parseInt(outObj.g_yjyc_cnt,10),
    						   whole_yield : outObj.whole_yield,
    						   checkUser : checkUser,
    						   mdlId : mdlId,
    						   prdAry : prd_id,
    						   yjycAry : yjyc_cnt,
    						   syAry : sy_cnt,
    						   cHsAry : C_hs_cnt,
    						   cAdAry : C_ad_cnt,
    						   cTdAry : C_td_cnt,
    						   cShbjAry : C_shbj_cnt,
    						   cPsAry : C_ps_cnt,
    						   cZwAry : C_zw_cnt,
    						   cLyAry : C_ly_cnt,
    						   cOtherAry : C_other,
    						   tHsAry : T_hs_cnt,
    						   tAdAry : T_ad_cnt,
    						   tTdAry : T_td_cnt,
    						   tShbjAry : T_shbj_cnt,
    						   tPsAry : T_ps_cnt,
    						   tZwAry : T_zw_cnt,
    						   tLyAry : T_ly_cnt,
    						   tOtherAry : T_other,
    						   remarkAry : remark
           		        	};
                  		   label.PrintDMQA098(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
                	   }
            	   }else if(outObj.cus_id == "115"){//115客户
            		   controlsQuery.mainGrd.fatherDiv003.hide();
            		   controlsQuery.mainGrd.fatherDiv006.hide();
            		   controlsQuery.mainGrd.fatherDiv008.hide();
            		   controlsQuery.mainGrd.fatherDiv063.hide();
            		   controlsQuery.mainGrd.fatherDiv115.show();
           			   controlsQuery.mainGrd.fatherDivTM.hide();
           			   controlsQuery.mainGrd.fatherDivOther.hide();
           			   resizeFnc();
           			   var oary = outObj.prd_cnt > 1 ? outObj.oary115 : [outObj.oary115];
           			   var l_llhx_cnt = [],l_yjyc_cnt = [],l_atd_cnt = [],l_pl_cnt = [],l_hs_cnt = [],l_wj_cnt = [],
           			   		l_other_cnt = [],lCnt = [],z_hs_cnt = [],z_atd_cnt = [],z_ssbj_cnt = [],z_lw_cnt = [],
           			   		z_ps_cnt = [],z_ly_cnt = [],z_wj_cnt = [],z_pit_cnt = [], zCnt = [],status = [],remark = [];
//           			   if(outObj.ope_eff_flg == "Y"){// 原箱
//           				   for(var i=0; i<outObj.std_qty ; i++ ){
//           					   for(var j=0; j<outObj.prd_cnt ; j++){
//           						   if((i+1) == oary[j].slot_no){
//           							   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
//           							   mtrl_box_id[i] = oary[j].mtrl_box_id_fk;
//           							   l_llhx_cnt[i] = oary[j].l_llhx_cnt;
//           							   l_yjyc_cnt[i] = oary[j].l_yjyc_cnt;
//           							   l_atd_cnt[i] = oary[j].l_atd_cnt;
//           							   l_pl_cnt[i] = oary[j].l_pl_cnt;
//           							   l_hs_cnt[i] = oary[j].l_hs_cnt;
//           							   l_wj_cnt[i] = oary[j].l_wj_cnt;
//           							   l_other_cnt[i] = oary[j].l_other_cnt;
//           							   lCnt[i] = parseInt(oary[j].l_llhx_cnt,10) + parseInt(oary[j].l_yjyc_cnt,10) + parseInt(oary[j].l_atd_cnt,10)
//              						 		+ parseInt(oary[j].l_pl_cnt,10) + parseInt(oary[j].l_hs_cnt,10) + parseInt(oary[j].l_wj_cnt,10)
//              						 		+ parseInt(oary[j].l_other_cnt,10);
//           							   z_hs_cnt[i] = oary[j].z_hs_cnt;
//           							   z_atd_cnt[i] = oary[j].z_atd_cnt;
//           							   z_ssbj_cnt[i] = oary[j].z_ssbj_cnt;
//           							   z_lw_cnt[i] = oary[j].z_lw_cnt;
//           							   z_ps_cnt[i] = oary[j].z_ps_cnt;
//           							   z_ly_cnt[i] = oary[j].z_ly_cnt;
//           							   z_wj_cnt[i] = oary[j].z_wj_cnt;
//           							   z_pit_cnt[i] = oary[j].z_pit_cnt;
//           							   zCnt[i] = parseInt(oary[j].z_hs_cnt,10) + parseInt(oary[j].z_atd_cnt,10) + parseInt(oary[j].z_ssbj_cnt,10)
//              						 		+ parseInt(oary[j].z_lw_cnt,10) + parseInt(oary[j].z_ps_cnt,10) + parseInt(oary[j].z_ly_cnt,10)
//              						 		+ parseInt(oary[j].z_wj_cnt,10) + parseInt(oary[j].z_pit_cnt,10);
//           							   if(oary[j].remark.length == 0){
//           								   remark[i] = "";
//           							   }else{
//           								   remark[i] = oary[j].remark;
//           							   }
//          							   if(proc_id == "JBQA"){
//           								   status[i] = "已减薄";  
//           							   }else if(proc_id == "DMQA"){
//           								   status[i] = "已镀膜"; 
//           							   }
//           							   break;
//              					   }
//              					   if(j == parseInt(outObj.prd_cnt,10) - 1){
//              						   prd_id[i] = mtrl_box_id[i] = "";
//              						   l_llhx_cnt[i] = l_yjyc_cnt[i] = l_atd_cnt[i] = l_pl_cnt[i] = l_hs_cnt[i] = l_wj_cnt[i] = l_other_cnt[i] = lCnt[i] = "";
//              						   z_hs_cnt[i] = z_atd_cnt[i] = z_ssbj_cnt[i] = z_lw_cnt[i] = z_ps_cnt[i] = z_ly_cnt[i] = z_wj_cnt[i] = z_pit_cnt[i] = zCnt[i] = "";
//              						   status[i] = remark[i] = "";
//              					   }
//              				   }
//              			   }
//              		   }else{// 非原箱
              			  for(var j=0; j<outObj.prd_cnt ; j++){
              				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
              				  mtrl_box_id[j] = oary[j].mtrl_box_id_fk;
              				  l_llhx_cnt[j] = oary[j].l_llhx_cnt;
              				  l_yjyc_cnt[j] = oary[j].l_yjyc_cnt;
              				  l_atd_cnt[j] = oary[j].l_atd_cnt;
              				  l_pl_cnt[j] = oary[j].l_pl_cnt;
              				  l_hs_cnt[j] = oary[j].l_hs_cnt;
              				  l_wj_cnt[j] = oary[j].l_wj_cnt;
              				  l_other_cnt[j] = oary[j].l_other_cnt;
              				  lCnt[j] = parseInt(oary[j].l_llhx_cnt,10) + parseInt(oary[j].l_yjyc_cnt,10) + parseInt(oary[j].l_atd_cnt,10)
      						 		+ parseInt(oary[j].l_pl_cnt,10) + parseInt(oary[j].l_hs_cnt,10) + parseInt(oary[j].l_wj_cnt,10)
      						 		+ parseInt(oary[j].l_other_cnt,10);
              				  z_hs_cnt[j] = oary[j].z_hs_cnt;
              				  z_atd_cnt[j] = oary[j].z_atd_cnt;
              				  z_ssbj_cnt[j] = oary[j].z_ssbj_cnt;
              				  z_lw_cnt[j] = oary[j].z_lw_cnt;
              				  z_ps_cnt[j] = oary[j].z_ps_cnt;
              				  z_ly_cnt[j] = oary[j].z_ly_cnt;
              				  z_wj_cnt[j] = oary[j].z_wj_cnt;
              				  z_pit_cnt[j] = oary[j].z_pit_cnt;
              				  zCnt[j] = parseInt(oary[j].z_hs_cnt,10) + parseInt(oary[j].z_atd_cnt,10) + parseInt(oary[j].z_ssbj_cnt,10)
      						 		+ parseInt(oary[j].z_lw_cnt,10) + parseInt(oary[j].z_ps_cnt,10) + parseInt(oary[j].z_ly_cnt,10)
      						 		+ parseInt(oary[j].z_wj_cnt,10) + parseInt(oary[j].z_pit_cnt,10);
              				  if(oary[j].remark.length == 0){
              					  remark[j] = "";
              				  }else{
              					  remark[j] = oary[j].remark;
              				  }
              				  if(proc_id == "JBQA"){
              					  status[j] = "已减薄";  
              				  }else if(proc_id == "DMQA"){
              					  status[j] = "已镀膜"; 
              				  }
              			  }
              			  if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
              				  for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
              					  prd_id[i] = mtrl_box_id[i] = "";
              					  l_llhx_cnt[i] = l_yjyc_cnt[i] = l_atd_cnt[i] = l_pl_cnt[i] = l_hs_cnt[i] = l_wj_cnt[i] = l_other_cnt[i] = lCnt[i] = "";
              					  z_hs_cnt[i] = z_atd_cnt[i] = z_ssbj_cnt[i] = z_lw_cnt[i] = z_ps_cnt[i] = z_ly_cnt[i] = z_wj_cnt[i] = z_pit_cnt[i] = zCnt[i] = "";
              					  status[i] = remark[i] = "";
              				  }
              			  }
//              		   }
           			   setGridInfo(outObj.oary115,"#shtListGrd115",true);
           			   var printObj = {
           					   printTyp : printTyp,
           					   procId : proc_id,
           					   stdQty : outObj.std_qty,
           					   cusId : outObj.cus_id,
           					   woId : outObj.wo_id,
           					   soId : outObj.so_id,
           					   boxId : outObj.box_id,
           					   toThickness : outObj.to_thickness,
           					   prdCnt : outObj.prd_cnt,
           					   mdlId : mdlId,
           					   prdAry : prd_id,
           					   mtrlBoxAry : mtrl_box_id,
           					   lLlhxAry : l_llhx_cnt,
           					   lYjycAry : l_yjyc_cnt,
           					   lAtdAry : l_atd_cnt,
           					   lPlAry : l_pl_cnt,
           					   lHsAry : l_hs_cnt,
           					   lWjAry : l_wj_cnt,
           					   lOtherAry : l_other_cnt,
           					   lCntAry : lCnt,
           					   zHsAry : z_hs_cnt,
           					   zAtdAry : z_atd_cnt,
           					   zSsbjAry : z_ssbj_cnt,
           					   zLwAry : z_lw_cnt,
           					   zPsAry : z_ps_cnt,
           					   zLyAry : z_ly_cnt,
           					   zWjAry : z_wj_cnt,
           					   zPitAry : z_pit_cnt,
           					   zCntAry : zCnt,
           					   statusAry : status,
           					   remarkAry : remark
           			   };
           			   label.PrintJBorDMQA115(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
            	   }else{//other 华南客户
            		   controlsQuery.mainGrd.fatherDiv003.hide();
            		   controlsQuery.mainGrd.fatherDiv006.hide();
            		   controlsQuery.mainGrd.fatherDiv008.hide();
            		   controlsQuery.mainGrd.fatherDiv063.hide();
            		   controlsQuery.mainGrd.fatherDiv115.hide();
           			   controlsQuery.mainGrd.fatherDivTM.hide();
           			   controlsQuery.mainGrd.fatherDivOther.show();
           			   resizeFnc();
           			   var oary = outObj.prd_cnt > 1 ? outObj.oaryHN : [outObj.oaryHN];
           			   var l_hnbl_cnt = [],l_llhx_cnt = [],l_atd_cnt = [],l_ps_cnt = [],l_lw_cnt = [],l_hs_cnt = [],l_zw_cnt = [],l_other = [],
           			   		z_d_atd_cnt = [],z_d_hs_cnt = [],z_d_ssbj_cnt = [],z_d_zw_cnt = [],z_d_ly_cnt = [],
           			   		z_ps_cnt = [],z_lw_cnt = [],z_ly_cnt = [],z_ls_cnt = [], z_fs_cnt = [],z_other = [],
           			   		status = [],remark = [];
//           			   if(outObj.ope_eff_flg == "Y"){// 原箱
//           				   for(var i=0; i<outObj.std_qty ; i++ ){
//           					   for(var j=0; j<outObj.prd_cnt ; j++){
//           						   if((i+1) == oary[j].slot_no){
//           							   prd_id[i] = oary[j].fab_sn;// prd_seq_id->fab_sn
//           							   l_hnbl_cnt[i] = oary[j].l_hnbl_cnt;
//           							   l_llhx_cnt[i] = oary[j].l_llhx_cnt;
//           							   l_atd_cnt[i] = oary[j].l_atd_cnt;
//           							   l_ps_cnt[i] = oary[j].l_ps_cnt;
//           							   l_lw_cnt[i] = oary[j].l_lw_cnt;
//           							   l_hs_cnt[i] = oary[j].l_hs_cnt;
//           							   l_zw_cnt[i] = oary[j].l_zw_cnt;
//           							   l_other[i] = oary[j].l_other;
//           							   z_d_atd_cnt[i] = oary[j].z_d_atd_cnt;
//           							   z_d_hs_cnt[i] = oary[j].z_d_hs_cnt;
//           							   z_d_ssbj_cnt[i] = oary[j].z_d_ssbj_cnt;
//           							   z_d_zw_cnt[i] = oary[j].z_d_zw_cnt;
//           							   z_d_ly_cnt[i] = oary[j].z_d_ly_cnt;
//           							   z_ps_cnt[i] = oary[j].z_ps_cnt;
//           							   z_lw_cnt[i] = oary[j].z_lw_cnt;
//           							   z_ly_cnt[i] = oary[j].z_ly_cnt;
//           							   z_ls_cnt[i] = oary[j].z_ls_cnt;
//           							   z_fs_cnt[i] = oary[j].z_fs_cnt;
//           							   z_other[i] = oary[j].z_other;
//           							   if(oary[j].remark.length == 0){
//           								   remark[i] = "";
//           							   }else{
//           								   remark[i] = oary[j].remark;
//           							   }
//          							   if(proc_id == "JBQA"){
//           								   status[i] = "已减薄";  
//           							   }else if(proc_id == "DMQA"){
//           								   status[i] = "已镀膜"; 
//           							   }
//           							   break;
//              					   }
//              					   if(j == parseInt(outObj.prd_cnt,10) - 1){
//              						   prd_id[i] = "";
//              						   l_hnbl_cnt[i] = l_llhx_cnt[i] = l_atd_cnt[i] = l_ps_cnt[i] = l_lw_cnt[i] = l_hs_cnt[i] = l_zw_cnt[i] = l_other[i] = "";
//              						   z_d_atd_cnt[i] = z_d_hs_cnt[i] = z_d_ssbj_cnt[i] = z_d_zw_cnt[i] = z_d_ly_cnt[i] = "";
//              						   z_ps_cnt[i] = z_lw_cnt[i] = z_ly_cnt[i] = z_ls_cnt[i] = z_fs_cnt[i] = z_other[i] = "";
//              						   status[i] = remark[i] = "";
//              					   }
//              				   }
//              			   }
//              		   }else{// 非原箱
              			  for(var j=0; j<outObj.prd_cnt ; j++){
              				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
              				  l_hnbl_cnt[j] = oary[j].l_hnbl_cnt;
							  l_llhx_cnt[j] = oary[j].l_llhx_cnt;
							  l_atd_cnt[j] = oary[j].l_atd_cnt;
							  l_ps_cnt[j] = oary[j].l_ps_cnt;
							  l_lw_cnt[j] = oary[j].l_lw_cnt;
							  l_hs_cnt[j] = oary[j].l_hs_cnt;
							  l_zw_cnt[j] = oary[j].l_zw_cnt;
							  l_other[j] = oary[j].l_other;
							  z_d_atd_cnt[j] = oary[j].z_d_atd_cnt;
							  z_d_hs_cnt[j] = oary[j].z_d_hs_cnt;
							  z_d_ssbj_cnt[j] = oary[j].z_d_ssbj_cnt;
							  z_d_zw_cnt[j] = oary[j].z_d_zw_cnt;
							  z_d_ly_cnt[j] = oary[j].z_d_ly_cnt;
							  z_ps_cnt[j] = oary[j].z_ps_cnt;
							  z_lw_cnt[j] = oary[j].z_lw_cnt;
							  z_ly_cnt[j] = oary[j].z_ly_cnt;
							  z_ls_cnt[j] = oary[j].z_ls_cnt;
							  z_fs_cnt[j] = oary[j].z_fs_cnt;
							  z_other[j] = oary[j].z_other;
              				  if(oary[j].remark.length == 0){
              					  remark[j] = "";
              				  }else{
              					  remark[j] = oary[j].remark;
              				  }
              				  if(proc_id == "JBQA"){
              					  status[j] = "已减薄";  
              				  }else if(proc_id == "DMQA"){
              					  status[j] = "已镀膜"; 
              				  }
              			  }
              			  if(parseInt(outObj.prd_cnt,10) < parseInt(outObj.std_qty,10)){
              				  for(var i=parseInt(outObj.prd_cnt,10) ; i<parseInt(outObj.std_qty,10);i++){
              					  prd_id[i] = "";
              					  l_hnbl_cnt[i] = l_llhx_cnt[i] = l_atd_cnt[i] = l_ps_cnt[i] = l_lw_cnt[i] = l_hs_cnt[i] = l_zw_cnt[i] = l_other[i] = "";
              					  z_d_atd_cnt[i] = z_d_hs_cnt[i] = z_d_ssbj_cnt[i] = z_d_zw_cnt[i] = z_d_ly_cnt[i] = "";
              					  z_ps_cnt[i] = z_lw_cnt[i] = z_ly_cnt[i] = z_ls_cnt[i] = z_fs_cnt[i] = z_other[i] = "";
              					  status[i] = remark[i] = "";
              				  }
              			  }
//              		   }
           			   setGridInfo(outObj.oaryHN,"#shtListGrdOther",true);
           			   var printObj = {
           					   printTyp : printTyp,
           					   procId : proc_id,
           					   stdQty : outObj.std_qty,
           					   cusId : outObj.cus_id,
           					   woId : outObj.wo_id,
           					   soId : outObj.so_id,
           					   actSoId : outObj.act_so_id,
           					   boxId : outObj.box_id,
           					   toThickness : outObj.to_thickness,
           					   prdCnt : outObj.prd_cnt,
    						   modelCnt : outObj.model_cnt,
    						   totalModelCnt : totalModelCnt,
    						   whole_def_cnt : parseInt(outObj.d_def_cnt,10) + parseInt(outObj.p_def_cnt,10),
    						   whole_yield : outObj.whole_yield,
    						   checkUser : checkUser,
           					   mdlId : mdlId,
           					   prdAry : prd_id,
           					   lHnblAry : l_hnbl_cnt,
           					   lLlhxAry : l_llhx_cnt,
           					   lAtdAry : l_atd_cnt,
           					   lPsAry : l_ps_cnt,
           					   lLwAry : l_lw_cnt,
           					   lHsAry : l_hs_cnt,
           					   lZwAry : l_zw_cnt,
           					   lOther : l_other,
           					   zdAtdAry : z_d_atd_cnt,
           					   zdHsAry : z_d_hs_cnt,
           					   zdSsbjAry : z_d_ssbj_cnt,
           					   zdZwAry : z_d_zw_cnt,
           					   zdLyAry : z_d_ly_cnt,
           					   zPsAry : z_ps_cnt,
           					   zLwAry : z_lw_cnt,
           					   zLyAry : z_ly_cnt,
           					   zLsAry : z_ls_cnt,
           					   zFsAry : z_fs_cnt,
           					   zOther : z_other,
           					   statusAry : status,
           					   remarkAry : remark
           			   };
           			   label.PrintJBorDMQAHN(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
            	   }
	        	}else{
	        		toolFunc.iniClear();
	        	}
	        }
	  };
	  function toJsonString(ary){
		  var cnt = ary.length;
		  var subAry = [];
		  for(var i=0;i<cnt;i++){
			 subAry[i] = JSON.stringify(ary[i]);
		  }
		  return JSON.stringify(subAry);
	  }
	  var iniGridInfo = function(){
	        controlsQuery.mainGrd.grdId003.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      autowidth:true,// 宽度根据父元素自适应
			      height:400,
			      shrinkToFit:false,
			      scroll:true,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, // 显示总记录数
			      rownumbers  :true ,// 显示行号
			      rowNum:50,         // 每页多少行，用于分页
			      rownumWidth : 20,  // 行号列宽度
			      emptyrecords :true ,
		          colModel: [
		      				{name:"prd_seq_id"     , index:"prd_seq_id" , label:PRD_SEQ_ID_TAG , width :PRD_SEQ_ID_CLM  ,align:"left" ,hidden:true},
		     				{name : 'fab_sn'       , index : 'fab_sn'        ,label : PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM,align:"left" ,sortable:true},
		    	            {name:"slot_no"        , index:"slot_no"    , label:SLOT_NO_TAG   , width :SLOT_NO_CLM ,align:"left" ,sortable:true,sorttype:'int'},
		    	            {name:"r_yjyc_cnt"     , index:"r_yjyc_cnt"  , label:"液晶异常"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},       
		    	            {name:"r_llhx_cnt"     , index:"r_llhx_cnt"     , label:"来料划X"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},          
		    	            {name:"r_atd_cnt"      , index:"r_atd_cnt"   , label:"凹凸点"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"r_pl_cnt"       , index:"r_pl_cnt"   , label:"入料破裂"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},       
		    	            {name:"r_lh_cnt"       , index:"r_lh_cnt"   , label:"入料裂痕"    , width :DEFDSC_CLM  ,align:"left" ,sortable:true},        
		    	            {name:"r_qj_cnt"       , index:"r_qj_cnt" , label:"入料缺角"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},   
		    	            {name:"r_hs_cnt"       , index:"r_hs_cnt"   , label:"入料刮伤"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"r_bw_cnt"       , index:"r_bw_cnt"   , label:"入料表污"        , width :DEFDSC_CLM ,align:"left" ,sortable:true},     
		    	            {name:"r_other"        , index:"r_other"   , label:"入料其他不良"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},       
		    	            {name:"c_pl_cnt"       , index:"c_pl_cnt"    , label:"出货破裂"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},   
		    	            {name:"c_lh_cnt"       , index:"c_lh_cnt"   , label:"出货裂痕"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},     
		    	            {name:"c_qj_cnt"       , index:"c_qj_cnt"   , label:"出货缺角"    , width :DEFDSC_CLM ,align:"left" ,sortable:true},        
		    	            {name:"c_cj_cnt"       , index:"c_cj_cnt"   , label:"出货残胶"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},     
		    	            {name:"c_ly_cnt"       , index:"c_ly_cnt"   , label:"出货漏液"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},  
		    	            {name:"c_lsw_cnt"      , index:"c_lsw_cnt"   , label:"流水纹"          , width :DEFDSC_CLM  ,align:"left" ,sortable:true}, 
		    	            {name:"c_td_cnt"       , index:"c_td_cnt"   , label:"凸点"          , width :DEFDSC_CLM ,align:"left" ,sortable:true},  
		    	            {name:"c_lhy_cnt"      , index:"c_lhy_cnt"   , label:"轮痕"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},   
		    	            {name:"c_ad_cnt"       , index:"c_ad_cnt"    , label:"凹点"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
		    	            {name:"c_hs_cnt"       , index:"c_hs_cnt"    , label:"出货刮伤"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
		    	            {name:"c_bw_cnt"       , index:"c_bw_cnt"    , label:"出货表污"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
		    	            {name:"c_other"        , index:"c_other"    , label:"出货其他不良"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
		    	            {name:"remark"         , index:"remark"     , label:"备注"       , width :DEFDSC_CLM ,align:"left" ,sortable:true}
		      		]
	        });
	        controlsQuery.mainGrd.grdId006.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      autowidth:true,// 宽度根据父元素自适应
			      height:400,
			      shrinkToFit:false,
			      scroll:true,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, // 显示总记录数
			      rownumbers  :true ,// 显示行号
			      rowNum:50,         // 每页多少行，用于分页
			      rownumWidth : 20,  // 行号列宽度
			      emptyrecords :true ,
		          colModel: [
		      			{name:"prd_seq_id"     , index:"prd_seq_id"      , label:PRD_SEQ_ID_TAG , width :PRD_SEQ_ID_CLM  ,align:"left" ,hidden:true},
	     				{name : 'fab_sn'       , index : 'fab_sn'        ,label : PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM,align:"left" ,sortable:true},
	    	            {name:"slot_no"        , index:"slot_no"         , label:SLOT_NO_TAG   , width :SLOT_NO_CLM ,align:"left" ,sortable:true,sorttype:'int'},
	    	            {name:"mtrl_box_id_fk" , index:"mtrl_box_id_fk"  , label:MTRL_BOX_ID_TAG   , width :LOT_ID_CLM  ,align:"left" ,sortable:true},
	    				{name:"prd_grade"      , index:"prd_grade"       , label:PRD_GRADE_TAG    , width :PRD_GRADE_CLM ,align:"left" ,sortable:true},
	    			    {name:"C_z_pl_cnt"     , index:"C_z_pl_cnt"     , label:"CF面制程破裂"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"C_z_ly_cnt"     , index:"C_z_ly_cnt"     , label:"CF面制程漏液"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"C_z_zw_cnt"     , index:"C_z_zw_cnt"     , label:"CF面制程脏污"    , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"C_z_skbj_cnt"   , index:"C_z_skbj_cnt"   , label:"CF面制程蚀刻不均"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"C_z_atd_cnt"    , index:"C_z_atd_cnt"    , label:"CF面制程凹凸点"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"C_z_hs_cnt"     , index:"C_z_hs_cnt"     , label:"CF面制程划伤"        , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"C_l_pl_cnt"     , index:"C_l_pl_cnt"     , label:"CF面来料破裂"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"C_l_yjyc_cnt"   , index:"C_l_yjyc_cnt"   , label:"CF面来料液晶异常"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"C_l_ad_cnt"     , index:"C_l_ad_cnt"     , label:"CF面来料凹点"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"C_l_mjad_cnt"   , index:"C_l_mjad_cnt"   , label:"CF面来料密集凹点"    , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"C_l_hs_cnt"     , index:"C_l_hs_cnt"     , label:"CF面来料划伤"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"C_l_gly_cnt"    , index:"C_l_gly_cnt"    , label:"CF面来料滚轮印"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"T_z_zw_cnt"     , index:"T_z_zw_cnt"     , label:"TFT面制程脏污"          , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"T_z_skbj_cnt"   , index:"T_z_skbj_cnt"   , label:"TFT面制程蚀刻不均"          , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"T_z_atd_cnt"    , index:"T_z_atd_cnt"    , label:"TFT面制程凹凸点"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"T_z_hs_cnt"     , index:"T_z_hs_cnt"     , label:"TFT面制程划伤"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"T_l_ad_cnt"     , index:"T_l_ad_cnt"     , label:"TFT面来料凹点"          , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"T_l_mjad_cnt"   , index:"T_l_mjad_cnt"   , label:"TFT面来料密集凹点"          , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"T_l_hs_cnt"     , index:"T_l_hs_cnt"     , label:"TFT面来料划伤"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"T_l_gly_cnt"    , index:"T_l_gly_cnt"    , label:"TFT面来料滚轮印"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    	            {name:"remark"         , index:"remark"         , label:"备注"       , width :DEFDSC_CLM ,align:"left" ,sortable:true}
		      		]
	        });
	        controlsQuery.mainGrd.grdId008.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      autowidth:true,// 宽度根据父元素自适应
			      height:400,
			      shrinkToFit:false,
			      scroll:true,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, // 显示总记录数
			      rownumbers  :true ,// 显示行号
			      rowNum:50,         // 每页多少行，用于分页
			      rownumWidth : 20,  // 行号列宽度
			      emptyrecords :true ,
		          colModel: [
		      				{name:"prd_seq_id"     , index:"prd_seq_id"   , label:PRD_SEQ_ID_TAG , width :PRD_SEQ_ID_CLM  ,align:"left" ,hidden:true},
		     				{name : 'fab_sn'       , index : 'fab_sn'     ,label : PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM,align:"left" ,sortable:true},
		    	            {name:"slot_no"        , index:"slot_no"      , label:SLOT_NO_TAG   , width :SLOT_NO_CLM ,align:"left" ,sortable:true,sorttype:'int'},
		    	            {name:"l_ly_cnt"       , index:"l_ly_cnt"     , label:"来料轮印"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},       
		    	            {name:"l_llxo_cnt"     , index:"l_llxo_cnt"   , label:"来料划X/O"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},          
		    	            {name:"l_hnbl_cnt"     , index:"l_hnbl_cnt"   , label:"盒内不良"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"l_pl_cnt"       , index:"l_pl_cnt"     , label:"来料破裂"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},       
		    	            {name:"z_c_skbj_cnt"   , index:"z_c_skbj_cnt" , label:"CF制程蚀刻不均"    , width :DEFDSC_CLM  ,align:"left" ,sortable:true},        
		    	            {name:"z_c_dqx_cnt"    , index:"z_c_dqx_cnt"  , label:"CF制程点缺陷"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},   
		    	            {name:"z_c_xqx_cnt"    , index:"z_c_xqx_cnt"  , label:"CF制程线缺陷"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"z_c_wj_cnt"     , index:"z_c_wj_cnt"   , label:"CF制程污迹"        , width :DEFDSC_CLM ,align:"left" ,sortable:true},     
		    	            {name:"z_t_skbj_cnt"   , index:"z_t_skbj_cnt" , label:"TFT制程蚀刻不均"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},       
		    	            {name:"z_t_dqx_cnt"    , index:"z_t_dqx_cnt"  , label:"TFT制程点缺陷"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},   
		    	            {name:"z_t_xqx_cnt"    , index:"z_t_xqx_cnt"  , label:"TFT制程线缺陷"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},     
		    	            {name:"z_t_wj_cnt"     , index:"z_t_wj_cnt"   , label:"TFT制程污迹"    , width :DEFDSC_CLM ,align:"left" ,sortable:true},        
		    	            {name:"z_t_pit_cnt"    , index:"z_t_pit_cnt"  , label:"TFT制程pit点"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},     
		    	            {name:"jbpl_cnt"       , index:"jbpl_cnt"     , label:"减薄制程破裂"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},  
		    	            {name:"jbly_cnt"       , index:"jbly_cnt"     , label:"减薄制程漏液"        , width :DEFDSC_CLM  ,align:"left" ,sortable:true}, 
		    	            {name:"remark"         , index:"remark"       , label:"备注"       , width :DEFDSC_CLM ,align:"left" ,sortable:true}
		      		]
	        });
	        controlsQuery.mainGrd.grdId063.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      autowidth:true,// 宽度根据父元素自适应
			      height:400,
			      shrinkToFit:false,
			      scroll:true,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, // 显示总记录数
			      rownumbers  :true ,// 显示行号
			      rowNum:50,         // 每页多少行，用于分页
			      rownumWidth : 20,  // 行号列宽度
			      emptyrecords :true ,
		          colModel: [
		      				{name:"prd_seq_id"     , index:"prd_seq_id" , label:PRD_SEQ_ID_TAG , width :PRD_SEQ_ID_CLM  ,align:"left" ,hidden:true},
		     				{name : 'fab_sn'       , index : 'fab_sn'        ,label : PRD_SEQ_ID_TAG,width : 200,align:"left" ,sortable:true},
		    	            {name:"remark"         , index:"remark"     , label:"备注"       , width :200 ,align:"left" ,sortable:true}
		      		]
	        });
	        controlsQuery.mainGrd.grdId115.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      autowidth:true,// 宽度根据父元素自适应
			      height:400,
			      shrinkToFit:false,
			      scroll:true,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, // 显示总记录数
			      rownumbers  :true ,// 显示行号
			      rowNum:50,         // 每页多少行，用于分页
			      rownumWidth : 20,  // 行号列宽度
			      emptyrecords :true ,
		          colModel: [
		      				{name:"prd_seq_id"     , index:"prd_seq_id"  , label:PRD_SEQ_ID_TAG , width :PRD_SEQ_ID_CLM  ,align:"left" ,hidden:true},
		     				{name : 'fab_sn'       , index : 'fab_sn'    , label :PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM,align:"left" ,sortable:true},
		    	            {name:"slot_no"        , index:"slot_no"     , label:SLOT_NO_TAG     , width :SLOT_NO_CLM ,align:"left" ,sortable:true,sorttype:'int'},
		    	            {name:"l_llhx_cnt"     , index:"l_llhx_cnt"  , label:"来料划X"       , width :DEFDSC_CLM ,align:"left" ,sortable:true},       
		    	            {name:"l_yjyc_cnt"     , index:"l_yjyc_cnt"  , label:"来料液晶异常"  , width :DEFDSC_CLM  ,align:"left" ,sortable:true},          
		    	            {name:"l_atd_cnt"      , index:"l_atd_cnt"   , label:"来料凹凸点"    , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"l_pl_cnt"       , index:"l_pl_cnt"    , label:"来料破裂"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},       
		    	            {name:"l_hs_cnt"       , index:"l_hs_cnt"    , label:"来料划伤"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},        
		    	            {name:"l_wj_cnt"       , index:"l_wj_cnt"    , label:"来料污迹"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},   
		    	            {name:"l_other_cnt"    , index:"l_other_cnt" , label:"来料其他不良"  , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"z_hs_cnt"       , index:"z_hs_cnt"    , label:"制程划伤"     , width :DEFDSC_CLM ,align:"left" ,sortable:true},     
		    	            {name:"z_atd_cnt"      , index:"z_atd_cnt"   , label:"制程凹凸点"   , width :DEFDSC_CLM  ,align:"left" ,sortable:true},       
		    	            {name:"z_ssbj_cnt"     , index:"z_ssbj_cnt"  , label:"制程酸蚀不均" , width :DEFDSC_CLM ,align:"left" ,sortable:true},   
		    	            {name:"z_lw_cnt"       , index:"z_lw_cnt"    , label:"制程裂纹"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},     
		    	            {name:"z_ps_cnt"       , index:"z_ps_cnt"    , label:"制程破损"     , width :DEFDSC_CLM ,align:"left" ,sortable:true},        
		    	            {name:"z_ly_cnt"       , index:"z_ly_cnt"    , label:"制程漏液"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},     
		    	            {name:"z_wj_cnt"       , index:"z_wj_cnt"    , label:"制程污迹"     , width :DEFDSC_CLM ,align:"left" ,sortable:true},  
		    	            {name:"z_pit_cnt"      , index:"z_pit_cnt"   , label:"制程pit点"   , width :DEFDSC_CLM  ,align:"left" ,sortable:true}
		      		]
	        });
	        controlsQuery.mainGrd.grdIdTM.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      autowidth:true,// 宽度根据父元素自适应
			      height:400,
			      shrinkToFit:false,
			      scroll:true,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, // 显示总记录数
			      rownumbers  :true ,// 显示行号
			      rowNum:50,         // 每页多少行，用于分页
			      rownumWidth : 20,  // 行号列宽度
			      emptyrecords :true ,
		          colModel: [
		      				{name:"prd_seq_id"     , index:"prd_seq_id" , label:PRD_SEQ_ID_TAG , width :PRD_SEQ_ID_CLM  ,align:"left" ,hidden:true},
		     				{name : 'fab_sn'       , index : 'fab_sn'        ,label : PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM,align:"left" ,sortable:true},
		    	            {name:"slot_no"        , index:"slot_no"    , label:SLOT_NO_TAG   , width :SLOT_NO_CLM ,align:"left" ,sortable:true,sorttype:'int'},
		    	            {name:"yjyc_cnt"       , index:"yjyc_cnt"  , label:"液晶异常"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},       
		    	            {name:"sy_cnt"         , index:"sy_cnt"     , label:"渗液"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},          
		    	            {name:"C_hs_cnt"       , index:"C_hs_cnt"   , label:"CF面划伤"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"C_ad_cnt"       , index:"C_ad_cnt"   , label:"CF面凹点"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},       
		    	            {name:"C_td_cnt"       , index:"C_td_cnt"   , label:"CF面凸点"    , width :DEFDSC_CLM  ,align:"left" ,sortable:true},        
		    	            {name:"C_shbj_cnt"     , index:"C_shbj_cnt" , label:"CF面酸蚀不匀"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},   
		    	            {name:"C_ps_cnt"       , index:"C_ps_cnt"   , label:"CF面破损"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"C_zw_cnt"       , index:"C_zw_cnt"   , label:"CF面脏污"        , width :DEFDSC_CLM ,align:"left" ,sortable:true},     
		    	            {name:"C_ly_cnt"       , index:"C_ly_cnt"   , label:"CF面轮印"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},       
		    	            {name:"C_other"        , index:"C_other"    , label:"CF面其他不良"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},   
		    	            {name:"T_hs_cnt"       , index:"T_hs_cnt"   , label:"TFT面划伤"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},     
		    	            {name:"T_ad_cnt"       , index:"T_ad_cnt"   , label:"TFT面凹点"    , width :DEFDSC_CLM ,align:"left" ,sortable:true},        
		    	            {name:"T_td_cnt"       , index:"T_td_cnt"   , label:"TFT面凸点"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},     
		    	            {name:"T_shbj_cnt"     , index:"T_shbj_cnt"   , label:"TFT面酸蚀不匀"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},  
		    	            {name:"T_ps_cnt"       , index:"T_ps_cnt"   , label:"TFT面破损"          , width :DEFDSC_CLM  ,align:"left" ,sortable:true}, 
		    	            {name:"T_zw_cnt"       , index:"T_zw_cnt"   , label:"TFT面脏污"          , width :DEFDSC_CLM ,align:"left" ,sortable:true},  
		    	            {name:"T_ly_cnt"       , index:"T_ly_cnt"   , label:"TFT面轮印"     , width :DEFDSC_CLM  ,align:"left" ,sortable:true},      
		    	            {name:"T_other"        , index:"T_other"    , label:"TFT面其他不良"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
		    	            {name:"remark"         , index:"remark"     , label:"备注"       , width :DEFDSC_CLM ,align:"left" ,sortable:true}
		      		]
	        });
	        controlsQuery.mainGrd.grdIdOther.jqGrid({
	        	url:"",
	        	datatype:"local",
	        	mtype:"POST",
	        	autowidth:true,// 宽度根据父元素自适应
	        	height:400,
	        	shrinkToFit:false,
	        	scroll:true,
	        	resizable : true,
	        	loadonce:true,
	        	fixed: true,
	        	viewrecords : true, // 显示总记录数
	        	rownumbers  :true ,// 显示行号
	        	rowNum:50,         // 每页多少行，用于分页
	        	rownumWidth : 20,  // 行号列宽度
	        	emptyrecords :true ,
	        	colModel: [
	      				{name:"prd_seq_id"     , index:"prd_seq_id"      , label:PRD_SEQ_ID_TAG , width :PRD_SEQ_ID_CLM  ,align:"left" ,hidden:true},
	     				{name : 'fab_sn'       , index : 'fab_sn'        ,label : PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM,align:"left" ,sortable:true},
	    	            {name:"slot_no"        , index:"slot_no"         , label:SLOT_NO_TAG    , width :SLOT_NO_CLM ,align:"left" ,sortable:true,sorttype:'int'},
	    			    {name:"l_hnbl_cnt"     , index:"l_hnbl_cnt"      , label:"盒内不良"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"l_llhx_cnt"     , index:"l_llhx_cnt"      , label:"来料划X"       , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"l_atd_cnt"      , index:"l_atd_cnt"       , label:"来料凹凸点"    , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"l_ps_cnt"       , index:"l_ps_cnt"        , label:"来料破损"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"l_lw_cnt"       , index:"l_lw_cnt"        , label:"来料裂纹"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"l_hs_cnt"       , index:"l_hs_cnt"        , label:"来料划伤"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"l_zw_cnt"       , index:"l_zw_cnt"        , label:"来料脏污"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"l_other"        , index:"l_other"         , label:"来料其他"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"z_d_atd_cnt"    , index:"z_d_atd_cnt"     , label:"待确认凹凸点"  , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"z_d_hs_cnt"     , index:"z_d_hs_cnt"      , label:"待确认划伤"    , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"z_d_ssbj_cnt"   , index:"z_d_ssbj_cnt"    , label:"酸蚀不均"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"z_d_zw_cnt"     , index:"z_d_zw_cnt"      , label:"待确认脏污"    , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"z_d_ly_cnt"     , index:"z_d_ly_cnt"      , label:"待确认轮印"    , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"z_ps_cnt"       , index:"z_ps_cnt"        , label:"制程破损"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"z_lw_cnt"       , index:"z_lw_cnt"        , label:"制程裂纹"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"z_lw_cnt"       , index:"z_lw_cnt"        , label:"制程漏液"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"z_ls_cnt"       , index:"z_ls_cnt"        , label:"制程漏酸"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    			    {name:"z_fs_cnt"       , index:"z_fs_cnt"        , label:"制程粉碎"      , width :DEFDSC_CLM  ,align:"left" ,sortable:true},
	    			    {name:"z_other"        , index:"z_other"         , label:"制程其他"      , width :DEFDSC_CLM ,align:"left" ,sortable:true},
	    	            {name:"remark"         , index:"remark"          , label:"备注"          , width :DEFDSC_CLM ,align:"left" ,sortable:true}
	      		]
	        });
	  };
	  controlsQuery.boxidTxt.keydown(function(event){
	    	if(event.keyCode == 13){
	    		btnFunc.f1_func();
	    	}
	   });
	  /**
		 * Bind button click action
		 */
	    var iniButtonAction = function(){
	        btnQuery.f1.click(function(){
	            btnFunc.f1_func();
	        });
	    };
	  /**
		 * Ini view and data
		 */
	    var initializationFunc = function(){
	    	controlsQuery.mainGrd.fatherDiv003.show();
	    	controlsQuery.mainGrd.fatherDiv006.hide();
	    	controlsQuery.mainGrd.fatherDiv008.hide();
	    	controlsQuery.mainGrd.fatherDiv063.hide();
	    	controlsQuery.mainGrd.fatherDiv115.hide();
		    controlsQuery.mainGrd.fatherDivTM.hide();
		    controlsQuery.mainGrd.fatherDivOther.hide();
		    resizeFnc();
	        iniGridInfo();
	        toolFunc.iniClear();
	        iniButtonAction();
	        getPrinters();
	    };
	    initializationFunc();
	    
		function resizeFnc(){                                                                      		    
			var divWidth003 = controlsQuery.mainGrd.fatherDiv003.width();                                   
			var offsetBottom003 = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDiv003.offset().top;
			controlsQuery.mainGrd.fatherDiv003.height(offsetBottom003 * 0.95);                          
			controlsQuery.mainGrd.grdId003.setGridWidth(divWidth003 * 0.99);                   
			controlsQuery.mainGrd.grdId003.setGridHeight(offsetBottom003 * 0.99 - 51);  
			
			var divWidth006 = controlsQuery.mainGrd.fatherDiv006.width();                                   
			var offsetBottom006 = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDiv006.offset().top;
			controlsQuery.mainGrd.fatherDiv006.height(offsetBottom006 * 0.95);                          
			controlsQuery.mainGrd.grdId006.setGridWidth(divWidth006 * 0.99);                   
			controlsQuery.mainGrd.grdId006.setGridHeight(offsetBottom006 * 0.99 - 51);  
			
			var divWidth008 = controlsQuery.mainGrd.fatherDiv008.width();                                   
			var offsetBottom008 = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDiv008.offset().top;
			controlsQuery.mainGrd.fatherDiv008.height(offsetBottom008 * 0.95);                          
			controlsQuery.mainGrd.grdId008.setGridWidth(divWidth008 * 0.99);                   
			controlsQuery.mainGrd.grdId008.setGridHeight(offsetBottom008 * 0.99 - 51);  
			
			var divWidth063 = controlsQuery.mainGrd.fatherDiv063.width();                                   
			var offsetBottom063 = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDiv063.offset().top;
			controlsQuery.mainGrd.fatherDiv063.height(offsetBottom063 * 0.95);                          
			controlsQuery.mainGrd.grdId063.setGridWidth(divWidth063 * 0.99);                   
			controlsQuery.mainGrd.grdId063.setGridHeight(offsetBottom063 * 0.99 - 51);
			
			var divWidth115 = controlsQuery.mainGrd.fatherDiv115.width();                                   
			var offsetBottom115 = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDiv115.offset().top;
			controlsQuery.mainGrd.fatherDiv115.height(offsetBottom115 * 0.95);                          
			controlsQuery.mainGrd.grdId115.setGridWidth(divWidth115 * 0.99);                   
			controlsQuery.mainGrd.grdId115.setGridHeight(offsetBottom115 * 0.99 - 51);
			
			var divWidthTM = controlsQuery.mainGrd.fatherDivTM.width();                                   
			var offsetBottomTM = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDivTM.offset().top;
			controlsQuery.mainGrd.fatherDivTM.height(offsetBottomTM * 0.95);                          
			controlsQuery.mainGrd.grdIdTM.setGridWidth(divWidthTM * 0.99);                   
			controlsQuery.mainGrd.grdIdTM.setGridHeight(offsetBottomTM * 0.99 - 51);   
			
			var divWidthOther = controlsQuery.mainGrd.fatherDivOther.width();                                   
			var offsetBottomOther = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDivOther.offset().top;
			controlsQuery.mainGrd.fatherDivOther.height(offsetBottomOther * 0.95);                          
			controlsQuery.mainGrd.grdIdOther.setGridWidth(divWidthOther * 0.99);                   
			controlsQuery.mainGrd.grdIdOther.setGridHeight(offsetBottomOther * 0.99 - 51);  
			
	    };                                                                                         
                                                                           
	    controlsQuery.W.resize(function() {                                                         
	    	resizeFnc();                                                                             
		});
  });