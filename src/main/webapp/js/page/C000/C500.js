  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  RePrint Management                                    */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2016/03/08 N0.00   wfq      Initial release                      */
  /*                                                                        */
  /**************************************************************************/
  $(document).ready(function() {
	  var label = new Label();
	  var VAL ={
	        NORMAL     : "0000000"  ,
	        EVT_USER   : $("#userId").text(),
	        T_XPINQPALT : "XPINQPALT"
	  };
	  var controlsQuery = {
		    W                  : $(window),
		    palletIdTxt        : $("#palletIdTxt"),
			print1Select       : $("#print1Select"),
			noteTxt            : $("#noteTxt"),
			mainGrd:{
				grdId          : $("#boxInfGrd"),
				grdPgText      : "#boxInfPg",
	            fatherDiv      : $("#boxInfDiv")
			}
		};
	  var btnQuery ={
		  f1    : $("#f1_query_btn"),
		  print : $("#print_btn")
	  };
	  var toolFunc = {
		  iniClear:function(){
			
			controlsQuery.palletIdTxt.val("");
			controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
			$("form").submit(function(){
                return false ;
             });
		  }
	  };
	  /**
	     * All button click function
	     * @type {Object}
	     */
	    var btnFunc = {
	        //Query
	        f1_func : function(){
	        	var inObj,outObj,pallet_id;
	        	pallet_id = controlsQuery.palletIdTxt.val();
        		if(pallet_id==""){
        			showErrorDialog("","请输入栈板号！");
	                return false;
        		}
        		inObj={
        			trx_id : VAL.T_XPINQPALT,
        			action_flg : 'Q',
        			pallet_id  : pallet_id
        		}
        		outObj = comTrxSubSendPostJson(inObj);
                if(outObj.rtn_code == VAL.NORMAL) {
                    setGridInfo(outObj.oary2,"#boxInfGrd");
                }
	        },
	        print_func:function(){
	        	var tcnt = 0;
	        	var ok_tcnt = 0;
	        	var modeCnt = 0;
	        	var cus_id,
	        	    pallet_id,
	        	    mdlId,
	        	    mtrlId,
	        	    mtrlPart,
	        	    to_thickness,
	        	    realSo,
	        	    box_cnt,
	        	    galssFrom,
	        	    type,
	        	    crdate,
	        	    crTime,
	        	    crYear,
	        	    crMonth,
	        	    crday,
	        	    note;
	        	pallet_id = controlsQuery.palletIdTxt.val();
        		if(pallet_id==""){
        			showErrorDialog("","请输入栈板号！");
	                return false;
        		}
	        	var rowIds = controlsQuery.mainGrd.grdId.jqGrid('getDataIDs');
	        	if( rowIds.length == 0){
	        		showErrorDialog("","没有要打印的数据！");
	                return false;
	        	}
	        	note = controlsQuery.noteTxt.val();
	        	if(note==""){
	        		showErrorDialog("","请输入备注！");
	                return false;
	        	}
	        	for(i=0;i<rowIds.length;i++){
	        		var rowData = controlsQuery.mainGrd.grdId.jqGrid('getRowData',rowIds[i]);
	        		tcnt = tcnt + parseInt(rowData['prd_qty'], 10);
	        		if(i==0){
	        			mdlId = rowData['final_mdl'];
	        			cus_id = rowData['cus_id'];
	        			mtrlId = rowData['cus_mtrl_id'];
	        			mtrlPart = rowData['mtrl_part'];
	        			to_thickness = rowData['to_thickness'];
	        			realSo = rowData['real_so'];
	        			galssFrom = rowData['glass_from'];
	        		}
	        	}
	            box_cnt = rowIds.length;
        		crTime = new Date();
        	    crYear = crTime.getFullYear().toString();
                crMonth = crTime.getMonth() + 1;
                crday = crTime.getDate();
                crdate = crYear + "-" + crMonth + "-" + crday;

                try{
//                    if(cus_id=="007"){
//                  	  showMessengerSuccessDialog("正在打印栈板标签...",1);
//                      label.PrintLabelForWhot007(controlsQuery.print1Select.find("option:selected").text(), mdlId, box_cnt,tcnt,pallet_id);
//                    }
//                    if(cus_id=="003"){
//                 		var boxAry = [];
//              			for(j=0; j<12; j++){
//            				boxAry[j] = "";
//            	        };
//                    	for(i=0;i<rowIds.length;i++){
//                    	  boxAry[i] = rowData['box_id'];
//                    	  var r_cnt=0,c_cnt=0;
//                 	      var inObject={
//                			   trx_id  : "XPINQDEF",
//                			   action_flg : "O"   ,
//                			   box_id  : rowData['box_id'],
//                			   proc_id : "DMQA",
//                			   check_flg : 'N'
//                	        };
//                	       var outObject =  comTrxSubSendPostJson(inObject);
//                	        if( outObject.rtn_code == VAL.NORMAL ){
//               			      var oary = outObject.prd_cnt > 1 ? outObject.oary003 : [outObject.oary003];
//               			      for(var j=0; j<outObject.prd_cnt ; j++){
//               			    	r_cnt = r_cnt +parseInt(oary[j].r_llhx_cnt,10) + parseInt(oary[j].r_atd_cnt,10) + parseInt(oary[j].r_pl_cnt,10)+ parseInt(oary[j].r_lh_cnt,10) +
//               				   			parseInt(oary[j].r_qj_cnt,10) + parseInt(oary[j].r_hs_cnt,10) + parseInt(oary[j].r_bw_cnt,10) + parseInt(oary[j].r_other,10);
//               				    c_cnt = c_cnt+parseInt(oary[j].c_pl_cnt,10) + parseInt(oary[j].c_lh_cnt,10) + parseInt(oary[j].c_qj_cnt,10)+ parseInt(oary[j].c_cj_cnt,10) +
//        			   				parseInt(oary[j].c_ly_cnt,10) + parseInt(oary[j].c_lsw_cnt,10) + parseInt(oary[j].c_td_cnt,10) + parseInt(oary[j].c_lhy_cnt,10) +
//        			   				parseInt(oary[j].c_ad_cnt,10) + parseInt(oary[j].c_hs_cnt,10) + parseInt(oary[j].c_bw_cnt,10) + parseInt(oary[j].c_other,10);
//               			       }
//                	        }
//                	        var okCount = parseInt(rowData['mode_cnt'],10)-r_cnt-c_cnt;
//                	        ok_tcnt = ok_tcnt + okCount;
//                	        modeCnt = modeCnt + parseInt(rowData['mode_cnt'],10);
//                    	}
//                    	  var oktotalCnt = ok_tcnt + "/" + modeCnt;
//                    	  var substrate =  mdlId + " " + mtrlPart + " " + to_thickness + "T SH";
//                    		 if(galssFrom == "CT2A"){
//                    			 type = "T";
//                    		 }else if(galssFrom == "L1A"){
//                    			 type = "A";
//                    		 }else if(galssFrom == "L1B"){
//                    			 type = "B";
//                    		 }else if(galssFrom == "L2C"){
//                    			 type = "C";
//                    		 }else if(galssFrom == "L2F"){
//                    			 type = "F";
//                    		 }else{
//                    			 type = "";
//                    		 }
//                    		 
//                    	  showMessengerSuccessDialog("正在打印栈板标签...",1);
//                    	  label.PrintLabelForWhot003( controlsQuery.print1Select.find("option:selected").text(), pallet_id, mdlId,mtrlId,tcnt,oktotalCnt,substrate,
//                  				"",realSo,crdate,box_cnt,type);
//                    	  label.PrintLabelForWhotBoxList003( controlsQuery.print1Select.find("option:selected").text(), JSON.stringify(boxAry));
//                      }
//                 	label.PrintWhotHct(controlsQuery.print1Select.find("option:selected").text(),note,mdlId,realSo,tcnt);
                	label.PrintpalforHX(controlsQuery.print1Select.find("option:selected").text(), mdlId,box_cnt, tcnt,pallet_id,"201706");
                }catch(ex){
                 	showMessengerErrorDialog("栈板标签打印失败!",1);
              	    console.error(ex);
                }
	        }
	    };
	  var iniGridInfo = function(){
 			var grdInfoCM = [
	 				{name:"box_id"       , index:"box_id"          , label:BOX_ID_TAG        ,align:"left" },
		            {name:"cus_id"       , index:"cus_id"          , label:CUS_ID_TAG        ,align:"left" },
		            {name:"wo_id"        , index:"wo_id"           , label:WO_ID_TAG    ,align:"left" },
		            {name:"so_id"        , index:"so_id"           , label:SO_ID_TAG        ,align:"left" },
		            {name:"real_so"      , index:"real_so"         , label:"实际商务订单"        ,align:"left" },
		            {name:"final_mdl"    , index:"final_mdl"       , label:"成品型号"            ,align:"left" },
		            {name:"cus_mtrl_id"  , index:"cus_mtrl_id"     , label:"客户料号"            ,align:"left" },
		            {name:"prd_qty"      , index:"prd_qty"         , label:PRD_QTY_TAG        ,align:"left" },
					{name:"pallet_id"    , index:"pallet_id"       , label:PALLET_ID_TAG     ,align:"left" },
					{name:"phy_pallet_id", index:"phy_pallet_id"   , label:"物理栈板号"       ,align:"left"},
					{name:"to_thickness"     , index:"to_thickness"   , label:"厚度"       ,align:"left"},
					{name:"glass_from"     , index:"glass_from"   , label:"基板来源"       ,align:"left"},
					{name:"mode_cnt"     , index:"mode_cnt"   , label:"模数"       ,align:"left",hidden : true},
					{name:"mtrl_part"     , index:"mtrl_part"   , label:"材质"       ,align:"left",hidden : true}
  		];
	        controlsQuery.mainGrd.grdId.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      width:1170,
			      height:400,
			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, //显示总记录数
			      rownumbers  :true ,//显示行号
			      rowNum:50,         //每页多少行，用于分页
			      rownumWidth : 40,  //行号列宽度
			      emptyrecords :true ,
			      sortname : "rcv_timestamp",
			      sortorder : "asc",
		          colModel: grdInfoCM
	        });
	  };
	  /**
	     * Bind button click action
	     */
	    var iniButtonAction = function(){
	        btnQuery.f1.click(function(){
	            btnFunc.f1_func();
	        });
	        btnQuery.print.click(function(){
	            btnFunc.print_func();
	        });
	    };
	    var otherActionBind = function(){
	    	controlsQuery.palletIdTxt.keydown(function(event){
	            if(event.keyCode == ENT_KEY){
	                btnFunc.f1_func();
	            }
	        });
	    };
	  /**
	     * Ini view and data
	     */
	    var initializationFunc = function(){
	        iniGridInfo();
	        toolFunc.iniClear();
	        iniButtonAction();
	        otherActionBind();
	        getPrinters();

	    };
	    initializationFunc();
		  /**
		   * 获取打印机
		   */
		  function getPrinters(){
			  controlsQuery.print1Select.append("<option ></option>");
			  if (printers !== undefined) {
				  for(var i=0; i<printers.length; i++){
					  controlsQuery.print1Select.append("<option>"+ printers[i] +"</option>");
				  }
			  }
			  controlsQuery.print1Select.select2({
		 	    	theme : "bootstrap"
		 	    });
		  }
  });