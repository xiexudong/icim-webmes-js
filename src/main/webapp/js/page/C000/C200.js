  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  RePrint Lot Label Management                          */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2014/06/26 N0.00   meijiao.C      Initial release                      */
  /*                                                                        */
  /**************************************************************************/
  $(document).ready(function() {
	  var label = new Label();
	  var VAL ={
	        NORMAL     : "0000000"  ,
	        EVT_USER   : $("#userId").text(),
	        T_XPINQLOT : "XPINQLOT",
	  };
	  var controlsQuery = {
		    W                  : $(window)             ,
		    lotidTxt           : $("#lotidTxt"),
			mainGrd:{
				grdId          : $("#shtListGrd"),
				grdPgText      : "#shtListPg"       ,
	            fatherDiv      : $("#shtListDiv")
			}
		};
	  var btnQuery ={
		  f1    : $("#f1_query_btn"),
		  print : $("#print_btn")
	  };
	  var toolFunc = {
		  iniClear:function(){
			controlsQuery.lotidTxt.val("");
			controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
			$("form").submit(function(){
                return false ;
             });
		  }
	  };
	  /**
	     * All button click function
	     * @type {Object}
	     */
	    var btnFunc = {
	        //Query
	        f1_func : function(){
	        	var inObj,outObj;
	        	var lot_id = controlsQuery.lotidTxt.val();
	        	if( !lot_id){
	        		showErrorDialog("","请输入批次号！");
	                return false;
	        	}
	        	inObj={
	        		trx_id : VAL.T_XPINQLOT,
	        		action_flg : 'L',
	        		lot_id : lot_id
	        	}
	        	outObj =  comTrxSubSendPostJson(inObj);
	        	if( outObj.rtn_code == VAL.NORMAL ){
	        		setGridInfo(outObj.oary,"#shtListGrd");
	        	}
	        },
	        print_func:function(){
	        	var rowIds,crGrid,rowData,lot_id; 
	        	crGrid = controlsQuery.mainGrd.grdId;
	        	rowIds = crGrid.jqGrid('getDataIDs');
	        	if(rowIds.length == 0){
	        		showErrorDialog("","请根据批次号查询出产品信息！");
	                return false;
	        	}
	        	rowData = crGrid.jqGrid('getRowData',rowIds[0]);
	        	lot_id = rowData.lot_id_fk;
				//label.PrintBacth(lot_id);
				label.PrintLabelForBox(lot_id);
				showSuccessDialog("补印成功");
				toolFunc.iniClear();
	  		}
	  };
	  var iniGridInfo = function(){
 			var grdInfoCM = [
 				{name:"prd_seq_id"     , index:"prd_seq_id"      , label:PRD_SEQ_ID_TAG , width :PRD_SEQ_ID_CLM  ,align:"left" ,hidden:true},
 				{name : 'fab_sn'       , index : 'fab_sn'        ,label : PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM,align:"left" ,sortable:true},
	            {name:"box_id_fk"      , index:"box_id_fk"       , label:PPBOX_ID_TAG   , width :BOX_ID_CLM ,align:"left" ,sortable:true},
	            {name:"lot_id_fk"      , index:"lot_id_fk"       , label:LOT_ID_TAG     , width :LOT_ID_CLM  ,align:"left" ,sortable:true},
				{name:"wo_id_fk"       , index:"wo_id_fk"        , label:WO_ID_TAG      , width :WO_ID_CLM ,align:"left" ,sortable:true},
  		];
	        controlsQuery.mainGrd.grdId.jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      autowidth:true,//宽度根据父元素自适应
			      height:400,
			      shrinkToFit:true,
			      scroll:false,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      jsonReader : {
			            // repeatitems: false
			          },
			      viewrecords : true, //显示总记录数
			      rownumbers  :true ,//显示行号
			      rowNum:50,         //每页多少行，用于分页
			      rownumWidth : 40,  //行号列宽度
			      emptyrecords :true ,
		          colModel: grdInfoCM
	        });
	  };
	  controlsQuery.lotidTxt.keydown(function(event){
	    	if(event.keyCode == 13){
	    		btnFunc.f1_func();
	    	}
	   });
	  /**
	     * Bind button click action
	     */
	    var iniButtonAction = function(){
	        btnQuery.f1.click(function(){
	            btnFunc.f1_func();
	        });
	        btnQuery.print.click(function(){
	            btnFunc.print_func();
	        });
	    };
	  /**
	     * Ini view and data
	     */
	    var initializationFunc = function(){
	        iniGridInfo();
	        toolFunc.iniClear();
	        iniButtonAction();
	    };
	    initializationFunc();
  });