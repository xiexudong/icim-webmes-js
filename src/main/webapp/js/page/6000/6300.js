
  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Bis_data Management                                   */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/04/28 N0.00   Lin.Xin      Initial release                        */
  /* 2013/09/03 N0.01   meijiao.C    Add autr for CODE                      */
  /*                                                                        */
  /**************************************************************************/

$(document).ready(function() {
  var _NORMAL = "0000000";
  $("form").submit(function(){
      return false;  
  });
  var colNames = [MODIFY_BTN_TAG,DELETE_BTN_TAG,DATA_CATE_TAG,DATA_ID_TAG,DATA_EXT_TAG,DATA_ITEM_TAG,
                  EXT_1_TAG,EXT_2_TAG,EXT_3_TAG,EXT_4_TAG,EXT_5_TAG,DATA_SEQ_ID_TAG,DATA_DESC_TAG];
  var itemInfoCM = [
        {name: 'modifyItem'   , index: 'modifyItem'    , label: MODIFY_BTN_TAG,     width: 50 },
        {name: 'deleteItem'   , index: 'deleteItem'    , label: DELETE_BTN_TAG,     width: 50 },
        {name: 'data_cate'    , index: 'data_cate'     , label: DATA_CATE_TAG,      width: 60 ,hidden:true},
        {name: 'data_id'      , index: 'data_id'       , label: DATA_ID_TAG,        width: 60 },
        {name: 'data_ext'     , index: 'data_ext'      , label: DATA_EXT_TAG,       width: 75},
        {name: 'data_item'    , index: 'data_item'     , label: DATA_ITEM_TAG,      width: 100},
        {name: 'ext_1'        , index: 'ext_1'         , label: EXT_1_TAG,          width: 80},
        {name: 'ext_2'        , index: 'ext_2'         , label: EXT_2_TAG,          width: 80},
        {name: 'ext_3'        , index: 'ext_3'         , label: EXT_3_TAG,          width: 80},
        {name: 'ext_4'        , index: 'ext_4'         , label: EXT_4_TAG,          width: 80},
        {name: 'ext_5'        , index: 'ext_5'         , label: EXT_5_TAG,          width: 80},
        {name: 'data_seq_id'  , index: 'data_seq_id'   , label: DATA_SEQ_ID_TAG,    width: 60 ,hidden:true},
        {name: 'data_desc'    , index: 'data_desc'     , label: DATA_DESC_TAG,      width: 160}
  ];
  $("#dataListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      // height:400,//TODO:需要根据页面自适应，要相对很高
      // width:"99%",
      autowidth:true,//宽度根据父元素自适应
      shrinkToFit:true,
      scroll:false,
      resizable : true,
      loadonce:true,
      // toppager: true , 翻页控件是否在上层显示
      fixed: true,
      // hidedlg:true,
      jsonReader : {
            // repeatitems: false
          },
      viewrecords : true, //显示总记录数
      pager : '#dataListPg',
      rownumbers  :true ,//显示行号
      rowNum:20,         //每页多少行，用于分页
      rownumWidth : 20,  //行号列宽度
      emptyrecords :true ,
      pginput  : true ,//可以输入跳转页数
      rowList:[10,15,20], //每页多少行
      toolbar : [true, "top"],//显示工具列 : top,buttom,both
      colModel: itemInfoCM,
      colNames: colNames,
      gridComplete:function(){
        addGridButton();
      } 
  });
  $("#dataListGrd").jqGrid('navGrid','#dataListPg',{ del:false,add:false,edit:false},{},{},{},{multipleSearch:true});
  //页面高度变化时，自动调整Grid大小
  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#dataListGrd").setGridWidth($("#dataListDiv").width()*0.95); 
      $("#dataListGrd").setGridHeight($("#dataListDiv").height()*0.80);     
      $(window).bind("onresize", this);  
  };
  //显示输入提醒
  $('[rel=tooltip]').tooltip({
    placement:"bottom",
    trigger:"focus",
    animation:true
  });
  /***** getCATE :
    获取所有的data_cate : sql
      select data_id from bis_data  where data_cate ='CATE'
  **********/
  function getCATE(){
  	var adminFlag = "";
    $("#dataCateSel").empty();
    //-->N0.01 根据权限,显示登陆用户可以看见的CODE
         $.post('jcom/checkUserAdmin', {
			}, function(admin) {
				if( admin== false){
					var inObj = {
						trx_id     : 'XPINQCOD' ,
						action_flg : 'S'        ,
						data_cate  : 'CATE',
						data_ext   : $("#userId").text()
					};
					var outObj = comTrxSubSendPostJson( inObj );
					for( var i=0;i<outObj.tbl_cnt;i++ ){
						$("#dataCateSel").append("<option value='Value'>"+ outObj.oary[i].data_id +"</option>");
					}
				}else{
				    var iary = {
				        data_cate  : 'CATE'
				    };
				    var inTrxObj = {
				         trx_id     : 'XPLSTDAT' ,
				         action_flg : 'Q'        ,
				         iary       : iary
				    };
				    var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
					for(var i=0;i<outTrxObj.tbl_cnt;i++){
						if( outTrxObj.oary[i].data_id == "UFUG" ){
            				;
            			}else{
            				$("#dataCateSel").append("<option value='Value'>"+ outTrxObj.oary[i].data_id +"</option>");
            			}						
					}
				}
				$("#dataCateSel").select2({
			    	theme : "bootstrap"
			    });
            });//<--N0.01
          dataCateSelFnc();
  }
  function initFnc(){
    resetJqgrid();
    getCATE();
    //分页控件 高度控制
    $("#dataListPg").css({"height":"30px"});
    $(".ui-pg-selbox").css({"height":"28px"});
  }
  initFnc();

  /****** dataCateSelFnc :
    查询DATA_CATE的描述，
       select data_desc from bis_data 
              where data_cate ='CATE' and 
              data_id=$("#dataCateSel").find("option:selected").text()

  *******/
  function dataCateSelFnc(){
     $("#cateDesSpn").text("");
     var iary = {
          data_cate  : 'CATE'     ,
          data_id    : $("#dataCateSel").find("option:selected").text()
     };
     var inTrxObj = {
         trx_id     : 'XPLSTDAT' ,
         action_flg : 'Q'        ,
         iary       : iary

     };
     var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
     if(  outTrxObj.rtn_code == _NORMAL ) {
          var dataCnt = comParseInt( outTrxObj.tbl_cnt);
          if( dataCnt == 1 ){
            $("#cateDesSpn").text(outTrxObj.oary.data_desc);
          }else if(dataCnt>1){
            $("#cateDesSpn").text(outTrxObj.oary[0].data_desc);
          }
          f1QueryFnc();
     }
  }
  function translateFnc(){
    var tbl_cnt,
        iary,
        inTrxObj,
        outTrxObj;

      iary = {
        data_cate : "CODE",
        data_id   : $("#dataCateSel").find("option:selected").text()
      };
      inTrxObj = {
        trx_id     : 'XPLSTDAT' ,
        action_flg : "Q"        ,
        iary       : iary       ,
        tbl_cnt    : 1
      };
      outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if( outTrxObj.rtn_code == _NORMAL ) {
         tbl_cnt = outTrxObj.tbl_cnt;
         if(tbl_cnt ==0){
            $("#dataListGrd").jqGrid("setLabel","data_id",DATA_ID_TAG); 
            $("#dataListGrd").jqGrid("setLabel","data_ext",DATA_EXT_TAG);
            $("#dataListGrd").jqGrid("setLabel","data_item",DATA_ITEM_TAG);
            $("#dataListGrd").jqGrid("setLabel","data_desc",DATA_DESC_TAG);
            $("#dataListGrd").jqGrid("setLabel","ext_1",EXT_1_TAG);
            $("#dataListGrd").jqGrid("setLabel","ext_2",EXT_2_TAG);
            $("#dataListGrd").jqGrid("setLabel","ext_3",EXT_3_TAG);
            $("#dataListGrd").jqGrid("setLabel","ext_4",EXT_4_TAG);
            $("#dataListGrd").jqGrid("setLabel","ext_5",EXT_5_TAG);
            return false;
         }
         for(var i=0;i<tbl_cnt;i++){
            if(tbl_cnt==1){
              oary = outTrxObj.oary;
            }else{
              oary = outTrxObj.oary[i];
            }
            switch(oary.data_ext){
              case "DATA_ID":
                   $("#dataListGrd").jqGrid("setLabel","data_id",oary.data_desc);
                   break;
              case "DATA_EXT":
                   $("#dataListGrd").jqGrid("setLabel","data_ext",oary.data_desc);
                   break;
              case "DATA_ITEM":
                   $("#dataListGrd").jqGrid("setLabel","data_item",oary.data_desc);
                   break;
              case "DATA_DESC":
                   $("#dataListGrd").jqGrid("setLabel","data_desc",oary.data_desc);
                   break;
              case "EXT_1":
                   $("#dataListGrd").jqGrid("setLabel","ext_1",oary.data_desc);
                   break;
              case "EXT_2":
                   $("#dataListGrd").jqGrid("setLabel","ext_2",oary.data_desc);
                   break;
              case "EXT_3":
                   $("#dataListGrd").jqGrid("setLabel","ext_3",oary.data_desc);
                   break;
              case "EXT_4":
                   $("#dataListGrd").jqGrid("setLabel","ext_4",oary.data_desc);
                   break;
              case "EXT_5":
                   $("#dataListGrd").jqGrid("setLabel","ext_5",oary.data_desc);
                   break;                    
            }
         }
      }
      
  }
  $("#f10_clear_btn").click(function(){
    $("#dataListGrd").jqGrid("setLabel","ext_5","测试用");
  });
  $("#dataCateSel").change(function(){
      dataCateSelFnc();
      translateFnc();
  });

  /*****
    说明:(1) addGridButton :在Grid中添加了两个Button: "修改" , "删除"
         (2) modifyFnc     :点击修改时,调用此函数.
         (3) deleteFnc     :点击删除时,调用此函数.
  *********/
  function modifyFnc(){
    var rowData  = $("#dataListGrd").jqGrid("getRowData",this.id.substr(3));
     $("#f6_add_btn").showBisDataDialog({
          action_flg  : "U",
          data_seq_id : rowData.data_seq_id,
          data_cate   : rowData.data_cate,
          data_id     : rowData.data_id,
          data_ext    : rowData.data_ext,
          data_item   : rowData.data_item,
          ext_1       : rowData.ext_1,
          ext_2       : rowData.ext_2,
          ext_3       : rowData.ext_3,
          ext_4       : rowData.ext_4,
          ext_5       : rowData.ext_5,
          data_desc   : rowData.data_desc,
        callbackFn : function(data) {
          if(data.result==true){
            showSuccessDialog("修改参数成功");  
            f1QueryFnc();
          }
        }
      });
     //屏蔽其他事件
      return false;
  }
  function deleteFnc(){
    var obj = this ;
    $("#deleteDatacateDialog_deleteDateCateBtn").showCallBackWarnningDialog({
        errMsg  : "是否删除参数,请确认!!!!",
        callbackFn : function(data) {
          if(data.result==true){
            var rowData  = $("#dataListGrd").jqGrid("getRowData",obj.id.substr(3));
            var iary = {
              data_seq_id : rowData.data_seq_id,
              data_cate   : rowData.data_cate,
              data_id     : rowData.data_id,
              data_ext    : rowData.data_ext
            };
            var inTrxObj = {
                trx_id     : 'XPLSTDAT' ,
                action_flg : 'D'        ,
                iary       : iary 
            };
            var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if(  outTrxObj.rtn_code == _NORMAL ) {
                 showSuccessDialog("删除参数成功");
                 f1QueryFnc();
              // setGridInfo(outTrxObj.oary,"#dataListGrd");
              // addGridButton();
            }  
          }
        }
    });
    return false ;
  }
  function addGridButton(){
    var ids=$("#dataListGrd").jqGrid('getDataIDs');
    for(var i=0; i<ids.length; i++){
        var id=ids[i];   
        var modify = "<button id=" + "'mod" + ids[i] + "'" + " style='color:#f60'>修改</button>";  
        var deleteItem = "<button id=" + "'del" + ids[i] + "'" + " style='color:#f60'>删除</button>";  
        $("#dataListGrd").jqGrid('setRowData', ids[i], { modifyItem: modify,deleteItem:deleteItem});
        $("#mod" + ids[i]).click( modifyFnc );
        $("#del" + ids[i]).click( deleteFnc );
    }  
  }

  function f1QueryFnc(){
    var iary = {
      data_cate : $("#dataCateSel").find("option:selected").text()
    };
    var inTrxObj = {
        trx_id     : 'XPLSTDAT' ,
        action_flg : 'Q'        ,
        iary       : iary 
    };
    var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(  outTrxObj.rtn_code == _NORMAL ) {
        setGridInfo(outTrxObj.oary,"#dataListGrd",false,true);
        addGridButton();
    }
  }
  function f6AddFnc(){
     $("#f6_add_btn").showBisDataDialog({
        action_flg:"A",
        data_cate : $("#dataCateSel").find("option:selected").text(),
        callbackFn : function(data) {
            if(data.result==true){
               showSuccessDialog("新增参数成功");
               f1QueryFnc();
            }
        }
      });
  }

  /******
      f8AddDataCateFnc;
      新增 Data_cate;
      insert into bis_data(data_cate,data_id,data_dsc) 
      values('CATE', $("#addDataCateDialog_dataCateTxt").val() , data_desc );
  ****/
  function f8AddDataCateFnc(){
    $("#addDataCateDialog_dataCateTxt").val("");
    $("#addDataCateDialog_dataDescTxt").val("");
    
    $("#addDataCateDialog_dsc_data_IdTxt").val("");
    $("#addDataCateDialog_dsc_data_ExtTxt").val("");
    $("#addDataCateDialog_dsc_data_ItemTxt").val("");
    $("#addDataCateDialog_dsc_ext_1Txt").val("");
    $("#addDataCateDialog_dsc_ext_2Txt").val("");
    $("#addDataCateDialog_dsc_ext_3Txt").val("");
    $("#addDataCateDialog_dsc_ext_4Txt").val("");
    $("#addDataCateDialog_dsc_ext_5Txt").val("");
    $("#addDataCateDialog_dsc_data_DscTxt").val("");
    
    $("#addDatacateDialog_addDateCateBtn").unbind('click');
    $("#addDatacateDialog_addDateCateBtn").bind('click',function(){
      var data_cate = $("#addDataCateDialog_dataCateTxt").val();
      var data_desc = $("#addDataCateDialog_dataDescTxt").val();
      if( data_cate == "" ){
          showErrorDialog("003","参数类型必须填写");
          return false;
      }
      if( data_desc == "" ){
          showErrorDialog("003","参数描述必须填写");
          return false;
      }
      if( comCheckMaxLenth( data_cate , 4 ) == false ){
          showErrorDialog("003","参数类型大于最大长度4");
          return false;
      }
      if( comCheckMaxLenth( data_cate , 4 ) == false ){
          showErrorDialog("003","参数类型大于最大长度4");
          return false;
      }
      var iary = {
        data_cate      : data_cate   ,
        data_desc      : data_desc   ,
        dsc_data_id    : $("#addDataCateDialog_dsc_data_IdTxt").val()      ,
        dsc_data_ext   : $("#addDataCateDialog_dsc_data_ExtTxt").val()     ,
        dsc_data_item  : $("#addDataCateDialog_dsc_data_ItemTxt").val()    ,
        dsc_ext_1      : $("#addDataCateDialog_dsc_ext_1Txt").val()        ,
        dsc_ext_2      : $("#addDataCateDialog_dsc_ext_2Txt").val()        ,
        dsc_ext_3      : $("#addDataCateDialog_dsc_ext_3Txt").val()        ,
        dsc_ext_4      : $("#addDataCateDialog_dsc_ext_4Txt").val()        ,
        dsc_ext_5      : $("#addDataCateDialog_dsc_ext_5Txt").val()        ,
        dsc_data_dsc   :  $("#addDataCateDialog_dsc_data_DscTxt").val()
      };
      var inTrxObj = {
          trx_id     : 'XPLSTDAT' ,
          action_flg : 'N'        ,
          iary       : iary       ,
          tbl_cnt    : 1
      };
      var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(  outTrxObj.rtn_code == _NORMAL ) {
           showSuccessDialog("新增参数类型成功");//TODO
           $('#addDataCateDialog').modal('hide');
           getCATE();
      }
    });
    $('#addDataCateDialog').modal({
      backdrop:true ,
      keyboard:false,
      show:true
    });
  }

  function f9DeleteDataCateFnc(){

    function getDeleteDataCateDialogDataCateFnc(){
      $("#deleteDataCateDialog_dataCateSel").empty();
      var iary = {
        data_cate : 'CATE'
      };
      var inTrxObj = {
         trx_id     : 'XPLSTDAT' ,
         action_flg : 'Q'        ,
         iary       : iary 
      };
      var outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if( outTrxObj.rtn_code == _NORMAL ) {
          var dataCnt = comParseInt( outTrxObj.tbl_cnt);
          if( dataCnt ==1 ){
            $("#deleteDataCateDialog_dataCateSel").append("<option value='Value'>"+ outTrxObj.oary.data_id +"</option>");
          }else{
            for(var i=0;i<dataCnt;i++){
              $("#deleteDataCateDialog_dataCateSel").append("<option value='Value'>"+ outTrxObj.oary[i].data_id +"</option>");
            } 
          }

          $("#deleteDataCateDialog_dataCateSel").select2({
		    	theme : "bootstrap"
		    });
          deleteDataCateDialogDataCateSelFnc();
      }
    }
    function deleteDataCateDialogDataCateSelFnc(){
       $("#deleteDataCateDialog_dataDescSpn").text("");
       var iary = {
        data_cate : 'CATE'  ,
        data_id   : $("#deleteDataCateDialog_dataCateSel").find("option:selected").text()
       };
       var inTrxObj = {
           trx_id     : 'XPLSTDAT' ,
           action_flg : 'Q'        ,
           iary       : iary
       };
       var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
       if(  outTrxObj.rtn_code == _NORMAL ) {
            var dataCnt = comParseInt( outTrxObj.tbl_cnt);
            if( dataCnt == 1 ){
              $("#deleteDataCateDialog_dataDescSpn").text(outTrxObj.oary.data_desc);
            }else if(dataCnt>1){
              $("#deleteDataCateDialog_dataDescSpn").text(outTrxObj.oary[0].data_desc);
            }
       }
    }
    function f6DialogDeleteDataCateFnc(){
      $("#deleteDatacateDialog_deleteDateCateBtn").showCallBackWarnningDialog({
        errMsg  : "是否删除操作类型,请确认!!!!",
        callbackFn : function(data) {
          if(data.result==true){
            var iary = {
              data_cate:$("#deleteDataCateDialog_dataCateSel").find("option:selected").text()
            };
            var inTrxObj = {
                trx_id     : 'XPLSTDAT' ,
                action_flg : 'C'        ,
                iary       : iary
            };
            var outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if( outTrxObj.rtn_code == _NORMAL ) {
                showSuccessDialog("删除参数类型成功");//TODO
                getDeleteDataCateDialogDataCateFnc();
                getCATE();
            }
          }
        } 
      });
    }

    $("#deleteDataCateDialog_dataCateSel").empty();
    $("#deleteDataCateDialog_dataDescSpn").val("");
    /*****
      title :dialog的重复提交问题。
      reason: 多次执行modal('show')时,会多次绑定click事件。
              这样会带来点击一次click，执行多次提交的bug.
      solve :dialog弹出前,将unbind('click'),再bind('click'),只绑定一个click事件.

    *****/
    $("#deleteDatacateDialog_deleteDateCateBtn").unbind('click');
    $("#deleteDataCateDialog_dataCateSel").unbind('change');
    $("#deleteDatacateDialog_deleteDateCateBtn").bind('click',f6DialogDeleteDataCateFnc);
    $("#deleteDataCateDialog_dataCateSel").bind('change',deleteDataCateDialogDataCateSelFnc);

   
    $("#deleteDataCateDialog").modal({
        backdrop:true  ,
        keyboard:false ,
        show:true      
    });
    getDeleteDataCateDialogDataCateFnc();
  }
  // function f10ClearFnc(){
  //   $("#dataListGrd").jqGrid('clearGridData');
  // }

  $("#f1_query_btn").click(f1QueryFnc);
  $("#f6_add_btn").click(f6AddFnc);
  $("#f8_addDataCate_btn").click(f8AddDataCateFnc);
  $("#f9_deleteDataCate_btn").click(f9DeleteDataCateFnc);
  // $("#f10_clear_btn").click(f10ClearFnc);

});