
  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Bis_qrs Management                                   */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/04/28 N0.00   Lin.Xin      Initial release                        */
  /*                                                                        */
  /**************************************************************************/

$(document).ready(function() {
  var _NORMAL = "0000000";
  $("form").submit(function(){
      return false;
  });
  var itemInfoCM = [
    {name: 'ope_no_fk'    , index: 'ope_no_fk'    , label: OPE_ID_TAG        , width: 60 },
    {name: 'tool_id_fk'   , index: 'tool_id_fk'   , label: TOOL_ID_TAG       , width: 60 },
    {name: 'ope_id_fk'    , index: 'ope_id_fk'    , label: START_OPE_ID_TAG  , width: 120 },
    {name: 'ope_ver_fk'   , index: 'ope_ver_fk'   , label: START_OPE_VER_TAG , width: 120 },
    {name: 'mdl_typ_fk'   , index: 'mdl_typ_fk'   , label: MDL_ID_TAG        , width: 60 },
    {name: 'mdl_id_fk'    , index: 'mdl_id_fk'    , label: MDL_TYPE_TAG      , width: 60 },
    {name: 'path_id_fk'   , index: 'path_id_fk'   , label: PATH_ID_TAG       , width: 60 },
    {name: 'path_ver_fk'  , index: 'path_ver_fk'  , label: PATH_VER_TAG      , width: 60 },
    {name: 'mdl_id_fk'    , index: 'mdl_id_fk'    , label: OPE_VER_TAG       , width: 60 }
  ];
  $("#qrsListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:$("#qrsListDiv").height(),
      // height:400,//TODO:需要根据页面自适应，要相对很高
      width:$("#qrsListDiv").width()*0.99,
      // autowidth:true,//宽度根据父元素自适应
      autowidth:false,
      // shrinkToFit:true,
      shrinkToFit:false,
      // scroll:true,
      resizable : true,
      loadonce:true,
      // toppager: true , 翻页控件是否在上层显示
      // fixed: true,
      // hidedlg:true,
      jsonReader : {
            // repeatitems: false
          },
      viewrecords : true, //显示总记录数
      // pager : '#qrsListPg',
      rownumbers  :true ,//显示行号
      rowNum:20,         //每页多少行，用于分页
      // rownumWidth : 20,  //行号列宽度
      emptyrecords :true ,
      // pginput  : true ,//可以输入跳转页数
      // rowList:[10,15,20], //每页多少行
      // toolbar : [true, "top"],//显示工具列 : top,buttom,both
      colModel: itemInfoCM,
      gridComplete:function(){
          var gridPager,
              pageLen;
          gridPager = $(this).jqGrid("getGridParam","pager");
          if(gridPager.length<2){
             return false;
          }
          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
          $(".ui-pg-input").hide();
          $('td[dir="ltr"]').hide(); 
          $(gridPager+"_left").hide();
          $(gridPager+"_center").css({width:0});
      },
      onSelectRow:function(id){
        $("#qrsConditionForm input").attr({'disabled':true});
        $("#qrsConditionForm select").attr({'disabled':true});
        var rowData = $(this).jqGrid("getRowData",id);
        var iary ={};
        if(rowData.mdl_id_fk!=""){
          iary.mdl_id_fk =rowData.mdl_id_fk;
        }
        if(rowData.mdl_typ_fk!=""){
          iary.mdl_typ_fk = rowData.mdl_typ_fk;
        }
        if(rowData.path_id_fk!=""){
          iary.path_id_fk = rowData.path_id_fk;
        }
        if(rowData.path_ver_fk!=""){
          iary.path_ver_fk = rowData.path_ver_fk;
        }
        if(rowData.ope_id_fk!=""){
          iary.ope_id_fk = rowData.ope_id_fk;
        }
        if(rowData.ope_no_fk!=""){
          iary.ope_no_fk = rowData.ope_no_fk;
        }
        if(rowData.ope_ver_fk!=""){
          iary.ope_ver_fk = rowData.ope_ver_fk;
        }
        if(rowData.tool_id_fk!=""){
          iary.tool_id_fk = rowData.tool_id_fk;
        }
        var inTrxObj ={
          trx_id : "XPBISQRS",
          action_flg:"Q",
          iary   : iary,
          tbl_cnt: 1
        };
        var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if(  outTrxObj.rtn_code == _NORMAL ) {
          $("#qrsConditionForm select").empty();
          var oary={};
          var tbl_cnt = outTrxObj.tbl_cnt ;
          if( tbl_cnt > 0 ){
            setGridInfo(outTrxObj.oary,"#qrsItemListGrd");
            addGridButton();
            if(tbl_cnt==1){
               oary = outTrxObj.oary;
            }else if( tbl_cnt >1 ){
               oary = outTrxObj.oary[0]; 
            }
            if(oary.mdl_id_fk!=undefined){
              $("#mdlIDSel").append("<option value='Value'>"+ oary.mdl_id_fk +"</option>");
              $("#mdlIDSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oary.path_id_fk!=undefined){
              $("#pathIDSel").append("<option value='Value'>"+ oary.path_id_fk +"</option>");
              $("#pathIDSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oary.path_ver_fk!=undefined){
              $("#pathVerSel").append("<option value='Value'>"+ oary.path_ver_fk +"</option>");
              $("#pathVerSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oary.ope_no_fk!=undefined){
              $("#opeNOSel").append("<option value='Value'>"+ oary.ope_no_fk +"</option>");
              $("#opeNOSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oary.ope_id_fk!=undefined){
              $("#opeIDSel").append("<option value='Value'>"+ oary.ope_id_fk +"</option>");
              $("#opeIDSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oary.ope_ver_fk!=undefined){
              $("#opeVerSel").append("<option value='Value'>"+ oary.ope_ver_fk +"</option>");
              $("#opeVerSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oary.tool_id_fk!=undefined){
              $("#toolIDSel").append("<option value='Value'>"+ oary.tool_id_fk +"</option>");
              $("#toolIDSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            
          }
          
        }

      } 
  })
  //QRS 详细子项目明细
  var qrsItemInfoCM2 = [
    {name: 'addItem'          , index: 'addItem'        , label: "新增标记"    , width: 50 ,hidden:true},
    {name: 'modifyItem'       , index: 'modifyItem'     , label: MODIFY_BTN_TAG, width: 50 },
    {name: 'deleteItem'       , index: 'deleteItem'     , label: DELETE_BTN_TAG, width: 50 },
    {name: 'qrs_id'           , index: 'qrs_id'         , label: QRS_ID_TAG       , width: 70 },
    {name: 'qrs_path_id_fk'   , index: 'qrs_path_id_fk' , label: QRS_PATH_ID_TAG  , width: 120 },
    {name: 'qrs_path_ver_fk'  , index: 'qrs_path_ver_fk', label: QRS_PATH_VER_TAG , width: 120 },
    {name: 'qrs_ope_no_fk'    , index: 'qrs_ope_no_fk'  , label: QRS_OPE_NO_TAG   , width: 120 },
    {name: 'qrs_ope_id_fk'    , index: 'qrs_ope_id_fk'  , label: QRS_OPE_ID_TAG   , width: 120 },
    {name: 'qrs_ope_ver_fk'   , index: 'qrs_ope_ver_fk' , label: QRS_OPE_VER_TAG  , width: 120 },
    {name: 'qrs_typ'          , index: 'qrs_typ'        , label: QRS_TYPE_TAG     , width: 70 },
    {name: 'qrs_time'         , index: 'qrs_time'       , label: QRS_TIME_TAG     , width: 70 },
    {name: 'qrk_time'         , index: 'qrk_time'       , label: QRK_TIME_TAG     , width: 70 },
    {name: 'swh_path_id_fk'   , index: 'swh_path_id_fk' , label: SWH_PATH_ID_TAG  , width: 120 },
    {name: 'swh_path_ver_fk'  , index: 'swh_path_ver_fk', label: SWH_PATH_VER_TAG , width: 120 }
  ];
  $("#qrsItemListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:$("#qrsItemListDiv").height(),
      // height:400,//TODO:需要根据页面自适应，要相对很高
      // width: "99%", 
       
      // autowidth:true,//宽度根据父元素自适应
      autowidth: false,
      shrinkToFit:false,
      width:$("#qrsItemListDiv").width()*0.95,
      // shrinkToFit:true,
      // shrinkToFit:false,
      autoScroll: true, 
      scroll:false,//如需要分页控件,必须将此项置为false
      // resizable : true,
      loadonce:true,
      // toppager: true , 翻页控件是否在上层显示
      // fixed: true,
      // hidedlg:true,
      jsonReader : {
            // repeatitems: false
          },
      viewrecords : true, //显示总记录数
      emptyrecords :true ,
      // pginput  : true ,//可以输入跳转页数
      toolbar:true,
      colModel: qrsItemInfoCM2,
      gridComplete:function(){
        addGridButton
      },
      onSelectRow:function(id){

      } 
  });
  /*****
    说明:(1) addGridButton :在Grid中添加了两个Button: "修改" , "删除"
         (2) modifyFnc     :点击修改时,调用此函数.
         (3) deleteFnc     :点击删除时,调用此函数.
  *********/
  function addGridButton(){
    var ids=$("#qrsItemListGrd").jqGrid('getDataIDs');
    for(var i=0; i<ids.length; i++){
        var id=ids[i];   
        var modify = "<button id=" + "'mod" + ids[i] + "'" + " style='color:#f60'>修改</button>";  
        var deleteItem = "<button id=" + "'del" + ids[i] + "'" + " style='color:#f60'>删除</button>";  
        $("#qrsItemListGrd").jqGrid('setRowData', ids[i], { modifyItem: modify,deleteItem:deleteItem});
        $("#mod" + ids[i]).click( modifyFnc );
        $("#del" + ids[i]).click( deleteFnc );
    }  
  }
  function modifyFnc(){
       var rowData = $("#qrsItemListGrd").jqGrid("getRowData",this.id.substr(3));
       showAddQrsItemDialog( rowData );
      //屏蔽其他事件
      return false;
  }
  function deleteFnc(){
      var obj = this ;
      $("#f1_query_btn").showCallBackWarnningDialog({
          errMsg  : "是否删除限定项目,请确认!!!!",
          callbackFn : function(data) {
            if(data.result==true){
              var rowData  = $("#qrsItemListGrd").jqGrid("getRowData",obj.id.substr(3));
              if(rowData.qrs_id==null||rowData.qrs_id==""){
                $("#qrsItemListGrd").jqGrid("delRowData",obj.id.substr(3));
                return false;
              }
              var iary = {
                 mdl_id_fk       :$("#mdlIDSel").find("option:selected").text(),
                 path_id_fk      :$("#pathIDSel").find("option:selected").text(),
                 path_ver_fk     :$("#pathVerSel").find("option:selected").text(),
                 ope_no_fk       :$("#opeNOSel").find("option:selected").text(),
                 ope_id_fk       :$("#opeIDSel").find("option:selected").text(),
                 ope_ver_fk      :$("#opeVerSel").find("option:selected").text(),
                 tool_id_fk      :$("#toolIDSel").find("option:selected").text(),
                 qrs_id          :rowData.qrs_id,
              }
              var inTrxObj = {
                  trx_id     : 'XPBISQRS' ,
                  action_flg : 'D'        ,
                  iary       : iary 
              };
              var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
              if(  outTrxObj.rtn_code == _NORMAL ) {
                  showSuccessDialog("删除限定成功");
                  var mdl_id_fk   =$("#mdlIDSel").find("option:selected").text();
                  var path_id_fk  =$("#pathIDSel").find("option:selected").text();
                  var path_ver_fk =$("#pathVerSel").find("option:selected").text();
                  var ope_no_fk   =$("#opeNOSel").find("option:selected").text();
                  var ope_id_fk   =$("#opeIDSel").find("option:selected").text();
                  var ope_ver_fk  =$("#opeVerSel").find("option:selected").text();
                  var tool_id_fk  =$("#toolIDSel").find("option:selected").text();

                  var inTrxObj ={
                    trx_id : "XPBISQRS",
                    action_flg:"Q",
                    iary   : iary,
                    tbl_cnt: 1
                  };
                  var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
                  if(  outTrxObj.rtn_code == _NORMAL ) {
                    setGridInfo(outTrxObj.oary,"#qrsItemListGrd");
                  }
                  addGridButton();
              }  
            }
          }
      })
      return false ;
  }

  $("#dataListGrd").jqGrid('navGrid','#dataListPg',{ del:false,add:false,edit:false},{},{},{},{multipleSearch:true});
  //页面高度变化时，自动调整Grid大小
  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#qrsListGrd").setGridWidth($("#qrsListDiv").width()*0.95); 
      $("#qrsListGrd").setGridHeight($("#qrsListDiv").height()*0.80);     
      $(window).bind("onresize", this);  
  };
  function initFnc(){
    $("input").attr({'disabled':true});
    $("select").attr({'disabled':true});
    $("input").val("");
    $("select").empty();
    $("#qrsItemForm").css({"margin-top":$("#qrsListDiv").height()-50});
  }
  function showAddQrsItemDialog( qrsItemObj ){
    function qrsItemDialogShowFnc(qrsItemObj){
      $("#qrsIDTxt").val(qrsItemObj.qrs_id );
      // $("#qrsIDTxt").append("<option value='Value'>"+ qrsItemObj.qrs_id +"</option>")
      $("#qrsPathIDSel").empty();
      $("#qrsPathVerSel").empty();
      $("#qrsTimeTxt").val(qrsItemObj.qrs_time);                       
      $("#qrkTimeTxt").val(qrsItemObj.qrk_time);                  
      $("#qrsTypeSel").empty();
      $("#qrsTypeSel").append("<option value=I>"+ "最小" +"</option>");
      $("#qrsTypeSel").append("<option value=M>"+ "最大" +"</option>");
      $("#qrsTypeSel").select2({
	    	theme : "bootstrap"
	    });
      $("#qrsOpeNOSel").empty();
      $("#qrsOpeIDSel").empty();
      $("#qrsOpeVerSel").empty();
      $("#swhPathIDSel ").empty();
      $("#swhPathVerSel").empty();
      $('#qrsItemDialog  input').attr({'disabled':false});
      $('#qrsItemDialog  select').attr({'disabled':false});
      if(qrsItemObj.addItem=="Y"){
        $('#qrsIDTxt').attr({'disabled':false});
        $("#qrsItemDialog_addQrsItemBtn").data("save_flg","A");

      }else{
        $('#qrsIDTxt').attr({'disabled':true});
        $("#qrsItemDialog_addQrsItemBtn").data("save_flg","U");
        $("#qrsPathIDSel").append("<option value="+qrsItemObj.qrs_path_id_fk +">"+ qrsItemObj.qrs_path_id_fk +"</option>")
         $("#qrsPathIDSel").select2({
	    	theme : "bootstrap"
	    });
        $("#qrsPathVerSel").append("<option value="+qrsItemObj.qrs_path_ver_fk +">"+ qrsItemObj.qrs_path_ver_fk +"</option>");
        $("#qrsPathVerSel").select2({
	    	theme : "bootstrap"
	    });
        $("#qrsTypeSel").val(qrsItemObj.qrs_typ);
        $("#qrsOpeNOSel").append("<option value="+qrsItemObj.qrs_ope_no_fk+">"+ qrsItemObj.qrs_ope_no_fk +"</option>");
        $("#qrsOpeNOSel").select2({
	    	theme : "bootstrap"
	    });
        $("#qrsOpeIDSel").append("<option value="+qrsItemObj.qrs_ope_id_fk+">"+ qrsItemObj.qrs_ope_id_fk +"</option>");
        $("#qrsOpeIDSel").select2({
	    	theme : "bootstrap"
	    });
        $("#qrsOpeVerSel").append("<option value="+qrsItemObj.qrs_ope_ver_fk+">"+ qrsItemObj.qrs_ope_ver_fk +"</option>");
        $("#qrsOpeVerSel").select2({
	    	theme : "bootstrap"
	    });
        $("#swhPathIDSel").append("<option value="+qrsItemObj.swh_path_id_fk+">"+ qrsItemObj.swh_path_id_fk +"</option>");  
        $("#swhPathIDSel").select2({
	    	theme : "bootstrap"
	    });
        $("#swhPathVerSel").append("<option value="+qrsItemObj.swh_path_ver_fk+">"+ qrsItemObj.swh_path_ver_fk +"</option>");  
        $("#swhPathVerSel").select2({
	    	theme : "bootstrap"
	    });
      }
    }
    function addQrsItemFnc(){
      var action_flg = $("#qrsItemDialog_addQrsItemBtn").data("save_flg");
      if(action_flg==undefined){
        return false;
      }
      var iary = {
       mdl_id_fk       :$("#mdlIDSel").find("option:selected").text(),
       path_id_fk      :$("#pathIDSel").find("option:selected").text(),
       path_ver_fk     :$("#pathVerSel").find("option:selected").text(),
       ope_no_fk       :$("#opeNOSel").find("option:selected").text(),
       ope_id_fk       :$("#opeIDSel").find("option:selected").text(),
       ope_ver_fk      :$("#opeVerSel").find("option:selected").text(),
       tool_id_fk      :$("#toolIDSel").find("option:selected").text(),
       qrs_id          :$("#qrsIDTxt").val(),
       qrs_time        :$("#qrsTimeTxt").val(),
       qrk_time        :$("#qrkTimeTxt").val(),
       qrs_path_id_fk  :$("#qrsPathIDSel").find("option:selected").text(),
       qrs_path_ver_fk :$("#qrsPathVerSel").find("option:selected").text(),
       qrs_typ         :$("#qrsTypeSel").find("option:selected").text(),
       qrs_ope_no_fk   :$("#qrsOpeNOSel").find("option:selected").text(),
       qrs_ope_id_fk   :$("#qrsOpeIDSel").find("option:selected").text(),
       qrs_ope_ver_fk  :$("#qrsOpeVerSel").find("option:selected").text(),
       swh_path_id_fk  :$("#swhPathIDSel ").find("option:selected").text(),
       swh_path_ver_fk :$("#swhPathVerSel").find("option:selected").text()
      };
      var inTrxObj = {
          trx_id     : 'XPBISQRS' ,
          action_flg : $("#qrsItemDialog_addQrsItemBtn").data("save_flg") ,
          iary       : iary 
      };
      var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(  outTrxObj.rtn_code == _NORMAL ) {
          if(action_flg=="A"){
            showSuccessDialog("新增限定项目成功");  
          }else if(action_flg=="U"){
            showSuccessDialog("修改限定项目成功");  
          }
          $('#qrsItemDialog').modal("hide");
          var mdl_id_fk   =$("#mdlIDSel").find("option:selected").text();
          var path_id_fk  =$("#pathIDSel").find("option:selected").text();
          var path_ver_fk =$("#pathVerSel").find("option:selected").text();
          var ope_no_fk   =$("#opeNOSel").find("option:selected").text();
          var ope_id_fk   =$("#opeIDSel").find("option:selected").text();
          var ope_ver_fk  =$("#opeVerSel").find("option:selected").text();
          var tool_id_fk  =$("#toolIDSel").find("option:selected").text();
          var inTrxObj ={
            trx_id : "XPBISQRS",
            action_flg:"Q",
            iary   : iary,
            tbl_cnt: 1
          };
          var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if(  outTrxObj.rtn_code == _NORMAL ) {
            setGridInfo(outTrxObj.oary,"#qrsItemListGrd");
          }
          addGridButton();
      }  
    }
    $('#qrsItemDialog').modal({
      backdrop:true ,
      keyboard:false,
      show:true
    });
    $('#qrsItemDialog').unbind('shown.bs.modal');
    $("#qrsItemDialog_addQrsItemBtn").unbind('click');
    $('#qrsItemDialog').bind('shown.bs.modal',qrsItemDialogShowFnc(qrsItemObj));
    $("#qrsItemDialog_addQrsItemBtn").bind('click',addQrsItemFnc);
  }
  function f1QueryFnc(){

    /*** 将Div实例化为modal窗体 ***/
    function diaLogQueryFnc(){
      var iary = {};
      if($("#qrsListDialog_mdlIDTxt").val()!=""){
        iary.mdl_id_fk = $("#qrsListDialog_mdlIDTxt").val();
      }
      if($("#qrsListDialog_mdlTypeTxt").val()!=""){
        iary.mdl_typ_fk = $("#qrsListDialog_mdlTypeTxt").val();
      }
      if($("#qrsListDialog_pathIDTxt").val()!=""){
        iary.path_id_fk = $("#qrsListDialog_pathIDTxt").val();
      }
      if($("#qrsListDialog_pathVerTxt").val()!=""){
        iary.path_ver_fk = $("#qrsListDialog_pathVerTxt").val();
      }
      if($("#qrsListDialog_opeNOTxt").val()!=""){
        iary.ope_no_fk = $("#qrsListDialog_opeNOTxt").val();
      }
      if($("#qrsListDialog_opeIDTxt").val()!=""){
        iary.ope_id_fk = $("#qrsListDialog_opeIDTxt").val();
      }
      if($("#qrsListDialog_opeVerTxt").val()!=""){
        iary.ope_ver_fk = $("#qrsListDialog_opeVerTxt").val();
      }
      if($("#qrsListDialog_toolIDTxt").val()!=""){
        iary.tool_id_fk = $("#qrsListDialog_toolIDTxt").val();
      }
      var iary = {
        ope_id  : $("#queryOpeDialog_opeIDTxt").val() ,
        ope_ver : $("#queryOpeDialog_opeVerTxt").val()
      }
      var inTrxObj ={
         trx_id     : "XPBISQRS" ,
         action_flg : 'L'        ,   
         iary       : iary       ,
         tbl_cnt    : 1
      };
      var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(  outTrxObj.rtn_code == _NORMAL ) {
         setGridInfo(outTrxObj.oary,"#qrsListGrd");
         $('#qrsListDialog').modal("hide");
      }
    }
    
    $('#qrsListDialog').modal({
        backdrop:true,
        keyboard:false,
        show:false
    });
    $('#qrsListDialog').unbind('shown.bs.modal');
    $("#qrsListDialog_queryBtn").unbind('click');

    $('#qrsListDialog').bind('shown.bs.modal');
    $('#qrsListDialog').modal("show");
    $("#qrsListDialog_queryBtn").bind('click',diaLogQueryFnc);
    $("#qrsListDialog input").attr({'disabled':false});
  }

  
  initFnc();
  //显示输入提醒
  $('[rel=tooltip]').tooltip({
    placement:"bottom",
    trigger:"focus",
    animation:true,

  });
  function f8AddQrsItemBtn(){
    if(  $("#mdlIDSel").find("option:selected").text()!=""
      && $("#pathIDSel").find("option:selected").text()!=""
      && $("#pathVerSel").find("option:selected").text()!=""
      && $("#opeNOSel").find("option:selected").text()!=""
      && $("#opeIDSel").find("option:selected").text()!=""
      && $("#opeVerSel").find("option:selected").text()!=""
      && $("#toolIDSel").find("option:selected").text()!=""
      ){
        var rowIDs = $("#qrsItemListGrd").jqGrid('getDataIDs');
        var row_id = 1;
        if(rowIDs.length != 0){
          row_id = rowIDs[ rowIDs.length - 1 ] + 1 ;  
        }
        var rowData  = {
          addItem : "Y"
        };
        $("#qrsItemListGrd").jqGrid("addRowData",row_id,rowData);
        addGridButton();
    }else{
        showErrorDialog("003","请在列表中选择QTIME项目");
        return false;
    }
    
  }
  function f4DeleteFnc(){
    var selectID = $("#qrsListGrd").jqGrid("getGridParam", "selrow" );
    if( selectID == null){
      showErrorDialog("003","请选择需要删除的限定项目");
      return false;
    }
    var rowData  = $("#qrsListGrd").jqGrid("getRowData",selectID);
    $("#f1_query_btn").showCallBackWarnningDialog({
          errMsg  : "是否删除限定,请确认!!!!",
          callbackFn : function(data) {
            if(data.result==true){
              
              var iary ={};
              iary.mdl_id_fk =rowData.mdl_id_fk;
              iary.path_id_fk = rowData.path_id_fk;
              iary.path_ver_fk = rowData.path_ver_fk;
              iary.ope_id_fk = rowData.ope_id_fk;
              iary.ope_no_fk = rowData.ope_no_fk;
              iary.ope_ver_fk = rowData.ope_ver_fk;
              iary.tool_id_fk = rowData.tool_id_fk;
              var inTrxObj ={
                trx_id : "XPBISQRS",
                action_flg:"C",
                iary   : iary,
                tbl_cnt: 31
              };
              var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
              if(  outTrxObj.rtn_code == _NORMAL ) {
                showSuccessDialog("删除限定成功");
                $("#qrsConditionForm select").empty();
                $("#qrsItemListGrd").jqGrid("clearGridData"); 
                $("#qrsListGrd").jqGrid('delRowData', selectID);
              }
            }
          }
    })
    
  }
  function f6AddFnc(){
    $("#qrsConditionForm input").attr({'disabled':false});
    $("#qrsConditionForm select").attr({'disabled':false});
    $("#qrsConditionForm input").val("");
    $("#qrsConditionForm select").empty();
    var iary ={};
    var inTrxObj = {
      trx_id     : "XPBMDLDF" ,
      action_flg : "Q"        ,
      iary       : iary
    };
   setSelectObjByinTrx("#mdlIDSel",inTrxObj,"mdl_id");
   QrsCondition.mdlIDSelChangeFnc();
  }
 var QrsCondition = {} ;
  QrsCondition.mdlIDSelChangeFnc = function mdlIDSelChangeFnc(){
    var iary ={
      mdl_id : $("#mdlIDSel").find("option:selected").text()
    };
    var inTrxObj = {
      trx_id     : "XPBMDLDF" ,
      action_flg : "Q"        ,
      iary       : iary
    };
    $("#pathIDSel").empty();
    $("#pathVerSel").empty();
    var outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if( outTrxObj.rtn_code == "0000000" ) {
        var tbl_cnt = comParseInt( outTrxObj.tbl_cnt);
        if( tbl_cnt == 1 ){
          $("#pathIDSel").append("<option value="+ outTrxObj.oary["path_id_fk"] +">"+ outTrxObj.oary["path_id_fk"] +"</option>");         
          $("#pathVerSel").append("<option value="+ outTrxObj.oary["path_ver_fk"] +">"+ outTrxObj.oary["path_ver_fk"] +"</option>");         
        }else if(tbl_cnt>1){
          for(var i=0;i<tbl_cnt;i++){
            $("#pathIDSel").append("<option value="+ outTrxObj.oary[i]["path_id_fk"] +">"+ outTrxObj.oary[i]["path_id_fk"] +"</option>");
            $("#pathVerSel").append("<option value="+ outTrxObj.oary[i]["path_ver_fk"] +">"+ outTrxObj.oary[i]["path_ver_fk"] +"</option>");
          }
        }
        $("#pathIDSel").select2({
	    	theme : "bootstrap"
	    });
      $("#pathVerSel").select2({
	    	theme : "bootstrap"
	    });
    }

    var iaryB = {
      path_id_fk  : $("#pathIDSel").find("option:selected").text()  ,
      path_ver_fk : $("#pathVerSel").find("option:selected").text()
    };
    var inTrxObj1 = {
      trx_id     : "XPBISPTH" ,
      action_flg : "I"        ,
      iaryB       : iaryB
    };
    $("#opeNOSel").empty();
    var outTrxObj1 = comTrxSubSendPostJson(inTrxObj1);
    if( outTrxObj1.rtn_code == "0000000" ) {
        var tbl_cnt_b = comParseInt( outTrxObj1.tbl_cnt_b);
        if( tbl_cnt_b == 1 ){
          $("#opeNOSel").append("<option value="+ outTrxObj1.oaryB["cr_ope_no"] +">"+ outTrxObj1.oaryB["cr_ope_no"] +"</option>")
        }else if(tbl_cnt_b>1){
          for(var i=0;i<tbl_cnt_b;i++){
            $("#opeNOSel").append("<option value="+ outTrxObj1.oaryB[i]["cr_ope_no"] +">"+ outTrxObj1.oaryB[i]["cr_ope_no"] +"</option>");
          }
        }
    }
    $("#opeNOSel").select2({
    	theme : "bootstrap"
    });
    QrsCondition.opeNOSelChangeFnc();
  }
  $("#mdlIDSel").change(function(){
    QrsCondition.mdlIDSelChangeFnc();
  })
  QrsCondition.opeNOSelChangeFnc = function opeNOSelChangeFnc(){
    var iaryB = {
      path_id_fk  : $("#pathIDSel").find("option:selected").text(),
      path_ver_fk : $("#pathVerSel").find("option:selected").text(),
      cr_ope_no   : $("#opeNOSel").find("option:selected").text()
    };
    var inTrxObj = {
      trx_id     : "XPBISPTH" ,
      action_flg : "I"        ,
      iaryB       : iaryB
    };
    $("#opeIDSel").empty();
    $("#opeVerSel").empty();
    var outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if( outTrxObj.rtn_code == "0000000" ) {
      var tbl_cnt_b = comParseInt( outTrxObj.tbl_cnt_b);
        if( tbl_cnt_b == 1 ){
          $("#opeIDSel").append("<option value="+ outTrxObj.oaryB["cr_ope_id_fk"] +">"+ outTrxObj.oaryB["cr_ope_id_fk"] +"</option>")
          $("#opeVerSel").append("<option value="+ outTrxObj.oaryB["cr_ope_ver_fk"] +">"+ outTrxObj.oaryB["cr_ope_ver_fk"] +"</option>")
        }else if(tbl_cnt_b>1){
          for(var i=0;i<tbl_cnt_b;i++){
            $("#opeIDSel").append("<option value="+ outTrxObj.oaryB[i]["cr_ope_id_fk"] +">"+ outTrxObj.oaryB[i]["cr_ope_id_fk"] +"</option>");
            $("#opeVerSel").append("<option value="+ outTrxObj.oaryB[i]["cr_ope_ver_fk"] +">"+ outTrxObj.oaryB[i]["cr_ope_ver_fk"] +"</option>")
          }
        }
        $("#opeIDSel").select2({
        	theme : "bootstrap"
        });
        $("#opeVerSel").select2({
        	theme : "bootstrap"
        });
      QrsCondition.opeIDChangeFnc();  
    }
  }
  $("#opeNOSel").change(function(){
    QrsCondition.opeNOSelChangeFnc();
  })
  QrsCondition.opeIDChangeFnc = function opeIDChangeFnc(){
    var iary,
        inTrxObj,
        outTrxObj;
    if( $("#opeIDSel").val() == "" || $("#opeIDSel").val() == null ){
      return false;
    }    
    iary = {
      ope_id : $("#opeIDSel").val(),
      ope_ver: $("#opeVerSel").val()
    };
    inTrxObj = {
      trx_id     : "XPBISTOL" ,
      action_flg : "Q",
      iary       : iary
    };
    setSelectObjByinTrx("#toolIDSel",inTrxObj,"tool_id");
  }
  $("#opeIDSel").change(QrsCondition.opeIDChangeFnc);
  // function f5UpdateFnc(){
  //   $("qrsConditionForm input").attr({'disabled':false});
  //   $("qrsConditionForm select").attr({'disabled':false});
  //   $("#opeIDTxt").attr({'disabled':true});
  //   $("#opeVerTxt").attr({'disabled':true});
  //   $("#f9_save_btn").data("save_flg","U");
  // }
  function f10ClearFnc(){
    $("input").val("");
    $("select").empty();
    $("#qrsListGrd").jqGrid("clearGridData");
    $("#qrsItemListGrd").jqGrid("clearGridData");
  }
  $("#f1_query_btn").click(f1QueryFnc);
  $("#f6_add_btn").click(f6AddFnc);
  $("#f4_del_btn").click(f4DeleteFnc);
  $("#f8_add_qrsItem_btn").click(f8AddQrsItemBtn);
  $("#f10_clear_btn").click(f10ClearFnc);
})