$(document).ready(function() {
	$("form").submit(function() {
				return false;
			});
	var VAL = {
		T_XPBISBOM : "XPBISBOM",
		T_XPBMDLDF : "XPBMDLDF",
		T_XPBISOPE : "XPBISOPE",
		T_XPBISMTR : "XPBISMTR",
		T_XPLSTDAT : "XPLSTDAT",
		T_XPLSTDAT : "XPLSTDAT",
		T_XPBISPTH : "XPBISPTH",
		T_XPLAYOUT : "XPLAYOUT",
		NORMAL : "0000000",
		DISABLED_ATTR : {
			"disabled" : true
		},
		ENABLED_ATTR : {
			"disabled" : false
		}
	};
	var domObj = {
		$mdlIdTxt : $("#mdlIdTxt"),
		$mdlTypeSel : $("#mdlTypeSel"),
		$layoutIDSel : $("#layoutIDSel"),
		$mdlCateSel : $("#mdlCateSel"),
		$mdlSizeSel : $("#mdlSizeSel"),
		$mdlCodeSel : $("#mdlCodeSel"),
		$mdlDscTxt : $("#mdlDscTxt"),
		$okRadioTxt : $("#okRadioTxt"),
		$fromThicknessTxt : $("#fromThicknessTxt"),
		$toThicknessTxt : $("#toThicknessTxt"),
		$swhTimeTxt : $("#swhTimeTxt"),
		$boxCntTxt : $("#boxCntTxt"),
		$dqrLayoutIDFlgChk : $("#dqrLayoutIDFlgChk"),
		$dqrFromThicknessFlgChk : $("#dqrFromThicknessFlgChk"),
        $paramSel : $("#paramSel"),
		buttons : {
			$f1_query_btn : $("#f1_query_btn"),
			$f4_del_btn : $("#f4_del_btn"),
			$f5_upd_btn : $("#f5_upd_btn"),
			$f6_add_btn : $("#f6_add_btn"),
			$f9_save_btn : $("#f9_save_btn"),
			$f10_clear_btn : $("#f10_clear_btn"),
			$addBomMtrl : $("#addBomMtrl"),
			$updateBomMtrl : $("#updateBomMtrl"),
			$deleteBomMtrl : $("#deleteBomMtrl"),

			$addPathBtn : $("#addPathBtn"),
			$deletePathBtn : $("#deletePathBtn")
		},
		grids : {
			$mdlDefListGrd : $("#mdlDefListGrd"),
			$pathListGrd : $("#pathListGrd"),
			$selectPathDialog_pathListGrd : $("#selectPathDialog_pathListGrd"),
			$bomListGrd : $("#bomListGrd")
		},
		dialogs : {
			$queryMdlDefDialog : $("#queryMdlDefDialog"),
			$queryMdlDefDialog_queryBtn : $("#queryMdlDefDialog_queryBtn"),
			$queryMdlDefDialog_mdlIdTxt : $("#queryMdlDefDialog_mdlIdTxt"),
			$selectPathDialog : $("#selectPathDialog"),
			$selectPathDialog_selectPathBtn : $("#selectPathDialog_selectPathBtn"),
			$bomEditDialog : $("#bomEditDialog"),
			$bomEditDialog_OpeIDSel : $("#bomEditDialog_OpeIDSel"),
			$bomEditDialog_mtrlProdIDSel : $("#bomEditDialog_mtrlProdIDSel"),
			$bomEditDialog_mtrlCatesel : $("#bomEditDialog_mtrlCatesel"),
			$bomEditDialog_mdlIdTxt : $("#bomEditDialog_mdlIdTxt"),
			$bomEditDialog_mdlMtrlSeqIDTxt : $("#bomEditDialog_mdlMtrlSeqIDTxt"),
		}
	};
	var baseFnc = {
		sendXplstData : function(dataCate) {
			var inObj = {
				trx_id : "XPLSTDAT",
				action_flg : "Q",
				iary : {
					data_cate : dataCate
				}
			};
			outObj = comTrxSubSendPostJson(inObj);
			return outObj.rtn_code === VAL.NORMAL ? outObj : false;
		},
		getAddRowID : function($grid) {
			var rowIds = $grid.jqGrid("getDataIDs");
			return rowIds.length > 0 ? (rowIds[rowIds.length - 1]) + 1 : "1";
		},
		clear : function() {
			domObj.grids.$bomListGrd.jqGrid("clearGridData");
			domObj.grids.$pathListGrd.jqGrid("clearGridData");
			$("input[type='text']").val("");
			SelectDom.setSelect($("select"), "", "");
		},
		isDisabled : function($selector) {
			return ($selector.attr("disabled") === "disabled" ? true : false);
		}
	};
	var controlsFunc = {
		initMdlTypSel : function() {
			var outObj = baseFnc.sendXplstData("MDTP");

			SelectDom.initWithSpace(domObj.$mdlTypeSel);
			SelectDom.addSelect(domObj.$mdlTypeSel, "DQR");
			if (outObj) {
				SelectDom.addSelectArr(domObj.$mdlTypeSel, outObj.oary,
						"data_ext");
			}

		},
		initLayoutSel : function() {
			var inObj, outObj,tblCnt,i,oary,val,txt;
			SelectDom.initWithSpace(domObj.$layoutIDSel);

			inObj = {
				trx_id : VAL.T_XPLAYOUT,
				action_flg : 'L'
			};
			outObj = comTrxSubSendPostJson(inObj);
			tblCnt = outObj.tbl_cnt ;
			if (outObj.rtn_code === VAL.NORMAL && tblCnt > 0) {
				for(i=0;i<tblCnt;i++){
					oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
					txt = oary.layot_id+ " : "+ oary.layot_dsc ;
					val = oary.layot_id;
					SelectDom.addSelect(domObj.$layoutIDSel,val,txt);
				}
			}
		},
		initMdlCateSel : function() {
			var outObj = baseFnc.sendXplstData("MDCT");
			SelectDom.initWithSpace(domObj.$mdlCateSel);
			if (outObj) {
				SelectDom.addSelectArr(domObj.$mdlCateSel, outObj.oary,
						"data_ext","data_desc");
			}
		},
		initMdlSizeSel : function() {
			var outObj = baseFnc.sendXplstData("MDSZ");
			SelectDom.initWithSpace(domObj.$mdlSizeSel);
			if (outObj) {
				SelectDom.addSelectArr(domObj.$mdlSizeSel, outObj.oary,
						"data_ext");
			}
		},
		initMdlCodeSel : function() {
			var outObj = baseFnc.sendXplstData("MDCD");
			SelectDom.initWithSpace(domObj.$mdlCodeSel);
			if (outObj) {
				SelectDom.addSelectArr(domObj.$mdlCodeSel, outObj.oary,
						"data_ext");
			}
		},
		initParamSel : function() {
				addValueByDataCateUniqueFnc(domObj.$paramSel, "PRDM", "data_id", true);
		},
		mdlGridSelRowFnc : function(rowId) {
			var rowData, mdlId, iary, inObj, outObj, oary, i, tblCnt, oaryArr;
			$("input").attr(VAL.DISABLED_ATTR);
			$("select").attr(VAL.DISABLED_ATTR);

			rowData = domObj.grids.$mdlDefListGrd.jqGrid("getRowData", rowId);
			mdl_id = rowData.mdl_id;

			inObj = {
				trx_id : VAL.T_XPBMDLDF,
				action_flg : "Q",
				iary : {
					mdl_id : mdl_id
				}
			};
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code == VAL.NORMAL) {
				oary = outObj.oary;
				if (!oary) {
					return false;
				}

				domObj.$mdlIdTxt.val(oary.mdl_id);

				SelectDom.setSelect(domObj.$mdlTypeSel, oary.mdl_typ);
				SelectDom.setSelect(domObj.$layoutIDSel, oary.layot_id_fk);
				SelectDom.setSelect(domObj.$mdlCateSel, oary.mdl_cate);
				SelectDom.setSelect(domObj.$mdlSizeSel, oary.mdl_size);
				SelectDom.setSelect(domObj.$mdlCodeSel, oary.mdl_code);
				SelectDom.setSelect(domObj.$paramSel, oary.mdl_param);

				domObj.$mdlDscTxt.val(oary.mdl_dsc);
				domObj.$okRadioTxt.val(oary.ok_ratio);
				domObj.$fromThicknessTxt.val(oary.from_thickness);
				domObj.$toThicknessTxt.val(oary.to_thickness);
				domObj.$boxCntTxt.val(oary.box_cnt);
				CheckBoxDom.setCheckBox(domObj.$dqrLayoutIDFlgChk,
						oary.dqr_layout_id_flg === "Y" ? true : false);
				CheckBoxDom.setCheckBox(domObj.$dqrFromThicknessFlgChk,
						oary.dqr_from_thickness_flg === "Y" ? true : false);

				inObj = {
					trx_id : VAL.T_XPBISBOM,
					action_flg : "Q",
					iary : {
						mdl_id_fk : mdl_id
					}
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code === VAL.NORMAL) {
					setGridInfo(outObj.oary, "#bomListGrd");
				}

				inObj = {
					trx_id : VAL.T_XPLSTDAT,
					action_flg : "Q",
					iary : {
						data_cate : "MDPH",
						ext_1 : mdl_id
					}
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code === VAL.NORMAL) {
					tblCnt = outObj.tbl_cnt;
					oaryArr = [];
					for (i = 0; i < tblCnt; i++) {
						oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
						oaryArr.push({
									mdl_id : oary.ext_1,
									path_id : oary.ext_2,
									path_ver : oary.ext_3,
									data_seq_id : oary.data_seq_id
								});
					}
					setGridInfo(oaryArr, domObj.grids.$pathListGrd);
				}
			}
		},
		initMdlGirdInfo : function() {
			var itemInfoCM = [{
						name : 'mdl_id',
						index : 'mdl_id',
						label : MDL_ID_TAG,
						width : 100
					}, {
						name : 'mdl_typ',
						index : 'mdl_typ',
						label : "产品规格",
						width : 100
					}];
			domObj.grids.$mdlDefListGrd.jqGrid({
						url : "",
						datatype : "local",
						mtype : "POST",
						height : $("#mdlDefListDiv").height() * 0.95,
						width : $("#mdlDefListDiv").width(),
						autowidth : false,
						shrinkToFit : false,
						scroll : false,
						resizable : true,
						loadonce : true,
						fixed : true,
						viewrecords : true,
						pager : '#mdlDefListPg',
						rownumbers : true,
						rowNum : 15,
						rownumWidth : 20,
						emptyrecords : true,
						toolbar : [true, "top"],
						colModel : itemInfoCM,
						gridComplete : function() {
							var gridPager, pageLen;
							gridPager = $(this).jqGrid("getGridParam", "pager");
							if (gridPager.length < 2) {
								return false;
							}
							$("#sp_1_" + gridPager.substr(1, pageLen - 1))
									.hide();
							$(".ui-pg-input").hide();
							$('td[dir="ltr"]').hide();
							$(gridPager + "_left").hide();
							$(gridPager + "_center").css({
										width : 0
									});
						},
						onSelectRow : function(id) {
							controlsFunc.mdlGridSelRowFnc(id);
						}
					})
		},
		initBomGridInfo : function() {
			var bomItemInfoCM = [{
						name : 'mdl_id_fk',
						index : 'mdl_id_fk',
						label : MDL_ID_TAG,
						width : 80,
						hidden : true
					}, {
						name : 'mdl_mtrl_seq_id',
						index : 'mdl_mtrl_seq_id',
						label : "SEQ_ID",
						width : 10,
						hidden : true
					}, {
						name : 'mtrl_prod_id_fk',
						index : 'mtrl_prod_id_fk',
						label : MTRL_PRODUCT_ID_TAG,
						width : 80
					}, {
						name : 'mtrl_cate',
						index : 'mtrl_cate',
						label : MTRL_CATE_TAG,
						width : 60
					}, {
						name : 'ope_id_fk',
						index : 'ope_id_fk',
						label : OPE_ID_TAG,
						width : 60
					}, {
						name : 'mtrl_mkr',
						index : 'mtrl_mkr',
						label : MTRL_MKR_TAG,
						width : 80
					}, {
						name : 'mtrl_prod_dsc',
						index : 'mtrl_prod_dsc',
						label : MTRL_PROD_DSC_TAG,
						width : 160
					}];

			domObj.grids.$bomListGrd.jqGrid({
						url : "",
						datatype : "local",
						mtype : "POST",
						height : 350,
						width : 200,
//						width : $("#bomListDiv").width()*0.98,
						autowidth : true,
//						shrinkToFit : false,
//						scroll : false,
//						resizable : true,
						loadonce : true,
//						fixed : true,
						viewrecords : true,
						pager : '#bomListPg',
						rownumbers : true,
						rowNum : 15,
//						rownumWidth : 20,
						emptyrecords : true,
						colModel : bomItemInfoCM,
						gridComplete : function() {
						}
					})
		},
		initPathListGrd : function() {
			domObj.grids.$pathListGrd.jqGrid({
						url : "",
						datatype : "local",
						mtype : "POST",
						width : 350,
						height : 85,
						width : $("#pathListDiv").width(),
						loadonce : true,
						viewrecords : true,
						pager : '#bomListPg',
						rownumbers : true,
						rowNum : 15,
						rownumWidth : 20,
						colModel : [{
									name : 'path_id',
									index : 'path_id',
									label : PATH_ID_TAG,
									width : 100
								}, {
									name : 'path_ver',
									index : 'path_ver',
									label : PATH_VER_TAG,
									width : 100
								}, {
									name : 'data_seq_id',
									index : 'data_seq_id',
									label : "主键",
									width : 100,
									hidden : true
								}],
						gridComplete : function() {
							var gridPager, pageLen;
							gridPager = $(this).jqGrid("getGridParam", "pager");
							if (gridPager.length < 2) {
								return false;
							}
							$("#sp_1_" + gridPager.substr(1, pageLen - 1))
									.hide();
							$(".ui-pg-input").hide();
							$('td[dir="ltr"]').hide();
							$(gridPager + "_left").hide();
							$(gridPager + "_center").css({
										width : 0
									});
							$(".ui-pg-button").remove();
						}
					});
		},
		initSelectPathDialogPathListGrd : function() {
			domObj.grids.$selectPathDialog_pathListGrd.jqGrid({
						url : "",
						datatype : "local",
						mtype : "POST",
						width : 360,
						height : 450,
						shrinkToFit : true,
						scroll : false,
						resizable : true,
						loadonce : true,
						fixed : true,
						viewrecords : true,
						rownumbers : true,
						rowNum : 20,
						rownumWidth : 20,
						emptyrecords : true,
						colModel : [{
									name : 'path_id',
									index : 'path_id',
									label : '工艺路线',
									width : 120
								}, {
									name : 'path_ver',
									index : 'path_ver',
									label : "工艺路线版本",
									width : 120
								}],
						ondblClickRow : function(id) {
						}
					});
		}
	};
	var dialogFnc = {
		queryDialog : {
			queryFnc : function() {
				var inObj, outObj, mdlId, iary = {};
				mdlId = domObj.dialogs.$queryMdlDefDialog_mdlIdTxt.val();
				if (mdlId) {
					iary.mdl_id = mdlId;
				}
				inObj = {
					trx_id : "XPBMDLDF",
					action_flg : 'Q',
					iary : iary
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code === VAL.NORMAL) {
					setGridInfo(outObj.oary, "#mdlDefListGrd");
					domObj.dialogs.$queryMdlDefDialog.modal("hide");
				}
			}
		},
		bomDialog : {
			bomEditDialogMtrlCateSelFnc : function() {
				var iary, inObj, outObj;
				iary = {
					mtrl_cate : $("#bomEditDialog_mtrlCatesel").val()
				};
				inObj = {
					trx_id : 'XPBISMTR',
					action_flg : 'Q',
					iary : iary
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code === VAL.NORMAL && outObj.tbl_cnt > 0) {
					$("#bomEditDialog_mtrlProdIDSel").empty();
					SelectDom.addSelectArr($("#bomEditDialog_mtrlProdIDSel"),
							outObj.oary, "mtrl_prod_id")
				}
			},

			addBom : function() {
				dialogFnc.bomDialog.showBomDialog("A");
			},
			updateBom : function() {
				var selRowId, rowData;
				selRowId = domObj.grids.$bomListGrd.jqGrid("getGridParam",
						"selrow");
				if (!selRowId) {
					showErrorDialog("", "请选择需要修改的物料信息");
					return false;
				}
				rowData = domObj.grids.$bomListGrd.jqGrid("getRowData",
						selRowId);
				dialogFnc.bomDialog.showBomDialog("U", rowData);
			},
			showBomDialog : function(actionFlg, bomRowData) {
				var inObj, outObj, tblCnt, mdlId;
				mdlId = domObj.$mdlIdTxt.val().trim();
				if (!mdlId) {
					showErrorDialog("", "生产型号为空,不能添加物料");
					return false;
				}
				if (!baseFnc.isDisabled(domObj.$mdlIdTxt)) {
					showErrorDialog("", "新增物料信息前,请先保存产品信息");
					return false;
				}
				domObj.dialogs.$bomEditDialog.modal({
							backdrop : true,
							keyboard : false,
							show : false
						});
				$("#bomEditDialog_saveBtn").unbind('click');
				$("#bomEditDialog_mtrlCatesel").unbind('change');
				$("#bomEditDialog_saveBtn").bind('click', this.saveBomMtrlFnc);
				$("#bomEditDialog_mtrlCatesel").bind('change',
						this.bomEditDialogMtrlCateSelFnc);
				$('#bomEditDialog').modal("show");

				$("#bomEditDialogForm").attr(VAL.ENABLED_ATTR);
				$("#bomEditDialogForm select").attr(VAL.ENABLED_ATTR);
				domObj.dialogs.$bomEditDialog_mdlIdTxt.val(domObj.$mdlIdTxt
						.val());

				inObj = {
					trx_id : 'XPBISOPE',
					action_flg : 'L'
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code == VAL.NORMAL) {
					domObj.dialogs.$bomEditDialog_OpeIDSel.empty();
					SelectDom.addSelectArr(
							domObj.dialogs.$bomEditDialog_OpeIDSel,
							outObj.oary, "ope_id");
				}
				outObj = baseFnc.sendXplstData("MTCT");
				if (outObj) {
					SelectDom
							.initWithSpace(domObj.dialogs.$bomEditDialog_mtrlCatesel);
					SelectDom.addSelectArr(
							domObj.dialogs.$bomEditDialog_mtrlCatesel,
							outObj.oary, "data_ext");
				}
				// inObj = {
				// trx_id : 'XPBISMTR',
				// action_flg : 'Q',
				// iary : {
				// mtrl_cate : domObj.dialogs.$bomEditDialog_mtrlCatesel
				// .val()
				// }
				// };
				// outObj = comTrxSubSendPostJson(inObj);
				// if (outObj.rtn_code == VAL.NORMAL) {
				// domObj.dialogs.$bomEditDialog_mtrlProdIDSel.empty();
				// SelectDom.addSelectArr(
				// domObj.dialogs.$bomEditDialog_mtrlProdIDSel,
				// outObj.oary, "mtrl_prod_id");
				// }

				if (actionFlg === "A") {
					domObj.dialogs.$bomEditDialog_mdlIdTxt
							.attr(VAL.ENABLED_ATTR);
				} else {
					domObj.dialogs.$bomEditDialog_mtrlCatesel
							.attr(VAL.DISABLED_ATTR);
					domObj.dialogs.$bomEditDialog_mdlIdTxt
							.attr(VAL.DISABLED_ATTR);
					domObj.dialogs.$bomEditDialog_mtrlProdIDSel
							.attr(VAL.DISABLED_ATTR);
					domObj.dialogs.$bomEditDialog_mdlMtrlSeqIDTxt
							.val(bomRowData.mdl_mtrl_seq_id);
					SelectDom.setSelect(
							domObj.dialogs.$bomEditDialog_mtrlCatesel,
							bomRowData.mtrl_cate);
					this.bomEditDialogMtrlCateSelFnc();
					SelectDom.setSelect(
							domObj.dialogs.$bomEditDialog_mtrlProdIDSel,
							bomRowData.mtrl_prod_id_fk);
					SelectDom.setSelect(domObj.dialogs.$bomEditDialog_OpeIDSel,
							bomRowData.ope_id_fk);
				}
			},
			saveBomMtrlFnc : function() {
				var actionFlg, iary, inObj, outObj, newRowId, mdlId;

				actionFlg = domObj.dialogs.$bomEditDialog_mdlIdTxt
						.attr("disabled") === "disabled" ? "U" : "A";
				mdlId = domObj.dialogs.$bomEditDialog_mdlIdTxt.val().trim();
				var mtrlpro=domObj.dialogs.$bomEditDialog_mtrlProdIDSel.find("option:selected").text();
				iary = {
					mdl_id_fk : mdlId,
					mtrl_prod_id_fk : domObj.dialogs.$bomEditDialog_mtrlProdIDSel
							.find("option:selected").text(),
					mdl_mtrl_seq_id : domObj.dialogs.$bomEditDialog_mdlMtrlSeqIDTxt
							.val(),
					ope_id_fk : domObj.dialogs.$bomEditDialog_OpeIDSel.val(),
					mtrl_unit_fk : "",
					mtrl_use_qty : "",
					cns_flg : "Y",
					mnt_flg : "Y"
				};
			    var ids=domObj.grids.$bomListGrd.jqGrid('getDataIDs');
			    for(var z=0; z<ids.length; z++){
			    	var rowDatas = domObj.grids.$bomListGrd.jqGrid("getRowData",ids[z]);
			        if(rowDatas.mtrl_prod_id_fk==mtrlpro){
						showErrorDialog("", "选择物料重复，请检查！");
						return false;
			        }   
			        
			    } 
				inObj = {
					trx_id : VAL.T_XPBISBOM,
					action_flg : actionFlg,
					iary : iary,
					tbl_cnt : 1
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code == VAL.NORMAL) {
					// getBisBomFnc($("#bomEditDialog_mdlIdTxt").val());
					newRowId = baseFnc.getAddRowID(domObj.grids.$bomListGrd);
					if (actionFlg == "A") {
						inObj.action_flg = "Q";
						iary = {
							mdl_id_fk : mdlId
						};
						outObj = comTrxSubSendPostJson(inObj, "N");
						if (outObj.rtn_code === VAL.NORMAL) {
							setGridInfo(outObj.oary, domObj.grids.$bomListGrd);
							showSuccessDialog("BOM信息新增成功");
						}

					} else if (actionFlg == "U") {
						domObj.grids.$bomListGrd.jqGrid("setRowData", iary);
						showSuccessDialog("BOM信息更新成功");
					}
					domObj.dialogs.$bomEditDialog.modal("hide");

				}
			},
			deleteBomMtrlFnc : function() {
				var selRowId, rowData, inObj, outObj;
				selRowId = domObj.grids.$bomListGrd.jqGrid("getGridParam",
						"selrow");
				if (!selRowId) {
					showErrorDialog("", "请选择需要删除的物料");
					return false;
				}
				domObj.buttons.$deletePathBtn.showCallBackWarnningDialog({
							errMsg : "是否删除物料信息,请确认!!!!",
							callbackFn : function(data) {
								if (data.result == true) {
									rowData = domObj.grids.$bomListGrd.jqGrid(
											"getRowData", selRowId);
									inObj = {
										trx_id : VAL.T_XPBISBOM,
										action_flg : "D",
										iary : {
											mdl_id_fk : rowData.mdl_id_fk,
											mtrl_prod_id_fk : rowData.mtrl_prod_id_fk,
											mdl_mtrl_seq_id : rowData.mdl_mtrl_seq_id
										}
									};
									outObj = comTrxSubSendPostJson(inObj);
									if (outObj.rtn_code === VAL.NORMAL) {
										showSuccessDialog("删除物料成功");
										domObj.grids.$bomListGrd.jqGrid(
												"delRowData", selRowId);
									}
								}
							}
						})

			}
		},
		pathDialog : {
			initPathGridData : function() {
				var inObj, outObj;
				inObj = {
					trx_id : "XPBISPTH",
					action_flg : "Q",
					iaryA : {
						path_cate : "MAIN"
					}
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code === VAL.NORMAL) {
					setGridInfo(outObj.oaryA, "#selectPathDialog_pathListGrd");
				}
			},
			isMdlBindPath : function(pathId, pathVer) {
				var rowIds, rowData, i;
				rowIds = domObj.grids.$pathListGrd.jqGrid("getDataIDs");
				for (i = 0; i < rowIds.length; i++) {
					rowData = domObj.grids.$pathListGrd.jqGrid("getRowData",
							rowIds[i]);
					if (rowData.path_id === pathId
							&& rowData.path_ver === pathVer) {
						return true;
					}
				}
				return false;
			},
			showSelectPathDialog : function() {
				domObj.dialogs.$selectPathDialog.modal({
							backdrop : true,
							keyboard : false,
							show : false
						});
				domObj.dialogs.$selectPathDialog.unbind('shown.bs.modal');
				domObj.dialogs.$selectPathDialog_selectPathBtn.unbind('click');

				domObj.dialogs.$selectPathDialog.modal("show");
				domObj.dialogs.$selectPathDialog_selectPathBtn.bind('click',
						dialogFnc.pathDialog.selectPathFnc);
				dialogFnc.pathDialog.initPathGridData();
			},
			selectPathFnc : function() {
				var selRowId, rowData, newRowId, inObj, outObj, iary, mdlId, pathId, pathVer;
				mdlId = domObj.$mdlIdTxt.val();
				if (!mdlId) {
					showErrorDialog("", "请选择产品名称");
					return false;
				}
				selRowId = domObj.grids.$selectPathDialog_pathListGrd.jqGrid(
						"getGridParam", "selrow");
				if (!selRowId) {
					showErrorDialog("", "请选择需要绑定的工艺路线");
					return false;
				}
				rowData = domObj.grids.$selectPathDialog_pathListGrd.jqGrid(
						"getRowData", selRowId);
				pathId = rowData.path_id;
				pathVer = rowData.path_ver;
				if (!pathId || !pathVer) {
					showErrorDialog("", "工艺路线或者工艺路线版本为空,错误");
					return false;
				}
				if (dialogFnc.pathDialog.isMdlBindPath(pathId, pathVer)) {
					showErrorDialog("", "重复的绑定关系,错误");
					return false;
				}
				iary = {
					data_cate : "MDPH",
					ext_1 : mdlId,
					ext_2 : pathId,
					ext_3 : pathVer
				};
				inObj = {
					trx_id : "XPLSTDAT",
					action_flg : "A",
					iary : iary
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code === VAL.NORMAL) {
					newRowId = baseFnc.getAddRowID(domObj.grids.$pathListGrd);
					rowData.data_seq_id = outObj.oary.data_seq_id;
					domObj.grids.$pathListGrd.jqGrid("addRowData", newRowId,
							rowData);
					domObj.dialogs.$selectPathDialog.modal('hide');
				}

			},
			deletePathFnc : function() {
				var inObj, outObj, iary, selRowId, rowData;
				var mdlId, pathId, pathVer;

				mdlId = domObj.$mdlIdTxt.val().trim();
				if (!mdlId) {
					showErrorDialog("", "请选择产品名称");
					return false;
				}
				selRowId = domObj.grids.$pathListGrd.jqGrid("getGridParam",
						"selrow");
				if (!selRowId) {
					showErrorDialog("", "请选择需要删除的绑定关系");
					return false;
				}
				rowData = domObj.grids.$pathListGrd.jqGrid("getRowData",
						selRowId);
				pathId = rowData.path_id;
				pathVer = rowData.path_ver;
				if (!pathId || !pathVer) {
					showErrorDialog("", "工艺路线或工艺路线版本为空,错误");
					return false;
				}
				domObj.buttons.$deletePathBtn.showCallBackWarnningDialog({
							errMsg : "是否删除产品和工艺路线绑定关系,请确认!!!!",
							callbackFn : function(data) {
								if (data.result == true) {
									inObj = {
										trx_id : "XPLSTDAT",
										action_flg : "D",
										iary : {
											data_seq_id : rowData.data_seq_id
										}
									};
									outObj = comTrxSubSendPostJson(inObj);
									if (outObj.rtn_code === VAL.NORMAL) {
										showSuccessDialog("成功删除绑定关系");
										domObj.grids.$pathListGrd.jqGrid(
												"delRowData", selRowId);
									}
								}
							}
						});

			}
		}

	};
	var btnsFnc = {
		f1_query : function() {
			domObj.dialogs.$queryMdlDefDialog.modal({
						backdrop : true,
						keyboard : false,
						show : false
					});
			// domObj.dialogs.$queryMdlDefDialog.unbind('shown.bs.modal');
			domObj.dialogs.$queryMdlDefDialog_queryBtn.unbind('click');

			// domObj.dialogs.$queryMdlDefDialog.bind('shown.bs.modal',
			// mdlDefDialogShowFnc);
			domObj.dialogs.$queryMdlDefDialog.modal("show");
			domObj.dialogs.$queryMdlDefDialog_queryBtn.bind('click',
					dialogFnc.queryDialog.queryFnc);
			domObj.dialogs.$queryMdlDefDialog_mdlIdTxt.attr(VAL.ENABLED_ATTR);
			baseFnc.clear();
		},
		f4_del : function() {
			var selRowId, rowData, iary, inObj, outObj;
			selRowId = domObj.grids.$mdlDefListGrd.jqGrid("getGridParam",
					"selrow");
			if (!selRowId) {
				showErrorDialog("", "请选择需要删除的产品信息");
				return false;
			}
			domObj.buttons.$f4_del_btn.showCallBackWarnningDialog({
						errMsg : "是否删除产品信息,请确认!!!!",
						callbackFn : function(data) {
							if (data.result == true) {
								rowData = domObj.grids.$mdlDefListGrd.jqGrid(
										"getRowData", selRowId);
								iary = {
									mdl_id : rowData.mdl_id
								};
								inObj = {
									trx_id : VAL.T_XPBMDLDF,
									action_flg : "D",
									iary : iary
								};
								outObj = comTrxSubSendPostJson(inObj);
								if (outObj.rtn_code === VAL.NORMAL) {
									showSuccessDialog("删除产品信息成功");
									domObj.grids.$mdlDefListGrd.jqGrid(
											"delRowData", selRowId);
								}
							}
						}
					});
		},
		f6_add : function() {
			baseFnc.clear();
			$("#mdlDefConditionForm input").attr(VAL.ENABLED_ATTR);
			$("#mdlDefConditionForm select").attr(VAL.ENABLED_ATTR);
		},
		f5_upd : function() {
			$("#mdlDefConditionForm input:not(#mdlIdTxt)")
					.attr(VAL.ENABLED_ATTR);
			$("#mdlDefConditionForm select").attr(VAL.ENABLED_ATTR);
		},
		f9_save : function() {
			var actionFlg, inObj, outObj, iary, newRowId;
			var mdlId, mdlTyp, mdlDsc, layoutId, mdlCate, mdlSize, mdlCode, mdlParam, okRatio,mdlParam;
			var fromThickness, toThickness, boxCnt;
			var dqrMdlTypFlg, dqrLayoutIdFlg,dqrLayoutIdFlg1, dqrLayoutIdFlg2,dqrFromThicknessFlg;

			actionFlg = domObj.$mdlIdTxt.attr("disabled") === "disabled"
					? "U"
					: "A";
			mdlId = domObj.$mdlIdTxt.val().trim();
			mdlTyp = domObj.$mdlTypeSel.val();
			mdlDsc = domObj.$mdlDscTxt.val().trim();
			layoutId = domObj.$layoutIDSel.val();
			mdlCate = domObj.$mdlCateSel.val();
			mdlSize = domObj.$mdlSizeSel.val();
			mdlCode = domObj.$mdlCodeSel.val();
			mdlParam = domObj.$paramSel.val();
			okRatio = domObj.$okRadioTxt.val().trim();
			fromThickness = domObj.$fromThicknessTxt.val().trim();
			toThickness = domObj.$toThicknessTxt.val().trim();
			boxCnt = domObj.$boxCntTxt.val().trim();
			dqrMdlTypFlg = (mdlTyp.indexOf("DQR") === -1 ? "N" : "Y");
			dqrLayoutIdFlg1 = (layoutId.indexOf("DQR") === -1 ? "N" : "Y");
			dqrLayoutIdFlg2 = (CheckBoxDom.isChecked(domObj.$dqrLayoutIDFlgChk)
					? "Y"
					: "N");
			dqrLayoutIdFlg = "N";
			if (dqrLayoutIdFlg1 ==="Y"){
				dqrLayoutIdFlg = "Y";
			}
			if (dqrLayoutIdFlg2 ==="Y"){
				dqrLayoutIdFlg = "Y";
			}
			dqrFromThicknessFlg = (CheckBoxDom
					.isChecked(domObj.$dqrFromThicknessFlgChk) ? "Y" : "N");
			if (!mdlId) {
				showErrorDialog("", "产品名称不能为空");
				return false;
			}
			if (!fromThickness) {
				showErrorDialog("", "来料厚度不能为空");
				return false;
			}
			if (!mdlTyp) {
				showErrorDialog("", "产品规格不能为空");
				return false;
			}
			if (!layoutId) {
				showErrorDialog("", "请选择版式信息");
				return false;
			}
			if(!mdlCate){
				showErrorDialog("","请选择产品属性");
				return false;
			}
			if(!mdlSize){
				showErrorDialog("","请选择产品尺寸");
				return false;
			}
			if(!mdlCode){
				showErrorDialog("","请选择产品代码");
				return false;
			}
			if (domObj.grids.$pathListGrd.jqGrid("getDataIDs") <= 0) {
				showErrorDialog("", "产品维护和工艺路线的关系");
				return false;
			}
			iary = {
				mdl_id : mdlId,
				mdl_typ : mdlTyp,
				mdl_dsc : mdlDsc,
				layot_id_fk : layoutId,
				mdl_cate : mdlCate,
				mdl_size : mdlSize,
				mdl_code : mdlCode,
				mdl_param : mdlParam,
				from_thickness : fromThickness,
				dqr_mdl_typ_flg : dqrMdlTypFlg,
				dqr_layout_id_flg : dqrLayoutIdFlg,
				dqr_from_thickness_flg : dqrFromThicknessFlg
			};
			if (boxCnt) {
				iary.box_cnt = boxCnt;
			}
			if (okRatio) {
				iary.ok_ratio = okRatio;
			}
			if (toThickness) {
				iary.to_thickness = toThickness;
			}
			inObj = {
				trx_id : VAL.T_XPBMDLDF,
				action_flg : actionFlg,
				iary : iary
			};
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
				if (actionFlg == "A") {
					showSuccessDialog("新增产品信息成功");
					newRowId = baseFnc.getAddRowID(domObj.grids.$mdlDefListGrd);
					domObj.grids.$mdlDefListGrd.jqGrid("addRowData", newRowId,
							iary);
				} else if (actionFlg == "U") {
					showSuccessDialog("产品信息更新成功");
				}
				$("input").attr(VAL.DISABLED_ATTR);
				$("select").attr(VAL.DISABLED_ATTR);
			}

		},
		f10_clear : function() {
			domObj.grids.$bomListGrd.jqGrid("clearGridData");
			domObj.grids.$pathListGrd.jqGrid("clearGridData");
			$("input[type='text']").val("");
			SelectDom.setSelect($("select"), "", "");
		}

	};
	var pageFnc = {
		initControl : function() {
			controlsFunc.initMdlGirdInfo();
			controlsFunc.initBomGridInfo();
			controlsFunc.initPathListGrd();
			controlsFunc.initSelectPathDialogPathListGrd();
		},
		initData : function() {
			baseFnc.clear();
			controlsFunc.initMdlTypSel();
			controlsFunc.initLayoutSel();
			controlsFunc.initMdlCateSel();
			controlsFunc.initMdlSizeSel();
			controlsFunc.initMdlCodeSel();
			controlsFunc.initParamSel();
			$("#chooseDiv").hide();
			$("#csDiv").hide();
		},
		initEvent : function() {
			domObj.buttons.$f1_query_btn.click(btnsFnc.f1_query);
			domObj.buttons.$f4_del_btn.click(btnsFnc.f4_del);
			domObj.buttons.$f5_upd_btn.click(btnsFnc.f5_upd);
			domObj.buttons.$f6_add_btn.click(btnsFnc.f6_add);
			domObj.buttons.$f9_save_btn.click(btnsFnc.f9_save);
			domObj.buttons.$f10_clear_btn.click(btnsFnc.f10_clear);
			domObj.buttons.$addPathBtn
					.click(dialogFnc.pathDialog.showSelectPathDialog);
			domObj.buttons.$deletePathBtn
					.click(dialogFnc.pathDialog.deletePathFnc)
			domObj.buttons.$addBomMtrl.click(dialogFnc.bomDialog.addBom);
			domObj.buttons.$updateBomMtrl.click(dialogFnc.bomDialog.updateBom);
			domObj.buttons.$deleteBomMtrl
					.click(dialogFnc.bomDialog.deleteBomMtrlFnc);
		}
	};
	/**
	 * 检查DQR权限
	 * @author CMJ
	 */
	domObj.$dqrLayoutIDFlgChk.click(function(){
//		var func_code = "F_6400_LAYOT_DQR";
//		if(checkUserFunc(func_code) == false){
//			if(domObj.$dqrLayoutIDFlgChk.attr("checked")=="checked"){
//				domObj.$dqrLayoutIDFlgChk.attr("checked",false);
//			}else{
//				domObj.$dqrLayoutIDFlgChk.attr("checked",true);
//			}
//			showErrorDialog("","你没有权限选择版式DQR！");
//			return;
//		}
	});
	domObj.$dqrFromThicknessFlgChk.click(function(){
//		var func_code = "F_6400_THCK_DQR";
//		if(checkUserFunc(func_code) == false){
//			if(domObj.$dqrFromThicknessFlgChk.attr("checked")=="checked"){
//				domObj.$dqrFromThicknessFlgChk.attr("checked",false);
//			}else{
//				domObj.$dqrFromThicknessFlgChk.attr("checked",true);
//			}
//			showErrorDialog("","你没有权限选择来料厚度DQR！");
//			return;
//		}
	});
	pageFnc.initControl()
	pageFnc.initData();
	pageFnc.initEvent();
})