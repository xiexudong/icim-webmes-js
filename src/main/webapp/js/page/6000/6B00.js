
/**************************************************************************/
/*                                                                        */
/*  System  Name :  ICIM                                                  */
/*                                                                        */
/*  Description  :  Bis_mtrl Management                                   */
/*                                                                        */
/*  MODIFICATION HISTORY                                                  */
/*    Date     Ver     Name          Description                          */
/* ---------- ----- ----------- ----------------------------------------- */
/* 2013/05/09 N0.00   Lin.Xin      Initial release                        */
/*                                                                        */
/**************************************************************************/

$(document).ready(function() {
    var _NORMAL  = "0000000",
        evt_usr  = $("#userId").text();
    $("form").submit(function(){
        return false;
    });



    $("#hxEmptyBoxChk")[0].checked = false;
    $("#boxPhyIdGroup").hide();
    $("#mtrlPalletCapacityGroup").hide();

    $("#hxEmptyBoxChk").click(function(){
        if($("#hxEmptyBoxChk")[0].checked == true){
            $("#boxPhyIdGroup").show();
            $("#mtrlPalletCapacityGroup").show();
        }else{
            $("#boxPhyIdGroup").hide();
            $("#mtrlPalletCapacityGroup").hide();
        }
    })

    var itemInfoCM = [
        {name: 'box_id'       , index: 'box_id'       , label: BOX_ID_TAG,       width: 130},
        {name: 'std_qty'      , index: 'std_qty'      , label: STD_QTY_TAG,      width: 80},
        {name: 'box_size'     , index: 'box_size'     , label: BOX_SIZE_TAG,     width: 110},
        {name: 'box_cate'     , index: 'box_cate'     , label: BOX_CATE_TAG,     width: 110},
        {name: 'box_set_code' , index: 'box_set_code' , label: BOX_SET_CODE_TAG, width: 120},
        {name: 'box_mkr'      , index: 'box_mkr'      , label: BOX_MKR_TAG,      width: 100},
        {name: 'box_phy_id'  , index: 'box_phy_id'    , label: "来料箱号",      width: 100},
        {name: 'mtrl_pallet_capacity'  , index: 'mtrl_pallet_capacity'      , label: "来料栈板容量",      width: 100},

    ];
    $("#boxListGrd").jqGrid({
        url:"",
        datatype:"local",
        mtype:"POST",
        height:400,
        // height:400,//TODO:需要根据页面自适应，要相对很高
        // width:"99%",
        autowidth:true,//宽度根据父元素自适应
        shrinkToFit:true,
        scroll:false,
        resizable : true,
        loadonce:true,
        // toppager: true , 翻页控件是否在上层显示
        fixed: true,
        // hidedlg:true,
        jsonReader : {
            // repeatitems: false
        },
        viewrecords : true, //显示总记录数
        pager : '#boxListPg',
        rownumbers  :true ,//显示行号
        rowNum:20,         //每页多少行，用于分页
        rownumWidth : 20,  //行号列宽度
        emptyrecords :true ,
        pginput  : true ,//可以输入跳转页数
        toolbar : [true, "top"],//显示工具列 : top,buttom,both
        colModel: itemInfoCM,
        onSelectRow:function(id){
            var rowData ;
            $("#f1_query_btn").hide();
            $("#f4_del_btn").hide();
            $("#f8_regist_btn").hide();
            $("#f10_clear_btn").hide();
            $("#f9_cancel_update_btn").show();
            $("#f11_save_Changes_Btn").show();
            $("#boxIDTxt").attr({'disabled':true});
            rowData = $(this).jqGrid("getRowData",id);
            $("#boxIDTxt").val(rowData.box_id);
            $("#boxCateSel").val(rowData.box_cate);
            $("#boxSizeSel").val(rowData.box_size);
            $("#boxSetCodeSel").val(rowData.box_set_code);
            $("#boxMkrSel").val(rowData.box_mkr);
            if(rowData.box_phy_id){
                $("#boxPhyIdGroup").show();
                $("#mtrlPalletCapacityGroup").show();
                $("#hxEmptyBoxChk")[0].checked = true ;
                $("#boxPhyIdTxt").val(rowData.box_phy_id);
                $("#mtrlPalletCapacityTxt").val(rowData.mtrl_pallet_capacity);

            }
        }
    })
    $("#boxListGrd").jqGrid('navGrid','#boxListPg',{ del:false,add:false,edit:false},{},{},{},{multipleSearch:true});
    //页面高度变化时，自动调整Grid大小
    var resetJqgrid = function(){
        $(window).unbind("onresize");
        $("#boxListGrd").setGridWidth($("#boxListDiv").width()*0.95);
        $("#boxListGrd").setGridHeight($("#boxListDiv").height()*0.80);
        $(window).bind("onresize", this);
    };
    function initFnc(){
        $("input").attr({'disabled':false});
        $("select").attr({'disabled':false});
        $("input").val("");
        $("select").empty();
        comAddValueByDataCateFnc("#boxSizeSel","BXSZ","data_ext","data_ext");
        comAddValueByDataCateFnc("#boxCateSel","BXCT","data_ext","data_ext");
        comAddValueByDataCateFnc("#boxSetCodeSel","BXSC","data_ext","data_ext");
        comAddValueByDataCateFnc("#boxMkrSel","BXMK","data_ext","data_ext");
    }
    function cancelUpdateFnc(){
        $("#boxIDTxt").attr({'disabled':false});
        $("#f1_query_btn").show();
        $("#f4_del_btn").show();
        $("#f8_regist_btn").show();
        $("#f10_clear_btn").show();
        $("#f9_cancel_update_btn").hide();
        $("#f11_save_Changes_Btn").hide();
    }
    $("#f9_cancel_update_btn").click(cancelUpdateFnc);
    function saveChangesFnc(){
        var inTrx,
            outTrx,
            selectRowID;
        inTrx ={
            trx_id       : "XPMAGBOX",
            action_flg   : "U",
            box_id       : $("#boxIDTxt").val().trim(),
            box_cate     : $("#boxCateSel").val(),
            box_size     : $("#boxSizeSel").val(),
            box_set_code : $("#boxSetCodeSel").val(),
            box_mkr      : $("#boxMkrSel").val(),
            evt_usr      : evt_usr
        };
        if($("#hxEmptyBoxChk")[0].checked == true){
            var boxPhyId = 	$("#boxPhyIdTxt").val();
            var mtrlPalletCapacity  = $("#mtrlPalletCapacityTxt").val();
            if(!boxPhyId){
                showErrorDialog("","请输入来料箱号");
                return false;
            }
            if(!mtrlPalletCapacity){
                showErrorDialog("","请输入来料栈板容量");
                return false;
            }
            inTrx.hx_empty_flg = "Y";
            inTrx.box_phy_id = boxPhyId;
            inTrx.mtrl_pallet_capacity = mtrlPalletCapacity;
        }
        outTrx = comTrxSubSendPostJson(inTrx);
        if(outTrx.rtn_code=="0000000"){
            showSuccessDialog("箱子信息修改成功");
            selectRowID = $("#boxListGrd").jqGrid("getGridParam","selrow");
            $("#boxListGrd").jqGrid("setRowData",selectRowID,outTrx.oary2);
            cancelUpdateFnc();
        }
    }
    $("#f11_save_Changes_Btn").click(saveChangesFnc);
    $("#boxIDTxt").keyup(function(e) {
        $(this).val($(this).val().toUpperCase());
        if (e.keyCode == 13) {
            f1QueryFnc();
        }
    })
    function f1QueryFnc(){
        var box_id       = $("#boxIDTxt").val().trim(),
            box_cate     = $("#boxCateSel").val(),
            box_size     = $("#boxSizeSel").val(),
            box_set_code = $("#boxSetCodeSel").val(),
            box_mkr      = $("#boxMkrSel").val(),
            inTrx,
            outTrx;
        inTrx = {
            trx_id       : "XPMAGBOX",
            action_flg   : "L",
        };
        if(box_id!=null&&box_id!=""){
            inTrx.box_id = box_id;
        }else{
            if(box_cate!=null&&box_cate!=""){
                inTrx.box_cate = box_cate;
            }else if(box_size!=null&&box_size!=""){
                inTrx.box_size = box_size;
            }else if(box_set_code!=null&&box_set_code!=""){
                inTrx.box_set_code = box_set_code;
            }else if(box_mkr!=null&&box_mkr!=""){
                inTrx.box_mkr = box_mkr;
            }
        }
        outTrx = comTrxSubSendPostJson(inTrx);
        if(outTrx.rtn_code=="0000000"){
            setGridInfo(outTrx.oary2,"#boxListGrd");
        }

    }

    $("#f1_query_btn").click(f1QueryFnc);
    initFnc();
    //显示输入提醒
    $('[rel=tooltip]').tooltip({
        placement:"bottom",
        trigger:"focus",
        animation:true,

    });
    function f8RegistFnc(){
        var box_id       = $("#boxIDTxt").val().trim(),
            box_cate     = $("#boxCateSel").val(),
            box_size     = $("#boxSizeSel").val(),
            box_set_code = $("#boxSetCodeSel").val(),
            box_mkr      = $("#boxMkrSel").val(),
            inTrx,
            outTrx,
            newRowID;
        if(!box_id){
            showErrorDialog("003","请输入箱号");
            return false;
        }
        if(!box_size){
            showErrorDialog("003","请选择箱子尺寸");
            return false;
        }
        if(!box_cate){
            showErrorDialog("","请选择箱子类型");
            return false;
        }
        if(!box_set_code){
            showErrorDialog("","请选择单箱容量");
            return false;
        }
        if(!box_mkr){
            showErrorDialog("","请选择箱子制造商");
            return false;
        }
        inTrx = {
            trx_id       : "XPMAGBOX",
            action_flg   : "A",
            box_id       : box_id,
            box_cate     : box_cate,
            box_size     : box_size,
            box_set_code : box_set_code,
            box_mkr      : box_mkr ,
            evt_usr      : evt_usr
        };
        if($("#hxEmptyBoxChk")[0].checked == true){
            var boxPhyId = 	$("#boxPhyIdTxt").val();
            var mtrlPalletCapacity  = $("#mtrlPalletCapacityTxt").val();
            if(!boxPhyId){
                showErrorDialog("","请输入来料箱号");
                return false;
            }
            if(!mtrlPalletCapacity){
                showErrorDialog("","请输入来料栈板容量");
                return false;
            }
            inTrx.hx_empty_flg = "Y";
            inTrx.box_phy_id = boxPhyId;
            inTrx.mtrl_pallet_capacity = mtrlPalletCapacity;
        }

        outTrx = comTrxSubSendPostJson(inTrx);
        if(outTrx.rtn_code=="0000000"){
            showSuccessDialog("箱子创建成功");
            newRowID = getGridNewRowID("#boxListGrd");

            $("#boxListGrd").jqGrid("addRowData",newRowID,outTrx.oary2);
        }
    }
    $("#f8_regist_btn").click(f8RegistFnc);
    $("#f4_del_btn").click(function(){
        var box_id = $("#boxIDTxt").val();
        if(box_id==null||box_id==""){
            showErrorDialog("003","请输入箱号");
            return false;
        }
        $("#f4_del_btn").showCallBackWarnningDialog({
            errMsg  : "是否删除箱子信息,请确认!!!!",
            callbackFn : function(data) {
                if(data.result==true){
                    var inTrx,
                        outTrx,
                        selectRowID;
                    inTrx ={
                        trx_id     : "XPMAGBOX",
                        action_flg : "D",
                        box_id     : box_id
                    };
                    outTrx = comTrxSubSendPostJson(inTrx);
                    if(outTrx.rtn_code=="0000000"){
                        showSuccessDialog("箱子删除成功");
                        selectRowID = $("#boxListGrd").jqGrid("getGridParam","selrow");
                        $("#boxListGrd").jqGrid("delRowData",selectRowID);
                        $("input").val("");
                        $("select").empty();
                        cancelUpdateFnc();
                    }

                }
            }
        })
    })

    function comAddValueByDataCateFnc(selectObj,data_cate,valItem,txtItem){
        var selectTxt = $(selectObj).find("option:selected").text();
        $(selectObj).empty();
        $(selectObj).append('<option value=""></option>')
        var iary = {
            data_cate : data_cate
        };
        var inTrxObj = {
            trx_id      : 'XPLSTDAT' ,
            action_flg : 'Q'        ,
            iary        : iary
        };
        var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if(  outTrxObj.rtn_code == "0000000" ) {
            var tbl_cnt = comParseInt( outTrxObj.tbl_cnt);
            if( tbl_cnt == 1 ){
                $(selectObj).append("<option value="+ outTrxObj.oary[valItem] +">"+ outTrxObj.oary[txtItem] +"</option>")
            }else if(tbl_cnt>1){
                for(var i=0;i<tbl_cnt;i++){
                    $(selectObj).append("<option value="+ outTrxObj.oary[i][valItem] +">"+ outTrxObj.oary[i][txtItem] +"</option>");
                }

            }
        }
        $(selectObj).val("");
    }

})