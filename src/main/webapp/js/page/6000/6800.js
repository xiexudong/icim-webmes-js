
  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Tool Management                                       */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/05/17 N0.00   Lin.Xin      Initial release                        */
  /*                                                                        */
  /**************************************************************************/

$(document).ready(function() {
  var VAL = {
	  NORMAL : "0000000"
  };
   $("form").submit(function(){
       return false;
   });
  $("#tabDiv").tab('show');
  var itemInfoCM = [
    {name: 'tool_id'        , index: 'tool_id'     , label: TOOL_ID_TAG     , width: 100 },
    {name: 'tool_dsc'       , index: 'tool_dsc'    , label: TOOL_DSC_TAG    , width: 180 }
  ];

  function getBisBomFnc(mdl_id_fk){
    $("#bomListGrd").jqGrid("clearGridData");
    var iary = {
        mdl_id_fk : mdl_id_fk
    };
    var inTrxObj ={
      trx_id : "XPBISBOM",
      action_flg:"Q",
      iary   : iary,
      tbl_cnt: 1
    };
    var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if(  outTrxObj.rtn_code == VAL.NORMAL ) {
       setGridInfo(outTrxObj.oary,"#bomListGrd");
    }
  }
  
  $("#toolListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:$("#toolListDiv").height(),
      // height:400,//TODO:需要根据页面自适应，要相对很高
      width:$("#toolListDiv").width(),
      autowidth:false,//宽度根据父元素自适应:true
      shrinkToFit:false,
      scroll:false,
      resizable : true,
      loadonce:true,
      fixed: true,
      viewrecords : true, //显示总记录数
      pager : '#toolListPg',
      rownumbers  :true ,//显示行号
      rowNum:20,         //每页多少行，用于分页
      rownumWidth : 20,  //行号列宽度
      emptyrecords :true ,
      colModel: itemInfoCM,
      gridComplete:function(){
          var gridPager,
              pageLen;
          gridPager = $(this).jqGrid("getGridParam","pager");
          if(gridPager.length<2){
             return false;
          }
          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
          $(".ui-pg-input").hide();
          $('td[dir="ltr"]').hide(); 
          $(gridPager+"_left").hide();
          $(gridPager+"_center").css({width:0});
      },
      onSelectRow:function(id){
        $("input").val("");
        $("select").empty();
        $("input").attr({'disabled':true});
        $("select").attr({'disabled':true});
        var rowData = $(this).jqGrid("getRowData",id);
        var tool_id  = rowData.tool_id;
        var iary = {
          tool_id:tool_id
        };
        var inTrxObj ={
          trx_id : "XPBISTOL",
          action_flg:"Q",
          iary   : iary,
          tbl_cnt: 1
        };
        var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if(  outTrxObj.rtn_code == VAL.NORMAL ) {
          var oary = outTrxObj.oary;
          $("#toolIDTxt").val(oary.tool_id);
          if(oary.unit_typ!=undefined){
            $("#UnitTypeSel").append("<option value="+ oary.unit_typ +">"+oary.unit_typ+"</option>") ;
            $("#UnitTypeSel").select2({
		    	theme : "bootstrap"
		    });
          }
          $("#toolDscTxt").val(oary.tool_dsc);
          if(oary.root_tool_id!=undefined){
            $("#rootToolIDSel").append("<option value="+oary.root_tool_id +">"+ oary.root_tool_id +"</option>");  
            $("#rootToolIDSel").select2({
		    	theme : "bootstrap"
		    });
          }
          if(oary.tool_cate!=undefined){
            $("#toolCateSel").append("<option value="+oary.tool_cate+">"+oary.tool_cate+"</option>");  
            $("#toolCateSel").select2({
		    	theme : "bootstrap"
		    });
          }
          if(oary.bay_id!=undefined){
            $("#bayIDSel").append("<option value="+oary.bay_id+">"+oary.bay_id+"</option>");  
            $("#bayIDSel").select2({
		    	theme : "bootstrap"
		    });
          }
          if(oary.floor_code!=undefined){
              $("#floorCodeSel").append("<option value="+oary.floor_code+">"+oary.floor_code+"</option>"); 
              $("#floorCodeSel").select2({
			    	theme : "bootstrap"
			    });
            }
          if(oary.toolg_id!=undefined){
            $("#toolGroupSel").append("<option value="+oary.toolg_id+">"+oary.toolg_id+"</option>");  
            $("#toolGroupSel").select2({
		    	theme : "bootstrap"
		    });
          }
// ?         $("#floorCodeTxt").val(oary.floor_code);
          $("#maxShtCntTxt").val(oary.max_sht_cnt);
          $("#maxShtTimeTxt").val(oary.max_sht_time);
          if(oary.tool_trns_cate!=undefined){
            $("#toolTrnsCateSel").append("<option value="+oary.tool_trns_cate+">"+ oary.tool_trns_cate+"</option>");
            $("#toolTrnsCateSel").select2({
		    	theme : "bootstrap"
		    });
          }
         
          SelectDom.setSelect($("#fabIdSel"),oary.fab_id_fk);
          // getBisBomFnc(oary.mdl_id);
        }
        getpmlist(tool_id);
        getpdalist(tool_id);
      } 
  })

  // $("#toolListGrd").jqGrid('navGrid','#toolListPg',{ del:false,add:false,edit:false},{},{},{},{multipleSearch:true});
  // $("#dataListGrd").jqGrid('navGrid','#dataListPg',{ del:false,add:false,edit:false},{},{},{},{multipleSearch:true});

  var toolPortInfoCM = [
    {name: 'tool_port_id'   , index: 'tool_port_id'    , label: TOOL_PORT_ID_TAG        , width: 120 },
    {name: 'tool_port_type' , index: 'tool_port_type'  , label: TOOL_PORT_TYPE_TAG      , width: 120 }
  ];
  
  $("#toolPortListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:$("#toolPortListDiv").height(),
      height:350,
      width:$("#toolPortListDiv").width(),
      autowidth:true,//宽度根据父元素自适应:true
      shrinkToFit:false,
      scroll:false,
      resizable : true,
      loadonce:true,
      fixed: true,
      viewrecords : true, //显示总记录数
      pager : '#toolPortListPg',
      rownumbers  :true ,//显示行号
      rowNum:23,         //每页多少行，用于分页
      rownumWidth : 40,  //行号列宽度
      emptyrecords :true ,
      pginput  : true ,//可以输入跳转页数
      rowList:[10,15,20], //每页多少行
      toolbar : [true, "top"],//显示工具列 : top,buttom,both
      colModel: toolPortInfoCM,
      gridComplete:function(){

      }
  })
  
    var toolPmInfoCM = [
    {name: 'pm_type'   , index: 'pm_type'    , label: "保养类型"  ,hidden:true       },
    {name: 'pm_type_dsc'   , index: 'pm_type_dsc'    , label: "保养类型"         },
    {name: 'from_date' , index: 'from_date'  , label: "开始时间"     },
    {name: 'to_date' , index: 'to_date'  , label: "结束时间"      },
    {name: 'mes_id_fk' , index: 'mes_id_fk'  , label: "保养列表" ,hidden:true     },
    {name: 'mes_dsc' , index: 'mes_dsc'  , label: "保养列表"      }
  ];
  
  $("#toolPmListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:$("#toolPmListDiv").height(),
      height:350,
      width:$("#toolPmListDiv").width(),
      autowidth:true,//宽度根据父元素自适应:true
      shrinkToFit:false,
      scroll:false,
      resizable : true,
      loadonce:true,
      fixed: true,
      viewrecords : true, //显示总记录数
      pager : '#toolPmListPg',
      rownumbers  :true ,//显示行号
      rowNum:23,         //每页多少行，用于分页
      rownumWidth : 40,  //行号列宽度
      emptyrecords :true ,
      pginput  : true ,//可以输入跳转页数
      rowList:[10,15,20], //每页多少行
      toolbar : [true, "top"],//显示工具列 : top,buttom,both
      colModel: toolPmInfoCM,
      gridComplete:function(){

      }
  })
  
          var grdInfoCM = [
            {name: 'tool_id',        index: 'tool_id',        label: '', width: 1 , hidden: true},
            {name: 'rep_unit_id',    index: 'rep_unit_id',    label: '', width: 1 , hidden: true},
            {name: 'data_pat_id',    index: 'data_pat_id',    label: '', width: 1 , hidden: true},
            {name: 'mes_id',         index: 'mes_id',         label: '', width: 1 , hidden: true},
            {name: 'data_group' ,    index: 'data_group',     label: '', width: 1 , hidden: true},

            {name: 'data_dsc',          index: 'data_dsc',         label: MLITEM_GROUP_TAG      },
            {name: 'data_value',        index: 'data_value',       label: MLITEM_VALUE_TAG     },
            {name: 'spec_chk_u',        index: 'spec_chk_u',       label: U_SPEC_TAG            },
            {name: 'spec_chk_l',        index: 'spec_chk_l',       label: L_SPEC_TAG            },
            {name: 'data_id'  ,         index: 'data_id',          label: TOOL_CHECK_SEQ_NO_TAG }
        ];
        $("#toolCheckInfoGrd").jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:$("#toolCheckInfoDiv").height(),
            height:350,
            width:$("#toolCheckInfoDiv").width(),
            autowidth:true,//宽度根据父元素自适应:true
            shrinkToFit:false,
            scroll:false,
            resizable : true,
            loadonce:true,
            fixed: true,
            viewrecords : true, //显示总记录数
            pager : '#toolCheckInfoPg',
            rownumbers  :true ,//显示行号
            rowNum:23,         //每页多少行，用于分页
            rownumWidth : 40,  //行号列宽度
            emptyrecords :true ,
            pginput  : true ,//可以输入跳转页数
            rowList:[10,15,20], //每页多少行
            toolbar : [true, "top"],//显示工具列 : top,buttom,both
            colModel: grdInfoCM,
            gridComplete:function(){

            }
        });
  //页面高度变化时，自动调整Grid大小
  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#toolListGrd").setGridWidth($("#toolListDiv").width()*0.95); 
      $("#toolListGrd").setGridHeight($("#toolListDiv").height()*0.80); 
      $("#toolPortListGrd").setGridHeight($("#toolPortListPg").height()*0.80); 
      $("#toolPmListGrd").setGridHeight($("#toolPmListPg").height()*0.80); 
      $(window).bind("onresize", this);  
  };
  
  function f1QueryFnc(){

    function diaLogQueryFnc(){
      $("#toolListGrd").jqGrid("clearGridData");
      $("#toolPortListGrd").jqGrid("clearGridData");
      $("#toolPmListGrd").jqGrid("clearGridData");
      var tool_id = $("#queryToolDialog_toolIDTxt").val();

      $("input").val("");
      $("select").empty();
      var iary = {};
      
      if( tool_id != "" ){
        iary.tool_id = tool_id 
      }
      var inTrxObj ={
         trx_id     : "XPBISTOL" ,
         action_flg : 'Q'        ,   
         iary       : iary       ,
         tbl_cnt    : 1
      };
      var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(  outTrxObj.rtn_code == VAL.NORMAL ) {
         setGridInfo(outTrxObj.oary,"#toolListGrd");
         $('#queryToolDialog').modal("hide");
      }
    }
    function queryToolDialogShowFnc(){
       
    }
    $('#queryToolDialog').modal({
        backdrop:true,
        keyboard:false,
        show:false
    });
    $('#queryToolDialog').unbind('shown.bs.modal');
    $("#queryToolDialog_queryBtn").unbind('click');
    $("#queryToolDialog_toolIDTxt").unbind('keyup');

    $('#queryToolDialog').bind('shown.bs.modal',queryToolDialogShowFnc);
    $('#queryToolDialog').modal("show");
    $("#queryToolDialog_queryBtn").bind('click',diaLogQueryFnc);
    $("#queryToolDialog_toolIDTxt").bind('keyup',function(event){
      if(event.keyCode==13){
        diaLogQueryFnc();
      }
    })
    // $("#queryMdlDefDialog_mdlIDTxt")attr({'disabled':true});
    $("#queryToolDialog_toolIDTxt").attr({'disabled':false});
    $("#queryToolDialog_toolIDTxt").val("");
    $("#queryToolDialog_toolIDTxt").focus();
  }

  $("#f1_query_btn").click(f1QueryFnc);
  //显示输入提醒
  $('[rel=tooltip]').tooltip({
    placement:"bottom",
    trigger:"focus",
    animation:true

  });
  function f9SaveFnc(){
    var action_flg = $("#f9_save_btn").data("save_flg");
    
    if(action_flg==undefined){
      return false;
    }

    var iary ={
       tool_id        : $("#toolIDTxt").val().trim(),
       tool_dsc       : $("#toolDscTxt").val().trim(),
       unit_typ       : $("#UnitTypeSel").find("option:selected").text(), 
       root_tool_id   : $("#rootToolIDSel").find("option:selected").text(),
       tool_cate      : $("#toolCateSel").find("option:selected").text(),
       bay_id         : $("#bayIDSel").find("option:selected").text(),
       floor_code     : $("#floorCodeSel").find("option:selected").text(),
       tool_trns_cate : $("#toolTrnsCateSel").find("option:selected").text(),
       tool_ip        : "",
       evt_user       : $("#userId").text(),
       toolg_id       : $("#toolGroupSel").find("option:selected").text(),
       fab_id_fk      : $("#fabIdSel").val()
    };
    var inTrxObj = {
          trx_id     : 'XPBISTOL' ,
          action_flg : action_flg ,
          iary       : iary       ,
          tbl_cnt    : 1
    };
    var outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if(outTrxObj.rtn_code == VAL.NORMAL) {
      if(action_flg=="A"){
        showSuccessDialog("新增设备信息成功");  
        setGridInfo(iary,"#toolListGrd");
      }else if(action_flg=="U"){
        showSuccessDialog("设备信息更新成功");  
      }
      
    }
  };
  $("#f9_save_btn").click(f9SaveFnc);
  function getMainTool(){
    var toolg_id = $("#rootToolIDSel").val();
    var iary={
      tool_cate:"MAIN"
    };
    var inTrxObj = {
          trx_id     : 'XPBISTOL' ,
          action_flg : 'Q' ,
          iary       : iary       ,
          tbl_cnt    : 1
    }
    var outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if(outTrxObj.rtn_code == VAL.NORMAL) {
      var tbl_cnt= comParseInt(outTrxObj.tbl_cnt);
      var oary = outTrxObj.oary;
      $("#rootToolIDSel").empty();
      if(tbl_cnt==1){
         $("#rootToolIDSel").append("<option value="+ oary.tool_id  +">"+ oary.tool_id+"</option>")
      }else{
        for(var i=0;i<tbl_cnt;i++){
         $("#rootToolIDSel").append("<option value="+ oary[i].tool_id  +">"+ oary[i].tool_id+"</option>")
        }
      }
      $("#rootToolIDSel").val(toolg_id).trigger("change");;
    }
  }
  function f6AddFnc(){
    $("input").attr({'disabled':false});
    $("select").attr({'disabled':false});
    $("input").val("");
    $("select").empty();
    addValueByDataCateFnc("#UnitTypeSel","EQTP","data_ext");
    addValueByDataCateFnc("#toolCateSel","EQCT","data_ext");
    addValueByDataCateFnc("#fabIdSel","FBID","data_id");
    $("#UnitTypeSel").val("MAIN").trigger("change");;
    $("#toolCateSel").val("PROC").trigger("change");;
    addValueByDataCateFnc("#bayIDSel","AREA","data_ext");
    addValueByDataCateFnc("#toolTrnsCateSel","TTCT","data_ext");
    addValueByDataCateFnc("#toolGroupSel","TOLG","data_item");
    $("#fabIdSel").change();
    getMainTool();
    $("#f9_save_btn").data("save_flg","A");
  }
  // $("#UnitTypeSel").click(function(){
  //   addValueByDataCateFnc("#UnitTypeSel","EQTP","data_ext");
  // });
  // $("#toolCateSel").click(function(){
  //   addValueByDataCateFnc("#toolCateSel","EQCT","data_ext");
  // });
  // $("#bayIDSel").click(function(){
  //   addValueByDataCateFnc("#bayIDSel","AREA","data_ext");
  // });
  // $("#toolTrnsCateSel").click(function(){
  //   addValueByDataCateFnc("#toolTrnsCateSel","TTCT","data_ext");
  // });
  // $("#rootToolIDSel").click(function(){
  //   getMainTool();
  // })
  function setValueByDataCate($domObj,dataCate,valProp,txtProp){
	  var inObj,outObj,iary,tblCnt,oldVal,oldTxt,i;
	  oldVal = $domObj.val();
	  oldTxt = $domObj.find("option:selected").text();
	  inObj = {
		trx_id     : "XPLSTDAT",
		action_flg : "Q",
		iary       : {data_cate : dataCate}
	  };
	  outObj = comTrxSubSendPostJson(inObj);
	  if(outObj.rtn_code === VAL.NORMAL){
		  $domObj.empty();
		  tblCnt = outObj.tbl_cnt;
		  for(i=0;i<tblCnt;i++){
			  oary = tblCnt >1 ? outObj.oary[i] : outObj.oary;
			  SelectDom.addSelect($domObj,oary[valProp],oary[txtProp]);
		  }
	  }
	  SelectDom.setSelect($domObj,oldVal,oldTxt);
  }
  function setValueByData($domObj,dataCate,dataId,dataExt,valProp,txtProp){
	  var inObj,outObj,iary,tblCnt,i;

	  inObj = {
		trx_id     : "XPLSTDAT",
		action_flg : "Q",
		iary       : {data_cate : dataCate,
			          data_id   : dataId,
			          data_ext  : dataExt}
	  };
	  outObj = comTrxSubSendPostJson(inObj);
	  if(outObj.rtn_code === VAL.NORMAL){
		  tblCnt = outObj.tbl_cnt;
		  for(i=0;i<tblCnt;i++){
			  oary = tblCnt >1 ? outObj.oary[i] : outObj.oary;
			  SelectDom.addSelect($domObj,oary[valProp],oary[txtProp]);
		  }
	  }
  }  
  function f5UpdateFnc(){
    $("input").attr({'disabled':false});
    $("#toolIDTxt").attr({'disabled':true});
    $("select").attr({'disabled':false});
    setValueByDataCate($("#UnitTypeSel"),"EQTP","data_ext");
    setValueByDataCate($("#toolCateSel"),"EQCT","data_ext");
    setValueByDataCate($("#bayIDSel"),"AREA","data_ext");
    setValueByDataCate($("#toolTrnsCateSel"),"TTCT","data_ext"); 
    setValueByDataCate($("#toolGroupSel"),"TOLG","data_item");
    setValueByDataCate($("#fabIdSel"),"FBID","data_id");
    $("#fabIdSel").change();
    getMainTool();
    $("#f9_save_btn").data("save_flg","U");
  }
  function f4DeleteFnc(){
    var selectID = $("#toolListGrd").jqGrid("getGridParam","selrow");
    if( selectID==null){
       showErrorDialog("003","请选择需要删除的设备");
       return false;
    }
    var rowData = $("#toolListGrd").jqGrid("getRowData",selectID);
    var tool_id = rowData.tool_id;
    if( tool_id == ""|| tool_id == undefined ){
       showErrorDialog("003","请选择需要删除的设备");
       return false;
    }
    $("#f1_query_btn").showCallBackWarnningDialog({
      errMsg  : "是否删除设备,请确认!!!!",
      callbackFn : function(data) {
        if(data.result==true){
          var iary = {
            tool_id:tool_id 
          };
          var inTrxObj = {
            trx_id     : 'XPBISTOL' ,
            action_flg : 'D' ,
            iary       : iary       ,
            tbl_cnt    : 1
          }
          var outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if(outTrxObj.rtn_code == VAL.NORMAL) {
            showSuccessDialog("删除设备成功");
            $("#toolListGrd").jqGrid("delRowData",selectID);
            $("#toolPortListGrd").jqGrid("clearGridData");
            $("#toolPmListGrd").jqGrid("clearGridData");
          }
        }
      }
    })
   
  }
  function f10ClearFnc(){
    $("input").val("");
    $("select").empty();
    $("#mdlDefListGrd").jqGrid("clearGridData");
  }

//  function savPmItemFnc(){
//	  var iary =[],inTrx,outTrx;
//	  var selectRowID = $("#toolListGrd").jqGrid("getGridParam","selrow");
//	  var rowData1 = $("#toolListGrd").jqGrid("getRowData",selectRowID)
//	  var tool_id  = rowData1.tool_id;
//	  if(!tool_id){
//		    showErrorDialog("003","请选择设备!");
//	        return false;
//	  }
//      var ids = $("#toolPmListGrd").jqGrid('getDataIDs');
//      var idLen = ids.length;
//      if(idLen==0){
//    	  showErrorDialog("003","请选择要保存的数据!");
//	        return false;
//      }
//      for( i=0; i < idLen; i++){
//    	  var rowData = $("#toolPmListGrd").jqGrid('getRowData', ids[i]);
//          iary.push({
//        	  tool_id_fk : tool_id,
//        	  pm_type    : rowData['pm_type'],
//        	  from_date  : rowData['from_date'],
//        	  to_date    : rowData['to_date'],
//        	  mes_id_fk  : rowData['mes_id_fk'],
//        	  evt_user   : $("#userId").text()
//          });
//      }
//      inTrx={
//		  trx_id : 'XPMLITEM',
//		  action_flg : 'S',
//		  tbl_cnt_a  : idLen,
//		  iaryA : iary
//      }
//      outTrx = comTrxSubSendPostJson(inTrx);
//		if (outTrx.rtn_code == VAL.NORMAL) {
//			showSuccessDialog("新增信息成功！");  
//		}
//  }
  function delPmItemFnc(){
		var selectRowID = $("#toolPmListGrd").jqGrid("getGridParam","selrow");
		var rowData = $("#toolPmListGrd").jqGrid("getRowData",selectRowID)

	    var selectRowIDtool = $("#toolListGrd").jqGrid("getGridParam","selrow");
		var rowDatatool = $("#toolListGrd").jqGrid("getRowData",selectRowIDtool)
		if(selectRowID==null){
			  showErrorDialog("003","请选择要删除的数据!");
		       return false;
		}
	  	  var iary =[],inTrx,outTrx;
		  var tool_id  = rowDatatool.tool_id;
		  if(!tool_id){
			    showErrorDialog("003","请选择设备!");
		        return false;
		  }
	      inTrx={
			  trx_id : 'XPMLITEM',
			  action_flg : 'E',
			  iaryA : {
				  tool_id_fk : tool_id,
	        	  pm_type    : rowData['pm_type'],
			  }
	      }
	      outTrx = comTrxSubSendPostJson(inTrx);
			if (outTrx.rtn_code == VAL.NORMAL) {
				getpmlist(tool_id);
				showSuccessDialog("删除信息成功！");  
			}
//		$("#toolPmListGrd").jqGrid('delRowData', selectRowID);
  }
  function addPmItemFnc(){
      $('#pmItemDialog  input').val("");
	  $("#pmItemDialog  select").attr({'disabled':false});
	  $("#pmItemDialog  input").attr({'disabled':false});
      $('#pmItemDialog  select').empty();
      //iniDateTimePicker();
		var inTrxObjpmcate, outTrxObjpmcate,
		    inTrxObjpmid,outTrxObjpmid;
	
		var selectRowID = $("#toolListGrd").jqGrid("getGridParam","selrow");
		var rowData = $("#toolListGrd").jqGrid("getRowData",selectRowID)
	    var tool_id  = rowData.tool_id;
		if(!tool_id){
		       showErrorDialog("003","请选择设备!");
		       return false;
		}

		inTrxObjpmcate = {
			trx_id : 'XPLSTDAT',
			action_flg : 'Q',
			iary : {
				data_cate : "PMTP",
			}
		};
		outTrxObjpmcate = comTrxSubSendPostJson(inTrxObjpmcate);
		if (outTrxObjpmcate.rtn_code == VAL.NORMAL) {
			_setSelectDate(outTrxObjpmcate.tbl_cnt, outTrxObjpmcate.oary,
					"data_id", "data_ext", "#pmCateSel",
					false);
		}
		
		inTrxObjpmid = {
				trx_id : 'XPMLITEM',
				action_flg : 'B',
				iaryA : {
					tool_id_fk : tool_id
				}
			};
		 outTrxObjpmid = comTrxSubSendPostJson(inTrxObjpmid);
			if (outTrxObjpmid.rtn_code == VAL.NORMAL) {
				_setSelectDate(outTrxObjpmid.tbl_cnt_a, outTrxObjpmid.oaryA,
						"mes_id_fk", "mes_id_fk", "#pmIdSel",
						false);
			}
		$("#pmItemDialog").modal({
            backdrop:true,
            keyboard:false,
            show:false
         });
		$("#pmItemDialog").modal("show");
  
	  }
  $("#pmItemDialog_saveBtn").click(function(){
	  
		var tool_pm_cate = $("#pmCateSel").val();
		//var tool_pm_begin_time =  $("#beginTimeDatepicker").data("datetimepicker").getLocalDate().format('yyyy-MM-dd hh:mm:ss');
		//var tool_pm_end_time = $("#endTimeDatepicker").data("datetimepicker").getLocalDate().format('yyyy-MM-dd hh:mm:ss');
		var tool_pm_begin_time = $("#beginTimeTxt").val().trim();
		var tool_pm_end_time = $("#endTimeTxt").val().trim();
		var tool_pm_id = $("#pmIdSel").val();
        if(!tool_pm_cate){
        	   showErrorDialog("003","请选择保养种类!");
		       return false;
        }
        if(!tool_pm_begin_time){
     	   showErrorDialog("003","请选择开始时间!");
		       return false;
        }
        if(!tool_pm_end_time){
     	   showErrorDialog("003","请选择结束时间!");
		       return false;
        }
        if(!tool_pm_id){
     	   showErrorDialog("003","请选择保养项目!");
		       return false;
        }
        if(tool_pm_end_time<tool_pm_begin_time){
        	 showErrorDialog("003","开始时间不能大于结束时间!");
		       return false;
        }
//        var rowIds = $("#toolPmListGrd").jqGrid('getDataIDs');
//	    var dataRow={
//	    		pm_type       : tool_pm_cate,
//	    		from_date : tool_pm_begin_time,
//	    		to_date   : tool_pm_end_time,
//	    		mes_id_fk         : tool_pm_id
//		}
//			if(rowIds.length==0){
//				$("#toolPmListGrd").jqGrid("addRowData",1,dataRow);
//			}else{
//				for(var i=0;i<rowIds.length;i++){
//					var rowData = $("#toolPmListGrd").jqGrid('getRowData',rowIds[i]);
//					var t1 = rowData['pm_type'];
//					if(t1 == tool_pm_cate){
//					   showErrorDialog("003","同一机台同种保养种类只能设定一笔记录!");
//					       return false;
//					}
//				}
//				if(i==rowIds.length){
//					var rowID = getGridNewRowIDInt("#toolPmListGrd");
//					$("#toolPmListGrd").jqGrid("addRowData",parseInt( rowID ),dataRow);
//				}
//			}
  	  var iary =[],inTrx,outTrx;
		var selectRowID = $("#toolListGrd").jqGrid("getGridParam","selrow");
		var rowData = $("#toolListGrd").jqGrid("getRowData",selectRowID)
	    var tool_id  = rowData.tool_id;
		if(!tool_id){
		       showErrorDialog("003","请选择设备!");
		       return false;
		}
      inTrx={
		  trx_id : 'XPMLITEM',
		  action_flg : 'S',
		  iaryA : {
			  tool_id_fk : tool_id,
        	  pm_type    : tool_pm_cate,
        	  from_date  : tool_pm_begin_time,
        	  to_date    : tool_pm_end_time,
        	  mes_id_fk  : tool_pm_id,
        	  evt_user   : $("#userId").text()
		  }
      }
      outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == VAL.NORMAL) {
			getpmlist(tool_id);
			showSuccessDialog("新增信息成功！");  
		}
	    $("#pmItemDialog").modal("hide");
		
      })
  function getpmlist(tool_id){
	  $("#toolPmListGrd").jqGrid("clearGridData");
      var inTrxObjpm,outTrxObjpm;
      inTrxObjpm = {
				trx_id : 'XPMLITEM',
				action_flg : 'G',
				iaryA : {
					tool_id_fk : tool_id
				}
			};
      outTrxObjpm = comTrxSubSendPostJson(inTrxObjpm);
			if (outTrxObjpm.rtn_code == VAL.NORMAL) {
				if(outTrxObjpm.tbl_cnt_a>0){
				    setGridInfo(outTrxObjpm.oaryA, "#toolPmListGrd");
				}
			}
  }
  function getpdalist(tool_id){
      var inObj,
      outObj;
      inObj = {
         trx_id      : 'XPTOOLCHK' ,
         action_flg  : 'F'             ,
         tool_id     : tool_id
      };
      outObj = comTrxSubSendPostJson(inObj);
      if (outObj.rtn_code == VAL.NORMAL) {
         setGridInfo(outObj.oary,"#toolCheckInfoGrd");
      }
  }
  //Ope select change ==> tool auto refresh
  $("#fabIdSel").change(function(){
      var fab_info;  	   
      fab_info = $.trim($("#fabIdSel").val());
      if(!fab_info){
          return false;
      }
      $("#floorCodeSel").empty();
      setValueByData($("#floorCodeSel"),"LNID","",$("#fabIdSel").val(),"data_item","data_id");

      
//      line_info_ary = fab_info.split("@");        
//      $("#floorCodeSel").append("<option value="+ line_info_ary[0] + "@" + line_info_ary[1] + "@" + line_info_ary[2] +">"+ line_info_ary[3] +"</option>");

  });
	$("#beginTimeTxt, #endTimeTxt").datepicker({
		defaultDate : "",
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1
	});
  $("#f5_upd_btn").click(f5UpdateFnc);
  $("#f6_add_btn").click(f6AddFnc);
  $("#f4_del_btn").click(f4DeleteFnc);
  $("#f10_clear_btn").click(f10ClearFnc);
  

  
  //pm
  $("#toolPm_addBtn").click(addPmItemFnc);
  $("#toolm_delBtn").click(delPmItemFnc);
  //$("#toolPm_savBtn").click(savPmItemFnc);
})