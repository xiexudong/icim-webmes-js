
  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Bis_qrs Management                                   */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/04/28 N0.00   Lin.Xin      Initial release                        */
  /*                                                                        */
  /**************************************************************************/

$(document).ready(function() {
  var _NORMAL = "0000000";
  $("form").submit(function(){
      return false;
  });
  var measureListInfoCM = [
    {name: 'tool_id_fk'  , index: 'tool_id_fk'  , label: TOOL_ID_TAG      , width: 80  },
    {name: 'rep_unit'    , index: 'rep_unit'    , label: REP_UNIT_TAG     , width: 60 },
    {name: 'data_pat'    , index: 'data_pat'    , label: DATA_PAT_TAG     , width: 60 },
    {name: 'mes_id_fk'   , index: 'mes_id_fk'   , label: MES_ID_TAG       , width: 60  }
  ];
  $("#measureListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:($(window).height() - $("#measureListDiv").offset().top) * 0.95,
      // height:400,//TODO:需要根据页面自适应，要相对很高
      width:$("#measureListDiv").width()*0.99,
      // autowidth:true,//宽度根据父元素自适应
      autowidth:false,
      // shrinkToFit:true,
      shrinkToFit:false,
      // scroll:true,
      resizable : true,
      loadonce:true,
      // toppager: true , 翻页控件是否在上层显示
      // fixed: true,
      // hidedlg:true,
      jsonReader : {
            // repeatitems: false
          },
      viewrecords : true, //显示总记录数
      pager : '#measureListPg',
      rownumbers  :true ,//显示行号
      rowNum:20,         //每页多少行，用于分页
      // rownumWidth : 20,  //行号列宽度
      emptyrecords :true ,
      // pginput  : true ,//可以输入跳转页数
      // rowList:[10,15,20], //每页多少行
      // toolbar : [true, "top"],//显示工具列 : top,buttom,both
      colModel: measureListInfoCM,
      gridComplete:function(){
          var gridPager,
              pageLen;
          gridPager = $(this).jqGrid("getGridParam","pager");
          if(gridPager.length<2){
             return false;
          }
          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
          $(".ui-pg-input").hide();
          $('td[dir="ltr"]').hide(); 
          $(gridPager+"_left").hide();
          $(gridPager+"_center").css({width:0});
      },
      onSelectRow:function(id){
        var rowData = $(this).jqGrid("getRowData",id);
        var iaryA ={};
        var iaryB ={};
        if(rowData.tool_id_fk!=""){
          iaryB.tool_id_fk =rowData.tool_id_fk;
          iaryA.tool_id_fk =rowData.tool_id_fk;
        }
        if(rowData.rep_unit!=""){
          iaryB.rep_unit_fk = rowData.rep_unit;
          iaryA.rep_unit    = rowData.rep_unit;
        }
        if(rowData.data_pat!=""){
          iaryB.data_pat_fk = rowData.data_pat;
          iaryA.data_pat    = rowData.data_pat;
        }
        if(rowData.mes_id_fk!=""){
          iaryB.mes_id_fk = rowData.mes_id_fk;
          iaryA.mes_id_fk = rowData.mes_id_fk;
        }
        // if(rowData.tool_id_fk!=""){
        //   iary.tool_id_fk = rowData.tool_id_fk;
        // }
        var inTrxObj ={
          trx_id     : "XPMLITEM",
          action_flg : "Q"       ,
          iaryA      : iaryA     ,
          tbl_cnt_a  : 1         ,
          iaryB      : iaryB     ,
          tbl_cnt_b  : 1
        };
        var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if(  outTrxObj.rtn_code == "0000000" ) {
          $("#measureItemListForm select").empty();
          var oaryA= outTrxObj.oaryA;
          var tbl_cnt_a = outTrxObj.tbl_cnt_a ;
          if( tbl_cnt_a > 0 ){
            setGridInfo(outTrxObj.oaryB,"#measureItemListGrd");
            if(oaryA.tool_id_fk!=undefined){//待优化，写成统一check是否为undeined的函数调用
              $("#toolIDSel").empty();
              $("#toolIDSel").append("<option value="+ oaryA.tool_id_fk +">"+ oaryA.tool_id_fk +"</option>");
              $("#toolIDSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oaryA.rep_unit!=undefined){
              $("#repUnitSel").empty();
              $("#repUnitSel").append("<option value="+ oaryA.rep_unit +">"+ oaryA.rep_unit +"</option>");
              $("#repUnitSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oaryA.data_pat!=undefined){
              $("#dataPatSel").empty();
              $("#dataPatSel").append("<option value="+ oaryA.data_pat +">"+ oaryA.data_pat_txt +"</option>");
              $("#dataPatSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
            if(oaryA.mes_id_fk!=undefined){
              $("#mesIDSel").empty();
              $("#mesIDSel").append("<option value=" + oaryA.mes_id_fk + ">"+ oaryA.mes_id_fk +"</option>");
              $("#mesIDSel").select2({
  		    	theme : "bootstrap"
  		    });
            }
          }
        }
      } 
  })
  //QRS 详细子项目明细
  var measureItemListInfoCM = [
    {name: 'data_group'      , index: 'data_group'     , label: DATA_GROUP_TAG     , width: 90 },
    {name: 'data_dsc'        , index: 'data_dsc'       , label: DATA_DSC_TAG       , width: 120 },
    {name: 'data_group_seq'  , index: 'data_group_seq' , label: DATA_GROUP_SEQ_TAG , width: 70 },
    {name: 'data_attr'       , index: 'data_attr'      , label: DATA_ATTR_TAG      , width: 55 },
    {name: 'spec_chk_flg'    , index: 'spec_chk_flg'   , label: SPEC_CHK_FLG_TAG   , width: 55 },
    {name: 'u_spec'          , index: 'u_spec'         , label: U_SPEC_TAG         , width: 80 },
    {name: 'l_spec'          , index: 'l_spec'         , label: L_SPEC_TAG         , width: 85 },
    {name: 'spc_mode'        , index: 'spc_mode'       , label: SPC_MODE_TAG       , width: 55 },
    {name: 'spec_chk_typ'        , index: 'spec_chk_typ'       , label: "规格标准"       , width: 55 }
  ];
  $("#measureItemListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:($(window).height() - $("#measureItemListDiv").offset().top) * 0.95 - 51,
      // height:400,//TODO:需要根据页面自适应，要相对很高
      // width: "99%", 
       
      // autowidth:true,//宽度根据父元素自适应
      autowidth: false,
      shrinkToFit:false,
      width:$("#measureItemListDiv").width()*0.95,
      // shrinkToFit:true,
      // shrinkToFit:false,
      autoScroll: true, 
      scroll:false,//如需要分页控件,必须将此项置为false
      // resizable : true,
      loadonce:true,
      // toppager: true , 翻页控件是否在上层显示
      // fixed: true,
      // hidedlg:true,
      jsonReader : {
            // repeatitems: false
          },
      viewrecords : true, //显示总记录数
      // pginput  : true ,//可以输入跳转页数
      pager : '#measureItemListPg',
      toolbar:true,
      colModel: measureItemListInfoCM
  });

  //页面高度变化时，自动调整Grid大小
  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#measureListGrd").setGridWidth($("#measureListDiv").width()*0.95); 
      $("#measureItemListGrd").setGridHeight($("#measureListDiv").height()*0.80);  
      $(window).bind("onresize", this);  
  };
  function initFnc(){
    $("input").attr({'disabled':true});
    $("select").attr({'disabled':true});
    $("input").val("");
    $("select").empty();
  }
  initFnc();
  //显示输入提醒
  $('[rel=tooltip]').tooltip({
    placement:"bottom",
    trigger:"focus",
    animation:true

  });

  function showAddmeasureItemDialog(  ){
     if( $("#toolIDSel").find("option:selected").text()==""  ||
          $("#repUnitSel").find("option:selected").text()=="" ||
          $("#dataPatSel").find("option:selected").text()=="" ||
          $("#mesIDSel").find("option:selected").text()==""
        ){
            showErrorDialog("003","设备,上报类别,上报属性,量测名称不能为空");
            return false;
        }
    function measureItemDialogShowFnc(){
      $('#measureItemDialog  input').attr({'disabled':false});
      $('#measureItemDialog  input').val("");
      $('#measureItemDialog  select').attr({'disabled':false});
      $('#measureItemDialog  select').empty();
     // $("#dataDscTxt").attr({'disabled':true});
      $('#specChkFlgChk').removeAttr("checked");
    
      setSelectObjValueTxtByData("#dataGroupSel","MLGP","ext_1","data_ext");
      $("#dataGroupSpn").text($("#dataGroupSel").val());
      $("#dataAttrSel").append('<option value="F">F</option>');
      $("#dataAttrSel").append('<option value="C">C</option>');
      $("#dataAttrSel").append('<option value="B">B</option>');
      $("#dataAttrSel").select2({
	    	theme : "bootstrap"
	    });
      $("#ruleSel").append('<option value="0">绝对值</option>');
      $("#ruleSel").append('<option value="1">来料厚度的相对值</option>');
      $("#ruleSel").append('<option value="2">产出厚度的相对值</option>');
      $("#ruleSel").select2({
	    	theme : "bootstrap"
	    });
      // $("#uSpecTxt").attr({'disabled':true});
      // $("#lSpecTxt").attr({'disabled':true});
      specChkFlgChkClickFnc();
      
    }
    function addmeasureItemFnc(){
      if( $("#toolIDSel").find("option:selected").text()==""  ||
          $("#repUnitSel").find("option:selected").text()=="" ||
          $("#dataPatSel").find("option:selected").text()=="" ||
          $("#mesIDSel").find("option:selected").text()==""
        ){
            showErrorDialog("003","设备,上报类别,上报属性,量测名称不能为空");
            return false;
        }
      var tool_id_fk   = $("#toolIDSel").find("option:selected").text();
      var rep_unit_fk  = $("#repUnitSel").find("option:selected").text();
      // var data_pat_fk  = $("#dataPatSel").find("option:selected").text();
      var data_pat_fk  = $("#dataPatSel").val();
      var mes_id_fk    = $("#mesIDSel").find("option:selected").text();
      // var data_group   = $("#dataGroupTxt").val();
      var data_group   = $("#dataGroupSel").val();
      

      var rowIDs = $("#measureItemListGrd").jqGrid("getDataIDs");
      for(var i=0;i<rowIDs.length;i++){
        var rowData = $("#measureItemListGrd").jqGrid("getRowData",rowIDs[i]);
        if(tool_id_fk   == rowData.tool_id_fk  &&
            rep_unit_fk == rowData.rep_unit_fk &&
            data_pat_fk == rowData.data_pat_fk &&
            mes_id_fk   == rowData.mes_id_fk   &&
            data_group  == rowData.data_group 
          ){
            showErrorDialog("003","此收集项目已经存在,重复的记录");
            return false;
        }
      }
      var l_spec_ini = $("#lSpecTxt").val();
      var u_spec_ini = $("#uSpecTxt").val();
      var l_spec_rel = Math.round(l_spec_ini*1000)/1000;
      var u_spec_rel = Math.round(u_spec_ini*1000)/1000;
      var iaryB = {
         tool_id_fk      :$("#toolIDSel").find("option:selected").text(),
         rep_unit_fk     :$("#repUnitSel").find("option:selected").text(),
         data_pat_fk     :$("#dataPatSel").val(),
         mes_id_fk       :$("#mesIDSel").find("option:selected").text(),
         data_group      :$("#dataGroupSel").val(),
         data_dsc        :$("#dataDscTxt").val(),
         data_group_seq  :$("#dataGroupSeqTxt").val(),
         data_attr       :$("#dataAttrSel").find("option:selected").text(),
         l_spec          :l_spec_rel,
         u_spec          :u_spec_rel,
         spec_chk_typ    :$("#ruleSel").find("option:selected").val()
      };
      if($("#specChkFlgChk").attr("checked")=="checked"){
        iaryB.spec_chk_flg="Y";
      }else{
        iaryB.spec_chk_flg="N";
      }
      if($("#spcModeChk").attr("checked")=="checked"){
        iaryB.spc_mode ="Y";
      }else{
        iaryB.spc_mode ="N";
      }
      var newRowID = getGridNewRowID("#measureItemListGrd");
      $("#measureItemListGrd").jqGrid("addRowData",newRowID,iaryB);
      // $('#measureItemDialog').modal("hide"); 
      showSuccessDialog("新增成功");  
      // $("#dataDscTxt").val("");
      $("#dataGroupSeqTxt").val("");
      $("#lSpecTxt").val("");
      $("#uSpecTxt").val("");
      // $("#measureItemDialog_activeFlgChk").removeAttr("checked");
    }
    function dataGroupChangeFnc(){
        $("#dataGroupSpn").text($("#dataGroupSel").val());
        $("#dataDscTxt").val($("#dataGroupSel").find("option:selected").text());
    }
    function specChkFlgChkClickFnc(){
        if($('#specChkFlgChk').attr("checked")=="checked"){
          $("#uSpecTxt").attr({'disabled':false});
          $("#lSpecTxt").attr({'disabled':false});
        }else{
          $("#uSpecTxt").attr({'disabled':true});
          $("#lSpecTxt").attr({'disabled':true});
        }  
    }
    $('#measureItemDialog').modal({
        backdrop:true,
        keyboard:false,
        show:false
    });
    $('#measureItemDialog').unbind('shown.bs.modal');
    $("#dataGroupSel").unbind('change');
    $("#specChkFlgChk").unbind("click");
    $("#measureMentItemDialog_addMeasureItemBtn").unbind('click');

    $('#measureItemDialog').bind('shown.bs.modal',measureItemDialogShowFnc());
    $('#measureItemDialog').modal("show");
    $("#measureMentItemDialog_addMeasureItemBtn").bind('click',addmeasureItemFnc);
    $("#dataGroupSel").bind( "change", dataGroupChangeFnc );
    $("#specChkFlgChk").bind("click",specChkFlgChkClickFnc);
    dataGroupChangeFnc();
  
  }
  function delmeasureItemFnc(){
    var selectID = $("#measureItemListGrd").jqGrid("getGridParam","selrow");
    if(selectID==null){
      showErrorDialog("003","请选择需要删除的数据收集项目");
      return false;
    }
    $("#measureItemListGrd").jqGrid("delRowData",selectID);
    // $("#del_measureItem_btn").showCallBackWarnningDialog({
    //     errMsg  : "是否删除数据收集项目,请确认!!!!",
    //     callbackFn : function(data) {
    //       if(data.result==true){
    //         $("#measureItemListGrd").jqGrid("delRowData",selectID);
    //         showSuccessDialog("删除数据收集项目成功");
    //       }
    //     }
    // })
  }

  function f1QueryFnc(){

    /*** 将Div实例化为modal窗体 ***/
    function diaLogQueryFnc(){
      var iaryA = {};
      if($("#queryMeasureDialog_ToolIDTxt").val()!=""){
        iaryA.tool_id_fk = $("#queryMeasureDialog_ToolIDTxt").val();
      }
      if($("#queryMeasureDialog_repUnitTxt").val()!=""){
        iaryA.rep_unit = $("#queryMeasureDialog_repUnitTxt").val();
      }
      if($("#queryMeasureDialog_dataPatTxt").val()!=""){
        iary.data_pat = $("#queryMeasureDialog_dataPatTxt").val();
      }
      if($("#queryMeasureDialog_mesIDTxt").val()!=""){
        iary.mes_id_fk = $("#queryMeasureDialog_mesIDTxt").val();
      }
      var inTrxObj ={
         trx_id     : "XPMLITEM" ,
         action_flg : 'Q'        ,   
         iaryA      : iaryA       ,
         tbl_cnt_a  : 1
      };
      var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(  outTrxObj.rtn_code == "0000000" ) {
         setGridInfo(outTrxObj.oaryA,"#measureListGrd");
         setSelectObjValueTxtByData("#dataGroupSel","MLGP","ext_1","data_ext");
         $('#queryMeasureDialog').modal("hide");
      }
    }
    
    $('#queryMeasureDialog').modal({
        backdrop:true,
        keyboard:false,
        show:false
    });
    $('#queryMeasureDialog').unbind('shown.bs.modal');
    $("#queryMeasureDialog_queryBtn").unbind('click');

    $('#queryMeasureDialog').bind('shown.bs.modal');
    $('#queryMeasureDialog').modal("show");
    $("#queryMeasureDialog_queryBtn").bind('click',diaLogQueryFnc);
    $("#queryMeasureDialog input").attr({'disabled':false});
  }
  function queryMLitemFnc(){
    var iaryB = {};

      if($("#toolIDSel").val()){
        iaryB.tool_id_fk = $("#toolIDSel").val();
      }
      if($("#repUnitSel").val()){
        iaryB.rep_unit_fk = $("#repUnitSel").val();
      }
      if($("#dataPatSel").val()){
        iaryB.data_pat_fk = $("#dataPatSel").val();
      }
      if($("#mesIDSel").val()){
        iaryB.mes_id_fk = $("#mesIDSel").val();
      }
     	
	  var inTrxObj ={
	      trx_id     : "XPMLITEM",
	      action_flg : "I"       ,
	      iaryB      : iaryB     ,
	      tbl_cnt_b  : 1
    };
    var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if( outTrxObj.rtn_code == "0000000" ) {
    	  setGridInfo(outTrxObj.oaryB,"#measureItemListGrd");
    }
  }
  $("#toolIDSel").change(function(){
  	queryMLitemFnc();
  })
  $("#repUnitSel").change(function(){
    queryMLitemFnc();
  })
  $("#dataPatSel").change(function(){
    queryMLitemFnc();
  })
  $("#mesIDSel").change(function(){
    queryMLitemFnc();
  })
  function f4DeleteFnc(){
    var selectID = $("#measureListGrd").jqGrid("getGridParam", "selrow" );
    if( selectID == null){
      showErrorDialog("003","请选择需要删除的数据标签");
      return false;
    }
    var rowData  = $("#measureListGrd").jqGrid("getRowData",selectID);
    $("#f4_del_btn").showCallBackWarnningDialog({
          errMsg  : "是否删除数据标签,请确认!!!!",
          callbackFn : function(data) {
            if(data.result==true){
              
              var iaryA ={};
              iaryA.tool_id_fk = rowData.tool_id_fk ;
              iaryA.rep_unit   = rowData.rep_unit   ;
              iaryA.data_pat   = rowData.data_pat   ;
              iaryA.mes_id_fk  = rowData.mes_id_fk  ;
              var inTrxObj ={
                trx_id : "XPMLITEM",
                action_flg:"D",
                iaryA   : iaryA,
                tbl_cnt_a: 1
              };
              var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
              if(  outTrxObj.rtn_code == "0000000" ) {
                showSuccessDialog("删除数据标签成功");
                $("#measureConditionForm select").empty();
                $("#measureListGrd").jqGrid('delRowData', selectID);
                $("#measureItemListGrd").jqGrid("clearGridData");
              }
            }
          }
    })
  }
  function f6AddFnc(){
    $("#measureConditionForm input").attr({'disabled':false});
    $("#measureConditionForm select").attr({'disabled':false});
    $("#measureConditionForm input").val("");
    $("#measureConditionForm select").empty();
    addValueByDataCateFnc("#repUnitSel","MLRU","data_ext");
    // addValueByDataCateFnc("#dataPatSel","MLDP","data_ext");
    setSelectObjValueTxtByData("#dataPatSel","MLDP","data_id","data_ext");
    addValueByDataCateFnc("#mesIDSel"  ,"MLID","data_ext");
    var inTrxObj ={
      trx_id     : "XPBISTOL",
      action_flg : "L"
    };
    setSelectObjByinTrx("#toolIDSel",inTrxObj,"tool_id");
    var inObj = {
    	trx_id      : "XPLSTDAT",
    	action_flg  : "Q",
    	iary        : {
    		data_cate : "TOLG"
    	}
    };
    var outObj = comTrxSubSendPostJson(inObj);
    if(outObj.rtn_code === "0000000"){
    	var tblCnt = outObj.tbl_cnt;
    	var i ,oary;
    	for(i=0;i<tblCnt;i++){
    		oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
    		$("#toolIDSel").append("<option value="+ oary.data_item+">" + oary.data_item + "</option>");
    	}
    }
    
    // queryMLitemFnc();
  }
  function f9SaveFnc(){
    var rowCnt,
        rowIDs,
        iaryBList,
        rowData,
        inTrxObj,
        outTrxObj,
        addRowData,
        measureRowIDs,
        newRowID,
        measureRowData,
        existFlg;


    if( $("#toolIDSel").find("option:selected").text()==""  &&
          $("#repUnitSel").find("option:selected").text()=="" &&
          $("#dataPatSel").find("option:selected").text()=="" &&
          $("#mesIDSel").find("option:selected").text()==""
      ){ 
         showErrorDialog("003","设备代码,上报类别,上报属性,量测名称，都不能为空");
         return false;
       }
    rowCnt = $("#measureItemListGrd").jqGrid("getGridParam","reccount");
    if(rowCnt<=0){
       showErrorDialog("003","数据收集项目不能为空,请添加收集项目");
       return false;
    }
    rowIDs = $("#measureItemListGrd").jqGrid("getDataIDs");
    iaryBList = new Array();
    for(var i=0;i<rowIDs.length;i++){
      rowData = $("#measureItemListGrd").jqGrid("getRowData",rowIDs[i]);
      rowData.tool_id_fk  = $("#toolIDSel").find("option:selected").text();
      rowData.rep_unit_fk = $("#repUnitSel").find("option:selected").text();
      rowData.data_pat_fk = $("#dataPatSel").val();
      rowData.mes_id_fk   = $("#mesIDSel").find("option:selected").text();
      rowData.data_id     = i ;
      rowData.evt_usr     = $("#userId").text();
      rowData.data_seq    = i ;
      iaryBList.push(rowData);
    }
    inTrxObj = {
        trx_id     : 'XPMLITEM' ,
        action_flg : 'N'        ,
        iaryB      : iaryBList  ,
        tbl_cnt_b  : 1
    };
    outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if(  outTrxObj.rtn_code == "0000000" ) {
        showSuccessDialog("新增数据收集项目成功");  
        $("#measureConditionForm input").attr({"disabled":true});
        $("#measureConditionForm select").attr({"disabled":true});
        //当m_label中存在 则不添加
        addRowData = {
          tool_id_fk : $("#toolIDSel").find("option:selected").text()  ,
          rep_unit   : $("#repUnitSel").find("option:selected").text() ,
          data_pat   : $("#dataPatSel").val() ,
          mes_id_fk  : $("#mesIDSel").find("option:selected").text()
        };
        measureRowIDs = $("#measureListGrd").jqGrid("getDataIDs");
        existFlg = false;
        if(measureRowIDs!=null){
          for(var i=0;i<measureRowIDs.length;i++){
             measureRowData = $("#measureListGrd").jqGrid("getRowData",measureRowIDs[i]);
             if( measureRowData.tool_id_fk==addRowData.tool_id_fk &&
                measureRowData.rep_unit==addRowData.rep_unit &&
                measureRowData.data_pat==addRowData.data_pat &&
                measureRowData.mes_id_fk==addRowData.mes_id_fk 
              ){
               existFlg = true;
               return false;
             }
          }  
        }
        newRowID = getGridNewRowID("#measureListGrd");
        $("#measureListGrd").jqGrid("addRowData",newRowID,addRowData)
    }
  }

  function f10ClearFnc(){
    $("input").val("");
    $("select").empty();
    $("#MeasureListGrd").jqGrid("clearGridData");
    $("#measureItemListGrd").jqGrid("clearGridData");
  }
  $("#add_measureItem_btn").click(showAddmeasureItemDialog);
  // $("#upd_measureItem_btn").click(updmeasureItemFnc);
  $("#del_measureItem_btn").click(delmeasureItemFnc);

  $("#f1_query_btn").click(f1QueryFnc);

  $("#f6_add_btn").click(f6AddFnc);
  $("#f4_del_btn").click(f4DeleteFnc);
  $("#f9_save_btn").click(f9SaveFnc)
  $("#f10_clear_btn").click(f10ClearFnc);
})
