/**************************************************************************/
/*                                                                        */
/*  System  Name :  ICIM                                                  */
/*                                                                        */
/*  Description  :  Bis_data Management                                   */
/*                                                                        */
/*  MODIFICATION HISTORY                                                  */
/*    Date     Ver     Name          Description                          */
/* ---------- ----- ----------- ----------------------------------------- */
/* 2013/04/28 NO.00   Lin.Xin      Initial release                        */
/* 2014/03/18 NO.01   Lin.Xin      trim input                             */
/*                                                                        */
/**************************************************************************/

$(document).ready(function () {
    var _NORMAL = "0000000";
    $("form").submit(function () {
        return false;
    });
    $("#tabDiv").tab('show');
    var itemInfoCM = [
        {name: 'layot_id', index: 'layot_id', label: LAYOUT_ID_TAG, width: 80},
        {name: 'layot_dsc', index: 'layot_dsc', label: LAYOUT_DSC_TAG, width: 130}
    ];

    $("#layotListGrd").jqGrid({
        url: "",
        datatype: "local",
        mtype: "POST",
        height: $("#layotListDiv").height(),
        // height:400,//TODO:需要根据页面自适应，要相对很高
        width: $("#layotListDiv").width(),
        autowidth: false,//宽度根据父元素自适应:true
        shrinkToFit: false,
        scroll: false,
        resizable: true,
        loadonce: true,
        // toppager: true , 翻页控件是否在上层显示
        fixed: true,
        // hidedlg:true,
        jsonReader: {
            // repeatitems: false
        },
        viewrecords: true, //显示总记录数
        pager: '#layotListPg',
        rownumbers: true,//显示行号
        rowNum: 20,         //每页多少行，用于分页
        rownumWidth: 20,  //行号列宽度
        emptyrecords: true,
        pginput: true,//可以输入跳转页数
        rowList: [10, 15, 20], //每页多少行
        toolbar: [true, "top"],//显示工具列 : top,buttom,both
        colModel: itemInfoCM,
        gridComplete: function () {
            var gridPager,
                pageLen;
            gridPager = $(this).jqGrid("getGridParam", "pager");
            if (gridPager.length < 2) {
                return false;
            }
            $("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
            $(".ui-pg-input").hide();
            $('td[dir="ltr"]').hide();
            $(gridPager + "_left").hide();
            $(gridPager + "_center").css({width: 0});
        },
        onSelectRow: function (id) {
            $("input").attr({'disabled': true});
            $("select").attr({'disabled': true});
            $("#trunedlayoutDiv").empty();
            $("#layoutDiv").empty();
            var rowData = $(this).jqGrid("getRowData", id);
            var layot_id = rowData.layot_id;
            var iary = {
                layot_id: layot_id
            };
            var inTrxObj = {
                trx_id: "XPLAYOUT",
                action_flg: "Q",
                iary: iary,
                tbl_cnt: 1
            };
            var outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if (outTrxObj.rtn_code == _NORMAL) {
                var oary = outTrxObj.oary;
                $("#layotIDTxt").val(oary.layot_id);
                $("#layotDscTxt").val(oary.layot_dsc);
                $("#layotCateSel").val(oary.layot_cate);
                $("#layotCateSel").select2({
                    theme: "bootstrap"
                });
                $("#layotCateSel").trigger("change");
                // comSetSelectFnc("#layotCateSel",oary.layot_cate);
                $("#xAxisCntTxt").val(oary.x_axis_cnt);
                $("#xAxisLenTxt").val(oary.x_axis_len);
                $("#yAxisCntTxt").val(oary.y_axis_cnt);
                $("#yAxisLenTxt").val(oary.y_axis_len);
            }

        }
    })
    function comSetSelectFnc(selectObj, valueItem, textItem) {
        // $(selectObj).empty();
        if (valueItem != undefined ) {
            $(selectObj).append("<option value=" + valueItem + ">" + textItem + "</option>");
        }
        $(selectObj).select2({
            theme: "bootstrap"
        });
    }

    //页面高度变化时，自动调整Grid大小
    var resetJqgrid = function () {
        $(window).unbind("onresize");
        $("#layotListGrd").setGridWidth($("#dataListDiv").width() * 0.95);
        $("#layotListGrd").setGridHeight($("#dataListDiv").height() * 0.80);
        $(window).bind("onresize", this);
    };

    function initFnc() {
        $("input").attr({'disabled': true});
        $("select").attr({'disabled': true});
        $("input").val("");
        $("select").empty();
        setLayoutCateFnc();
    }


    /***************************************/
    function f1QueryFnc() {

        /*** 将Div实例化为modal窗体 ***/
        function diaLogQueryFnc() {
            var iary = {};
            var layot_id = $("#querylayotDialog_layotIDTxt").val();
            if (layot_id != "") {
                iary.layot_id = layot_id
            }
            var inTrxObj = {
                trx_id: "XPLAYOUT",
                action_flg: 'Q',
                iary: iary,
                tbl_cnt: 1
            };
            var outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if (outTrxObj.rtn_code == _NORMAL) {
                setGridInfo(outTrxObj.oary, "#layotListGrd");
                $('#querylayotDialog').modal("hide");
            }
        }

        function layotDialogShowFnc() {
            $("input").val("");
            $("select").empty();
            $("layotLcListGrd").jqGrid("clearGridData");
            $("#layotListGrd").jqGrid("clearGridData");
            setLayoutCateFnc();
        }

        $('#querylayotDialog').modal({
            backdrop: true,
            keyboard: false,
            show: false
        });
        $('#querylayotDialog').unbind('shown.bs.modal');
        $("#querylayotDialog_queryBtn").unbind('click');

        $('#querylayotDialog').bind('shown.bs.modal', layotDialogShowFnc);
        $('#querylayotDialog').modal("show");
        $("#querylayotDialog_queryBtn").bind('click', diaLogQueryFnc);
        $("#querylayotDialog_layotIDTxt").attr({'disabled': false});
    }

    $("#f1_query_btn").click(f1QueryFnc);
    initFnc();
    //显示输入提醒
    $('[rel=tooltip]').tooltip({
        placement: "bottom",
        trigger: "focus",
        animation: true,

    });
    function f9SaveFnc() {
        var action_flg = $("#f9_save_btn").data("save_flg");
        if (action_flg == undefined) {
            return false;
        }
        var layot_id = $("#layotIDTxt").val().trim();
        var layot_dsc = $("#layotDscTxt").val().trim();
        var layot_cate = $("#layotCateSel").val();
        var x_axis_cnt = $("#xAxisCntTxt").val().trim();
        var x_axis_len = $("#xAxisLenTxt").val().trim();
        var y_axis_cnt = $("#yAxisCntTxt").val().trim();
        var y_axis_len = $("#yAxisLenTxt").val().trim();
        var layot_flg = "S";
        var iary = {
            layot_id: layot_id,
            layot_cate : layot_cate ,
            layot_dsc: layot_dsc,
            layot_flg: layot_flg,
            x_axis_cnt: x_axis_cnt,
            x_axis_len: x_axis_len,
            y_axis_cnt: y_axis_cnt,
            y_axis_len: y_axis_len
        };
        var inTrxObj = {
            trx_id: 'XPLAYOUT',
            action_flg: "A",
            iary: iary,
            tbl_cnt: 1
        };
        var outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if (outTrxObj.rtn_code == "0000000") {
            if (action_flg == "A") {
                showSuccessDialog("新增产品信息成功");
                var newRowID = getGridNewRowID("#layotListGrd");
                $("#layotListGrd").jqGrid("addRowData", newRowID, iary);
            } else if (action_flg == "U") {
                showSuccessDialog("产品信息更新成功");
            }
        }
    };
    function f4DeleteFnc() {
        $("#f4_del_btn").showCallBackWarnningDialog({
            errMsg: "是否删除版式,请确认!!!!",
            callbackFn: function (data) {
                if (data.result == true) {
                    var layot_id = $("#layotIDTxt").val();
                    if (layot_id == "") {
                        showErrorDialog("003", "请选择需要删除的版式");
                        return false;
                    }
                    var iary = {
                        layot_id: layot_id
                    };
                    var inTrxObj = {
                        trx_id: "XPLAYOUT",
                        action_flg: "D",
                        iary: iary,
                        tbl_cnt: 1
                    }
                    var outTrxObj = comTrxSubSendPostJson(inTrxObj);
                    if (outTrxObj.rtn_code == "0000000") {
                        var selectedRowID = $("#layotListGrd").jqGrid("getGridParam", "selrow");
                        $("#layotListGrd").jqGrid('delRowData', selectedRowID);
                        showSuccessDialog("版式信息删除成功");
                        initFnc();
                    }
                }
            }
        })


    }

    function setLayoutCateFnc(){
        var iary = {
            data_cate : "LYCT"
        };
        var inTrxObj = {
            trx_id      : 'XPLSTDAT' ,
            action_flg : 'Q'        ,
            iary        : iary
        };
        var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if(  outTrxObj.rtn_code == "0000000" ) {
            var tbl_cnt = comParseInt(outTrxObj.tbl_cnt);
            var oaryArr = $.isArray(outTrxObj.oary) ? outTrxObj.oary : [outTrxObj.oary];
            for (var i = 0; i < tbl_cnt; i++) {
                var oary = oaryArr[i];
                $("#layotCateSel").append("<option value=" + oary.data_ext + ">" + oary.data_desc + "</option>");
            }
        }
    }

    function f6AddFnc() {
        $("input").attr({'disabled': false});
        $("select").attr({'disabled': false});
        // $("input").val("");
        // $("select").empty();
        // setLayoutCateFnc();
        // $("#layotCateSel").append("<option value="+ "N" +">"+ "正常" +"</option>");
        // $("#layotCateSel").append("<option value="+ "T" +">"+ "旋转90度" +"</option>");
        // $("#layotCateSel").select2({
        // 	theme : "bootstrap"
        // });
        $("#f9_save_btn").data("save_flg", "A");
    }

    function f5UpdateFnc() {
        $("input").attr({'disabled': false});
        $("select").attr({'disabled': false});
        $("#layotIDTxt").attr({'disabled': true});
        var layotCate = $("#layotCateSel").val();
        // $("#layotCateSel").append("<option value="+ "N" +">"+ "正常" +"</option>");
        // $("#layotCateSel").append("<option value="+ "T" +">"+ "旋转90度" +"</option>");
        // $("#layotCateSel").select2({
        //     theme : "bootstrap"
        // });
        $("#layotCate").val(layotCate);
        $("#f9_save_btn").data("save_flg", "U");
    }

    function f10ClearFnc() {
        $("input").val("");
        // $("select").empty();
        $("#layotListGrd").jqGrid("clearGridData");
    }

    function showLayoutFnc() {
        var xAxisCnt = parseInt($("#xAxisCntTxt").val(),10);
        var yAxisCnt = parseInt($("#yAxisCntTxt").val(),10);

        // var xAxisCnt = 5;
        // var yAxisCnt = 11;

        $("#layoutDiv").empty();

        var str = "<table class='table table-bordered '>";
        for (var y = 1; y <= yAxisCnt; y++) {
            str = str + "<tr>";
            for (var x = xAxisCnt; x >= 1; x--) {
                var oxNum = "(" + (parseInt(((x - 1) * yAxisCnt), 10) + y) + ")";
                // var oxNum = "";
                if (y == 1 && x == 1) {
                    str = str + "<td class='btn-danger'>" + x + "*" + y + oxNum + "</td>";
                } else {
                    str = str + "<td>" + x + "*" + y + oxNum + "</td>";
                }

            }
            str = str + "</tr>";
        }
        str = str + "</table>"

        $("#layoutDiv").append($(str));
    }

    function trunFnc(x, y) {
        var xAxisCnt = parseInt(y,10);
        var yAxisCnt = parseInt(x,10);
        $("#trunedlayoutDiv").empty();
        var str = "<table class='table table-bordered '>";
        for (var y = yAxisCnt; y >= 1; y--) {
            str = str + "<tr>";
            for (var x = xAxisCnt; x >= 1; x--) {
                var oxNum = "(" + ((parseInt((y - 1) * xAxisCnt, 10)) + x) + ")";
                // var oxNum = "";
                if (y == 1 && x == 1) {
                    str = str + "<td class='btn-danger'>" + y + "*" + x + oxNum + "</td>";
                } else {
                    str = str + "<td>" + y + "*" + x + oxNum + "</td>";
                }

            }
            str = str + "</tr>";
        }
        str = str + "</table>"
        $("#trunedlayoutDiv").append($(str));

    }


    $("#trunBtn").click(function () {
        var xAxisCnt = $("#xAxisCntTxt").val();
        var yAxisCnt = $("#yAxisCntTxt").val();
        trunFnc(xAxisCnt, yAxisCnt);
    })

    $("#showBtn").click(showLayoutFnc);
    $("#f4_del_btn").click(f4DeleteFnc);
    $("#f5_upd_btn").click(f5UpdateFnc);
    $("#f6_add_btn").click(f6AddFnc);
    $("#f9_save_btn").click(f9SaveFnc);
    $("#f10_clear_btn").click(f10ClearFnc);
})