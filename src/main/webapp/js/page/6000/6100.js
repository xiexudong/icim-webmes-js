
  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Bis_qrs Management                                   */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/04/28 N0.00   Lin.Xin      Initial release                        */
  /*                                                                        */
  /**************************************************************************/
 
$(document).ready(function() {
  var _NORMAL = "0000000";
   //Stop from auto commit
  $("form").on("submit",false);
  var VAL = {
  	NORMAL : "0000000"
  };
  var itemInfoCM = [
    {name: 'path_cate' , index: 'path_cate' , label: PATH_CATE_TAG     , width: 60 },
    {name: 'path_id'   , index: 'path_id'   , label: PATH_ID_TAG       , width: 80 },
    {name: 'path_ver'  , index: 'path_ver'  , label: PATH_VER_TAG      , width: 60 }
  ];
  $("#pathListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:($(window).height() - $("#pathListDiv").offset().top) * 0.95,
      // height:400,//TODO:需要根据页面自适应，要相对很高
      width:$("#pathListDiv").width()*0.99,
      // autowidth:true,//宽度根据父元素自适应
      autowidth:false,
      // shrinkToFit:true,
      shrinkToFit:false,
      // scroll:true,
      resizable : true,
      loadonce:true,
      // toppager: true , 翻页控件是否在上层显示
      // fixed: true,
      // hidedlg:true,
      jsonReader : {
            // repeatitems: false
          },
      viewrecords : true, //显示总记录数
      pager : '#pathListPg',
      rownumbers  :true ,//显示行号
      rowNum:20,         //每页多少行，用于分页
      rownumWidth : 20,  //行号列宽度
      emptyrecords :true ,
      // pginput  : true ,//可以输入跳转页数
      // rowList:[10,15,20], //每页多少行
      // toolbar : [true, "top"],//显示工具列 : top,buttom,both
      colModel: itemInfoCM,
      gridComplete:function(){
          var gridPager,
              pageLen;
          gridPager = $(this).jqGrid("getGridParam","pager");
          if(gridPager.length<2){
             return false;
          }
          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
          $(".ui-pg-input").hide();
          $('td[dir="ltr"]').hide(); 
          $(gridPager+"_left").hide();
          $(gridPager+"_center").css({width:0});
      },
      onSelectRow:function(id){
      	var inObj,outObj;
        $("#pathConditionForm  input").attr({"disabled":true});
        $("#pathConditionForm  select").attr({"disabled":true});
        var FIRST_OPE_NO ="FRST";
        var rowData = $(this).jqGrid("getRowData",id);
        var iaryA = {};
        var iaryB = {};
        if(rowData.path_id!=""){
          iaryA.path_id    = rowData.path_id;
          iaryB.path_id_fk = rowData.path_id;
        }
        if(rowData.path_ver!=""){
          iaryA.path_ver    = rowData.path_ver;
          iaryB.path_ver_fk = rowData.path_ver;
        }
        inObj = {
    	  trx_id     : "XPBISPTH",
          action_flg : "Q",
          iaryA      : iaryA
        };
        outObj = comTrxSubSendPostJson(inObj);
        if(outObj.rtn_code === VAL.NORMAL){
        	oaryA = outObj.oaryA;
	        $("#pathIDTxt").val(oaryA.path_id);
	        $("#pathVerTxt").val(oaryA.path_ver);
            $("#pathDscTxt").val(oaryA.path_dsc);
//            $("#maxSwhCntTxt").val(oaryA.max_swh_cnt);
        	SelectDom.setSelect($("#pathCateSel"),oaryA.path_cate);
        	SelectDom.setSelect($("#startBankSel"),oaryA.str_bank_id);
        	SelectDom.setSelect($("#endBankSel"),oaryA.end_bank_id);
        	SelectDom.setSelect($("#mdlCateSel"),oaryA.mdl_cate);
        	if (oaryA.max_swh_cnt == 1) {
        		$("#rmaSel").empty();
        	    $("#rmaSel").append("<option value = '1'>RMA返工</option>");            		
        	    $("#rmaSel").append("<option value = '2'>正常</option>");
        	}else{
        		$("#rmaSel").empty();
        	    $("#rmaSel").append("<option value = '2'>正常</option>");
        	    $("#rmaSel").append("<option value = '1'>RMA返工</option>");            		
        	}
        	$("#rmaSel").select2({
     	    	theme : "bootstrap"
     	    });
        }
        var inTrxObj ={
          trx_id     : "XPBISPTH",
          action_flg : "I",
          iaryB      : iaryB,
          tbl_cnt_b  : 1
        };
        var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if(  outTrxObj.rtn_code == _NORMAL ) {
          $("#queryPathForm select").empty();
          $("#pathItemListGrd").jqGrid("clearGridData");
          var tbl_cnt_a = outTrxObj.tbl_cnt_a ;
          var tbl_cnt_b = outTrxObj.tbl_cnt_b ;
          if( tbl_cnt_a > 0 ){

            if(tbl_cnt_b==1){
              setGridInfo(outTrxObj.oaryB,"#pathItemListGrd");  
            }else{
              var pv_ope_no ="";
              for(var i=0;i<tbl_cnt_b;i++){
            	  if(outTrxObj.oaryB[i].pv_ope_no==FIRST_OPE_NO){
                      $("#pathItemListGrd").jqGrid("addRowData",0,outTrxObj.oaryB[i]);
                      pv_ope_no = outTrxObj.oaryB[i].cr_ope_no;
                      break;
                  }
              }
               for(var i=0;i<tbl_cnt_b;i++){
                  if(outTrxObj.oaryB[i].pv_ope_no==FIRST_OPE_NO){
                	  continue;
                  }else{
                    var newRowID  = getGridNewRowID($("#pathItemListGrd"));
                    for(var j=0;j<tbl_cnt_b;j++){
                      if(outTrxObj.oaryB[j].pv_ope_no==pv_ope_no){
                        $("#pathItemListGrd").jqGrid("addRowData",newRowID,outTrxObj.oaryB[j]); 
                        pv_ope_no = outTrxObj.oaryB[j].cr_ope_no; 
                        break;
                      }
                    }
                    
                  }
               }
            }
            
            
          }
          
        }

      } 
  });
  //QRS 详细子项目明细
  var pathItemInfoCM2 = [
    {name: 'cr_ope_no'                , index: 'cr_ope_no'                , label: OPE_NO_TAG               , width: 70 },
    {name: 'cr_ope_id_fk'             , index: 'cr_ope_id_fk'             , label: OPE_ID_TAG               , width: 70 },
    {name: 'cr_ope_ver_fk'            , index: 'cr_ope_ver_fk'            , label: OPE_VER_TAG               , width: 70 },
    {name: 'swh_avl_flg'              , index: 'swh_avl_flg'              , label: SWH_AVL_FLG_TAG          , width: 90 },
    {name: 'wip_bank_flg'             , index: 'wip_bank_flg'             , label: WIP_BANK_FLG_TAG         , width: 100 },
    {name: 'dft_wip_bank_tool_id_fk'  , index: 'dft_wip_bank_tool_id_fk'  , label: DFT_WIP_BANK_TOOL_ID_TAG , width: 70 }
  ];
  $("#pathItemListGrd").jqGrid({
	  url:"",
      datatype:"local",
      mtype:"POST",
      height:($(window).height() - $("#pathItemListDiv").offset().top) * 0.95,
      // height:400,//TODO:需要根据页面自适应，要相对很高
      width:$("#pathItemListDiv").width()*0.99,
      // autowidth:true,//宽度根据父元素自适应
      autowidth:false,
      shrinkToFit:false,
      resizable : true,
      loadonce:true,
      viewrecords : true, //显示总记录数
      rownumbers  :true ,//显示行号
      rowNum:200,         //每页多少行，用于分页
      rownumWidth : 20,  //行号列宽度
      emptyrecords :true ,
      colModel: pathItemInfoCM2
//      url:"",
//      datatype:"local",
//      mtype:"POST",
//      height:$("#pathItemListDiv").height(),
//       
//      autowidth: false,
//      shrinkToFit:false,
//      width:$("#pathItemListDiv").width()*0.95,
//      scroll:false,//如需要分页控件,必须将此项置为false
//      loadonce:true,
//      jsonReader : {
//            // repeatitems: false
//          },
//      viewrecords : true, //显示总记录数
//      emptyrecords :true ,
//      rownumbers  :true ,//显示行号
//      rowNum:10,         //每页多少行，用于分页
//      pager : '#pathItemListPg',
//      toolbar:true,
//      colModel: pathItemInfoCM2
  });

  //页面高度变化时，自动调整Grid大小
  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#pathListGrd").setGridWidth($("#pathListDiv").width()*0.95); 
      $("#pathListGrd").setGridHeight($("pathListDiv").height()*0.80); 
      $("#pathItemListGrd").setGridWidth($("#pathItemListDiv").width()*0.95); 
      $("#pathItemListGrd").setGridHeight($("pathItemListDiv").height()*0.80);     
      $(window).bind("onresize", this);  
  };
  function initMdlCate(){
  	var outObj = baseFnc.sendXplstData("MDCT");
  	SelectDom.initWithSpace($("#mdlCateSel"));
	SelectDom.addSelectArr($("#mdlCateSel"),outObj.oary,"data_ext","data_desc");
  }
  function initFnc(){
    $("input").attr({'disabled':true});
    $("select").attr({'disabled':true});
    $("input").val("");
    $("select").empty();
    $("#rmaSel").append("<option value = '2'>正常</option>");
    $("#rmaSel").append("<option value = '1'>RMA返工</option>");
    $("#rmaSel").select2({
    	theme : "bootstrap"
    });
    initMdlCate();
    // $("#pathItemForm").css({"margin-top":$("#pathListDiv").height()-50});
  }
  
  
  function showAddPathItemDialog( pathItemObj ){
    function pathItemDialogShowFnc(pathItemObj){
      $('#pathItemDialog  input').attr({'disabled':false});
      $('#pathItemDialog  input').val("");
      $('#pathItemDialog  select').attr({'disabled':false});
      $('#pathItemDialog  select').empty();
      $('#pathItemDialog  checkbox').attr({'disabled':false});
      $('#pathItemDialog  checkbox').removeAttr("checked");

      var inTrxObj = {
          trx_id     : 'XPBISOPE' ,
          action_flg : 'L'        
      };
      setSelectObjByinTrx("#pathItemDialog_crOpeIDSel",inTrxObj,"ope_id");
      
      if(pathItemObj.action_flg=="N"){
        $("#pathItemDialog_savePathItemBtn").data("save_flg","N");
        crOpeIDChangeFnc();
      }else{
        $('#pathItemDialog_crOpeNOTxt').attr({'disabled':true});
        $("#pathItemDialog_savePathItemBtn").data("save_flg","R");
         
        $("#pathItemDialog_crOpeNOTxt").val(pathItemObj.cr_ope_no );
        $("#pathItemDialog_crOpeIDSel").val(pathItemObj.cr_ope_id_fk);
        $("#pathItemDialog_crOpeVerSel").empty();
        $("#pathItemDialog_crOpeVerSel").append("<option value="+ pathItemObj.cr_ope_ver_fk +">"+pathItemObj.cr_ope_ver_fk+"</option>>");
        $("#pathItemDialog_crOpeVerSel").select2({
        	theme : "bootstrap"
        });
        setSelectObjByinTrx("#pathItemDialog_crOpeIDSel",inTrxObj,"ope_id");
        if(pathItemObj.swh_avl_flg=="Y"){
          $("#pathItemDialog_swhAvlFlgChk").attr("checked","true");        
        }else{
          $("#pathItemDialog_swhAvlFlgChk").removeAttr("checked");   
        }
        if(pathItemObj.wip_bank_flg=="Y"){
          $("#pathItemDialog_wipBankFlgChk").attr("checked","true");   
        }else{
          $("#pathItemDialog_wipBankFlgChk").removeAttr("checked");
        }
      }
      if($("#pathCateSel").find("option:selected").text()!=="MAIN"){
    	  $("#pathItemDialog_swhAvlFlgChk").attr({"disabled":true});
      }else{
    	  $("#pathItemDialog_swhAvlFlgChk").attr({"disabled":false});

      }
      if($("#pathItemDialog_wipBankFlgChk").attr("checked")==="checked"){
    	  $("#pathItemDialog_wipBnkIdSel").attr({"disabled":false});
      }else{
    	  $("#pathItemDialog_wipBnkIdSel").attr({"disabled":true});
      }
      $("#pathItemDialog_wipBnkIdSel").append("<option></option>");
      var inObj = {
    		trx_id     : "XPBISTOL",
    		action_flg : "Q",
    		iary       :{
    			tool_cate : "WPBK"
    		}
      } ;
      var outObj = comTrxSubSendPostJson(inObj);
      var oary,tblCnt,i;
      if (outObj.rtn_code === "0000000") {
    	  
    	  tblCnt = outObj.tbl_cnt;
    	  for(i=0;i<tblCnt;i++){
    		  oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
    		  $("#pathItemDialog_wipBnkIdSel").append("<option value=" + oary.tool_id + ">"+ oary.tool_id + "</option>");
    	  }
      }
      $("#pathItemDialog_wipBnkIdSel").select2({
      	theme : "bootstrap"
      });
    }
    function savePathItemFnc(){
      var action_flg = $("#pathItemDialog_savePathItemBtn").data("save_flg");
      if( action_flg == undefined ){
        return false;
      }
      if($("#pathItemDialog_wipBankFlgChk").attr("checked")==="checked" && !$("#pathItemDialog_wipBnkIdSel").val()){
    	  showErrorDialog("","请选择在制仓位");
    	  return false;
      }
      var iaryB = {
          path_id_fk    : $("#pathIDTxt").val().trim(),
          path_ver_fk   : $("#pathVerTxt").val().trim(),
          cr_ope_no     : $('#pathItemDialog_crOpeNOTxt').val().trim()                    ,
          cr_ope_id_fk  : $("#pathItemDialog_crOpeIDSel").find("option:selected").text()  ,
          cr_ope_ver_fk : $("#pathItemDialog_crOpeVerSel").find("option:selected").text()

      };
      if($("#pathItemDialog_swhAvlFlgChk").attr("checked")=="checked"){
        iaryB.swh_avl_flg="Y";
      }else{
        iaryB.swh_avl_flg="N";
      }
      if($("#pathItemDialog_wipBankFlgChk").attr("checked")=="checked"){
        iaryB.wip_bank_flg="Y";
        iaryB.dft_wip_bank_tool_id_fk = $("#pathItemDialog_wipBnkIdSel").val();
      }else{
        iaryB.wip_bank_flg="N";
        iaryB.dft_wip_bank_tool_id_fk ="";
      }
      $('#pathItemDialog').modal("hide");
      var registActionFlg = $("#f8_regist_btn").data("regist_flg");
      if(action_flg=="N"){
        var newRowID = getGridNewRowID("#pathItemListGrd");
        $("#pathItemListGrd").jqGrid("addRowData",newRowID,iaryB);
        if(registActionFlg=="A"||registActionFlg=="AB"){
          $("#f8_regist_btn").data("regist_flg","AB"); 
        }else{
          $("#f8_regist_btn").data("regist_flg","B");  
        }
        
      }else{
        var selectRowID = $("#pathItemListGrd").jqGrid("getGridParam","selrow");
        $("#pathItemListGrd").jqGrid("setRowData",selectRowID,iaryB);  
        if(registActionFlg=="A"){
          $("#f8_regist_btn").data("regist_flg","AB");  
        }else{
          $("#f8_regist_btn").data("regist_flg","B");  
        }
      }
      
    }
    function crOpeIDChangeFnc(){
        var cr_ope_id = $("#pathItemDialog_crOpeIDSel").find("option:selected").text();
        
        var iary ={
          ope_id : cr_ope_id
        };
        var inTrxObj={
          trx_id      : 'XPBISOPE' ,
          action_flg  : "Q"        ,
          iary        : iary 
        };
        setSelectObjByinTrx("#pathItemDialog_crOpeVerSel",inTrxObj,"ope_ver");
    }
    function selectWipBankFlgChk(){
    	if($("#pathItemDialog_wipBankFlgChk").attr("checked")=="checked"){
    		$("#pathItemDialog_wipBnkIdSel").attr({"disabled":false});
    	}else{
    		$("#pathItemDialog_wipBnkIdSel").attr({"disabled":true});
    		$("#pathItemDialog_wipBnkIdSel").val("");
    	}
    }
    $('#pathItemDialog').modal({
      backdrop:true ,
      keyboard:false,
      show:true
    });
    $('#pathItemDialog').unbind('shown.bs.modal');
    $("#pathItemDialog_crOpeIDSel").unbind('change');
    $("#pathItemDialog_savePathItemBtn").unbind('click');
    $("#pathItemDialog_wipBankFlgChk").unbind('change');
    $('#pathItemDialog').bind('shown.bs.modal',pathItemDialogShowFnc(pathItemObj));
    $("#pathItemDialog_savePathItemBtn").bind('click',savePathItemFnc);
    $("#pathItemDialog_crOpeIDSel").bind('change',crOpeIDChangeFnc);
    $("#pathItemDialog_wipBankFlgChk").bind('change',selectWipBankFlgChk);

  }
  function addPathItemFnc(){
    if(  $("#pathIDTxt").val()!="" &&
         $("#pathVerTxt").val()!="" 
      ){
        // var rowIDs = $("#pathItemListGrd").jqGrid('getDataIDs');
        // var row_id = 1;
        // if(rowIDs.length != 0){
        //   row_id = rowIDs[ rowIDs.length - 1 ] + 1 ;  
        // }
        var pathItemObj  = {
          action_flg : "N"
        };
        // $("#pathItemListGrd").jqGrid("addRowData",row_id,pathItemObj);
        showAddPathItemDialog(pathItemObj); 
    }else{
        showErrorDialog("003","请在列表中选择工艺路线");
        return false;
    }
    
  }
  function updPamItemFnc(){
    if(  $("#pathIDTxt").val()!="" && $("#pathVerTxt").val()!=""){

        var selectID = $("#pathItemListGrd").jqGrid("getGridParam","selrow");
        if(selectID==null){
          showErrorDialog("003","请选择需要更新的工艺项目");
          return false;
        }
        var rowData = $("#pathItemListGrd").jqGrid("getRowData",selectID);
        rowData.action_flg ="R";
        showAddPathItemDialog(rowData);
    }
  }
  function delPamItemFnc(){
    var selectID = $("#pathItemListGrd").jqGrid("getGridParam","selrow");
    if(selectID==null){
      showErrorDialog("003","请选择需要删除的工艺项目");
      return false;
    }
    // var rowData = $("#pathItemListGrd").jqGrid("getRowData",selectID);
    $("#del_pathItem_btn").showCallBackWarnningDialog({
        errMsg  : "是否删除工艺项目,请确认!!!!",
        callbackFn : function(data) {
          if(data.result==true){
            $("#pathItemListGrd").jqGrid("delRowData",selectID);
            $("#f8_regist_btn").data("regist_flg","B"); 
            $("#pathItemDialog_savePathItemBtn").data("save_flg","N");
            // var path_id_fk  = $("#pathIDTxt").find("option:selected").text();
            // var path_ver_fk = $("#pathVerTxt").find("option:selected").text();
            // var iaryB = {
            //    path_id_fk      : path_id_fk       ,
            //    path_ver_fk     : path_ver_fk      ,
            //    ope_no_fk       : rowData.ope_no_fk
            // }
            // var inTrxObj = {
            //     trx_id     : 'XPBISPAM' ,
            //     action_flg : 'C'        ,
            //     iaryB      : iaryB 
            // };
            // var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
            // if(  outTrxObj.rtn_code == _NORMAL ) {
            //     showSuccessDialog("删除工艺项目成功");
            //     var iaryA = {};
            //     var iaryB = {};
            //     if(path_id_fk!=""){
            //       iaryA.path_id    = path_id_fk,
            //       iaryB.path_id_fk = path_ver_fk
            //     }
            //     if(path_ver_fk!=""){
            //       iaryA.path_ver    = path_id_fk,
            //       iaryB.path_ver_fk = path_ver_fk
            //     }
            //     // if(rowData.tool_id_fk!=""){
            //     //   iary.tool_id_fk = rowData.tool_id_fk;
            //     // }
            //     var inTrxObj ={
            //       trx_id     : "XPBISPTH",
            //       action_flg : "I",
            //       iaryA      : iaryA,
            //       iaryB      : iaryB,
            //       tbl_cnt_a  : 1    ,
            //       tbl_cnt_b  : 1
            //     };
            //     var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
            //     if(  outTrxObj.rtn_code == _NORMAL ) {
            //       $("#queryPathForm select").empty();
            //       var oary={};
            //       var tbl_cnt_a = outTrxObj.tbl_cnt_a ;
            //       if( tbl_cnt_a > 0 ){
            //         setGridInfo(outTrxObj.oaryB,"#pathItemListGrd");
            //       }
            //     }
            // }  
          }
        }
    });
  }

  function f1QueryFnc(){

    /*** 将Div实例化为modal窗体 ***/
    function diaLogQueryFnc(){
      var iaryA = {};
      if($("#queryPathDialog_pathCateTxt").val()!=""){
        iaryA.path_cate = $("#queryPathDialog_pathCateTxt").val();
      }
      if($("#queryPathDialog_pathIDTxt").val()!=""){
        iaryA.path_id = $("#queryPathDialog_pathIDTxt").val();
      }
      if($("#queryPathDialog_pathVerTxt").val()!=""){
        iaryA.path_ver = $("#queryPathDialog_pathVerTxt").val();
      }
      var inTrxObj ={
         trx_id     : "XPBISPTH" ,
         action_flg : 'Q'        ,   
         iaryA      : iaryA      ,
         tbl_cnt_a  : 1
      };
      var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(  outTrxObj.rtn_code == _NORMAL ) {
         setGridInfo(outTrxObj.oaryA,"#pathListGrd");
         $('#queryPathDialog').modal("hide");
      }
    }
    
    $('#queryPathDialog').modal({
        backdrop:true,
        keyboard:false,
        show:false
    });
    $('#queryPathDialog').unbind('shown.bs.modal');
    $("#queryPathDialog_queryBtn").unbind('click');

    $('#queryPathDialog').bind('shown.bs.modal');
    $('#queryPathDialog').modal("show");
    $("#queryPathDialog_queryBtn").bind('click',diaLogQueryFnc);
    $("#queryPathDialog input").attr({'disabled':false});
    // $("#queryPathDialog_pathIDTxt").bind('keyup',function(event){
    //   if(event.keyCode==13){
    //     diaLogQueryFnc();
    //   }
    // })
  }
  function f4DeleteFnc(){
    var selectID = $("#pathListGrd").jqGrid("getGridParam", "selrow" );
    if( selectID == null){
      showErrorDialog("003","请选择需要删除的工艺路线");
      return false;
    }
    var rowData  = $("#pathListGrd").jqGrid("getRowData",selectID);
    $("#f4_del_btn").showCallBackWarnningDialog({
          errMsg  : "是否删除工艺路线,请确认!!!!",
          callbackFn : function(data) {
            if(data.result==true){
              
              var iaryA ={};
              iaryA.path_id  = rowData.path_id;
              iaryA.path_ver = rowData.path_ver;
              var inTrxObj = {
                trx_id     : "XPBISPTH",
                action_flg :"D",
                iaryA      : iaryA,
                tbl_cnt_a  : 1
              };
              var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
              if(  outTrxObj.rtn_code == _NORMAL ) {
                showSuccessDialog("删除工艺路线成功");
                $("#pathConditionForm select").empty();
                $("#pathConditionForm input").val("");
                $("#pathListGrd").jqGrid('delRowData', selectID);
                $("#pathItemListGrd").jqGrid("clearGridData");
              }
            }
          }
    });
    
  }
  function setSelectObjByToolCateFnc(selectObj,tool_cate){
    var selectTxt = $(selectObj).find("option:selected").text();
     $(selectObj).empty();
     var iary = {
        tool_cate : tool_cate   
     };
     var inTrxObj = {
         trx_id      : 'XPBISTOL' ,
         action_flg : 'Q'        ,
         iary        : iary
     };
     var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
     if(  outTrxObj.rtn_code == "0000000" ) {
          var tbl_cnt = comParseInt( outTrxObj.tbl_cnt);
          if( tbl_cnt == 1 ){
            //下面的tool_id要加双引号"tool-_id", oary.tool_id=oary["tool_id"]
            $(selectObj).append("<option value="+ outTrxObj.oary["tool_id"] +">"+ outTrxObj.oary["tool_id"] +"</option>");
          }else if(tbl_cnt>1){
            for(var i=0;i<tbl_cnt;i++){
              $(selectObj).append("<option value="+ outTrxObj.oary[i]["tool_id"] +">"+ outTrxObj.oary[i]["tool_id"] +"</option>");
            }
            queryObj.select2({
    	    	theme : "bootstrap"
    	    });
          }
     }
    $(selectObj).val(selectTxt);     
  }
  function setBankFnc(){
    var iary,
      inTrxObj,
      outTrxObj,
      tool_cate,
      tbl_cnt,
      oary;
    var oldStartBank = $("#startBankSel").find("option:selected").text();
    var oldEndBank   = $("#endBankSel").find("option:selected").text();
    iary = {
       unit_typ  : "BANK"
       // tool_cate : "STBK"
    };
    inTrxObj = {
      trx_id      : "XPBISTOL",
      action_flg  : "Q"       ,
      iary        : iary      ,
      tbl_cnt     : 1
    };
    outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if( outTrxObj.rtn_code=="0000000"){
        tbl_cnt = outTrxObj.tbl_cnt;
          for(var i=0;i<tbl_cnt;i++){
            oary = tbl_cnt > 1 ?  outTrxObj.oary[i]: outTrxObj.oary;
            tool_cate = oary.tool_cate;
            if(tool_cate=="STBK"){
              $("#startBankSel").append("<option value="+oary.tool_id+">" +oary.tool_id +"</option>"); 
              $("#startBankSel").select2({
      	    	theme : "bootstrap"
      	    });
            }else if(tool_cate=="ENBK"){
              $("#endBankSel").append("<option value="+oary.tool_id+">" +oary.tool_id +"</option>"); 
              $("#endBankSel").select2({
      	    	theme : "bootstrap"
      	    });
            }
           
       }
       //$("#startBankSel").val(oldStartBank);
       //$("#endBankSel").val(oldEndBank);
    }
  }
  var baseFnc = {
		sendXplstData : function(dataCate) {
			var inObj = {
				trx_id : "XPLSTDAT",
				action_flg : "Q",
				iary : {
					data_cate : dataCate
				}
			};
			outObj = comTrxSubSendPostJson(inObj);
			return outObj.rtn_code === "0000000" ? outObj : false;
		}
	};
		
  function f6AddFnc(){
    var outObj;
    $("#pathConditionForm input").attr({'disabled':false});
    $("#pathConditionForm select").attr({'disabled':false});
    $("#pathConditionForm input").val("");
    $("#pathConditionForm select").empty();
    $("#rmaSel").append("<option value = '2'>正常</option>");
    $("#rmaSel").append("<option value = '1'>RMA返工</option>");    
    setBankFnc();
    addValueByDataCateFnc("#pathCateSel","RCAT","data_ext");
    outObj = baseFnc.sendXplstData("MDCT");
	$("#mdlCateSel").empty();
	SelectDom.addSelectArr($("#mdlCateSel"),outObj.oary,"data_ext","data_desc");
    $("#pathItemListGrd").jqGrid("clearGridData");
    $("#f8_regist_btn").data("regist_flg","A");
  }
  function savePathInfoFnc(){
	  	var rmaFlg;
	  	rmaFlg = $("#rmaSel").val();
	    if($("#pathIDTxt").val()==""){
	      showErrorDialog("003","请输入工艺路线");
	      $("#pathIDTxt").focus();
	      return false;
	    }
	    if($("#pathVerTxt").val()==""){
	      showErrorDialog("003","请输入工艺路线版本");
	      $("#pathVerTxt").focus();
	      return false;
	    }
	    var mdlCate = $("#mdlCateSel").val();
	    if(!mdlCate){
	    	showErrorDialog("","请选择工艺属性");
	    	return false;
	    }
	    var iaryA     = {
	      path_id    : $("#pathIDTxt").val(),
	      path_ver   : $("#pathVerTxt").val(),
	      path_dsc   : $("#pathDscTxt").val(),
	      path_cate  : $("#pathCateSel").find("option:selected").text(),
	      max_swh_cnt: rmaFlg,
	      str_bank_id: $("#startBankSel").find("option:selected").text(),
	      end_bank_id: $("#endBankSel").find("option:selected").text(),
	      mdl_cate   : mdlCate
	    };

	    var inTrxObj = {
	       trx_id      : 'XPBISPTH' ,
	       action_flg  : 'A'        ,
	       iaryA       : iaryA      ,
	       tbl_cnt_a   : 1
	    };
	    var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
	    if(  outTrxObj.rtn_code == "0000000" ) {
	        var newRowID = getGridNewRowID("#pathListGrd");
	        $("#pathListGrd").jqGrid("addRowData",newRowID,iaryA);
	        $("#pathConditionForm input").attr({'disabled':true});
	        $("#pathConditionForm select").attr({'disabled':true});
	        return true;
	    }
  }
  function savePathItemFnc(){
    var rowIDs = $("#pathItemListGrd").jqGrid("getDataIDs");
    if(rowIDs.length<=0){
      return false;
    }
    var rowCnt = rowIDs.length;
    var iaryBList = new Array();
    var iaryA = {};
    for(var i=0;i<rowIDs.length;i++){
      var pvRowData  = null; 
      var crRowData  = null; 
      var nxRowData  = null;

      var crRowData =$("#pathItemListGrd").jqGrid("getRowData",rowIDs[i]);
      if(i<rowCnt-1){
          rowData2 = $("#pathItemListGrd").jqGrid("getRowData",rowIDs[i+1]);  
      }
      var iaryB = crRowData;
      
      iaryB.path_id_fk  = $("#pathIDTxt").val();
      iaryB.path_ver_fk = $("#pathVerTxt").val();
      if(i==0){
        nxRowData       = $("#pathItemListGrd").jqGrid("getRowData",rowIDs[i+1]);
        iaryB.pv_ope_no = "FRST" ;
        iaryB.nx_ope_no = nxRowData.cr_ope_no ;
        iaryA.first_ope_no = crRowData.cr_ope_no;
      }else if(i==rowCnt.length-1){
        crRowData       = $("#pathItemListGrd").jqGrid("getRowData",rowIDs[i-1]);
        iaryB.pv_ope_no = pvRowData.cr_ope_no ;
        iaryB.nx_ope_no = "LAST" ;
      }else{
        nxRowData = $("#pathItemListGrd").jqGrid("getRowData",rowIDs[i+1]);
        crRowData = $("#pathItemListGrd").jqGrid("getRowData",rowIDs[i-1]);
        iaryB.pv_ope_no = crRowData.cr_ope_no ;
        iaryB.nx_ope_no = nxRowData.cr_ope_no ;
      }
      iaryBList.push(iaryB);
    }
    var inTrxObj = {
         trx_id      : 'XPBISPTH' ,
         action_flg  : 'N'        ,
         iaryB       : iaryBList  ,
         tbl_cnt_b   : rowCnt     ,
         iaryA       : iaryA      ,
         tbl_cnt_a   : 1
    };
    var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if(  outTrxObj.rtn_code == "0000000" ) {
        return true;
    }
  }
  function f8RegistFnc(){
    var registFlg = $("#f8_regist_btn").data("regist_flg");
    isPathChange = ( $("#pathIDTxt").attr("disabled") !== "disabled" ) ;
    if(isPathChange){
    	var isSuccess = savePathInfoFnc() && savePathItemFnc();
	    if( isSuccess ==true){
	        showSuccessDialog("新增工艺成功");  
	    }
    }else{
       var isSuccess = savePathItemFnc();
       if( isSuccess ==true){
           showSuccessDialog("新增工艺成功");  
       }
    }
  }
  function f10ClearFnc(){
    $("input").val("");
    $("select").empty();
    $("#rmaSel").append("<option value = '2'>正常</option>");
    $("#rmaSel").append("<option value = '1'>RMA返工</option>");
    $("#pathListGrd").jqGrid("clearGridData");
    $("#pathItemListGrd").jqGrid("clearGridData");
  }

  var BranchPath = {
    showBranchPathDialogFnc : function(){
      //初始化
	  $("#fr_path_id_fk").val("");
      $("#fr_path_ver_fk").val("");
      $("#fr_ope_no_fk").val("");
      $("#swh_id").val("")          ;
      $("#swh_dsc").val("")         ;
      $("#swh_path_id_fk").val("")  ;
      $("#swh_path_ver_fk").val("") ;
      $("#out_ope_no_fk").val("")   ;
      
      
      var selectRowID,fatherGridID;
      selectRowID = $("#pathItemListGrd").jqGrid("getGridParam","selrow");
      fatherGridID = $("#pathListGrd").jqGrid("getGridParam","selrow");
      if(selectRowID==""||selectRowID==null){
         showErrorDialog("003","请在工艺路线信息中选择需要添加分支的途程和站点");
         return false;
      }
      
      var rowData = $("#pathItemListGrd").jqGrid("getRowData", selectRowID);
      var rowDataFa = $("#pathListGrd").jqGrid("getRowData", fatherGridID);
      
      $("#fr_path_id_fk").val($("#pathIDTxt").val());
      $("#fr_path_ver_fk").val($("#pathVerTxt").val());
      $("#fr_ope_no_fk").val(rowData.cr_ope_no);
      
      $('#branchPathDialog').modal({
        backdrop:true ,
        keyboard:false,
        show:false
      });
      $('#branchPathDialog').unbind('shown.bs.modal');
      $("#branchPathDialog_add_branchItem_btn").unbind('click');
      $("#branchPathDialog_del_branchItem_btn").unbind('click');
      $("#branchPathDialog_branch_regist_btn").unbind('click');
      $('#branchPathDialog').bind('shown.bs.modal');
      $("#branchPathDialog_add_branchItem_btn").bind('click',this.addBranchFnc);
      $("#branchPathDialog_del_branchItem_btn").bind('click',this.deleteBranchFnc);
      $("#branchPathDialog_branch_regist_btn").bind('click',this.saveBranchFnc);
      $("#refreshGrid").unbind('click');
      $("#refreshGrid").bind('click',queryBranchByPath);
      $('#branchPathDialog').modal("show");
      this.initialBranchPathDialogFnc();
      this.getBranchByPath( $("#fr_path_id_fk").val(), $("#fr_path_ver_fk").val(),$("#fr_ope_no_fk").val());
    },
    getBranchByPath : function(fr_path_id_fk,fr_path_ver_fk,fr_ope_no_fk){
       var pathItemRowData,
           iary,
           inTrxObj,
           outTrxObj;
       iary = {
         fr_path_id_fk  : fr_path_id_fk,
         fr_path_ver_fk : fr_path_ver_fk,
         fr_ope_no_fk   : fr_ope_no_fk
       };
       inTrxObj ={
          trx_id      : "XPSWHPTH",
          action_flg  : "Q",
          iary        : iary,
          tbl_cnt     : 1
       };
       outTrxObj = comTrxSubSendPostJson(inTrxObj);
       if(outTrxObj.rtn_code=="0000000"){
          setGridInfo(outTrxObj.oary,"#branchPathListGrd");
       }

    },
    initialBranchPathDialogFnc   : function(){
      var branchItemInfoCM = [
        {name: 'swh_id'            , index: 'swh_id'           , label: SWITCH_ID_TAG       ,editable:true, width: 80 },
        {name: 'swh_path_id_fk'    , index: 'swh_path_id_fk'   , label: SWH_PATH_ID_FK_TAG  ,editable:true, width: 120 },
        {name: 'swh_path_ver_fk'   , index: 'swh_path_ver_fk'  , label: SWH_PAHT_VER_FK_TAG ,editable:true, width: 80 },
        {name: 'swh_dsc'           , index: 'swh_dsc'          , label: SWH_DSC_TAG         ,editable:true, width: 200 },
        {name: 'out_ope_no_fk'     , index: 'out_ope_no_fk'    , label: OUT_OPE_NO_FK_TAG   ,editable:true, width: 110 }
      ];
      $("#branchPathListGrd").jqGrid({
          url:"",
          datatype:"json",
          mtype:"POST",
          autowidth: true,
          width:500,
          shrinkToFit:false,
          width:$("#branchPathListDiv").width(),
          autoScroll: true, 
          loadonce:false,
          viewrecords : true, //显示总记录数
          emptyrecords :true ,
          cellsubmit : "clientArray",
          toolbar : [true, "top"],
          forceFit:true ,
          pager  : '#branchPathListPg', 
          colModel: branchItemInfoCM
      });
    },
    
    
    saveBranchFnc   : function(){
	    var iary,inTrxObj,outTrxObj;
	    iary = {
			 swh_id          : $("#swh_id").val()          ,
			 swh_dsc         : $("#swh_dsc").val()         ,
    		 swh_path_id_fk  : $("#swh_path_id_fk").val()  ,
    		 swh_path_ver_fk : $("#swh_path_ver_fk").val() ,
    		 out_ope_no_fk   : $("#out_ope_no_fk").val()   ,
    		 fr_path_id_fk   : $("#fr_path_id_fk").val()   ,
    		 fr_path_ver_fk  : $("#fr_path_ver_fk").val()  ,
    		 fr_ope_no_fk    : $("#fr_ope_no_fk").val()    ,
    		 swh_typ         : "R"
    	 };
	    
	    inTrxObj ={
            trx_id      : "XPSWHPTH",
            action_flg  : "A",
            iary        : iary
         };
         outTrxObj = comTrxSubSendPostJson(inTrxObj);
         if(outTrxObj.rtn_code=="0000000"){
            queryBranchByPath();
            $("#swh_id").val("")          ;
			$("#swh_dsc").val("")         ;
   		 	$("#swh_path_id_fk").val("")  ;
   		 	$("#swh_path_ver_fk").val("") ;
   		 	$("#out_ope_no_fk").val("")   ;
   		 	$("#pathInput input").attr("disabled",true);
         }
    },
    
    deleteBranchFnc : function(){
    	var selectRowID,rowData,iary,inTrx,outTrx;
	    var selectRowID = $("#branchPathListGrd").jqGrid("getGridParam","selrow");
//	    $("#branchPathListGrd").jqGrid("delRowData",selectRowID);
	    
	    if (selectRowID==""||selectRowID==null) {
			return false;
		}
	    var rowData = $("#branchPathListGrd").jqGrid("getRowData",selectRowID);
	    
	    iary = {
    		 fr_path_id_fk   : $("#fr_path_id_fk").val()   ,
    		 fr_path_ver_fk  : $("#fr_path_ver_fk").val()  ,
    		 fr_ope_no_fk    : $("#fr_ope_no_fk").val()    ,
    		 swh_id          : rowData['swh_id']           ,
    		 swh_typ         : "R"
	    };
	    
	    inTrx = {
    		trx_id      : "XPSWHPTH",
            action_flg  : "D",
            iary        : iary
	    };
	    
	    outTrx = comTrxSubSendPostJson(inTrx);
        if(outTrx.rtn_code=="0000000"){
           queryBranchByPath();
        }
	    
    },
    addBranchFnc : function(){
    	$("#pathInput input").attr("disabled",false);
    }
  };
  
  function queryBranchByPath (){
      var iary,
          inTrxObj,
          outTrxObj;
      iary = {
        fr_path_id_fk  : $("#fr_path_id_fk").val(),
        fr_path_ver_fk : $("#fr_path_ver_fk").val() ,
        fr_ope_no_fk   : $("#fr_ope_no_fk").val() 
      };
      inTrxObj ={
         trx_id      : "XPSWHPTH",
         action_flg  : "Q",
         iary        : iary,
         tbl_cnt     : 1
      };
      outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code=="0000000"){
         setGridInfo(outTrxObj.oary,"#branchPathListGrd");
      }
   }
  
  $("#branch_pathItem_btn").click(function (){
    var iary,
        inTrxObj,
        outTrxObj,
        selectRowID
    selectRowID = $("#pathItemListGrd").jqGrid("getGridParam","selrow");
    if(selectRowID==""||selectRowID==null){
       showErrorDialog("003","请在工艺路线信息中选择需要添加分支的途程和站点");
       return false;
    }
    BranchPath.showBranchPathDialogFnc();
  });
  $("#add_pathItem_btn").click(addPathItemFnc);
  $("#upd_pathItem_btn").click(updPamItemFnc);
  $("#del_pathItem_btn").click(delPamItemFnc);

  $("#f1_query_btn").click(f1QueryFnc);

  $("#f6_add_btn").click(f6AddFnc);
  $("#f8_regist_btn").click(f8RegistFnc);
  $("#f4_del_btn").click(f4DeleteFnc);
  $("#f10_clear_btn").click(f10ClearFnc);
  initFnc();  
})