/** *********************************************************************** */
/*                                                                        */
/* System Name : ICIM */
/*                                                                        */
/* Description : Bis_param Management */
/*                                                                        */
/* MODIFICATION HISTORY */
/* Date Ver Name Description */
/* ---------- ----- ----------- ----------------------------------------- */
/* 2013/04/28 N0.00 Lin.Xin Initial release */
/* 2014/06/30 N0.01 Lin.Xin Refactor all */
/*                                                                        */
/** *********************************************************************** */

$(document).ready(function() {
	$("form").submit(function() {
				return false;
			});
			
	var DomFactory = {
		
		$mdlIDSel   : $("#mdlIDSel"),
		$pathIDSel  : $("#pathIDSel"),
		$pathVerSel : $("#pathVerSel"),
		$opeNOSel   : $("#opeNOSel"),
		$opeIDSel   : $("#opeIDSel"),
		$opeVerSel  : $("#opeVerSel"),
		$window     : $(window),

		Dialog : {
			pamItemDialog_toolIDSel : $("#pamItemDialog_toolIDSel"),
			pamItemDialog_mesIDSel : $("#pamItemDialog_mesIDSel")
			//pamItemDialog_boxSetCodeSel : $("#pamItemDialog_boxSetCodeSel"),
			//pamItemDialog_swhCntPosSel : $("#pamItemDialog_swhCntPosSel"),

			//pamItemDialog_swhMaxCntTxt : $("#pamItemDialog_swhMaxCntTxt"),
			//pamItemDialog_recipeTxt : $("#pamItemDialog_recipeTxt"),

			//pamItemDialog_activeFlgChk : $("#pamItemDialog_activeFlgChk"),
//			pamItemDialog_skipFlgChk : $("#pamItemDialog_skipFlgChk"),
//			pamItemDialog_gdJudgeFLgChk : $("#pamItemDialog_gdJudgeFLgChk"),
//			pamItemDialog_defJudgeFlgChk : $("#pamItemDialog_defJudgeFlgChk"),
//			pamItemDialog_swhCntAddFlgChk : $("#pamItemDialog_swhCntAddFlgChk")
		},
		
		Grid: {
			pamListDiv : $("#pamListDiv"),
			pamListGrd : $("#pamListGrd"),
			pamItemListDiv : $("#pamItemListDiv"),
			pamItemListGrd : $("#pamItemListGrd"),
			pamItemListGrd : $("#pamItemListGrd") 
		}
	};
	var VAL = {
		T_XPBISPAM : "XPBISPAM",
		T_XPBMDLDF : "XPBMDLDF",
		T_XPBISPTH : "XPBISPTH",
		T_XPBISTOL : "XPBISTOL", 
		NORMAL     : "0000000",
		YES        : "Y" ,
		NO         : "N" ,
		DISABLED_ATTR : {'disabled' : true },
		ENABLED_ATTR  : {'disabled' : false}
		
	};

	var SelectDom = {
		
		hasTxt : function($selectDomObj, txt ) {
			
			var $options, optCnt, i ;
			
			$options = $($selectDomObj.selector + " option");
			optCnt = $options.length;
			
			for (i = 0; i < optCnt; i++) {
				if ($options[i].text === txt) {
					break;
				}
			}
			
			return i < optCnt ? true : false ; 
		},
		hasValue : function($selectDomObj, val ) {
			return $( $selectDomObj.selector + " option[value='" + val + "']" ).length > 0 ? true : false ;
		},
		addSelect : function($selectDomObj, val, txt) {
			txt = ( typeof(txt) === "undefined" ? val : txt );
			if (val && !this.hasValue(val)) {
				$selectDomObj.append("<option value=" + val + ">" + txt + "</option>");
			}
			$selectDomObj.select2({
			    	theme : "bootstrap"
			    });
		},
		setSelect : function($selectDomObj, val, txt) {
			if (val) {
				//这样写是不好的,三元运算符仅仅用在条件赋值中,而不要作为if语句的替代品
				//this.hasValue($selectDomObj, val) ? this.val(val) : this.addSelect($selectDomObj, val, txt);
				if(this.hasValue($selectDomObj, val)){
					$selectDomObj.val(val) ;
				}else{
					this.addSelect($selectDomObj, val, txt);
				}
			}
		} 
	};
	var CheckBoxDom = {
		setCheckBox : function($domObj, checkFlg){
			if (checkFlg) {
				$domObj.attr("checked","true"); 
			} else {
				$domObj.removeAttr("checked");
			}
		},
		isChecked : function($domObj){
			return ($domObj.attr("checked") === "checked" ? true : false);
		}
	};
	function comSetIary(iary, prop, val) {
		if (val) {
			iary[prop] = val;
		}
	}

	DomFactory.Grid.pamListGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : $("#pamListDiv").height(),
		width : $("#pamListDiv").width() * 0.99,
		autowidth : false,
		shrinkToFit : false,
		resizable : true,
		loadonce : true,
		viewrecords : true, 
		rownumbers : true,
		rowNum : 20, 
		emptyrecords : true,
		pager: "#pamItemListPg",
		colModel : [{name : 'ope_no_fk' ,				index : 'ope_no_fk',				label : OPE_ID_TAG,				width : 60          }, 
			        {name : 'tool_id_fk',				index : 'tool_id_fk',				label : TOOL_ID_TAG,			width : 100			}, 
			        {name : 'ope_id_fk',				index : 'ope_id_fk',				label : OPE_ID_TAG,				width : 120			}, 
			        {name : 'ope_ver_fk',				index : 'ope_ver_fk',				label : OPE_VER_TAG,			width : 120			}, 
			        {name : 'mdl_typ_fk',				index : 'mdl_typ_fk',				label : '产品规格',				width : 60			}, 
			        {name : 'mdl_id_fk',				index : 'mdl_id_fk',				label : MDL_TYPE_TAG,			width : 60			}, 
			        {name : 'path_id_fk',				index : 'path_id_fk',				label : PATH_ID_TAG,			width : 60			}, 
			        {name : 'path_ver_fk',				index : 'path_ver_fk',				label : PATH_VER_TAG,			width : 60			}, 
			        {name : 'mdl_id_fk',				index : 'mdl_id_fk',				label : MDL_ID_TAG,			width : 60			}],
		 gridComplete:function(){
//	          var gridPager,
//	              pageLen;
//	          gridPager = $(this).jqGrid("getGridParam","pager");
//	          if(gridPager.length<2){
//	             return false;
//	          }
//	          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
//	          $(".ui-pg-input").hide();
//	          $('td[dir="ltr"]').hide(); 
//	          $(gridPager+"_left").hide();
//	          $(gridPager+"_center").css({width:0});
    	},	        
		onSelectRow : function(id) {
			var rowData, iary, inObj, outObj, oary, tblCnt, i;

			rowData = $(this).jqGrid("getRowData", id);
			iary = {};

			$("#pamConditionForm input").attr(VAL.DISABLED_ATTR);
			$("#pamConditionForm select").attr(VAL.DISABLED_ATTR);

//			comSetIary(iary, "tool_id_fk", rowData.tool_id_fk);
			comSetIary(iary, "mdl_id_fk", rowData.mdl_id_fk);
			comSetIary(iary, "path_id_fk", rowData.path_id_fk);
			comSetIary(iary, "path_ver_fk", rowData.path_ver_fk);
			comSetIary(iary, "ope_id_fk", rowData.ope_id_fk);
			comSetIary(iary, "ope_no_fk", rowData.ope_no_fk);
			comSetIary(iary, "ope_ver_fk", rowData.ope_ver_fk);

			inObj = {
				trx_id     : VAL.T_XPBISPAM,
				action_flg : "Q",
				iary       : iary,
				tbl_cnt    : 1
			};
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
				$("#pamConditionForm select").empty();
				tblCnt = outObj.tbl_cnt;
				if (tblCnt > 0) {
					setGridInfo(outObj.oary, "#pamItemListGrd", true);
					oary = tblCnt > 1 ? outObj.oary[0] : outObj.oary;
					SelectDom.addSelect(DomFactory.$mdlIDSel, oary.mdl_id_fk);
					SelectDom.addSelect(DomFactory.$pathIDSel, oary.path_id_fk);
					SelectDom.addSelect(DomFactory.$pathVerSel,oary.path_ver_fk);
					SelectDom.addSelect(DomFactory.$opeIDSel, oary.ope_id_fk);
					SelectDom.addSelect(DomFactory.$opeNOSel, oary.ope_no_fk);
					SelectDom.addSelect(DomFactory.$opeVerSel, oary.ope_ver_fk);
					SelectDom.addSelect(DomFactory.$pathVerSel,oary.path_ver_fk);
				}
			}

		}
	})
	// QRS 详细子项目明细
	DomFactory.Grid.pamItemListGrd.jqGrid({
				url : "",
				datatype : "local",
				mtype : "POST",
				height : $("#pamItemListDiv").height(),
				autowidth : false,
				shrinkToFit : false,
				width : $("#pamItemListDiv").width() * 0.95,
				pager : "#pamItemListPg",
				autoScroll : true,
				viewrecords : true, 
				emptyrecords : true,
//				pager : '#pamItemListPg',
				toolbar : true,
				colModel: [
							{	name : 'tool_id_fk',			index : 'tool_id_fk',			label : TOOL_ID_TAG,		width : 70			}, 
							{	name : 'recipe_id',				index : 'recipe_id',			label : RECIPE_ID_TAG,		width : 70			}, 
							{	name : 'active_flg',			index : 'active_flg',			label : ACTIVE_FLG_TAG,		width : 70			}, 
							{	name : 'skip_flg',				index : 'skip_flg',				label : SKIP_FLG_TAG,		width : 70			}, 
							{	name : 'mes_id',				index : 'mes_id',				label : MES_ID_TAG,			width : 70			}, 
							{	name : 'box_set_code',			index : 'box_set_code',			label : BOX_SET_CODE_TAG,	width : 80			}, 
							{	name : 'gd_judge_flg',			index : 'gd_judge_flg',			label : GD_JUDEG_FLG_TAG,	width : 85			}, 
							{	name : 'def_judge_flg',			index : 'def_judge_flg',		label : DEF_JUDGE_FLG_TAG,	width : 100			}, 
							{	name : 'swh_cnt_add_flg',		index : 'swh_cnt_add_flg',		label : SWH_CNT_ADD_FLG_TAG,width : 110			}, 
							{	name : 'swh_cnt_add_pos',		index : 'swh_cnt_add_pos',		label : SWH_CNT_ADD_POS_TAG,width : 100			}, 
							{	name : 'swh_max_cnt',			index : 'swh_max_cnt',			label : SWH_MAX_CNT_TAG,	width : 85			}
						  ]
			});

	// 页面高度变化时，自动调整Grid大小
	var resetJqgrid = function() {
		$(window).unbind("onresize");
		DomFactory.Grid.pamListGrd.setGridWidth($("#pamListDiv").width() * 0.95);
		DomFactory.Grid.pamListGrd.setGridHeight($("#pamListDiv").height() * 0.80);
		$(window).bind("onresize", this);
	};

	function initFnc() {
		$("input").attr(VAL.DISABLED_ATTR);
		$("select").attr(VAL.DISABLED_ATTR);
		$("input").val("");
		$("select").empty();
		$("#qrsItemForm").css({
					"margin-top" : $("#qrsListDiv").height() - 50
				});
	}

	function showAddPamItemDialog(pamItemObj) {
		function pamItemDialogShowFnc(pamItemObj) {
			var iary, inObj, outObj, oary, tblCnt;

			$('#pamItemDialog  input').attr(VAL.ENABLED_ATTR);
			$('#pamItemDialog  select').attr(VAL.ENABLED_ATTR);
			$('#pamItemDialog  input').val("");
			$('#pamItemDialog  select').empty();
			$('#pamItemDialog  input[type="checkbox"]').removeAttr("checked");

			addValueByDataCateFnc("#pamItemDialog_mesIDSel", "MLID", "data_ext");
			//addValueByDataCateFnc("#pamItemDialog_boxSetCodeSel", "BXSC","data_ext");
			//DomFactory.Dialog.pamItemDialog_swhCntPosSel.empty();
			//SelectDom.addSelect(DomFactory.Dialog.pamItemDialog_swhCntPosSel,"I");
			//SelectDom.addSelect(DomFactory.Dialog.pamItemDialog_swhCntPosSel,"O");
		
			if (DomFactory.$opeIDSel.val() !== "*" && DomFactory.$opeVerSel.val() !=="*") {
				iary = {
				ope_id : DomFactory.$opeIDSel.val(),
				ope_ver : DomFactory.$opeVerSel.val(),
				unit_typ : "MAIN"
				};
			}else{
				iary = {
					unit_typ : "MAIN"
				};
			}
			inObj = {
				trx_id : VAL.T_XPBISTOL,
				action_flg : "Q",
				iary : iary,
				tbl_cnt : 1
			};
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
				tblCnt = outObj.tbl_cnt;
				for (i = 0; i < tblCnt; i++) {
					oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
					SelectDom.addSelect(DomFactory.Dialog.pamItemDialog_toolIDSel, oary.tool_id);
				}
			}
			if (pamItemObj.addItem === "Y") {
				DomFactory.Dialog.pamItemDialog_toolIDSel.attr(VAL.ENABLED_ATTR);
				$("#pamItemDialog_addPamItemBtn").data("save_flg", "A");
			} else {
				$("#pamItemDialog_addPamItemBtn").data("save_flg", "U");
				
				DomFactory.Dialog.pamItemDialog_toolIDSel.attr(VAL.DISABLED_ATTR);
//				DomFactory.Dialog.pamItemDialog_mesIDSel.empty();
//				DomFactory.Dialog.pamItemDialog_boxSetCodeSel.empty();
//				DomFactory.Dialog.pamItemDialog_swhCntPosSel.empty();

			//	DomFactory.Dialog.pamItemDialog_recipeTxt.val(pamItemObj.recipe_id);
			//	DomFactory.Dialog.pamItemDialog_swhMaxCntTxt.val(pamItemObj.swh_max_cnt);
				
				SelectDom.setSelect(DomFactory.Dialog.pamItemDialog_toolIDSel,pamItemObj.tool_id_fk)
				SelectDom.setSelect(DomFactory.Dialog.pamItemDialog_mesIDSel,pamItemObj.box_set_code)
				//SelectDom.setSelect(DomFactory.Dialog.pamItemDialog_boxSetCodeSel,pamItemObj.box_set_code);
				//SelectDom.setSelect(DomFactory.Dialog.pamItemDialog_swhCntPosSel,pamItemObj.swh_cnt_add_pos);
				
				//CheckBoxDom.setCheckBox( DomFactory.Dialog.pamItemDialog_activeFlgChk , pamItemObj.active_flg === VAL.YES ? true : false );
//				CheckBoxDom.setCheckBox( DomFactory.Dialog.pamItemDialog_skipFlgChk , pamItemObj.skip_flg === VAL.YES ? true : false );
//				CheckBoxDom.setCheckBox( DomFactory.Dialog.pamItemDialog_gdJudgeFLgChk , pamItemObj.gd_judge_flg === VAL.YES ? true : false );
//				CheckBoxDom.setCheckBox( DomFactory.Dialog.pamItemDialog_defJudgeFlgChk , pamItemObj.def_judge_flg === VAL.YES ? true : false );
//				CheckBoxDom.setCheckBox( DomFactory.Dialog.pamItemDialog_swhCntAddFlgChk , pamItemObj.swh_cnt_add_flg === VAL.YES ? true : false );
			}
		}
	
		function addQrsItemFnc() {
			var iary, inObj, outObj, actionFlg, rowIDs, existFlg, i, pamRowData;
			
			var mdlId, pathId, pathVer, opeNo, opeId, opeVer, recipeId, mesId, boxSetCode, swhCntAddPos, swhMaxCnt;
			
		  	mdlId = DomFactory.$mdlIDSel.find("option:selected").html().trim();
		  	pathId = DomFactory.$pathIDSel.val().trim();
		  	pathVer = DomFactory.$pathVerSel.val().trim();
		  	opeNo = DomFactory.$opeNOSel.val().trim();
		  	opeId = DomFactory.$opeIDSel.val().trim();
		  	opeVer = DomFactory.$opeVerSel.val().trim();
		  	toolId = DomFactory.Dialog.pamItemDialog_toolIDSel.val().trim();
		  //	recipeId = DomFactory.Dialog.pamItemDialog_recipeTxt.val().trim();
		  	mesId = DomFactory.Dialog.pamItemDialog_mesIDSel.val().trim();
		  	//boxSetCode = DomFactory.Dialog.pamItemDialog_boxSetCodeSel.val().trim();
		  	//swhCntAddPos = DomFactory.Dialog.pamItemDialog_swhCntPosSel.val();
			//swhMaxCnt = DomFactory.Dialog.pamItemDialog_swhMaxCntTxt.val();
		  	
			actionFlg = $("#pamItemDialog_addPamItemBtn").data("save_flg");
			if (!actionFlg) {
				return false;
			}
			if (!toolId) {
				showErrorDialog("", "必须输入设备代码");
				return false;
			}
			
			iary = {
				mdl_id_fk :mdlId,
				path_id_fk : pathId,
				path_ver_fk :pathVer,
				ope_no_fk : opeNo,
				ope_id_fk : opeId,
				ope_ver_fk : opeVer,
				tool_id_fk :toolId,
				recipe_id : "",
				mes_id : mesId,
				box_set_code : ""
			};
			
			comSetIary( iary, "active_flg",  VAL.NO );
			comSetIary( iary, "skip_flg", VAL.NO );
			comSetIary( iary, "gd_judge_flg", VAL.NO);
			comSetIary( iary, "def_judge_flg", VAL.NO);
			comSetIary( iary, "swh_cnt_add_flg", VAL.NO );
			
			comSetIary(iary, "swh_cnt_add_pos", "I");
			comSetIary(iary, "swh_max_cnt", "");
			
			inObj = {
				trx_id : VAL.T_XPBISPAM,
				action_flg : $("#pamItemDialog_addPamItemBtn").data("save_flg"),
				iary : iary
			};
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
				if (actionFlg === "A") {
					showSuccessDialog("新增限定项目成功");
					$("#pamConditionForm input").attr(VAL.DISABLED_ATTR);
					$("#pamConditionForm select").attr(VAL.DISABLED_ATTR);
//					rowIDs = DomFactory.Grid.pamListGrd.jqGrid("getDataIDs");
//					existFlg = false;
//					for (i = 0; i < rowIDs.length; i++) {
//						pamRowData = DomFactory.Grid.pamListGrd.jqGrid("getRowData", rowIDs[i]);
//						if (iary.mdl_id_fk == pamRowData.mdl_id_fk
//							&& iary.path_id_fk == pamRowData.path_id_fk
//							&& iary.path_ver_fk == pamRowData.path_ver_fk
//							&& iary.ope_no_fk == pamRowData.ope_no_fk
//							&& iary.ope_id_fk == pamRowData.ope_id_fk
//							&& iary.ope_ver_fk == pamRowData.ope_ver_fk) {
//								
//							existFlg = true;
//						}
//					}
//					if (existFlg == false) {
//						DomFactory.Grid.pamListGrd.jqGrid("addRowData", getGridNewRowID("#pamListGrd"), iary);
//					}

				} else if (actionFlg == "U") {
					showSuccessDialog("修改限定项目成功");
					$("#pamConditionForm input").attr(VAL.DISABLED_ATTR);
					$("#pamConditionForm select").attr(VAL.DISABLED_ATTR);
				}
				
				$('#pamItemDialog').modal("hide");
				iary = {
					mdl_id_fk : mdlId,
					path_id_fk : pathId,
					path_ver_fk : pathVer,
					ope_no_fk : opeNo,
					ope_id_fk : opeId,
					ope_ver_fk : opeVer
				};
				inObj = {
					trx_id : VAL.T_XPBISPAM,
					action_flg : "Q",
					iary : iary,
					tbl_cnt : 1
				};
				outObj = comTrxSubSendPostJson(inObj);
				if (outObj.rtn_code === VAL.NORMAL) {
					setGridInfo(outObj.oary, "#pamItemListGrd", true);
				}
			}
		}
		$('#pamItemDialog').modal({
					backdrop : true,
					keyboard : false,
					show : true
				});
		$("#pamItemDialog_mesIDSel").unbind('click');
		//$("#pamItemDialog_boxSetCodeSel").unbind('click');
		//$("#pamItemDialog_swhCntPosSel").unbind('click');
		$('#pamItemDialog').unbind('shown.bs.modal');
		$("#pamItemDialog_addPamItemBtn").unbind('click');
		
		$('#pamItemDialog').bind('shown.bs.modal', pamItemDialogShowFnc(pamItemObj));
		$("#pamItemDialog_addPamItemBtn").bind('click', addQrsItemFnc);

	}
	function addPamItemFnc() {
		var pamItemObj = { addItem : "Y" };
		
		if (DomFactory.$mdlIDSel.find("option:selected").html() && DomFactory.$pathIDSel.val() && DomFactory.$pathVerSel.val()
			 && DomFactory.$opeNOSel.val() && DomFactory.$opeIDSel.val() && DomFactory.$opeVerSel.val()) {
			 	
			showAddPamItemDialog(pamItemObj);
		} else {
			showErrorDialog("", "请新增或者选择Parameter");
		}
	}
	function updPamItemFnc() {
		var selRowId ,rowData;
		if ( DomFactory.$mdlIDSel.find("option:selected").html() && DomFactory.$pathIDSel.val() && DomFactory.$pathVerSel.val()
			 && DomFactory.$opeNOSel.val() && DomFactory.$opeIDSel.val() && DomFactory.$opeVerSel.val() ){
			 	
			selRowId = DomFactory.Grid.pamItemListGrd.jqGrid("getGridParam", "selrow");
			
			if (!selRowId) {
				showErrorDialog("", "请选择需要更新的Parameter");
				return false;
			}
			rowData = DomFactory.Grid.pamItemListGrd.jqGrid("getRowData", selRowId);
			showAddPamItemDialog(rowData);
		} else {
			showErrorDialog("", "请选择需要更新的Parameter");
			return false;
		}
	}
	function delPamItemFnc() {
		var mdlId, pathId, pathVer, opeNo, opeId, opeVer, toolId;
		var selRowId, rowData, inObj, iary, outObj, oary, i;
		
		mdlId = DomFactory.$mdlIDSel.find("option:selected").html() ;
		pathId = DomFactory.$pathIDSel.val();
		pathVer = DomFactory.$pathVerSel.val();
		opeNo = DomFactory.$opeNOSel.val();
		opeId = DomFactory.$opeIDSel.val();
		opeVer = DomFactory.$opeVerSel.val();
		
		selRowId = DomFactory.Grid.pamItemListGrd.jqGrid("getGridParam", "selrow");
		if (!selRowId) {
			showErrorDialog("", "请选择需要删除的Parameter");
			return false;
		}
		rowData = DomFactory.Grid.pamItemListGrd.jqGrid("getRowData", selRowId);
		toolId = rowData.tool_id_fk;
		$("#del_pamItem_btn").showCallBackWarnningDialog({
			errMsg : "是否删除Parameter项目,请确认!!!!",
			callbackFn : function(data) {
				if (data.result === true) {
					iary = {
						mdl_id_fk : mdlId,
						path_id_fk : pathId,
						path_ver_fk : pathVer,
						ope_no_fk : opeNo,
						ope_id_fk : opeId,
						ope_ver_fk : opeVer,
						tool_id_fk : toolId
					};
					inObj = {
						trx_id : VAL.T_XPBISPAM,
						action_flg : 'D',
						iary : iary
					};
					outObj = comTrxSubSendPostJson(inObj);
					if ( outObj.rtn_code === VAL.NORMAL ) {
						showSuccessDialog("删除Parameter成功");
						DomFactory.Grid.pamItemListGrd.jqGrid("delRowData", selRowId);
					}
				}
			}
		})
	}

	function f1QueryFnc() {
		/** * 将Div实例化为modal窗体 ** */
		function diaLogQueryFnc() {
			var iary = {},inObj,outObj;
			
			comSetIary(iary, "mdl_id_fk", $("#queryPamDialog_mdlIDTxt").val());
			comSetIary(iary, "mdl_typ_fk", $("#queryPamDialog_mdlTypeTxt").val());
			comSetIary(iary, "path_id_fk", $("#queryPamDialog_pathIDTxt").val());
			comSetIary(iary, "path_ver_fk", $("#queryPamDialog_pathVerTxt").val());
			comSetIary(iary, "ope_no_fk", $("#queryPamDialog_opeNOTxt").val());
			comSetIary(iary, "ope_id_fk", $("#queryPamDialog_opeIDTxt").val());
			comSetIary(iary, "ope_ver_fk", $("#queryPamDialog_opeVerTxt").val());
			comSetIary(iary, "tool_id_fk", $("#queryPamDialog_toolIDTxt").val());
			inObj = {
				trx_id : VAL.T_XPBISPAM,
				action_flg : 'F',
				iary : iary,
				tbl_cnt : 1
			};
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
				setGridInfo(outObj.oary, "#pamListGrd", true);
				$('#queryPamDialog').modal("hide");
			}
		}

		$('#queryPamDialog').modal({
					backdrop : true,
					keyboard : false,
					show : false
				});
		$('#queryPamDialog').unbind('shown.bs.modal');
		$("#queryPamDialog_queryBtn").unbind('click');

		$('#queryPamDialog').bind('shown.bs.modal');
		$('#queryPamDialog').modal("show");
		$("#queryPamDialog_queryBtn").bind('click', diaLogQueryFnc);
		$("#queryPamDialog input").attr(VAL.ENABLED_ATTR);
		$('#queryPamDialog input').val("");
	}

	function f4DeleteFnc() {
		var inObj,outObj;
		var selectID = DomFactory.Grid.pamListGrd.jqGrid("getGridParam", "selrow");
		if (!selectID) {
			showErrorDialog("", "请选择需要删除的Parameter");
			return false;
		}
		var rowData = DomFactory.Grid.pamListGrd.jqGrid("getRowData", selectID);
		$("#f4_del_btn").showCallBackWarnningDialog({
					errMsg : "是否删除Parameter,请确认!!!!",
					callbackFn : function(data) {
						if (data.result === true) {

							var iary = {};
							iary.mdl_id_fk = rowData.mdl_id_fk;
							iary.mdl_typ_fk = rowData.mdl_typ_fk;
							iary.path_id_fk = rowData.path_id_fk;
							iary.path_ver_fk = rowData.path_ver_fk;
							iary.ope_id_fk = rowData.ope_id_fk;
							iary.ope_no_fk = rowData.ope_no_fk;
							iary.ope_ver_fk = rowData.ope_ver_fk;
							inObj = {
								trx_id : VAL.T_XPBISPAM,
								action_flg : "C",
								iary : iary,
								tbl_cnt : 31
							};
							outObj = comTrxSubSendPostJson(inObj);
							if (outObj.rtn_code == VAL.NORMAL) {
								showSuccessDialog("删除限定成功");
								$("#qrsConditionForm select").empty();
								DomFactory.Grid.pamListGrd.jqGrid('delRowData', selectID);
								DomFactory.Grid.pamItemListGrd.jqGrid("clearGridData");
							}
						}
					}
				})

	}

	function mdlIDSelChangeFnc() {
		var iary, inObj, outObj, i, oary, tblCnt, iaryB, mdlId;

		mdlId = DomFactory.$mdlIDSel.find("option:selected").html();
		
		iary = {
			mdl_id : mdlId
		};

		if (mdlId === "*") {
			inObj = {
				trx_id:VAL.T_XPBISPTH,
				action_flg:"Q",
				iaryA : {}
			};
			outObj  = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
				DomFactory.$pathIDSel.empty();
				DomFactory.$pathVerSel.empty();
				SelectDom.addSelect(DomFactory.$pathIDSel, "*" );
				SelectDom.addSelect(DomFactory.$pathVerSel, "*");
				tblCnt = outObj.tbl_cnt_a;
				for (i = 0; i < tblCnt; i++) {
					oary = tblCnt > 1 ? outObj.oaryA[i] : outObj.oaryA;
					SelectDom.addSelect(DomFactory.$pathIDSel, oary.path_id);
					SelectDom.addSelect(DomFactory.$pathVerSel, oary.path_ver);
				}
			}
		} else {
			inObj = {
				trx_id : VAL.T_XPBMDLDF,
				action_flg : "Q",
				iary : iary
			};
			outObj  = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
				DomFactory.$pathIDSel.empty();
				DomFactory.$pathVerSel.empty();
				SelectDom.addSelect(DomFactory.$pathIDSel, "*" );
				SelectDom.addSelect(DomFactory.$pathVerSel, "*");
				tblCnt = outObj.tbl_cnt;
				for (i = 0; i < tblCnt; i++) {
					oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
					SelectDom.addSelect(DomFactory.$pathIDSel, oary.path_id_fk);
					if (!SelectDom.hasValue(DomFactory.$pathVerSel, oary.path_ver_fk)) {
						SelectDom.addSelect(DomFactory.$pathVerSel, oary.path_ver_fk);
					}
				}
			}
		}

		
//		DomFactory.$opeNOSel.empty();
//		iaryB = {
//			path_id_fk : DomFactory.$pathIDSel.val(),
//			path_ver_fk : DomFactory.$pathVerSel.val()
//		};
//		inObj = {
//			trx_id : VAL.T_XPBISPTH,
//			action_flg : "I",
//			iaryB : iaryB
//		};
//		outObj = comTrxSubSendPostJson(inObj);
//		if (outObj.rtn_code === VAL.NORMAL) {
//			tblCnt = outObj.tbl_cnt_b;
//			for (i = 0; i < tblCnt; i++) {
//		    	oary = tblCnt > 1 ? outObj.oaryB[i] : outObj.oaryB;
//		    	SelectDom.addSelect(DomFactory.$opeNOSel,oary.cr_ope_no);
//			}
//		}
		pathIdSelChangeFnc();
	}
	

	function pathIdSelChangeFnc() {
		var inObj, outObj, iaryB, oaryB, i, tblCnt,pathId,pathVer;
		
		pathId = DomFactory.$pathIDSel.val();
		pathVer = DomFactory.$pathVerSel.val();

		if (pathId === "*" || pathVer === "*") {
			return false;
		}
		iaryB = {
			path_id_fk : pathId,
			path_ver_fk : pathVer
		};
		inObj = {
			trx_id : VAL.T_XPBISPTH,
			action_flg : "I",
			iaryB : iaryB
		};
		DomFactory.$opeIDSel.empty();
		DomFactory.$opeVerSel.empty();
		DomFactory.$opeNOSel.empty();
		SelectDom.addSelect(DomFactory.$opeIDSel,"*");
		SelectDom.addSelect(DomFactory.$opeVerSel,"*");
		SelectDom.addSelect(DomFactory.$opeNOSel,"*");
		
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code === VAL.NORMAL) {
			tblCnt = outObj.tbl_cnt_b;
			for (i = 0; i < tblCnt; i++) {
				oaryB = tblCnt > 1 ? outObj.oaryB[i] : outObj.oaryB;
//				SelectDom.addSelect(DomFactory.$opeIDSel, oaryB.cr_ope_id_fk);
//				SelectDom.addSelect(DomFactory.$opeVerSel, oaryB.cr_ope_ver_fk);
				SelectDom.addSelect(DomFactory.$opeNOSel, oaryB.cr_ope_no);
			}
		}
	}
	function opeNoSelChangeFnc(){
		
		var inObj, outObj, iaryB, oaryB, i, tblCnt;
		var opeNo = DomFactory.$opeNOSel.val();
		iaryB = {
			path_id_fk : DomFactory.$pathIDSel.val(),
			path_ver_fk : DomFactory.$pathVerSel.val(),
			cr_ope_no : opeNo
		};
		inObj = {
			trx_id : VAL.T_XPBISPTH,
			action_flg : "I",
			iaryB : iaryB
		};
		DomFactory.$opeIDSel.empty();
		DomFactory.$opeVerSel.empty();
		SelectDom.addSelect(DomFactory.$opeIDSel,"*");
		SelectDom.addSelect(DomFactory.$opeVerSel,"*");
		
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code === VAL.NORMAL) {
			tblCnt = outObj.tbl_cnt_b;
			for (i = 0; i < tblCnt; i++) {
				oaryB = tblCnt > 1 ? outObj.oaryB[i] : outObj.oaryB;
				SelectDom.addSelect(DomFactory.$opeIDSel, oaryB.cr_ope_id_fk);
				SelectDom.addSelect(DomFactory.$opeVerSel, oaryB.cr_ope_ver_fk);
				SelectDom.addSelect(DomFactory.$opeNOSel, oaryB.cr_ope_no);
				if (opeNo !== "*") {
					SelectDom.setSelect(DomFactory.$opeIDSel,oaryB.cr_ope_id_fk);
					SelectDom.setSelect(DomFactory.$opeVerSel,oaryB.cr_ope_ver_fk);
				}
			}
		}
	
	}
	
			
	function f6AddFnc() {
		var inObj, outObj, i, tblCnt, oary;
		
		$("#pamConditionForm input").attr(VAL.ENABLED_ATTR);
		$("#pamConditionForm select").attr(VAL.ENABLED_ATTR);
		$("#pamConditionForm input").val("");
		$("#pamConditionForm select").empty();
		DomFactory.Grid.pamItemListGrd.jqGrid("clearGridData");
		
		inObj = {
			trx_id : VAL.T_XPBMDLDF,
			action_flg : "Q",
			iary : {}
		};
		outObj = comTrxSubSendPostJson(inObj);
		tblCnt = outObj.tbl_cnt;
		
		SelectDom.addSelect(DomFactory.$mdlIDSel,"*");
		SelectDom.addSelect(DomFactory.$pathIDSel,"*");
		SelectDom.addSelect(DomFactory.$pathVerSel,"*");
		SelectDom.addSelect(DomFactory.$opeNOSel,"*");
		SelectDom.addSelect(DomFactory.$opeIDSel,"*");
		SelectDom.addSelect(DomFactory.$opeVerSel,"*");
		for (i = 0; i < tblCnt; i++) {
			oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
			SelectDom.addSelect(DomFactory.$mdlIDSel, oary.mdl_id);
		}
		mdlIDSelChangeFnc();
	}

	function f10ClearFnc() {
		$("input").val("");
		$("select").empty();
		DomFactory.Grid.pamListGrd.jqGrid("clearGridData");
		DomFactory.Grid.pamItemListGrd.jqGrid("clearGridData");
	}
	
	DomFactory.$mdlIDSel.change(mdlIDSelChangeFnc);
	DomFactory.$pathIDSel.change(pathIdSelChangeFnc);
	DomFactory.$pathVerSel.change(pathIdSelChangeFnc);
	DomFactory.$opeNOSel.change(opeNoSelChangeFnc);
	
	$("#add_pamItem_btn").click(addPamItemFnc);
	$("#upd_pamItem_btn").click(updPamItemFnc);
	$("#del_pamItem_btn").click(delPamItemFnc);

	$("#f1_query_btn").click(f1QueryFnc);
	$("#f6_add_btn").click(f6AddFnc);
	$("#f4_del_btn").click(f4DeleteFnc);
	$("#f10_clear_btn").click(f10ClearFnc);

	initFnc();
	
	function resizeFnc(){        
    	var offsetBottom, divWidth;        
    	
		offsetBottom = DomFactory.$window.height() - DomFactory.Grid.pamListDiv.offset().top;
		DomFactory.Grid.pamListGrd.setGridHeight(offsetBottom * 0.99 - 30);
		
		divWidth = DomFactory.$window.width() - DomFactory.Grid.pamItemListDiv.offset().left;
		DomFactory.Grid.pamItemListGrd.setGridWidth(divWidth * 0.99 -25);
		offsetBottom = DomFactory.$window.height() - DomFactory.Grid.pamItemListDiv.offset().top;
		DomFactory.Grid.pamItemListGrd.setGridHeight(offsetBottom * 0.99 - 61);
    };                                                                                         
    resizeFnc(); 
    var divWidth;
    divWidth=DomFactory.Grid.pamListDiv.width();
	DomFactory.Grid.pamListDiv.width=divWidth * 1.3;
	DomFactory.Grid.pamListGrd.setGridWidth(divWidth * 1.3);
    DomFactory.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	}); 
})
