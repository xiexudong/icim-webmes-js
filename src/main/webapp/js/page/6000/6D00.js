
  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Layout Group Management                               */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/07/03 NO.00   Lin.Xin      Initial release                        */
  /*                                                                        */
  /**************************************************************************/
$(document).ready(function() {
	var controlsQuery = {
        W                  : $(window)      ,
        userID             : $("#userIDTxt"),
        layotgIDTxt        : $("#layotgIDTxt"),
        layotgDscTxt       : $("#layotgDscTxt"),
        layotgCateSel      : $("#layotgCateSel"),
        layotIDSel         : $("#layotIDSel"),
        querylayotgID      : $("#querylayotgDialog_layotgIDTxt"),
        querylayotgDialog  : $("#querylayotgDialog"),
        mainGrd   :{
            grdId     : $("#layotgListGrd")   ,
            grdPgText : "#layotgListPg"       ,
            fatherDiv : $("#layotgListDiv")
        },
        layotGrd:{
        	grdId     : $("#layotListGrd"),
        	grdPgText : "#layotListPg"    ,
        	fatherDiv : $("#layotListDiv")
        }
	};
	var VAL = {
		NORMAL     : "0000000"  ,
        EVT_USER   : $("#userId").text(),
        T_XPLAYOUT : "XPLAYOUT"
	};
	var btnQuery={
		f1 : $("#f1_query_btn"),
		f4 : $("#f4_del_btn"),
		f5 : $("#f5_upd_btn"),
		f6 : $("#f6_add_btn"),
		f9 : $("#f9_save_btn"),
		f10: $("#f10_clear_btn"),
		add: $("#add_layot_btn"),
		del: $("#delete_layot_btn"),
		query : $("#querylayotgDialog_queryBtn"),
		cancel: $("#querylayotgDialog_cancelBtn")
	};
	var toolFunc ={
		iniCateSel : function(){
    		var Cate = ['S','M','L'];
    		for( var i = 0 ;i<Cate.length;i++){
				controlsQuery.layotgCateSel.append("<option value="+ Cate[i] +">" +Cate[i] +"</option>");
			}
    		controlsQuery.layotgCateSel.select2({
       	    	theme : "bootstrap"
       	    });
    	},
    	
		initFnc : function(){
			$("input").val("");
			$("select").val("").trigger("change");;
			$("input").val();
    		$(".input").attr("disabled",true);
    		$("select").attr("disabled",true);
    		btnQuery.add.attr("disabled",true);
    		btnQuery.add.fadeTo("fast",0.25);
    		btnQuery.del.attr("disabled",true);
    		btnQuery.del.fadeTo("fast",0.25);
			controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
			controlsQuery.layotGrd.grdId.jqGrid("clearGridData");
			toolFunc.iniCateSel();
		},
		iniLayotProc : function(){
			var inObj,outObj;
			var iary = {
				layot_flg  : 'S'
			};
			inObj = {
				trx_id : VAL.T_XPLAYOUT,
				action_flg : 'Q',
				iary : iary
			};
			outObj = comTrxSubSendPostJson(inObj);
			if(outObj.rtn_code == VAL.NORMAL){
				_setSelectDate(outObj.tbl_cnt, outObj.oary, "layot_id", "layot_id", "#layotIDSel", true);
			}
		},
		layotDialogShow:function(){
			controlsQuery.querylayotgID.val("");
			controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
		},
		ListLayotGroup:function(){
			var inObj,outObj;
			var layot_id = controlsQuery.querylayotgID.val();
			var iary = {
				layot_flg : 'G',
			};
			if(layot_id){
				iary.layot_id = layot_id;
			};
			inObj = {
				trx_id : VAL.T_XPLAYOUT,
				action_flg : 'Q',
				iary : iary
			};
			outObj = comTrxSubSendPostJson(inObj);
			if(outObj.rtn_code == VAL.NORMAL){
				setGridInfo(outObj.oary, "#layotgListGrd" );
				controlsQuery.querylayotgDialog.modal("hide");
			}
		},
		
    	QueryLayotgInfo : function(layot_id){
    		var inObj,outObj;
    		var iary = {
				layot_id  : layot_id
			}
			inObj = {
				trx_id : VAL.T_XPLAYOUT,
				action_flg : 'Q',
				iary : iary
			};
    		outObj = comTrxSubSendPostJson(inObj);
			if(outObj.rtn_code == VAL.NORMAL){
				setGridInfo(outObj.oary.oaryB, "#layotListGrd" );
				controlsQuery.layotgIDTxt.val(outObj.oary.layot_id);
				controlsQuery.layotgDscTxt.val(outObj.oary.layot_dsc);
				controlsQuery.layotgCateSel.val(outObj.oary.layot_cate).trigger("change");;
			}
    	}
	};
	var btnFunc = {
    	query_func : function(){
    		toolFunc.layotDialogShow();
    		controlsQuery.querylayotgDialog.modal({
                backdrop:true,
                keyboard:false,
                show:false
             });
    		controlsQuery.querylayotgDialog.unbind('shown.bs.modal');
    		btnQuery.query.unbind('click');
            $('[rel=tooltip]').tooltip({
                placement:"bottom",
                trigger:"focus",
                animation:true
            });
            controlsQuery.querylayotgDialog.bind('shown.bs.modal',toolFunc.layotDialogShow);
            controlsQuery.querylayotgDialog.modal("show");
            btnQuery.query.bind('click',toolFunc.ListLayotGroup);
            controlsQuery.querylayotgID.focus();
    	},
    	f4_func : function(){
    		var layotg_id = controlsQuery.layotgIDTxt.val();
    		if(!layotg_id){
    			showErrorDialog("","请选择需要删除的版式组!");
	        	return false;
    		}
    		var iary={
    			layot_id  : layotg_id
    		};
    		inObj={
    			trx_id     : VAL.T_XPLAYOUT,
    			action_flg : 'D',
    			iary       : iary
    		};
    		outObj = comTrxSubSendPostJson(inObj);
    		if(outObj.rtn_code == VAL.NORMAL){
    			showSuccessDialog("删除成功"); 
    			toolFunc.initFnc();
    			return;
    		}
    	},
    	f5_func : function(){
    		var layot_id = controlsQuery.layotgIDTxt.val();
    		if(!layot_id){
    			showErrorDialog("","请选择需要修改的版式组!");
	        	return false;
    		}
    		controlsQuery.layotgDscTxt.attr("disabled",false);
    		$("select").attr("disabled",false);
    		btnQuery.add.attr("disabled",false);
    		btnQuery.add.fadeTo("fast",1.00);
    		btnQuery.del.attr("disabled",false);
    		btnQuery.del.fadeTo("fast",1.00);
    		btnQuery.f9.data("save_flg","U");
    	},
    	f6_func : function(){
    		$(".input").val("");
    		$(".input").attr("disabled",false);
    		$("select").attr("disabled",false);
    		controlsQuery.layotGrd.grdId.jqGrid("clearGridData");
    		controlsQuery.layotgIDTxt.focus();
    		btnQuery.add.attr("disabled",false);
    		btnQuery.add.fadeTo("fast",1.00);
    		btnQuery.del.attr("disabled",false);
    		btnQuery.del.fadeTo("fast",1.00);
    		btnQuery.f9.data("save_flg","A");
    	},
    	f9_func : function(){
    		var layotg_id,
    			layotg_dsc,
    			layot_cate,
    			layot_flg;
    		var rowIds,
    			crGrid;
    		var action_flg = btnQuery.f9.data("save_flg");
    		var inObj,outObj;
    		var iaryB = new Array();
    		layot_flg = "G",
    		crGrid = controlsQuery.layotGrd.grdId;
    		rowIds = crGrid.jqGrid('getDataIDs');
    		layotg_id = controlsQuery.layotgIDTxt.val();
    		layotg_dsc = controlsQuery.layotgDscTxt.val();
    		layot_cate = controlsQuery.layotgCateSel.val();
    		if(!layotg_id){
    			showErrorDialog("","请输入版式组代码!");
	        	return false;
    		}
    		if(!layot_cate){
    			showErrorDialog("","请选择版式规格!");
	        	return false;
    		}
    		for( var i=0;i<rowIds.length;i++){
    			rowData = crGrid.jqGrid('getRowData',rowIds[i]);
    			var layout_info={
    				layotg_id_fk : layotg_id          ,
    				layot_id_fk  : rowData.layot_id_fk,
    				x_axis       : rowData.x_axis     ,
    				y_axis       : rowData.y_axis
    			};
    			iaryB.push(layout_info);
    		}
    		var iary={
    				layot_id  : layotg_id ,
        			layot_dsc : layotg_dsc,
        			layot_cate: layot_cate,
        			layot_flg : layot_flg ,
        			iaryB     : iaryB
    		};
    		inObj={
    			trx_id     : VAL.T_XPLAYOUT,
    			action_flg : 'N',
    			iary       : iary
    		};
    		outObj = comTrxSubSendPostJson(inObj);
    		if(outObj.rtn_code == VAL.NORMAL){
    			if(action_flg == "A"){
    				showSuccessDialog("新增版式组信息成功"); 
    			}else if(action_flg == "U"){
    				showSuccessDialog("更新版式组信息成功"); 
    			}
    			btnQuery.add.attr("disabled",true);
        		btnQuery.add.fadeTo("fast",0.25);
        		btnQuery.del.attr("disabled",true);
        		btnQuery.del.fadeTo("fast",0.25);
        		$(".input").attr("disabled",true);
        		$("select").attr("disabled",true);
        		toolFunc.ListLayotGroup();
        		return false;
    		}
    	},
    	f10_func : function(){
    		$("input").val("");
    		$("select").val("");
    		controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
			controlsQuery.layotGrd.grdId.jqGrid("clearGridData");
    	},
    	add_layot_func:function(){
    		var inObj,outObj;
    		var layout_id="",i;
    		var rowIds,crGrid;
    		layout_id = controlsQuery.layotIDSel.val();
    		crGrid = controlsQuery.layotGrd.grdId;
    		rowIds = crGrid.jqGrid('getDataIDs');
    		if(!layout_id){
    			showErrorDialog("","请选择版式代码!");
	        	return false;
    		}
    		for( i=0;i<rowIds.length;i++){
    			rowData = crGrid.jqGrid('getRowData',rowIds[i]);
    			if( $.trim(layout_id) == rowData.layot_id_fk){
    				return false;
    			}
    		}
    		var iary={
    			layot_id : layout_id
    		};
    		inObj = {
    			trx_id : VAL.T_XPLAYOUT,
    			action_flg : 'Q',
    			iary : iary
    		};
    		outObj = comTrxSubSendPostJson(inObj);
    		if(outObj.rtn_code == VAL.NORMAL){
    			var new_row_id = getGridNewRowID("#layotListGrd");
    			var data={
    				layot_id_fk : outObj.oary.layot_id,
    				layot_dsc   : outObj.oary.layot_dsc,
    				x_axis      : outObj.oary.x_axis_cnt,
    				y_axis      : outObj.oary.y_axis_cnt
    			};
    			crGrid.jqGrid("addRowData",new_row_id,data);
    		}
    	},
    	del_layot_func:function(){
    		var crGrid = controlsQuery.layotGrd.grdId;
    		var selectID = crGrid.jqGrid("getGridParam","selrow");
		    if( selectID ==null){
		        showErrorDialog("003","请选择需要删除的行");
		        return false;
		    }
		    $("#delete_layot_btn").showCallBackWarnningDialog({
		        errMsg  : "是否删除版式,请确认!!!!",
		        callbackFn : function(data) {
		          if(data.result==true){
		        	var rowData = crGrid.jqGrid('getRowData',selectID);
		            var layot_id = rowData.layot_id_fk;
		              $("#layotListGrd").jqGrid('delRowData', selectID );
		            }
		          }
		      });
    	}
	};
	var iniGridInfo = { 
		iniLayotgGridInfo:function(){
    		var grdInfoCM = [
 	            {name: 'layot_id'       , index: 'layot_id'    , label: LAYOUT_G_ID_TAG    , width: 100 },
     			{name: 'layot_dsc'      , index: 'layot_dsc'   , label: LAYOUT_G_DSC_TAG   , width: 130 }
     		];
 	        controlsQuery.mainGrd.grdId.jqGrid({
 	              url:"",
 			      datatype:"local",
 			      mtype:"POST",
 			      autowidth:true,//宽度根据父元素自适应
 			      height:500,
 			      shrinkToFit:false,
 			      scroll:false,
 			      resizable : true,
 			      loadonce:true,
 			      fixed: true,
 			      jsonReader : {
 			            // repeatitems: false
 			          },
 			      viewrecords : true, //显示总记录数
 			      pager : '#userListPg',
 			      rownumbers  :true ,//显示行号
 			      rowNum:25,         //每页多少行，用于分页
 			      rownumWidth : 20,  //行号列宽度
 			      emptyrecords :true ,
 			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
 			      colModel: grdInfoCM,
 			      gridComplete:function(){
 			          var gridPager,
 			              pageLen;
 			          gridPager = $(this).jqGrid("getGridParam","pager");
 			          if(gridPager.length<2){
 			             return false;
 			          }
 			          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
 			          $(".ui-pg-input").hide();
 			          $('td[dir="ltr"]').hide(); 
 			          $(gridPager+"_left").hide();
 			          $(gridPager+"_center").css({width:0});
 			      },
 		          onSelectRow:function(id){
 			         $(".input").attr({"disabled":true});
 			         $("select").attr({"disabled":true});
 			         var rowData = $(this).jqGrid("getRowData",id);
 			           toolFunc.QueryLayotgInfo(rowData['layot_id']);
 		          }
 	        });
    	},
    	iniLayotGridInfo:function(){
    		var grdInfoCM = [
  	            {name: 'layot_id_fk'    , index: 'layot_id_fk'   , label: LAYOUT_ID_TAG    , width: 120 },
      			{name: 'layot_dsc'      , index: 'layot_dsc'     , label: LAYOUT_DSC_TAG   , width: 120 },
  	            {name: 'x_axis'         , index: 'x_axis'        , label: X_AXIS_CNT_TAG   , width: 100 },
  	            {name: 'y_axis'         , index: 'y_axis'        , label: Y_AXIS_CNT_TAG   , width: 80 }
      		];
  	        controlsQuery.layotGrd.grdId.jqGrid({
  	              url:"",
  			      datatype:"local",
  			      mtype:"POST",
	  			  autowidth:true,//宽度根据父元素自适应
  			      height:200,
  			      shrinkToFit:false,
  			      scroll:false,
  			      resizable : true,
  			      loadonce:true,
  			      fixed: true,
  			      // hidedlg:true,
  			      jsonReader : {
  			            // repeatitems: false
  			          },
  			      viewrecords : true, //显示总记录数
  			      pager : '#userListPg',
  			      rownumbers  :true ,//显示行号
  			      rownumWidth : 20,  //行号列宽度
  			      rowNum:500,         //每页多少行，用于分页
  			      emptyrecords :true ,
  			      toolbar : [true, "top"],//显示工具列 : top,buttom,both
  			      colModel: grdInfoCM
  	        });
    	}
    };
	var iniButtonAction = function(){
        btnQuery.f1.click(function(){
            btnFunc.query_func();
        });
        btnQuery.f5.click(function(){
            btnFunc.f5_func();
        });
        btnQuery.f6.click(function(){
            btnFunc.f6_func();
        });
        btnQuery.f9.click(function(){
            btnFunc.f9_func();
        });
        btnQuery.f10.click(function(){
            btnFunc.f10_func();
        });
        btnQuery.add.click(function(){
        	btnFunc.add_layot_func();
        });
        btnQuery.del.click(function(){
        	btnFunc.del_layot_func();
        });
        btnQuery.f4.click(function(){
        	btnFunc.f4_func();
        });
	};
	var initializationFunc = function(){
		iniGridInfo.iniLayotgGridInfo();
        iniGridInfo.iniLayotGridInfo();
        iniButtonAction();
        toolFunc.initFnc();
        toolFunc.iniLayotProc();
        $("form").submit(function(){
            return false;
        });
    };
    initializationFunc();
});