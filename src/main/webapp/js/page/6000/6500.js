
  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  Bis_ope Management                                   */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2013/04/28 N0.00   Lin.Xin      Initial release                        */
  /*                                                                        */
  /**************************************************************************/

$(document).ready(function() {
  var _NORMAL = "0000000";
  $("form").submit(function(){
      return false;
  });
  var itemInfoCM = [
    {name: 'ope_id'       , index: 'ope_id'     , label: OPE_ID_TAG     , width: 60 },
    {name: 'ope_ver'      , index: 'ope_ver'    , label: OPE_VER_TAG    , width: 60 },
    {name: 'ope_dsc'      , index: 'ope_dsc'    , label: "站点描述"   , width: 60 },
  ];
  $("#opeListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:$("#opeListDiv").height(),
      // height:400,//TODO:需要根据页面自适应，要相对很高
      // width:"99%",
      autowidth:true,//宽度根据父元素自适应
      shrinkToFit:true,
      scroll:false,
      resizable : true,
      loadonce:true,
      // toppager: true , 翻页控件是否在上层显示
      fixed: true,
      // hidedlg:true,
      jsonReader : {
            // repeatitems: false
          },
      viewrecords : true, //显示总记录数
      pager : '#opeListPg',
      rownumbers  :true ,//显示行号
      rowNum:25,         //每页多少行，用于分页
      rownumWidth : 20,  //行号列宽度
      emptyrecords :true ,
      // pginput  : true ,//可以输入跳转页数
      // toolbar : [true, "top"],//显示工具列 : top,buttom,both
      colModel: itemInfoCM,
      gridComplete:function(){
          var gridPager,
              pageLen;
          gridPager = $(this).jqGrid("getGridParam","pager");
          if(gridPager.length<2){
             return false;
          }
          $("#sp_1_"+gridPager.substr(1,pageLen-1) ).hide();
          $(".ui-pg-input").hide();
          $('td[dir="ltr"]').hide(); 
          $(gridPager+"_left").hide();
          $(gridPager+"_center").css({width:0});
      },
      onSelectRow:function(id){
        $("select").attr({"disabled":true});
        $("input").attr({"disabled":true});
        var rowData = $(this).jqGrid("getRowData",id);
        var ope_id  = rowData.ope_id;
        var ope_ver = rowData.ope_ver;
        var iary = {
          ope_id:ope_id,
          ope_ver:ope_ver
        };
        var inTrxObj ={
          trx_id : "XPBISOPE",
          action_flg:"Q",
          iary   : iary,
          tbl_cnt: 1
        };
        var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if(  outTrxObj.rtn_code == _NORMAL ) {
          var oary = outTrxObj.oary;
          $("#opeIDTxt").val(oary.ope_id);
          $("#opeVerTxt").val(oary.ope_ver);
          $("#opeDscTxt").val(oary.ope_dsc);
          $("#procIDSel").empty();
          if(oary.proc_id!=undefined){
            $("#procIDSel").append("<option value="+oary.proc_id+">" + oary.proc_id + "</option>");  
            $("#procIDSel").select2({
		    	theme : "bootstrap"
		    });
          }
          $("#toolgIDSel").empty();
          if(oary.toolg_id!=undefined){
            $("#toolgIDSel").append("<option value="+oary.toolg_id+">" + oary.toolg_id + "</option>");  
            $("#toolgIDSel").select2({
		    	theme : "bootstrap"
		    });
          }
          
          $("#pepLvlTxt").val(oary.pep_lvl);
          $("#deptIDSel").empty();
          if(oary.dept_id_data_id_fk!=undefined){
            $("#deptIDSel").append("<option value="+oary.dept_id_data_id_fk+">" + oary.dept_id_data_id_fk + "</option>");  
            $("#deptIDSel").select2({
		    	theme : "bootstrap"
		    });
          }
          $("#stbOpeTimeTxt").val(oary.std_ope_time);
        }

      } 
  })
  $("#opeListGrd").jqGrid('navGrid','#opeListPg',{ del:false,add:false,edit:false},{},{},{},{multipleSearch:true});
  //页面高度变化时，自动调整Grid大小
  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#opeListGrd").setGridWidth($("#opeListDiv").width()*0.95); 
      $("#opeListGrd").setGridHeight($("#opeListDiv").height()*0.80);     
      $(window).bind("onresize", this);  
  };
  function initFnc(){
    $("input").attr({'disabled':true});
    $("select").attr({'disabled':true});
    $("input").val("");
    $("select").empty();
    $("checkbox").removeAttr("checked");
  }

  function f1QueryFnc(){

    /*** 将Div实例化为modal窗体 ***/
    function diaLogQueryFnc(){
      var iary = {};
        // ope_id  : $("#queryOpeDialog_opeIDTxt").val() ,
        // ope_ver : $("#queryOpeDialog_opeVerTxt").val()
      // };
      if($("#queryOpeDialog_opeIDTxt").val()!=""){
        iary.ope_id  = $("#queryOpeDialog_opeIDTxt").val() ;
      }
      if($("#queryOpeDialog_opeVerTxt").val()!=""){
        iary.ope_ver = $("#queryOpeDialog_opeVerTxt").val();
      }
      var inTrxObj ={
         trx_id     : "XPBISOPE" ,
         action_flg : 'Q'        ,   
         iary       : iary       ,
         tbl_cnt    : 1
      };
      var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(  outTrxObj.rtn_code == _NORMAL ) {
         setGridInfo(outTrxObj.oary,"#opeListGrd");
         $('#queryOpeDialog').modal("hide");
      }
    }
    function mdlDefDialogShowFnc(){

    }
    $('#queryOpeDialog').modal({
        backdrop:true,
        keyboard:false,
        show:false
    });
    $('#queryOpeDialog').unbind('shown.bs.modal');
    $("#queryOpeDialog_queryBtn").unbind('click');

    $('#queryOpeDialog').bind('shown.bs.modal',mdlDefDialogShowFnc);
    $('#queryOpeDialog').modal("show");
    $("#queryOpeDialog_queryBtn").bind('click',diaLogQueryFnc);
    $("#queryOpeDialog_opeIDTxt").attr({'disabled':false});
    $("#queryOpeDialog_opeVerTxt").attr({'disabled':false});

  }

  $("#f1_query_btn").click(f1QueryFnc);
  initFnc();
  //显示输入提醒
  $('[rel=tooltip]').tooltip({
    placement:"bottom",
    trigger:"focus",
    animation:true,

  });
  function isEmpty(str){
    if(str==null||str==undefined||str==""){
      return true;
    }
    return false;
  }
  function f9SaveFnc(){
    var action_flg = $("#f9_save_btn").data("save_flg");
    if(action_flg==undefined){
      return false;
    }
  //   private String ope_id;
    var ope_id   = $("#opeIDTxt").val().trim();
    var ope_ver  = $("#opeVerTxt").val().trim();
    var ope_dsc  = $("#opeDscTxt").val().trim();
    var proc_id  = $("#procIDSel").find("option:selected").text();
    var toolg_id = $("#toolgIDSel").find("option:selected").text();
    var pep_lvl  = $("#pepLvlTxt").val().trim();
    var dept_id  = $("#deptIDSel").find("option:selected").text();
    var std_ope_time =$("#stbOpeTimeTxt").val().trim();
    var yield_chk_flg = "N";
    var hand_over_flg = "N";
    if(isEmpty(ope_id)){
      showErrorDialog("003","必须输入站点信息");
      return false;
    }
    if(isEmpty(ope_ver)){
      showErrorDialog("003","必须输入站点版本");
      return false; 
    }
    var iary ={
        ope_id             : ope_id         ,
        ope_ver            : ope_ver        
    };
    if(ope_dsc!=""){
      iary.ope_dsc            = ope_dsc        ;
    }
    if(proc_id!=""){
      iary.proc_id            = proc_id        ;
    }
    if(toolg_id!=""){
      iary.toolg_id           = toolg_id       ;   
    }   
    if(pep_lvl!=""){
      iary.pep_lvl            = pep_lvl        ;   
    }
    if(yield_chk_flg!=""){
      iary.yield_chk_flg      = yield_chk_flg  ;   
    }
    if(hand_over_flg!=""){
      iary.hand_over_flg      = hand_over_flg  ;
    }
    if(dept_id!=""){
      iary.dept_id_data_id_fk = dept_id        ; 
    }
    if(std_ope_time!=""){
      iary.std_ope_time       = std_ope_time   ;
    }
    var inTrxObj = {
          trx_id     : 'XPBISOPE' ,
          action_flg : action_flg ,
          iary       : iary       ,
          tbl_cnt    : 1
    }
    var outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if(outTrxObj.rtn_code == "0000000") {
      if(action_flg=="A"){
        showSuccessDialog("新增站点信息成功"); 
        var rowIDs = $("#opeListGrd").jqGrid("getDataIDs");
        var addRowID = getGridNewRowID("#opeListGrd");
        $("#opeListGrd").jqGrid("addRowData",addRowID,iary);
      }else if(action_flg=="U"){
        showSuccessDialog("站点信息更新成功");  
      }
    }
  };
  $("#f9_save_btn").click(f9SaveFnc);
  function f6AddFnc(){
    $("input").attr({'disabled':false});
    $("select").attr({'disabled':false});
    $("input").val("");
    $("select").empty();
    $("checkbox").removeAttr("checked");
    addValueByDataCateFnc("#procIDSel","PRST","data_ext");
    addValueByDataCateFnc("#deptIDSel","DEPT","data_ext");
    addValueByDataCateFnc("#toolgIDSel","TOLG","data_item");
    $("#f9_save_btn").data("save_flg","A");
  }
  function f5UpdateFnc(){
    $("input").attr({'disabled':false});
    $("select").attr({'disabled':false});
    $("#opeIDTxt").attr({'disabled':true});
    $("#opeVerTxt").attr({'disabled':true});
    addValueByDataCateFnc("#procIDSel","PRST","data_ext");
    addValueByDataCateFnc("#deptIDSel","DEPT","data_ext");
    addValueByDataCateFnc("#toolgIDSel","TOLG","data_item");
    $("#f9_save_btn").data("save_flg","U");
  }
  function f4DeleteFnc(){
    var selectRowId ,
        rowData,
        iary,
        inTrxObj,
        outTrxObj;
    selectRowId = $("#opeListGrd").jqGrid("getGridParam","selrow");
    if(selectRowId==""||selectRowId==null){
      showErrorDialog("003","请选择需要删除的站点");
      return false;
    } 
    rowData = $("#opeListGrd").jqGrid("getRowData",selectRowId);
    $("#f1_query_btn").showCallBackWarnningDialog({
      errMsg  : "是否删除站点:["+ rowData.ope_id  + "],请确认!!!!",
      callbackFn : function(data) {
        if(data.result==true){
          
          iary  ={
              ope_id : rowData.ope_id,
              ope_ver: rowData.ope_ver
          };
          inTrxObj={
              trx_id     : "XPBISOPE",
              action_flg : "D"  ,
              iary       : iary ,
              tbl_cnt    : 1
          };
          outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if(outTrxObj.rtn_code=="0000000"){
              showSuccessDialog("站点信息删除成功");
              $("#opeListGrd").jqGrid("delRowData",selectRowId);
              $("#opeConditionForm input").val("");
              $("#opeConditionForm select").empty();
          }
        }
      }
    })
  }
  
  function f10ClearFnc(){
    $("input").val("");
    $("select").empty();
    $("checkbox").removeAttr("checked");
    $("#opeListGrd").jqGrid("clearGridData");
  }
  $("#f4_del_btn").click(f4DeleteFnc);
  $("#f5_upd_btn").click(f5UpdateFnc);
  $("#f6_add_btn").click(f6AddFnc);
  $("#f10_clear_btn").click(f10ClearFnc);
})