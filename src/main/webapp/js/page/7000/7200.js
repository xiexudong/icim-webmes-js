/**
 * NO.01 添加卡控，输入数值超过标准上下限时 弹出错误提示.
 * NO.02 添加BY内部订单上报参数.
 */

$(document).ready(function() {
	 $("form").submit(function(){
	      return false;
	  });
	var GlobalBean = {
		NORMAL : "0000000",
		mes_id : null,
		opeKey : null,
		TX_XPBISTOL : "XPBISTOL",
		F8_KEY : 119
	};
	var VAL = {
	        T_XPINQCOD : "XPINQCOD" ,
			NORMAL     : "0000000",
	        EVT_USER   : $("#userId").text()
	};
	var domObj = {
			$window        : $(window),
			$prdSeqInfoDiv : $("#prdSeqInfoDiv"),
			$prdSeqInfoGrd : $("#prdSeqInfoGrd"),
			$grdDiv        : $("#grdDiv"),
			$itemDiv       : $("#itemDiv")
	};
	var prdItemInfoCM = [
         {name : 'prd_seq_id'    ,index : 'prd_seq_id'    ,label : PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM ,hidden:true},
         {name : 'fab_sn'        ,index : 'fab_sn'        ,label : PRD_SEQ_ID_TAG,width : PRD_SEQ_ID_CLM}, 
         {name : 'box_id_fk'     ,index : 'box_id_fk'     ,label : BOX_TAG       ,width : BOX_ID_FK_CLM}, 
         {name : 'mdl_id_fk'     ,index : 'mdl_id_fk'     ,label : BOX_TAG       ,width : MDL_ID_FK_CLM     ,hidden : true}, 
         {name : 'cr_path_id_fk' ,index : 'cr_path_id_fk' ,label : BOX_TAG       ,width : CR_PATH_ID_FK_CLM ,hidden : true}, 
         {name : 'cr_path_ver_fk',index : 'cr_path_ver_fk',label : BOX_TAG       ,width : CR_PATH_VER_FK_CLM,hidden : true}, 
         {name : 'cr_ope_no_fk'  ,index : 'cr_ope_no_fk'  ,label : BOX_TAG       ,width : CR_OPE_NO_FK_CLM  ,hidden : true}, 
         {name : 'cr_ope_id_fk'  ,index : 'cr_ope_id_fk'  ,label : BOX_TAG       ,width : CR_OPE_ID_FK_CLM  ,hidden : true}, 
         {name : 'cr_ope_ver_fk' ,index : 'cr_ope_ver_fk' ,label : BOX_TAG       ,width : CR_OPE_VER_FK_CLM ,hidden : true}, 
         {name : 'cr_proc_id_fk' ,index : 'cr_proc_id_fk' ,label : BOX_TAG       ,width : CR_PROC_ID_FK_CLM ,hidden : true},
         {name : 'wo_id_fk'      ,index : 'wo_id_fk'      ,label : BOX_TAG       ,width : CR_PROC_ID_FK_CLM ,hidden : true},
         {name : 'lot_id'        ,index : 'lot_id'        ,label : BOX_TAG       ,width : CR_PROC_ID_FK_CLM ,hidden : true}
         ];
	$("#prdSeqInfoGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 200,// TODO:需要根据页面自适应，要相对很高
		autowidth : true,// 宽度根据父元素自适应
		shrinkToFit : true,
		scroll : true,
		viewrecords : true,
		rownumbers : true,
		rowNum : 20,
		rownumWidth : 20,
		resizable : true,
		loadonce : true,
		toppager : true,
		fixed : true,
		pager : '#prdSeqInfoPg',
		pginput : true,
		fixed : true,
		cellsubmit : "clientArray",
		jsonReader : {
		},
		emptyrecords : true,// 当数据源为null时，显示为0
		colModel : prdItemInfoCM,
		onSelectRow : function(id) {
			f1QueryFnc();
		}
	});

    var toolFunc = {
    };
	var resetJqgrid = function() {
		
	};

	function getOpeListFnc() {
		var ope_id, ope_ver, ope_dsc,inTrxObj, outTrxObj, tbl_cnt, i;

		inTrxObj = {
			trx_id : "XPBISOPE",
			action_flg : "L"
		};

		outTrxObj = comTrxSubSendPostJson(inTrxObj);

		if (outTrxObj.rtn_code == GlobalBean.NORMAL) {

			GlobalBean.opeKey = new Array();
			tbl_cnt = comParseInt(outTrxObj.tbl_cnt);
			if (tbl_cnt == 1) {
				ope_id = outTrxObj.oary["ope_id"];
				ope_ver = outTrxObj.oary["ope_ver"];
				ope_dsc = outTrxObj.oary["ope_dsc"];
				GlobalBean.opeKey.push({
					ope_id : ope_id,
					ope_ver : ope_ver
				});
				$("#opeIDSel").append("<option value=" + ope_id + ">" + ope_dsc	+ "</option>");
			} else if (tbl_cnt > 1) {
				for (i = 0; i < tbl_cnt; i++) {
					ope_id = outTrxObj.oary[i]["ope_id"];
					ope_ver = outTrxObj.oary[i]["ope_ver"];
					ope_dsc = outTrxObj.oary[i]["ope_dsc"];
					GlobalBean.opeKey.push({
						ope_id : ope_id,
						ope_ver : ope_ver
					});
					$("#opeIDSel").append("<option value=" + ope_id + ">"
						+ ope_dsc + "</option>");
				}
			}
			 $("#opeIDSel").select2({
		 	    	theme : "bootstrap"
		 	    });

		}
	}
	function getToolListFnc() {
		var inTrxObj = {
			trx_id : "XPBISTOL",
			action_flg : "L"
		};
		setSelectObjByinTrx("#toolIDSel", inTrxObj, "tool_id");
	}
	
	function setMesIdSel(){
		var inObj,outObj,toolId;        
		toolId = $("#toolIDSel").val();
		if(!toolId){
			return;
		}
		inObj = {
			trx_id:"XPMLITEM",
			action_flg : "M",
			iaryA :{
				tool_id_fk : toolId 
			}
		};
		outObj = comTrxSubSendPostJson(inObj);
		if(outObj.rtn_code === "0000000"){
			SelectDom.initWithSpace($("#mesIDSel"));
			SelectDom.addSelectArr($("#mesIDSel"),outObj.oaryA,["mes_id_fk"],["mes_id_fk"],"Y");
		}
	}
	
	$("#toolIDSel").on("change",setMesIdSel);
	
	function itemRadioFnc() {
		var item = $("input[name='items']:checked").val();
		switch (item) {
			case "lot" :
				$("#lotIDTxt").attr({'disabled' : false	});
				$("#boxIDTxt").attr({'disabled' : true  });
				$("#prdSeqIdTxt").attr({'disabled' : true });
				$("#woIDTxt").attr({'disabled' : true	});
				break;
			case "box" :
				$("#lotIDTxt").attr({'disabled' : true});
				$("#boxIDTxt").attr({'disabled' : false});
				$("#prdSeqIdTxt").attr({'disabled' : true});
				$("#woIDTxt").attr({'disabled' : true	});
				break;
			case "prdSeq" :
				$("#lotIDTxt").attr({'disabled' : true});
				$("#boxIDTxt").attr({'disabled' : true});
				$("#prdSeqIdTxt").attr({'disabled' : false});
				$("#woIDTxt").attr({'disabled' : true	});
				break;
			case "worder" :
				$("#woIDTxt").attr({'disabled' : false	});
				$("#lotIDTxt").attr({'disabled' : true});
				$("#boxIDTxt").attr({'disabled' : true});
				$("#prdSeqIdTxt").attr({'disabled' : true});
				break;
			default :
				$("#lotIDTxt").attr({'disabled' : true});
				$("#boxIDTxt").attr({'disabled' : true});
				$("#prdSeqIdTxt").attr({'disabled' : true});
				$("#woIDTxt").attr({'disabled' : true	});
				alert("please choose one");
				break;
		}
	}
	
	$("input[name='items']").click(function() {
		itemRadioFnc();
	});

	$("#boxIDTxt").keypress(function(event) {
		if (event.keyCode == 13) {
			var inTrxObj = {
				trx_id : 'XPINQBOX',
				action_flg : 'S',
				box_id : $("#boxIDTxt").val()
			};
			var outTrxObj = comTrxSubSendPostJson(inTrxObj);
			if (outTrxObj.rtn_code == GlobalBean.NORMAL) {
				setGridInfo(outTrxObj.table, "#prdSeqInfoGrd",true);

				$("#toolIDSel").val(outTrxObj.tool_id_fk).trigger("change");
				$("#opeIDSel").val(outTrxObj.nx_ope_id_fk).trigger("change");

				setMesIdSel();
			}
		}
	});
	$("#prdSeqIdTxt").keyup(function(event) {
		var prdRowData, rowIDs, rowData;
		if (event.keyCode == 13) {
			query_main_prd_info_func();
		}
	});
	
	//查询是否存在重复玻璃ＩＤ
	function query_main_prd_info_func (){
		var prd_seq_id = $.trim($("#prdSeqIdTxt").val());
    	if(prd_seq_id){
    		inObj = {
    			trx_id : "XPINQSHT",
    			action_flg : "F",
    			fab_sn : prd_seq_id
    		};
    		outObj = comTrxSubSendPostJson(inObj);
    		if (outObj.rtn_code == GlobalBean.NORMAL) {
    			if(outObj.has_more_flg === "Y"){
    				FabSn.showSelectWorderDialog(prd_seq_id, outObj.oary3, function (data){
        				$("#prdSeqIdTxt").val(data.prd_seq_id);
        				query_prd_info_func();
        			});
    			}else{
    				query_prd_info_func();
    			}
    		}
    	}
	};
	
	function query_prd_info_func (){
		var prdRowData, rowIDs, rowData,prd_seq_id;
		$("#prdSeqInfoGrd").jqGrid("clearGridData");
		prd_seq_id = $.trim($("#prdSeqIdTxt").val());
		var inTrxObj = {
			trx_id : "XPINQSHT",
			action_flg : "Q",
			prd_seq_id : prd_seq_id
		};
		var outTrxObj = comTrxSubSendPostJson(inTrxObj);
		if (outTrxObj.rtn_code == GlobalBean.NORMAL) {
			$("#prdSeqInfoGrd").jqGrid("addRowData",
				getGridNewRowID($("#prdSeqInfoGrd")), outTrxObj);
			$("#toolIDSel").val(outTrxObj.cr_tool_id_fk).trigger("change");
			$("#opeIDSel").val(outTrxObj.cr_ope_id_fk).trigger("change");
			$("#fromThicknessSpn").val(outTrxObj.from_thickness);
			$("#toThicknessSpn").val(outTrxObj.to_thickness);
			setMesIdSel();
		}
	}
	
	$("#lotIDTxt").keypress(function(event) {
		if (event.keyCode == 13) {
			var lot_id,inTrx,outTrx;
			lot_id = $(this).val();
			inTrx = {
				trx_id     : "XPINQLOT",
				action_flg : "L",
				lot_id     : lot_id
			};
			outTrx = comTrxSubSendPostJson(inTrx);
			if(outTrx.rtn_code ==GlobalBean.NORMAL){
				setGridInfo(outTrx.oary, "#prdSeqInfoGrd",true);

				$("#toolIDSel").val(outTrx.oary[0].tool_id_fk).trigger("change");
				$("#opeIDSel").val(outTrx.oary[0].nx_ope_id_fk).trigger("change");

				setMesIdSel();
			}
		}
	});
	$("#woIDTxt").keypress(function(event) {      //NO.02
		if (event.keyCode == 13) {
			var worder_id,inTrx,outTrx;
			worder_id = $(this).val();
			inTrx = {
				trx_id     : "XPINQWOR",
				action_flg : "P",
				worder_id  : worder_id
			};
			outTrx = comTrxSubSendPostJson(inTrx);
			if(outTrx.rtn_code ==GlobalBean.NORMAL){
				    outTrx.oary5 = $.isArray(outTrx.oary5) ? outTrx.oary5 : [outTrx.oary5];
					
				setGridInfo(outTrx.oary5, "#prdSeqInfoGrd",true);

				$("#toolIDSel").val(outTrx.oary5[0].tool_id_fk);//.trigger("change");
				$("#opeIDSel").val(outTrx.oary5[0].nx_ope_id_fk);//.trigger("change");

				setMesIdSel();
			}
		}
	});
	function f1QueryFnc() {
		var prdSeqId,toolgId;
		var prdSeqInfoGrid =  $("#prdSeqInfoGrd");
		var rowIDs = prdSeqInfoGrid.jqGrid("getDataIDs");
		if (rowIDs.length == 0) {
			showErrorDialog("003", "玻璃列表为空,请先输入需要上报的玻璃");
			return false;
		}
		var selectRowID =prdSeqInfoGrid.jqGrid("getGridParam", "selrow" );
		var rowData = prdSeqInfoGrid.jqGrid("getRowData",selectRowID);
		prdSeqId = rowData.prd_seq_id;
		if(!prdSeqId){
			showErrorDialog("", "玻璃列表为空,请先输入需要上报的玻璃");
			return false;
		}
		var inTrxObj = {
			trx_id : "XPINQSHT",
			action_flg : "Q",
			prd_seq_id : prdSeqId
		};
		var outTrxObj = comTrxSubSendPostJson(inTrxObj);
		var wo_id_fk = outTrxObj.wo_id_fk;
		if (outTrxObj.rtn_code == GlobalBean.NORMAL) {
			$("#fromThicknessSpn").val(outTrxObj.from_thickness);
			$("#toThicknessSpn").val(outTrxObj.to_thickness);
		}
		var iary = {
			tool_id_fk : $("#toolIDSel").find("option:selected").text(),
			mdl_id_fk : rowData.mdl_id_fk,
			path_id_fk : rowData.cr_path_id_fk,
			path_ver_fk : rowData.cr_path_ver_fk,
			ope_no_fk : rowData.cr_ope_no_fk,
			ope_id_fk : $("#opeIDSel").val(),
			ope_ver_fk : rowData.cr_ope_ver_fk
		};
		var inTrxObj = {
			trx_id : 'XPBISPAM',
			action_flg : 'K',
			iary : iary,
			tbl_cnt : 1,
			wo_id_fk : outTrxObj.wo_id_fk
		};
		var outTrxObj = comTrxSubSendPostJson(inTrxObj);
		var oary;
		var mesId;
		if (outTrxObj.rtn_code == GlobalBean.NORMAL) {
			if (outTrxObj.tbl_cnt = 1) {
				oary = outTrxObj.oary;
			} else if (outTrxObj.tbl_cnt > 1) {
				oary = outTrxObj.oary[0];
			}
			
			if(oary && oary.mes_id){
                 mesId = $("#mesIDSel").val()?  $("#mesIDSel").val():oary.mes_id;
            }else{
                 mesId = $("#mesIDSel").val();
            }
            GlobalBean.mes_id = mesId;
            if(!mesId){
                 $("#dataItemEditDiv").empty();
                 return false;
            }
			mesId = $("#mesIDSel").val()?  $("#mesIDSel").val():oary.mes_id;
			var iary1 = {
				tool_id_fk : $("#toolIDSel").find("option:selected").text(),
				ope_no : rowData.cr_ope_no_fk,
				mes_id_fk : mesId,
				rep_unit : 'P',// TODO
				data_pat : '00' // TODO
			};
			var inTrxObj1 = {
				trx_id : 'XPMLITEM',
				action_flg : 'Q',
				iaryA : iary1,
				wo_id_fk : wo_id_fk
			};
			var outTrxObj1 = comTrxSubSendPostJson(inTrxObj1);
			if (outTrxObj1.rtn_code == GlobalBean.NORMAL) {
				var tbl_cnt = outTrxObj1.tbl_cnt_b;
				$("#dataItemEditDiv").empty();
				var str = '<table id="dataItemTbl" class="table table-striped table-bordered table-condensed" ><tr>'
					+ '<td class="Header_Text " >参数序号 </td>'
					+ '<td class="Header_Text " >参数名称 </td>'
					+ '<td class="Header_Text " >组内序号 </td>'
					+ '<td class="Header_Text " >参数说明 </td>'
					+ '<td class="Header_Text " >值 </td>'
					+ '<td class="Header_Text " >下限 </td>'
					+ '<td class="Header_Text " >上限</td></tr> ';
				for (var i = 0; i < tbl_cnt; i++) {
					str = str + "<tr>" + '<td class=" dataItemTdCss">'
						+ outTrxObj1.oaryB[i].data_id + "</td>"
						+ '<td id ="tbl_data_group' + i
						+ '\" class=" dataItemTdCss">'
						+ outTrxObj1.oaryB[i].data_group + "</td>"
						+ '<td id ="tbl_data_group_seq' + i
						+ '\" class=" dataItemTdCss">'
						+ outTrxObj1.oaryB[i].data_group_seq + "</td>"
						+ '<td class=" dataItemTdCss">'
						+ outTrxObj1.oaryB[i].data_dsc + "</td>"
						+ '<td class=" dataItemTdCss">'
						+ '<input type="text" name='+ i +' class ="span2 numberCssA" id="dataItem_'
						+ i + '_Txt\" />' + "</td>";
					if(outTrxObj1.oaryB[i].l_spec){
						str = str + '<td class=" dataItemTdCss"'+'id="lsl_'+i +'" >'
						+ outTrxObj1.oaryB[i].l_spec + "</td>";
					}
					if(outTrxObj1.oaryB[i].u_spec){
						str = str + '<td class=" dataItemTdCss"'+'id="usl_'+i+ '" >'
						+ outTrxObj1.oaryB[i].u_spec + "</td>";
					}	
					+ "</tr>";
				}
				str = str + "</table>";
				$(str).appendTo("#dataItemEditDiv");
				for (var i = 0; i < tbl_cnt; i++) {
				$("#dataItem_" + i + "_Txt").keypress(function(event){
						if (event.keyCode == 13) {
							dataItemKeyPressFnc(this);
						}
					});
				}
//				GlobalBean.mes_id = oary.mes_id;
				showMessengerSuccessDialog("刷新列表成功!!",1);
				
				$(".numberCssA").blur(function() {
					var $this = $(this);
					var val = $this.val();
					if (val && !ComRegex.isNumber(val)) {
						showErrorDialog("", "[" + val + "]不是一个有效的数字,错误");
						$this.val("");
					}
				});
				
			}
		}
	}
	
	/**
	 * 当没有上下限时，usl，lsl的值是 Nan
	 * value>usl,value<lsl 都不会成立
	 * 可达到不参加check的目的
	 */
	function dataItemKeyPressFnc(obj){
		var nextSelecter,uslSelector,lslSelector,usl,lsl,index,value;
		index = obj.id.split("_")[1];
		uslSelector = "#usl_"+index ;
		usl = parseFloat($(uslSelector).text());
		lslSelector = "#lsl_"+index;
		lsl = parseFloat($(lslSelector).text());
		value = parseFloat($(obj).val()); 
		if(value>usl){
			showMessengerErrorDialog("输入数值高于标准上限"+usl,2);
			$(obj).focus();
			return false;
		}else if(value<lsl){
			showMessengerErrorDialog("输入数值低于标准下限"+lsl,2);
			$(obj).focus();
			return false;
		}
		nextSelecter = "#dataItem_" +( parseInt(obj.name,10)+1 ) + "_Txt";
		$(nextSelecter).focus();	
	}
	$("#f1_btn").click(f1QueryFnc);
	
	function f8ReportFnc(){

		var selectID = $("#prdSeqInfoGrd").jqGrid("getGridParam","selrow");
		if (!selectID) {
			showErrorDialog("003", "请选择需要上报的玻璃");
			return false;
		}
		var prdSeqRowData = $("#prdSeqInfoGrd").jqGrid("getRowData",
				selectID);
		var prd_seq_id = prdSeqRowData.prd_seq_id;
		
		var judgeStr = "OK";
		
		var tableItemCnt = 0;
		tableItemCnt = $("#dataItemTbl tr").length - 1;
		var iaryAry = new Array();
		for (var i = 0; i < tableItemCnt; i++) {
			if ($("#dataItem_" + i + "_Txt").val() != "") {
				var iary = {
					data_group : $("#tbl_data_group" + i).text(),
					data_value : $("#dataItem_" + i + "_Txt").val(),
					data_group_seq : $("#tbl_data_group_seq" + i).text()
				};
				iaryAry.push(iary);
				if (judgeStr != "NG"){
					if(($("#dataItem_" + i + "_Txt").val()) < ($("#lsl_"+i ).text()) || ($("#dataItem_" + i + "_Txt").val()) > ($("#usl_"+i ).text())){
						judgeStr = "NG";
					}else {
						judgeStr = "OK";
					}
				}
			}
		}
		
		var opeId = prdSeqRowData.cr_ope_id_fk;
		var woId = prdSeqRowData.wo_id_fk;
		var lotId = prdSeqRowData.lot_id;
		// Send Tx
		var inTrxObj = {
			trx_id      :  'XPCMSRDT',
			prd_seq_id  :  prd_seq_id,
			tool_id     :  $("#toolIDSel").find("option:selected").text(),
			data_pat_fk :  '00',
			mes_id_fk   :  GlobalBean.mes_id,
			path_id     :  prdSeqRowData.cr_path_id_fk,
			path_ver    :  prdSeqRowData.cr_path_ver_fk,
			ope_no      :  prdSeqRowData.cr_ope_no_fk,
			ope_id      :  opeId,
			ope_ver     :  prdSeqRowData.cr_ope_ver_fk,
			proc_id     :  prdSeqRowData.cr_proc_id_fk,
			rpt_usr     :  $("#userId").text(),
			judgeStr    :  judgeStr,
			data_cnt    :  iaryAry.length,
			iary        :  iaryAry
		};
   		
   		spcThicknessRule.reportSPC(inTrxObj, GlobalBean.mes_id, opeId, woId, lotId, function(outTrxObj){
			if (outTrxObj.rtn_code == VAL.NORMAL) {
    			showMessengerSuccessDialog("数据上报成功!!");
    			$("[id^='dataItem_']").val("");
    		}
		});
		
		

	}
	
	$("#f8_btn").click(f8ReportFnc);
	
	function rpt_all_btnFnc(){
        var rowIds, iaryB = [], i, rowData, inTrxObj, outTrxObj;
		var selectID = $("#prdSeqInfoGrd").jqGrid("getGridParam","selrow");
		if (!selectID) {
			showErrorDialog("003", "请选择需要上报的玻璃");
			return false;
		}
		var prdSeqRowData = $("#prdSeqInfoGrd").jqGrid("getRowData",selectID);
		var prd_seq_id = prdSeqRowData.prd_seq_id;
		
		var judgeStr = "OK";
		
		var tableItemCnt = 0;
		tableItemCnt = $("#dataItemTbl tr").length - 1;
		var iaryAry = new Array();
		for (i = 0; i < tableItemCnt; i++) {
			if ($("#dataItem_" + i + "_Txt").val() != "") {
				var iary = {
					data_group : $("#tbl_data_group" + i).text(),
					data_value : $("#dataItem_" + i + "_Txt").val(),
					data_group_seq : $("#tbl_data_group_seq" + i).text()
				};
				iaryAry.push(iary);
				if (judgeStr != "NG"){
					if(($("#dataItem_" + i + "_Txt").val()) < ($("#lsl_"+i ).text()) || ($("#dataItem_" + i + "_Txt").val()) > ($("#usl_"+i ).text())){
						judgeStr = "NG";
					}else {
						judgeStr = "OK";
					}
				}
			}
		}
		rowIds = $("#prdSeqInfoGrd").jqGrid("getDataIDs");
        if(parseInt(rowIds.length) != 0){
        	for (i = 0; i < rowIds.length; i ++){
                rowData = $("#prdSeqInfoGrd").jqGrid("getRowData", rowIds[i]);
                iaryB.push({ prd_seq_id_fk  : rowData["prd_seq_id"] });
        	}
        }		
		
		var opeId = prdSeqRowData.cr_ope_id_fk;
		var woId = prdSeqRowData.wo_id_fk;
		var lotId = prdSeqRowData.lot_id;
		// Send Tx
		inTrxObj = {
			trx_id      :  'XPCMSRDT',
			rep_unit_fk :  "T",
			prd_seq_id  :  prd_seq_id,
			tool_id     :  $("#toolIDSel").find("option:selected").text(),
			data_pat_fk :  '00',
			mes_id_fk   :  GlobalBean.mes_id,
			path_id     :  prdSeqRowData.cr_path_id_fk,
			path_ver    :  prdSeqRowData.cr_path_ver_fk,
			ope_no      :  prdSeqRowData.cr_ope_no_fk,
			ope_id      :  opeId,
			ope_ver     :  prdSeqRowData.cr_ope_ver_fk,
			proc_id     :  prdSeqRowData.cr_proc_id_fk,
			rpt_usr     :  $("#userId").text(),
			judgeStr    :  judgeStr,
			data_cnt    :  iaryAry.length,
			iary        :  iaryAry,
			id_cnt      :  parseInt(rowIds.length),
			iary_id     :  iaryB
		};
		outTrxObj = comTrxSubSendPostJson(inTrxObj);
		if (outTrxObj.rtn_code == VAL.NORMAL) {
			showMessengerSuccessDialog("数据上报成功!!");
			$("[id^='dataItem_']").val("");
		}
		
	}	

	$("#rpt_all_btn").click(rpt_all_btnFnc);
	
//	function opeIDSelChangeFnc() {
//		var ope_id, i, iary, inTrx;
//
//		ope_id = $("#opeIDSel").val();
//		inTrx = {
//			trx_id : "XPBISTOL",
//			action_flg : "Q"
//		};
//
//		for (i = 0; i < GlobalBean.opeKey.length; i++) {
//			if (GlobalBean.opeKey[i].ope_id == ope_id) {
//				inTrx.iary = {
//					ope_id : ope_id,
//					ope_ver : GlobalBean.opeKey[i].ope_ver
//				};
//			}
//		}
//
//		setSelectObjByinTrx("#toolIDSel", inTrx, "tool_id");
//
//	}
	function opeIDSelChangeFnc() {
		var ope_id, i, iary, inTrx;

		ope_id = $("#opeIDSel").val();
		inTrx = {
			trx_id : "XPBISTOL",
			action_flg : "Q"
		};
		var existFlg  = false;
		for (i = 0; i < GlobalBean.opeKey.length; i++) {
			if (GlobalBean.opeKey[i].ope_id == ope_id) {
				existFlg = true;
				inTrx.iary = {
					ope_id : ope_id,
					ope_ver : GlobalBean.opeKey[i].ope_ver
				};
			}
		}

		if(!existFlg){
			return false;
		}
		setSelectObjByinTrx("#toolIDSel", inTrx, "tool_id");

	}

	$("#opeIDSel").change(opeIDSelChangeFnc);
	
	$("#clear_btn").click(function() {
		$("[id^='dataItem_']").val("");
	});
	
	document.onkeydown = function(event) {
        if(event.keyCode == F8_KEY){
            f8ReportFnc();
        }
        return true;
    };
	function initFnc() {
		$("#itemDiv").css({
			height : ($(document).height() * 0.7)
		});
		resetJqgrid();
		itemRadioFnc();
		getOpeListFnc();
		getToolListFnc();
		$("#fromThicknessSpn").val("");
		$("#toThicknessSpn").val("");
		
	}
	initFnc();
	$(window).resize(function() {
		resetJqgrid();
	});    
    function resizeFnc(){                                                                      
    	var offsetBottom, divWidth;  
		divWidth = domObj.$prdSeqInfoDiv.width();
		offsetBottom = domObj.$window.height() - domObj.$prdSeqInfoDiv.offset().top;
		domObj.$grdDiv.height(domObj.$window.height() - domObj.$grdDiv.offset().top);
		domObj.$itemDiv.height(domObj.$window.height() - domObj.$itemDiv.offset().top);
		domObj.$prdSeqInfoDiv.height(offsetBottom * 0.80);                          
		domObj.$prdSeqInfoGrd.setGridWidth(divWidth * 0.99);                   
		domObj.$prdSeqInfoGrd.setGridHeight(offsetBottom * 0.8 - 80);
        if(($(window).width() > 1000)||($(window).width() < 1100)){
            $("#prdSeqInfoDiv").height("500px");
        }
        if(($(window).width() > 900)||($(window).width() < 1000)){
            $("#gview_prdSeqInfoGrd").children(".ui-jqgrid-bdiv").height("300px");
            $("#itemDiv").height("300px");
        }
    };
    resizeFnc();                                                                               
    domObj.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});  
    
});