
$(document).ready(function() {
  var GlobalVal = {
     mes_id  : ""
  };
	var queryTool,mainGroup,subGroup;

  function QueryTool(){}
      QueryTool.prototype.showDialog = function(){
          $('#queryGroupDialog').modal({
            backdrop:true,
            keyboard:false,
            show:false
          });
//          $('#queryGroupDialog').unbind('shown');
          $("#queryGroupDialog_queryBtn").unbind('click');
//          $('#queryGroupDialog').bind('shown',this.groupDialogShowFnc);
          $('#queryGroupDialog').modal("show");
          $("#queryGroupDialog_queryBtn").bind('click',this.queryFnc);
          $("#queryGroupDialog_groupNameTxt").attr({'disabled':false});
          //this.groupDialogShowFnc();
        };
      QueryTool.prototype.initFnc = function(){
          $("queryGroupDialog input").val("");
          $("queryGroupDialog select").empty();
          $(mainGroup.mainGroupID).jqGrid("clearGridData");
          $(subGroup.subGroupID).jqGrid("clearGridData");
        };
      QueryTool.prototype.queryFnc=function(){
          var grpMain ={},
          grp_name,
          url,
          outTrxObj;
          grp_name = $("#queryGroupDialog_groupNameTxt").val();
          if(grp_name!=""){
            grpMain.grp_name = grp_name ;
          }
          url = "spc/findSPCGroupMain.action" ;
          outTrxObj = comSendSpcJson(url,grpMain);
          if( outTrxObj.rtn_code == 0 ){
            setGridInfo(outTrxObj.oary,"#groupListGrd");
            $('#queryGroupDialog').modal("hide");  
          }
        };

  function MainGroup(){
    this.groupItemInfoCM =
      [
        {name: 'col_typ'   , index: 'col_typ'   ,label: SPC_COL_TYP_TAG     , width: 80 ,hidden:true },
        {name: 'grp_no'    , index: 'grp_no'    ,label: SPC_GROUP_NO_TAG    , width: 80 ,hidden:true },
        {name: 'grp_name'  , index: 'grp_name'  ,label: SPC_GROUP_NAME_TAG  , width: 80 },
        {name: 'grp_dsc'   , index: 'grp_dsc'   ,label: SPC_GROUP_DSC       , width: 80 }
      ];
    this.mainGroupID = "#groupListGrd";
    this.initFnc     = function(){
        $("#groupListGrd").jqGrid({
          url:"",
          datatype:"local",
          mtype:"POST",
          height:$("#groupListDiv").height(),
          width:$("#groupListDiv").width()*0.99,
          autowidth:false,
          shrinkToFit:false,
          resizable : true,
          loadonce:true,
          viewrecords : true, // 显示总记录数
          rownumbers  :true ,// 显示行号
          emptyrecords :true ,
          colModel: this.groupItemInfoCM,
          gridComplete:function(){

          },
          onSelectRow:function(id){
            var rowData,
            col_typ_fk,
            grp_no_fk,
            iary,
            inTrxObj,
            outTrxObj;

            rowData = $(this).jqGrid("getRowData",id);
            col_typ_fk = rowData.col_typ;
            grp_no_fk  = rowData.grp_no;
            iary ={
                col_typ_fk : col_typ_fk,
                grp_no_fk  : grp_no_fk
            };
            inTrxObj ={
              trx_id     : "SPCGRPDT",
              action_flg :"Q",
              iary       : iary
            };
            outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if(outTrxObj.rtn_code=="0000000"){
              setGridInfo(outTrxObj.oary,"#subGroupListGrd");
            }
            inTrxObj ={
              trx_id    : "SPCCHTMN",
              action_flg: "Q",
              iary      : iary 
            };
            outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if(outTrxObj.rtn_code=="0000000"){
              setGridInfo(outTrxObj.oary,"#chartListGrd");
            }
          }
       });
    };
    this.action_flg  = null ;
    this.showEditDialog = function(){
        $('#mainGroupEditDialog').modal({
          backdrop:true,
          keyboard:false,
          show:false
        });
        this._this = this;
//        $('#mainGroupEditDialog').unbind('shown');
        $("#mainGroupEditDialog_saveBtn").unbind('click');
        $("#mainGroupEditDialog_saveBtn").bind('click',this.saveMainGroupFnc);
        $('#mainGroupEditDialog').modal("show");
        this.initEditDialogFnc();
    };
    /***************************************************************************
	 * click事件中的this都变成了jQuery对象,所以要使用MainGroup, 需要定义that 变量,储存click之前的this
	 **************************************************************************/
    this.initEditDialogFnc  = function(){
    	
        var selectRowID,
            rowData,
            that;
        that = mainGroup;// TODO
        subGroup.setColTypSelFnc();
        
        if(that.action_flg=="A"){
           $("#colTypSel").attr({"disabled":false});
           $("#grpNameTxt").val("");
           $("#grpNameTxt").attr({"disabled":false});
           $("#grpDscTxt").val("");
           $("#grpDscTxt").attr({"disabled":false});
        }else{
           selectRowID = $(that.mainGroupID).jqGrid("getGridParam","selrow");
           rowData     = $(that.mainGroupID).jqGrid("getRowData",selectRowID);
           $("#colTypSel").val(rowData.col_typ);
           $("#colTypSel").attr({"disabled":true});
           $("#grpNameTxt").val(rowData.grp_name);
           $("#grpNameTxt").attr({"disabled":true});
           $("#grpDscTxt").val(rowData.grp_dsc);
           $("#grpDscTxt").attr({"disabled":false});
        }
    };
    this.saveMainGroupFnc   = function(){
        var action_flg,
        iary,
        inTrxObj,
        outTrxObj,
        newRowID,
        selectRowID,
        rowData,
        that;

        that = mainGroup;// TODO
        action_flg = that.action_flg;
        if(action_flg==null){
          return false;
        }
        if(action_flg=="A"){
          iary = {
             col_typ  : $("#colTypSel").val(),
             grp_name : $("#grpNameTxt").val(),
             grp_dsc  : $("#grpDscTxt").val(),
             evt_usr  : $("#userId").text()
          };
          inTrxObj ={
             trx_id      : "SPCGRPMN" ,
             action_flg  : "A"        ,
             iary        : iary  
          };
          outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if( outTrxObj.rtn_code=="0000000"){
            newRowID = getGridNewRowID("#groupListGrd");
            $("#groupListGrd").jqGrid("addRowData",newRowID,outTrxObj.oary);
            showSuccessDialog("新增管制图群组成功");
            // initMainGroupEditDialogFnc();
          }
        }else{
          selectRowID = $("#groupListGrd").jqGrid("getGridParam","selrow");
          rowData = $("#groupListGrd").jqGrid("getRowData",selectRowID);
          iary = {
             col_typ  : $("#colTypSel").val(),
             grp_no   : rowData.grp_no,
             grp_name : $("#grpNameTxt").val(),
             grp_dsc  : $("#grpDscTxt").val(),
             evt_usr  : $("#userId").text()
          };
          inTrxObj ={
             trx_id      : "SPCGRPMN" ,
             action_flg  : "U"        ,
             iary        : iary  
          };
          outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if( outTrxObj.rtn_code=="0000000"){
            $("#groupListGrd").jqGrid("setRowData",selectRowID,iary);
            showSuccessDialog("修改管制图群组信息成功");
          }
        }
        $('#mainGroupEditDialog').modal("hide");
    };
    this.addMainGroupFnc    = function(){
          this.action_flg = "A";
          this.showEditDialog();
    };
    this.updateMainGroupFnc = function(){
          var selRowID,
              rowData;
  
          selRowID = $("#groupListGrd").jqGrid("getGridParam","selrow");
          if( selRowID ==""||selRowID == null){
              showErrorDialog("003","请选择需要编辑的行");
              return false;
          }
          this.action_flg = "U";
          this.showEditDialog;
    };
    this.deleteGroupFnc     = function(){
          var selRowID,
              rowData,
              iary,
              outTrxObj;
          selRowID = $("#groupListGrd").jqGrid("getGridParam","selrow");
          if( selRowID==""||selRowID==null){
              showErrorDialog("003","请选择需要删除的行");
              return false;
          }
          $("#f4_del_btn").showCallBackWarnningDialog({
              errMsg  : "是否删除Group信息,执行此操作Chart同时也被删除,请确认!!!!",
              callbackFn : function(data) {
                if(data.result==true){
                  rowData  = $("#groupListGrd").jqGrid("getRowData",selRowID);
                  iary = {
                     col_typ  : rowData.col_typ,
                     grp_no   : rowData.grp_no,
                     grp_name : rowData.grp_name,
                     grp_dsc  : rowData.grp_dsc,
                     evt_usr  : rowData.evt_usr
                  };
                  
                  inTrxObj ={
                     trx_id      : "SPCGRPMN" ,
                     action_flg  : "D"        ,
                     iary        : iary  
                   };
                  outTrxObj = comTrxSubSendPostJson(inTrxObj);
                  if( outTrxObj.rtn_code=="0000000"){
                    $("#groupListGrd").jqGrid("delRowData",selRowID);
                    showSuccessDialog("删除Group成功");  
                  }
                }
              }
           });
    };
    // }
  };
  function f4Fnc(){
    mainGroup.deleteGroupFnc();
  }
  function f5Fnc(){
    mainGroup.updateMainGroupFnc();
  }
  function f6Fnc(){
    mainGroup.addMainGroupFnc();
  }
  $("#f4_del_btn").click(f4Fnc);
  $("#f5_upd_btn").click(f5Fnc);
  $("#f6_add_btn").click(f6Fnc);
  

  function SubGroup(){
    this.subGroupItemInfoCM =
      [
          {name: 'col_typ_fk',    index: 'col_typ_fk',   label: "col_typ_fk",     width: 40,hidden:true},// ,hidden:true},
          {name: 'grp_no_fk',     index: 'grp_no_fk',    label: "grp_no_fk",      width: 40,hidden:true},
          {name: 'sub_grp_no',    index: 'sub_grp_no',   label: "sub_grp_no",     width: 40,hidden:true},
          {name: 'cus_id_fk',     index: 'cus_id_fk',    label: "客户",           width: 80},
          {name: 'wo_id_fk',      index: 'wo_id_fk',     label: "内部订单",       width: 100},
          {name: 'tool_id_fk',    index: 'tool_id_fk',   label: TOOL_ID_TAG,      width: 80},
          {name: 'mdl_id_fk',     index: 'mdl_id_fk',    label: MDL_ID_TAG,       width: 180},
          {name: 'path_id_fk',    index: 'path_id_fk',   label: PATH_ID_TAG    ,  width: 80},
          {name: 'ope_id_fk',     index: 'ope_id_fk',    label: OPE_ID_TAG     ,  width: 80}
      ];
    this.subGroupID = "#subGroupListGrd";
    this.initFnc=function(){
      $("#subGroupListGrd").jqGrid({
          url:"",
          datatype:"local",
          mtype:"POST",
          height:400,// TODO:需要根据页面自适应，要相对很高
          autowidth:true,// 宽度根据父元素自适应
          shrinkToFit:true,
          scroll:true,
          multiselect:true,
          viewrecords:true,
          rownumbers  :true ,
          rowNum:20,
          rownumWidth : 20,

          resizable : true,
          loadonce:true,
          toppager: true ,
          fixed:true,
          pager : '#subGroupListPg',
          pginput :true,
          fixed: true,
          cellEdit : true,
          cellsubmit : "clientArray",
          emptyrecords :true ,// 当数据源为null时，显示为0
          colModel: this.subGroupItemInfoCM  
      });
    };
    this.action_flg = null;
    this.showSubEditDialog = function(){
      var action_flg,
          mainGroupSelRowID;

      action_flg = subGroup.action_flg;

      mainGroupSelRowID = $("#groupListGrd").jqGrid("getGridParam","selrow");
      if(mainGroupSelRowID==null){
           showErrorDialog("003","请选择Main Group");
           return false;
      }
      $('#subGroupEditDialog').modal({
          backdrop:true,
          keyboard:false,
          show:false
      });
//      $('#subGroupEditDialog').unbind('shown');
      $("#subGroupEditDialog_saveBtn").unbind('click');
      $("#subGroupEditDialog_saveBtn").bind('click',this.saveSubGroupFnc);
      $('#subGroupEditDialog').modal("show");
      this.initSubEditDialogFnc();
    };
    this.initSubEditDialogFnc=function(){
      var mainGroupSelRowID,
          mainGroupRowData,
          selRowIDs,
          rowData;
      action_flg = subGroup.action_flg;
      $("#subGroupEditDialogForm input").attr({"disabled":false});
//      $("#subGroupEditDialogForm select").attr({"disabled":false});    
      mainGroupSelRowID = $("#groupListGrd").jqGrid("getGridParam","selrow");
      mainGroupRowData  = $("#groupListGrd").jqGrid("getRowData",mainGroupSelRowID);
      
      $("#subGroupEdit_colTypTxt").val(mainGroupRowData.col_typ);
      $("#subGroupEdit_grpNoTxt").val(mainGroupRowData.grp_no);
      subGroup.setToolListFnc();
      subGroup.setOpeListFnc();
      subGroup.setMdlIDListFnc();
      subGroup.setPathIDListFnc();
      subGroup.setWoIdSel();
      subGroup.setCusIdSel();
      if(action_flg=="A"){
        
      }else{
        selRowIDs = $("#subGroupListGrd").jqGrid("getGridParam","selarrrow");
        rowData = $("#subGroupListGrd").jqGrid("getRowData",selRowIDs[0]);
        $("#toolIDSel").val(rowData.tool_id_fk).trigger("change");;
        $("#mdlIDSel").val(rowData.mdl_id_fk).trigger("change");;
        $("#pathIDSel").val(rowData.path_id_fk).trigger("change");;
        $("#opeIDSel").val(rowData.ope_id_fk).trigger("change");;
      }
    };
    this.saveSubGroupFnc=function(){
      var action_flg, 
      	  cus_id_fk,
      	  wo_id_fk,
          tool_id_fk,
          mdl_id_fk,
          path_id_fk,
          ope_id_fk,
          sub_grp_no,
          rowIDs,// 新增时遍历Grid时所有的rowid
          gridRowData,// 新增时遍历Grid时所有的rowData
          newRowID,// 新增时，添加行的rowid
          selectRowID,// 修改时的选中行ID
          selectRowData,// 修改时的选中行RowData
          iary,
          inTrxObj,
          outTrxObj;

      action_flg= subGroup.action_flg;
      if(action_flg==undefined){
         return false;
      }

      wo_id_fk   = $("#woIDSel").val();
      cus_id_fk  = $("#cusIDSel").val();
      tool_id_fk = $("#toolIDSel").find("option:selected").text();
      mdl_id_fk  = $("#mdlIDSel").find("option:selected").text();
      path_id_fk = $("#pathIDSel").find("option:selected").text();
      ope_id_fk  = $("#opeIDSel").find("option:selected").text();
      rowIDs = $("#subGroupListGrd").jqGrid("getDataIDs");
      if(action_flg=="A"){
        sub_grp_no =  "000000" ;
        if(rowIDs.length>0){
          for(var i=0;i<rowIDs.length;i++){
            gridRowData = $("#subGroupListGrd").jqGrid("getRowData",rowIDs[i]);

            if(gridRowData.tool_id_fk == tool_id_fk &&
               gridRowData.mdl_id_fk  == mdl_id_fk  &&
               gridRowData.path_id_fk == path_id_fk &&
               gridRowData.ope_id_fk  == ope_id_fk  &&
               gridRowData.cus_id_fk  == cus_id_fk  &&
               gridRowData.wo_id_fk   == wo_id_fk ){
                 showErrorDialog("003","重复的记录");
                 return false;
            }
            if(gridRowData.sub_grp_no>sub_grp_no){
              sub_grp_no = gridRowData.sub_grp_no;
            }
          }
          sub_grp_no = numToString(parseInt( sub_grp_no ,10)+1,6);
        }else{
          sub_grp_no = "000001";
        }
      }else{
        selectRowID   = $("#subGroupListGrd").jqGrid("getGridParam","selrow");
        selectRowData = $("#subGroupListGrd").jqGrid("getRowData",selectRowID);
        sub_grp_no    = selectRowData.sub_grp_no;
      }
      
      
      iary = {
          col_typ_fk :$("#subGroupEdit_colTypTxt").val(),
          grp_no_fk  :$("#subGroupEdit_grpNoTxt").val(),
          sub_grp_no : sub_grp_no ,
          cus_id_fk  : cus_id_fk ,
          wo_id_fk   : wo_id_fk,
          tool_id_fk : tool_id_fk ,
          mdl_id_fk  : mdl_id_fk  ,
          path_id_fk : path_id_fk ,
          ope_id_fk  : ope_id_fk  
      };
      inTrxObj ={
        trx_id     : "SPCGRPDT" ,
        action_flg : action_flg ,
        iary       : iary
      };
      outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code=="0000000"){
        if(action_flg=="A"){
          newRowID = getGridNewRowID("#subGroupListGrd");
          $("#subGroupListGrd").jqGrid("addRowData",newRowID,iary);
          showSuccessDialog("新增subGroup成功");  
        }else{
          $("#subGroupListGrd").jqGrid("setRowData",selectRowID,iary);
          showSuccessDialog("修改sugGroup成功");
        }
      } 
    };
    this.setColTypSelFnc=function(){
      $("#colTypSel").empty();
      $("#colTypSel").append("<option value="+"2"+">"+"Process" +"</option>");
      $("#colTypSel").select2({
 	    	theme : "bootstrap"
 	    });
//      $("#colTypSel").append("<option value="+"2"+">"+"Other" +"</option>");
//      $("#colTypSel").append("<option value="+"2"+">"+"Measure" +"</option>");
    };
    this.setOpeListFnc=function(){
      var inTrxObj ={
        trx_id     : "XPBISOPE" ,
        action_flg : "L"        
      };
      this.setSelectObjByinTrx("#opeIDSel",inTrxObj,"ope_id","*");
    };
    this.setToolListFnc=function(){
      var inTrxObj ={
        trx_id     : "XPBISTOL" ,
        action_flg : "L"        
      };
      this.setSelectObjByinTrx("#toolIDSel",inTrxObj,"tool_id","*");
    };
    this.setMdlIDListFnc=function(){
      var iary ={

      };
      var inTrxObj ={
        trx_id     : "XPBMDLDF" ,
        action_flg : "Q"        ,  
        iary       : iary
      };
      this.setSelectObjByinTrx("#mdlIDSel",inTrxObj,"mdl_id","*");
    };
    this.setPathIDListFnc=function(){
      var inTrxObj ={
        trx_id     : "XPBISPTH" ,
        action_flg : "L"        
      };
      var outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if( outTrxObj.rtn_code=="0000000"){
         // this.setSelectDate(outTrxObj.tbl_cnt_a, outTrxObj.oaryA,
			// "path_id", "path_id", "#pathIDSel")
        $("#pathIDSel").empty();
        $("#pathIDSel").append("<option value=*>*</option>");
        var oary = $.isArray(outTrxObj.oaryA) ? outTrxObj.oaryA : [outTrxObj.oaryA];
        var tbl_cnt = comParseInt( outTrxObj.tbl_cnt_a);
        for(var i=0;i<tbl_cnt;i++){
        	$("#pathIDSel").append("<option value="+ oary[i]["path_id"] +">"+ oary[i]["path_id"] +"</option>");
        }
        $("#pathIDSel").select2({
 	    	theme : "bootstrap"
 	    });
      }
    };
    this.setSelectObjByinTrx=function(selectObj,inTrxObj,item,initItem){
      var selectTxt = $(selectObj).find("option:selected").text();
      $(selectObj).empty();
      $(selectObj).append("<option value="+ initItem +">"+ initItem +"</option>") ;
       var  outTrxObj = comTrxSubSendPostJson(inTrxObj);
       if(  outTrxObj.rtn_code == "0000000" ) {
            var tbl_cnt = comParseInt( outTrxObj.tbl_cnt);
            if( tbl_cnt == 1 ){
              $(selectObj).append("<option value="+ outTrxObj.oary[item] +">"+ outTrxObj.oary[item] +"</option>");
            }else if(tbl_cnt>1){
              for(var i=0;i<tbl_cnt;i++){
                $(selectObj).append("<option value="+ outTrxObj.oary[i][item] +">"+ outTrxObj.oary[i][item] +"</option>");
              }
            }
       }
       $(selectObj).select2({
    	   theme : "bootstrap"
       });
//      $(selectObj).val(selectTxt);
    };
    this.setWoIdSel=function(){
    	$("#woIDSel").empty();
    	var inObj = {
    		trx_id: "XPAPLYWO",
    		action_flg : "W"
    	};
    	this.setSelectObjByinTrx("#woIDSel",inObj,"wo_id","*");
    	
    };
    this.setCusIdSel=function(){
    	var inObj, outObj,oaryArr,tblCnt,i;
    	
    	$("#cusIDSel").empty();
    	$("#cusIDSel").append("<option value='*'>*</option>");
		inObj = {
			trx_id : 'XPLSTDAT',
			action_flg : 'Q',
			iary : {
				data_cate : 'CUSD'
			}
		};
		outObj = comTrxSubSendPostJson(inObj);
		if(outObj.rtn_code === "0000000"){
			oaryArr = $.isArray(outObj.oary) ? outObj.oary : [outObj.oary];
			tblCnt = outObj.tbl_cnt;
			for(i=0;i<tblCnt;i++){
				$("#cusIDSel").append("<option value='"+oaryArr[i].data_item + "'>"+oaryArr[i].data_item)+"</option>";
			}
		}
		$("#cusIDSel").select2({
 	    	theme : "bootstrap"
 	    });
    };
  }
  $("#addSubGroupBtn").click(function(){
    subGroup.action_flg = "A";
    subGroup.showSubEditDialog();
  });
  $("#updateSubGroupBtn").click(function(){
    var selRowIDs;
    subGroup.action_flg="U";
    selRowIDs = $("#subGroupListGrd").jqGrid("getGridParam","selarrrow");
    if( selRowIDs.length<=0){
       showErrorDialog("003","请选择需要编辑的行");
       return false;
    }else if(selRowIDs.length>1){
       showErrorDialog("003","不可以同时编辑两行");
       return false;
    }
    subGroup.showSubEditDialog();
  });
  $("#deleteSubGroupBtn").click(function(){
    var selRowIDs = $("#subGroupListGrd").jqGrid("getGridParam","selarrrow");
    if( selRowIDs.length<=0){
       showErrorDialog("003","请选择需要删除的行");
       return false;
    }
    $("#deleteSubGroupBtn").showCallBackWarnningDialog({
        errMsg  : "是否删除Sub Group信息,请确认!!!!",
        callbackFn : function(data) {
          if(data.result==true){
            var iaryList = new Array(),
                delRows  = selRowIDs.concat(),
                delRowData,
                iary,
                inTrxObj;
            for(var i=0;i<delRows.length;i++){
              delRowData  = $("#subGroupListGrd").jqGrid("getRowData",selRowIDs[i]);
              iary = {
                 col_typ_fk : delRowData.col_typ_fk,
                 grp_no_fk  : delRowData.grp_no_fk,
                 sub_grp_no : delRowData.sub_grp_no
              };
              iaryList.push(delRowData);
            }
            
            inTrxObj ={
               trx_id      : "SPCGRPDT" ,
               action_flg  : "D"        ,
               iary        : iaryList  
            };
            outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if( outTrxObj.rtn_code=="0000000"){
              for(var i=0;i<delRows.length;i++){
                $("#subGroupListGrd").jqGrid("delRowData",delRows[i]);
              }
              showSuccessDialog("删除subGroup成功");
            }
          }
        }
     });
  });
  
  /** ********Chart List **************************** */
  var chart = {};
    chart.slaveChartCnt = 1;
    chart.initFnc=function(){
      chart.slaveChartCnt = 1;
    };
    chart.selectSlaveChart = function(){
      this.slaveChartCnt ++;
    };
    chart.action_flg = null;
    chart.chartItemInfoCM =
      [
        {name: 'col_typ_fk'     , index: 'col_typ_fk'      ,label: SPC_COL_TYP_TAG     , width: 80 ,hidden:true },
        {name: 'grp_no_fk'      , index: 'grp_no_fk'       ,label: SPC_GROUP_NO_TAG    , width: 80 ,hidden:true },
        {name: 'chart_no'       , index: 'chart_no'        ,label: SPC_GROUP_NAME_TAG  , width: 80 ,hidden:true },
        {name: 'chart_name'     , index: 'chart_name'      ,label: SPC_CHART_NAME_TAG  , width: 160 },
        {name: 'data_group_fk'  , index: 'data_group_fk'   ,label: DATA_GROUP_TAG      , width: 120 },
        {name: 'chart_typ'      , index: 'chart_typ'       ,label: SPC_CHART_TYPE_TAG  , width: 80  },
        {name: 'chart_owner'    , index: 'chart_owner'     ,label: SPC_CHART_OWNER_TAG , width: 80  },
        {name: 'chart_dsc'      , index: 'chart_dsc'       ,label: SPC_CHART_DSC_TAG   , width: 150 },
        {name: 'sample_size'    , index: 'sample_size'     ,label: SPC_SAMPLE_SIZE_TAG , width: 100 },
        {name: 'max_chart_point', index: 'max_chart_point' ,label: SPC_CHART_MAX_POINT_TAG   , width: 100 },
        {name: 'raw_keep_flg'   , index: 'raw_keep_flg'    ,label: SPC_RAW_KEEP_FLG_TAG   , width: 100 },
        {name: 'chart_sub_typ'  , index: 'chart_sub_typ'   ,label: SPC_CHART_SUB_TYP_TAG  , width: 80 }
      ];
    chart.initChart = function(){
      $("#chartListGrd").jqGrid({
          url:"",
          datatype:"local",
          mtype:"POST",
          height:$("#chartListDiv").height(),
          width:$("#chartListDiv").width()*0.99,
          autowidth:false,
          shrinkToFit:false,
          resizable : true,
          loadonce:true,
          viewrecords : true, // 显示总记录数
          rownumbers  :true ,// 显示行号
          emptyrecords :true ,
          colModel: this.chartItemInfoCM
        });
    };
    chart.showDialog = function(rowData){
        $('#chartEditDialog').modal({
            backdrop:true,
            keyboard:false,
            show:false
        });
//        $('#chartEditDialog').unbind('shown');
        $("#type1Chk").unbind('click');
        $("#type2Chk").unbind('click');
        $("#type3Chk").unbind('click');
        $("#chartTypSel").unbind("change");
        $("#chartEditDialog_saveBtn").unbind('click');
//        $('#chartEditDialog').bind('shown',chart.initialDialogFnc);
        $("#chartEditDialog_saveBtn").bind('click',chart.saveFnc);
        $("#chartEditDialogForm").attr({"disabled":false});
        $("#chartEditDialogForm input").attr({"disabled":false});
        $("#chartEditDialogForm select").attr({"disabled":false});
        
        chart.initialDialogFnc();

        $("#chartTypSel").bind("change",function(){
          chart.chartChangeFnc($(this).find("option:selected").text());
        });

        $("#type1Chk").bind('click',function(){
            if ($(this).attr("checked") == "checked") { 
              if($("#type2Chk").attr("checked") == "checked" &&
                 $("#type2Spn").text()=="SIGMA"){
                    showErrorDialog("003","SIGMA和Range Chart 只能选择一种");
                    $(this).attr("checked",false);
                    return false;               
              }
              chart.typeCheckFnc(2,true,$("#type1Spn").text());
            }else{
              chart.typeCheckFnc(2,false);
            }
        });
        $("#type2Chk").bind('click',function(){
        	if(!$(this).hasClass("hiddenCss")){
          if ($(this).attr("checked") == "checked" ){
              if($("#type1Chk").attr("checked") == "checked" &&
                 $("#type1Spn").text()=="RANGE"){
                    showErrorDialog("003","SIGMA和Range Chart 只能选择一种");
                    $(this).attr("checked",false);
                    return false;              
              } 
              chart.typeCheckFnc(2,true,$("#type2Spn").text());
            }else{
              chart.typeCheckFnc(2,false);
          }
         }
        });
        $("#type3Chk").bind('click',function(){
          if(!$(this).hasClass("hiddenCss")){
          if ($(this).attr("checked") == "checked") { 
              chart.typeCheckFnc(3,true,$("#type3Spn").text());
            }else{
              chart.typeCheckFnc(3,false);
          }
         }
        });
        $('#chartEditDialog').modal("show");
    };
    // chart.typeCheckFnc = function(index){
    chart.typeCheckFnc=function(chartIndex,isShow,chartTyp){
      switch(chartIndex){
        case 2:
          if(isShow==true){
            $("#div2").removeClass("hiddenCss");  
            $("#div2 input").val("");
            $("#chart2Lbel").text(chartTyp);
            $("#div2_checkSpecFlgChk").attr("checked",true);
          }else{
            $("#div2").addClass("hiddenCss"); 
          }
          break;
        case 3:
          if(isShow==true){
            $("#div3").removeClass("hiddenCss");
            $("#div3 input").val(""); 
            $("#chart3Lbel").text(chartTyp); 
            $("#div3_checkSpecFlgChk").attr("checked",true);
          }else{
            $("#div3").addClass("hiddenCss");  
          }
          break;  
      }
    };

    chart.initialDialogFnc = function(){
      var groupRowData,
          chartRowData,
          iary,
          inTrxObj,
          outTrxObj,
          oaryB,
          chart_name;

      $("#chartEditDialogForm input").val("");
      $("#chartEditDialogForm select:not(#thicknessTypeSel)").empty();

      // $("#chartEditDialogForm label").text("");
      $("#chartEditDialogForm span").text("");
      setSelectObjValueTxtByData("#dataGroupSel","MLGP","ext_1","data_ext");
      $("#chartTypSel").empty();
      $("#chartTypSel").append('<option value="10">SINGLE</option>');
      $("#chartTypSel").append('<option value="01">MEAN</option>');
/*      $("#chartTypSel").append('<option value="30">MEDIAN</option>');  // NO.03
      $("#chartTypSel").append('<option value="40">P</option>');       // NO.03
      $("#chartTypSel").append('<option value="50">NP</option>');      // NO.03
      $("#chartTypSel").append('<option value="60">C</option>');       // NO.03
*/      $("#chartTypSel").select2({
	    	theme : "bootstrap"
	    });
      groupRowData     = getSeleletRowData("#groupListGrd");
      chartRowData     = getSeleletRowData("#chartListGrd");
      $("#chartEditDialog_colTypTxt").val(groupRowData.col_typ);
      $("#chartEditDialog_grpNoTxt").val(groupRowData.grp_no);
      $("#chartEditDialog_chartNoTxt").val(chartRowData.chart_no);
      $("#div2").addClass("hiddenCss"); 
      $("#div3").addClass("hiddenCss"); 
      if(chart.action_flg=="A"){
    	$("#dataGroupSel").attr({"disabled":false});
        chart.chartChangeFnc($("#chartTypSel").find("option:selected").text());
        $("#thicknessTypeSel").val("O");
        $("#thicknessTypeSel").select2({
  	    	theme : "bootstrap"
  	    }); 

      }else{
    	 $("#dataGroupSel").attr({"disabled":true});
    	 $("#type1Chk").attr("disabled",true);
    	 $("#type2Chk").attr("disabled",true);
    	 $("#type3Chk").attr("disabled",true);
    	
         $("#dataGroupSel").val(chartRowData.data_group_fk).trigger("change");
         $("#chartNameTxt").val(chartRowData.chart_name);
         $("#chartDscTxt").val(chartRowData.chart_dsc);
         $("#chartOwnerTxt").val(chartRowData.chart_owner);
         $("#sampleSizeTxt").val(chartRowData.sample_size);
         $("#maxChartPointTxt").val(chartRowData.max_chart_point);
         $("#chartTypSel").val(chartRowData.chart_typ).trigger("change");
         $("#dataGroupSel").val(chartRowData.data_group_fk).trigger("change");;
         if(chartRowData.chart_sub_typ=="Y"){
            $("#singleFLgChk").attr({"checked":true}); 
         }else{
            $("#singleFLgChk").removeAttr("checked");
         }
         if(chartRowData.raw_keep_flg=="Y"){
            $("#keepRawChk").attr({"checked":true}); 
         }else{
            $("#keepRawChk").removeAttr("checked");
         }
         $("#chartTypSel").attr({"disabled":true});
         chart.chartChangeFnc($("#chartTypSel").find("option:selected").text());
         // 给chart setting 设定 界面组件赋值
         iary = {
            col_typ_fk : groupRowData.col_typ,
            grp_no_fk  : groupRowData.grp_no,
            chart_no   : chartRowData.chart_no,
         };
         inTrxObj ={
            trx_id     : "SPCCHTMN",
            action_flg : "I",
            iary       : iary
         };
         outTrxObj = comTrxSubSendPostJson(inTrxObj,false);
         if(outTrxObj.rtn_code=="0000000") {
            for(var i=0;i<outTrxObj.tbl_cnt;i++){
              if(outTrxObj.tbl_cnt ==1){
                oaryB = outTrxObj.oaryB;
              }else{
                oaryB = outTrxObj.oaryB[i];  
              }
              for(var j=1;j<=3;j++){
                chart_type_name = chart.getChartName(oaryB.chart_typ_fk);
                if( chart_type_name == $("#type"+j+"Spn").text() ){
                  $("#type"+j+"Chk").attr({"checked":true});
                  break;
                }
              }
              
              $("#thicknessTypeSel").val(oaryB.chart_ack_flg);
              $("#thicknessTypeSel").select2({
      	    	theme : "bootstrap"
      	    });
              $("#div"+(i+1)).removeClass("hiddenCss");
              $("#div"+(i+1)+"_uplTxt").val(oaryB.chart_upl);         
              $("#div"+(i+1)+"_uslTxt").val(oaryB.chart_usl);         
              $("#div"+(i+1)+"_uclTxt").val(oaryB.chart_ucl);         
              $("#div"+(i+1)+"_targetTxt").val(oaryB.chart_target);   
              $("#div"+(i+1)+"_lplTxt").val(oaryB.chart_lpl);         
              $("#div"+(i+1)+"_lslTxt").val(oaryB.chart_lsl);         
              $("#div"+(i+1)+"_lclTxt").val(oaryB.chart_lcl);         
              chart_type_name = chart.getChartName(oaryB.chart_typ_fk);
              $("#chart"+(i+1)+"Lbel").text(chart_type_name);
              if(oaryB.chart_spec_chk_flg = "Y"){
                 $("#div"+(i+1)+"_checkSpecFlgChk").attr("checked",true);
              }else{
                 $("#div"+(i+1)+"_checkSpecFlgChk").removeAttr("checked");
              }
            }
         }
      }
      
    };
    chart.chartChangeFnc =function(chartTyp){
      $("#type1Chk").addClass("hiddenCss");
      $("#type2Chk").addClass("hiddenCss");
      $("#type3Chk").addClass("hiddenCss");
      $("#type1Chk").attr("checked",false);
      $("#type2Chk").attr("checked",false);
      $("#type3Chk").attr("checked",false);
      $("#type1Spn").text("");
      $("#type2Spn").text("");
      $("#type3Spn").text("");
      $("#div1_checkSpecFlgChk").attr("checked",true);
      $("#div2").addClass("hiddenCss"); 
      $("#div3").addClass("hiddenCss"); 
      switch(chartTyp){
        case "SINGLE":
            $("#type1Spn").text("MOVINGRANGE");
            $("#type1Chk").removeClass("hiddenCss");
          break;
        case "MEAN" :
            $("#type1Spn").text("RANGE");
            $("#type1Chk").removeClass("hiddenCss");
            $("#type2Spn").text("SIGMA");
            $("#type2Chk").removeClass("hiddenCss");
            $("#type3Spn").text("UNIFORM");
            $("#type3Chk").removeClass("hiddenCss");
          break;
        case "MEDIAN":
            $("#type1Spn").text("MRANGE");
            $("#type1Chk").removeClass("hiddenCss");        	
        	
      }
      $("#div1 input").val("");
      $("#chart1Lbel").text(chartTyp);
    };
    chart.saveFnc=function(){
      var iary,
          iaryB,
          iaryBList = new Array(),
          chartSettingList = new Array(),
          chartSelectRowID,
          chartNewRowID,
          col_typ_fk,
          grp_no_fk,
          chart_no,
          chart_name,
          chart_typ,
          chart_sub_typ,
          chart_dsc,
          chart_owner,
          data_group_fk,
          upl,usl,ucl,target,lpl,lsl,lcl,
          max_chart_point,
          sample_size,
          raw_keep_flg,
          chart_ack_flg,
          evt_usr,
          inTrxObj,
          outTrxObj;

      col_typ_fk  = $("#chartEditDialog_colTypTxt").val();
      grp_no_fk   = $("#chartEditDialog_grpNoTxt").val();
      chart_no    = $("#chartEditDialog_chartNoTxt").val();
      chart_name  = $("#chartNameTxt").val();
      data_group_fk = $("#dataGroupSel").val();
      chart_dsc   = $("#chartDscTxt").val();
      chart_typ   = $("#chartTypSel").val();
      chart_owner = $("#chartOwnerTxt").val();
      sample_size = $("#sampleSizeTxt").val();
      max_chart_point=$("#maxChartPointTxt").val();
      chart_ack_flg = $("#thicknessTypeSel").val();

      evt_usr     = $("#userId").text();
      if($("#singleFLgChk").attr("checked")=="checked"){
        chart_sub_typ = "Y";
      }else{
        chart_sub_typ = "N";
      }
       if($("#keepRawChk").attr("checked")=="checked"){
        raw_keep_flg = "Y";
      }else{
        raw_keep_flg = "N";
      }

      
      iary ={
          col_typ_fk    : col_typ_fk,
          grp_no_fk     : grp_no_fk,
          chart_name    : chart_name,
          data_group_fk : data_group_fk,
          chart_dsc     : chart_dsc,
          chart_typ     : chart_typ,
          chart_owner   : chart_owner,
          sample_size   : sample_size,
          max_chart_point:max_chart_point,
          chart_sub_typ : chart_sub_typ,
          raw_keep_flg  : raw_keep_flg,
          evt_usr       : evt_usr
        };
      inTrxObj = {
        trx_id     : "SPCCHTMN"       ,
        action_flg : chart.action_flg 
      };
      upl = $("#div1_uplTxt").val();
      usl = $("#div1_uslTxt").val();
      ucl = $("#div1_uclTxt").val();
      target=$("#div1_targetTxt").val();
      lpl=$("#div1_lplTxt").val();
      lsl=$("#div1_lslTxt").val();
      lcl=$("#div1_lclTxt").val();
      
      if(!upl){
    	  showErrorDialog("","数值上限不能为空");
    	  return false;
      }
      if(!usl){
    	  showErrorDialog("","规格上限不能为空");
    	  return false;
      }
      if(!ucl){
    	  showErrorDialog("","控制上限不能为空");
    	  return false;
      }
      if(!target){
    	  showErrorDialog("","标准线不能为空");
    	  return false;
      }
      if(!lpl){
    	  showErrorDialog("","数值下限不能为空");
    	  return false;
      }
      if(!lsl){
    	  showErrorDialog("","规格下限不能为空");
    	  return false;
      }
      if(!lcl){
    	  showErrorDialog("","控制下限不能为空");
    	  return false;
      }
    					  
      // 写入主Chart
      iaryB={
               col_typ_fk   : col_typ_fk ,
               grp_no_fk    : grp_no_fk  ,
               chart_typ_fk : getChartTpyNo($("#chart1Lbel").text()),
               data_group_fk: data_group_fk,
               chart_upl    : upl,
               chart_usl    : usl,
               chart_ucl    : ucl,
               chart_target : target,
               chart_lpl    : lpl,
               chart_lsl    : lsl,
               chart_lcl    : lcl,
               chart_ack_flg : chart_ack_flg
            };
      if($("#div"+i+"_checkSpecFlgChk").attr("checked")=="checked"){
         iaryB.chart_spec_chk_flg = "Y";
      }else{
         iaryB.chart_spec_chk_flg = "N";
      }
      iaryBList.push(iaryB);
      for(var i=1;i<=4;i++){
        if($("#type"+i+"Chk").attr("checked")=="checked"){
          chartSettingList.push(getChartTpyNo($("#chart"+(i+1)+"Lbel").text()))   ;
        }  
      }
      for(var i=1;i<=chartSettingList.length;i++){
          
    	  upl = $("#div"+(i+1)+"_uplTxt").val();
          usl = $("#div"+(i+1)+"_uslTxt").val();
          ucl = $("#div"+(i+1)+"_uclTxt").val();
          target=$("#div"+(i+1)+"_targetTxt").val();
          lpl=$("#div"+(i+1)+"_lplTxt").val();
          lsl=$("#div"+(i+1)+"_lslTxt").val();
          lcl=$("#div"+(i+1)+"_lclTxt").val();
          
          if(!upl){
        	  showErrorDialog("","数值上限不能为空");
        	  return false;
          }
          if(!usl){
        	  showErrorDialog("","规格上限不能为空");
        	  return false;
          }
          if(!ucl){
        	  showErrorDialog("","控制上限不能为空");
        	  return false;
          }
          if(!target){
        	  showErrorDialog("","标准线不能为空");
        	  return false;
          }
          if(!lpl){
        	  showErrorDialog("","数值下限不能为空");
        	  return false;
          }
          if(!lsl){
        	  showErrorDialog("","规格下限不能为空");
        	  return false;
          }
          if(!lcl){
        	  showErrorDialog("","控制下限不能为空");
        	  return false;
          }
          
            iaryB={
               col_typ_fk   : col_typ_fk ,
               grp_no_fk    : grp_no_fk  ,
               chart_typ_fk : getChartTpyNo($("#chart"+(i+1)+"Lbel").text()),
               data_group_fk: data_group_fk,
               chart_upl    : upl,
               chart_usl    : usl,
               chart_ucl    : ucl,
               chart_target : target,
               chart_lpl    : lpl,
               chart_lsl    : lsl,
               chart_lcl    : lcl,
               chart_ack_flg : chart_ack_flg
            };
            if($("#div"+(i+1)+"_checkSpecFlgChk").attr("checked")=="checked"){
               iaryB.chart_spec_chk_flg = "Y";
            }else{
               iaryB.chart_spec_chk_flg = "N";
            }
            iaryBList.push(iaryB);
        }
      if(chart.action_flg=="U"){
         iary.chart_no = $("#chartEditDialog_chartNoTxt").val();
      }
      inTrxObj.iary  = iary ;
      inTrxObj.iaryB = iaryBList;
      outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code=="0000000"){
        if(chart.action_flg=="A"){
          showSuccessDialog("新建chart成功");
          chartNewRowID = getGridNewRowID("#chartListGrd");
          $("#chartListGrd").jqGrid("addRowData",chartNewRowID,outTrxObj.oary);
        }else{
          showSuccessDialog("修改chart成功");
          chartSelectRowID =   $("#chartListGrd").jqGrid("getGridParam","selrow");
          $("#chartListGrd").jqGrid("setRowData",chartSelectRowID,iary);
        }
        $('#chartEditDialog').modal("hide");
      }
    };
    chart.deleteFunc = function(){
       var chartSelectRowID,
           chartRowData,
           iary,
           inTrxObj,
           outTrxObj;
        chartSelectRowID = $("#chartListGrd").jqGrid("getGridParam","selrow");   
        chartRowData = $("#chartListGrd").jqGrid("getRowData",chartSelectRowID);
        iary = {
          col_typ_fk: chartRowData.col_typ_fk,
          grp_no_fk : chartRowData.grp_no_fk,
          chart_no  : chartRowData.chart_no
        };
        inTrxObj = {
          trx_id     : "SPCCHTMN",
          action_flg : "D",
          iary       : iary,
          tbl_cnt    : 1
        };
        outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if(outTrxObj.rtn_code=="0000000"){
          showSuccessDialog("删除Chart图成功");
          $("#chartListGrd").jqGrid("delRowData",chartSelectRowID);
        }
       
    };
    chart.getChartName = function(chart_no){
        var chart_typ_no = "00";
        switch(chart_no){
          case "11" :  
            chart_typ_no = "MOVINGRANGE";break;
          case "10":
            chart_typ_no = "SINGLE";break;
          case "01":
            chart_typ_no = "MEAN";break;
          case "02":
            chart_typ_no = "RANGE";break;
          case "03":
            chart_typ_no = "SIGMA";break;
          case "04":
            chart_typ_no = "UNIFORM";break; 
          case "30":
        	chart_typ_no = "MEDIAN";break;
          case "31":
          	chart_typ_no = "MRANGE";break;
          case "40":
          	chart_typ_no = "P";break;
          case "50":
          	chart_typ_no = "NP";break;
          case "60":
          	chart_typ_no = "C";break;          	
        }
        return chart_typ_no;
    };
  function getChartTpyNo(chart_typ_name){
    var chart_typ_no="00";
    switch(chart_typ_name){
      case "MOVINGRANGE" :  
        chart_typ_no = "11";break;
      case "SINGLE":
        chart_typ_no = "10";break;
      case "MEAN":
        chart_typ_no = "01";break;
      case "RANGE":
        chart_typ_no = "02";break;
      case "SIGMA":
        chart_typ_no = "03";break;
      case "UNIFORM":
        chart_typ_no = "04";break; 
      case "MEDIAN":
        chart_typ_no = "30";break;
      case "MRANGE":
        chart_typ_no = "31";break;
      case "P":
        chart_typ_no = "40";break;
      case "NP":
        chart_typ_no = "50";break;
      case "C":
        chart_typ_no = "60";break;        
    }
    return chart_typ_no;
  }
  $("#addChartBtn").click(function(){
    if($("#groupListGrd").jqGrid("getGridParam","selrow")==null){
      showErrorDialog("003","请选择管制图Group后,再新增chart图");
      return false;
    };

    chart.action_flg="A";
    chart.showDialog(); 
  });
  $("#updateChartBtn").click(function(){
    if($("#chartListGrd").jqGrid("getGridParam","selrow")==null){
      showErrorDialog("003","请选择需要修改的Chart图");
      return false;
    }
    chart.action_flg="U";
    chart.showDialog(); 
  });
  $("#deleteChartBtn").click(function(){
    if($("#chartListGrd").jqGrid("getGridParam","selrow")==null){
      showErrorDialog("003","请选择需要删除的Chart图");
      return false;
    }
    $("#deleteChartBtn").showCallBackWarnningDialog({
        errMsg  : "是否删除Chart图信息,执行此操作Chart数据同时也被删除,请确认!!!!",
        callbackFn : function(data) {
          if(data.result==true){
             chart.action_flg ="D";
             chart.deleteFunc();
          }
        }
      });
  });
  var chartRule = {
	  initChartRuleDialog:function(){
//		  $("#chartRuleDialog").unbind("show");
//		  $("#chartRuleDialog").bind("show",chartRule.showChartRuleDialog);
		  
		  $("#chartRuleDialog_saveBtn").unbind("click");
		  $("#chartRuleDialog_saveBtn").bind("click",chartRule.saveChartRuleFnc);
		  
		  $("#chartRuleDialog").modal("show");
		  chartRule.showChartRuleDialog();
	  },
	  getChartRule : function(chartInfo){
		  var inObj,outObj ,iary,chartRule;
		  iary = {
            col_typ_fk : chartInfo.col_typ,
            grp_no_fk  : chartInfo.grp_no,
            chart_no   : chartInfo.chart_no,
            chart_typ  : chartInfo.chart_typ
          };
          inObj ={
            trx_id     : "SPCCHTMN",
            action_flg : "I",
            iary       : iary
          };
          outObj = comTrxSubSendPostJson(inObj);
          if(outObj.rtn_code === "0000000"){
        	  return outObj.oaryB.chart_rule_1_info;
          }
          return false;
		  
	  },
	  
	  showChartRuleDialog:function(){
		  var i,rule,selRowId,rowData,chartInfo;
		  selRowId = $("#chartListGrd").jqGrid("getGridParam","selrow");
		  if(!selRowId){
			  showErrorDialog("","请选择需要设定规则的Chart图");
		      return false;
		  }
		  rowData = $("#chartListGrd").jqGrid("getRowData",selRowId);
		  chartInfo={};
		  chartInfo.grp_no = rowData.grp_no_fk;
		  chartInfo.col_typ = rowData.col_typ_fk;
		  chartInfo.chart_no = rowData.chart_no;
		  chartInfo.chart_typ = rowData.chart_typ;
		  rule = chartRule.getChartRule(chartInfo);
		  if(!rule){
			  $("#chartRuleDialogForm input:checkbox").attr("checked",false);
		  }else{
			  for(i=0;i<7;i++){
				  if(rule.substr(i,1)=="Y"){
					  $("#rule"+(i+1)+"Chk").attr("checked",true);
				  }else{
					  $("#rule"+(i+1)+"Chk").attr("checked",false);
				  }
			  }
		  }
	  },
	  buildChartRuleFnc:function(){
		  var i,rule;
		  rule="";
		  for(i=0;i<7;i++){
			  if($("#rule"+(i+1)+"Chk").attr("checked")==="checked"){
				  rule = rule + "Y";
			  }else{
				  rule = rule + "N";
			  }
		  }
		  return rule;
	  },
	  saveChartRuleFnc:function(){
		  var inObj,outObj,iaryB,rule,selRowId,rowData;
		  rule = chartRule.buildChartRuleFnc();
		  
		  var selRowId = $("#chartListGrd").jqGrid("getGridParam","selrow");
		  if(!selRowId){
			  showErrorDialog("","请选择需要设定规则的Chart图");
		      return false;
		  }
		  var rowData = $("#chartListGrd").jqGrid("getRowData",selRowId);
		  
		  iaryB={
			  col_typ_fk  :  rowData.col_typ_fk,
			  grp_no_fk   :  rowData.grp_no_fk,
			  chart_no_fk :  rowData.chart_no,
			  chart_typ_fk:  rowData.chart_typ,
			  chart_rule_1_info: rule
		  };
		  
		  inObj = {
			trx_id     : "SPCCHTMN",
			action_flg : "R",
			iaryB      : iaryB
		  };
		  
		  outObj = comTrxSubSendPostJson(inObj);
		  if(outObj.rtn_code === "0000000"){
			  showSuccessDialog("管制图规则设定成功");
		  }
		  
		  
		  
	  }
	  
  };
  
  $("#setChartRuleBtn").click(function(){
// alert("hello world");
// console.info("clicked ..");
//	  
	  chartRule.initChartRuleDialog();
// var rule = chartRule.getChartRule(chartInfo);
	  
// chartRule.showChartRuleDialog(rule);
  });
  $("#f1_query_btn").click(function(){
    queryTool = new QueryTool();
    queryTool.showDialog(); 
  });
  function getSeleletRowData(domObj){
    return $(domObj).jqGrid("getRowData",$(domObj).jqGrid("getGridParam","selrow"));
  }
  function numToString(num,len) {
     var numlen = num.toString().length; // 得到num的长度
     var strChar = "";  // 空缺位填充字符
     var str = num;
     for (var i = 0; i < len - numlen; i++) {
         strChar = strChar +"0";
     }
     str = strChar+num ;
     return str;
  }

  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $(window).bind("onresize", this);  
  };
  $(window).resize(function(){  
    resetJqgrid();
  });
  function initFnc(){
    $("#tabDiv").tab('show');
    mainGroup  = new MainGroup();
    subGroup   = new SubGroup();
    mainGroup.initFnc();
    subGroup.initFnc();
    chart.initChart();
    resetJqgrid();
  }
  initFnc();
});