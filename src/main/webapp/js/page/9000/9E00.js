$(document).ready(function() {
	$("form").submit(function() {
		return false;
	});
	$("#tabDiv").tab('show');
	var VAL = {
		T_XPBISOPE : "XPBISOPE",
		T_XPINQWOR : "XPINQWOR",
		NORMAL : "0000000"
	};
	var domObjs = {
		$woIDTxt : $("#woIDTxt"),
		grids : {
			$holdBoxGrd        : $("#holdBoxGrd"),
			holdBoxGrd         : "#holdBoxGrd",
			$exchangeBoxGrd    : $("#exchangeBoxGrd"),
			exchangeBoxGrd     : "#exchangeBoxGrd",
			$scrpBoxGrd        : $("#scrpBoxGrd"),
			scrpBoxGrd         : "#scrpBoxGrd",
			$exWhBoxGrd        : $("#exWhBoxGrd"),
			exWhBoxGrd         : "#exWhBoxGrd"
		}

	};
	var globalBean = {
		opeObjs : {},
		getOpeDsc : function(opeID, opeVer) {
			var i, oary;
			for (i = 0; i < this.opeObjs.length; i++) {
				oary = this.opeObjs[i];
				if (typeof (opeVer) === "undefined") {
					if (opeID == oary.ope_id) {
						return oary.ope_dsc
					}
				} else {
					if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
						return oary.ope_dsc
					}
				}
			}
		},
		woStat : {
			WAIT : "正常",
			CLOS : "关闭"
		},
		prdStat : {
			WAIT : "等待入账",
			INPR : "制程中",
			SHIP_2 : "半成品入库",
			SHIP_0 : "成品入库",
			SHIP_1 : "成品入库",
			COMP : "完成",
			SHTC : "出货",
			SCRP : "报废"
		},
		getPrdStat : function(prdStat, bnkFlg) {
			if (typeof (bnkFlg) === "undefined") {
				return this.prdStat[prdStat];
			}
			return this.prdStat[prdStat + "_" + bnkFlg];
		}
	};
	function initFnc() {
		$("input").val("");
		var inObj, outObj;

		inObj = {
			trx_id : VAL.T_XPBISOPE,
			action_flg : 'L'
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			globalBean.opeObjs = outObj.oary;
		}
	}
	initFnc();
	domObjs.grids.$holdBoxGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 500,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : "#holdBoxPg",
		colModel : [ {
			name : 'box_id',
			index : 'box_id',
			label : "保留箱号",
			width : BOX_ID_CLM
		}, {
			name : 'mdl_id',
			index : 'mdl_id',
			label : "产品ID",
			width : MDL_ID_CLM
		}, {
			name : 'lot_id',
			index : 'lot_id',
			label : "批次号",
			width : LOT_ID_CLM
		}, {
			name : 'wo_id',
			index : 'wo_id',
			label : "内部订单号",
			width : WO_ID_CLM
		}, {
			name : 'ope_id',
			index : 'ope_id',
			label : "保留站",
			width : OPE_ID_CLM
		}, {
			name : 'hold_usr',
			index : 'hold_usr',
			label : "保留操作人",
			width : HOLD_USR_CLM
		}, {
			name : 'hold_timestamp',
			index : 'hold_timestamp',
			label : "保留时间",
			width : HOLD_TIMESTAMP_CLM
		}, {
			name : 'cancel_hold_usr',
			index : 'cancel_hold_usr',// 内部订单的状态
			label : "取消保留人",
			width : CANCEL_HOLD_USR_CLM
		}, {
			name : 'cancel_hold_timestamp',
			index : 'cancel_hold_timestamp',
			label : "取消保留时间",
			width : CANCEL_HOLD_TIMESTAMP_CLM
		}, {
			name : 'so_id',
			index : 'so_id',
			label : "生产订单",
			width : SO_ID_CLM
		}],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.grids.$exchangeBoxGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 500,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : "#woQtyPg",
		colModel : [ {
			name : 'ex_box1_id',
			index : 'ex_box1_id',
			label : "交换箱1号",// 内部订单总数量
			width : EX_BOX1_ID_CLM
		}, {
			name : 'ex_box2_id',
			index : 'ex_box2_id',
			label : "交换箱2号",
			width : EX_BOX2_ID_CLM
		}, {
			name : 'mdl_id',
			index : 'mdl_id',
			label : "产品ID",
			width : MDL_ID_CLM
		}, {
			name : 'lot_id',
			index : 'lot_id',
			label : "批次号",
			width : LOT_ID_CLM
		}, {
			name : 'wo_id',
			index : 'wo_id',
			label : "内部订单号",
			width : WO_ID_CLM
		}, {
			name : 'ex_ope_id',
			index : 'ex_ope_id',
			label : "交换站",
			width : EX_OPE_ID_CLM
		}, {
			name : 'ex_usr_id',
			index : 'ex_usr_id',
			label : "交换操作人",
			width : EX_USR_ID_CLM
		}, {
			name : 'ex_evt_timestamp',
			index : 'ex_evt_timestamp',
			label : "交换时间",
			width : 80
		} ],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.grids.$scrpBoxGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 500,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : "#scrpBoxGrd",
		colModel : [ {
			name : 'scrp_box_id',// 来自RET_WO.CUS_ID_FK
			index : 'scrp_box_id',
			label : "报废箱",
			width : SCRP_BOX_ID_CLM
		}, {
			name : 'prd_seq_id',
			index : 'prd_seq_id',
			label : "玻璃ID",
			width : PRD_SEQ_ID_CLM
		}, {
			name : 'lot_id',
			index : 'lot_id',
			label : "批次号",// 来自RET_SHIP_NOTICE.SHIP_DATE
			width : LOT_ID_CLM
		}, {
			name : 'wo_id',// 来自RET_WO.MDL_ID_FK
			index : 'wo_id',
			label : "内部订单号",
			width : WO_ID_CLM
		}, {
			name : 'scrp_ope_id',
			index : 'scrp_ope_id',
			label : "报废站",
			width : SCRP_OPE_ID_CLM
		}, {
			name : 'scrp_usr_id',
			index : 'scrp_usr_id',
			label : "报废人",
			width : SCRP_USR_ID_CLM
		}, {
			name : 'scrp_timestamp',
			index : 'scrp_timestamp',
			label : "报废时间",
			width : SCRP_TIMESTAMP_CLM
		} ],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.grids.$exWhBoxGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 600,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		colModel : [ {
			name : 'ex_wh_box',
			index :'ex_wh_box',
			label : "转仓箱",
			width : EX_WH_BOX_CLM
		}, {
			name : 'prd_qty',
			index : 'prd_qty',
			label : "数量",
			width : PRD_QTY_CLM
		}, {
			name : 'ex_wh_out_dest_shop',
			index : 'ex_wh_out_dest_shop',
			label : "转出仓库仓位",
			width : EX_WH_OUT_DEST_SHOP_CLM
		}, {
			name  : 'ex_wh_in_dest_shop',
			index : 'ex_wh_in_dest_shop',
			label : "转入仓库仓位",
			width : EX_WH_IN_DEST_SHOP_CLM
		}, {
			name : 'wo_id',
			index : 'wo_id',
			label : "内部订单号",
			width : WO_ID_CLM
		}, {
			name : 'lot_id',
			index : 'lot_id',
			label : "批次号",
			width : LOT_ID_CLM
		}, {
			name : 'ex_wh_out_usr',
			index : 'ex_wh_out_usr',
			label : "转出人",
			width : EX_WH_OUT_USR_CLM
		}, {
			name : 'ex_wh_out_timestamp',
			index : 'ex_wh_out_timestamp',
			label : "转出时间",
			width : EX_WH_OUT_TIMESTAMP_CLM
		}, {
			name : 'ex_wh_in_usr',
			index : 'ex_wh_in_usr',
			label : "接收人",
			width : EX_WH_IN_USR_CLM
		}, {
			name : 'ex_wh_in_timestamp',
			index : 'ex_wh_in_timestamp',
			label : "接收时间",
			width : EX_WH_IN_TIMESTAMP_CLM
		}],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	function initFnc() {
		Trans.addTrans("STAT");
	}
	initFnc();

	function f1QueryFnc() {
		Trans.getTrans("STAT");
		console.info(Trans.getTran("STAT","WAIT"));
		return false;
		
		var woID, inTrx, outTrx, boxCnt, i, oary, oaryArr, opeDsc, prdStat;
		var oary1, oary2, oary3, oary1Arr, oary2Arr, oary3Arr, oary1_cnt, oary2_cnt, oary3_cnt, i;
		woID = domObjs.$woIDTxt.val();
		if (!woID) {
			showErrorDialog("003", "请输入内部订单号");
			return false;
		}
		inTrx = {
			trx_id : VAL.T_XPINQWOR,
			action_flg : "M",
			worder_id : woID
		};
		outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == VAL.NORMAL) {
			oary1Arr = outTrx.oary1;
			oary2Arr = outTrx.oary2;
			oary3Arr = outTrx.oary3;
			oary4Arr = outTrx.oary4;
			oary1_cnt = outTrx.oary1_cnt;
			oary2_cnt = outTrx.oary2_cnt;
			oary3_cnt = outTrx.oary3_cnt;
			oary4_cnt = outTrx.oary4_cnt;
			for (i = 0; i < oary1_cnt; i++) {
				oary1 = oary1_cnt > 1 ? outTrx.oary1[i] : outTrx.oary1;
				oary1.ope_id = globalBean.getOpeDsc(oary1.ope_id);
				oary1.wo_stat = globalBean.woStat[oary1.wo_stat];
				prdStat = oary1.prd_stat;
				if (prdStat === "SHIP") {
					oary1.prd_stat = globalBean.getPrdStat(prdStat, oary1.bnk_flg);
				} else {
					oary1.prd_stat = globalBean.getPrdStat(prdStat);
				}
			}
			for(i=0;i<oary4_cnt;i++){
				oary4 = oary4_cnt > 1 ? oary4Arr[i] : oary4Arr ;
				oary4.ope_id = globalBean.getOpeDsc(oary4.ope_id);
			}
			setGridInfo(outTrx.oary1, domObjs.grids.woInfoGrdSelector);
			setGridInfo(outTrx.oary2, domObjs.grids.woQtyGrdSelector);
			setGridInfo(outTrx.oary3, domObjs.grids.shipNoticeGrdSelector);
			setGridInfo(outTrx.oary4, domObjs.grids.$woDpsInfoListGrd);
		}
	}
	domObjs.$woIDTxt.keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
	})
	$("#f1_query_btn").click(f1QueryFnc);
	$("#f2ExportWoInfo").click(function(){
		generateExcel("#woInfoGrd");
	});
	$("#f3ExportSoInfo").click(function(){
		generateExcel("#woQtyGrd");
	});
	$("#f4ExportDnInfo").click(function(){
		generateExcel("#shipNoticeGrd");
	});
	$("#f5ExportDpsPlanInfo").click(function(){
		generateExcel("#woDpsInfoListGrd");
	});

})