$(document).ready(function() {

	$("#tabDiv").tab('show');
	$("form").submit(function() {
		return false;
	});
	var VAL = {
		T_XPLSTDAT : "XPLSTDAT",
		T_XPINQOPE : "XPINQOPE",
		T_XPBMDLDF : "XPBMDLDF",
		T_XPAPLYWO : "XPAPLYWO",
		NORMAL : "0000000"
	};
	var domObjs = {
		$window : $(window),
		cusIDSel : $("#cusIDSel"),
		mdlIDSel : $("#mdlIDSel"),
		woIDSel  : $("#woIDSel"),
		beginTimeTxt : $("#beginTimeTxt"),
		endTimeTxt   : $("#endTimeTxt"),
		grids : {
			$soInfoListDiv :$("#tabPane_so"),
			$soInfoListGrd :$("#soInfoListGrd"),
			$woInfoListDiv :$("#tabPane_wo"),
			$woInfoListGrd :$("#woInfoListGrd"),
			$dpsInfoListDiv:$("#tabPane_dps"),
			$dpsInfoListGrd:$("#dpsInfoListGrd"),
			$dnInfoListDiv :$("#tabPane_dn"),
			$dnInfoListGrd :$("#dnInfoListGrd"),
		}
	};
	function queryMdlInf(){
		var inObj,outObj;
		var cus_id = domObjs.cusIDSel.val();
		if(!cus_id){
			domObjs.cusIDSel.empty();
			return false;
		}
		var iary={
			cus_id : cus_id
		}
		inObj = {
			trx_id : VAL.T_XPBMDLDF,
			action_flg : 'Q',
			iary : iary
		}
		outObj = comTrxSubSendPostJson(inObj);
		if(outObj.rtn_code == VAL.NORMAL){
			_setSelectDate(outObj.tbl_cnt, outObj.oary, "mdl_id", "mdl_id", "#mdlIDSel",true);
		}
	}
	function queryWoInf(){
		var inObj,outObj;
		var mdl_id = domObjs.mdlIDSel.val();
		var iary={
			mdl_id : mdl_id
		}
		var inObj={
			trx_id : VAL.T_XPAPLYWO,
			action_flg : "Q",
			iary : iary
		}
		var outObj = comTrxSubSendPostJson(inObj);
		if(outObj.rtn_code == VAL.NORMAL){
			_setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woIDSel",true);
		}
	}
	function disPlay() {
		$("input").val("");
		$("#soInfoListGrd").jqGrid("clearGridData");
		$("#woInfoListGrd").jqGrid("clearGridData");
		$("#dpsInfoListGrd").jqGrid("clearGridData");
		$("#dnInfoListGrd").jqGrid("clearGridData");
		var inObj, outObj;
		var iary ={
			data_cate :"CUSD"
		}
		inObj = {
			trx_id : VAL.T_XPLSTDAT,
			action_flg : 'Q',
			iary   : iary
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			_setSelectDate(outObj.tbl_cnt, outObj.oary, "data_item", "data_ext", "#cusIDSel",true);
			// globalBean.opeObjs = outObj.oary;
		}
	}
	var SoitemInfoCM = [ {
		name  : 'so_id',
		index : 'so_id',
		label : SO_ID_TAG,
		width : SO_ID_CLM,
		align : "center"
	}, {
		name  : 'pln_prd_qty',
		index : 'pln_prd_qty',
		label : "订单"+TOTAL_QTY_TAG,
		width : SO_PLN_PRD_QTY_CLM,
		align : "center"
	}, {
		name  : "rl_prd_qty",
		index : "rl_prd_qty",
		label : RL_PRD_QTY_TAG,
		width : RL_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'rl_yiled',//投产率
		index : 'rl_yiled',
		label : RELEASE_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name : 'no_rl_prd_qty',
		index : 'no_rl_prd_qty',
		label : NO_RL_PRD_QTY_TAG,
		width : UN_RL_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'comp_prd_qty',
		index : 'comp_prd_qty',
		label : COMP_QTY_TAG,
		width : COMP_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'comp_yiled',
		index : 'comp_yiled',
		label : COMP_YILED_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name  : 'no_comp_prd_qty',
		index : 'no_comp_prd_qty',
		label : NO_COMP_QTY_TAG,
		width : COMP_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'wh_out_prd_qty',
		index : 'wh_out_prd_qty',
		label : WH_OUT_PRD_QTY_TAG+DEF_QTY_TAG,
		width : WH_OUT_PRD_QTY_CLM,
		align : "center"
	}, {
		name : "wh_out_yiled",
		index : "wh_out_yiled",
		label : WH_OUT_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name : "no_wh_out_prd_qty",
		index : "no_wh_out_prd_qty",
		label : "未交货数量",
		width : UN_WH_OUT_PRD_QTY_CLM,
		align : "center"
	}, {
		name : "gk_grade_cnt",
		index : "gk_grade_cnt",
		label : GRADE_CONTROL_TAG,
		width : GK_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "ng_grade_cnt",
		index : "ng_grade_cnt",
		label : NG_CNT_TAG,
		width : NG_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "lz_grade_cnt",
		index : "lz_grade_cnt",
		label : GRADE_SCRP_TAG,
		width : LZ_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "ok_yiled",
		index : "ok_yiled",
		label : OK_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}];
	$("#soInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 390,
		width  : $("#tabPane_so").width()*0.99,
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : $("#soInfoListPg"),
		fixed : true,
		colModel : SoitemInfoCM,
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	/**
	 * Wo Tab
	 */
	var WoitemInfoCM = [ {
		name  : 'wo_id',
		index : 'wo_id',
		label : WO_ID_TAG,
		width : WO_ID_CLM,
		align : "center"
	}, {
		name  : 'pln_prd_qty',
		index : 'pln_prd_qty',
		label : "订单"+TOTAL_QTY_TAG,
		width : SO_PLN_PRD_QTY_CLM,
		align : "center"
	}, {
		name  : "rl_prd_qty",
		index : "rl_prd_qty",
		label : RL_PRD_QTY_TAG,
		width : RL_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'rl_yiled',//投产率
		index : 'rl_yiled',
		label : RELEASE_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name : 'no_rl_prd_qty',
		index : 'no_rl_prd_qty',
		label : NO_RL_PRD_QTY_TAG,
		width : UN_RL_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'comp_prd_qty',
		index : 'comp_prd_qty',
		label : COMP_QTY_TAG,
		width : COMP_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'comp_yiled',
		index : 'comp_yiled',
		label : COMP_YILED_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name  : 'no_comp_prd_qty',
		index : 'no_comp_prd_qty',
		label : NO_COMP_QTY_TAG,
		width : COMP_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'wh_out_prd_qty',
		index : 'wh_out_prd_qty',
		label : WH_OUT_PRD_QTY_TAG+DEF_QTY_TAG,
		width : WH_OUT_PRD_QTY_CLM,
		align : "center"
	}, {
		name : "wh_out_yiled",
		index : "wh_out_yiled",
		label : WH_OUT_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name : "no_wh_out_prd_qty",
		index : "no_wh_out_prd_qty",
		label : "未交货数量",
		width : UN_WH_OUT_PRD_QTY_CLM,
		align : "center"
	}, {
		name : "gk_grade_cnt",
		index : "gk_grade_cnt",
		label : GRADE_CONTROL_TAG,
		width : GK_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "ng_grade_cnt",
		index : "ng_grade_cnt",
		label : NG_CNT_TAG,
		width : NG_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "lz_grade_cnt",
		index : "lz_grade_cnt",
		label : GRADE_SCRP_TAG,
		width : LZ_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "ok_yiled",
		index : "ok_yiled",
		label : OK_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}];
	$("#woInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 390,
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : $("#woInfoListPg"),
		fixed : true,
		colModel : WoitemInfoCM,
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	/**
	 * DN Tab
	 */
	var DNitemInfoCM = [ {
		name  : 'dn_no',
		index : 'dn_no',
		label : DN_NO_TAG,
		width : DN_NO_CLM,
		align : "center"
	}, {
		name  : 'pln_prd_qty',
		index : 'pln_prd_qty',
		label : "订单"+TOTAL_QTY_TAG,
		width : SO_PLN_PRD_QTY_CLM,
		align : "center"
	}, {
		name  : "rl_prd_qty",
		index : "rl_prd_qty",
		label : RL_PRD_QTY_TAG,
		width : RL_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'rl_yiled',//投产率
		index : 'rl_yiled',
		label : RELEASE_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name : 'no_rl_prd_qty',
		index : 'no_rl_prd_qty',
		label : NO_RL_PRD_QTY_TAG,
		width : UN_RL_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'comp_prd_qty',
		index : 'comp_prd_qty',
		label : COMP_QTY_TAG,
		width : COMP_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'comp_yiled',
		index : 'comp_yiled',
		label : COMP_YILED_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name  : 'no_comp_prd_qty',
		index : 'no_comp_prd_qty',
		label : NO_COMP_QTY_TAG,
		width : COMP_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'wh_out_prd_qty',
		index : 'wh_out_prd_qty',
		label : WH_OUT_PRD_QTY_TAG+DEF_QTY_TAG,
		width : WH_OUT_PRD_QTY_CLM,
		align : "center"
	}, {
		name : "wh_out_yiled",
		index : "wh_out_yiled",
		label : WH_OUT_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name : "no_wh_out_prd_qty",
		index : "no_wh_out_prd_qty",
		label : "未交货数量",
		width : UN_WH_OUT_PRD_QTY_CLM,
		align : "center"
	}, {
		name : "gk_grade_cnt",
		index : "gk_grade_cnt",
		label : GRADE_CONTROL_TAG,
		width : GK_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "ng_grade_cnt",
		index : "ng_grade_cnt",
		label : NG_CNT_TAG,
		width : NG_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "lz_grade_cnt",
		index : "lz_grade_cnt",
		label : GRADE_SCRP_TAG,
		width : LZ_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "ok_yiled",
		index : "ok_yiled",
		label : OK_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}];
	$("#dnInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 390,
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : $("#dnInfoListPg"),
		fixed : true,
		colModel : DNitemInfoCM,
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	/**
	 * DPS Tab
	 */
	var DPSitemInfoCM = [ {
		name  : 'dps_id_level1',
		index : 'dps_id_level1',
		label : DPS_ID_LEVEL1_TAG,
		width : DPS_ID_CLM,
		align : "center"
	}, {
		name  : 'dps_id_level2',
		index : 'dps_id_level2',
		label : DPS_ID_LEVEL2_TAG,
		width : DPS_ID_CLM,
		align : "center"
	},{
		name  : 'dps_id_level3',
		index : 'dps_id_level3',
		label : DPS_ID_LEVEL3_TAG,
		width : DPS_ID_CLM,
		align : "center"
	},{
		name  : 'pln_prd_qty',
		index : 'pln_prd_qty',
		label : "订单"+TOTAL_QTY_TAG,
		width : SO_PLN_PRD_QTY_CLM,
		align : "center"
	}, {
		name  : "rl_prd_qty",
		index : "rl_prd_qty",
		label : RL_PRD_QTY_TAG,
		width : RL_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'rl_yiled',//投产率
		index : 'rl_yiled',
		label : RELEASE_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name : 'no_rl_prd_qty',
		index : 'no_rl_prd_qty',
		label : NO_RL_PRD_QTY_TAG,
		width : UN_RL_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'comp_prd_qty',
		index : 'comp_prd_qty',
		label : COMP_QTY_TAG,
		width : COMP_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'comp_yiled',
		index : 'comp_yiled',
		label : COMP_YILED_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name  : 'no_comp_prd_qty',
		index : 'no_comp_prd_qty',
		label : NO_COMP_QTY_TAG,
		width : COMP_PRD_QTY_CLM,
		align : "center"
	}, {
		name : 'wh_out_prd_qty',
		index : 'wh_out_prd_qty',
		label : WH_OUT_PRD_QTY_TAG+DEF_QTY_TAG,
		width : WH_OUT_PRD_QTY_CLM,
		align : "center"
	}, {
		name : "wh_out_yiled",
		index : "wh_out_yiled",
		label : WH_OUT_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}, {
		name : "no_wh_out_prd_qty",
		index : "no_wh_out_prd_qty",
		label : "未交货数量",
		width : UN_WH_OUT_PRD_QTY_CLM,
		align : "center"
	}, {
		name : "gk_grade_cnt",
		index : "gk_grade_cnt",
		label : GRADE_CONTROL_TAG,
		width : GK_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "ng_grade_cnt",
		index : "ng_grade_cnt",
		label : NG_CNT_TAG,
		width : NG_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "lz_grade_cnt",
		index : "lz_grade_cnt",
		label : GRADE_SCRP_TAG,
		width : LZ_GRADE_CNT_CLM,
		align : "center"
	}, {
		name : "ok_yiled",
		index : "ok_yiled",
		label : OK_YIELD_TAG,
		width : YIELD_CLM,
		align : "center"
	}];
	$("#dpsInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 390,
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : $("#dpsInfoListPg"),
		fixed : true,
		colModel : DPSitemInfoCM,
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	$("#beginTimeTxt, #endTimeTxt").datepicker({
		defaultDate : "",
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1
	});
	domObjs.cusIDSel.change(function(){
		queryMdlInf();
    });
	domObjs.mdlIDSel.change(function(){
		queryWoInf();
	});
	
	disPlay();
});