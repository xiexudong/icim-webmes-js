$(document).ready(function() {

	$("#tabDiv").tab('show');
	$("form").submit(function() {
				return false;
			});
	var VAL = {
		T_XPAPLYDN : "XPAPLYDN",
		NORMAL : "0000000"
	};
	var domObjs = {
		$window : $(window),
		dnIDTxt : $("#dnIDTxt"),
		zdbeginTimeTxt : $("#beginTimeTxt1"),
		zdendTimeTxt : $("#endTimeTxt1"),
		jhbeginTimeTxt : $("#beginTimeTxt2"),
		jhendTimeTxt : $("#endTimeTxt2"),
		grids:{
			$woInfoListDiv:$("#tabPane_wo"),
			$woInfoListGrd:$("#woInfoListGrd")
		}
	};
	$("#beginTimeTxt1, #endTimeTxt1,#beginTimeTxt2, #endTimeTxt2").datepicker({
		defaultDate : "",
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1
	});
	function disPlay() {
		$("input").val("");
		$("#woInfoListGrd").jqGrid("clearGridData");
		$("#boxInfoListGrd").jqGrid("clearGridData");
	}
	disPlay();
	var WoitemInfoCM = [
			{name : 'cus_id_fk'     , index : 'cus_id_fk'     , label : CUS_ID_TAG             , width : CUS_ID_CLM,align:"center"},
			{name : 'dn_stat'       , index : 'dn_stat'       , label : DELIVERY_ORDER_STAT_TAG, width : DN_STAT_CLM,align:"center"},
			{name : 'ship_date'     , index : 'ship_date'     , label : DELIVERY_DATE_TAG      , width : DATE_CLM,align:"center"},
			{name : 'crt_usr'       , index : 'crt_usr'       , label : CRT_USER_ID_TAG        , width : USER_CLM,align:"center"},
			{name : 'crt_timestamp' , index : 'crt_timestamp' , label : CRT_DATE_TAG           , width : TIMESTAMP_CLM,align:"center"},
			{name: 'dn_no'          , index: 'dn_no'          , label: "交货订单号",             width: DN_NO_CLM},
			{name : 'wo_id_fk'      , index : 'wo_id_fk'      , label : WO_ID_TAG              , width : WO_ID_CLM,align:"center"},
			{name : 'pln_prd_qty'   , index : 'pln_prd_qty'   , label : "订单数"               , width : QTY_CLM,align:"center"},
			{name : 'mdl_id_fk'     , index : 'mdl_id_fk'     , label : "交货型号"             , width : MDL_ID_CLM,align:"center"},
			{name : 'ship_qty'      , index : 'ship_qty'      , label : PLN_SHIP_QTY_TAG       , width : QTY_CLM,align:"center"},
			{name : 'wh_in_prd_qty' , index : 'wh_in_prd_qty' , label : PRD_WH_IN_QTY_TAG      , width : QTY_CLM,align:"center"},
			{name : 'wh_out_prd_qty', index : 'wh_out_prd_qty', label : WH_OUT_PRD_QTY_TAG     , width : QTY_CLM,align:"center"},
            {name: 'plan_shipping_timestamp', index: 'plan_shipping_timestamp' , label: "计划交货时间",        width: EVT_TIMESTAMP_CLM},
			{name : 'shipping_timestamp', index : 'shipping_timestamp', label : WH_OUT_DATE_TAG, width : TIMESTAMP_CLM,align:"center"},
			{name : 'no_out_prd_qty', index : 'no_out_prd_qty', label : NO_OUT_PRD_QTY_TAG     , width : QTY_CLM,align:"center"},
			{name : 'wip_qty'       , index : 'wip_qty'       , label : WIP_QTY_TAG            , width : QTY_CLM,align:"center"},
	];
	$("#woInfoListGrd").jqGrid({
			url : "",
			datatype : "local",
			mtype : "POST",
			height : 400,// TODO:高度如何调整
			autowidth : true,
			shrinkToFit : false,
			autoScroll : true,
			scroll : false,// 如需要分页控件,必须将此项置为false
			loadonce : true,
			viewrecords : true, // 显示总记录数
			emptyrecords : true,
			pager : $("#woInfoListPg"),
            fixed: true,
			colModel : WoitemInfoCM
	});
	domObjs.dnIDTxt.keyup(function(e) {
				$(this).val($(this).val().toUpperCase());
				if (e.keyCode == 13) {
					f1QueryFnc();
				}
			})

	function f1QueryFnc() {
		var inObj,outObj,dn_no;
		var iary ={};
		dn_no = domObjs.dnIDTxt.val().trim();
		zdfrom_date = domObjs.zdbeginTimeTxt.val();
		zdto_date = domObjs.zdendTimeTxt.val();
		jhfrom_date = domObjs.jhbeginTimeTxt.val();
		jhto_date = domObjs.jhendTimeTxt.val();
		
		var from_year,from_month,from_day;
		var to_year,to_month,to_day;
		from_year = zdfrom_date.substr(0,4);
		from_month = zdfrom_date.substr(5,2);
		from_day = zdfrom_date.substr(8,2);
		to_year = zdto_date.substr(0,4);
		to_month = zdto_date.substr(5,2);
		to_day = zdto_date.substr(8,2);
		if(from_year!=""&&to_year!=""){
		if(from_year<to_year){
		}else if(from_year == to_year){
			if(from_month<to_month){
			}else if(from_month==to_month){
				if(from_day<=to_day){
				}else{
					showErrorDialog("", "年份和月份相同的情况下，开始天数大于结束天数，请检查！");
					return false;
				}
			}else {
				showErrorDialog("", "年份相同的情况下，开始月份大于结束月份，请检查！");
				return false;
			}
		}else{
			showErrorDialog("", "开始年份大于结束年份，请检查！");
			return false;
		}
		}
		var from_year1,from_month1,from_day1;
		var to_year1,to_month1,to_day1;
		from_year1 = jhfrom_date.substr(0,4);
		from_month1 = jhfrom_date.substr(5,2);
		from_day1 = jhfrom_date.substr(8,2);
		to_year1 = jhto_date.substr(0,4);
		to_month1 = jhto_date.substr(5,2);
		to_day1 = jhto_date.substr(8,2);
		if(from_year1!=""&&to_year1!=""){
		if(from_year1<to_year1){
		}else if(from_year1 == to_year1){
			if(from_month1<to_month1){
			}else if(from_month1==to_month1){
				if(from_day1<=to_day1){
				}else{
					showErrorDialog("", "年份和月份相同的情况下，开始天数大于结束天数，请检查！");
					return false;
				}
			}else {
				showErrorDialog("", "年份相同的情况下，开始月份大于结束月份，请检查！");
				return false;
			}
		}else{
			showErrorDialog("", "开始年份大于结束年份，请检查！");
			return false;
		}
		}

		iary={
			dn_no : dn_no
		}
		inObj = {
			trx_id : VAL.T_XPAPLYDN,
			action_flg : 'M',
			iary : iary
		}
		if(zdfrom_date){
        	inObj.zdfrom_date = zdfrom_date+" 00:00:00.0";
        }
        if(zdto_date){
        	inObj.zdto_date = zdto_date+" 23:59:59.0";
        }
        if(jhfrom_date){
        	inObj.jhfrom_date = jhfrom_date;
        }
        if(jhto_date){
        	inObj.jhto_date = jhto_date;
        }
		outObj = comTrxSubSendPostJson(inObj);
		if(outObj.rtn_code == VAL.NORMAL){
			setGridInfo(TimestampUtil.formatTimestamp(outObj.oaryB), "#woInfoListGrd");
		}
	}
	$("#f1_query_btn").click(f1QueryFnc);
	$("#export_btn").click(function() {
		  generateExcel("#woInfoListGrd");
	});
	
	function resizeFnc(){
		var offsetBottom, grdDivWidth;
		tabPaneDivWidth = $("#tabContentDiv").width();
		
		offsetBottom = domObjs.$window.height() - $("#tabContentDiv").offset().top;
		domObjs.grids.$woInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$woInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoListGrd.setGridHeight(offsetBottom*0.95-51);
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
})