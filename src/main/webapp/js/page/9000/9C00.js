$(document).ready(function() {

	$("#tabDiv").tab('show');
	$("form").submit(function() {
		return false;
	});
	var VAL = {
		T_XPBISOPE : "XPBISOPE",
		T_XPINQOPE : "XPINQOPE",
		NORMAL : "0000000"
	};
	var domObjs = {
		$window : $(window),
		opeIDSel : $("#opeIDSel"),
		grids : {
			$opeInfoListDiv:$("#tabPane_ope"),
			$opeInfoListGrd:$("#opeInfoListGrd"),
			$detailInfoListDiv:$("#tabPane_detail"),
			$detailInfoListGrd:$("#detailInfoListGrd")
			
		}
	};
	function disPlay() {
		$("input").val("");
		$("#woIdSelect").val("");
		$("#opeInfoListGrd").jqGrid("clearGridData");
		$("#detailInfoListGrd").jqGrid("clearGridData");
		var inObj, outObj;

		inObj = {
			trx_id : VAL.T_XPBISOPE,
			action_flg : 'L'
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			_setSelectDate(outObj.tbl_cnt, outObj.oary, "ope_id", "ope_dsc", "#opeIDSel");
			// globalBean.opeObjs = outObj.oary;
		}
	}
	disPlay();
	var itemInfoCM = [ {
		name : 'prd_seq_id',
		index : 'prd_seq_id',
		label : PRD_SEQ_ID_TAG,
		width : PRD_SEQ_ID_CLM,
		align : "center",
		hidden: true
	},{
		name : 'fab_sn',
		index : 'fab_sn',
		label : PRD_SEQ_ID_TAG,
		width : PRD_SEQ_ID_CLM
	},{
		name : 'prd_stat',
		index : 'prd_stat',
		label : PRD_STAT_TAG,
		width : PRD_STAT_CLM,
		align : "center"
	}, {
		name : "prd_grade",
		index : "prd_grade",
		label : SHEET_GRADE_TAG,
		width : PRD_GRADE_CLM,
		align : "center"
	}, {
		name : 'rewk_flag',
		index : 'rewk_flag',
		label : "返品",
		width : REWK_FLAG_CLM,
		align : "center"
	}, {
		name : 'logof_timestamp',
		index : 'logof_timestamp',
		label : PV_OPE_LOGOF_TIME_TAG,
		width : TIMESTAMP_CLM,
		align : "center"
	}, {
		name : 'logon_timestamp',
		index : 'logon_timestamp',
		label : CR_OPE_LOGOF_TIME_TAG,
		width : TIMESTAMP_CLM,
		align : "center"
	}, {
		name : 'evt_usr',
		index : 'evt_usr',
		label : EVT_USR,
		width : USER_CLM,
		align : "center"
	}, {
		name : 'evt_timestamp',
		index : 'evt_timestamp',
		label : EVT_TIMESTAMP,
		width : TIMESTAMP_CLM,
		align : "center"
	}, {
		name : 'wo_id',
		index : 'wo_id',
		label : WO_ID_TAG,
		width : WO_ID_CLM,
		align : "center"
	}, {
		name : "lot_id",
		index : "lot_id",
		label : LOT_ID_TAG,
		width : LOT_ID_CLM,
		align : "center"
	}, {
		name : "box_id_fk",
		index : "box_id_fk",
		label : BOX_ID_TAG,
		width : BOX_ID_FK_CLM,
		width : BOX_ID_CLM,
		align : "center"
	} ];
	$("#detailInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 390,
		width  : $("#detailInfoListDiv").width()*0.99,
//		autowidth : true,
//		shrinkToFit : false,
//		autoScroll : true,
//		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : $("#detailInfoListPg"),
		fixed : true,
		colModel : itemInfoCM,
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	var OpeitemInfoCM = [ {
		name : 'cus_id_fk',
		index : 'cus_id_fk',
		label : CUS_ID_TAG,
		width : CUS_ID_CLM,
		align : "center"
	}, {
		name : 'so_id',
		index : 'so_id',
		label : SOR_ID_TAG,
		width : WO_ID_CLM,
		align : "center"
	}, {
		name : 'wo_id',
		index : 'wo_id',
		label : WO_ID_TAG,
		width : WO_ID_CLM,
		align : "center"
	}, {
		name : "lot_id",
		index : "lot_id",
		label : LOT_ID_TAG,
		width : LOT_ID_CLM,
		align : "center"
	}, {
		name : "box_id",
		index : "box_id",
		label : BOX_ID_TAG,
		width : BOX_ID_CLM,
		align : "center"
	}, {
		name : "box_stat",
		index : "box_stat",
		label : STATUS_TAG,
		width : BOX_STAT_CLM,
		align : "center"
	}, {
		name : 'prd_qty',
		index : 'prd_qty',
		label : "箱内" + QTY_TAG,
		width : QTY_CLM,
		align : "center"
	},
	// {name : 'wip_qty' , index : 'wip_qty' , label : "在制数量" , width :
	// 100,align:"center"},
	{
		name : 'rewk_flag',
		index : 'rewk_flag',
		label : "返品",
		width : REWK_FLAG_CLM,
		align : "center"
	}, {
		name : 'logof_timestamp',
		index : 'logof_timestamp',
		label : PV_OPE_LOGOF_TIME_TAG,
		width : TIMESTAMP_CLM,
		align : "center"
	}, {
		name : 'logof_usr',
		index : 'logof_usr',
		label : "出站操作人",
		width : USER_CLM,
		align : "center"
	}, {
		name : 'logon_timestamp',
		index : 'logon_timestamp',
		label : CR_OPE_LOGOF_TIME_TAG,
		width : LOGON_TIMESTAMP_CLM,
		align : "center"
	}, {
		name : 'logon_usr',
		index : 'logon_usr',
		label : EVT_USR,
		width : LOGON_USR_CLM,
		align : "center"
	} ];
	$("#opeInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 390,// TODO:高度如何调整
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : $("#opeInfoListPg"),
		fixed : true,
		colModel : OpeitemInfoCM,
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.opeIDSel.keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
	})

	function f1QueryFnc() {
		$("#opeInfoListGrd").jqGrid("clearGridData");
		$("#detailInfoListGrd").jqGrid("clearGridData");
		var ope_id, inObj, outObj,wo_id;
		ope_id = domObjs.opeIDSel.val();
		wo_id =$("#woIdSelect").val().trim();
		inObj = {
			trx_id : VAL.T_XPINQOPE,
			action_flg : 'M',
			ope_id : ope_id
		}
		if(wo_id){
			inObj.wo_id=wo_id;
		}
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code === VAL.NORMAL) {
			setGridInfo(TimestampUtil.formatTimestamp(outObj.oary2), "#opeInfoListGrd", true);
			setGridInfo(TimestampUtil.formatTimestamp(outObj.oary1), "#detailInfoListGrd", true);
		}
	}
	$("#f1_query_btn").click(f1QueryFnc);
	$("#export_ope_btn").click(function() {
		generateExcel("#opeInfoListGrd");
	});
	$("#export_detail_btn").click(function() {
		generateExcel("#detailInfoListGrd");
	});
	
	function resizeFnc(){
		var offsetBottom, grdDivWidth;
		tabPaneDivWidth = $("#tabContentDiv").width();
		
		offsetBottom = domObjs.$window.height() - $("#tabContentDiv").offset().top;
		domObjs.grids.$opeInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$opeInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$opeInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$opeInfoListGrd.setGridHeight(offsetBottom*0.95-51);
		
		domObjs.grids.$detailInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$detailInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$detailInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$detailInfoListGrd.setGridHeight(offsetBottom*0.95-51);
		
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
})