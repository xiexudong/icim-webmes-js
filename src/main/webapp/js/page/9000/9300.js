$(document).ready(function() {
	$("form").submit(function() {
		return false;
	});
	var realid="";
	var VAL ={
	        NORMAL     : "0000000"  ,
	        T_XPSPCRET : "XPSPCRET",
        	T_XPDEFECT : "XPDEFECT"	,
        	T_XPINQCOD : "XPINQCOD",
        	T_XPBISOPE : "XPBISOPE",
        	EVT_USER   : $("#userId").text()
	};
	var gOpeMap = {}, //Save ope info proc_id->ope_dsc
     	gDefectMap = {}; //Get from DATA-DFCT
	var domObj = {
		$window : $(window),
		$pnlWipInfoDiv : $("#pnlWipInfoDiv"),
		$pnlWipInfoGrd : $("#pnlWipInfoGrd"),
		$cdpcInfoDiv : $("#cdpcInfoDiv"),
		$cdpcInfoGrd : $("#cdpcInfoGrd"),
		$cdpzInfoDiv : $("#cdpzInfoDiv"),
		$cdpzInfoGrd : $("#cdpzInfoGrd"),
		$defectInfoDiv : $("#defectInfoDiv"),
		$defectInfoGrd : $("#defectInfoGrd")
	};
	var itemInfoCM = [ 
        {name : 'evt_cate_dsc',index : 'evt_cate_dsc',label : EVT_CATE_TAG ,width : EVT_CATE_DSC_CLM,},
		{name : 'lot_id'      ,index : 'lot_id'      ,label : LOT_ID_TAG   ,width : LOT_ID_CLM}, 
		{name : 'box_id_fk'   ,index : 'box_id_fk'   ,label : BOX_ID_TAG   ,width : BOX_ID_CLM}, 
		{name : 'prd_grade'   ,index : 'prd_grade'   ,label : "品质"        ,width : PRD_STAT_CLM},
		{name : 'group_id'    ,index : 'group_id'    ,label : "属主代码"     ,width : PRD_STAT_CLM},
		{name : 'slot_no'     ,index : 'slot_no'     ,label : SLOT_NO_TAG  ,width : SLOT_NO_CLM}, 
		{name : 'prd_stat'    ,index : 'prd_stat'    ,label : STATUS_TAG   ,width : PRD_STAT_CLM}, 
		//{name : 'mdl_id_fk'   ,index : 'mdl_id_fk'   ,label : MDL_TYPE_TAG ,width : MDL_ID_CLM}, 
		{name : 'mtrl_prod_id_fk'   ,index : 'mdl_id_fk'   ,label : "来料型号" ,width : MDL_ID_CLM}, 
		{name : 'th_mdl_id_fk'   ,index : 'th_mdl_id_fk'   ,label : "减薄后型号" ,width : MDL_ID_CLM}, 
		{name : 'fm_mdl_id_fk'   ,index : 'fm_mdl_id_fk'   ,label : "镀膜后型号" ,width : MDL_ID_CLM}, 
		{name : 'cut_mdl_id_fk'   ,index : 'cut_mdl_id_fk'   ,label : "切割后型号" ,width : MDL_ID_CLM}, 
		{name : 'mdl_typ_fk'  ,index : 'mdl_typ_fk'  ,label : MDL_SPEC_TAG ,width : MDL_TYP_FK_CLM}, 
		{name : 'prd_qty'     ,index : 'prd_qty'     ,label : "模数"         ,width : QTY_CLM}, 
		{name : 'path_id_fk'  ,index : 'path_id_fk'  ,label : PATH_ID_TAG  ,width : PATH_ID_CLM}, 
		{name : 'path_ver_fk' ,index : 'path_ver_fk' ,label : PATH_VER_TAG ,width : PATH_VER_CLM},
		{name : 'ope_no_dsc'  ,index : 'ope_no_dsc'  ,label : OPE_ID_TAG   ,width : OPE_ID_CLM}, 
		{name : 'toolg_id_fk' ,index : 'toolg_id_fk' ,label : TOOLG_ID_TAG ,width : TOOLG_ID_FK_CLM}, 
		{name : 'tool_id_fk'  ,index : 'tool_id_fk'  ,label : TOOL_ID_TAG  ,width : TOOL_ID_CLM} 
		];
	domObj.$pnlWipInfoGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
//		height : 400,
		autowidth : true,
		shrinkToFit : false,
//		width : 400,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		colModel : itemInfoCM
	});
	
	var cdpcInfoCM = [ 
		{name: 'prd_seq_id_fk'       , index: 'prd_seq_id_fk'           , label: '产品ID',       width: PRD_SEQ_ID_CLM,hidden:true},
		{name : 'fab_sn'              ,index : 'fab_sn'                  ,label :'产品ID'        ,width : PRD_SEQ_ID_CLM},
		{name: 'ope_id_fk'           , index: 'ope_id_fk'               , label: '站点',         width: OPE_ID_FK_CLM,hidden:true },
		{name: 'ope_des'             , index: 'ope_des'                 , label: '站点',         width: OPE_ID_CLM},
		{name: 'data_num'            , index: 'data_num'                , label: '测点',         width: DATA_NUM_CLM , hidden:true },
		{name: 'data_num_cn'         , index: 'data_num_cn'             , label: '测量项',         width: DATA_NUM_CLM },
		{name: 'data_value'          , index: 'data_value'              , label: '测量值',        width: DATA_NUM_CLM},
		{name: 'rpt_usr'             , index: 'rpt_usr'                 , label: '上报人',        width: RPT_USR_CLM },
		{name: 'rpt_timestamp'       , index: 'rpt_timestamp'           , label: '上报时间',      width: RPT_TIMESTAMP_CLM}
  		];
  	domObj.$cdpcInfoGrd.jqGrid({
  		url : "",
  		datatype : "local",
  		mtype : "POST",
//  		height : 400,
  		autowidth : false,
  		shrinkToFit : false,
//  		width : 400,
  		autoScroll : true,
  		scroll : false,// 如需要分页控件,必须将此项置为false
  		loadonce : true,
  		viewrecords : true, // 显示总记录数
  		emptyrecords : true,
  		colModel : cdpcInfoCM
  	});
  	
  	var cdpzInfoCM = [ 
		{name: 'prd_seq_id_fk'       , index: 'prd_seq_id_fk'           , label: '产品ID',       width: PRD_SEQ_ID_CLM,hidden:true},
		{name : 'fab_sn'              ,index : 'fab_sn'                  ,label :'产品ID'        ,width : PRD_SEQ_ID_CLM},
		{name: 'ope_id_fk'           , index: 'ope_id_fk'               , label: '站点',         width: OPE_ID_FK_CLM,hidden:true },
		{name: 'ope_des'             , index: 'ope_des'                 , label: '站点',         width: OPE_ID_CLM},
		{name: 'data_num'            , index: 'data_num'                , label: '测点',         width: DATA_NUM_CLM , hidden:true },
		{name: 'data_num_cn'         , index: 'data_num_cn'             , label: '测量项',         width: DATA_NUM_CLM },
		{name: 'data_value'          , index: 'data_value'              , label: '测量值',        width: DATA_NUM_CLM},
		{name: 'rpt_usr'             , index: 'rpt_usr'                 , label: '上报人',        width: RPT_USR_CLM },
		{name: 'rpt_timestamp'       , index: 'rpt_timestamp'           , label: '上报时间',      width: RPT_TIMESTAMP_CLM}
  		];
  	domObj.$cdpzInfoGrd.jqGrid({
  		url : "",
  		datatype : "local",
  		mtype : "POST",
//  	          		height : 400,
  		autowidth : false,
  		shrinkToFit : false,
//  	          		width : 400,
  		autoScroll : true,
  		scroll : false,// 如需要分页控件,必须将此项置为false
  		loadonce : true,
  		viewrecords : true, // 显示总记录数
  		emptyrecords : true,
  		colModel : cdpzInfoCM
  	});
  	
  	var defectInfoCM = [
         {name: 'defect_code' ,   index: 'defect_code',    label: ''               ,   width: DEFECT_CODE_CLM,hidden:true},
         {name: 'proc_id'     ,   index: 'proc_id'    ,    label: ''               ,   width: PROC_ID_CLM,hidden:true},
         {name: 'ope_dsc'     ,   index: 'ope_dsc'    ,    label: '判定站点'       ,   width: OPE_DSC_CLM},
         {name: 'defDsc'      ,   index: 'defDsc'     ,    label:DEF_DESC_TAG       ,   width: DEFDSC_CLM},
         {name: 'defect_side' ,   index: 'defect_side',    label: DEF_LC_TAG             ,   width: DEFECT_SIDE_CLM },
         {name: 'position'    ,   index: 'position'   ,    label: DEF_POSITION_TAG ,   width: POSITION_CLM},
         {name: 'defect_cnt'  ,   index: 'defect_cnt' ,    label: DEF_QTY_TAG           ,   width: DEFECT_CNT_CLM},
         {name: 'judge_timestamp',index: 'judge_timestamp',label: '判定时间'       ,   width: TIMESTAMP_CLM}
     ];
  	domObj.$defectInfoGrd.jqGrid({
  		url : "",
  		datatype : "local",
  		mtype : "POST",
//  	          		height : 400,
  		autowidth : false,
  		shrinkToFit : false,
//  	          		width : 400,
  		autoScroll : true,
  		scroll : false,// 如需要分页控件,必须将此项置为false
  		loadonce : true,
  		viewrecords : true, // 显示总记录数
  		emptyrecords : true,
  		colModel : defectInfoCM
     });
  	
	$("#prdSeqIDTxt").keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			$("#f1_query_btn").click();
		}
	})

	function f1QueryFnc() {
		var prd_seq_id, inTrx, outTrx;
		//prd_seq_id = $("#prdSeqIDTxt").val();
		if (prd_seq_id == "") {
			showErrorDialog("003", "请输入玻璃ID");
			return false;
		}
		inTrx = {
			trx_id : "XPINQSHT",
			action_flg : "I",
			prd_seq_id : realid
		};
		outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == "0000000") {
			if (!outTrx.oary2) {
				showErrorDialog("", "根据玻璃ID查询不到信息");
				return false;
			}
			setGridInfo(outTrx.oary2, "#pnlWipInfoGrd", true);
		}
	}
	
	function f1QueryCdpczFnc(mes_id,obj){
		 var iary,
	         inTrx,
	         outTrx;
		 //var prd_seq_id = $.trim($("#prdSeqIDTxt").val());
		 inTrx = {
	         trx_id     : VAL.T_XPSPCRET ,
	         action_flg : 'Q'        ,
	         prd_seq_id_fk :  realid,
	         mes_id : mes_id
	     };
	     outTrx = comTrxSubSendPostJson(inTrx);
//	     var oary;
	     if (outTrx.rtn_code==VAL.NORMAL){
	    	 obj.jqGrid("clearGridData");
	         outTrx.data_cnt == "1" ? outTrx.oary[0] = outTrx.oary : outTrx.oary;
	         for(var i=0;i<outTrx.data_cnt;i++){
//	         	$("#ope_id").val(outTrx.oary[0].ope_des);
	         	outTrx.oary[i].data_num_cn = outTrx.oary[i].data_num.replace('T','测点');
	         }
	         setGridInfo(outTrx.oary,obj);
//	         showMessengerSuccessDialog("刷新列表成功!!",1);
	     }
	}
	
	function f1QueryDefFnc(){
		var inObj,
	        outObj, 
	        def_cnt,
            defList = [],
            defListDistinctPos = [],
            defListHasPositionStr = '', //defect_code + position
            tmpDefPosStr,
            prd_jge_info = { };	
		//var prd_seq_id = $.trim($("#prdSeqIDTxt").val());
	    inObj = {
	        trx_id      : VAL.T_XPDEFECT,
	        action_flg  : 'Q'           ,
	        prd_seq_id  : realid
	    };
	
	    outObj = comTrxSubSendPostJson(inObj);
	    if (outObj.rtn_code == VAL.NORMAL) {
	    	def_cnt = parseInt(outObj.def_cnt, 10);
            if (1 === def_cnt) {
                defList.push(outObj.defList);
            } else if (1 < def_cnt) {
                defList = outObj.defList;
            }

            defList.sort(function (a, b) {
                if (a.judge_timestamp > b.judge_timestamp)
                    return -1;
                if (a.judge_timestamp < b.judge_timestamp)
                    return 1;
                return 0;
            });
            
            $.each(defList, function (index, value) {
                var defectMap,
                    opeMap,
                    tmpDefCode = value.defect_code,
                    tmpProcId = value.proc_id;

                defectMap = gDefectMap;
                if (defectMap.hasOwnProperty(tmpDefCode)) {
                    value.defDsc = defectMap[tmpDefCode].defDsc;
                } else {
                    value.defDsc = tmpDefCode;
                    console.error("Defect [%d] in prd [%d] not define in [DATE-DFCT]!", tmpDefCode, prd_seq_id);
                }

                opeMap = gOpeMap;
                if (opeMap.hasOwnProperty(tmpProcId)) {
                    value.ope_dsc = opeMap[tmpProcId].ope_dsc;
                } else {
                    value.ope_dsc = tmpProcId;
                    console.error("Get ope_dsc for proc %s error!", tmpProcId);
                }
            });
            
	    	setGridInfo(outObj.defList,domObj.$defectInfoGrd);
	    } else{
	    	domObj.$defectInfoGrd.jqGrid("clearGridData");
	    }
	
	}
	
	function get_all_defect_func (){
		var i,
	        defectCnt,
	        inTrxObj,
	        outTrxObj,
	        dataAry = [];
	
	    inTrxObj = {
	        trx_id: VAL.T_XPINQCOD,
	        action_flg: 'I',
	        data_cate : 'DFCT'
	    };
	    outTrxObj = comTrxSubSendPostJson(inTrxObj);
	    if( outTrxObj.rtn_code == VAL.NORMAL) {
	        gDefectMap = {};
	        defectCnt = parseInt(outTrxObj.tbl_cnt, 10);
	        if( 1 === defectCnt){
	            dataAry.push(outTrxObj.oary);
	        }else{
	            dataAry = outTrxObj.oary;
	        }
	        for( i = 0; i < defectCnt; i++ ){
	            gDefectMap[ dataAry[i].data_id ] = {
	                defDsc : dataAry[i].data_ext
	            };
	        }
	    }
	}
	
	function get_all_ope_func (){
		var inObj, 
			outObj,
			ope_ary = [];
	    var user_id = VAL.EVT_USER;
	    var iaryB = {
	        user_id     : user_id
	    };
	    inObj = {
	        trx_id      : VAL.T_XPBISOPE,
	        action_flg  : 'I',
	        iaryB       : iaryB
	    };
	    outObj = comTrxSubSendPostJson(inObj);
	    if (outObj.rtn_code == VAL.NORMAL) {
	        tbl_cnt = parseInt( outObj.tbl_cnt, 10 );
	        if(0 < tbl_cnt){
	            if(1 === tbl_cnt){
	                ope_ary.push(outObj.oary);
	            }else{
	                ope_ary = outObj.oary;
	            }
	            $.each(ope_ary, function( index, value ) {
	                gOpeMap[value.proc_id] = {
	                    ope_dsc : value.ope_dsc
	                };
	            });
	        }
	    }
	}
	
	$("#f1_query_btn").click(function (){
		domObj.$defectInfoGrd.jqGrid("clearGridData");
		domObj.$pnlWipInfoGrd.jqGrid("clearGridData");
		domObj.$cdpcInfoGrd.jqGrid("clearGridData");
		domObj.$cdpzInfoGrd.jqGrid("clearGridData");
		var prd_seq_id =$.trim($("#prdSeqIDTxt").val());
		if (prd_seq_id === ""){
			showErrorDialog("003", "请输入玻璃ID");
			return false;
		}
    	inObj = {
            	trx_id     : "XPINQSHT",
            	action_flg : "F",
            	fab_sn     : prd_seq_id
            };
            outObj = comTrxSubSendPostJson(inObj);
            if(outObj.rtn_code == "0000000"){
            	if(outObj.sht_cnt === "0"){
    				showErrorDialog("", "根据玻璃ID查询不到信息");
    				return false;
    			}
            	if(outObj.has_more_flg === "Y"){
            		FabSn.showSelectWorderDialog(prd_seq_id,outObj.oary3,function(data){
            			realid=data.prd_seq_id;
            			f1QueryFnc();
            			f1QueryCdpczFnc("CDPC",domObj.$cdpcInfoGrd);
            			f1QueryCdpczFnc("CZPC",domObj.$cdpzInfoGrd);
            			f1QueryDefFnc();
            		});
            	}else{
            		realid=prd_seq_id;
            		f1QueryFnc();
            		f1QueryCdpczFnc("CDPC",domObj.$cdpcInfoGrd);
            		f1QueryCdpczFnc("CZPC",domObj.$cdpzInfoGrd);
            		f1QueryDefFnc();
                }
            }
	});
	
	get_all_defect_func();
	get_all_ope_func();
	
	function resizeFnc(){                                                                      
    	var offsetBottom, divWidth;                                                              
		//玻璃当前信息grid调整  
		divWidth = domObj.$pnlWipInfoDiv.width();                                   
		offsetBottom = domObj.$window.height() - domObj.$pnlWipInfoDiv.offset().top;
		domObj.$pnlWipInfoDiv.height(offsetBottom * 0.95);                          
		domObj.$pnlWipInfoGrd.setGridWidth(divWidth * 0.99);                   
		domObj.$pnlWipInfoGrd.setGridHeight(offsetBottom * 0.99 - 51); 
		//不良判定信息grid调整
		divWidth = domObj.$defectInfoDiv.width();                                   
		offsetBottom = domObj.$window.height();
		domObj.$defectInfoDiv.height(offsetBottom * 0.25 - 20);                          
		domObj.$defectInfoGrd.setGridWidth(divWidth * 0.99);                   
		domObj.$defectInfoGrd.setGridHeight(offsetBottom * 0.23 - 21); 
		//厚度判定信息grid调整
		divWidth = domObj.$cdpcInfoDiv.width();                                   
		offsetBottom = domObj.$window.height();
		domObj.$cdpcInfoDiv.height(offsetBottom * 0.25 - 20);                          
		domObj.$cdpcInfoGrd.setGridWidth(divWidth * 0.99);                   
		domObj.$cdpcInfoGrd.setGridHeight(offsetBottom * 0.23 - 21); 
		//阻值判定信息grid调整
		divWidth = domObj.$cdpzInfoDiv.width();                                   
		offsetBottom = domObj.$window.height();
		domObj.$cdpzInfoDiv.height(offsetBottom * 0.25 - 20);                          
		domObj.$cdpzInfoGrd.setGridWidth(divWidth * 0.98);                   
		domObj.$cdpzInfoGrd.setGridHeight(offsetBottom * 0.23 - 21); 
    };                                                                                         
    resizeFnc();                                                                               
    domObj.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
	
})