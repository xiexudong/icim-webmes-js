$(document).ready(function() {
	// 测试数据:604641
	$("form").on("submit", false);
	var VAL = {
		T_XPINQWOR : "XPINQWOR",
		NORMAL : "0000000"
	};
	var domObjs = {
	    $window : $(window),
		$woIDTxt : $("#woIDTxt"),
		grids : {
			$woInfoDiv : $("#tabPane_woInfo"),
			$woInfoGrd : $("#woInfoGrd"),
			woInfoGrdSelector : "#woInfoGrd",
			
			$woQtyDiv : $("#tabPane_woQtyInfo"),
			$woQtyGrd : $("#woQtyGrd"),
			woQtyGrdSelector : "#woQtyGrd",
			
			$shipNoticeDiv : $("#tabPane_shipNoticeInfo"),
			$shipNoticeGrd : $("#shipNoticeGrd"),
			shipNoticeGrdSelector : "#shipNoticeGrd",
			
			$woDpsInfoListDiv : $("#tabPane_woDpsInfo"),
			$woDpsInfoListGrd : $("#woDpsInfoListGrd"),
			woDpsInfoListGrdSelector : "#woDpsInfoListGrd"
		}

	};
	function disPlay() {
		$("input").val("");
		TransUtil.addOpeDsc();
		TransUtil.addPrdStat();
		TransUtil.addWoStat();
	}
	domObjs.grids.$woInfoGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 420,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : "#woInfoPg",
		colModel : [ {
			name : 'cus_id',
			index : 'cus_id',
			label : CUS_ID_TAG,
			width : CUS_ID_CLM
		}, {
			name : 'so_id',
			index : 'so_id',
			label : SO_ID_TAG,
			width : WO_ID_CLM
		}, {
			name : 'mtrl_prod_id',
			index : 'mtrl_prod_id',
			label : MTRL_PROD_ID_TAG,
			width : MTRL_PROD_ID_CLM
		}, {
			name : 'wo_stat',
			index : 'wo_stat',// 内部订单的状态
			label : WO_STAT_TAG,
			width : WO_STAT_CLM
		}, {
			name : 'lot_id',
			index : 'lot_id',
			label : LOT_ID_TAG,
			width : LOT_ID_CLM
		}, {
			name : 'box_id',
			index : 'box_id',
			label : "厂内箱号",
			width : BOX_ID_CLM
		}, {
			name : 'mtrl_box_id_fk',
			index : 'mtrl_box_id_fk',
			label : "原箱号",
			width : BOX_ID_CLM
		},{
			name : 'prd_qty',
			index : 'prd_qty',
			label : "箱内数",
			width : QTY_CLM
		}, {
			name : 'wip_qty',
			index : 'wip_qty',
			label : "批次箱内数",
			width : QTY_CLM
		}, {
			name : 'ope_id',
			index : 'ope_id',
			label : "站点信息",
			width : OPE_ID_CLM
		}, {
			name : 'des_data_desc',
			index : 'des_data_desc',
			label : "仓位",
			width : OPE_ID_CLM
		},{
			name : 'prd_stat',
			index : 'prd_stat',
			label : "生产状态",
			width : STAT_DSC_CLM
		}, {
			name : 'mdl_id',
			index : 'mdl_id',
			label : TH_MDL_ID_TAG,
			width : MDL_ID_CLM
		}, {
			name : 'fm_mdl_id',
			index : 'fm_mdl_id',
			label : FM_MDL_ID_TAG,
			width : MDL_ID_CLM
		}, {
			name : 'cut_mdl_id',
			index : 'cut_mdl_id',
			label : CUT_MDL_ID_TAG,
			width : MDL_ID_CLM
		} ],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.grids.$woQtyGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 420,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : "#woQtyPg",
		colModel : [ {
			name : 'pln_prd_qty',
			index : 'pln_prd_qty',
			label : "订单数",// 内部订单总数量
			width : QTY_CLM
		}, {
			name : 'rl_prd_qty',
			index : 'rl_prd_qty',
			label : "已投数",
			width : QTY_CLM
		}, {
			name : 'un_rl_prd_qty',
			index : 'un_rl_prd_qty',
			label : "未投数",
			width : QTY_CLM
		}, {
			name : 'comp_prd_qty',
			index : 'comp_prd_qty',
			label : "已完数",
			width : QTY_CLM
		}, {
			name : 'un_comp_prd_qty',
			index : 'un_comp_prd_qty',
			label : "未完数",
			width : QTY_CLM
		}, {
			name : 'wh_out_prd_qty',
			index : 'wh_out_prd_qty',
			label : "已交数",
			width : QTY_CLM
		}, {
			name : 'un_wh_out_prd_qty',
			index : 'un_wh_out_prd_qty',
			label : "未交数",
			width : QTY_CLM
		}, {
			name : 'grade_gk_qty',
			index : 'grade_gk_qty',
			label : "GK数",
			width : QTY_CLM
		}, {
			name : 'grade_ng_qty',
			index : 'grade_ng_qty',
			label : "NG数",
			width : QTY_CLM
		}, {
			name : 'grade_lz_qty',
			index : 'grade_lz_qty',
			label : "LZ数",
			width : QTY_CLM
		},{
			name : 'grade_ok_qty',
			index : 'grade_ok_qty',
			label : "OK数",
			width : QTY_CLM
		},{
			name : 'grade_sc_qty',
			index : 'grade_sc_qty',
			label : "SC数",
			width : QTY_CLM
		}],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.grids.$shipNoticeGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 420,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : "#shipNoticePg",
		colModel : [ {
			name : 'cus_id',// 来自RET_WO.CUS_ID_FK
			index : 'cus_id',
			label : "交货客户ID",
			width : CUS_ID_CLM
		}, {
			name : 'dn_no',
			index : 'dn_no',
			label : "交货订单号",
			width : DN_NO_CLM
		}, {
			name : 'plan_shipping_timestamp',
			index : 'plan_shipping_timestamp',
			label : "交货计划日期",// 来自RET_SHIP_NOTICE.SHIP_DATE
			width : TIMESTAMP_CLM
		}, {
			name : 'mdl_id',// 来自RET_WO.MDL_ID_FK
			index : 'mdl_id',
			label : "交货型号",
			width : MDL_ID_CLM
		}, {
			name : 'ship_qty',
			index : 'ship_qty',
			label : "计交数",
			width : QTY_CLM
		}, {
			name : 'wh_out_prd_qty',
			index : 'wh_out_prd_qty',
			label : "已交数",
			width : QTY_CLM
		}, {
			name : 'shipping_timestamp',
			index : 'shipping_timestamp',
			label : "交货时间",// 实际交货时间
			// 来自RET_SHIP_NOTICE.SHIPPING_TIMESTAMP
			width : TIMESTAMP_CLM
		} ],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.grids.$woDpsInfoListGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		width : $("#woDpsDiv").width()*0.99,
		height : 400,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		colModel : [ {
			name : 'pln_stb_timestamp',
			index : 'pln_stb_timestamp',
			label : "计投时",
			width : TIMESTAMP_CLM
		}, {
			name : 'out_comp_timestamp',
			index : 'out_comp_timestamp',
			label : "计产时",
			width : TIMESTAMP_CLM
		}, {
			name : 'rl_prd_qty',
			index : 'rl_prd_qty',
			label : "计投数",
			width : QTY_CLM
		}, {
			name : 'out_prd_qty',
			index : 'out_prd_qty',
			label : "计产数",
			width : QTY_CLM
		}, {
			name : 'wo_id',
			index : 'wo_id',
			label : WO_ID_TAG,
			width : WO_ID_CLM
		}, {
			name : 'act_comp_qty',
			index : 'act_comp_qty',
			label : "计已完数",
			width : QTY_CLM
		}, {
			name : 'act_comp_timestamp',
			index : 'act_comp_timestamp',
			label : "完成时间",
			width : TIMESTAMP_CLM
		}, {
			name : 'un_comp_qty',
			index : 'un_comp_qty',
			label : "计未完数",
			width : QTY_CLM
		}, {
			name : 'lot_id',
			index : 'lot_id',
			label : LOT_ID_TAG,
			width : LOT_ID_CLM
		}, {
			name : 'box_id',
			index : 'box_id',
			label : "箱号",
			width : BOX_ID_CLM
		}, {
			name : 'ope_id',
			index : 'ope_id',
			label : OPE_ID_TAG,
			width : OPE_ID_CLM
		}, {
			name : 'prd_stat',
			index : 'prd_stat',
			label : "生产状态",
			width : PRD_STAT_CLM,
			hidden : true
		} ],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});

	function f1QueryFnc() {
		var woID, inTrx, outTrx, boxCnt, i, oary, oaryArr, opeDsc, prdStat;
		var oary1, oary4, oary1Arr, oary2Arr, oary3Arr, oary4Arr, oary1_cnt, oary4_cnt, i;
		woID = domObjs.$woIDTxt.val().trim();
		if (!woID) {
			showErrorDialog("003", "请输入内部订单号");
			return false;
		}
		inTrx = {
			trx_id : VAL.T_XPINQWOR,
			action_flg : "M",
			worder_id : woID
		};
		outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == VAL.NORMAL) {
			oary1Arr = outTrx.oary1;
			oary2Arr = outTrx.oary2;
			oary3Arr = outTrx.oary3;
			oary4Arr = outTrx.oary4;
			oary1_cnt = outTrx.oary1_cnt;
			oary4_cnt = outTrx.oary4_cnt;
			for (i = 0; i < oary1_cnt; i++) {
				oary1 = oary1_cnt > 1 ? oary1Arr[i] : oary1Arr;
				oary1.ope_id = TransUtil.getOpeDsc(oary1.ope_id);
				oary1.wo_stat = TransUtil.getWoStat(oary1.wo_stat);
				prdStat = oary1.prd_stat;
				if (prdStat === "SHIP") {
					oary1.prd_stat = TransUtil.getPrdStat(prdStat, oary1.bnk_flg)
				} else {
					oary1.prd_stat = TransUtil.getPrdStat(prdStat);
				}
			}
			for (i = 0; i < oary4_cnt; i++) {
				oary4 = oary4_cnt > 1 ? oary4Arr[i] : oary4Arr;
				oary4.ope_id = TransUtil.getOpeDsc(oary4.ope_id);
			}
			setGridInfo(oary1Arr, domObjs.grids.woInfoGrdSelector);
			setGridInfo(oary2Arr, domObjs.grids.woQtyGrdSelector);
			setGridInfo(TimestampUtil.formatTimestamp(oary3Arr), domObjs.grids.shipNoticeGrdSelector);
			setGridInfo(TimestampUtil.formatTimestamp(oary4Arr), domObjs.grids.woDpsInfoListGrdSelector);
		}
	}
	domObjs.$woIDTxt.keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
	})
	$("#f1_query_btn").click(f1QueryFnc);
	$("#f2ExportWoInfo").click(function() {
		generateExcel("#woInfoGrd");
	});
	$("#f3ExportSoInfo").click(function() {
		generateExcel("#woQtyGrd");
	});
	$("#f4ExportDnInfo").click(function() {
		generateExcel("#shipNoticeGrd");
	});
	$("#f5ExportDpsPlanInfo").click(function() {
		generateExcel("#woDpsInfoListGrd");
	});
	disPlay();
	
	function resizeFnc(){
		var offsetBottom, grdDivWidth;
		tabPaneDivWidth = $("#tabContentDiv").width();
		
		offsetBottom = domObjs.$window.height() - $("#tabContentDiv").offset().top;
		domObjs.grids.$woInfoDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoDiv.height(offsetBottom*0.96);
		domObjs.grids.$woInfoGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoGrd.setGridHeight(offsetBottom*0.95-51);
		
		domObjs.grids.$woQtyDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$woQtyDiv.height(offsetBottom*0.96);
		domObjs.grids.$woQtyGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$woQtyGrd.setGridHeight(offsetBottom*0.95-51);
		
		domObjs.grids.$shipNoticeDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$shipNoticeDiv.height(offsetBottom*0.96);
		domObjs.grids.$shipNoticeGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$shipNoticeGrd.setGridHeight(offsetBottom*0.95-51);
		
		domObjs.grids.$woDpsInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$woDpsInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$woDpsInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$woDpsInfoListGrd.setGridHeight(offsetBottom*0.95-51);
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
})