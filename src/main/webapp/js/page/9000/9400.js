$(document).ready(function() {

	// 测试数据
	$("#tabDiv").tab('show');
	$("form").submit(function() {
		return false;
	});
	var VAL = {
		T_XPBISOPE : "XPBISOPE",
		NORMAL : "0000000"
	};
	var domObjs = {
		$window   : $(window),
		$lotIDTxt : $("#lotIDTxt"),
		$lotID2Txt : $("#lotID2Txt"),
		$lotStatTxt : $("#lotStatTxt"),
		$opeIDTxt : $("#opeIDTxt"),
		$lotInfoListDiv:$("#tabPane_lotInfo"),
		$lotInfoListGrd:$("#lotInfoListGrd"),
		$pnlInfoListDiv:$("#tabPane_pnlInfo"),
		$pnlInfoListGrd:$("#pnlInfoListGrd")
	}
	var globalBean = {
		opeObjs : {},
		getOpeDsc : function(opeID, opeVer, prdStat, bnkFlg) {
			var i, oary;
			// if(typeof(prdStat)!=="undefined"){
			// if(prdStat==="COMP"){
			// return "已完成";
			// }else if (prdStat === "SCRP"){
			// return "已报废";
			// }else if(prdStat ==="SHTC"){
			// return "已出货";
			// }else if(prdStat === "SHIP"){
			// if(typeof(bnkFlg)!=="undefined"){
			// if(bnkFlg==="2"){
			// return "已半成品入库";
			// }else{
			// return "成品入库";
			// }
			// }
			// return "已入库";
			// }
			//				
			// }
			for (i = 0; i < this.opeObjs.length; i++) {
				oary = this.opeObjs[i];
				if (typeof (opeVer) === "undefined") {
					if (opeID == oary.ope_id) {
						return oary.ope_dsc
					}
				} else {
					if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
						return oary.ope_dsc
					}
				}
			}
		},
		prdStat : {
			WAIT : "等待入账",
			INPR : "制程中",
			SHIP_2 : "半成品入库",
			SHIP_0 : "成品入库",
			SHIP_1 : "成品入库",
			COMP : "完成",
			SHTC : "出货",
			SCRP : "报废"
		},
		getPrdStat : function(prdStat, bnkFlg) {
			if (typeof (bnkFlg) === "undefined") {
				return this.prdStat[prdStat];
			}
			return this.prdStat[prdStat + "_" + bnkFlg];
		}

	};
	function disPlay() {
//		$("input").val("");
		domObjs.$lotIDTxt.val("");
		var inObj, outObj;

		inObj = {
			trx_id : VAL.T_XPBISOPE,
			action_flg : 'L'
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			globalBean.opeObjs = outObj.oary;
		}
	}
	disPlay();
	var itemInfoCM = [ {
		name : 'prd_seq_id',
		index : 'prd_seq_id',
		label : PRD_SEQ_ID_TAG,
		width : PRD_SEQ_ID_CLM,
		hidden:true
	}, {
		name : 'fab_sn',
		index : 'fab_sn',
		label : PRD_SEQ_ID_TAG,
		width : PRD_SEQ_ID_CLM
	},{
		name : 'slot_no',
		index : 'slot_no',
		sorttype:'int',
		label : SLOT_NO_TAG,
		width : SLOT_NO_CLM
	}, {
		name : 'mdl_id_fk',
		index : 'mdl_id_fk',
		label : "生产型号",
		width : MDL_ID_CLM
	}, {
		name : 'box_id_fk',
		index : 'box_id_fk',
		label : "厂内箱号",
		width : BOX_ID_CLM
	}, {
		name : 'cr_ope_id_fk',
		index : 'cr_ope_id_fk',
		label : OPE_ID_TAG,
		width : OPE_ID_CLM
	}, {
		name : "prd_stat",
		index : "prd_stat",
		label : "生产状态",
		widht : PRD_STAT_CLM
	}, {
		name : "prd_grade",
		index : "prd_grade",
		label : "质量状态",
		widht : PRD_GRADE_CLM
	} , {
		name: 'oqc_skip_flg',    
		index: 'oqc_skip_flg',      
		label: '返品'             ,  
		width: PRD_GRADE_CLM,
		align:"center"
	} , {
		name : 'mtrl_box_id',
		index : 'mtrl_box_id',
		label : "来料箱号",
		width : MTRL_BOX_ID_CLM
	}];
	$("#pnlInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 500,// TODO:高度如何调整
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		pager : "#pnlInfoListPg",
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		colModel : itemInfoCM,
		sortable:true,
		sortname:"slot_no",
		sortorder:"asc",
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	$("#lotInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 570,//
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,
		loadonce : true,
		viewrecords : true,
		emptyrecords : true,
		pager : "#lotInfoListPg",
		colModel : [ {
			name : "cus_id",
			index : "cus_id",
			label : CUS_ID_TAG,
			width : CUS_ID_CLM
		}, {
			name : "so_id",
			index : "so_id",
			label : SO_ID_TAG,
			width : WO_ID_CLM
		}, {
			name : "wo_id",
			index : "wo_id",
			label : WO_ID_TAG,
			width : WO_ID_CLM
		}, {
			name : "mdl_id",
			index : "mdl_id",
			label : MDL_ID_TAG,
			width : MDL_ID_CLM
		}, {
			name : "prd_stat",
			index : "prd_stat",
			label : PRD_STAT_TAG,
			width : PRD_STAT_CLM
		}, {
			name: 'oqc_skip_flg',    
			index: 'oqc_skip_flg',      
			label: '返品'             ,  
			width: PRD_GRADE_CLM,
			align:"center"
		} , {
			name : "box_id_fk",
			index : "box_id_fk",
			label : "厂内箱号",
			width : BOX_ID_CLM
		}, {
			name : "lot_total_cnt",
			index : "lot_total_cnt",
			label : "批次数",
			width : QTY_CLM
		}, {
			name : "wip_cnt",
			index : "wip_cnt",
			label : "WIP数",
			width : QTY_CLM
		}, {
			name : "finished_cnt",
			index : "finished_cnt",
			label : "成品数",
			width : QTY_CLM
		}, {
			name : "unfinished_cnt",
			index : "unfinished_cnt",
			label : "半品数",
			width : QTY_CLM
		}, {
			name : "ope_id",
			index : "ope_id",
			label : "站点信息",
			width : OPE_ID_CLM
		}, /*
			 * TODO:暂时注掉，等HIS_RET_PRD_INFO表正常后再启用{ name : "ope_logof_cnt", index :
			 * "ope_logof_cnt", label : "该站已完成数量", width : 100 },
			 */{
			name : "ope_unlogof_cnt",
			index : "ope_unlogof_cnt",
			label : "该站未完数",
			width : QTY_CLM
		}],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});

	$("#lotIDTxt").keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
	});

	function f1QueryFnc() {

		var lot_id, inTrx, outTrx, shtCnt, oary, opeDsc, prdStat, bnkFlg, radioVal, procFlg;
		var oaryB, oaryArr, oaryBArr, i, sumCnt;
		var prdStat, bnkFlg;
		lot_id = domObjs.$lotIDTxt.val().trim();
		radioVal = $("input[name='partRadios']:checked").val();
		if (lot_id == "") {
			showErrorDialog("003", "请输入批次号");
			return false;
		}
		if(radioVal == "JB"){
			procFlg = "JB";
		}else if(radioVal == "DM"){
			procFlg = "DM";
		}else{
            showErrorDialog("","请选择工艺段！");
            return false;
		}
		inTrx = {
			trx_id : "XPINQLOT",
			action_flg : "M",
			lot_id : lot_id ,
			proc_flg : procFlg
		};
		outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == VAL.NORMAL) {
			shtCnt = outTrx.sht_cnt;
			sumCnt = outTrx.sum_cnt;
			if (shtCnt <= 0) {
				showErrorDialog("", "根据此批次号查询不到信息");
				return false;
			}
			for (i = 0; i < sumCnt; i++) {
				oaryB = sumCnt > 1 ? outTrx.oaryB[i] : outTrx.oaryB;

				prdStat = oaryB.prd_stat;
				bnkFlg = oaryB.bnk_flg;
				if (prdStat === "SHIP") {
					oaryB.prd_stat = globalBean.getPrdStat(prdStat, oaryB.bnk_flg);
				} else {
					oaryB.prd_stat = globalBean.getPrdStat(prdStat);
				}
				oaryB.ope_id = globalBean.getOpeDsc(oaryB.ope_id);
				// if(prdStat==="COMP"||prdStat ==="SCRP"||prdStat==="SHTC"){
				// oaryB.ope_id = globalBean.getOpeDsc(oaryB.ope_id, null,
				// prdStat);
				// }else if(prdStat === "SHIP"){
				// oaryB.ope_id = globalBean.getOpeDsc(oaryB.ope_id, null,
				// prdStat, bnkFlg);
				// }else{
				// oaryB.ope_id = globalBean.getOpeDsc(oaryB.ope_id);
				// }
			}
			for (i = 0; i < shtCnt; i++) {
				oary = shtCnt > 1 ? outTrx.oary[i] : outTrx.oary;
				prdStat = oary.prd_stat;
				bnkFlg = oary.bnk_flg;
				// if(prdStat==="COMP"||prdStat ==="SCRP"||prdStat==="SHTC"){
				// oaryB.ope_id = globalBean.getOpeDsc(oaryB.ope_id, null,
				// prdStat);
				// }else if(prdStat === "SHIP"){
				// oaryB.ope_id = globalBean.getOpeDsc(oaryB.ope_id, null,
				// prdStat, bnkFlg);
				// }else{
				// oaryB.ope_id = globalBean.getOpeDsc(oaryB.ope_id);
				// }
				if (prdStat === "SHIP") {
					oary.prd_stat = globalBean.getPrdStat(prdStat, bnkFlg);
				} else {
					oary.prd_stat = globalBean.getPrdStat(prdStat);
				}
				oary.cr_ope_id_fk = globalBean.getOpeDsc(oary.cr_ope_id_fk);
			}
			setGridInfo(outTrx.oaryB, "#lotInfoListGrd", true);
			setGridInfo(outTrx.oary, "#pnlInfoListGrd", true);
		}

	}
	$("#f1_query_btn").click(f1QueryFnc);
	$("#f2ExportLotInfo").click(function(){
		generateExcel("#lotInfoListGrd");
	});
	$("#f3ExportPrdInfo").click(function(){
		generateExcel("#pnlInfoListGrd");
	});
	
	function resizeFnc(){
		var offsetBottom, grdDivWidth;
		tabPaneDivWidth = $("#tabPaneDiv").width();
		
		offsetBottom = domObjs.$window.height() - $("#tabPaneDiv").offset().top;

		domObjs.$lotInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.$lotInfoListDiv.height(offsetBottom*0.96);
		domObjs.$lotInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.$lotInfoListGrd.setGridHeight(offsetBottom*0.95-51);
		
//		offsetBottom = domObjs.$window.height() - domObjs.$pnlInfoListDiv.offset().top;
		domObjs.$pnlInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.$pnlInfoListDiv.height(offsetBottom*0.96);
		domObjs.$pnlInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.$pnlInfoListGrd.setGridHeight(offsetBottom*0.95-51);
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
});