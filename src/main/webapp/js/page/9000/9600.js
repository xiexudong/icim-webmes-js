$(document).ready(function() {

	$("#tabDiv").tab('show');
	$("form").submit(function() {
				return false;
			});
	var VAL = {
		T_XPBISOPE : "XPBISOPE",
		T_XPINQCUS : "XPINQCUS",
		NORMAL : "0000000"
	};
	var domObjs = {
		$window   : $(window),
		$cusIDTxt : $("#cusIDTxt"),
		beginTimeTxt : $("#beginTimeTxt"),
		endTimeTxt : $("#endTimeTxt"),
		grids : {
			$woInfoListDiv : $("#tabPane_wo"),
			$woInfoListGrd : $("#woInfoListGrd"),
			$boxInfoListDiv : $("#tabPane_box"),
			$boxInfoListGrd : $("#boxInfoListGrd")
		}
	};
	var globalBean = {
		opeObjs : {},
		getOpeDsc : function(opeID, opeVer) {
			var i, oary;
//			for (i = 0; i < this.opeObjs.length; i++) {
//				oary = this.opeObjs[i];
//				if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
//					return oary.ope_dsc
//				}
//			}
			for (i = 0; i < this.opeObjs.length; i++) {
				oary = this.opeObjs[i];
				if (typeof (opeVer) === "undefined") {
					if (opeID == oary.ope_id) {
						return oary.ope_dsc
					}
				} else {
					if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
						return oary.ope_dsc
					}
				}
			}
		}
	};
	function disPlay() {
		$("input").val("");
		$("#boxInfoListGrd").jqGrid("clearGridData");
		$("#woInfoListGrd").jqGrid("clearGridData");
//		var inObj, outObj;
//
		inObj = {
			trx_id : VAL.T_XPBISOPE,
			action_flg : 'L'
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			globalBean.opeObjs = outObj.oary;
		}
	}
	disPlay();
	var itemInfoCM = [
	        {name : 'wo_id' ,index : 'wo_id' ,label : WO_ID_TAG ,width : WO_ID_CLM,align:"left"}, 
			{name : 'box_id',index : 'box_id',label : BOX_ID_TAG,width : BOX_ID_CLM,align:"left"}, 
			{name : "lot_id",index : "lot_id",label : LOT_ID_TAG,widht : LOT_ID_CLM,align:"left"}, 
			{name : "ope_no",index : "ope_no",label : OPE_ID_TAG,widht : OPE_ID_CLM,align:"left"},
			{name : "mtrl_box_id",index : "mtrl_box_id",label : "原料箱",widht : OPE_ID_CLM,align:"left"},
			{name : "prd_tl_qty",index : "prd_tl_qty",label : "总数量",widht : OPE_ID_CLM,align:"left"},
			{name : "prd_proc_qty",index : "prd_proc_qty",label : "投产数量",widht : OPE_ID_CLM,align:"left"},
			{name : "prd_comp_qty",index : "prd_comp_qty",label : "完成数量",widht : OPE_ID_CLM,align:"left"},
			{name : "prd_notcomp_qty",index : "prd_notcomp_qty",label : "未完成数量",widht : OPE_ID_CLM,align:"left"},
	];
	$("#boxInfoListGrd").jqGrid({
				url : "",
				datatype : "local",
				mtype : "POST",
				height : 450,// TODO:高度如何调整
				autowidth : true,
				shrinkToFit : false,
				autoScroll : true,
				scroll : false,// 如需要分页控件,必须将此项置为false
				loadonce : true,
				viewrecords : true, // 显示总记录数
				emptyrecords : true,
				pager : $("#boxInfoListPg"),
	            fixed: true,
				colModel : itemInfoCM
	            
			});
	var WoitemInfoCM = [
			{name : 'so_id'         , index : 'so_id'         , label : SO_ID_TAG         , width : SO_ID_CLM,align:"left"},
			{name : 'pln_stb_date'  , index : 'pln_stb_date'  , label : PLAN_FROM_DATE_TAG, width : DATE_CLM,align:"center"},
			{name : 'pln_prd_qty'   , index : 'pln_prd_qty'   , label : WO_TOTAL_CNT_TAG  , width : QTY_CLM,align:"center"},
			{name : 'wo_id'         , index : 'wo_id'         , label : WO_ID_TAG         , width : WO_ID_CLM,align:"left"},
			{name : 'ac_so_id'         , index : 'ac_so_id'         , label : "实际订单"         , width : WO_ID_CLM,align:"left"},
			{name : 'rl_mtrl_qty'   , index : 'rl_mtrl_qty'   , label : MTRL_RL_QTY_TAG   , width : QTY_CLM,align:"center"},
			{name : 'rl_mtrl_date'  , index : 'rl_mtrl_date'  , label : MTRL_RL_DATE_TAG  , width : TIMESTAMP_CLM,align:"center"},
			{name : 'rl_prd_qty'    , index : 'rl_prd_qty'    , label : RL_PRD_QTY_TAG    , width : QTY_CLM,align:"center"},
			{name : 'no_rl_prd_qty' , index : 'no_rl_prd_qty' , label : NO_RL_PRD_QTY_TAG , width : QTY_CLM,align:"center"},
			{name : 'wh_in_prd_qty' , index : 'wh_in_prd_qty' , label : PRD_WH_IN_QTY_TAG , width : QTY_CLM,align:"center"},
			{name : 'wip_qty'       , index : 'wip_qty'       , label : WIP_QTY_TAG       , width : QTY_CLM,align:"center"},
			{name : 'wh_out_prd_qty', index : 'wh_out_prd_qty', label : WH_OUT_PRD_QTY_TAG, width : QTY_CLM,align:"center"},
			{name : 'no_out_prd_qty', index : 'no_out_prd_qty', label : NO_OUT_PRD_QTY_TAG, width : QTY_CLM,align:"center"},
            {name : 'ng_grade_cnt'  , index : 'ng_grade_cnt'  , label : NG_CNT_TAG        , width : QTY_CLM ,align:"center"},
            {name : 'gk_grade_cnt'  , index : 'gk_grade_cnt'  , label : GRADE_CONTROL_TAG , width : QTY_CLM ,align:"center"},
            {name : 'lz_grade_cnt'  , index : 'lz_grade_cnt'  , label : GRADE_SCRP_TAG    , width : QTY_CLM ,align:"center"},
            {name : 'sc_grade_cnt'  , index : 'sc_grade_cnt'  , label : "报废数量"        , width : QTY_CLM ,align:"center"}
	];
	$("#woInfoListGrd").jqGrid({
			url : "",
			datatype : "local",
			mtype : "POST",
			height : 400,// TODO:高度如何调整
			autowidth : true,
			shrinkToFit : false,
			autoScroll : true,
			scroll : false,// 如需要分页控件,必须将此项置为false
			loadonce : true,
			viewrecords : true, // 显示总记录数
			emptyrecords : true,
			pager : $("#woInfoListPg"),
            fixed: true,
			colModel : WoitemInfoCM
	});
	domObjs.$cusIDTxt.keyup(function(e) {
				$(this).val($(this).val().toUpperCase());
				if (e.keyCode == 13) {
					f1QueryFnc();
				}
			})

	function f1QueryFnc() {
		var cusid, inTrx, outTrx, i, tblCnt, oary, oaryArr, opeDsc;
		var from_date,to_date;
		cusid = domObjs.$cusIDTxt.val().trim();
		from_date = domObjs.beginTimeTxt.val();
		to_date = domObjs.endTimeTxt.val();
//		if (cusid == "") {
//			showErrorDialog("", "请输入客户代码");
//			return false;
//		}
		if(from_date == "" ){
			showErrorDialog("", "请选择开始日期!");
			return false;
		}
		if(to_date == ""){
			showErrorDialog("", "请选择结束日期!");
			return false;
		}
		/**
		 * 检查开始和结束时间是否OK.
		 */
		var from_year,from_month,from_day;
		var to_year,to_month,to_day;
		from_year = from_date.substr(0,4);
		from_month = from_date.substr(5,2);
		from_day = from_date.substr(8,2);
		to_year = to_date.substr(0,4);
		to_month = to_date.substr(5,2);
		to_day = to_date.substr(8,2);
		if(from_year<to_year){
		}else if(from_year == to_year){
			if(from_month<to_month){
			}else if(from_month==to_month){
				if(from_day<=to_day){
				}else{
					showErrorDialog("", "年份和月份相同的情况下，开始天数大于结束天数，请检查！");
					return false;
				}
			}else {
				showErrorDialog("", "年份相同的情况下，开始月份大于结束月份，请检查！");
				return false;
			}
		}else{
			showErrorDialog("", "开始年份大于结束年份，请检查！");
			return false;
		}
		/**
		 * 发送Trx
		 */
		inTrx = {
			trx_id : VAL.T_XPINQCUS,
			action_flg : "L",
			cus_id : cusid,
			from_date : from_date+" 00:00:00.0",
			to_date   : to_date + " 23:59:59.0"
		};
		outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == VAL.NORMAL) {
			$("#boxInfoListGrd").jqGrid("clearGridData");
			$("#woInfoListGrd").jqGrid("clearGridData");
			
			var oaryBCnt = outTrx.tbl_cntB;
			var oaryB;
			for(i = 0;i<oaryBCnt;i++ ){
				oaryB = oaryBCnt > 1 ? outTrx.oaryB[i] : outTrx.oaryB;
				oaryB.ope_no = globalBean.getOpeDsc(oaryB.ope_no);
			}
			
			setGridInfo(TimestampUtil.formatTimestamp(outTrx.oary,"rl_mtrl_date"), "#woInfoListGrd",true);
			setGridInfo(outTrx.oaryB, "#boxInfoListGrd",true);
		}
		
	}
	function checkDate(from_date,to_date){
		
		
	}
	$("#beginTimeTxt, #endTimeTxt").datepicker({
		defaultDate : "",
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1
	});
	$("#f1_query_btn").click(f1QueryFnc);
	$("#export_wo_btn").click(function() {
		  generateExcel("#woInfoListGrd");
    });
	$("#export_box_btn").click(function() {
		  generateExcel("#boxInfoListGrd");
    });
	
	function resizeFnc(){
		var offsetBottom, grdDivWidth;
		tabPaneDivWidth = $("#tabContentDiv").width();
		
		offsetBottom = domObjs.$window.height() - $("#tabContentDiv").offset().top;
		domObjs.grids.$woInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$woInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoListGrd.setGridHeight(offsetBottom*0.95-51);
		
		domObjs.grids.$boxInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$boxInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$boxInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$boxInfoListGrd.setGridHeight(offsetBottom*0.95-51);
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
})