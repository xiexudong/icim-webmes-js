$(document).ready(function() {
	$("#tabDiv").tab('show');
	$("form").submit(function() {
		return false;
	});
    //测试数据 来料箱号:LL09100101,厂内箱号:B201405011652
	var domObjs = {
		$window   : $(window),
		$woIDTxt : $("#woIDTxt"),
		$boxIDTxt : $("#boxIDTxt"),
		$outboxIDTxt:$("#outboxIDTxt"),
		$palletIDTxt:$("#palletIDTxt"),
		$ppboxIDTxt : $("#ppboxIDTxt"),
		grids : {
			$grdDiv         : $("#grdDiv"),
			$boxInfoListGrd : $("#boxInfoListGrd"),
			$boxInfoListDiv : $("#boxInfoListDiv"),
			boxInfoListGrdSelector : "#boxInfoListGrd",
			$pnlInfoListGrd : $("#pnlInfoListGrd"),
			$pnlInfoListDiv : $("#pnlInfoListDiv"),
			pnlInfoListGrdSelector : "#pnlInfoListGrd",
		    $palletInfoListGrd : $("#palletInfoListGrd"),
		    $palletInfoListDiv : $("#palletInfoListDiv"),
		    palletInfoListGrdSelector : "#palletInfoListGrd"
		}
	};
	var VAL = {
		T_XPBISOPE : "XPBISOPE",
		NORMAL : "0000000"
	};
	var globalBean = {
		opeObjs : {},
		getOpeDsc : function(opeID, opeVer) {
			var i, oary;
			for (i = 0; i < this.opeObjs.length; i++) {
				oary = this.opeObjs[i];
				if (typeof (opeVer) === "undefined") {
					if (opeID == oary.ope_id) {
						return oary.ope_dsc;
					}
				} else {
					if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
						return oary.ope_dsc;
					}
				}
			}
		},
		prdStat : {
			WAIT : "等待入账",
			INPR : "制程中",
			SHIP_2 : "半成品入库",
			SHIP_0 : "成品入库",
			SHIP_1 : "成品入库",
			SHIP_4 : "转仓中",
			COMP : "完成",
			SHTC : "出货",
			SCRP : "报废",
			EMPT : "空",
			WFRL : "等待释放",
			RELS : "已下线",
			HOLD : "保留"
		},
		getPrdStat : function(prdStat, bnkFlg) {
			if (typeof (bnkFlg) === "undefined") {
				return this.prdStat[prdStat];
			}
			return this.prdStat[prdStat + "_" + bnkFlg];
		}
	};
	function disPlay() {
		$("input").val("");
		var inObj, outObj;

		inObj = {
			trx_id : VAL.T_XPBISOPE,
			action_flg : 'L'
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			globalBean.opeObjs = outObj.oary;
		}
	}
	disPlay();

	domObjs.grids.$boxInfoListGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 400,
		width  : $("#boxInfoListDiv").width(),
		pager : "#boxInfoListPg",
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		colModel : [ {
			name : 'cus_id',
			index : 'cus_id',
			label : CUS_ID_TAG,
			width : CUS_ID_CLM
		}, {
			name : 'so_id',
			index : 'so_id',
			label : SO_ID_TAG,
			width : WO_ID_CLM
		}, {
			name : 'wo_id',
			index : 'wo_id',
			label : WO_ID_TAG,
			width : WO_ID_CLM
		}, {
			name : 'mdl_id',
			index : 'mdl_id',
			label : "玻璃型号",
			width : MDL_ID_CLM
		}, {
			name : 'box_stat',
			index : 'box_stat',
			label : "状态",
			width : STAT_DSC_CLM
		} , {
			name: 'oqc_skip_flg',    
			index: 'oqc_skip_flg',      
			label: '返品'             ,  
			width: PRD_GRADE_CLM,
			align:"center"
		}, {
			name: 'ext1',    
			index: 'ext1',      
			label: 'RMA标识'             ,  
			width: PRD_GRADE_CLM,
			align:"center"
		}, {
			name : 'lot_id',
			index : 'lot_id',
			label : "批次号",
			width : LOT_ID_CLM
		}, {
			name : 'box_id',
			index : 'box_id',
			label : "厂内箱号",
			width : BOX_ID_CLM
		}, {
			name : 'mtrl_box_id_fk',
			index : 'mtrl_box_id_fk',
			label : "原箱号",
			width : BOX_ID_CLM
		},{
			name : 'ship_box_id',
			index : 'ship_box_id',
			label : "出货箱号",
			width : BOX_ID_CLM
		},{
			name : 'pallet_id',
			index : 'pallet_id',
			label : "栈板号",
			width : BOX_ID_CLM
		},{
			name : 'prd_qty',
			index : 'prd_qty',
			label : "总数量",
			width : QTY_CLM
		}, {
			name : 'wip_qty',
			index : 'wip_qty',
			label : "厂内数量",
			width : QTY_CLM
		}, {
			name : 'finished_qty',
			index : 'finished_qty',
			label : "未出成品",
			width : QTY_CLM
		}, {
			name : 'un_finished_qty',
			index : 'un_finished_qty',
			label : "WO在制",
			width : QTY_CLM
		}, {
			name : 'ope_id',
			index : 'ope_id',
			label : "站点信息",
			width : OPE_ID_CLM
		},{
			name : 'des_data_desc',
			index : 'des_data_desc',
			label : "仓位",
			width : OPE_ID_CLM
		},{
			name : 'prd_stat',
			index : 'prd_stat',
			label : "玻璃生产状态",
			width : PRD_STAT_CLM,
			hidden: true
		}, {
			name : "logof_qty",
			index : "logof_qty",
			label : "站已完",
			width : QTY_CLM,
			hidden: true//TODO:等HIS_RET_PRD_INFO表OK了再显示
		},{
			name : "un_logof_qty",
			index : "un_logof_qty",
			label : "站未完",
			width : QTY_CLM
		}],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.grids.$pnlInfoListGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		width  : $("#pnlInfoListDiv").width(),
		height : 400,
		pager : "#pnlInfoListPg",
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		sortable:true,
		sortname:"slot_no",
		sortorder:"asc",
		colModel :  [ {
			name : 'prd_seq_id',
			index : 'prd_seq_id',
			label : PRD_SEQ_ID_TAG,
			width : PRD_SEQ_ID_CLM,
			hidden:true
		},{
			name : 'fab_sn',
			index : 'fab_sn',
			label : PRD_SEQ_ID_TAG,
			width : PRD_SEQ_ID_CLM
		},{
			name : 'slot_no',
			index : 'slot_no',
			sorttype:'int',
			label : SLOT_NO_TAG,
			width : SLOT_NO_CLM
		},{
			name : 'mtrl_slot_no_fk',
			index : 'mtrl_slot_no_fk',
			sorttype:'int',
			label : "实位",
			width : SLOT_NO_CLM
		}, {
			name : 'mdl_id',
			index : 'mdl_id',
			label : MDL_ID_TAG,
			width : MDL_ID_CLM
		}, {
			name : 'ope_id',
			index : 'ope_id',
			label : OPE_ID_TAG,
			width : OPE_ID_CLM
		},{
			name : 'prd_stat',
			index : 'prd_stat',
			label : PRD_STAT_TAG,
			width : STAT_DSC_CLM
		},  {
			name : 'prd_grade',
			index : 'prd_grade',
			label : "品质",
			width : PRD_STAT_CLM
		} ,{
			name : 'group_id',
			index : 'group_id',
			label : "属主代码",
			width : PRD_STAT_CLM
		}, {
			name: 'oqc_skip_flg',    
			index: 'oqc_skip_flg',      
			label: '返品'             ,  
			width: PRD_GRADE_CLM,
			align:"center"
		},  {
			name: 'ext1',    
			index: 'ext1',      
			label: 'RMA标识'             ,  
			width: PRD_GRADE_CLM,
			align:"center"
		},{
			name : 'mtrl_box_id',
			index : 'mtrl_box_id',
			label : "来料箱号",
			width : BOX_ID_CLM
		}],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.grids.$palletInfoListGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		width  : $("#palletInfoListDiv").width(),
		height : 300,
		pager : "#palletInfoListPg",
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		sortable:true,
		colModel :  [ {
			name : 'wo_id',
			index : 'wo_id',
			label : WO_ID_TAG,
			width : WO_ID_CLM
		},{
			name : 'c_pnl_cnt',
			index : 'c_pnl_cnt',
			label : "数量",
			width : QTY_CLM
		}],
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	domObjs.$palletIDTxt.keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
		domObjs.$ppboxIDTxt.val("");
		domObjs.$boxIDTxt.val("");
		domObjs.$outboxIDTxt.val("");
	});
	domObjs.$outboxIDTxt.keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
		domObjs.$ppboxIDTxt.val("");
		domObjs.$boxIDTxt.val("");
		domObjs.$palletIDTxt.val("");
	});
	domObjs.$boxIDTxt.keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
		domObjs.$ppboxIDTxt.val("");
		domObjs.$outboxIDTxt.val("");
		domObjs.$palletIDTxt.val("");
	});
	domObjs.$ppboxIDTxt.keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
		domObjs.$boxIDTxt.val("");
		domObjs.$outboxIDTxt.val("");
		domObjs.$palletIDTxt.val("");
	});

	function f1QueryFnc() {
		var woId,boxId, ppboxId, stat, bnkFlg, inTrx, outTrx,oary1,oary1Cnt,oary2,oary2Cnt,i,outboxId,palletId;
		woId = domObjs.$woIDTxt.val().trim();
		boxId = domObjs.$boxIDTxt.val().trim();
		outboxId = domObjs.$outboxIDTxt.val().trim();
		ppboxId = domObjs.$ppboxIDTxt.val().trim();
		palletId = domObjs.$palletIDTxt.val().trim();
		if (!boxId && !ppboxId && !outboxId && !palletId) {
			showErrorDialog("", "厂内箱号,来料箱号,栈板号和出货箱号不能同时为空,请输入");
			return false;
		}
		inTrx = {
			trx_id : "XPINQBOX",
			action_flg : "M"
		};
		if ( woId ) {
			inTrx.wo_id = woId;
		}
		if(boxId){
			inTrx.box_id = boxId ; 			
		}else if(ppboxId){
			inTrx.ppbox_id = ppboxId ; 
		}else if(outboxId){
			inTrx.ship_box_id = outboxId;
		}else if(palletId){
			inTrx.pallet_id = palletId;
		}
		outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == "0000000") {
			oary1Cnt = outTrx.oary1_cnt;
			for (i = 0; i < oary1Cnt; i++) {
				oary1 = oary1Cnt > 1 ? outTrx.oary1[i] : outTrx.oary1;
				stat = oary1.box_stat;
				var prdStat = oary1.prd_stat;
				bnkFlg = oary1.bnk_flg;
				oary1.ope_id = globalBean.getOpeDsc(oary1.ope_id);
				if (stat === "SHIP") {
					oary1.box_stat = globalBean.getPrdStat(stat, bnkFlg);
				} else if(stat === "EMPT" && prdStat === "INPR"){//产品转入箱子时，箱子状态显示在制中
					oary1.box_stat = globalBean.getPrdStat(prdStat);
				}else{
					oary1.box_stat = globalBean.getPrdStat(stat);
				}
			}
			oary2Cnt = outTrx.oary2_cnt;
			for(i = 0;i<oary2Cnt;i++ ){
				oary2 = oary2Cnt > 1 ? outTrx.oary2[i] : outTrx.oary2;
				stat = oary2.prd_stat;
				bnkFlg = oary2.bnk_flg;
				oary2.ope_id = globalBean.getOpeDsc(oary2.ope_id);
				if (stat === "SHIP") {
					oary2.prd_stat = globalBean.getPrdStat(stat, bnkFlg);
				} else {
					oary2.prd_stat = globalBean.getPrdStat(stat);
				}
			}
			setGridInfo(outTrx.oary1, domObjs.grids.boxInfoListGrdSelector, true);
			setGridInfo(outTrx.oary2, domObjs.grids.pnlInfoListGrdSelector, true);
			setGridInfo(outTrx.box_info, domObjs.grids.palletInfoListGrdSelector, true);
		}
	}
	$("#f1_query_btn").click(f1QueryFnc);
	$("#f2ExportBoxInfo").click(function(){
		generateExcel("#boxInfoListGrd");
	});
	$("#f3ExportPrdInfo").click(function(){
		generateExcel("#pnlInfoListGrd");
	});
	function resizeFnc(){
		var offsetBottom, grdDivWidth;
		grdDivWidth = domObjs.grids.$grdDiv.width();
		
		offsetBottom = domObjs.$window.height() - domObjs.grids.$grdDiv.offset().top;

		domObjs.grids.$boxInfoListDiv.width(grdDivWidth*0.98);
		domObjs.grids.$boxInfoListDiv.height(offsetBottom*0.99);
		domObjs.grids.$boxInfoListGrd.setGridWidth(grdDivWidth*0.97);
		domObjs.grids.$boxInfoListGrd.setGridHeight(offsetBottom*0.95-51);
		
		domObjs.grids.$pnlInfoListDiv.width(grdDivWidth*0.98);
		domObjs.grids.$pnlInfoListDiv.height(offsetBottom*0.99);
		domObjs.grids.$pnlInfoListGrd.setGridWidth(grdDivWidth*0.97);
		domObjs.grids.$pnlInfoListGrd.setGridHeight(offsetBottom*0.95-51);
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
});