$(document).ready(function() {
	$("form").submit(function() {
		return false;
	});
	var domObj = {
		$window : $(window),
		$pnlHistoryDiv : $("#pnlHistoryDiv"),
		$pnlHistroyGrd : $("#pnlHistroyGrd")
	}
	var globalBean = {
		opeObjs : {},
		getOpeDsc : function(opeID, opeVer) {
			var i, oary;
			for (i = 0; i < this.opeObjs.length; i++) {
				oary = this.opeObjs[i];
				if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
					return oary.ope_dsc
				}
			}
		}
	};
	function disPlay() {
		var inObj, outObj;

		inObj = {
			trx_id : "XPBISOPE",
			action_flg : 'L'
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == "0000000") {
			globalBean.opeObjs = outObj.oary;
		}
	}
	disPlay();
	var itemInfoCM = [

	{
		name : 'sta_time_d',
		index : 'sta_time_d',
		label : START_DATETIME_TAG,
		width : TIMESTAMP_CLM
	}, {
		name : 'evt_cate_dsc',
		index : 'evt_cate_dsc',
		label : EVT_CATE_TAG,
		width : EVT_CATE_DSC_CLM
	}, {
		name : 'evt_usr',
		index : 'evt_usr',
		label : EVT_USR,
		width : USER_CLM
	}, {
		name : 'wo_id_fk',
		index : 'wo_id_fk',
		label : WO_ID_TAG,
		width : WO_ID_CLM
	}, {
		name : 'prd_grade',
		index : 'prd_grade',
		label : "品质",
		width : PRD_STAT_CLM
	},{
		name : 'group_id',
		index : 'group_id',
		label : "属主代码",
		width : PRD_STAT_CLM
	},{
		name : 'box_id_fk',
		index : 'box_id_fk',
		label : BOX_ID_TAG,
		width : BOX_ID_CLM
	}, {
		name : 'slot_no',
		index : 'slot_no',
		label : SLOT_NO_TAG,
		width : SLOT_NO_CLM
	}, {
		name : 'lot_id',
		index : 'lot_id',
		label : LOT_ID_TAG,
		width : LOT_ID_CLM
	}, {
		name : 'mdl_id_fk',
		index : 'mdl_id_fk',
		label : MDL_ID_TAG,
		width : MDL_ID_CLM
	}, {
		name : 'prd_qty',
		index : 'prd_qty',
		label : "模数",
		width : QTY_CLM
	}, {
		name : 'prd_stat',
		index : 'prd_stat',
		label : "产品" + STATUS_TAG,
		width : PRD_STAT_CLM
	}, {name   : 'dest_shop',   
	   index  : 'dest_shop',  
	   label  : DEST_SHOP_TAG , 
	   width  : DEST_SHOP_CLM ,
	   hidden : true
	},{name: 'dest_shop_dsc',   
	   index: 'dest_shop_dsc',  
	   label: DEST_SHOP_TAG , 
	   width: DEST_SHOP_CLM 
	},{name : 'ope_no_dsc',
	   index : 'ope_no_dsc',
	   label : OPE_ID_TAG,
	   width : OPE_ID_CLM
	}, {
		name : 'toolg_id_fk',
		index : 'toolg_id_fk',
		label : TOOLG_ID_TAG,
		width : TOOLG_ID_FK_CLM
	}, {
		name : 'tool_id_fk',
		index : 'tool_id_fk',
		label : TOOL_ID_TAG,
		width : TOOL_ID_CLM
	},{
		name : 'sht_ope_msg',
		index : 'sht_ope_msg',
		label : REMARK_TAG,
		width : SHT_OPE_MSG_CLM
	},{
		name : 'ds_recipe_id',
		index : 'ds_recipe_id',
		label : '产品备注',
		width : SHT_OPE_MSG_CLM
	} ];
	domObj.$pnlHistroyGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		// height:$("#pnlInfoListDiv").height()*0.95,
		height : 400,// TODO:高度如何调整
		// autowidth: true,
		shrinkToFit : false,
		width : $("#pnlHistoryDiv").width() * 0.99,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		// pager : '#pnlInfoListPg',
		// toolbar:true,
		colModel : itemInfoCM
	});
	var resetJqgrid = function() {
		$(window).unbind("onresize");
		// $("#pnlInfoListGrd").setGridWidth($("#pnlInfoListDiv").width()*0.95);
		// $("#pnlInfoListGrd").setGridHeight($("#pnlInfoListDiv").height()*0.80);
		$(window).bind("onresize", this);
	}
	function initFnc() {
		resetJqgrid();
	}
	initFnc();
	$(window).resize(function() {
		resetJqgrid();
	})
	$("#prdSeqIDTxt").keyup(function(e) {
		//$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
	})

	function f1QueryFnc() {
		var prd_seq_id, inTrx, outTrx, i, oary, tbl_cnt;
		prd_seq_id = $("#prdSeqIDTxt").val().trim();
		if (prd_seq_id == "") {
			showErrorDialog("", "请输入玻璃代码");
			return false;
		}
		//重复玻璃
    	inObj = {
            	trx_id     : "XPINQSHT",
            	action_flg : "F",
            	fab_sn     : prd_seq_id
            };
            outObj = comTrxSubSendPostJson(inObj);
            if(outObj.rtn_code == "0000000"){
            	if(outObj.has_more_flg === "Y"){
            		FabSn.showSelectWorderDialog(prd_seq_id,outObj.oary3,function(data){
            			//controlsQuery.prdIdInput.val(data.prd_seq_id)
            			prd_seq_id=data.prd_seq_id;
            			inTrx = {
            					trx_id : "HPSHTHIS",
            					action_flg : "Q",
            					prd_seq_id : prd_seq_id
            				};
            				outTrx = comTrxSubSendPostJson(inTrx);
            				if (outTrx.rtn_code == "0000000") {
            					tbl_cnt = outTrx.tbl_cnt;
            					if (tbl_cnt <= 0) {
            						showErrorDialog("", "根据玻璃ID查询不到信息");
            						return false;
            					}
            					for (i = 0; i < tbl_cnt; i++) {
            						oary = tbl_cnt > 1 ? outTrx.oary[i] : outTrx.oary;
            						opeDsc = globalBean.getOpeDsc(oary.ope_id_fk, oary.ope_ver_fk);
            						oary.ope_id_fk = opeDsc;
            					}

            					setGridInfo(outTrx.oary, "#pnlHistroyGrd", true);
            				}
            		});
            	}else{
            		inTrx = {
            				trx_id : "HPSHTHIS",
            				action_flg : "Q",
            				prd_seq_id : prd_seq_id
            			};
            			outTrx = comTrxSubSendPostJson(inTrx);
            			if (outTrx.rtn_code == "0000000") {
            				tbl_cnt = outTrx.tbl_cnt;
            				if (tbl_cnt <= 0) {
            					showErrorDialog("", "根据玻璃ID查询不到信息");
            					return false;
            				}
            				for (i = 0; i < tbl_cnt; i++) {
            					oary = tbl_cnt > 1 ? outTrx.oary[i] : outTrx.oary;
            					opeDsc = globalBean.getOpeDsc(oary.ope_id_fk, oary.ope_ver_fk);
            					oary.ope_id_fk = opeDsc;
            				}

            				setGridInfo(outTrx.oary, "#pnlHistroyGrd", true);
            			}
            	}
            }
            
	}
	$("#f1_query_btn").click(f1QueryFnc);
	$("#f2_export_btn").click(function() {
		generateExcel("#pnlHistroyGrd");
	});
	function resizeFnc(){                                                                      
    	var offsetBottom, divWidth;                                                              
		    
		divWidth = domObj.$pnlHistoryDiv.width();                                   
		offsetBottom = domObj.$window.height() - domObj.$pnlHistoryDiv.offset().top;
		domObj.$pnlHistoryDiv.height(offsetBottom * 0.95);                          
		domObj.$pnlHistroyGrd.setGridWidth(divWidth * 0.99);                   
		domObj.$pnlHistroyGrd.setGridHeight(offsetBottom * 0.99 - 51);               
    };                                                                                         
    resizeFnc();                                                                               
    domObj.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
});