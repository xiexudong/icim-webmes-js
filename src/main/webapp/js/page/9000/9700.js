$(document).ready(function() {

	$("#tabDiv").tab('show');
	$("form").submit(function() {
				return false;
			});
	var VAL = {
		T_XPBMDLDF : "XPBMDLDF",
		T_XPINQMDL : "XPINQMDL",
		NORMAL : "0000000"
	};
	var domObjs = {
		$window : $(window),
		mdlIDSel : $("#mdlIDSel"),
		beginTimeTxt : $("#beginTimeTxt"),
		endTimeTxt : $("#endTimeTxt"),
		grids : {
			$woInfoListDiv:$("#tabPane_wo"),
			$woInfoListGrd:$("#woInfoListGrd")
		}
	};
	var baseFnc = {
		replaceSpace:function(str){
			if(!str){
				return str;
			}
			return str.replace(/[ ]/g,"@");	
		},
		returnSpace:function(str){
			if(!str){
				return str;
			}
			return str.replace(/[@]/g," ");
		}
	}
//	var SelectDom = {
//		addSelect : function($selectDomObj, val, txt) {
//			txt = (typeof (txt) === "undefined" ? val : txt);
//			if (val && !this.hasValue(val)) {
//				$selectDomObj.append("<option value=" + val + ">" + txt + "</option>");
//			}
//		},
//		
//		hasValue : function($selectDomObj, val) {
//			return $($selectDomObj.selector + " option[value='" + val + "']").length > 0 ? true : false;
//		}
//	}
	function disPlay() {
		$("input").val("");
		$("#woInfoListGrd").jqGrid("clearGridData");
		$("#boxInfoListGrd").jqGrid("clearGridData");
		iniMdlDefProc();
	}
	function iniMdlDefProc(){
		var inObj,outObj,tblCnt,oary;
		
		var iary ={
		}
		inObj={
			trx_id : VAL.T_XPBMDLDF,
			action_flg : 'Q',
			iary :iary
		}
		outObj = comTrxSubSendPostJson(inObj);
		
		if(outObj.rtn_code == VAL.NORMAL){
			tblCnt = outObj.tbl_cnt;
			for (i = 0; i < tblCnt; i++) {
				oary = tblCnt > 1 ? outObj.oary[i] : outObj.oary;
				
				SelectDom.addSelect($("#mdlIDSel"), baseFnc.replaceSpace(oary.mdl_id),oary.mdl_id);
			}
			
//			_setSelectDate(outObj.tbl_cnt, oary, "mdl_id","mdl_id", "#mdlIDSel");
		}
	}
	disPlay();
	var WoitemInfoCM = [
			{name : 'so_id'         , index : 'so_id'         , label : SO_ID_TAG         , width : WO_ID_CLM,align:"left"},
			{name : 'crt_timestamp' , index : 'crt_timestamp' , label : SO_CRT_TIMESTAMP  , width : TIMESTAMP_CLM,align:"left"},
			{name : 'wo_id'         , index : 'wo_id'         , label : WO_ID_TAG         , width : WO_ID_CLM,align:"left"},
			{name : 'rl_mtrl_date'  , index : 'rl_mtrl_date'  , label : MTRL_RL_DATE_TAG  , width : TIMESTAMP_CLM,align:"center"},
			{name : 'rl_mtrl_qty'   , index : 'rl_mtrl_qty'   , label : MTRL_RL_QTY_TAG   , width : QTY_CLM,align:"center"},
			{name : 'cmp_prd_qty'   , index : 'cmp_prd_qty'   , label : "完成数"       , width : QTY_CLM,align:"center"},
			{name : 'wip_qty'       , index : 'wip_qty'       , label : WIP_QTY_TAG       , width : QTY_CLM,align:"center"},
			{name : 'no_rl_prd_qty' , index : 'no_rl_prd_qty' , label : NO_RL_PRD_QTY_TAG , width : QTY_CLM,align:"center"},
			{name : 'wh_out_prd_qty', index : 'wh_out_prd_qty', label : WH_OUT_PRD_QTY_TAG, width : QTY_CLM,align:"center"},
			{name : 'no_out_prd_qty', index : 'no_out_prd_qty', label : NO_OUT_PRD_QTY_TAG, width : QTY_CLM,align:"center"}
	];
	$("#woInfoListGrd").jqGrid({
			url : "",
			datatype : "local",
			mtype : "POST",
			height : 400,// TODO:高度如何调整
			autowidth : true,
			shrinkToFit : false,
			autoScroll : true,
			scroll : false,// 如需要分页控件,必须将此项置为false
			loadonce : true,
			viewrecords : true, // 显示总记录数
			emptyrecords : true,
			pager : $("#woInfoListPg"),
            fixed: true,
			colModel : WoitemInfoCM
	});
//	domObjs.mdlIDSel.keyup(function(e) {
//				$(this).val($(this).val().toUpperCase());
//				if (e.keyCode == 13) {
//					f1QueryFnc();
//				}
//			})

	function f1QueryFnc() {
		var mdl_id,from_date,to_date;
		var inObj,outObj;
		mdl_id = baseFnc.returnSpace(domObjs.mdlIDSel.val().trim());
		from_date = domObjs.beginTimeTxt.val();
		to_date = domObjs.endTimeTxt.val();
		if(!mdl_id){
			showErrorDialog("", "请选择需要查询的产品型号!");
			return false;
		}
		if(from_date == "" ){
			showErrorDialog("", "请选择开始日期!");
			return false;
		}
		if(to_date == ""){
			showErrorDialog("", "请选择结束日期!");
			return false;
		}
		/**
		 * 检查开始和结束时间是否OK.
		 */
		var from_year,from_month,from_day;
		var to_year,to_month,to_day;
		from_year = from_date.substr(0,4);
		from_month = from_date.substr(5,2);
		from_day = from_date.substr(8,2);
		to_year = to_date.substr(0,4);
		to_month = to_date.substr(5,2);
		to_day = to_date.substr(8,2);
		if(from_year<to_year){
		}else if(from_year == to_year){
			if(from_month<to_month){
			}else if(from_month==to_month){
				if(from_day<=to_day){
				}else{
					showErrorDialog("", "年份和月份相同的情况下，开始天数大于结束天数，请检查！");
					return false;
				}
			}else {
				showErrorDialog("", "年份相同的情况下，开始月份大于结束月份，请检查！");
				return false;
			}
		}else{
			showErrorDialog("", "开始年份大于结束年份，请检查！");
			return false;
		}
		inObj={
			trx_id : VAL.T_XPINQMDL,
			action_flg : 'Q',
			mdl_id : mdl_id,
			from_date : from_date+" 00:00:00.0",
			to_date   : to_date + " 23:59:59.0"
		}
		outTrx = comTrxSubSendPostJson(inObj);
		if (outTrx.rtn_code == VAL.NORMAL) {
			$("#woInfoListGrd").jqGrid("clearGridData");
			setGridInfo(TimestampUtil.formatTimestamp(outTrx.oary), "#woInfoListGrd",true);
		}
	}
	$("#beginTimeTxt, #endTimeTxt").datepicker({
		defaultDate : "",
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1
	});
	$("#f1_query_btn").click(f1QueryFnc);
	$("#export_btn").click(function() {
		  generateExcel("#woInfoListGrd");
    });
	
	function resizeFnc(){
		var offsetBottom, grdDivWidth;
		tabPaneDivWidth = $("#tabContentDiv").width();
		
		offsetBottom = domObjs.$window.height() - $("#tabContentDiv").offset().top;
		domObjs.grids.$woInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$woInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoListGrd.setGridHeight(offsetBottom*0.95-51);
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
	
	
})