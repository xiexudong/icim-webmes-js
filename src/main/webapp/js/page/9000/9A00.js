$(document).ready(function() {

	//测试数据:
	$("form").submit(function() {
		return false;
	});
	var VAL = {
		T_XPBISOPE : "XPBISOPE",
		T_XPAPLYWO : "XPAPLYWO",
		T_XPINQDEF : "XPINQDEF",
		NORMAL : "0000000"
	};
	var domObjs = {
		$window : $(window),
		cusIdInput  : $("#cusIdInput"),
		woIdInput   : $("#woIdInput"),
		beginTimeTxt : $("#beginTimeTxt"),
		endTimeTxt  : $("#endTimeTxt"),
		gradeGKChk : $("#gradeGKChk"),
		gradeLZChk : $("#gradeLZChk"),
		gradeNGChk : $("#gradeNGChk"),
		gradeSCChk : $("#gradeSCChk"),
		dmProcChk : $("#dmProcChk"),
		jbProcChk : $("#jbProcChk"),
		grids : {
			$defectInfoListDiv : $("#defectInfoListDiv"),
			$defectInfoListGrd : $("#defectInfoListGrd"),
			defectInfoListGrdSelector : "#defectInfoListGrd",
		}
	};
	var globalBean = {
		opeObjs : {},
		getOpeDsc : function(opeID, opeVer) {
			var i, oary;
			for (i = 0; i < this.opeObjs.length; i++) {
				oary = this.opeObjs[i];
				if (typeof (opeVer) === "undefined") {
					if (opeID == oary.ope_id) {
						return oary.ope_dsc
					}
				} else {
					if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
						return oary.ope_dsc
					}
				}
			}
		},
		prdStat : {
			INPR   : "制程中",
			WAIT_0 : "等待下一制程",
			WAIT_1 : "进入WIP BANK",
			SHIP_1 : "成品入库",
			SHIP_2 : "半成品入库",
			SHTC_0 : "半成品出货",
			SHTC_1 : "成品出货",		
			COMP   : "完成",
			SCRP   : "报废"
		},
		getPrdStat : function(prdStat, bnkFlg) {
			if (typeof (bnkFlg) === "undefined") {
				return this.prdStat[prdStat];
			}
			return this.prdStat[prdStat + "_" + bnkFlg];
		}
	};
	
    /**
     * All tool functions
     * @type {Object}
     */
    var toolFunc = {
        iniQueryOpeDsc : function(){
			var inObj, outObj;

			inObj = {
				trx_id : VAL.T_XPBISOPE,
				action_flg : 'L'
			};
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code == VAL.NORMAL) {
				globalBean.opeObjs = outObj.oary;
			}
	    }
    }

	domObjs.grids.$defectInfoListGrd.jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 600,
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		pager : "#defectInfoListPg",
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		colModel : [ {name:'judge_timestamp', index:'judge_timestamp',  label:"判定日期",   width:TIMESTAMP_CLM}, 
		             {name:'judge_usr',       index:'judge_usr',        label:"判定人", width:USER_CLM},
		             {name:'wh_in_timestamp', index:'wh_in_timestamp',  label:"入库日期",   width:TIMESTAMP_CLM},
		             {name:'cus_id',          index:'cus_id',           label:CUS_ID_TAG,   width:CUS_ID_CLM},
		             {name:'wo_id',           index:'wo_id',            label:WO_ID_TAG, width:WO_ID_CLM},
		             {name:'model_Cnt',       index:'model_Cnt',        label:"模数",       width:MODEL_CNT_CLM},
		             {name:'to_thickness',    index:'to_thickness',     label:"厚度",       width:QTY_CLM},
		             {name:'mtrl_prod_id',    index:'mtrl_prod_id',     label:MTRL_PROD_ID_TAG, width:MTRL_PROD_ID_CLM},
		             {name:'mdl_id',          index:'mdl_id',           label:TH_MDL_ID_TAG, width:MDL_ID_CLM},
		             {name:'fm_mdl_id',       index:'fm_mdl_id',        label:FM_MDL_ID_TAG, width:MDL_ID_CLM},
		             {name:'prd_seq_id',      index:'prd_seq_id',       label:PRD_SEQ_ID_TAG,   width:PRD_SEQ_ID_CLM},
		             {name:'prd_grade',      index: 'prd_grade',      label: JUDGE_RESULT_TAG       ,  width: PRD_GRADE_CLM},
		             {name:'wgbl_cnt',        index:'wgbl_cnt',         label:"外观不良",   width:QTY_CLM},
		             {name:'psls_cnt',        index:'psls_cnt',         label:"破裂粒数",   width:QTY_CLM},
		             {name:'fensui_cnt',      index:'fensui_cnt',       label:"粉碎",       width:QTY_CLM},
		             {name:'sht_ope_msg',     index:'sht_ope_msg',      label:"发生原因",   width:SHT_OPE_MSG_CLM},
		             {name:'dest_shop_dsc',   index:'dest_shop_dsc',    label:DEST_SHOP_TAG,   width:DEST_SHOP_CLM},
		             {name:'dest_shop',       index:'dest_shop',        label:DEST_SHOP_TAG,   width:DEST_SHOP_CLM,hidden: true},
		             {name:'prd_stat_dsc',    index:'prd_stat_dsc',     label:"产品状态",   width:PRD_STAT_DSC_CLM},
		             {name:'prd_stat',        index:'prd_stat',         label:"产品状态",   width:1, hidden: true},
		             {name:'bnk_flg',         index:'bnk_flg',          label:"",   width:1, hidden: true}]
	});
	function initializationFunc() {
        toolFunc.iniQueryOpeDsc();
	}

	function f1QueryFnc() {
		var inTrx,
		    outTrx,
		    cus_id,
		    wo_id,
		    begineTime,
		    endTime,
		    prd_grades,
		    proc_ids;
		//prd_grades = "'GK','LZ'";
		proc_ids = "'JBYZ','DMZJ','JBZJ'";
		cus_id = $.trim(domObjs.cusIdInput.val());
		if (!cus_id) {
			showErrorDialog("", "请先输入客户代码！");
			return false;
		}
		inTrx = {
				trx_id : VAL.T_XPINQDEF,
				action_flg : "Q",
				cus_id : cus_id
			};
		
		wo_id = $.trim(domObjs.woIdInput.val());
		if(wo_id){
			inTrx.wo_id = wo_id;
		}
		
		begineTime = $.trim(domObjs.beginTimeTxt.val());
		if(begineTime){
			inTrx.begineTime = begineTime + " 00:00:00.0";
		}
		
		endTime = $.trim(domObjs.endTimeTxt.val());
		if(endTime){
			inTrx.endTime = endTime + " 23:59:59.0";
		}
		
//		if((domObjs.gradeGKChk.attr("checked")=="checked" && domObjs.gradeLZChk.attr("checked")=="checked")
//				|| (domObjs.gradeGKChk.attr("checked")!="checked" && domObjs.gradeLZChk.attr("checked")!="checked")){
//			inTrx.prd_grades = prd_grades;//勾选管控品和滞留品 或者 均未勾选
//	    }else if(domObjs.gradeGKChk.attr("checked")=="checked" && domObjs.gradeLZChk.attr("checked")!="checked"){
//	    	inTrx.prd_grades = "'GK'";//只勾选管控品
//	    }else if(domObjs.gradeGKChk.attr("checked")!="checked" && domObjs.gradeLZChk.attr("checked")=="checked"){
//	    	inTrx.prd_grades = "'LZ'";//只勾选滞留品
//	    }
		
		//prd_grades
		if(domObjs.gradeGKChk.attr("checked")=="checked"&&domObjs.gradeLZChk.attr("checked")!="checked"&&
		   domObjs.gradeNGChk.attr("checked")!="checked"&&domObjs.gradeSCChk.attr("checked")!="checked"	){
	    	inTrx.prd_grades = "'GK'";
	    }else if(domObjs.gradeGKChk.attr("checked")!="checked"&&domObjs.gradeLZChk.attr("checked")=="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")!="checked"&&domObjs.gradeSCChk.attr("checked")!="checked"){
	    	inTrx.prd_grades = "'LZ'";
	    }else if(domObjs.gradeGKChk.attr("checked")!="checked"&&domObjs.gradeLZChk.attr("checked")!="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")=="checked"&&domObjs.gradeSCChk.attr("checked")!="checked"){
	    	inTrx.prd_grades = "'NG'";
	    }else if(domObjs.gradeGKChk.attr("checked")!="checked"&&domObjs.gradeLZChk.attr("checked")!="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")!="checked"&&domObjs.gradeSCChk.attr("checked")=="checked"){
	    	inTrx.prd_grades = "'SC'";
	    }else if(domObjs.gradeGKChk.attr("checked")=="checked"&&domObjs.gradeLZChk.attr("checked")=="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")!="checked"&&domObjs.gradeSCChk.attr("checked")!="checked"){
	    	inTrx.prd_grades = "'GK','LZ'";
	    }else if(domObjs.gradeGKChk.attr("checked")=="checked"&&domObjs.gradeLZChk.attr("checked")!="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")=="checked"&&domObjs.gradeSCChk.attr("checked")!="checked"){
	    	inTrx.prd_grades = "'GK','NG'";
	    }else if(domObjs.gradeGKChk.attr("checked")=="checked"&&domObjs.gradeLZChk.attr("checked")!="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")!="checked"&&domObjs.gradeSCChk.attr("checked")=="checked"){
	    	inTrx.prd_grades = "'GK','SC'";
	    }else if(domObjs.gradeGKChk.attr("checked")!="checked"&&domObjs.gradeLZChk.attr("checked")=="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")=="checked"&&domObjs.gradeSCChk.attr("checked")!="checked"){
	    	inTrx.prd_grades = "'LZ','NG'";
	    }else if(domObjs.gradeGKChk.attr("checked")!="checked"&&domObjs.gradeLZChk.attr("checked")=="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")!="checked"&&domObjs.gradeSCChk.attr("checked")=="checked"){
	    	inTrx.prd_grades = "'LZ','SC'";
	    }else if(domObjs.gradeGKChk.attr("checked")!="checked"&&domObjs.gradeLZChk.attr("checked")!="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")=="checked"&&domObjs.gradeSCChk.attr("checked")=="checked"){
	    	inTrx.prd_grades = "'NG','SC'";
	    }else if(domObjs.gradeGKChk.attr("checked")=="checked"&&domObjs.gradeLZChk.attr("checked")=="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")=="checked"&&domObjs.gradeSCChk.attr("checked")!="checked"){
	    	inTrx.prd_grades = "'GK','LZ','NG'";
	    }else if(domObjs.gradeGKChk.attr("checked")=="checked"&&domObjs.gradeLZChk.attr("checked")=="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")!="checked"&&domObjs.gradeSCChk.attr("checked")=="checked"){
	    	inTrx.prd_grades = "'GK','LZ','SC'";
	    }else if(domObjs.gradeGKChk.attr("checked")=="checked"&&domObjs.gradeLZChk.attr("checked")!="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")=="checked"&&domObjs.gradeSCChk.attr("checked")=="checked"){
	    	inTrx.prd_grades = "'GK','NG','SC'";
	    }else if(domObjs.gradeGKChk.attr("checked")!="checked"&&domObjs.gradeLZChk.attr("checked")=="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")=="checked"&&domObjs.gradeSCChk.attr("checked")=="checked"){
	    	inTrx.prd_grades = "'LZ','NG','SC'";
	    }else if(domObjs.gradeGKChk.attr("checked")!="checked"&&domObjs.gradeLZChk.attr("checked")!="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")!="checked"&&domObjs.gradeSCChk.attr("checked")!="checked"){
	    	inTrx.prd_grades = "'GK','LZ','NG','SC'";
	    }else if(domObjs.gradeGKChk.attr("checked")=="checked"&&domObjs.gradeLZChk.attr("checked")=="checked"&&
	 		   domObjs.gradeNGChk.attr("checked")=="checked"&&domObjs.gradeSCChk.attr("checked")=="checked"){
	    	inTrx.prd_grades = "'GK','LZ','NG','SC'";
	    }
		
		
		//
		
		if((domObjs.dmProcChk.attr("checked")=="checked" && domObjs.jbProcChk.attr("checked")=="checked")
				||(domObjs.dmProcChk.attr("checked")!="checked" && domObjs.jbProcChk.attr("checked")!="checked")){
			inTrx.proc_ids = proc_ids;//勾选镀膜和减薄 或者 均未勾选
	    }else if(domObjs.dmProcChk.attr("checked")=="checked" && domObjs.jbProcChk.attr("checked")!="checked"){
	    	inTrx.proc_ids = "'DMZJ'";//只勾选镀膜
	    }else if(domObjs.dmProcChk.attr("checked")!="checked" && domObjs.jbProcChk.attr("checked")=="checked"){
	    	inTrx.proc_ids = "'JBYZ','JBZJ'";//只勾选减薄
	    }
	
		outTrx = comTrxSubSendPostJson(inTrx);
		if (outTrx.rtn_code == VAL.NORMAL) {
			for (var i = 0; i < outTrx.sht_cnt; i++) {
				var oary1 = outTrx.sht_cnt > 1 ? outTrx.oary[i] : outTrx.oary;
				var prdStat = oary1.prd_stat;
				if(prdStat == "WAIT" ||
					prdStat == "SHIP"||
					prdStat == "SHTC"){
					oary1.prd_stat_dsc = globalBean.getPrdStat(prdStat, oary1.bnk_flg);
				}else{
					oary1.prd_stat_dsc = globalBean.getPrdStat(prdStat);
				}
			}
			setGridInfo(TimestampUtil.formatTimestamp(outTrx.oary),domObjs.grids.defectInfoListGrdSelector,true);
		}
	}

	$("#f1_query_btn").click(f1QueryFnc);
	var dates =$("#beginTimeTxt, #endTimeTxt").datepicker({
		defaultDate : "",
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1,
        onSelect : function(selectedDate) {
	          var option   = this.id == "beginTimeTxt" ? "minDate" : "maxDate",
	           instance = $( this ).data( "datepicker" ),
	           date     = $.datepicker.parseDate(
	             instance.settings.dateFormat||$.datepicker._defaults.dateFormat,
	             selectedDate, instance.settings );
	         dates.not( this ).datepicker( "option", option, date );
	      }
	});
//	initializationFunc();
	document.onkeydown = function(event) {
        if(event.keyCode == ENT_KEY){
            $("#f1_query_btn").click();
            return false;
        }
	}
    $("#f2_export_btn").click(function() {
	    generateExcel(domObjs.grids.defectInfoListGrdSelector);
    });
    
	function resizeFnc(){                                                                      
    	var offsetBottom, divWidth;                                                              
		    
		divWidth = domObjs.grids.$defectInfoListDiv.width();                                   
		offsetBottom = domObjs.$window.height() - domObjs.grids.$defectInfoListDiv.offset().top;
		domObjs.grids.$defectInfoListDiv.height(offsetBottom * 0.95);                          
		domObjs.grids.$defectInfoListGrd.setGridWidth(divWidth * 0.99);                   
		domObjs.grids.$defectInfoListGrd.setGridHeight(offsetBottom * 0.99 - 51);               
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
})