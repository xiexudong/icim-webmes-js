$(document).ready(function() {

	$("#tabDiv").tab('show');
	$("form").submit(function() {
		return false;
	});
	var VAL = {
		NORMAL : "0000000",
		T_XPBISOPE : "XPBISOPE"
	};
	var domObjs = {
		$window : $(window),
		soIDTxt : $("#soIDTxt"),
		zdbeginTimeTxt : $("#beginTimeTxt1"),
		zdendTimeTxt : $("#endTimeTxt1"),
		jlbeginTimeTxt : $("#beginTimeTxt2"),
		jlendTimeTxt : $("#endTimeTxt2"),
		jjbeginTimeTxt : $("#beginTimeTxt3"),
		jjendTimeTxt : $("#endTimeTxt3"),
		grids:{
			$woInfoListDiv:$("#tabPane_wo"),
			$woInfoListGrd:$("#woInfoListGrd"),
			$boxInfoListDiv:$("#tabPane_box"),
			$boxInfoListGrd:$("#boxInfoListGrd")
		}
	};
	$("#beginTimeTxt1, #endTimeTxt1,#beginTimeTxt2, #endTimeTxt2,#beginTimeTxt3, #endTimeTxt3").datepicker({
		defaultDate : "",
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1
	});
	var globalBean = {
		opeObjs : {},
		getOpeDsc : function(opeID, opeVer) {
			var i, oary;
			for (i = 0; i < this.opeObjs.length; i++) {
				oary = this.opeObjs[i];
				if (typeof (opeVer) === "undefined") {
					if (opeID == oary.ope_id) {
						return oary.ope_dsc
					}
				} else {
					if (opeID == oary.ope_id && opeVer == oary.ope_ver) {
						return oary.ope_dsc
					}
				}
			}
		}
	};
	function disPlay() {
		var inObj, outObj;
		$("input").val("");
		$("#woInfoListGrd").jqGrid("clearGridData");
		$("#boxInfoListGrd").jqGrid("clearGridData");
		inObj = {
			trx_id : VAL.T_XPBISOPE,
			action_flg : 'L'
		};
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			globalBean.opeObjs = outObj.oary;
		}
	}

	var WoitemInfoCM = [ {
		name : 'so_id',
		index : 'so_id',
		label : SO_ID_TAG,
		width : WO_ID_CLM,
		align : "center"
	}, {
		name : 'so_pln_prd_qty',
		index : 'so_pln_prd_qty',
		label : "总数量",
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'wo_id',
		index : 'wo_id',
		label : WO_ID_TAG,
		width : WO_ID_CLM,
		align : "center"
	}, {
		name : 'crt_usr',
		index : 'crt_usr',
		label : "制定人",
		width : USER_CLM,
		align : "center"
	}, {
		name : 'crt_timestamp',
		index : 'crt_timestamp',
		label : "制定日期",
		width : TIMESTAMP_CLM,
		align : "center"
	}, {
		name : 'pln_stb_date',
		index : 'pln_stb_date',
		label : "计来日期",
		width : DATE_CLM,
		align : "center"
	}, {
		name : 'pln_cmp_date',
		index : 'pln_cmp_date',
		label : "计交日期",
		width : DATE_CLM,
		align : "center"
	}, {
		name : 'mtrl_prod_id',
		index : 'mtrl_prod_id',
		label : MTRL_PROD_ID_TAG,
		width : MTRL_PROD_ID_CLM,
		align : "center"
	}, {
		name : 'mdl_id',
		index : 'mdl_id',
		label : TH_MDL_ID_TAG,
		width : MDL_ID_CLM,
		align : "center"
	}, {
		name : 'fm_mdl_id',
		index : 'fm_mdl_id',
		label : FM_MDL_ID_TAG,
		width : MDL_ID_CLM,
		align : "center"
	}, {
		name : 'cut_mdl_id',
		index : 'cut_mdl_id',
		label : CUT_MDL_ID_TAG,
		width : MDL_ID_CLM,
		align : "center"
	}, {
		name : 'wo_pln_prd_qty',
		index : 'wo_pln_prd_qty',
		label : MTRL_RL_QTY_TAG,
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'recive_timestamp',
		index : 'recive_timestamp',
		label : "来料时间",
		width : TIMESTAMP_CLM,
		align : "center"
	} ];
	$("#woInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 400,// TODO:高度如何调整
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		scroll : false,// 如需要分页控件,必须将此项置为false
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : $("#woInfoListPg"),
		fixed : true,
		colModel : WoitemInfoCM,
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});
	var itemInfoCM = [ {
		name : 'wo_id',
		index : 'wo_id',
		label : WO_ID_TAG,
		width : WO_ID_CLM,
		align : "center"
	}, {
		name : 'dn_no',
		index : 'dn_no',
		label : "交货订单号",
		width : DN_NO_CLM,
		align : "center"
	}, {
		name : 'wo_pln_prd_qty',
		index : 'wo_pln_prd_qty',
		label : "总数量",
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'rl_prd_qty',
		index : 'rl_prd_qty',
		label : "已投数",
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'un_rl_prd_qty',
		index : 'un_rl_prd_qty',
		label : "未投数",
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'comp_prd_qty',
		index : 'comp_prd_qty',
		label : "已完数",
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'un_comp_prd_qty',
		index : 'un_comp_prd_qty',
		label : "未完数",
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'wh_out_prd_qty',
		index : 'wh_out_prd_qty',
		label : "已交数",
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'un_wh_out_prd_qty',
		index : 'un_wh_out_prd_qty',
		label : "未交数",
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'grade_gk_qty',
		index : 'grade_gk_qty',
		label : GRADE_CONTROL_TAG,
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'grade_ng_qty',
		index : 'grade_ng_qty',
		label : NG_CNT_TAG,
		width : QTY_CLM,
		align : "center"
	}, {
		name : 'grade_lz_qty',
		index : 'grade_lz_qty',
		label : GRADE_SCRP_TAG,
		width : QTY_CLM,
		align : "center"
	}, {
		name : "lot_id",
		index : "lot_id",
		label : LOT_ID_TAG,
		widht : LOT_ID_CLM,
		align : "center"
	}, {
		name : 'box_id',
		index : 'box_id',
		label : BOX_ID_TAG,
		width : BOX_ID_CLM,
		align : "center"
	}, {
		name : "ope_id",
		index : "ope_id",
		label : OPE_ID_TAG,
		widht : OPE_ID_CLM,
		align : "center"
	} ];
	$("#boxInfoListGrd").jqGrid({
		url : "",
		datatype : "local",
		mtype : "POST",
		height : 390,// TODO:高度如何调整
		autowidth : true,
		shrinkToFit : false,
		autoScroll : true,
		loadonce : true,
		viewrecords : true, // 显示总记录数
		emptyrecords : true,
		pager : "#boxInfoListPg",
		fixed : true,
		colModel : itemInfoCM,
		gridComplete : function() {
			var gridPager, pageLen;
			gridPager = $(this).jqGrid("getGridParam", "pager");
			if (gridPager.length < 2) {
				return false;
			}
			$("#sp_1_" + gridPager.substr(1, pageLen - 1)).hide();
			$(".ui-pg-input").hide();
			$('td[dir="ltr"]').hide();
			$(gridPager + "_left").hide();
			$(gridPager + "_center").hide();
		}
	});

	domObjs.soIDTxt.keyup(function(e) {
		$(this).val($(this).val().toUpperCase());
		if (e.keyCode == 13) {
			f1QueryFnc();
		}
	})

	function f1QueryFnc() {
		var soId,inObj, outObj, iary, oary1Cnt, oary2Cnt, oary1Arr, oary2Arr, oary1, oary2, i;
		soId = domObjs.soIDTxt.val().trim();
		zdfrom_date = domObjs.zdbeginTimeTxt.val();
		zdto_date = domObjs.zdendTimeTxt.val();
		jlfrom_date = domObjs.jlbeginTimeTxt.val();
		jlto_date = domObjs.jlendTimeTxt.val();
		jjfrom_date = domObjs.jjbeginTimeTxt.val();
		jjto_date = domObjs.jjendTimeTxt.val();
		
		var from_year,from_month,from_day;
		var to_year,to_month,to_day;
		from_year = zdfrom_date.substr(0,4);
		from_month = zdfrom_date.substr(5,2);
		from_day = zdfrom_date.substr(8,2);
		to_year = zdto_date.substr(0,4);
		to_month = zdto_date.substr(5,2);
		to_day = zdto_date.substr(8,2);
		if(from_year!=""&&to_year!=""){
		if(from_year<to_year){
		}else if(from_year == to_year){
			if(from_month<to_month){
			}else if(from_month==to_month){
				if(from_day<=to_day){
				}else{
					showErrorDialog("", "年份和月份相同的情况下，开始天数大于结束天数，请检查！");
					return false;
				}
			}else {
				showErrorDialog("", "年份相同的情况下，开始月份大于结束月份，请检查！");
				return false;
			}
		}else{
			showErrorDialog("", "开始年份大于结束年份，请检查！");
			return false;
		}
		}
		var from_year1,from_month1,from_day1;
		var to_year1,to_month1,to_day1;
		from_year1 = jlfrom_date.substr(0,4);
		from_month1 = jlfrom_date.substr(5,2);
		from_day1 = jlfrom_date.substr(8,2);
		to_year1 = jlto_date.substr(0,4);
		to_month1 = jlto_date.substr(5,2);
		to_day1 = jlto_date.substr(8,2);
		if(from_year1!=""&&to_year1!=""){
		if(from_year1<to_year1){
		}else if(from_year1 == to_year1){
			if(from_month1<to_month1){
			}else if(from_month1==to_month1){
				if(from_day1<=to_day1){
				}else{
					showErrorDialog("", "年份和月份相同的情况下，开始天数大于结束天数，请检查！");
					return false;
				}
			}else {
				showErrorDialog("", "年份相同的情况下，开始月份大于结束月份，请检查！");
				return false;
			}
		}else{
			showErrorDialog("", "开始年份大于结束年份，请检查！");
			return false;
		}
		}
		var from_year2,from_month2,from_day2;
		var to_year2,to_month2,to_day2;
		from_year2 = jjfrom_date.substr(0,4);
		from_month2 = jjfrom_date.substr(5,2);
		from_day2 = jjfrom_date.substr(8,2);
		to_year2 = jjto_date.substr(0,4);
		to_month2 = jjto_date.substr(5,2);
		to_day2 = jjto_date.substr(8,2);
		if(from_year2!=""&&to_year2!=""){
		if(from_year2<to_year2){
		}else if(from_year2 == to_year2){
			if(from_month2<to_month2){
			}else if(from_month2==to_month2){
				if(from_day2<=to_day2){
				}else{
					showErrorDialog("", "年份和月份相同的情况下，开始天数大于结束天数，请检查！");
					return false;
				}
			}else {
				showErrorDialog("", "年份相同的情况下，开始月份大于结束月份，请检查！");
				return false;
			}
		}else{
			showErrorDialog("", "开始年份大于结束年份，请检查！");
			return false;
		}
		}
		inObj = {
				trx_id : "XPINQSOR",
				action_flg : "M",
				so_id : soId,
			};
        if(zdfrom_date){
        	inObj.zdfrom_date = zdfrom_date+" 00:00:00.0";
        }
        if(zdto_date){
        	inObj.zdto_date = zdto_date+" 23:59:59.0";
        }
        if(jlfrom_date){
        	inObj.jlfrom_date = jlfrom_date+" 00:00:00.0";
        }
        if(jlto_date){
        	inObj.jlto_date = jlto_date+" 23:59:59.0";
        }
        if(jjfrom_date){
        	inObj.jjfrom_date = jjfrom_date+" 00:00:00.0";
        }
        if(jjto_date){
        	inObj.jjto_date = jjto_date+" 23:59:59.0";
        }
        
		
		
		outObj = comTrxSubSendPostJson(inObj);
		if (outObj.rtn_code == VAL.NORMAL) {
			
			setGridInfo(TimestampUtil.formatTimestamp(outObj.oary1), "#woInfoListGrd");
			oary2Cnt = outObj.oary2_cnt;
			for (i = 0; i < oary2Cnt; i++) {
				oary2 = oary2Cnt > 1 ? outObj.oary2[i] : outObj.oary2;
				oary2.ope_id = globalBean.getOpeDsc(oary2.ope_id);
			}
			setGridInfo(outObj.oary2, "#boxInfoListGrd");
		}
	}
	disPlay();
	$("#f1_query_btn").click(f1QueryFnc);
	
	$("#export_wo_btn").click(function(){
		generateExcel("#woInfoListGrd");
	}) 
	
	$("#export_box_btn").click(function(){
		generateExcel("#boxInfoListGrd");
	})
	
	function resizeFnc(){
		var offsetBottom, grdDivWidth;
		tabPaneDivWidth = $("#tabContentDiv").width();
		
		offsetBottom = domObjs.$window.height() - $("#tabContentDiv").offset().top;
		domObjs.grids.$woInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$woInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$woInfoListGrd.setGridHeight(offsetBottom*0.95-51);
		
		domObjs.grids.$boxInfoListDiv.width(tabPaneDivWidth*0.99);
		domObjs.grids.$boxInfoListDiv.height(offsetBottom*0.96);
		domObjs.grids.$boxInfoListGrd.setGridWidth(tabPaneDivWidth*0.99);
		domObjs.grids.$boxInfoListGrd.setGridHeight(offsetBottom*0.95-51);
    };                                                                                         
    resizeFnc();                                                                               
    domObjs.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
})
