  /**************************************************************************/
  /*                                                                        */
  /*  System  Name :  ICIM                                                  */
  /*                                                                        */
  /*  Description  :  WH Rerurn By Box                                      */
  /*                                                                        */
  /*  MODIFICATION HISTORY                                                  */
  /*    Date     Ver     Name          Description                          */
  /* ---------- ----- ----------- ----------------------------------------- */
  /* 2014/05/12 N0.00   meijiao.C      Initial release                      */
  /*                                                                        */
  /**************************************************************************/
$(document).ready(function() {
	var VAL ={
	        NORMAL     : "0000000"  ,
	        EVT_USER   : $("#userId").text(),
	        T_XPWMSOPE : "XPWMSOPE" ,
	        T_XPINQBOX : "XPINQBOX" ,
            T_XPAPLYWO : "XPAPLYWO" ,
	        T_XPINQSHT : "XPINQSHT",
	        T_XPBISPTH : "XPBISPTH",
//	        T_XPJMPOPE : "XPJMPOPE",
	        T_XPINQCOD : "XPINQCOD",
	        T_XPBISOPE : "XPBISOPE"
	    };
    var gSwhOpe = "";
	/**
	 All controls's jquery object/text
     * @type {Object}
     */
	 var domObj = {
				$window : $(window),
				$shtListDiv : $("#shtListDiv"),
				$shtListGrd : $("#shtListGrd"),
				$bx1        : $("#bx1"),
				$bx2        : $("#bx2")
	 };
	 var controlsQuery = {
        W              : $(window)           ,
        box_id         : $("#boxIDTxt")      ,
        shipboxInput   : $("#shipboxInput")      ,
        woIdSelect     : $("#woIdSelect")    ,
        newWoIdSelect  : $("#newWoIdSelect") ,
        opeSel         : $("#opeSel"),
        pathIdSel      : $("#pathIDSel"),
        pathVerSel     : $("#pathVerSel"),
        mainGrd    :{
        	grdId     : $("#shtListGrd")   ,
            grdPgText : "#shtListPg"       ,
            fatherDiv : $("#shtListDiv")
        }
    };
	 
	   $("#bx1").attr("checked",false);
	   $("#bx2").attr("checked",false);
       $("#newWoIdSelect").attr({'disabled':false});
       $("#opeSel").attr({'disabled':false});
	   $("#bx1").click(function(){
			      $("#bx1").attr("checked")=="checked";
			      $("#bx2").attr("checked",false);
			      $("#newWoIdSelect").attr({'disabled':true});
			      $("#opeSel").attr({'disabled':false});
			      $("#newWoIdSelect").val("");
			      $("#opeSel")[0].options.length = 0;
			      toolFunc.iniClearFunc();
			   });
	   $("#bx2").click(function(){
		      $("#bx2").attr("checked")=="checked";
	    	  $("#bx1").attr("checked",false);
		      $("#opeSel").attr({'disabled':true});
		      $("#newWoIdSelect").attr({'disabled':false});
		      $("#opeSel").empty();
		 });
	 var btnQuery = {
         f1 : $('#query_btn'),
         f6 : $('#rtn_btn')
     };
	 var toolFunc = {
         iniClearFunc: function(){
            var ope1="J001",ope2="J007",ope3="QP02",ope4="QD02";
            var opeStr ="<option value='' ></option>"+
            			"<option value="+ope1+">"+"来料检"+"</option>"+            			
            			"<option value="+ope2+">"+"涂胶包装"+"</option>"+
            			"<option value="+ope3+">"+"抛光包装QA"+"</option>"+
            			"<option value="+ope4+">"+"镀膜包装QA"+"</option>";
            controlsQuery.box_id.val("");
            controlsQuery.shipboxInput.val("");
            controlsQuery.box_id.focus();
            controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
            controlsQuery.opeSel.append(opeStr);
//            controlsQuery.opeSel.select2({
//    	    	theme : "bootstrap"
//    	    });
        },
         iniWoIdSelect : function(){
             var inObj,
                 outObj;
             inObj = {
                 trx_id      : VAL.T_XPAPLYWO,
                 action_flg  : 'Q'           ,
                 iary : {
                	 wo_stats : "'WAIT'"
                 }
             };

             outObj = comTrxSubSendPostJson(inObj);
             if (outObj.rtn_code == VAL.NORMAL) {
                 _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woIdSelect", false);
                 _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#newWoIdSelect", true);
             }
         },
         //Query box in list
         queryBox_func : function(){
             var box_id;

             box_id = $.trim(controlsQuery.box_id.val());
             if(!box_id){
                 showErrorDialog("","请输入箱号!");
                 return false;
             }

             _findRowInGrid('shtListDiv', controlsQuery.mainGrd.grdId, 'box_id', box_id);
             return true;
         },
         queryBox_func1 : function(){
 	        var	 ship_box_id = controlsQuery.shipboxInput.val();
            if(!ship_box_id){
                showErrorDialog("","请输入出货箱号!");
                return false;
            }
        	var boxinObj = {
                    trx_id      : VAL.T_XPINQBOX,
                    action_flg  : 'G'           ,
                    box_id      : ship_box_id
               };
       		var boxObj = comTrxSubSendPostJson(boxinObj);
               if(boxObj.rtn_code != VAL.NORMAL){
           	    return false;
               }

             _findRowInGrid('shtListDiv', controlsQuery.mainGrd.grdId, 'box_id', boxObj.box_id);
             return true;
         }
	 };
	 var btnFunc = {
		  f1_func:function(){
              'use strict';
              var wo_id,
                  inObj,
                  outObj,
                  tbl_cnt;

              controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
              wo_id = controlsQuery.woIdSelect.val();
              if(!wo_id){
                  showErrorDialog('P4C00-101','请选择要查询的内部订单号');
                  return false;
              }

              inObj = {
                  trx_id : VAL.T_XPINQBOX,
                  action_flg : 'C'   ,
                  box_stat   : 'SHIP',
                  wo_id      : wo_id 
              };

			  outObj = comTrxSubSendPostJson(inObj);
			  if( outObj.rtn_code == VAL.NORMAL ){
                  tbl_cnt = parseInt(outObj.box_cnt, 10);
                  if(tbl_cnt > 0){
                      setGridInfo(outObj.table, "#shtListGrd");
                      return true;
                  }
			  }

              return true;
		  },
		  f6_func:function(){
			  'use strict';
              var i,
                  ope_id,
                  inObj,
                  outObj,
			      iary = [],
                  grdId,
                  idLen,
                  rowIds,
                  rowData,
                  wo_id,
                  new_wo_id,
                  left_mdl_id="",
                  right_mdl_id="",
                  inObjWO,
                  outObjWO;

              grdId = controlsQuery.mainGrd.grdId;
              rowIds = grdId.jqGrid("getGridParam","selarrrow");
        	  
              idLen = rowIds.length;
			  if(idLen === 0){
					showErrorDialog("002","请选择需要操作的箱号！");
	          		return false;
			  }

              for( i=0; i < idLen; i++){
                  rowData = grdId.jqGrid('getRowData',rowIds[i]);
                  iary.push({
                      box_id : rowData['box_id']
                  });
              }

			  inObj={
				  trx_id     : VAL.T_XPWMSOPE,
				  action_flg : 'R',
                  box_cnt    : idLen,
				  iary       : iary,
				  evt_user   : VAL.EVT_USER
			  };

              wo_id = controlsQuery.woIdSelect.val();
              if(!wo_id){
                  showErrorDialog("004","请选择内部订单！");
                  return false;
              }
              inObj.wo_id = wo_id;

              ope_id = controlsQuery.opeSel.val();
              new_wo_id = controlsQuery.newWoIdSelect.val();
              if(new_wo_id){
                  inObj.use_new_wo_id = 'Y';
                  inObj.new_wo_id = new_wo_id;
              }else{
                  if(!ope_id){
                      showErrorDialog("003","请选择站点信息！");
                      return false;
                  }
                  inObj.ope_id = ope_id;
              }
              
              if (new_wo_id) {
      	        inObjWO = {
    		            trx_id      : VAL.T_XPAPLYWO,
    		            action_flg  : 'Q'           ,
    		            iary        : { wo_id : wo_id }
    		    };
    		
    	        outObjWO = comTrxSubSendPostJson(inObjWO);
    	        if(outObjWO.rtn_code == VAL.NORMAL) {
    	        	left_mdl_id = outObjWO.oary.mdl_id;
    	        }
    	        
    	        inObjWO = {
    		            trx_id      : VAL.T_XPAPLYWO,
    		            action_flg  : 'Q'           ,
    		            iary        : { wo_id : new_wo_id }
    		    };
    		
    	        outObjWO = comTrxSubSendPostJson(inObjWO);
    	        if(outObjWO.rtn_code == VAL.NORMAL) {
    	        	right_mdl_id = outObjWO.oary.mdl_id;
    	        }

    	        if (left_mdl_id != right_mdl_id) {
    	        	btnQuery.f6.showCallBackWarnningDialog({
    					errMsg : "工单: ["+wo_id+"]产品型号:["+left_mdl_id+"]与工单:["+new_wo_id+"]产品型号:["+right_mdl_id+"]不一致，是否变更?",
    					callbackFn : function(data) {
    						if (data.result == true) {
    						   outObj = comTrxSubSendPostJson( inObj);
			    			   if( outObj.rtn_code == VAL.NORMAL ){
			    				   showSuccessDialog("返工成功!");
			                       return true;
			    			   }
    				        }else {
    				           return false;
    				        }						
    					}	        		
    	        	});
    	        }else{
    	        	outObj = comTrxSubSendPostJson( inObj);
	    			   if( outObj.rtn_code == VAL.NORMAL ){
	    				   showSuccessDialog("返工成功!");
	                       return true;
	    			   }
    	        }

              }else{
                  outObj = comTrxSubSendPostJson( inObj);
    			  if( outObj.rtn_code == VAL.NORMAL ){
    				  showSuccessDialog("返工成功!");
                      return true;
    			  }
              }


              return false;
		  }
	 };

	 var iniGridInfo = function(){
			var grdInfoCM = [
                {name:"box_id"   , index:"box_id"   , label:BOX_ID_TAG , width :BOX_ID_CLM  , align:"left", sortable:true},
                {name: 'ship_box_id',   index: 'ship_box_id',   label: "出货箱号"   , width: BOX_ID_CLM,align:"left", sortable:true},
                {name:"std_qty"  , index:"std_qty"  , label:STD_QTY_TAG, width :QTY_CLM     , align:"left", sortable:true},
                {name:"prd_qty"  , index:"prd_qty"  , label:PNL_CNT_TAG, width :QTY_CLM     , align:"left", sortable:true},
                {name:"wo_id_fk" , index:"wo_id_fk" , label:WO_ID_TAG  , width :WO_ID_CLM   , align:"left", sortable:true},
                {name:"lot_id_fk", index:"lot_id_fk", label:LOT_ID_TAG , width :LOT_ID_CLM  , align:"left", sortable:true},
	            {name:"box_stat" , index:"box_stat" , label:STATUS_TAG , width :PRD_STAT_CLM, align:"left", sortable:true},
	            {name:"mdl_id_fk", index:"mdl_id_fk", label:MDL_ID_TAG , width :MDL_ID_CLM  , align:"left", sortable:true}
			];
	        $("#shtListGrd").jqGrid({
	              url:"",
			      datatype:"local",
			      mtype:"POST",
			      height:320,
			      autowidth:true,//宽度根据父元素自适应
			      shrinkToFit:false,
			      scroll:true,
			      resizable : true,
			      loadonce:true,
			      fixed: true,
			      viewrecords : true, //显示总记录数
			      pager : '#shtListPg',
			      rownumbers  :true ,//显示行号
                  multiselect : true,
//			      rowNum:30,         //每页多少行，用于分页
//			      emptyrecords :true ,
			      colModel: grdInfoCM
	        });
	 };

	 /**
	    * Bind button click action
	    */
     var iniButtonAction = function(){
	       btnQuery.f1.click(function(){
	           btnFunc.f1_func();
	       });
	       btnQuery.f6.click(function(){
	           btnFunc.f6_func();
	       });
	 };

	 controlsQuery.box_id.keydown(function(event){
	   	if( event.keyCode == 13 ){
            toolFunc.queryBox_func();
	   	}
     });
	 controlsQuery.shipboxInput.keydown(function(event){
		   	if( event.keyCode == 13 ){
	            toolFunc.queryBox_func1();
		   	}
	     });

	 var initializationFunc = function(){
	  	toolFunc.iniClearFunc();
        toolFunc.iniWoIdSelect();
	   	iniGridInfo();
	   	iniButtonAction();
	   	$("form").submit(function(){
           return false ;
        });
	 };
	 $("#opeSel").change(function(){
         gSwhOpe = controlsQuery.opeSel.val();

	 });
	 $("#newWoIdSelect").change(function(){
         var new_wo_id,inObj, outObj;
         new_wo_id = controlsQuery.newWoIdSelect.val();
	     inObj = {
	         trx_id      : VAL.T_XPAPLYWO,
	         action_flg  : 'Q'           ,
	         iary        : { wo_id : new_wo_id }
	     };
	
	     outObj = comTrxSubSendPostJson(inObj);
	     if (outObj.rtn_code == VAL.NORMAL) {
	         gSwhOpe = outObj.oary.str_ope_no;
	     }		 

	 });	 
	    
	 initializationFunc();

	 function resizeFnc(){
	   	comResize(domObj.$window,domObj.$shtListDiv,domObj.$shtListGrd);
     }

	 resizeFnc();

	 domObj.$window.resize(function() {
	   	resizeFnc();
	 });
	  
});