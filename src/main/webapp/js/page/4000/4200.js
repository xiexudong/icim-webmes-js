$(document).ready(function() {
	var DestoutObj="";
	var _NORMAL = "0000000",
      T_XPWHSOPE = "XPWHSOPE",
      T_XPAPLYWO = "XPAPLYWO",
      EVT_USER = $("#userId").text(),
      _woAry = [];

    var controlsQuery = {
        vdrIDSelect    : $("#vdrIDSelect")   ,
        mtrlTypeSelect : $("#mtrlTypeSelect"),
        destShopSelect : $("#destShopSelect"),
    };
    var domObj = {
			$window : $(window),
			$paneluseDiv : $("#paneluseDiv"),
			$paneluseGrd : $("#paneluseGrd")
	};
    
    var VAL = {
    		NORMAL : "0000000",
    		DISABLE_ATTR : {
    			"disabled" : true
    		},
    		ENABLE_ATTR : {
    			"disabled" : false
    		}
    	};
  /**
   * All button's jquery object
   * @type {Object}
   */
  var btnQuery = {
      f1 : $('#f1_btn'),
      f2 : $('#f2_btn'),
      f4 : $('#f4_btn'),
      f8 : $('#f8_btn'),
      f5 : $('#f5_btn')
  };

  /**
   * Clear user input control
   */
  var clearInput = function(){
      $("#useUserInput").val('');
      $("#giveUserInput").val('');

      $("#woIdSp").val('');
      $("#woCntSp").val('');
      $("#wfrlCntSp").val('');
      $("#waitCntSp").val('');
      $("#mdlTypeSp").val('');
      $("#mtrlTypeSp").val('');
      $("#cusIdSp").val('');
      
      $("#rawBoxIDInput").val('').focus();
  };


  /**
   * [resetJqgrid description]
   * @return {[type]} [description]
   */
  var resetJqgrid = function(){
//      $(window).unbind("onresize"); 
//      $("#paneluseGrd").setGridWidth($("#paneluseDiv").width()*0.80);
//      $(window).bind("onresize", this);  
  };

  var baseFnc = {
			replaceSpace:function(str){
				if(!str){
					return str;
				}
				return str.replace(/[ ]/g,"@");	
			},
			returnSpace:function(str){
				if(!str){
					return str;
				}
				return str.replace(/[@]/g," ");
			},
			getMdlId:function(){
				var mdlId = baseFnc.returnSpace(domObjs.$thMdlIdSel.val().trim());
				if(!mdlId){
					mdlId = baseFnc.returnSpace(domObjs.$fmMdlIDSel.val().trim()); 
				}
				if(!mdlId){
					mdlId = baseFnc.returnSpace(domObjs.$cutMdlIDSel.val().trim());
				}
				return mdlId;
			}
		};
  
  /**
   * grid  initialization
   */
  var iniGridInfo = function(){
     var infoCM = [
         {name: 'box_id',         index: 'box_id',         label: PPBOX_ID_TAG     , width: BOX_ID_CLM},
         {name: 'wo_id',          index: 'wo_id',          label: WO_ID_TAG     , width: WO_ID_CLM},
         {name: 'dest_shop',      index: 'dest_shop',      label: DEST_SHOP_TAG    , width: DEST_SHOP_CLM },
         {name: 'dest_shop_dsc',      index: 'dest_shop_dsc',      label: DEST_SHOP_TAG    , hidden : true},
         {name: 'count',          index: 'count',          label: TOTAL_QTY_TAG    , width: QTY_CLM },
         {name: 'weight',         index: 'weight',         label: WEIGHT_TAG       , width: QTY_CLM },
         {name: 'rcv_user',       index: 'rcv_user',       label: "入库人员"  , width: USER_CLM},
         {name: 'rcv_timestamp',  index: 'rcv_timestamp',  label: "入库时间"  , width: TIMESTAMP_CLM},
         {name: 'evt_user',       index: 'evt_user',       label: "发料人员"  , width: USER_CLM,  hidden: true},
         {name: 'evt_timestamp',  index: 'evt_timestamp',  label: "发料时间"  , width: TIMESTAMP_CLM, hidden: true},
         {name: 'relate_user'  ,  index: 'relate_user'  ,  label:     "收料人员"  , width: USER_CLM,  hidden: true},
         {name: 'evt_timestamp',  index: 'evt_timestamp',  label: "收料时间"  , width: TIMESTAMP_CLM, hidden: true},
         {name: 'cus_id',         index: 'cus_id'       ,  label: ""  , width: TIMESTAMP_CLM, hidden: true},
         {name: 'ppbox_stat'   ,  index: 'ppbox_stat'   ,  label: '', width: 1 , hidden: true},
         {name: 'bind_so_flg'  ,  index: 'bind_so_flg'  ,  label: '', width: 1 , hidden: true}
     ];
     $('#paneluseGrd').jqGrid({
         url:"",
         datatype:"local",
         mtype:"POST",
         height:360,
         width:650,
         shrinkToFit:false,
         scroll:true,
         rownumWidth : true,
         resizable : true,
         rowNum:40,
         loadonce:true,
         multiselect : true,
         viewrecords:true,
         pager : 'panelusePg',
         fixed: true,
         onSelectRow : function (id,status){//单选时，若选择箱号不符合发料条件重置该次选择
    	     if(status){
        	     var selectRowData = $("#paneluseGrd").jqGrid("getRowData",id);
        	     var wo_id = selectRowData['wo_id'];
        	     var box_id = selectRowData['box_id'];
        	     if(!setWoInfo(wo_id,box_id)){
        	    	 $("#paneluseGrd").jqGrid("resetSelection",id);
        	     }else{
        	    	 $("#paneluseGrd").jqGrid('setRowData',id,{
     	    		    bind_so_flg: 'Y'
     	             });
        	     }
    	     }
         },
         onSelectAll : function (ids,status){//全选时，若有箱子不符合发料条件，重置选择
    	     if(status){
    	    	 for(var i=0;i < ids.length; i++){
            	     var selectRowData = $("#paneluseGrd").jqGrid("getRowData",ids[i]);
            	     var wo_id = selectRowData['wo_id'];
            	     var box_id = selectRowData['box_id'];
            	     if(!setWoInfo(wo_id,box_id)){
            	    	 $("#paneluseGrd").jqGrid("resetSelection");
            	    	 break;
            	     }else{
            	    	 $("#paneluseGrd").jqGrid('setRowData',ids[i],{
            	    		    bind_so_flg: 'Y'
            	            });
            	     }
    	    	 }
    	     }
         },
         colModel: infoCM
     }); 
  };

  /**
   * Query wo id list which bind So
   */
  var setWoIdList = function(){
      var inObj,
          outObj;
      inObj = {
          trx_id      : T_XPAPLYWO,
          action_flg  : 'I'
      };

      outObj = comTrxSubSendPostJson(inObj);
      if (outObj.rtn_code == _NORMAL) {
          _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woSelect", true);

          _woAry = _.pluck(outObj.oary, 'wo_id');
      }
  };

    /**
     * 初始化客户代码
     */
    var setVdrIDSelect = function(){
        var inTrxObj,
            outTrxObj;

        inTrxObj = {
            trx_id      : 'XPLSTDAT' ,
            action_flg  : 'Q'        ,
            iary        : {
                data_cate : "CUSD"
            }
        };
        outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if( outTrxObj.rtn_code == _NORMAL ) {
            if(1 < parseInt(outTrxObj.tbl_cnt, 10)){
                outTrxObj.oary.sort(function(x, y){
                    if(x.data_item < y.data_item){
                        return -1;
                    }else if(x.data_item > y.data_item){
                        return 1;
                    }else{
                        return 0;
                    }
                });
            }
            _setSelectDate(outTrxObj.tbl_cnt, outTrxObj.oary,
                "data_item", "data_item", "#vdrIDSelect", true);
        }
    };

    /**
     * 初始化来料型号
     * @returns {boolean}
     */
  var setMtrlTypeSelect = function(){
        "use strict";
        var crSelect = controlsQuery.mtrlTypeSelect,
            vdrID,
            i,
            tbl_cnt,
            inTrxObj,
            outTrxObj,
            oary;

        crSelect.empty();
        crSelect.append('<option ></option>');

        vdrID = controlsQuery.vdrIDSelect.val();
        if(!vdrID){
            console.log('客户代码为空。');
            return false;
        }

        inTrxObj = {
            trx_id     : 'XPLSTDAT' ,
            action_flg : 'H' ,
            iary       : {
                data_cate : 'CSMT',
                data_item : vdrID
            }
        };

        outTrxObj = comTrxSubSendPostJson(inTrxObj);
        if( outTrxObj.rtn_code == _NORMAL ) {
            tbl_cnt = parseInt( outTrxObj.tbl_cnt, 10);
            if(tbl_cnt <=0){
            	return;
            }
            oary = tbl_cnt > 1 ? outTrxObj.oary : [outTrxObj.oary];
            for(i=0;i<tbl_cnt;i++){
            	crSelect.append('<option value=' + baseFnc.replaceSpace(oary[i]['ext_1']) + '>'+ oary[i]['ext_1'] +'</option>');
            }
            
        }

        crSelect.select2({
	    	theme : "bootstrap"
	    });
    };
    
    var iniWoIdSelect =function(){
        var inObj,
            outObj;
//        var iary=[];
        var vdr_id = $.trim(controlsQuery.vdrIDSelect.val()); 
        var mtrl_type = $.trim(baseFnc.returnSpace(controlsQuery.mtrlTypeSelect.val()));
        inObj = {
                trx_id      : 'XPAPLYWO',
                action_flg  : 'Q'           ,
        		iary       : {
        			wo_stat: "WAIT"
        		}

            };

        if( vdr_id ){
    		inObj.iary.cus_id = vdr_id;        		
        }
        if(mtrl_type ){
    		inObj.iary.mtrl_prod_id_fk = mtrl_type;        		
        }        
//   	    iary.push({
//		 wo_stat : "WAIT"
//        });
//        if(vdr_id!=""&&mtrl_type==""){
//        	 iary.push({
//        		 wo_stat : "WAIT",
//        		 cus_id : vdr_id
//             });
//        }
//        if(mtrl_type!=""&&vdr_id==""){
//       	 iary.push({
//       		 wo_stat : "WAIT",
//    		 mtrl_prod_id_fk : mtrl_type
//         });
//        }
//        if(mtrl_type!=""&&vdr_id!=""){
//          	 iary.push({
//          		 wo_stat : "WAIT",
//          		cus_id : vdr_id,
//       		 mtrl_prod_id_fk : mtrl_type
//            });
//           }
//        if(mtrl_type!=""&&vdr_id==""){
//          	 iary.push({
//          		 wo_stat : "WAIT",
//       		
//            });
//           }
//
//        inObj = {
//            trx_id      :'XPAPLYWO',
//            action_flg  : 'Q'           ,
//            iary : iary
//
//        };

        outObj = comTrxSubSendPostJson(inObj);
        if (outObj.rtn_code == _NORMAL) {
            _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woSelect", true);
        }
    };
    /**
     * 筛选出用户可以操作的仓位
     */
    setDestShopSel = function(){
    	var inObj;
    	var data_ext = "'C','D'";
    	var iary={
    		data_cate : 'DEST',
    		data_ext  : data_ext,
    		user_id   : EVT_USER
    	};
    	inObj = {
		   trx_id     : 'XPLSTDAT' ,
           action_flg : 'F'        ,
           iary       : iary
    	};
    	DestoutObj = comTrxSubSendPostJson(inObj);
    	if(DestoutObj.rtn_code == _NORMAL){
    		_setSelectDate(DestoutObj.tbl_cnt,DestoutObj.oary,"data_id","data_desc","#destShopSelect",true);
    	}
    };
    /**
     * Show worder detail info
     * @returns {boolean}
     */
    var setWoInfo = function(wo_id,box_id){
      clearInput();

      var inTrxObj,
          outTrxObj,
          hasBindSo;

      hasBindSo = false;
      
      if(!wo_id){
          return false;
      }
      
      inTrxObj ={
          trx_id     : T_XPAPLYWO,
          action_flg : 'Q',
          tbl_cnt    : 1,
          iary  : {
              wo_id     : wo_id ,
              sum_cnt   : 'Y'   ,
              wo_stat   : "WAIT",
              mtrl_cate : "SHMT"
          }
      };


      outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code == _NORMAL && outTrxObj.oary) {

          $("#woIdSp").val(outTrxObj.oary.wo_id);
          $("#woCntSp").val(outTrxObj.oary.pln_prd_qty);
          $("#wfrlCntSp").val(
              parseInt(outTrxObj.oary.pln_prd_qty, 10) -
                  parseInt(outTrxObj.oary.wait_box_sum, 10)
          );
          $("#waitCntSp").val(outTrxObj.oary.wait_box_sum);
          $("#mdlTypeSp").val(outTrxObj.oary.mdl_id);
          $("#mtrlTypeSp").val(outTrxObj.oary.mtrl_prod_id);
          $("#cusIdSp").val(outTrxObj.oary.cus_id_fk);

          //Check SO
            if(!outTrxObj.oary.hasOwnProperty("so_id") ||
                !outTrxObj.oary.so_id){
            	hasBindSo = false;
            	if(!box_id){
                    showErrorDialog("","内部订单["+ wo_id +"]尚未绑定客户订单，不允许发料！");
            	}else{
                    showErrorDialog("","箱号["+ box_id+ "]的内部订单["+ wo_id +"]尚未绑定客户订单，不允许发料！");
            	}
                return false;
            }else{
            	hasBindSo = true;
            }
      }
        return hasBindSo;
    };

  /**
   * All button click function
   * @type {Object}
   */
  var btnFunc = {
      //Query box list pre check,待分配的为WAIT
      f1_func : function(){
          "use strict";
          var wo_id,
              vdr_id,
              mtrl_type,
              dest_shop,
              iary = {};

	      var showAry = ["rcv_user","rcv_timestamp"];
	      var hideAry = ["evt_user","evt_timestamp","relate_user"];
	      $("#paneluseGrd").jqGrid("showCol",showAry).jqGrid("hideCol",hideAry);

	      /**
           * 判断用户是否有可以操作的仓位
           */
          if(DestoutObj.tbl_cnt == 0){
          	showErrorDialog("","此用户没有可以操作的仓位！");
              return false;
          }
          
          vdr_id = $.trim(controlsQuery.vdrIDSelect.val());
          mtrl_type = $.trim(baseFnc.returnSpace(controlsQuery.mtrlTypeSelect.val()));
          wo_id = $.trim($("#woSelect").val());
          dest_shop = $.trim(controlsQuery.destShopSelect.val());
          
          if(dest_shop){
              iary.dest_shop = "'"+dest_shop+"'";
          }else{
          	/**
          	 * 增加筛选条件：仓位 By CMJ
          	 */
          	for(var i=0;i<DestoutObj.tbl_cnt;i++){
          		var oary = DestoutObj.tbl_cnt>1 ? DestoutObj.oary[i] : DestoutObj.oary;
          		if(i==0){
          			dest_shop = "'"+oary.data_id+"'";
          		}else{
          			dest_shop +=",'"+oary.data_id+"'";
          		}
          	}
          	iary.dest_shop = dest_shop;        	  
          }
          
          if(vdr_id){
              iary.vdr_id = vdr_id;
          }
          if(mtrl_type){
              iary.mtrl_type = mtrl_type;
          }
          if(wo_id){
              iary.wo_id = wo_id;
          }

          var inTrxObj;
          inTrxObj = {
              trx_id: T_XPWHSOPE,
              action_flg: 'Q',
              box_stat: 'WAIT',
              iary: iary
          };
          var outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if(outTrxObj.rtn_code == _NORMAL) {
				/**
				 * 获取仓位描述
				 */
				for(var i=0;i<outTrxObj.box_cnt;i++){
					var oary = outTrxObj.box_cnt>1 ? outTrxObj.oary[i] : outTrxObj.oary;
					var desc = DestTrans.getTran("'C','D'",oary.dest_shop);
					oary.dest_shop = desc==undefined ? oary.dest_shop : desc;
				}
                setGridInfo(outTrxObj.oary,"#paneluseGrd",true);
          }
      },
      //Query box list ,已分配的为WFRL & 已经释放的RELS
      f2_func : function(){
          "use strict";
          var wo_id,
              vdr_id,
              mtrl_type,
              dest_shop,
              iary = {};

	      var hideAry = ["rcv_user","rcv_timestamp"];
	      var showAry = ["evt_user","evt_timestamp","relate_user"];
	      $("#paneluseGrd").jqGrid("showCol",showAry).jqGrid("hideCol",hideAry);
	      
	      /**
           * 判断用户是否有可以操作的仓位
           */
          if(DestoutObj.tbl_cnt == 0){
          	showErrorDialog("","此用户没有可以操作的仓位！");
              return false;
          }
          
          vdr_id = $.trim(controlsQuery.vdrIDSelect.val());
          mtrl_type = $.trim(baseFnc.returnSpace(controlsQuery.mtrlTypeSelect.val()));
          wo_id = $.trim($("#woSelect").val());
          dest_shop = $.trim(controlsQuery.destShopSelect.val());
          if (!wo_id) {
        	showErrorDialog("","请选择工单！");
            return false;
          }
          if(dest_shop){
              iary.dest_shop = "'"+dest_shop+"'";
          }{
          	/**
          	 * 增加筛选条件：仓位 By CMJ
          	 */
          	for(var i=0;i<DestoutObj.tbl_cnt;i++){
          		var oary = DestoutObj.tbl_cnt>1 ? DestoutObj.oary[i] : DestoutObj.oary;
          		if(i==0){
          			dest_shop = "'"+oary.data_id+"'";
          		}else{
          			dest_shop +=",'"+oary.data_id+"'";
          		}
          	}
          	iary.dest_shop = dest_shop;
          }
          if(vdr_id){
              iary.vdr_id = vdr_id;
          }
          if(mtrl_type){
              iary.mtrl_type = mtrl_type;
          }
          if(wo_id){
              iary.wo_id = wo_id;
          }else{
//              iary.wo_ids = "'" + _woAry.join("','") + "'";
          }

          var inTrxObj = {
              trx_id: T_XPWHSOPE,
              action_flg : 'Q',
              box_stats  : "'WFRL','RELS','EMPT'",
              iary       : iary
          };

          var outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if(outTrxObj.rtn_code == _NORMAL) {
				/**
				 * 获取仓位描述
				 */
				for(var i=0;i<outTrxObj.box_cnt;i++){
					var oary = outTrxObj.box_cnt>1 ? outTrxObj.oary[i] : outTrxObj.oary;
					var desc = DestTrans.getTran("'C','D'",oary.dest_shop);
					oary.dest_shop = desc==undefined ? oary.dest_shop : desc;
				}
				setGridInfo(outTrxObj.oary,"#paneluseGrd",true);
          }
      },
      
      //update ret_prd_in ppbox_stat = "WAIT"
      f5_func : function (){
    	  var panelInfoGrd = $('#paneluseGrd');
    	  var selectRowIds = panelInfoGrd.jqGrid('getGridParam','selarrrow');
    	  var iary=[],
    	  	  inTrx,
    	  	  outTrx,
    	  	  iaryTemp={},
    	  	  updateCnt = 0, wo_id ="";
    	  for(var iRow=0; iRow<selectRowIds.length; iRow++){
        	  var errflg="YES";
              var selectRow = panelInfoGrd.jqGrid("getRowData", selectRowIds[iRow]);
              if(DestoutObj.tbl_cnt>1){
                  for(var d=0;d<DestoutObj.tbl_cnt;d++){
                	  if(DestoutObj.oary[d].data_id!=selectRow['dest_shop_dsc']){
                            continue;
                	  }else{
                		    errflg="NO";
                	  }
                  }
                  if(errflg=="YES"){
                	    showErrorDialog("","您没有操作箱子所属仓位的权限！");
                        return false;
                  }
              }else{
            	  if(DestoutObj.oary.data_id!=selectRow['dest_shop_dsc']){
                      showErrorDialog("","您没有操作箱子所属仓位的权限！");
                      return false;
            	  }  
              }
             
              iaryTemp = {
                  box_id : selectRow['box_id'],
                  cus_id  : selectRow['cus_id']
              };
              updateCnt += parseInt(selectRow['count']);
              wo_id = selectRow['wo_id'];
              if("WAIT" !== selectRow['ppbox_stat']){
            	  iary.push(iaryTemp);
            	  iaryTemp={};
              }else {
            	  showErrorDialog("","此功能为已发料退料！");
                  return false;
              }
          }
    
    	  if (selectRowIds.length === 0 ){
    		  showErrorDialog("","请选择一行数据！");
              return false;
    	  }
    	  inTrx = {
              trx_id     : T_XPWHSOPE,
              action_flg : 'E',
              evt_user   : EVT_USER,
              updateCnt  : parseInt(updateCnt),
              wo_id      : wo_id,
              iary       : iary
          };
    	  outTrx = comTrxSubSendPostJson(inTrx);
          if(outTrx.rtn_code == _NORMAL) {
        	  showSuccessDialog("成功已发料退料！");
              btnFunc.f2_func();
          }
      },
      
      //Un regist box in list
      f4_func : function(){
          var wo_id = $.trim($("#woSelect").val());
          var panelInfoGrd = $('#paneluseGrd');
          var selecBoxIDs = [];
          var selectRowIds = panelInfoGrd.jqGrid('getGridParam','selarrrow');

          if(!selectRowIds.length){
              showErrorDialog("","请选择要取消的箱号！");
              return false;
          }
          for(var iRow=0; iRow<selectRowIds.length; iRow++){
              var selectRow = panelInfoGrd.jqGrid("getRowData", selectRowIds[iRow]);
              selecBoxIDs[iRow] = {
                  box_id : selectRow['box_id'],
                  wo_id  : wo_id
              };
          }
          var inTrxObj = {
              trx_id     : T_XPWHSOPE,
              action_flg : 'R',
              evt_user   : EVT_USER,
              box_cnt    : selectRowIds.length ,
              iary       : selecBoxIDs
          };
          var outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if(outTrxObj.rtn_code == _NORMAL) {
              btnFunc.f1_func();
          }
      },
      //Regist all box in list
      f8_func : function(){
          var paneluseGrd = $('#paneluseGrd');
          var relate_usr = $.trim($("#useUserInput").val());
          var iary = [],
              selectRowIdsLen,
              selectRow,
              iRow;
          
	      /**
           * 判断用户是否有可以操作的仓位
           */
          if(DestoutObj.tbl_cnt == 0){
          	showErrorDialog("","此用户没有可以操作的仓位！");
              return false;
          }
          
          if( !relate_usr ){showErrorDialog("","请输入领料人员！"); return false;}

          selectRowIds = paneluseGrd.jqGrid('getGridParam','selarrrow');
          selectRowIdsLen = selectRowIds.length;
          if(selectRowIdsLen <= 0){
              showErrorDialog("","请选择要发料的箱号！");
              return false;
          }

          for( iRow = 0; iRow<selectRowIdsLen; iRow++){
        	  var errflg="YES";
              selectRow = paneluseGrd.jqGrid("getRowData", selectRowIds[iRow]);
              if(DestoutObj.tbl_cnt>1){
                  for(var d=0;d<DestoutObj.tbl_cnt;d++){
                	  if(DestoutObj.oary[d].data_id!=selectRow['dest_shop_dsc']){
                            continue;
                	  }else{
                		    errflg="NO";
                	  }
                  }
                  if(errflg=="YES"){
                	    showErrorDialog("","您没有操作箱子所属仓位的权限！");
                        return false;
                  }
              }else{
            	  if(DestoutObj.oary.data_id!=selectRow['dest_shop_dsc']){
                      showErrorDialog("","您没有操作箱子所属仓位的权限！");
                      return false;
            	  }  
              }

              if(selectRow['bind_so_flg'] == 'Y'){
                  iary[iRow] = {
                          wo_id : selectRow['wo_id'],
                          box_id : selectRow['box_id']
                      };
              }
          }

          if(iary.length <= 0){
              showErrorDialog("","请选择要发料的箱号！");
              return false;
          }
          var inTrxObj = {
              trx_id     : T_XPWHSOPE,
              action_flg : 'B',
              evt_user   : EVT_USER,//发料人员改为登陆人员
              relate_usr : relate_usr,
              box_cnt    : iary.length,
              iary       : iary
          };
          //WAIT -> WFRL
          var outTrxObj = comTrxSubSendPostJson(inTrxObj);
          if(outTrxObj.rtn_code == _NORMAL){
              btnFunc.f1_func();
              clearInput();
              setWoInfo($('#woSelect').val(),"");
          }
      },
      ent_fnc : function(){
		  var box_id = $.trim($("#rawBoxIDInput").val());
		  if( !box_id ){showErrorDialog("","请输入发料箱号！"); return false;}
	      var inTrxObj = {
                trx_id: T_XPWHSOPE,
                action_flg : 'Q',
                iary :{
	    	       box_id : box_id
                }
            };
            var outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if(outTrxObj.rtn_code == _NORMAL && outTrxObj.box_cnt != "0") {
                var hasFind = _findRowInGrid('paneluseDiv', $("#paneluseGrd"), 'box_id', box_id);
                if(!hasFind.has){
                	if(outTrxObj.oary.ppbox_id != "WAIT"){
                		showErrorDialog("","在下列表格中无法找到已发料箱号["+box_id+"],请确认！"); return false;
                	}else{
                		showErrorDialog("","在下列表格中无法找到未发料箱号["+box_id+"],请确认！"); return false;
                	} 	
                }
            }else{
            	showErrorDialog("","箱号["+box_id+"]未入库登记,请确认！"); return false;
            }
      }
  };

  /**
   * All button action
   */
  var iniButtonAction = function(){
    btnQuery.f1.click(function(){
        btnFunc.f1_func();
    });
    btnQuery.f2.click(function(){
        btnFunc.f2_func();
    });
    btnQuery.f4.click(function(){
        btnFunc.f4_func();
    });
    btnQuery.f8.click(function(){
        btnFunc.f8_func();
    });
    btnQuery.f5.click(function(){
    	btnFunc.f5_func();
    });
  };

  /**
   * Ini data bind
   */
  var iniDataFunc = function(){
      setWoIdList();
  };

    var shortCutKeyBind = function(){
        document.onkeydown = function(event) {
            if(event.keyCode == F1_KEY){
                btnQuery.f1.click();
                return false;
            }else if(event.keyCode == F2_KEY){
                btnQuery.f2.click();
                return false;
            }else if (event.keyCode == F3_KEY) {
                btnQuery.f8.click();
                return false;
            }else if (event.keyCode == F5_KEY) {
                btnQuery.f5.click();
                return false;
            }

            return true;
        };
    };
    /**
     * Other action bind
     */
    var otherActionBind = function(){
        //Auto query when press enter after keyin box ID
    	$("#rawBoxIDInput").keydown(function(event){
            if(event.keyCode == ENT_KEY){
                btnFunc.ent_fnc();
            }
        });

        //Bind select wo id action
        $('#woSelect').change(function(){
            var wo_id = $('#woSelect').val();
            setWoInfo(wo_id,"");
        });

        //Set mtrl
        controlsQuery.vdrIDSelect.change(function(){
            setMtrlTypeSelect();
            iniWoIdSelect();
        });
        
        controlsQuery.mtrlTypeSelect.change(function(){
            
            iniWoIdSelect();
        });
    };
  /**
   * Ini view and ...
   */
  var initializationFunc = function(){
      clearInput();
      iniGridInfo();
      resetJqgrid();
      iniButtonAction();
      otherActionBind();

      setVdrIDSelect();
      setMtrlTypeSelect();
      setDestShopSel();
      DestTrans.addTrans("'C','D'");
      shortCutKeyBind();

      //Wo info
      iniWoIdSelect();
      

  };

  initializationFunc();
  //iniDataFunc();

  /**
   * Reset size of jqgrid when window resize
   * @param  {[type]} )
   * @param  {[type]} this);
   * @return {[type]}
   */
//  $(window).resize(function(){
//    resetJqgrid();
//  });

  //Stop from auto commit
  $("form").submit(function(e){
      return false;
  });
  function resizeFnc(){                                                                      
  	var offsetBottom, divWidth;                                                              

  	divWidth = domObj.$paneluseDiv.width();                                   
	offsetBottom = domObj.$window.height() - domObj.$paneluseDiv.offset().top;
	domObj.$paneluseDiv.height(offsetBottom * 0.95);                          
	domObj.$paneluseGrd.setGridWidth(divWidth * 0.99);                   
	domObj.$paneluseGrd.setGridHeight(offsetBottom * 0.99 - 51);               
  };                                                                                         
  resizeFnc();                                                                               
  domObj.$window.resize(function() {                                                         
  	resizeFnc();                                                                             
  });  
  
  $("#woInfoForm input").attr(VAL.DISABLE_ATTR);
  $("#woIdSp").attr(VAL.DISABLE_ATTR);
});