$(document).ready(function() {
	var DestoutObj="";
    var VAL ={
        NORMAL     : "0000000"  ,
        EVT_USER   : $("#userId").text(),
        T_XPAPLYWO : "XPAPLYWO",
        T_XPWMSOPE : "XPWMSOPE",
        T_XPINQCOD : "XPINQCOD",
        T_XPINQBOX : "XPINQBOX"
    };
    var domObj = {
			$window : $(window),
			$ppboxInfoGrdDiv : $("#ppboxInfoGrdDiv"),
			$ppboxInfoGrd : $("#ppboxInfoGrd")
	};
    
    /**
     * All controls's jquery object/text
     * @type {Object}
     */
    var controlsQuery = {
        W                  : $(window)               ,
        vdrIDSelect        : $("#vdrIDSelect")       ,
        mdlIdSel           : $("#mdlIdSel")          ,
        woIdSelect         : $("#woIdSelect")        ,
        ppBoxIDInput   	   : $("#ppBoxIDInput")      ,
        shipboxInput   	   : $("#shipboxInput")      ,
        fromDestShopSelect : $("#fromDestShopSelect"),
        destShopSelect     : $("#destShopSelect")    ,
        destShopCate       : $("#destShopCate")      ,
        mainGrd   :{
            grdId     : $("#ppboxInfoGrd")   ,
            grdPgText : "#ppboxInfoPg"       ,
            fatherDiv : $("#ppboxInfoGrdDiv")
        }
    };

    /**
     * All button's jquery object
     * @type {Object}
     */
    var btnQuery = {
    	f1 : $("#f1_btn"),
        f2 : $('#f2_btn'),
        f4 : $('#f4_btn'),
        f5 : $('#f5_btn'),
        f8 : $('#f8_btn'),
        f9 : $('#f9_btn'),
        f10: $('#f10_btn')
    };
    /**
     * All tool functions
     * @type {Object}
     */
    var toolFunc = {
        resetJqgrid :function(){
            controlsQuery.W.unbind("onresize");
            controlsQuery.mainGrd.grdId.setGridWidth(controlsQuery.mainGrd.fatherDiv.width()*0.80);
            controlsQuery.W.bind("onresize", this);
        },
        clearInput : function(){
            controlsQuery.ppBoxIDInput.val("");
            controlsQuery.shipboxInput.val("");
        },
        intDestShopCate   : function(){
        	SelectDom.initWithSpace(controlsQuery.destShopCate);
        	SelectDom.addSelect(controlsQuery.destShopCate,"F","成品仓");
        	SelectDom.addSelect(controlsQuery.destShopCate,"E","半成品仓");
        },
        destShopCateSelChange:function(){
        	var inObj,outObj,destShopCate,inObjf,outObjf;
        	controlsQuery.fromDestShopSelect.empty();
        	controlsQuery.destShopSelect.empty();
        	destShopCate = controlsQuery.destShopCate.val();
        	inObj = {
        		trx_id : "XPLSTDAT",
        		action_flg : "F",
        		iary   :{
			  		data_cate : 'DEST',
		    		data_ext  : "'"+ destShopCate +"'",
		    		user_id   : VAL.EVT_USER
        		}
        	};
        	outObj = comTrxSubSendPostJson(inObj);
        	if(outObj.rtn_code === VAL.NORMAL){
        		SelectDom.addSelectArr(controlsQuery.fromDestShopSelect,outObj.oary,"data_id","data_desc");
            	btnFunc.f1_func();
        		//SelectDom.addSelectArr(controlsQuery.destShopSelect,outObj.oary,"data_id","data_desc");
        	}
        	inObjf = {
            		trx_id : "XPLSTDAT",
            		action_flg : "S",
            		iary   :{
    			  		data_cate : 'DEST',
    		    		data_ext  : "'"+ destShopCate +"'",
    		    		user_id   : VAL.EVT_USER
            		}
            	};
            	outObjf = comTrxSubSendPostJson(inObjf);
            	if(outObjf.rtn_code === VAL.NORMAL){
            		SelectDom.addSelectArr(controlsQuery.destShopSelect,outObjf.oary,"data_id","data_desc");
            	}
        },
        setVdrIDSelect : function(){
            var inTrxObj,
                outTrxObj;

            inTrxObj = {
                trx_id      : 'XPLSTDAT' ,
                action_flg  : 'Q'        ,
                iary        : {
                    data_cate : "CUSD"
                }
            };
            outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if( outTrxObj.rtn_code == VAL.NORMAL ) {
            	if(1 < parseInt(outTrxObj.tbl_cnt, 10)){
                    outTrxObj.oary.sort(function(x, y){
                        if(x.data_item < y.data_item){
                            return -1;
                        }else if(x.data_item > y.data_item){
                            return 1;
                        }else{
                            return 0;
                        }
                    });
                }
                _setSelectDate(outTrxObj.tbl_cnt, outTrxObj.oary, "data_item", "data_item", "#vdrIDSelect", false);
            }
        },
        initMdlIdSel : function(cusId) {
			var inObj, outObj, tbl_cnt, i ,oary;
	        var crSelect = controlsQuery.mdlIdSel;
            crSelect.empty();
            crSelect.append('<option ></option>');
			//SelectDom.initWithSpace($.trim(baseFnc.returnSpace(controlsQuery.mdlIdSel.val())));
			inObj = {
				trx_id : "XPBMDLDF",
				action_flg : "C",
				iary : {}
			};
			if(typeof(cusId)!=="undefined"){
				inObj.iary.cus_id = cusId;
			}
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
        		//_setSelectDate(outObj.tbl_cnt,outObj.oary,"mdl_id","mdl_id",controlsQuery.mdlIdSel,true);
	            tbl_cnt = parseInt( outObj.tbl_cnt, 10);
	            if( tbl_cnt == 1 ){
	            	mtrlProdId = outObj.oary['mdl_id'];
	                crSelect.append('<option value = '+ baseFnc.replaceSpace(mtrlProdId)  +' >'+ mtrlProdId +'</option>');
	            }else if(tbl_cnt>1){
	                for(i=0;i<tbl_cnt;i++){
	                	mtrlProdId = outObj.oary[i]['mdl_id'];
	                	crSelect.append('<option value = '+ baseFnc.replaceSpace(mtrlProdId)  +'>'+ mtrlProdId +'</option>');
	                }
	            }
	        }
			crSelect.select2({
    	    	theme : "bootstrap"
    	    });
		},
		cusSelChange:function(){
			var cusId = controlsQuery.vdrIDSelect.val();
			toolFunc.initMdlIdSel(cusId);
			toolFunc.iniWoIdSelect(cusId);
		},
		initWoSel : function(mdlId) {
			var inObj, outObj;
			SelectDom.initWithSpace(controlsQuery.woIdSelect);
			inObj = {
				trx_id     : "XPAPLYWO",
				action_flg : "Q",
				iary       : {
					dps_flg : "Y"
				}
			};
			if(typeof(mdlId)!=="undefined"){
				inObj.iary.mdl_id = mdlId;
			}
			outObj = comTrxSubSendPostJson(inObj);
			if (outObj.rtn_code === VAL.NORMAL) {
				SelectDom.addSelectArr(controlsQuery.woIdSelect, outObj.oary,"wo_id");
			}
		},
		mdlIdSelChange:function(){
			var mdlId = $.trim(baseFnc.returnSpace(controlsQuery.mdlIdSel.val()));
//			if(mdlId){
//				mdlId = baseFnc.returnSpace(mdlId);
//			}
			toolFunc.initWoSel(mdlId);
		},
        /**
         * 筛选出用户可以操作的仓位 By CMJ
         */
        iniWoIdSelect : function(cusId){
            var inObj,
                outObj;
            inObj = {
                trx_id      : VAL.T_XPAPLYWO,
                action_flg  : 'Q'           ,
                iary : {
                    wo_stats : "'WAIT','CLOS'",
                    cus_id   :  cusId
                }
            };

            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woIdSelect", true);
            }
        },
        woIdSelChange:function(){
        	var inObj,outObj,woId,destCate,destShop,oary,i,boxCnt;
        	woId = controlsQuery.woIdSelect.val();
        	destCate = controlsQuery.destShopCate.val();
        	destShop = controlsQuery.fromDestShopSelect.val();
//        	destShop = controlsQuery.destShopSelect.val();
			
//        	if(!destCate){
//        		showErrorDialog("","请选择仓别种类");
//        		return false;
//        	}
			
        	inObj = {
        		trx_id     : "XPINQBOX",
        		action_flg : "C",
        		box_stat   : "SHIP"
        	};
        	if(woId){
        		inObj.wo_id  = woId;
        	}
        	if(destShop){
        		inObj.dest_shop = destShop;
        	}
        	outObj = comTrxSubSendPostJson(inObj);
        	if(outObj.rtn_code === VAL.NORMAL){
        		boxCnt = outObj.box_cnt;
        		for(i=0;i<boxCnt;i++){
        			oary = boxCnt >1 ?  outObj.table[i] : outObj.table;
        			var desc = DestTrans.getTran("'"+destCate+"'",oary.dest_shop);
	                oary.dest_shop = desc==undefined ? oary.dest_shop : desc;
	                oary.box_stat = "入库";
	                
//		    		controlsQuery.mainGrd.grdId.jqGrid('addRowData', $.jgrid.randId(), {
//	                    box_id : box_id,
//	                    batch_no_fk : outObj.lot_id_fk,
//	                    prd_qty : outObj.prd_qty,
//	                    mdl_id_fk : outObj.mdl_id_fk,
//	                    dest_shop : outObj.dest_shop,
//	                    wo_id_fk : outObj.wo_id_fk,
//	                    so_id_fk : outObj.so_id_fk,
//	                    box_stat : outObj.box_stat
//	                });
        		}
        		setGridInfo(outObj.table,"#ppboxInfoGrd");
        	}
        }
    };

    /**
     * All button click function
     * @type {Object}
     */
    var btnFunc = {
    	f1_func : function(){
    		var inObj,outObj,woId,destShop,oary,i,boxCnt,j,desc;
	        
        	destShop = controlsQuery.fromDestShopSelect.val();
        	woId = controlsQuery.woIdSelect.val();
			
        	if(!woId){
        		showErrorDialog("","请选择内部订单");
        		return false;
        	}
			
        	inObj = {
        		trx_id     : "XPINQBOX",
        		action_flg : "F",
        		box_stat   : "SHIP"
        	};
        	if(woId){
        		inObj.wo_id  = woId;
        	}
        	if(destShop){
        		inObj.dest_shop = destShop;
        	}
        	outObj = comTrxSubSendPostJson(inObj);
        	if(outObj.rtn_code === VAL.NORMAL){
        		boxCnt = outObj.box_cnt;
        		for(i=0;i<boxCnt;i++){
        			oary = boxCnt >1 ?  outObj.table[i] : outObj.table;
    	    		desc = DestTrans.getTran("'F'",oary.dest_shop);
    	    		if(!desc){
    	    			desc = DestTrans.getTran("'E'",oary.dest_shop);
    	    		}
	                oary.dest_shop = desc==undefined ? oary.dest_shop : desc;
	                oary.box_stat = "已入库";
        		}
        		setGridInfo(outObj.table,"#ppboxInfoGrd");
        	}
    	},
    	f10_func : function(){
    		var inObj,outObj,woId,i;
        	woId = controlsQuery.woIdSelect.val();
        	if(!woId){
        		showErrorDialog("","请选择内部订单");
        		return false;
        	}
            inObj = {
        		trx_id     : "XPINQBOX",
        		action_flg : "C",
        		box_stat   : "SHIP",
        		evt_cate   : "WHCX"
        	};
        	if(woId){
        		inObj.wo_id  = woId;
        	}

        	outObj = comTrxSubSendPostJson(inObj);
        	if(outObj.rtn_code === VAL.NORMAL){
        		boxCnt = outObj.box_cnt;
        		for(i=0;i<boxCnt;i++){
        			oary = boxCnt >1 ?  outObj.table[i] : outObj.table;
        			oary.box_def1=oary.box_def;
    	    	    var desc = DestTrans.getTran("'F'",oary.box_def);
    	    	    if(!desc){
    	    			 desc = DestTrans.getTran("'E'",oary.box_def);
    	    		}
	                oary.box_def = desc==undefined ? oary.box_def : desc;
	                
        	
    	    	    var desc2 = DestTrans.getTran("'F'",oary.pv_prty);
    	    	    if(!desc2){
    	    			 desc2 = DestTrans.getTran("'E'",oary.pv_prty);
    	    		}
	                oary.pv_prty = desc2==undefined ? oary.pv_prty : desc2;
	                oary.box_stat = "转仓中";
	                oary.dest_shop="虚拟仓库";
        		}
        		setGridInfo(outObj.table,"#ppboxInfoGrd");
        	}
    	},
        //Clear
        f2_func : function(){
            controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
        },
        //Delete
        f4_func : function(){
            var ids = controlsQuery.mainGrd.grdId.jqGrid("getGridParam","selarrrow"); //删除选中的出库单
            for ( var i = 0, j = ids.length; i < j; i++) {
                controlsQuery.mainGrd.grdId.jqGrid('delRowData', ids[0]);
            }
        },
        //ADD
        f5_func : function(){
	        var	 boxId = controlsQuery.ppBoxIDInput.val();
        	if(!_findRowInGrid('ppboxInfoGrdDiv', controlsQuery.mainGrd.grdId, 'box_id', boxId)){
        		showErrorDialog("","查询的箱子在列表中不存在,请确认");
        		return false;
        	}
//            var box_id;
//
//            box_id = $.trim( controlsQuery.ppBoxIDInput.val() );
//            if(!box_id){
//                showErrorDialog("","请输入箱号");
//                return false;
//            }
//
//            var rowIds = controlsQuery.mainGrd.grdId.jqGrid('getDataIDs');
//            if(rowIds){
//                for(var i = 0, j = rowIds.length; i < j; i++) {
//                    var curRowData = controlsQuery.mainGrd.grdId.jqGrid('getRowData', rowIds[i]);
//                    if(curRowData['box_id'] == box_id){
//                        showErrorDialog("",box_id+"已经存在！");
//                        return false;
//                    }
//                }
//            }
//
//            inObj = {
//                trx_id      : VAL.T_XPINQBOX,
//                action_flg  : 'Q'           ,
//                box_id      : box_id
//            };
//            outObj = comTrxSubSendPostJson(inObj);
//            if(outObj.rtn_code == VAL.NORMAL){
//                if( "SHIP" != outObj.box_stat ){
//                    showErrorDialog("","箱子状态必须为SHIP!");
//                    return false;
//                }
//                /**
//                 * 检查此用户是否有权限操作这个箱子 By CMJ
//                 */
//                for(var i=0;i<DestoutObj.tbl_cnt;i++){
//                	 var oary = DestoutObj.tbl_cnt>1 ? DestoutObj.oary[i] : DestoutObj.oary;
//    		    	if($.trim(outObj.dest_shop) == $.trim(oary.data_id)){
//    		    		/**
//    	            	 * 获取仓位描述
//    	            	 * @author CMJ
//    	            	 */
//    	                var desc = DestTrans.getTran("'F'",outObj.dest_shop);
//    	                outObj.dest_shop = desc==undefined ? outObj.dest_shop : desc;
//    		    		controlsQuery.mainGrd.grdId.jqGrid('addRowData', $.jgrid.randId(), {
//    	                    box_id : box_id,
//    	                    batch_no_fk : outObj.lot_id_fk,
//    	                    prd_qty : outObj.prd_qty,
//    	                    mdl_id_fk : outObj.mdl_id_fk,
//    	                    dest_shop : outObj.dest_shop,
//    	                    wo_id_fk : outObj.wo_id_fk,
//    	                    so_id_fk : outObj.so_id_fk,
//    	                    box_stat : outObj.box_stat
//    	                });
//    		    		break;
//    		    	}
//    		    }
//    		    if(DestoutObj.tbl_cnt ==0 || i>=DestoutObj.tbl_cnt){
//    		    	showErrorDialog("", "此用户没有权限操作仓位为["+outObj.dest_shop+"]的箱子！");
//    			    return false; 
//    		    }
//                toolFunc.clearInput();
//            }
        },
        f8_func : function(){
            var inObj,
                outObj,
                fr_dest_shop,
                dest_shop,
                iary = [],
                ids,
                box_cnt,
                rowData,
                i;
            fr_dest_shop = controlsQuery.fromDestShopSelect.val();
            dest_shop = controlsQuery.destShopSelect.val();

            if(!fr_dest_shop){
            	showErrorDialog("", "请选择当前仓别");
                return false;
            }
            if(!dest_shop){
            	showErrorDialog("", "请选择转入仓别");
                return false;
            }
            ids = controlsQuery.mainGrd.grdId.jqGrid("getGridParam","selarrrow");
            box_cnt = ids.length;
            if( box_cnt === 0 ){
                showErrorDialog("","请勾选需要转仓的箱号。");
                return false;
            }
            for( i = 0; i < box_cnt; i++ ){
                rowData = controlsQuery.mainGrd.grdId.jqGrid('getRowData', ids[i]);
                iary.push({
                    box_id : rowData['box_id']
                });
            }

            inObj = {
                trx_id       : VAL.T_XPWMSOPE,
                action_flg   : 'X'           ,
                evt_user     : VAL.EVT_USER,
                fr_dest_shop : fr_dest_shop,
                dest_shop    : dest_shop,
                box_cnt      : ids.length,
                iary         : iary
            };
            outObj = comTrxSubSendPostJson(inObj);
            if(outObj.rtn_code == VAL.NORMAL){
                showSuccessDialog("转出成功！");
                controlsQuery.destShopSelect.empty();
            }
        },
        f9_func : function(){
            var inObj,
                outObj,
                dest_shop,
                iary = [],
                iary1 = [],
                ids,
                box_cnt,
                rowData,
                i,j;

            //权限控制
            var x=0;
        	var data_ext = "'F','E'";
        	var iary1={
        		data_cate : 'DEST',
        		data_ext  : data_ext,
        		user_id   : VAL.EVT_USER
        	};
        	var inObjq = {
			   trx_id     : 'XPLSTDAT' ,
	           action_flg : 'F'        ,
	           iary       : iary1
        	};
        	var outObjq = comTrxSubSendPostJson(inObjq);
        	if(outObjq.rtn_code != VAL.NORMAL){
        	       return false;	
        	}

            ids = controlsQuery.mainGrd.grdId.jqGrid("getGridParam","selarrrow");
            box_cnt = ids.length;
            if( box_cnt === 0 ){
                showErrorDialog("","请勾选需要转仓的箱号。");
                return false;
            }
            for( i = 0; i < box_cnt; i++ ){
                rowData = controlsQuery.mainGrd.grdId.jqGrid('getRowData', ids[i]);
                if(outObjq.tbl_cnt==0){
                    showErrorDialog("","没有操作此仓库权限！");
                    return false;
                }else if(outObjq.tbl_cnt==1){
                	if(rowData.box_def1==outObjq.oary.data_id){
                		x=1;
                	}
                }else if(outObjq.tbl_cnt>1){
                    for(j =0;j<outObjq.tbl_cnt;j++){
                        if(rowData.box_def1==outObjq.oary[j].data_id){
                     		x=1;
                     		break;
                     	}
                     }
                }

                if(x==0){
                    showErrorDialog("","没有操作此仓库权限！");
                    return false;
                }
                iary.push({
                    box_id : rowData.box_id
                });
            }

            inObj = {
                trx_id      : VAL.T_XPWMSOPE,
                action_flg  : 'Y'           ,
                evt_user    : VAL.EVT_USER,
                box_cnt : ids.length,
                iary : iary
            };
            outObj = comTrxSubSendPostJson(inObj);
            if(outObj.rtn_code == VAL.NORMAL){
                showSuccessDialog("收入成功！");
            }
        }
    };
    /**
     * base function 
     * 
     */
    var baseFnc = {
		replaceSpace:function(str){
			if(!str){
				return str;
			}
			return str.replace(/[ ]/g,"@");	
		},
		returnSpace:function(str){
			if(!str){
				return str;
			}
			return str.replace(/[@]/g," ");
		}
	};
    /**
     * Bind button click action
     */
    var iniButtonAction = function(){
    	btnQuery.f1.click(btnFunc.f1_func);
        btnQuery.f2.click(function(){
            btnFunc.f2_func();
        });
        btnQuery.f4.click(function(){
            btnFunc.f4_func();
        });
        btnQuery.f5.click(function(event){
            btnFunc.f5_func(event);
        });
        btnQuery.f8.click(function(){
            btnFunc.f8_func();
        });
        btnQuery.f9.click(function(){
            btnFunc.f9_func();
        });
        btnQuery.f10.click(function(){
            btnFunc.f10_func();
        });
    };

    /**
     * grid  initialization
     */
    var iniGridInfo = function(){
        var infoCM = [
            {name: 'box_id',        index: 'box_id',      label: CRR_ID_TAG       , width: BOX_ID_CLM},
            {name: 'ship_box_id',   index: 'ship_box_id',   label: "出货箱号"   , width: BOX_ID_CLM},
            {name: 'lot_id_fk'    , index: 'lot_id_fk',   label: LOT_ID_TAG       , width: LOT_ID_CLM},
            {name: 'prd_qty',       index: 'prd_qty',     label: TOTAL_QTY_TAG    , width: QTY_CLM },
            {name: 'box_weight',    index: 'box_weight',  label: WEIGHT_TAG    ,    width: QTY_CLM },
            {name: 'mdl_id_fk',     index: 'mdl_id_fk',   label: MDL_ID_TAG       , width: MDL_ID_CLM},
            {name: 'yield',         index: 'yield',       label: YIELD_TAG        , width: QTY_CLM ,hidden:true},
            {name: 'dest_shop',     index: 'dest_shop',   label: DEST_SHOP_TAG    , width: DEST_SHOP_CLM },
            {name: 'box_def',       index: 'box_def',     label: "目标仓位"  ,         width: DEST_SHOP_CLM },
            {name: 'pv_prty',       index: 'pv_prty',     label: "原仓位"  ,           width: DEST_SHOP_CLM },
            {name: 'nx_ope_id_fk',  index: 'nx_ope_id_fk',label: "站点"  ,            width: DEST_SHOP_CLM ,hidden:true},
            {name: 'box_def1',      index: 'box_def1',    label: "目标仓位"  ,         width: DEST_SHOP_CLM ,hidden:true},
            {name: 'wo_id_fk',      index: 'wo_id_fk',    label: WO_ID_TAG        , width: WO_ID_CLM},
            {name: 'so_id_fk',      index: 'so_id_fk',    label: SO_NO_TAG        , width: WO_ID_CLM},
            {name: 'box_stat'   ,   index: 'box_stat',    label: STATUS_TAG       , width: PRD_STAT_CLM}
        ];
        controlsQuery.mainGrd.grdId.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:360,
            width:620,
            shrinkToFit:false,
            scroll:true,
            rownumWidth : true,
            resizable : true,
            rowNum:40,
            loadonce:true,
            multiselect : true,
            viewrecords:true,
            pager : controlsQuery.mainGrd.grdPgText,
            fixed: true,
            colModel: infoCM
        });
    };

    var shortCutKeyBind = function(){
        document.onkeydown = function(event) {
        	if(event.keyCode == F1_KEY){
                btnQuery.f1.click();
                return false;
            }else if(event.keyCode == F2_KEY){
                btnQuery.f2.click();
                return false;
            }else if(event.keyCode == F3_KEY){
                btnQuery.f4.click();
                return false;
            }else if (event.keyCode == F4_KEY) {
                btnQuery.f5.click();
                return false;
            }else if (event.keyCode == F5_KEY) {
                btnQuery.f8.click();
                return false;
            }

            return true;
        };
    };

    /**
     * Other action bind
     */
    var otherActionBind = function(){
        //Stop from auto commit
        $("form").submit(function(){
            return false;
        });

        // Reset size of jqgrid when window resize
        controlsQuery.W.resize(function(){
            toolFunc.resetJqgrid();
        });

        controlsQuery.ppBoxIDInput.keydown(function(event){
            if(event.keyCode === 13){
                btnQuery.f5.click();
            }
        });
        controlsQuery.shipboxInput.keydown(function(event){
            if(event.keyCode === 13){
    	        var	 ship_box_id = controlsQuery.shipboxInput.val();
                if(!ship_box_id){
                    showErrorDialog("","请输入出货箱号!");
                    return false;
                }
            	var boxinObj = {
                        trx_id      : VAL.T_XPINQBOX,
                        action_flg  : 'G'           ,
                        box_id      : ship_box_id
                   };
           		var boxObj = comTrxSubSendPostJson(boxinObj);
                   if(boxObj.rtn_code != VAL.NORMAL){
               	    return false;
                   }
            	if(!_findRowInGrid('ppboxInfoGrdDiv', controlsQuery.mainGrd.grdId, 'box_id', boxObj.box_id)){
            		showErrorDialog("","查询的箱子在列表中不存在,请确认");
            		return false;
            	}
            }
        });
        controlsQuery.vdrIDSelect.on("change",toolFunc.cusSelChange);
        controlsQuery.mdlIdSel.on("change",toolFunc.mdlIdSelChange);
       // controlsQuery.woIdSelect.on("change",toolFunc.woIdSelChange);
        controlsQuery.destShopCate.on("change",toolFunc.destShopCateSelChange);
        shortCutKeyBind();
    };

    /**
     * Ini view and data
     */
    var initializationFunc = function(){
        iniGridInfo();
        iniButtonAction();
        otherActionBind();
        toolFunc.clearInput();
        toolFunc.resetJqgrid();
        //Ini data
        toolFunc.intDestShopCate();
//        toolFunc.setDestShopSel();
//        toolFunc.setFromD
        
        toolFunc.setVdrIDSelect();
        toolFunc.initMdlIdSel();
        //Wo info
        toolFunc.iniWoIdSelect(controlsQuery.vdrIDSelect.val());
        DestTrans.addTrans("'F'");
        DestTrans.addTrans("'E'");
        btnQuery.f5.hide();
    };
    controlsQuery.fromDestShopSelect.change(function(){
    	btnFunc.f1_func();
    });

    initializationFunc();
    function resizeFnc(){                                                                      
    	var offsetBottom, divWidth;                                                              
		    
		divWidth = domObj.$ppboxInfoGrdDiv.width();                                   
		offsetBottom = domObj.$window.height() - domObj.$ppboxInfoGrdDiv.offset().top;
		domObj.$ppboxInfoGrdDiv.height(offsetBottom * 0.95);                          
		domObj.$ppboxInfoGrd.setGridWidth(divWidth * 0.99);                   
		domObj.$ppboxInfoGrd.setGridHeight(offsetBottom * 0.99 - 51);               
    };                                                                                         
    resizeFnc();                                                                               
    domObj.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	});
});