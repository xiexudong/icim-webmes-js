/**
 * Created with JetBrains WebStorm.
 * User: C1008010
 * Date: 13-5-29
 * Time: 上午10:47
 * To change this template use File | Settings | File Templates.
 */
var login_init = function(){
    var login_button = $('#loginBtn');

    // 登录
    $('#u,#p,#f,#l').unbind('keyup').bind('keyup',function(event){
    	
        if(event.keyCode == 13) {
            login_button.click();
        }
    });

    // 登录按钮
    login_button.unbind('click').bind('click',function(){
        if(!enterlogin())return false;

    });

    var enterlogin = function(){
        if($.trim($('#u').val()) == ''){
            $('#u').focus();
            return false;
        }
        if($.trim($('#p').val()) == ''){
            $('#p').focus();
            return false;
        }
//        if($.trim($('#f').val()) == ''){
//            $('#f').focus();
//            return false;
//        }
//        if($.trim($('#l').val()) == ''){
//            $('#l').focus();
//            return false;
//        }
        return true;
    };
};

function request(paras){
    var url = location.href;
    var paraString = url.substring(url.indexOf("?")+1,url.length).split("&");
    var paraObj = {}
    for (i=0; j=paraString[i]; i++){
        paraObj[j.substring(0,j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=")+1,j.length);
    }
    var returnValue = paraObj[paras.toLowerCase()];
    if(typeof(returnValue)=="undefined"){
        return "";
    }else{
        return returnValue;
    }
}

var parseUrl = function(){
    var userid = request('usr_id');
    if(!userid){
        return true;
    }
    var reason_code = parseInt(request('reason_code'),10);
    $('#u').val(userid);
    $('#u').focus();
    switch(reason_code)
    {
        case 1: // user id not found
        	showErrorDialog("","用户名不存在");
            break;
        case 2: // pwd not match
        	showErrorDialog("","密码错误");
            $('#p').focus();
            break;
        default: // 其他错
        	showErrorDialog("","用户名或密码错误");
            $('#p').val('');
            $('#u').val('');
            $('#u').focus();
            break;
    }
};
$(document).ready(function(){
	login_init();
	parseUrl();
//	$("#loginBtn").click(function(){
//		var inObj,outObj,oary,i,userId,lineId;
//		userId = $("#u").val();
////		lineId= $("#lineIdSel").val();
////    	inObj = {
////    		trx_id : "XPLSTDAT"	,
////    		action_flg : "Q",
////    		iary : {
////    			data_cate : "URLN",
////    			data_ext  : userId,
////    			ext_1     : lineId
////    		}
////    	};
////    	outObj = sendAjax(inObj);
////    	if(outObj.tbl_cnt <= 0){
////    		showErrorDialog("","用户与线别没有绑定关系，无法登陆");
////    		return false;
////    	}
//    	var outObj = findSingleLogin(userId);
//    	if(outObj.isSingleLogin == "N"){
//    		$("#loginForm").showCallBackWarnningDialog({
//    			errMsg : "账号[" + userId + "]已在另一台电脑[" + outObj.loginIp+ "]登录，登录时间是["+ outObj.loginTimestamp+"]，是否注销之前登录?",
//                callbackFn : function(data) {
//    				if(data.result){
//    					$("#loginForm").submit();
//    				}else{
//    					return false;
//    				}
//    			}
//    		});
//    		return false;
//    	}
//    	
//	});
	
	//检查账号是否登录
	function findSingleLogin(user_id){
		var url = "jcom2/findSingleLogin";
		var outObj = null;
		jQuery.ajax({
	        type:"post",
	        url:url,
			data : {
				user_id : user_id
			},
	        timeout:60000, 
	        async:false, 
	        success:function (data) {
				outObj = data;
	        }
		})
		return outObj;
	}
	
    function sendAjax(inTrxObj){
    	var inTrxStr = JSON.stringify(inTrxObj);
    	jQuery.ajax({
            type:"post",
            url:"jcom2/sendMsgNoCheck",
            timeout:60000, 
            data:{
                strInMsg:inTrxStr
            },
            async:false, // false代表等待ajax执行完毕后才执行alert("ajax执行完毕")语句;
            beforeSend:function () {
            },
            complete:function () {// ajaxStop改为ajaxComplete也是一样的
            },
            success:function (data) {
                //check denied
                if(data instanceof Object){
                    outTrxObj =  JSON.parse(data.strOutMsg);
                    var rtn_code = outTrxObj.rtn_code;
                    if (rtn_code != "0000000") {
                        if(showErrDlg &&
                            showErrDlg == "N" ){
                            ;
                        }else{
                            // showErrorDialog(rtn_code, outTrxObj);
                            showErrorDialog(rtn_code,outTrxObj.rtn_mesg);
                            // alert(rtn_code);
                        }
                    }
                }
            },
            error : function(xhr, stat, e){
                console.error(xhr);

            }

        });
    	
    	return outTrxObj;
    };
    
//    function getFabId(){
//    	var inObj,outObj,oary,i;
//    	inObj = {
//    		trx_id : "XPLSTDAT"	,
//    		action_flg : "Q",
//    		iary : {
//    			data_cate : "FBID"
//    		}
//    	};
//    	outObj = sendAjax(inObj);
//    	console.info(outObj);
//    	oary = $.isArray(outObj.oary) ? outObj.oary : [outObj.oary];
//    	
//    	$("#fabIdSel").empty();
//    	SelectDom.addSelectArr($("#fabIdSel"), oary, "data_id", "data_desc",false);
//    	
//    }
//    
//    getFabId();
//    getLineId();
    
    
//    function getLineId(){
//    	var inObj,outObj,oary;
//    	inObj = {
//    		trx_id     :"XPLSTDAT",
//    		action_flg : "Q",
//    		iary       :{
//    			data_cate : "LNID",
//    			data_ext  : $("#fabIdSel").val()
//    		}
//    	};
//    	outObj = sendAjax(inObj);
//    	if(outObj.rtn_code === "0000000"){
//    		oary = $.isArray(outObj.oary) ? outObj.oary : [outObj.oary];
//        	
//        	$("#lineIdSel").empty();
//        	SelectDom.addSelectArr($("#lineIdSel"), oary, "data_id", "data_item",false);
//    	}
//    	
//    }
//    
//    $("#fabIdSel").on("change",getLineId);

});