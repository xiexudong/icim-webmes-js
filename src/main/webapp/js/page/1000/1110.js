$(document).ready(function() { 

  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#WOInforGrd").setGridWidth($("#WOInforDiv").width()*0.95); 
      $("#WOInforGrd").setGridHeight($("#WOInforDiv").height()*0.80);      
      $(window).bind("onresize", this);  
  }

  var initializationFunc = function(){
    resetJqgrid();
    $('#btnBar').sticky({ topSpacing: 50 });
  }

  /**
   * 显示对话框
   * @param  {[type]} myModalHeadText [对话框标题]
   * @param  {[type]} myModalbodyText [对话框内容]
   * @param  {[type]} myModalID       [对话框元素ID，默认为myModal]
   * @return {[type]}                 [null]
   */
  var showMyModal = function(myModalHeadText,myModalbodyText,myModalID){
    $('#myModalHead').text(myModalHeadText);
    $('#myModalbody').text(myModalbodyText);
    var realModal;
    if(myModalID){
      realModal = $('#'+myModalID);
    }else{
      realModal = $('#myModal');
    }
    realModal.modal({
        backdrop:true,
        keyboard:true,
        show:true
    });        
  } 

  var f1_func = function() {
      var inTrxObj = {
        trx_id: 'XPINQWOR',
        action_flg: 'I',
        worder_id: 'test'
      };
      var outTrxObj = comTrxSubSendPostJson(inTrxObj);
      if(outTrxObj.rtn_code == "0000000") {
        setGridInfo(outTrxObj,"#WOInforGrd");
      }
    }

  /**
   * Reset size of jqgrid when window resize
   * @param  {[type]} )
   * @param  {[type]} this);
   * @return {[type]}
   */
  $(window).resize(function(){   
      $(window).unbind("onresize"); 
      $("#WOInforGrd").setGridWidth($("#WOInforDiv").width()*0.95); 
      $("#WOInforGrd").setGridHeight($("#WOInforDiv").height()*0.80);      
      $(window).bind("onresize", this);  
  });

    /**
     * grid  initialization
     */
   var woInfoCM = [
        {name: 'wo_id',          index: 'wo_id',          label: '订单号码',       width: 150},
        {name: 'wo_dsc',         index: 'wo_dsc',         label: '订单说明',       width: 90 },
        {name: 'wo_typ',         index: 'wo_typ',         label: '订单种类',       width: 100},
        {name: 'wo_cate',        index: 'wo_cate',        label: '订单**',         width: 100},
        {name: 'wo_stat',        index: 'wo_stat',        label: '订单状态',       width: 100},
        {name: 'wo_prty',        index: 'wo_prty',        label: '订单优先级',     width: 100},
        {name: 'pln_stb_date',   index: 'pln_stb_date',   label: '预计投产日期',   width: 100},
        {name: 'pln_cmp_date',   index: 'pln_cmp_date',   label: '预计结束日期',   width: 100},
        {name: 'dest_shop',      index: 'dest_shop',      label: '产出仓别',       width: 50 },
        {name: 'evt_user',       index: 'evt_user',       label: '操作人',         width: 100 },
        {name: 'evt_timestamp',  index: 'evt_timestamp',  label: '操作时间',       width: 160}
  ];
  $('#WOInforGrd').jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:200,
      width:800,
      shrinkToFit:false,
      scroll:true,
      rownumWidth : true,
      resizable : true,
      rowNum:40,
      loadonce:true,
      fixed:true,
      viewrecords:true,
      pager : 'WOInforPg',
     fixed: true,
     colModel: woInfoCM  
  }); 

  initializationFunc();


  $('#f1_btn').click(function(){
    f1_func();
  });
 
});