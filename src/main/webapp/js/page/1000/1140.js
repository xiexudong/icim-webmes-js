$(document).ready(function() {
  /****
   * JQgrid根据屏幕分辨率 自适应
   * 以下函数原理：根据父元素的大小自适应
   * ********/  
  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#boxListGrd").setGridWidth($("#boxListDiv").width()*0.95); 
      $("#boxListGrd").setGridHeight($("#boxListDiv").height()*0.80);     
      $("#qTimeListGrd ").setGridWidth($("#qTimeListDiv").width()*0.95); 
      $("#qTimeListGrd ").setGridHeight($("#qTimeListDiv").height()*0.80); 
      $(window).bind("onresize", this);  
  }
  var initializationFunc = function(){
    resetJqgrid();
    //TODO:
    $("#toolPortIDSel").empty();
    for(var i=1;i<=3;i++){
      $("#toolPortIDSel").append("<option value=>" + i + "</option>");
    }
    $("#toolPortIDSel").select2({
    	theme : "bootstrap"
    });
  }
 
  /**
   * Reset size of jqgrid when window resize
   * @param  {[type]} )
   * @param  {[type]} this);
   * @return {[type]}
   */
  $(window).resize(function(){   
     resetJqgrid();
  });


  initializationFunc();
  var boxListCM = [
                  {name: 'box_id'  ,        index: 'box_id'  ,        label: BOX_ID_TAG,     width: 150},
                  {name: 'batch_no',        index: 'batch_no',        label: BATCH_NO_TAG,   width: 80 },
                  {name: 'qty'     ,        index: 'qty'     ,        label: QTY_TAG,        width: 50},
                  {name: 'mdl_id'  ,        index: 'mdl_id'  ,        label: MDL_ID_TAG,     width: 150},
                  {name: 'path_id' ,        index: 'path_id' ,        label: PATH_ID_TAG,    width: 80 },
                  {name: 'ope_id'  ,        index: 'ope_id'  ,        label: OPE_ID_TAG,     width: 80},
                  {name: 'wo_id'   ,        index: 'wo_id'   ,        label: WO_ID_TAG,      width: 150},
            ];
  $("#boxListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      // height:"100%",
      // width:"99%",
      autowidth:true,//宽度根据父元素自适应
      shrinkToFit:true,
      scroll:true,
      rownumbers  :true ,
      rownumWidth : 20,
      resizable : true,
      rowNum:40,
      loadonce:true,
      fixed:true,
      viewrecords:true,
      pager : 'boxListPg',
      fixed: true,
      colModel: boxListCM ,
      multiselect:true,
      onSelectRow : function(id){
        var rowData = $(this).jqGrid("getRowData",id);
        $("#boxId2Spn").text(rowData.box_id);
        $("#mdlId2Spn").text(rowData.mdl_id);
        $("#woID2Spn").text(rowData.wo_id);
        // $("#prty2Spn").text(rowData.pri) TODO
      } 
  })
    var qTimeCM = [
                  {name: 'mdl_id' ,        index: 'mdl_id',          label: MDL_ID_TAG,     width: 150},
                  {name: 'q_timestamp' ,   index: 'q_timestamp',     label: Q_TIMESTAMP,    width: 100},
                  {name: 'remain_time' ,   index: 'remain_time',     label: REMAIN_TIME,    width: 100},
                  {name: 'path_id',        index: 'path_id',         label: PATH_ID_TAG,    width: 80 },
            ];
  $("#qTimeListGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      // height:"100%",
      // width:"99%",
      autowidth:true,//宽度根据父元素自适应
      shrinkToFit:true,
      scroll:true,
      rownumbers  :true ,
      rownumWidth : 20,
      resizable : true,
      rowNum:40,
      loadonce:true,
      fixed:true,
      viewrecords:true,
      pager : 'qTimeListPg',
      fixed: true,
      colModel: qTimeCM 
  });
  $("#eqptIdSel").click(function(){
    $("#eqptIdSel").showSelectBayToolDialog({
      callbackFn : function(data) {
            $("#eqptIdSel").empty();
            $("#eqptIdSel").append("<option value=>" + data.toolID + "</option>");
            $("#eqptIdSel").select2({
	          	theme : "bootstrap"
	          });
        }
    })
  })
  function f1_func(){
    var inTrxObj = {
          trx_id     : 'XPIWIPEQ',
          action_flg : 'I',
          tool_id    : $("#eqptIdSel").find('option:selected').text(),
          wo_id      : $("#wordIDTxt").val()
        }

    var outTrxObj = comTrxSubSendPostJson(inTrxObj);
    if(outTrxObj.rtn_code == "0000000") {
      setGridInfo(outTrxObj.oary,$("#boxListGrd"));
    }  
  }
  $("#f1_btn").click(f1_func);
});