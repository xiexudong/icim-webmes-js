$(document).ready(function() {
	var label = new Label();
    var VAL ={
        NORMAL        : "0000000"  ,
        EVT_USER      : $("#userId").text(),
        T_XPBISPTH    : "XPBISPTH",
        T_XPTOOLOPE   : "XPTOOLOPE",
        T_XPINQBOX    : "XPINQBOX",
        T_XPLSTEQP    : "XPLSTEQP",
        T_XPIWIPEQ    : "XPIWIPEQ",
        T_XPAPLYWO    : "XPAPLYWO",
        T_XPPNLOUT    : "XPPNLOUT",
        T_XPMOVEIN    : "XPMOVEIN",
        T_XPCNPNLOUT  : "XPCNPNLOUT",
        T_XPSTARTPROC : "XPSTARTPROC",
        T_XPMOVEOUT   : "XPMOVEOUT",
        T_XPBISOPE    : "XPBISOPE",
        T_XPSWITCH    : "XPSWITCH",
        T_XPINQCOD    : "XPINQCOD",
        T_XPDEFECT    : "XPDEFECT",
        T_XPWIPRETAIN : "XPWIPRETAIN",
        T_XPSPCRET    : "XPSPCRET",
        T_XPINQDEF    : "XPINQDEF",
        T_XPSKIPOPE   : "XPSKIPOPE",
        T_XPPNLJGE    : "XPPNLJGE",
        TX_XPBISTOL   : "XPBISTOL",
        SLOT_NO_CUS_ID         : ['018','058','068','088','098'],//需要用户输入slot_no的客户
        SLOT_NO_PROC           : ['DMZQ','PGZQ','JBGH'],//需要用户输入slot_no的站点
        GRADE_SEL_ROCS         : ['JBZJ','DMZJ'],
        CAN_SAVE_BUF_TIME_PROC : ['PGGY'],
        BUF_TIME_JBYZ          : ['JBYZ'],
        SHOW_F6_BTN            : ['PGGY'],
        JBPG_PROC              : "PGGY",
        QA_PROCS               : ['JBQA', 'DMQA'],
        QASK                   : ['JBQA','DMQA'],
        SHOW_YIELD_PROCS       : ['JBZJ', 'JBQA', 'DMIC', 'DMQA', 'DMZJ','PGZQ','DMZQ'],
        WIP_WAIT_VIALD         : ['DMGY','DMIC','DMQA','DMQX','DMZJ','JBQA','JBZJ'],
//        RTN_OPE                : ['JBIC','DJGH','JBQC','JBCL','JBSK','JBCJ','JBYZ','JBPG','JBZJ','JBQA','JBPK','DMIC','DMQX','DMGY','DMZJ','DMQA','DMDB'],
        GRADE_HIDE             : ['JBZJ','JBYZ','DMZJ'],
        WIP_WAIT_MASK          : ['DMGY','DMIC','DMQA','DMQX','DMZJ','JBQA','JBZJ'],
        GROUP_GRADE_CHECK      : ['JBIC','JBZJ','JBQA','JBPK','DMZJ','DMQA','DMDB']
    };

    var domObj = {
			$window : $(window),
			$defectInfoDiv : $("#defectInfoDiv"),
			$defectInfoGrd : $("#defectInfoGrd"),
			$wipInfoDiv : $("#wipInfoDiv"),
			$wipInfoGrd : $("#wipInfoGrd"),
			$retainBoxInfoDiv : $("#retainBoxInfoDiv"),
			$wipRetainInfoGrd : $("#wipRetainInfoGrd"),
			$wipRetainDetailInfoDiv : $("#wipRetainDetailInfoDiv"),
			$wipRetainDetailInfoGrd : $("#wipRetainDetailInfoGrd"),
			$boxInfoDiv : $("#boxInfoDiv"),
			$boxInfoGrd : $("#boxInfoGrd")
	};
    var gIsJBPG = false,
        gCanSaveBufTime = false,
        skipDialog_flg = false,
        gModeCnt = {
            wo_id : null,
            x_cnt : 0,
            y_cnt : 0,
            getModelCnt : function(){
                return parseInt(this.x_cnt, 10) * parseInt(this.y_cnt, 10);
            },
            clear : function(){
                this.wo_id = null;
                this.x_cnt = 0;
                this.y_cnt = 0;
            }
        },
        gOpeMap    = {}, //Save ope info proc_id->ope_dsc
        gDefectMap = {}; //Get from DATA-DFCT
        gSwhOpe    = "";
        gchk_flg   = "";
        gCus_id    = "";

    /**
     * All controls's jquery object/text
     * @type {Object}
     */
    var controlsQuery = {
        W                  : $(window)             ,
        opeSelect          : $("#opeSelect")       ,
        toolSelect         : $("#toolSelect")      ,
        toolSelectLb       : $("#toolSelectLb")    ,
        woIdSelect         : $("#woIdSelect")      ,
        woPlnQtyTxt        : $("#woPlnQtyTxt")     ,
        woRlQtyTxt         : $("#woRlQtyTxt")      ,
        woLevQtyTxt        : $("#woLevQtyTxt")     ,
        ope_eff_flgTxt     : $("#ope_eff_flgTxt")  ,
        moveInBoxTxt       : $("#moveInBoxTxt")  ,
        prdInBoxTxt        : $("#prdInBoxTxt")  ,
        ppboxIdTxt         : $("#ppboxIdTxt")  ,
        from_thicknessTxt  : $("#from_thicknessTxt"),
        to_thicknessTxt    : $("#to_thicknessTxt")  ,
        t_thicknessTxt     : $("#t_thicknessTxt")  ,
        c_thicknessTxt     : $("#c_thicknessTxt")  ,
        lotIdInput         : $("#lotIdInput")      ,
        prdIdInput         : $("#prdIdInput")      ,
        gradeSelect        : $("#gradeSelect")      ,
        okOutChk           : $("#okOutChk")        ,
        okOutCrrTxt        : $("#okOutCrrTxt")     ,
        judgeUserID        : $("#judgeUserID")     ,
        OkBoxMaxCntTxt     : $("#OkBoxMaxCntTxt")  ,
        OkBoxCurCntTxt     : $("#OkBoxCurCntTxt")  ,
        OkBoxModeCntTxt    : $("#OkBoxModeCntTxt") ,
        ngOutChk           : $("#ngOutChk")        ,
        buffInfoDiv       : $("#buffInfoDiv")  ,
        buffToolGrpSelect : $("#buffToolGrpSelect")  ,
        toolGrpDiv        : $("#toolGrpDiv"),
        tftBuffTxt        : $("#tftBuffTxt")  ,
        cfBuffTxt         : $("#cfBuffTxt")  ,
        tftToolSelect     : $("#tftToolSelect")  ,
        cfToolSelect      : $("#cfToolSelect")  ,
        tftToolDiv        : $("#tftToolDiv")  ,
        cfToolDiv         : $("#cfToolDiv")   ,
        gradeSelectDiv    : $("#gradeSelectDiv"),
        gradeChk          : $("#gradeChk"),
        swhOutOpeSelect   : $("#swhOutOpeSelect"),
        swhPathSelect     : $("#swhPathSelect"),
        swhOpeChk         : $("#swhOpeChk"),
        outBoxRadios      : $(".outBoxRadiosClass"),
        rwkIdDiv 		  : $("#rwkIdDiv"),
        defectPrdSeqIDTxt : $("#defectPrdSeqIDTxt"),
        defectCntSumTxt   : $("#defectCntSumTxt"),
        defOpeSelect      : $("#defOpeSelect"),
        yieldInfoDiv   	  : $("#yieldInfoDiv"),
        procDefTxt        : $("#procDefTxt"),
        cusDefTxt         : $("#cusDefTxt"),
        procYieldTxt      : $("#procYieldTxt"),
        wholeYieldTxt     : $("#wholeYieldTxt"),
        remarkTxt         : $("#remarkTxt"),
//        wip_wait_chk      : $("#wip_wait_chk"),
//        wip_wait_div      : $("#wip_wait_div"),
        print1Select      : $("#print1Select"),
        print2Select      : $("#print2Select"),
        panelRemarkTxt    : $("#panelRemarkTxt"),
        mainGrd   :{
            grdId     : $("#wipInfoGrd")   ,
            grdPgText : "#wipInfoPg"       ,
            fatherDiv : $("#wipInfoDiv")   ,
            defectDiv : $("#defectInfoDiv"),
            defectGrd : $("#defectInfoGrd")
        }
    };

    /**
     * All button's jquery object
     * @type {Object}
     */
    var btnQuery = {
        f1 : $("#f1_btn"),
        process_start : $("#process_start_btn"),
        f6 : $("#f6_btn"),
        f8 : $("#f8_btn"),
        save_buff_time_btn : $("#save_buff_time_btn"),
        qa_btn   : $("#qa_btn"),
        okOutBtn : $("#okOutBtn"),
        gradeSelectBtnOK : $("#gradeSelectBtnOK"),
        gradeSelectBtnGK : $("#gradeSelectBtnGK"),
        gradeSelectBtnLZ : $("#gradeSelectBtnLZ"),
        gradeSelectBtnNG : $("#gradeSelectBtnNG"),
        gradeSelectBtnSC : $("#gradeSelectBtnSC"),
        gradeSelectBtnRM : $("#gradeSelectBtnRM"),
        gradeSelectBtnRW : $("#gradeSelectBtnRW"),
        issue_move_out_btn : $("#issue_move_out_btn"),
        f9 : $("#f9_btn"),
        f7 : $("#f7_btn"),
        wip_retain_btn : $("#wip_retain_btn"),
        wip_retain_cancel_btn : $("#wip_retain_cancel_btn")
    };

    /**
     * All tool functions
     * @type {Object}
     */
    var toolFunc = {
        showGRDButton : function(cus_id){
    	    var ope_info, ope_info_ary,proc_id,inTrxObjM,outTrxObjM;

            if(!cus_id){
                console.warn("cus_id [%s] is null!", wo_id);
                return false;
            }

    	    //Check prod => show/hide btn-group
	        ope_info = $.trim(controlsQuery.opeSelect.val());

	        if(!ope_info){
	            return false;
	        }

	        ope_info_ary = ope_info.split("@");
	        proc_id = ope_info_ary[2];
	        if(0 <= $.inArray(proc_id, VAL.GRADE_SEL_ROCS)){
	            controlsQuery.gradeSelectDiv.show();

	            inTrxObjM = {
	                    trx_id     : VAL.T_XPINQCOD,
	                    action_flg : 'I',
	                    data_cate  : 'GDGP',
	                    cus_id     : cus_id
	                };
	            outTrxObjM = comTrxSubSendPostJson(inTrxObjM);
	            if( outTrxObjM.rtn_code === VAL.NORMAL) {
	        		controlsQuery.gradeSelectDiv.empty();
	            	if (outTrxObjM.tbl_cnt == 0){

	            	}else if (outTrxObjM.tbl_cnt == 1){
	            		controlsQuery.gradeSelectDiv.append('<input type="button" id = '+outTrxObjM.oary.data_ext+' value='+outTrxObjM.oary.data_ext+'>');
	        			$('#' + outTrxObjM.oary.data_ext).on('click', function() {
	       				 	btnFunc.set_judge_result_func(this.id);
	        			});
	        		}else {
	            		for (var i = 0; i < outTrxObjM.tbl_cnt; i ++){
	                		controlsQuery.gradeSelectDiv.append('<input type="button" id="'+outTrxObjM.oary[i].data_ext+'" value="'+outTrxObjM.oary[i].data_ext+'">');
	            			$('#' + outTrxObjM.oary[i].data_ext).on('click', function() {
	            				 btnFunc.set_judge_result_func(this.id);
	            			});
	            		}
	            	}
	            }
	        }else{
	        	controlsQuery.gradeSelectDiv.empty();
	            controlsQuery.gradeSelectDiv.hide();
	        }
    	},
       clearInput : function(){
            controlsQuery.woPlnQtyTxt.val("");
            controlsQuery.woRlQtyTxt.val("");
            controlsQuery.woLevQtyTxt.val("");
            controlsQuery.lotIdInput.val("");
            controlsQuery.prdIdInput.val("");
            controlsQuery.moveInBoxTxt.val("");
            controlsQuery.prdInBoxTxt.val("");
            controlsQuery.ppboxIdTxt.val("");
            controlsQuery.okOutCrrTxt.val("");
            controlsQuery.judgeUserID.val("");
            controlsQuery.OkBoxMaxCntTxt.val("");
            controlsQuery.OkBoxCurCntTxt.val("");
            controlsQuery.OkBoxModeCntTxt.val("");
            controlsQuery.tftBuffTxt.val("");
            controlsQuery.cfBuffTxt.val("");
            controlsQuery.defectPrdSeqIDTxt.val("");
            controlsQuery.defectCntSumTxt.val("");
            controlsQuery.swhOpeChk.removeAttr("checked");
            controlsQuery.swhPathSelect.attr("disabled","");
            controlsQuery.OkBoxMaxCntTxt.attr("disabled","");
            controlsQuery.OkBoxCurCntTxt.attr("disabled","");
            controlsQuery.OkBoxModeCntTxt.attr("disabled","");
            controlsQuery.from_thicknessTxt.val("");
            controlsQuery.to_thicknessTxt.val("");
            controlsQuery.t_thicknessTxt.val("");
            controlsQuery.c_thicknessTxt.val("");
            controlsQuery.ope_eff_flgTxt.val("");
            controlsQuery.procDefTxt.val("");
            controlsQuery.cusDefTxt.val("");
            controlsQuery.procYieldTxt.val("");
            controlsQuery.wholeYieldTxt.val("");
            $("#hideDiv").hide();
        },
        getBoxInfo : function(box_id,targetObj){
            var inObj,
                outObj;
            inObj = {
                trx_id      : VAL.T_XPINQBOX,
                action_flg  : 'Q'           ,
                box_id      : box_id
            };
            outObj = comTrxSubSendPostJson(inObj);
            if(outObj.rtn_code == VAL.NORMAL){
                return outObj;
            }else{
            	targetObj.val("");
                return null;
            }
        },
        getShtInfo : function(box_id){
            var inObj,
                outObj;
            inObj = {
                trx_id      : VAL.T_XPINQBOX,
                action_flg  : 'B'           ,
                box_id      : box_id
            };
            outObj = comTrxSubSendPostJson(inObj);
            if(outObj.rtn_code == VAL.NORMAL){
                return outObj.valid_flg;
            }else{
            	targetObj.val("");
                return null;
            }
        },
        setSwhPathDate : function(dataCnt, arr, selVal, selVal2,selVal3,queryObj){
            var i, realCnt;

            queryObj.empty();
            realCnt = parseInt( dataCnt, 10);

            if( realCnt == 1 ){
                 queryObj.append("<option value="+ arr[selVal] + "@" + arr[selVal2]+">"+ arr[selVal3] +"</option>");
            }else if( realCnt > 1 ){
                for( i = 0; i < realCnt; i++ ){
                   queryObj.append("<option value="+ arr[i][selVal] + "@" + arr[i][selVal2] +">"+ arr[i][selVal3] +"</option>");
                }
            }
            queryObj.select2({
    	    	theme : "bootstrap"
    	    });
        },
        setOpeSelectDate : function(dataCnt, arr, selVal, selVal2, selVal3, selTxt, queryObj, firstSpace){
            var i, realCnt;

            queryObj.empty();
            if(firstSpace === true){
                queryObj.append("<option ></option>");
            }

            realCnt = parseInt( dataCnt, 10);

            if( realCnt == 1 ){
                    queryObj.append("<option value="+ arr[selVal] + "@" + arr[selVal2] + "@" + arr[selVal3] + "@" + arr[selTxt] +">"+ arr[selTxt] +"</option>");
            }else if( realCnt > 1 ){
                for( i = 0; i < realCnt; i++ ){
                        queryObj.append("<option value="+ arr[i][selVal] + "@" + arr[i][selVal2] + "@" + arr[i][selVal3] +"@" + arr[i][selTxt] +">"+ arr[i][selTxt] +"</option>");
                }
            }
            queryObj.select2({
    	    	theme : "bootstrap"
    	    });
        },
        /**
         * 根据人员，厂别，站点的关系，筛选出可以使用的站点。By CMJ
         */
        iniOpeSelect : function(){
        	var inObj,
        		outObj,
        		ope_ary = [];
            var user_id = VAL.EVT_USER;
            var iaryB = {
        		user_id     : user_id
            };
            inObj = {
                trx_id      : VAL.T_XPBISOPE,
                action_flg  : 'I',
                iaryB       : iaryB
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                toolFunc.setOpeSelectDate(outObj.tbl_cnt, outObj.oary,
                    "ope_id", "ope_ver", "proc_id", "ope_dsc", controlsQuery.opeSelect);
                //Save ope info into global values
                tbl_cnt = parseInt( outObj.tbl_cnt, 10 );
                if(0 < tbl_cnt){
                    if(1 === tbl_cnt){
                        ope_ary.push(outObj.oary);
                    }else{
                        ope_ary = outObj.oary;
                    }
                    $.each(ope_ary, function( index, value ) {
                        gOpeMap[value.proc_id] = {
                            ope_dsc : value.ope_dsc
                        };
                    });
                }
            }
        },
        iniRwkOpeSelect : function(){
        	var inObj,
	            outObj,
	            tbl_cnt,ope_info,ope_info_ary,ope_id,ope_ver,
	            ope_ary = [];
            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id       = ope_info_ary[0];
            ope_ver      = ope_info_ary[1];

	        inObj = {
	            trx_id      : VAL.T_XPBISOPE,
	            action_flg  : 'L'
	        };
	        outObj = comTrxSubSendPostJson(inObj);
	        if (outObj.rtn_code == VAL.NORMAL) {
                toolFunc.setOpeSelectDate(outObj.tbl_cnt, outObj.oary,
                    "ope_id", "ope_ver", "proc_id", "ope_dsc", controlsQuery.defOpeSelect, true);
	            //Save ope info into global values
	            tbl_cnt = parseInt( outObj.tbl_cnt, 10 );
	            if(0 < tbl_cnt){
	                if(1 === tbl_cnt){
	                    ope_ary.push(outObj.oary);
	                }else{
	                    ope_ary = outObj.oary;
	                }
	                $.each(ope_ary, function( index, value ) {
	                    gOpeMap[value.proc_id] = {
	                        ope_dsc : value.ope_dsc
	                    };
	                });
	            }
	        }

	        inObj = {
		            trx_id      : VAL.T_XPBISOPE,
		            action_flg  : 'Q',
		            iary        : {ope_id  : ope_id,
	                           ope_ver : ope_ver}
	        };
	        outObj = comTrxSubSendPostJson(inObj);
	        if (outObj.rtn_code == VAL.NORMAL) {
//	            controlsQuery.swhOutOpeSelect.append("<option value= '' ></option>");
	            toolFunc.setOpeSelectDate(outObj.tbl_cnt, outObj.oary, "ope_id", "ope_ver", "proc_id", "ope_dsc", controlsQuery.swhOutOpeSelect,true);
	        }
	    },


        /**
         * 根据站点，人员，机台的关系；筛选出可以使用的机台。By CMJ
         * @returns {Boolean}
         */
        iniToolSelect : function(){
            var inObj, outObj, ope_id, ope_ver,
                ope_info, ope_info_ary;

            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];
            ope_ver = ope_info_ary[1];

            if(!ope_id){
                console.error(ope_id);
                return false;
            }

            if(!ope_ver){
                console.error(ope_ver);
                return false;
            }

            inObj = {
                    trx_id      : VAL.T_XPLSTEQP,
                    action_flg  : 'F'           ,
                    ope_id      : ope_id        ,
                    ope_ver     : ope_ver       ,
                    user_id     : VAL.EVT_USER
                };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                _setSelectDate(outObj.tbl_cnt, outObj.oary, "tool_id", "tool_id", "#toolSelect", false);
            }
        },
        iniBuffToolGrpSelect : function(){
            var inObj,
                outObj;
            inObj = {
                trx_id      : VAL.T_XPLSTEQP,
                action_flg  : 'G'
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                _setSelectDate(outObj.tbl_cnt, outObj.oaryBuffG, "root_tool_id", "root_tool_id", "#buffToolGrpSelect", true);
            }
        },
        iniBuffToolSelect : function(){
//            var inObj,
//                outObj,
//                root_tool_id;
//
//            root_tool_id = controlsQuery.buffToolGrpSelect.val();
//            if(!root_tool_id){
//                return false;
//            }
//
//            inObj = {
//                trx_id      : VAL.T_XPLSTEQP,
//                action_flg  : 'B'           ,
//                root_tool_id: root_tool_id
//            };
//            outObj = comTrxSubSendPostJson(inObj);
//            if (outObj.rtn_code == VAL.NORMAL) {
//                _setSelectDate(outObj.tbl_cnt, outObj.oaryBuffT, "tool_id", "tool_id", "#tftToolSelect", false);
//                _setSelectDate(outObj.tbl_cnt, outObj.oaryBuffT, "tool_id", "tool_id", "#cfToolSelect", false);
//            }
        	  var inObj,
              outObj;
          inObj = {
              trx_id      : VAL.T_XPLSTEQP,
              action_flg  : 'G'
          };
          outObj = comTrxSubSendPostJson(inObj);
          if (outObj.rtn_code == VAL.NORMAL) {
              _setSelectDate(outObj.tbl_cnt, outObj.oaryBuffG, "root_tool_id", "root_tool_id", "#tftToolSelect", true);
              _setSelectDate(outObj.tbl_cnt, outObj.oaryBuffG, "root_tool_id", "root_tool_id", "#cfToolSelect", true);
          }
        },
        /**
    	 * 增加：根据站点筛选出可以显示的内部订单。By CMJ
    	 */
        iniWoIdSelect : function(){
            "use strict";
            var inObj,
                outObj,
                ope_info,
                ope_info_ary,
                ope_id,ope_ver;
            /**
             * 站点为空时，没有对应可以操作的内部订单。
             */
            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];
            ope_ver = ope_info_ary[1];
            inObj = {
                trx_id      : VAL.T_XPAPLYWO,
                action_flg  : 'L'           ,
                iary : {
                    wo_stat : "WAIT",
                    ope_id  : ope_id,
                    ope_ver : ope_ver
                }
            };

            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                _setSelectDate(outObj.tbl_cnt, outObj.oary, "wo_id", "wo_id", "#woIdSelect", true);
            }
        },
        enhanceGridByID : function(id){
            var jBtnHtml,
                jBtnId,
                dBtnHtml,
                dBtnId;

            //judge button
            jBtnId = "judgeBtn_" + id;
            jBtnHtml = "<button id=" + jBtnId + ">点击</button>";

            //defect_detail button
            dBtnId = "defectDetailBtn_" + id;
            dBtnHtml = "<button id=" + dBtnId + ">明细</button>";

            //Set grid
            controlsQuery.mainGrd.grdId.jqGrid('setRowData', id, {
                judge: jBtnHtml,
                defect_detail: dBtnHtml
            });

            //Bind action
            $("#" + dBtnId).click(function(){
                toolFunc.showDefectDetail(id);
                return false;
            });
            $("#" + jBtnId).click( toolFunc.showDefectDialog );

        },
        enhanceGridOaryArr : function(oaryArr){
        	var i, oary , oaryCnt;
        	if(!oaryArr){
        		return false;
        	}
        	oaryArr = $.isArray(oaryArr) ? oaryArr : [oaryArr];
        	oaryCnt = oaryArr.length;
        	for(i=0;i<oaryCnt;i++){
        		oary = oaryArr[i];
        		var prd_grade = ($.trim(oary['prd_grade'])) ? $.trim(oary['prd_grade']) : $.trim(oary['pv_prd_grade']);
        		if(0 <= $.inArray($.trim(oary['cus_id']), VAL.SLOT_NO_CUS_ID) &&
            			0 <= $.inArray($.trim(oary['cr_proc']), VAL.SLOT_NO_PROC) &&
            			"INPR" == $.trim(oary['prd_stat']) &&
            			!$.trim(oary['box_id'])){//088客户，非OK 品，终检，已经入账，并且开始制程，未转入箱子
            		oary.mtrl_slot_no_btn = "<button class='slotNoCss'>修改</button>";
        		}else{
        			oary.mtrl_slot_no_btn = "";
        		}

        		if(0 <= $.inArray($.trim(oary['cus_id']), VAL.SLOT_NO_CUS_ID) &&
            			0 <= $.inArray($.trim(oary['cr_proc']), VAL.SLOT_NO_PROC)){
        			oary.mtrl_slot_no_fk = oary.slot_no;
        		}
        		//来料|制程 判定按钮逻辑
        		if(oaryArr[i].prd_stat == "WAIT"){
        		    //WAIT不可点击
                    oary.judge = "<button class='judgeCss' disabled='true'>WAIT不可点击</button>";
                    oary.defect_detail = "<button class='detailCss'>明细</button>";
                }else{
                    oary.judge = "<button class='judgeCss'>点击</button>";
                    oary.defect_detail = "<button class='detailCss'>明细</button>";
                }
        		if(oary.th_judge_flg == "OK"){
        			oary.th_judge_flg = "<button class='thCss'>OK</button>";
        		}else if(oary.th_judge_flg == "NG"){
        			oary.th_judge_flg = "<button class='thCss'>NG</button>";
        		}
        		if(oary.fz_flg == "OK"){
        			oary.fz_flg = "<button class='fzCss'>OK</button>";
        		}else if(oary.fz_flg == "NG"){
        			oary.fz_flg = "<button class='fzCss'>NG</button>";
        		}

    			if (oary.prd_admin_flg != undefined && oary.prd_admin_flg !== "") {
        			oary.qa_flg = oary.prd_admin_flg.substr(4,1) + oary.prd_admin_flg.substr(5,1);
				}

    			oary.remark_detail = "<button class='remarkDtl'>查看</button>";
        	}
        },
        initDelegate : function(){
        	controlsQuery.mainGrd.grdId.on("click","button.slotNoCss",function(){
        		var id =  $(this).closest("tr")[0].id;
        		var rowData = controlsQuery.mainGrd.grdId.jqGrid("getRowData",id);
    	        $(this)._showSimpleInputDialog({
    	        	itemName : "输入产品" + rowData['prd_seq_id'] + "子格位号",
    	            callbackFn : function(data) {
    	        		if(data.keyInResult && isNaN(data.keyInResult)){
                            showErrorDialog("","请输入数字！");
                            return false;
    	        		}
	                    rowData['mtrl_slot_no_fk'] = data.keyInResult;
	                    controlsQuery.mainGrd.grdId.jqGrid('setRowData', id, rowData);
    	                return false;
    	            }
    	        });
        		return false;
        	});
        	controlsQuery.mainGrd.grdId.on("click","button.judgeCss",function(){
        		var rowId =  $(this).closest("tr")[0].id;
        		toolFunc.showDefectDialog(rowId);
        		return false;
        	});
        	controlsQuery.mainGrd.grdId.on("click","button.remarkDtl",function(){
        		var rowId =  $(this).closest("tr")[0].id;
        		var remarkDetailDialog = new RemarkDetailDialog();
        		remarkDetailDialog.showDialogFnc(rowId);
        		var divWidth = $("#remarkDtlDialog").width();
        		$("#remarkDtlDiv").width(divWidth - 80);
        		$("remarkDtlGrd").setGridWidth(divWidth - 85);
        		$("remarkDtlGrd").setGridHeight(350);

        	});
        	controlsQuery.mainGrd.grdId.on("click","button.detailCss",function(){
        		var rowId =  $(this).closest("tr")[0].id;
        		toolFunc.showDefectDetail(rowId);
        		return false;
        	});
        	controlsQuery.mainGrd.grdId.on("click","button.thCss",function (){
        		var rowId =  $(this).closest("tr")[0].id;
        		console.info(rowId);
        		var spcDialog = new SpcDialog();
        		if(!spcDialog.showDialogFnc(rowId,"CDPC")){
        			return false;
        		}

        		var divWidth = $("#spcDialog").width() - $("#dataItemEditDiv").width();
        		//$("#spcDiv").width(divWidth - 80);
        		$("oGrd").setGridWidth(divWidth - 85);
        		return false;
        	});
        	controlsQuery.mainGrd.grdId.on("click","button.fzCss",function (){
        		var rowId =  $(this).closest("tr")[0].id;
        		console.info(rowId);
        		var spcDialog = new SpcDialog();
        		if(!spcDialog.showDialogFnc(rowId,"CZPC")){
        			return false;
        		}

        		var divWidth = $("#spcDialog").width() - $("#dataItemEditDiv").width();
        		//$("#spcDiv").width(divWidth - 80);
        		$("#spcDialog_woGrd").setGridWidth(divWidth - 85);

        		return false;
        	});
        },
        getDefectListByPrdSeqId : function(prd_seq_id, proc_id,jugeflag){
            var inObj,
                outObj,
                def_cnt,
                actionflg,
                defList = [],
                defListDistinctPos = [],
                defListHasPositionStr = '', //defect_code + position
                tmpDefPosStr,
                prd_jge_info = { };

            if(jugeflag == "Y"){
                actionflg = 'q';
            }else{
                actionflg = 'Q';
            }
            inObj = {
                trx_id      : VAL.T_XPDEFECT,
                action_flg  : actionflg           ,
                prd_seq_id  : prd_seq_id
            };

            if(proc_id){
                inObj.proc_id = proc_id;
            }

            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code != VAL.NORMAL) {
            } else {
                def_cnt = parseInt(outObj.def_cnt, 10);
                if (1 === def_cnt) {
                    defList.push(outObj.defList);
                } else if (1 < def_cnt) {
                    defList = outObj.defList;
                }

                defList.sort(function (a, b) {
                    if (a.judge_timestamp > b.judge_timestamp)
                        return -1;
                    if (a.judge_timestamp < b.judge_timestamp)
                        return 1;
                    return 0;
                });
                //Get other property in [DATA-DFCT] => defectAry
                $.each(defList, function (index, value) {
                    var defectMap,
                        opeMap,
                        tmpDefCode = value.defect_code,
                        tmpProcId = value.proc_id;

                    //Get zh defect description by defect code
                    defectMap = gDefectMap;
                    if (defectMap.hasOwnProperty(tmpDefCode)) {
                        value.defDsc = defectMap[tmpDefCode].defDsc;
                    } else {
                        value.defDsc = tmpDefCode;
                        console.error("Defect [%d] in prd [%d] not define in [DATE-DFCT]!", tmpDefCode, prd_seq_id);
                    }

                    //Get zh process description by proecss name
                    opeMap = gOpeMap;
                    if (opeMap.hasOwnProperty(tmpProcId)) {
                        value.ope_dsc = opeMap[tmpProcId].ope_dsc;
                    } else {
                        value.ope_dsc = tmpProcId;
                        console.error("Get ope_dsc for proc %s error!", tmpProcId);
                    }

                    //Distinct by defect_code & position =>defListHasPosition
                    if (value.hasOwnProperty('position') &&
                        _.isString(value.position)) {
                        tmpDefPosStr = value.defect_code.trim() + value.position.trim() + '@';
                        if (-1 === defListHasPositionStr.indexOf(tmpDefPosStr)) {
                            defListHasPositionStr += tmpDefPosStr;
                            defListDistinctPos.push(value);
                        } else {
                            console.log('Has found defect %s in position %s.', value.defect_code, value.position);
                        }
                    } else {
                        defListDistinctPos.push(value);
                    }
                });

                prd_jge_info.defList = defList;
                prd_jge_info.defListDistinctPos = defListDistinctPos;
                prd_jge_info.prd_grade = outObj.pv_prd_grade;
                prd_jge_info.pv_prd_grade = outObj.pv_prd_grade;
                prd_jge_info.new_logic_flg = outObj.new_logic_flg;
                prd_jge_info.prd_seq_grd_t = outObj.prd_seq_grd_t;
                prd_jge_info.prd_seq_grd_c = outObj.prd_seq_grd_c;
                prd_jge_info.act_ng_qty = outObj.act_ng_qty;
                prd_jge_info.own_typ = outObj.own_typ;
            }

            return prd_jge_info;
        },

        getShowDefect : function (defList) {
            // def.setPrd_seq_id_fk(ret_prd_jge.getId().getPrd_seq_id_fk());
            // def.setDefect_code(ret_prd_jge.getDef_code1());
            // def.setDefect_cnt(ret_prd_jge.getDef_code1_cnt());
            // def.setDefect_side(ret_prd_jge.getDef_code1_sd());
            // def.setPosition(ret_prd_jge.getDef_code1_lc());

            defList.position;
        },

        showDefectDialog : function(rowId){
            var process_user, //开始用户值
                rowData,
                proc_id,
                prd_seq_id,
                group_id,
                box_id,
                wo_id,
                pep_lvl,
                x_axis_cnt,
                y_axis_cnt,
                oldDefList,
                hide_flg,
                cus_id,
                rm_flg,
                old_judge_grade,
                prd_jge_info = {},
                new_logic_flg,
                prd_seq_grd_t,
                prd_seq_grd_c,
                //增加产品状态prd_stat,当前制程代码cr_proc_id_fk,下站制程代码nx_proc_id_fk
                prd_stat,
                cr_proc,
                nx_proc_id_fk,
                showDefList = [];

            rowData = controlsQuery.mainGrd.grdId.jqGrid("getRowData", rowId);
            prd_stat = rowData['prd_stat'];
            cr_proc = rowData['cr_proc'];
            nx_proc_id_fk = rowData['nx_proc_id_fk'];
            prd_seq_id = rowData['prd_seq_id'];
            group_id = rowData['group_id'];
            if(!prd_seq_id){
                return false;
            }
            rm_flg = false;
            cus_id = rowData['cus_id'];
            if(cus_id=="007"){
            	rm_flg = true;
            }
            box_id = rowData['box_id'].trim();

            proc_id = rowData['cr_proc'];
            if(!proc_id){
                return false;
            }
            pep_lvl = $.trim(rowData['pep_lvl']);
            wo_id = $.trim(rowData['wo_id']);
            if(!wo_id){
                showErrorDialog("","当前产品的内部订单号为空！");
                return false;
            }

            x_axis_cnt = rowData['x_axis_cnt_fk'];
            y_axis_cnt = rowData['y_axis_cnt_fk'];

            prd_jge_info = toolFunc.getDefectListByPrdSeqId(prd_seq_id, "","Y");
            oldDefList = prd_jge_info.defList;
            for(var i=0; i<oldDefList.length; i++){
            	if(oldDefList[i].show_flg == "Y"){
            		showDefList.push(oldDefList[i]);
            	}
            }
            if(rowData['prd_grade']){
            	old_judge_grade = rowData['prd_grade'];
            }else{
            	old_judge_grade = prd_jge_info.prd_grade;
            }

            old_pv_prd_grade = prd_jge_info.pv_prd_grade;
            new_logic_flg = prd_jge_info.new_logic_flg;
            prd_seq_grd_t = prd_jge_info.prd_seq_grd_t;
            prd_seq_grd_c = prd_jge_info.prd_seq_grd_c;
            hide_flg = false;
            if("SC" === old_pv_prd_grade){
            	hide_flg = true;
            }
            if(0 <= $.inArray(proc_id, VAL.GRADE_HIDE)){
            	if("SC" === old_pv_prd_grade){
            		hide_flg = false;
            	}
            }
            var ope_info = $.trim(controlsQuery.opeSelect.val());
            if(!ope_info){
                return false;
            }
            var ope_info_ary = ope_info.split("@");
            var proc_id = ope_info_ary[2];

            var gkLzFlg = false;
            if(0 <= $.inArray(proc_id, VAL.GRADE_SEL_ROCS)){
            	gkLzFlg = true;
            }else{
            	gkLzFlg = false;
            }


            $(this)._showDefectJudgeDialog({
                prd_seq_id    : prd_seq_id,
                group_id      : group_id,
                cus_id        : cus_id,
                proc_id       : proc_id,
                rowId         : rowId,
                pep_lvl       : pep_lvl,
                wo_id         : wo_id,
                x_axis_cnt    : x_axis_cnt,
                y_axis_cnt    : y_axis_cnt,
                oldDefList    : oldDefList,
                showDefList   : showDefList,
                judge_grade   : old_judge_grade,
                pv_prd_grade  : old_pv_prd_grade,
                new_logic_flg : new_logic_flg,
                prd_seq_grd_t   : prd_seq_grd_t,
                prd_seq_grd_c   : prd_seq_grd_c,
                hide_flg      : hide_flg,
                gkLzFlg       : gkLzFlg,
                groupFlg      : true,
                rm_flg        : rm_flg,
                ds_recipe_id  : rowData.ds_recipe_id,
                own_typ       : prd_jge_info.own_typ,
                //增加产品状态prd_stat,当前制程代码cr_proc_id_fk,下站制程代码nx_proc_id_fk
                prd_stat      : prd_stat,
                cr_proc       : cr_proc,
                nx_proc_id_fk :nx_proc_id_fk,
                callbackFn : function(data) {
                    if(data){
                        var rowData,
                            prd_seq_id,
                            inTrxObj,
                            outTrxObj,
                            newDefCnt,
                            newDefAry,
                            judge_grade,
                            own_typ,
                            curGridId,
                            groupId,
                            remark;

                        curGridId = controlsQuery.mainGrd.grdId;

                        newDefAry   = data.defectAry;
                        newDefCnt   = newDefAry.length;
                        judge_grade = data.judge_grade;
                        own_typ     = data.own_typ;
                        remark      = data.remark;
                        groupId     = data.group_id;
                        rowData     = curGridId.jqGrid("getRowData", data.rowId);
                        prd_seq_id  = rowData['prd_seq_id'];

                        inTrxObj = {
                            trx_id      : VAL.T_XPDEFECT ,
                            prd_seq_id  : prd_seq_id     ,
                            judge_grade : judge_grade    ,
                            own_typ     : own_typ        ,
                            def_cnt     : newDefCnt      ,
                            evt_user    : VAL.EVT_USER   ,
                            defList     : newDefAry
                        };
                        if(remark){
                        	inTrxObj.remark = remark;
                        }else{
                        	remark = "";
                        }
                        if(groupId){
                        	inTrxObj.group_id = groupId;
                        }else{
                        	groupId = "";
                        }

                        if(new_logic_flg == "Y"){//RET_PRD_GRD_INFO中存在信息，按照新逻辑判定
                        	inTrxObj.action_flg = "N" ;
                        }else{
                        	inTrxObj.action_flg = "R" ;
                        }
                        outTrxObj = comTrxSubSendPostJson(inTrxObj);
                        if( outTrxObj.rtn_code == VAL.NORMAL) {
                        	if(new_logic_flg == "Y"){
                        		newDefCnt = outTrxObj.def_cnt;
                        	}

                    		if(0 <= $.inArray($.trim(rowData['cus_id']), VAL.SLOT_NO_CUS_ID) &&
                        			0 <= $.inArray($.trim(rowData['cr_proc']), VAL.SLOT_NO_PROC) &&
                        			"INPR" == $.trim(rowData['prd_stat']) &&
                        			!$.trim(rowData['box_id'])){//088客户，非OK 品，终检，已经入账，并且开始制程，未转入箱子
                    			rowData.mtrl_slot_no_btn = "<button class='slotNoCss'>修改</button>";
                    		}else{
                    			rowData.mtrl_slot_no_btn = "";
                    		}

                    		if(0 <= $.inArray($.trim(rowData['cus_id']), VAL.SLOT_NO_CUS_ID) &&
                        			0 <= $.inArray($.trim(rowData['cr_proc']), VAL.SLOT_NO_PROC)){
                    			rowData.mtrl_slot_no_fk = rowData.slot_no;
                    		}

                    		rowData['judge_result'] = judge_grade;
                    		rowData['prd_grade'] = judge_grade;
                    		rowData['def_count'] = newDefCnt;
                    		rowData['ds_recipe_id'] = remark;
                    		rowData['group_id'] = groupId;
                    		curGridId.jqGrid('setRowData', data.rowId, rowData);
                            if("C1" == judge_grade ){
                            	$("#"+data.rowId).css("background","red");
                            }else{
                            	$("#"+data.rowId).css("background","");
                            }
                    		if($.trim(rowData['cus_id'])=="068"&&"S" == judge_grade ){
                    			$("#spcDialog_reportBtn").showCallBackWarnningDialog({
                					errMsg : "玻璃: ["+prd_seq_id+"]等级为S，请取消转入",
                					callbackFn : function(data) {
                						if (data.result == true) {
                			            inObj = {
                			                trx_id : VAL.T_XPCNPNLOUT ,
                			                evt_usr : VAL.EVT_USER,
                			                pnl_cnt : "1",
                			                iary : {
                			                    prd_seq_id : prd_seq_id
                			                }
                			            };

                			            outObj = comTrxSubSendPostJson(inObj);
                			            if (outObj.rtn_code == VAL.NORMAL) {
                			                    rowData['box_id'] = "";
                			                    rowData['end_user'] = "";
                			                    rowData['end_time'] = "";
                			                    rowData['evt_user'] = outObj.evt_user;
                			                    rowData['evt_timestamp'] = outObj.evt_timestamp;
                			                    toolFunc.enhanceGridOaryArr(rowData);
                			                    curGridId.jqGrid('setRowData', rowId, rowData);

                			                if(controlsQuery.okOutCrrTxt.val() == outObj.box_id){
                			                    controlsQuery.OkBoxCurCntTxt.val(outObj.prd_qty);
                			                    controlsQuery.OkBoxModeCntTxt.val(outObj.mode_cnt);
                			                }

                			                showMessengerSuccessDialog("成功取消转入!",1);
                			            }
                				        }else {
                				        	return false;
                				        }
                					}
                	        	});
                    		}

                            toolFunc.showDefectDetail(data.rowId);
                        }
                    }
                    return false;
                }
            });
            return false;
        },
        //By sht show defect detail info
        showDefectDetail : function(id){
            var rowData,
                prd_seq_id,
                proc_id,
                defectGrd,
                mainGrd,
                sumDefCnt = 0,
                prd_jge_info = {};

            mainGrd = controlsQuery.mainGrd.grdId;
            defectGrd = controlsQuery.mainGrd.defectGrd;
            defectGrd.jqGrid("clearGridData");

            rowData = mainGrd.jqGrid("getRowData",id);
            prd_seq_id = rowData.prd_seq_id;
            controlsQuery.defectPrdSeqIDTxt.val(prd_seq_id);

            proc_id = toolFunc.getCurProcByOpeselObj(controlsQuery.defOpeSelect);

            prd_jge_info = toolFunc.getDefectListByPrdSeqId(prd_seq_id, proc_id);
            if(prd_jge_info.new_logic_flg == "Y"){
                controlsQuery.defectCntSumTxt.val(prd_jge_info.act_ng_qty);
                setGridInfo(prd_jge_info.defList , defectGrd, true);
                toolFunc.addPositionView();
            }else{
                defList = prd_jge_info.defListDistinctPos;
                $.each(defList, function(index, value){
                    sumDefCnt += parseInt(value.defect_cnt, 10);
                });
                controlsQuery.defectCntSumTxt.val(sumDefCnt);
                setGridInfo(defList, defectGrd, true);
                toolFunc.addPositionView();

            }
            return false;
        },
        setOutBoxDialogBind : function(btnObj, itemName, targetObj, maxCntObj, crCntObj, totalModeCntObj){
            btnObj.click(function(){
                $(this)._showSimpleInputDialog({
                    itemName : itemName,
                    callbackFn : function(data) {
                        var box_id = $.trim(data.keyInResult);
                        if(box_id){
                            var boxObj = toolFunc.getBoxInfo(box_id);
                            if(boxObj){
                                if( "EMPT" !== boxObj.box_stat ){
                                    showErrorDialog("", "箱号["+ boxObj.box_id +"]状态不为空，请重新输入！");
                                    return false;
                                }
                                targetObj.val(box_id);

                                maxCntObj.val(boxObj.std_qty);
                                crCntObj.val(boxObj.prd_qty);
                                totalModeCntObj.val(parseInt(boxObj.prd_qty, 10) *
                                    gModeCnt.getModelCnt());
                            }
                        }
                    }
                });
            });
        },

        crrInputKeyDownFnc : function (btnObj, targetObj, maxCntObj, crCntObj, totalModeCntObj){
        	var box_id = $.trim(targetObj.val());
            if(box_id){
                var boxObj = toolFunc.getBoxInfo(box_id,targetObj);
                if(boxObj){
                    if( "EMPT" !== boxObj.box_stat ){
                        showErrorDialog("", "箱号["+ boxObj.box_id +"]状态不为空，请重新输入！");
                        return false;
                    }
                    targetObj.val(box_id);

                    maxCntObj.val(boxObj.std_qty);
                    crCntObj.val(boxObj.prd_qty);
                    totalModeCntObj.val(parseInt(boxObj.mode_cnt));
                }
            }
            targetObj[0].disabled = true;
            return true;
        },
        swhOpeCheckClick : function(){
            var isChecked;
            isChecked = ("checked" === controlsQuery.swhOpeChk.attr("checked"));

            if(isChecked){
                controlsQuery.swhPathSelect.removeAttr("disabled","");
                toolFunc.setRtnOpeFnc();
            }else{
                controlsQuery.swhPathSelect.attr("disabled","");
            }
        },

        setRtnOpeFnc : function (){
        	var crGrid,
        	ids,
        	rowData,
        	proc_id,
            prd_seq_id,wo_id;

        	crGrid = controlsQuery.mainGrd.grdId;

            ids = crGrid.jqGrid('getGridParam','selarrrow');
            if(0 === ids.length){
                showErrorDialog("","请勾选需要转入的产品ID！");
                controlsQuery.swhPathSelect.attr("disabled","");
                controlsQuery.swhOpeChk[0].checked = false;
                return false;
            }

            rowData = crGrid.jqGrid("getRowData", ids[0]);
            prd_seq_id = rowData.prd_seq_id ;
            wo_id      = rowData.wo_id      ;
            proc_id    = rowData.cr_proc;

            //check 返回站点
//            if(0 <= $.inArray(proc_id,VAL.RTN_OPE)){
            	var inObj, outObj;

                inObj = {
                    trx_id      : VAL.T_XPBISPTH,
                    action_flg  : 'B',
                    prd_seq_id  : prd_seq_id,
                    wo_id_fk    : wo_id
                };
                outObj = comTrxSubSendPostJson(inObj);
                if (outObj.rtn_code == VAL.NORMAL) {
                	if (outObj.tbl_cnt_a > 1){
                    	gSwhOpe = outObj.oaryA[0].first_ope_no;
                	}else if (outObj.tbl_cnt_a == 1) {
                    	gSwhOpe = outObj.oaryA.first_ope_no;
                	}
                    toolFunc.setSwhPathDate(outObj.tbl_cnt_a, outObj.oaryA, "path_id", "path_ver", "path_dsc", controlsQuery.swhPathSelect);
                }else{
                	controlsQuery.swhPathSelect.attr("disabled","");
                    controlsQuery.swhOpeChk[0].checked = false;
                }
//            }
        },
        //Start for one sht
        process_start_one_row_func : function(id){
            var crGrid,
                inObj,
                outObj,
                rowData,
                tool_id,
                root_tool_id,
                row_box_id,
                process_user,
                sht_id,
                iary_sht_id = [];

            if( !id ){
                return false;
            }

            crGrid = controlsQuery.mainGrd.grdId;
            if(gIsJBPG){
//                root_tool_id = controlsQuery.buffToolGrpSelect.val();
//                if(!root_tool_id){
//                    showErrorDialog("","请先选择抛光设备组--抛光设备！");
//                    return false;
//                }
                tool_id = controlsQuery.tftToolSelect.val();
            }else{
                tool_id = controlsQuery.toolSelect.val();
            }

            if (!tool_id) {
                showErrorDialog("","设备ID不能为空！");
                return false;
            }

            rowData = crGrid.jqGrid("getRowData", id);
            sht_id = $.trim(rowData['prd_seq_id']);
            row_box_id = $.trim(rowData['box_id']);
            prd_stat = $.trim(rowData['prd_stat']);
            if(prd_stat !== "INPR"){
                showErrorDialog('',sht_id + '尚未入账，请先输入入账箱号做点击入账！');
                return false;
            }
            if(row_box_id){
                showErrorDialog("", sht_id + "已经转入" + row_box_id + "请勿重复操作！");
                return false;
            }

            process_user = $.trim(rowData['process_user']);
            if(process_user){
                showErrorDialog("", sht_id + "已经由" + process_user + "点击开始,请勿重复操作！");
                return false;
            }

            iary_sht_id.push({
                prd_seq_id : rowData['prd_seq_id']
            });

            inObj = {
                trx_id : VAL.T_XPSTARTPROC ,
                act_flg : 'S',
                process_user : VAL.EVT_USER,
                tool_id : tool_id,
                iary_sht_id : iary_sht_id
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                rowData = crGrid.jqGrid("getRowData", id);
                rowData['process_user'] = VAL.EVT_USER;
                rowData['start_time'] = outObj.start_time;
                rowData['cr_tool_id_fk'] = outObj.tool_id;
                toolFunc.enhanceGridOaryArr(rowData);
                crGrid.jqGrid('setRowData', id, rowData);
            }
        },
        getCurProc : function(){
            var ope_info,
                ope_info_ary,
                proc_id;

            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            proc_id = ope_info_ary[2];

            return proc_id;
        },
        getCurProcByOpeselObj : function($opeSel){
            var ope_info,
                ope_info_ary,
                proc_id;

            ope_info = $.trim($opeSel.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            proc_id = ope_info_ary[2];

            return proc_id;
        },
        //Action by proc
        check_proc_func : function(){
            var ope_info,
                ope_info_ary,
                proc_id;

            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            proc_id = ope_info_ary[2];

            //Check prod => show/hide btn-group
            if(0 <= $.inArray(proc_id, VAL.GRADE_SEL_ROCS)){
                controlsQuery.gradeSelectDiv.show();
            }else{
            	controlsQuery.gradeSelectDiv.empty();
                controlsQuery.gradeSelectDiv.hide();
            }
            //check是否显示在制等待checkBox
//            controlsQuery.wip_wait_div.hide();
//            controlsQuery.wip_wait_chk[0].checked = false;
//            if(0 <= $.inArray(proc_id, VAL.WIP_WAIT_VIALD)){
//            	controlsQuery.wip_wait_div.show();
//            }
            //Check JBPG
            gIsJBPG = ( proc_id === VAL.JBPG_PROC );

            btnQuery.save_buff_time_btn.hide();
            gShowF6Btn = (-1 != $.inArray( proc_id, VAL.SHOW_F6_BTN));
            if (gShowF6Btn){
            	btnQuery.save_buff_time_btn.show();
            }

            //Can save buff time proc
            gCanSaveBufTime = (-1 != $.inArray( proc_id, VAL.CAN_SAVE_BUF_TIME_PROC));
            if( true === gCanSaveBufTime ){
                controlsQuery.buffInfoDiv.show();
                $("#pg").hide();
                controlsQuery.toolGrpDiv.show();
            	controlsQuery.tftToolDiv.show();
            	controlsQuery.cfToolDiv.show();
            }else{
                controlsQuery.buffInfoDiv.hide();
                $("#pg").show();
            }

            if (0 == $.inArray( proc_id,VAL.BUF_TIME_JBYZ)) {
            	controlsQuery.buffInfoDiv.show();
            	controlsQuery.toolGrpDiv.hide();
            	controlsQuery.tftToolDiv.hide();
            	controlsQuery.cfToolDiv.hide();
            	gCanSaveBufTime = true;
            }

            //QA_PROCS 需要显示检验按钮的制程
            if(0 <= $.inArray(proc_id, VAL.QA_PROCS)){
                btnQuery.qa_btn.show();
            }else{
                btnQuery.qa_btn.hide();
            }

            //SHOW_YIELD_PROCS 需要显示良率数据
//            if(0 <= $.inArray(proc_id, VAL.SHOW_YIELD_PROCS)){
//                controlsQuery.yieldInfoDiv.show();
//            }else{
//                controlsQuery.yieldInfoDiv.hide();
//            }
            //GROUP_GRADE_CHECK
            if(0 <= $.inArray(proc_id, VAL.GROUP_GRADE_CHECK)){
            	gchk_flg = "Y";
            }else{
            	gchk_flg = "N";
            }

        },
        //Get all defect in DATA-DFCT ==> gDefectMap
        get_all_defect_func : function(cus_id){
            var i,
                defectCnt,
                inTrxObj,
                outTrxObj,
                dataAry = [];

            inTrxObj = {
                trx_id: VAL.T_XPINQCOD,
                action_flg: 'I',
                data_cate : 'DFCT',
                cus_id     : cus_id
            };
            outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if( outTrxObj.rtn_code == VAL.NORMAL) {
                gDefectMap = {};
                defectCnt = parseInt(outTrxObj.tbl_cnt, 10);
                if( 1 === defectCnt){
                    dataAry.push(outTrxObj.oary);
                }else{
                    dataAry = outTrxObj.oary;
                }
                for( i = 0; i < defectCnt; i++ ){
                    gDefectMap[ dataAry[i].data_id ] = {
                        defDsc : dataAry[i].data_ext
                    };
                }
            }
        },
        getWoInfoFunc : function(wo_id){
            var inObj,
                outObj;

            controlsQuery.from_thicknessTxt.val("");
            controlsQuery.to_thicknessTxt.val("");
            controlsQuery.t_thicknessTxt.val("");
            controlsQuery.c_thicknessTxt.val("");
            if(!wo_id){
                console.warn("wo_id [%s] is null!", wo_id);
                return false;
            }

            inObj = {
                trx_id      : VAL.T_XPAPLYWO,
                action_flg  : 'Q'           ,
                tbl_cnt     : 1             ,
                iary : {
                    wo_id     : wo_id,
                    mtrl_cate : "SHMT"
                }
            };
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                controlsQuery.from_thicknessTxt.val(outObj.oary.from_thickness);
                controlsQuery.to_thicknessTxt.val(outObj.oary.to_thickness);
                controlsQuery.t_thicknessTxt.val(outObj.oary.t_thickness);
                controlsQuery.c_thicknessTxt.val(outObj.oary.c_thickness);
                controlsQuery.ope_eff_flgTxt.val(outObj.oary.ope_eff_flg);
                controlsQuery.woPlnQtyTxt.val(outObj.oary.pln_prd_qty);
                controlsQuery.woRlQtyTxt.val(outObj.oary.rl_prd_qty);
                controlsQuery.woLevQtyTxt.val(
                    parseInt(outObj.oary.pln_prd_qty,10) - parseInt(outObj.oary.rl_prd_qty,10)
                );
                gCus_id = outObj.oary.cus_id_fk;
            }
            return true;
        },
        //初始化mode cnt
        getLayoutByWoFunc : function(wo_id){
            var iary,
                inTrxObj,
                outTrxObj,
                oary;

            if(!wo_id){
                console.warn("wo_id [%s] is null!", wo_id);
                gModeCnt.clear();
                return false;
            }

            if(wo_id === gModeCnt.wo_id){
                return true;
            }

            iary = {
                wo_id : wo_id
            };
            inTrxObj ={
                trx_id     : "XPLAYOUT",
                action_flg : "W",
                iary       : iary,
                tbl_cnt    : 1
            };

            outTrxObj = comTrxSubSendPostJson(inTrxObj);
            if( outTrxObj.rtn_code == VAL.NORMAL ) {
                oary = outTrxObj.oary;
                if(oary==null){
                    conole.error("版式信息异常,请检查产品对应的版式信息");
                    return false;
                }
                gModeCnt.wo_id = wo_id;
                gModeCnt.x_cnt = oary.x_axis_cnt;
                gModeCnt.y_cnt = oary.y_axis_cnt;

                return true;
            }
            return true;
        },
        syncYieldInfo : function(box_id, cr_proc_id){
            var inTrx,
                outTrx,
                proc_id="",
                l_def_cnt,
                p_def_cnt,
                proc_yield,
                whole_yield;

            if(cr_proc_id == "JBZJ" || cr_proc_id == "JBQA"){
            	proc_id = "JBQA";
            }else if(cr_proc_id == "DMIC" || cr_proc_id == "DMQA" || cr_proc_id == "DMZJ"){
            	proc_id = "DMQA";
            }
            inTrx = {
      			   trx_id  : VAL.T_XPINQDEF,
    			   action_flg : "O"   ,
                   box_id  : box_id,
                   proc_id  : proc_id,
                   check_flg : 'N'
            };

            var qurl = "jcom/sendMsg";
            var inTrxStr = JSON.stringify(inTrx);
            var outTrxObj = null;
            jQuery.ajax({
                type:"post",
                url:qurl,
                timeout:60000,
                data:{
                    strInMsg:inTrxStr
                },
                success: function (data) {
                    //check denied
                    if(data instanceof Object){
                        outTrxObj =  JSON.parse(data.strOutMsg);
                        var rtn_code = outTrxObj.rtn_code;
                        if (rtn_code != "0000000") {
                            showErrorDialog(rtn_code,outTrxObj.rtn_mesg);
                        }else{
                            outTrx = outTrxObj;
                            l_def_cnt = outTrx.l_def_cnt;
                            p_def_cnt = outTrx.p_def_cnt;
                            proc_yield = outTrx.proc_yield;
                            whole_yield = outTrx.whole_yield;

                            controlsQuery.procDefTxt.val(p_def_cnt);//制程不良
                            controlsQuery.cusDefTxt.val(l_def_cnt);//来料不良
                            controlsQuery.procYieldTxt.val(proc_yield + "%");//制程良率
                            controlsQuery.wholeYieldTxt.val(whole_yield + "%");//综合良率
                        }
                    }
                },
                error : function(xhr, stat, e){
                    console.error(xhr);
                }
            });
        },
      //批量查询WAIT状态的箱子信息入账
        queryWAITbox : function(){
        	var inObj,
                outObj,
                ope_id,
                ope_ver,
                proc_id,
                ope_info_ary,
                wo_id,
                tool_id;

            tool_id = $.trim(controlsQuery.toolSelect.val());
            ope_info = $.trim(controlsQuery.opeSelect.val());
            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];
            ope_ver = ope_info_ary[1];
            proc_id = ope_info_ary[2];

            wo_id = controlsQuery.woIdSelect.val().trim();

            inObj = {
                trx_id      : VAL.T_XPIWIPEQ,
                action_flg  : "I"           ,
                cr_ope_id   : ope_id,
                cr_ope_ver  : ope_ver,
                tool_id     : tool_id,
                cr_proc_id  : proc_id
            };

            if(wo_id){
               inObj.wo_id = wo_id;
            }

            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
            	if(parseInt( outObj.tbl_cnt, 10) != 0){
                    $("#f1_btn").showWaitBoxDialog({
        		        ope_id     : ope_id,
        		        ope_ver    : ope_ver,
        				user_id    : VAL.EVT_USER,
        				boxOary    : outObj.oary
                    });
            	}
            }
        },
        //获取打印机
        getPrinters : function(){
        	controlsQuery.print1Select.append("<option ></option>");
        	controlsQuery.print2Select.append("<option ></option>");
        	if (printers !== undefined) {
        		for(var i=0; i<printers.length; i++){
                	controlsQuery.print1Select.append("<option>"+ printers[i] +"</option>");
                	controlsQuery.print2Select.append("<option>"+ printers[i] +"</option>");
            	}
			}
        },
        /**
         * 打印QA出货明细表
         */
        printQA : function(out_box_id,proc_id){//JBQA || DMQA
     	   showMessengerSuccessDialog(out_box_id + "正在打印出货明细表...",1);
     	   var printTyp = "P";//打印
    	   var inObject,outObject,mdlId="",cusInfoFst,checkUser;
    	   var prd_id = [],mtrl_box_id = [],grade = [];
    	   inObject={
    			   trx_id  : VAL.T_XPINQDEF,
    			   action_flg : "O"   ,
    			   box_id  : out_box_id,
    			   proc_id : proc_id,
    			   check_flg : 'Y'
    	   };
    	   outObject =  comTrxSubSendPostJson(inObject);
    	   if( outObject.rtn_code == VAL.NORMAL ){
    		   if(proc_id == "JBQA"){
    			   mdlId = outObject.mdl_id_fk;
    		   	}else if(proc_id == "DMQA"){
    		   		mdlId = outObject.fm_mdl_id_fk;
    		   	}
			   if(outObject.cus_info_fst.length == 0){
				   cusInfoFst = "";
			   }else{
				   cusInfoFst = outObject.cus_info_fst;
			   }
			   if(outObject.check_user.length == 0){
				   checkUser = "";
			   }else{
				   checkUser = outObject.check_user;
			   }
    		   var totalModelCnt = parseInt(outObject.prd_cnt,10) * parseInt(outObject.model_cnt,10);
       		   if(outObject.cus_id == "003"){
       			   var oary = outObject.prd_cnt > 1 ? outObject.oary003 : [outObject.oary003];
       			   var r_yjyc_cnt = [],r_llhx_cnt = [],r_atd_cnt = [],r_pl_cnt = [],r_lh_cnt = [],
       			   		r_qj_cnt = [],r_hs_cnt = [],r_bw_cnt = [],r_other = [],r_cnt = [],
       			   		c_pl_cnt = [],c_lh_cnt = [],c_qj_cnt = [],c_cj_cnt = [],c_ly_cnt = [],c_lsw_cnt = [],
       			   		c_td_cnt = [],c_lhy_cnt = [],c_ad_cnt = [],c_hs_cnt = [],c_bw_cnt = [],c_other = [],c_cnt = [],
       			   		remark = [];
       			   for(var j=0; j<outObject.prd_cnt ; j++){
       				   mtrl_box_id[j] = oary[j].mtrl_box_id_fk;
       				   grade[j] = oary[j].prd_grade;
       				   r_yjyc_cnt[j] = oary[j].r_yjyc_cnt;
       				   r_llhx_cnt[j] = oary[j].r_llhx_cnt;
       				   r_atd_cnt[j] = oary[j].r_atd_cnt;
       				   r_pl_cnt[j] = oary[j].r_pl_cnt;
       				   r_lh_cnt[j] = oary[j].r_lh_cnt;
       				   r_qj_cnt[j] = oary[j].r_qj_cnt;
       				   r_hs_cnt[j] = oary[j].r_hs_cnt;
       				   r_bw_cnt[j] = oary[j].r_bw_cnt;
       				   r_other[j] = oary[j].r_other;
       				   r_cnt[j] = parseInt(oary[j].r_llhx_cnt,10) + parseInt(oary[j].r_atd_cnt,10) + parseInt(oary[j].r_pl_cnt,10)+ parseInt(oary[j].r_lh_cnt,10) +
       				   			parseInt(oary[j].r_qj_cnt,10) + parseInt(oary[j].r_hs_cnt,10) + parseInt(oary[j].r_bw_cnt,10) + parseInt(oary[j].r_other,10);
       				   c_pl_cnt[j] = oary[j].c_pl_cnt;
       				   c_lh_cnt[j] = oary[j].c_lh_cnt;
       				   c_qj_cnt[j] = oary[j].c_qj_cnt;
       				   c_cj_cnt[j] = oary[j].c_cj_cnt;
       				   c_ly_cnt[j] = oary[j].c_ly_cnt;
       				   c_lsw_cnt[j] = oary[j].c_lsw_cnt;
       				   c_td_cnt[j] = oary[j].c_td_cnt;
       				   c_lhy_cnt[j] = oary[j].c_lhy_cnt;
       				   c_ad_cnt[j] = oary[j].c_ad_cnt;
       				   c_hs_cnt[j] = oary[j].c_hs_cnt;
       				   c_bw_cnt[j] = oary[j].c_bw_cnt;
       				   c_other[j] = oary[j].c_other;
       				   c_cnt[j] = parseInt(oary[j].c_pl_cnt,10) + parseInt(oary[j].c_lh_cnt,10) + parseInt(oary[j].c_qj_cnt,10)+ parseInt(oary[j].c_cj_cnt,10) +
			   				parseInt(oary[j].c_ly_cnt,10) + parseInt(oary[j].c_lsw_cnt,10) + parseInt(oary[j].c_td_cnt,10) + parseInt(oary[j].c_lhy_cnt,10) +
			   				parseInt(oary[j].c_ad_cnt,10) + parseInt(oary[j].c_hs_cnt,10) + parseInt(oary[j].c_bw_cnt,10) + parseInt(oary[j].c_other,10);
       				   if(oary[j].remark.length == 0){
       					   remark[j] = "";
       				   }else{
       					   remark[j] = oary[j].remark;
       				   }
       			   }
       			   if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
           				for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
         					 prd_id[i] = mtrl_box_id[i] = grade[i] = "";
         					 r_yjyc_cnt[i] = r_llhx_cnt[i] = r_atd_cnt[i] = r_pl_cnt[i] = "";
         					 r_lh_cnt[i] = r_qj_cnt[i] = r_hs_cnt[i] = r_bw_cnt[i] = r_other[i] = "";
         					 r_cnt[i] = "";
         					 c_pl_cnt[i] = c_lh_cnt[i] = c_qj_cnt[i] = c_cj_cnt[i] = "";
         					 c_ly_cnt[i] = c_lsw_cnt[i] = c_td_cnt[i] = c_lhy_cnt[i] = "";
         					 c_ad_cnt[i] = c_hs_cnt[i] = c_bw_cnt[i] = c_other[i] = "";
         					 c_cnt[i] = "";
       						 remark[i] = "";
   					   }
       			   }
       			   var printObj = {
       					printTyp : printTyp,
       					procId : proc_id,
       					stdQty : outObject.std_qty,
       					cusId : outObject.cus_id,
       					woId : outObject.wo_id,
       					boxId : outObject.box_id,
       					shipBoxId : outObject.ship_box_id,
       					toThickness : outObject.to_thickness,
       					prdCnt : outObject.prd_cnt,
       					modelCnt : outObject.model_cnt,
       					totalModelCnt : totalModelCnt,
       					mdlId : mdlId,
       					prdAry : prd_id,
       					mtrlBoxAry : mtrl_box_id,
       					rYjycAry : r_yjyc_cnt,
       					rLlhxAry : r_llhx_cnt,
       					rAtdAry : r_atd_cnt,
       					rPlAry : r_pl_cnt,
       					rLhAry : r_lh_cnt,
       					rQjAry : r_qj_cnt,
       					rHsAry : r_hs_cnt,
       					rBwAry : r_bw_cnt,
       					rOtherAry : r_other,
       					rCntAry : r_cnt,
       					cPlAry : c_pl_cnt,
       					cLhAry : c_lh_cnt,
       					cQjAry : c_qj_cnt,
       					cCjAry : c_cj_cnt,
       					cLyAry : c_ly_cnt,
       					cLswAry : c_lsw_cnt,
       					cTdAry : c_td_cnt,
       					cLhyAry : c_lhy_cnt,
       					cAdAry : c_ad_cnt,
       					cHsAry : c_hs_cnt,
       					cBwAry : c_bw_cnt,
       					cOther : c_other,
       					cCntAry : c_cnt,
       					remarkAry : remark
       			   };
       			   label.PrintJBorDMQA003(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
       		   }else if(outObject.cus_id == "006"){// 006客户
           			   var oary = outObject.prd_cnt > 1 ? outObject.oaryC : [outObject.oaryC];
 					   var yjyc_cnt = [],sy_cnt = [],C_hs_cnt = [],C_ad_cnt = [],
 					   		C_td_cnt = [],C_shbj_cnt = [],C_ps_cnt = [],C_zw_cnt = [],C_ly_cnt = [],
 					   		C_other = [],T_hs_cnt = [],T_ad_cnt = [],T_td_cnt = [],
 					   		T_shbj_cnt = [],T_ps_cnt = [],T_zw_cnt = [],T_ly_cnt = [],T_other = [],
 					   		remark = [];
              		   if(outObject.ope_eff_flg == "Y"){// 原箱
              			   for(var i=0; i<outObject.std_qty ; i++ ){
              				   for(var j=0; j<outObject.prd_cnt ; j++){
              					   if((i+1) == oary[j].slot_no){
              						   yjyc_cnt[i] = oary[j].yjyc_cnt;
              						   sy_cnt[i] = oary[j].sy_cnt;
              						   C_hs_cnt[i] = oary[j].C_hs_cnt;
              						   C_ad_cnt[i] = oary[j].C_ad_cnt;
              						   C_td_cnt[i] = oary[j].C_td_cnt;
              						   C_shbj_cnt[i] = oary[j].C_shbj_cnt;
              						   C_ps_cnt[i] = oary[j].C_ps_cnt;
              						   C_zw_cnt[i] = oary[j].C_zw_cnt;
              						   C_ly_cnt[i] = oary[j].C_ly_cnt;
              						   C_other[i] = oary[j].C_other;
              						   T_hs_cnt[i] = oary[j].T_hs_cnt;
              						   T_ad_cnt[i] = oary[j].T_ad_cnt;
              						   T_td_cnt[i] = oary[j].T_td_cnt;
              						   T_shbj_cnt[i] = oary[j].T_shbj_cnt;
              						   T_ps_cnt[i] = oary[j].T_ps_cnt;
              						   T_zw_cnt[i] = oary[j].T_zw_cnt;
              						   T_ly_cnt[i] = oary[j].T_ly_cnt;
              						   T_other[i] = oary[j].T_other;
              						   if(oary[j].remark.length == 0){
              							 remark[i] = "";
              						   }else{
                  						 remark[i] = oary[j].remark;
              						   }
              						   break;
              					   }
              					   if(j == parseInt(outObject.prd_cnt,10) - 1){
              						   prd_id[i] = "";
              						   yjyc_cnt[i] = sy_cnt[i] = "";
              						   C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] =  C_other[i] = "";
              						   T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = "";
              						   remark[i] = "";
              					   }
              				   }
              			   }
              		   }else{// 非原箱
              			  for(var j=0; j<outObject.prd_cnt ; j++){
              				  yjyc_cnt[j] = oary[j].yjyc_cnt;
              				  sy_cnt[j] = oary[j].sy_cnt;
              				  C_hs_cnt[j] = oary[j].C_hs_cnt;
              				  C_ad_cnt[j] = oary[j].C_ad_cnt;
              				  C_td_cnt[j] = oary[j].C_td_cnt;
              				  C_shbj_cnt[j] = oary[j].C_shbj_cnt;
              				  C_ps_cnt[j] = oary[j].C_ps_cnt;
              				  C_zw_cnt[j] = oary[j].C_zw_cnt;
              				  C_ly_cnt[j] = oary[j].C_ly_cnt;
              				  C_other[j] = oary[j].C_other;
              				  T_hs_cnt[j] = oary[j].T_hs_cnt;
              				  T_ad_cnt[j] = oary[j].T_ad_cnt;
              				  T_td_cnt[j] = oary[j].T_td_cnt;
              				  T_shbj_cnt[j] = oary[j].T_shbj_cnt;
              				  T_ps_cnt[j] = oary[j].T_ps_cnt;
              				  T_zw_cnt[j] = oary[j].T_zw_cnt;
              				  T_ly_cnt[j] = oary[j].T_ly_cnt;
              				  T_other[j] = oary[j].T_other;
              				  if(oary[j].remark.length == 0){
          						 remark[j] = "";
              				  }else{
          						 remark[j] = oary[j].remark;
              				  }
              			  }
              			  if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
              				  for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
              					  prd_id[i] = "";
              					  yjyc_cnt[i] = sy_cnt[i] = "";
              					  C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] = C_other[i] = "";
              					  T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = "";
              					  remark[i] = "";
              				  }
              			  }
              		   }
              		   var printObj = {
              			   printTyp : printTyp,
              			   procId : proc_id,
						   stdQty : outObject.std_qty,
						   cusId : outObject.cus_id,
						   woId : outObject.wo_id,
						   boxId : outObject.box_id,
//						   shipBoxId : outObject.ship_box_id,
						   toThickness : outObject.to_thickness,
						   prdCnt : outObject.prd_cnt,
						   modelCnt : outObject.model_cnt,
						   totalModelCnt : totalModelCnt,
						   whole_def_cnt : parseInt(outObject.l_def_cnt,10) + parseInt(outObject.p_def_cnt,10),
						   whole_yield : outObject.whole_yield,
						   checkUser : checkUser,
						   mdlId : mdlId,
						   prdAry : prd_id,
						   yjycAry : yjyc_cnt,
						   syAry : sy_cnt,
						   cHsAry : C_hs_cnt,
						   cAdAry : C_ad_cnt,
						   cTdAry : C_td_cnt,
						   cShbjAry : C_shbj_cnt,
						   cPsAry : C_ps_cnt,
						   cZwAry : C_zw_cnt,
						   cLyAry : C_ly_cnt,
						   cOtherAry : C_other,
						   tHsAry : T_hs_cnt,
						   tAdAry : T_ad_cnt,
						   tTdAry : T_td_cnt,
						   tShbjAry : T_shbj_cnt,
						   tPsAry : T_ps_cnt,
						   tZwAry : T_zw_cnt,
						   tLyAry : T_ly_cnt,
						   tOtherAry : T_other,
						   remarkAry : remark
       		        	};
       		        	label.PrintJBorDMQA2(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
//            	   }
       		   }else if(outObject.cus_id == "008"){//008 HSD客户
       			   var oary = outObject.prd_cnt > 1 ? outObject.oary008 : [outObject.oary008];
       			   var l_ly_cnt = [],l_llxo_cnt = [],l_hnbl_cnt = [],l_pl_cnt = [],
       			   		z_c_skbj_cnt = [],z_c_dqx_cnt = [],z_c_xqx_cnt = [],z_c_wj_cnt = [],
       			   		z_t_skbj_cnt = [],z_t_dqx_cnt = [],z_t_xqx_cnt = [],z_t_wj_cnt = [],z_t_pit_cnt = [],
       			   		jbpl_cnt = [],jbly_cnt = [],status = [],remark = [];
       				   for(var j=0; j<outObject.prd_cnt ; j++){
       					   l_ly_cnt[j] = oary[j].l_ly_cnt;
						   l_llxo_cnt[j] = oary[j].l_llxo_cnt;
						   l_hnbl_cnt[j] = oary[j].l_hnbl_cnt;
						   l_pl_cnt[j] = oary[j].l_pl_cnt;
						   z_c_skbj_cnt[j] = oary[j].z_c_skbj_cnt;
						   z_c_dqx_cnt[j] = oary[j].z_c_dqx_cnt;
						   z_c_xqx_cnt[j] = oary[j].z_c_xqx_cnt;
						   z_c_wj_cnt[j] = oary[j].z_c_wj_cnt;
						   z_t_skbj_cnt[j] = oary[j].z_t_skbj_cnt;
						   z_t_dqx_cnt[j] = oary[j].z_t_dqx_cnt;
						   z_t_xqx_cnt[j] = oary[j].z_t_xqx_cnt;
						   z_t_wj_cnt[j] = oary[j].z_t_wj_cnt;
						   z_t_pit_cnt[j] = oary[j].z_t_pit_cnt;
						   jbpl_cnt[j] = oary[j].jbpl_cnt;
						   jbly_cnt[j] = oary[j].jbly_cnt;
						   if(oary[j].remark.length == 0){
							   remark[j] = "";
						   }else{
							   remark[j] = oary[j].remark;
						   }
						   if(proc_id == "JBQA"){
								   status[j] = "已减薄";
							   }else if(proc_id == "DMQA"){
								   status[j] = "已镀膜";
							   }
       				   }
       				   if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
       					   for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
       						   prd_id[i] = "";
       						   l_ly_cnt[i] = l_llxo_cnt[i] = l_hnbl_cnt[i] = l_pl_cnt[i] = "";
							   z_c_skbj_cnt[i] = z_c_dqx_cnt[i] = z_c_xqx_cnt[i] = z_c_wj_cnt[i] = "";
							   z_t_skbj_cnt[i] = z_t_dqx_cnt[i] = z_t_xqx_cnt[i] = z_t_wj_cnt[i] = z_t_pit_cnt[i] = "";
							   jbpl_cnt[i] = jbly_cnt[i] = "";
							   status[i] = remark[i] = "";
       					   }
       				   }
//       			   }

       			   var printObj = {
       				   printTyp : printTyp,
       				   procId : proc_id,
   					   stdQty : outObject.std_qty,
   					   cusId : outObject.cus_id,
   					   woId : outObject.wo_id,
   					   soId : outObject.so_id,
   					   boxId : outObject.box_id,
   					   cusInfoFst : cusInfoFst,
   					   modelCnt : outObject.model_cnt,
					   totalModelCnt : totalModelCnt,
   					   prdCnt : outObject.prd_cnt,
   					   mdlId : mdlId,
   					   prdAry : prd_id,
					   lLyAry : l_ly_cnt,
					   lLlxoAry : l_llxo_cnt,
					   lHnblAry : l_hnbl_cnt,
					   lPlAry : l_pl_cnt,
					   zcSkbjAry : z_c_skbj_cnt,
					   zcDqxAry : z_c_dqx_cnt,
					   zcXqxAry : z_c_xqx_cnt,
					   zcWjAry : z_c_wj_cnt,
					   ztSkbjAry : z_t_skbj_cnt,
					   ztDqxAry : z_t_skbj_cnt,
					   ztXqxAry : z_t_xqx_cnt,
					   ztWjAry : z_t_wj_cnt,
					   ztPitAry : z_t_pit_cnt,
					   jbPlAry : jbpl_cnt,
					   jbLyAry : jbly_cnt,
					   statusAry : status,
   					   remarkAry : remark
       			   };
       			   label.PrintJBorDMQA008(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
       		   }else if(outObject.cus_id == "063"){//063客户
       			   var oary = outObject.prd_cnt > 1 ? outObject.oary063 : [outObject.oary063];
       			   var status = [], remark = [];
           			  for(var j=0; j<outObject.prd_cnt ; j++){
           				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
							   if(proc_id == "JBQA"){
								   status[j] = "已减薄";
							   }else if(proc_id == "DMQA"){
								   status[j] = "已镀膜";
							   }
           				  remark[j] = oary[j].remark;
           			  }
           			  if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
           				  for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
           					  prd_id[i] = "";
           					  status[i] = "";
           					  remark[i] = "";
       					   }
       				   }
//           		   }

       			   var printObj = {
       					   printTyp : printTyp,
       					   procId : proc_id,
						   stdQty : outObject.std_qty,
						   cusId : outObject.cus_id,
						   woId : outObject.wo_id,
						   soId : outObject.so_id,
						   boxId : outObject.box_id,
						   prdCnt : outObject.prd_cnt,
						   modelCnt : outObject.model_cnt,
						   totalModelCnt : totalModelCnt,
						   mdlId : mdlId,
						   prdAry : prd_id,
						   statusAry : status,
						   remarkAry : remark
       			   };
       			   label.PrintJBorDMQA063(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
       		   }else if(outObject.cus_id == "028" ||
    				   outObject.cus_id == "071" ||
    				   outObject.cus_id == "088"){//天马客户
    			   var oary = outObject.prd_cnt > 1 ? outObject.oaryC : [outObject.oaryC];
    			   var yjyc_cnt = [],sy_cnt = [],C_hs_cnt = [],C_ad_cnt = [],
						C_td_cnt = [],C_shbj_cnt = [],C_ps_cnt = [],C_zw_cnt = [],C_ly_cnt = [],
						C_other = [],T_hs_cnt = [],T_ad_cnt = [],T_td_cnt = [],
						T_shbj_cnt = [],T_ps_cnt = [],T_zw_cnt = [],T_ly_cnt = [],T_other = [],
						remark = [];
    			   if(outObject.ope_eff_flg == "Y"){//原箱
    				   for(var i=0; i<outObject.std_qty ; i++ ){
    					   for(var j=0; j<outObject.prd_cnt ; j++){
    						   if((i+1) == oary[j].slot_no){
    							   prd_id[i] = oary[j].fab_sn;
    							   yjyc_cnt[i] = oary[j].yjyc_cnt;
    							   sy_cnt[i] = oary[j].sy_cnt;
    							   C_hs_cnt[i] = oary[j].C_hs_cnt;
    							   C_ad_cnt[i] = oary[j].C_ad_cnt;
    							   C_td_cnt[i] = oary[j].C_td_cnt;
    							   C_shbj_cnt[i] = oary[j].C_shbj_cnt;
    							   C_ps_cnt[i] = oary[j].C_ps_cnt;
    							   C_zw_cnt[i] = oary[j].C_zw_cnt;
    							   C_ly_cnt[i] = oary[j].C_ly_cnt;
    							   C_other[i] = oary[j].C_other;
    							   T_hs_cnt[i] = oary[j].T_hs_cnt;
    							   T_ad_cnt[i] = oary[j].T_ad_cnt;
    							   T_td_cnt[i] = oary[j].T_td_cnt;
    							   T_shbj_cnt[i] = oary[j].T_shbj_cnt;
    							   T_ps_cnt[i] = oary[j].T_ps_cnt;
    							   T_zw_cnt[i] = oary[j].T_zw_cnt;
    							   T_ly_cnt[i] = oary[j].T_ly_cnt;
    							   T_other[i] = oary[j].T_other;
    							   if(oary[j].remark.length == 0){
    								   remark[i] = "";
    							   }else{
    								   remark[i] = oary[j].remark;
    							   }
    							   break;
    						   }
    						   if(j == parseInt(outObject.prd_cnt,10) - 1){
    							   prd_id[i] = "";
    							   yjyc_cnt[i] = sy_cnt[i] = "";
    							   C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] =  C_other[i] = "";
    							   T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = "";
    							   remark[i] = "";
    						   }
    					   }
    				   }
    			   }else{//非原箱
    				   for(var j=0; j<outObject.prd_cnt ; j++){
    					   prd_id[j] = oary[j].fab_sn;
    					   yjyc_cnt[j] = oary[j].yjyc_cnt;
    					   sy_cnt[j] = oary[j].sy_cnt;
    					   C_hs_cnt[j] = oary[j].C_hs_cnt;
    					   C_ad_cnt[j] = oary[j].C_ad_cnt;
    					   C_td_cnt[j] = oary[j].C_td_cnt;
    					   C_shbj_cnt[j] = oary[j].C_shbj_cnt;
    					   C_ps_cnt[j] = oary[j].C_ps_cnt;
    					   C_zw_cnt[j] = oary[j].C_zw_cnt;
    					   C_ly_cnt[j] = oary[j].C_ly_cnt;
    					   C_other[j] = oary[j].C_other;
    					   T_hs_cnt[j] = oary[j].T_hs_cnt;
    					   T_ad_cnt[j] = oary[j].T_ad_cnt;
    					   T_td_cnt[j] = oary[j].T_td_cnt;
    					   T_shbj_cnt[j] = oary[j].T_shbj_cnt;
    					   T_ps_cnt[j] = oary[j].T_ps_cnt;
    					   T_zw_cnt[j] = oary[j].T_zw_cnt;
    					   T_ly_cnt[j] = oary[j].T_ly_cnt;
    					   T_other[j] = oary[j].T_other;
    					   if(oary[j].remark.length == 0){
    						   remark[j] = "";
    					   }else{
    						   remark[j] = oary[j].remark;
    					   }
    				   }
    				   if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
    					   for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
    						   prd_id[i] = "";
    						   yjyc_cnt[i] = sy_cnt[i] = "";
    						   C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] = C_other[i] = "";
    						   T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = "";
    						   remark[i] = "";
    					   }
    				   }
    			   }
    			   var printObj = {
    					   printTyp : printTyp,
    					   procId : proc_id,
    					   stdQty : outObject.std_qty,
    					   cusId : outObject.cus_id,
    					   woId : outObject.wo_id,
    					   boxId : outObject.box_id,
    					   shipBoxId : outObject.ship_box_id,
    					   toThickness : outObject.to_thickness,
    					   prdCnt : outObject.prd_cnt,
    					   modelCnt : outObject.model_cnt,
    					   totalModelCnt : totalModelCnt,
						   whole_def_cnt : parseInt(outObject.l_def_cnt,10) + parseInt(outObject.p_def_cnt,10),
						   whole_yield : outObject.whole_yield,
						   checkUser : checkUser,
    					   mdlId : mdlId,
    					   prdAry : prd_id,
    					   yjycAry : yjyc_cnt,
    					   syAry : sy_cnt,
    					   cHsAry : C_hs_cnt,
    					   cAdAry : C_ad_cnt,
    					   cTdAry : C_td_cnt,
    					   cShbjAry : C_shbj_cnt,
    					   cPsAry : C_ps_cnt,
    					   cZwAry : C_zw_cnt,
    					   cLyAry : C_ly_cnt,
    					   cOtherAry : C_other,
    					   tHsAry : T_hs_cnt,
    					   tAdAry : T_ad_cnt,
    					   tTdAry : T_td_cnt,
    					   tShbjAry : T_shbj_cnt,
    					   tPsAry : T_ps_cnt,
    					   tZwAry : T_zw_cnt,
    					   tLyAry : T_ly_cnt,
    					   tOtherAry : T_other,
    					   remarkAry : remark
    			   };
    			   label.PrintJBorDMQA2(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
    		   }else if (outObject.cus_id == "098"){// 098客户
       			   if(proc_id == "JBQA"){
           			   var oary = outObject.prd_cnt > 1 ? outObject.oary006 : [outObject.oary006];
           			   var C_z_pl_cnt = [],C_z_ly_cnt = [],C_z_zw_cnt = [],C_z_skbj_cnt = [],C_z_atd_cnt = [],C_z_hs_cnt = [],
           			   		C_l_pl_cnt = [],C_l_yjyc_cnt = [],C_l_ad_cnt = [],C_l_mjad_cnt = [],C_l_hs_cnt = [],C_l_gly_cnt = [],
           			   		T_z_zw_cnt = [],T_z_skbj_cnt = [],T_z_atd_cnt = [],T_z_hs_cnt = [],
           			   		T_l_ad_cnt = [],T_l_hs_cnt = [],T_l_gly_cnt = [],
           			   		totalCnt = [],
           			   		status = [],remark = [];
              			  for(var j=0; j<outObject.prd_cnt ; j++){
              				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
              				  mtrl_box_id[j] = oary[j].mtrl_box_id_fk;
              				  C_z_pl_cnt[j] = oary[j].C_z_pl_cnt;
              				  C_z_ly_cnt[j] = oary[j].C_z_ly_cnt;
              				  C_z_zw_cnt[j] = oary[j].C_z_zw_cnt;
              				  C_z_skbj_cnt[j] = oary[j].C_z_skbj_cnt;
              				  C_z_atd_cnt[j] = oary[j].C_z_atd_cnt;
              				  C_z_hs_cnt[j] = oary[j].C_z_hs_cnt;
              				  C_l_pl_cnt[j] = oary[j].C_l_pl_cnt;
              				  C_l_yjyc_cnt[j] = oary[j].C_l_yjyc_cnt;
              				  C_l_ad_cnt[j] = oary[j].C_l_ad_cnt;
              				  C_l_mjad_cnt[j] = oary[j].C_l_mjad_cnt;
              				  C_l_hs_cnt[j] = oary[j].C_l_hs_cnt;
              				  C_l_gly_cnt[j] = oary[j].C_l_gly_cnt;
              				  T_z_zw_cnt[j] = oary[j].T_z_zw_cnt;
              				  T_z_skbj_cnt[j] = oary[j].T_z_skbj_cnt;
              				  T_z_atd_cnt[j] = oary[j].T_z_atd_cnt;
              				  T_z_hs_cnt[j] = oary[j].T_z_hs_cnt;
              				  T_l_ad_cnt[j] = oary[j].T_l_ad_cnt;
              				  T_l_hs_cnt[j] = oary[j].T_l_hs_cnt;
              				  T_l_gly_cnt[j] = oary[j].T_l_gly_cnt;
              				  totalCnt[j] = parseInt(oary[j].C_z_pl_cnt,10) + parseInt(oary[j].C_z_ly_cnt,10) + parseInt(oary[j].C_z_zw_cnt,10) + parseInt(oary[j].C_z_skbj_cnt,10)
              				  		+ parseInt(oary[j].C_z_atd_cnt,10) + parseInt(oary[j].C_z_hs_cnt,10) + parseInt(oary[j].C_l_pl_cnt,10) + parseInt(oary[j].C_l_yjyc_cnt,10)
              				  		+ parseInt(oary[j].C_l_ad_cnt,10) + parseInt(oary[j].C_l_mjad_cnt,10) + parseInt(oary[j].C_l_hs_cnt,10) + parseInt(oary[j].C_l_gly_cnt,10)
              				  		+ parseInt(oary[j].T_z_zw_cnt,10) + parseInt(oary[j].T_z_skbj_cnt,10) + parseInt(oary[j].T_z_atd_cnt,10) + parseInt(oary[j].T_z_hs_cnt,10)
              				  		+ parseInt(oary[j].T_l_ad_cnt,10) + parseInt(oary[j].T_l_hs_cnt,10) + parseInt(oary[j].T_l_gly_cnt,10);
              				  if(oary[j].remark.length == 0){
              					  remark[j] = "";
              				  }else{
              					  remark[j] = oary[j].remark;
              				  }
              				  status[j] = "已减薄";
              			  }
              			  if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
              				  for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
      							   prd_id[i] = mtrl_box_id[i] = "";
       							   C_z_pl_cnt[i] = C_z_ly_cnt[i] = C_z_zw_cnt[i] = C_z_skbj_cnt[i] = C_z_atd_cnt[i] = C_z_hs_cnt[i]= "";
       							   C_l_pl_cnt[i] = C_l_yjyc_cnt[i] = C_l_ad_cnt[i] = C_l_mjad_cnt[i] = C_l_hs_cnt[i] = C_l_gly_cnt[i] = "";
       							   T_z_zw_cnt[i] = T_z_skbj_cnt[i] = T_z_atd_cnt[i] = T_z_hs_cnt[i] = "";
       							   T_l_ad_cnt[i] = T_l_hs_cnt[i] = T_l_gly_cnt[i] = totalCnt[i] ="";
       							   status[i] = remark[i] = "";
              				  }
              			  }
//           			   }
           			   var printObj = {
           				   printTyp : printTyp,
           				   procId : proc_id,
 						   stdQty : outObject.std_qty,
 						   cusId : outObject.cus_id,
 						   soId : outObject.so_id,
 						   woId : outObject.wo_id,
 						   boxId : outObject.box_id,
 						   toThickness : outObject.to_thickness,
 						   prdCnt : outObject.prd_cnt,
 						   modelCnt : outObject.model_cnt,
 						   totalModelCnt : totalModelCnt,
 						   mdlId : mdlId,
 						   prdAry : prd_id,
 						   mtrlBoxAry : mtrl_box_id,
 						   czPlAry : C_z_pl_cnt,
 						   czLyAry : C_z_ly_cnt,
 						   czZwAry : C_z_zw_cnt,
 						   czSkbjAry : C_z_skbj_cnt,
 						   czAtdAry : C_z_atd_cnt,
 						   czHsAry : C_z_hs_cnt,
 						   clPlAry : C_l_pl_cnt,
 						   clYjycAry : C_l_yjyc_cnt,
 						   clAdAry : C_l_ad_cnt,
 						   clMjadAry : C_l_mjad_cnt,
 						   clHsAry : C_l_hs_cnt,
 						   clGlyAry : C_l_gly_cnt,
 						   tzZwAry : T_z_zw_cnt,
 						   tzSkbjAry : T_z_skbj_cnt,
 						   tzAtdAry : T_z_atd_cnt,
 						   tzHsAry : T_z_hs_cnt,
 						   tlAdAry : T_l_ad_cnt,
 						   tlHsAry : T_l_hs_cnt,
 						   tlGlyAry : T_l_gly_cnt,
 						   totalAry : totalCnt,
 						   statusAry : status,
 						   remarkAry : remark
           			   };
           			   label.PrintJBQA098(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
       			   }else if(proc_id == "DMQA"){// 098DMQA和028,071,088客户的出货信息表格式一致
           			   var oary = outObject.prd_cnt > 1 ? outObject.oaryC : [outObject.oaryC];
 					   var yjyc_cnt = [],sy_cnt = [],C_hs_cnt = [],C_ad_cnt = [],
 					   		C_td_cnt = [],C_shbj_cnt = [],C_ps_cnt = [],C_zw_cnt = [],C_ly_cnt = [],
 					   		C_other = [],T_hs_cnt = [],T_ad_cnt = [],T_td_cnt = [],
 					   		T_shbj_cnt = [],T_ps_cnt = [],T_zw_cnt = [],T_ly_cnt = [],T_other = [],
 					   		remark = [];
              			  for(var j=0; j<outObject.prd_cnt ; j++){
              				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
              				  yjyc_cnt[j] = oary[j].yjyc_cnt;
              				  sy_cnt[j] = oary[j].sy_cnt;
              				  C_hs_cnt[j] = oary[j].C_hs_cnt;
              				  C_ad_cnt[j] = oary[j].C_ad_cnt;
              				  C_td_cnt[j] = oary[j].C_td_cnt;
              				  C_shbj_cnt[j] = oary[j].C_shbj_cnt;
              				  C_ps_cnt[j] = oary[j].C_ps_cnt;
              				  C_zw_cnt[j] = oary[j].C_zw_cnt;
              				  C_ly_cnt[j] = oary[j].C_ly_cnt;
              				  C_other[j] = oary[j].C_other;
              				  T_hs_cnt[j] = oary[j].T_hs_cnt;
              				  T_ad_cnt[j] = oary[j].T_ad_cnt;
              				  T_td_cnt[j] = oary[j].T_td_cnt;
              				  T_shbj_cnt[j] = oary[j].T_shbj_cnt;
              				  T_ps_cnt[j] = oary[j].T_ps_cnt;
              				  T_zw_cnt[j] = oary[j].T_zw_cnt;
              				  T_ly_cnt[j] = oary[j].T_ly_cnt;
              				  T_other[j] = oary[j].T_other;
              				  if(oary[j].remark.length == 0){
          						 remark[j] = "";
              				  }else{
          						 remark[j] = oary[j].remark;
              				  }
              			  }
              			  if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
              				  for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
              					  prd_id[i] = "";
              					  yjyc_cnt[i] = sy_cnt[i] = "";
              					  C_hs_cnt[i] = C_ad_cnt[i] = C_td_cnt[i] = C_shbj_cnt[i] = C_ps_cnt[i] = C_zw_cnt[i] = C_ly_cnt[i] = C_other[i] = "";
              					  T_hs_cnt[i] = T_ad_cnt[i] = T_td_cnt[i] = T_shbj_cnt[i] = T_ps_cnt[i] = T_zw_cnt[i] = T_ly_cnt[i] = T_other[i] = "";
              					  remark[i] = "";
              				  }
              			  }
//              		   }
              		   var printObj = {
              			   printTyp : printTyp,
              			   procId : proc_id,
						   stdQty : outObject.std_qty,
						   cusId : outObject.cus_id,
						   woId : outObject.wo_id,
						   boxId : outObject.box_id,
						   toThickness : outObject.to_thickness,
						   prdCnt : outObject.prd_cnt,
						   modelCnt : outObject.model_cnt,
						   totalModelCnt : totalModelCnt,
						   whole_def_cnt : parseInt(outObject.l_def_cnt,10) + parseInt(outObject.p_def_cnt,10)+ parseInt(outObject.g_yjyc_cnt,10),
						   whole_yield : outObject.whole_yield,
						   checkUser : checkUser,
						   mdlId : mdlId,
						   prdAry : prd_id,
						   yjycAry : yjyc_cnt,
						   syAry : sy_cnt,
						   cHsAry : C_hs_cnt,
						   cAdAry : C_ad_cnt,
						   cTdAry : C_td_cnt,
						   cShbjAry : C_shbj_cnt,
						   cPsAry : C_ps_cnt,
						   cZwAry : C_zw_cnt,
						   cLyAry : C_ly_cnt,
						   cOtherAry : C_other,
						   tHsAry : T_hs_cnt,
						   tAdAry : T_ad_cnt,
						   tTdAry : T_td_cnt,
						   tShbjAry : T_shbj_cnt,
						   tPsAry : T_ps_cnt,
						   tZwAry : T_zw_cnt,
						   tLyAry : T_ly_cnt,
						   tOtherAry : T_other,
						   remarkAry : remark
       		        	};
              		   label.PrintDMQA098(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
            	   }
        	   }else if(outObject.cus_id == "115"){//115客户
       			   var oary = outObject.prd_cnt > 1 ? outObject.oary115 : [outObject.oary115];
       			   var l_llhx_cnt = [],l_yjyc_cnt = [],l_atd_cnt = [],l_pl_cnt = [],l_hs_cnt = [],l_wj_cnt = [],
       			   		l_other_cnt = [],lCnt = [],z_hs_cnt = [],z_atd_cnt = [],z_ssbj_cnt = [],z_lw_cnt = [],
       			   		z_ps_cnt = [],z_ly_cnt = [],z_wj_cnt = [],z_pit_cnt = [], zCnt = [],status = [],remark = [];
          			  for(var j=0; j<outObject.prd_cnt ; j++){
          				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
          				  mtrl_box_id[j] = oary[j].mtrl_box_id_fk;
          				  l_llhx_cnt[j] = oary[j].l_llhx_cnt;
          				  l_yjyc_cnt[j] = oary[j].l_yjyc_cnt;
          				  l_atd_cnt[j] = oary[j].l_atd_cnt;
          				  l_pl_cnt[j] = oary[j].l_pl_cnt;
          				  l_hs_cnt[j] = oary[j].l_hs_cnt;
          				  l_wj_cnt[j] = oary[j].l_wj_cnt;
          				  l_other_cnt[j] = oary[j].l_other_cnt;
          				  lCnt[j] = parseInt(oary[j].l_llhx_cnt,10) + parseInt(oary[j].l_yjyc_cnt,10) + parseInt(oary[j].l_atd_cnt,10)
  						 		+ parseInt(oary[j].l_pl_cnt,10) + parseInt(oary[j].l_hs_cnt,10) + parseInt(oary[j].l_wj_cnt,10)
  						 		+ parseInt(oary[j].l_other_cnt,10);
          				  z_hs_cnt[j] = oary[j].z_hs_cnt;
          				  z_atd_cnt[j] = oary[j].z_atd_cnt;
          				  z_ssbj_cnt[j] = oary[j].z_ssbj_cnt;
          				  z_lw_cnt[j] = oary[j].z_lw_cnt;
          				  z_ps_cnt[j] = oary[j].z_ps_cnt;
          				  z_ly_cnt[j] = oary[j].z_ly_cnt;
          				  z_wj_cnt[j] = oary[j].z_wj_cnt;
          				  z_pit_cnt[j] = oary[j].z_pit_cnt;
          				  zCnt[j] = parseInt(oary[j].z_hs_cnt,10) + parseInt(oary[j].z_atd_cnt,10) + parseInt(oary[j].z_ssbj_cnt,10)
  						 		+ parseInt(oary[j].z_lw_cnt,10) + parseInt(oary[j].z_ps_cnt,10) + parseInt(oary[j].z_ly_cnt,10)
  						 		+ parseInt(oary[j].z_wj_cnt,10) + parseInt(oary[j].z_pit_cnt,10);
          				  if(oary[j].remark.length == 0){
          					  remark[j] = "";
          				  }else{
          					  remark[j] = oary[j].remark;
          				  }
          				  if(proc_id == "JBQA"){
          					  status[j] = "已减薄";
          				  }else if(proc_id == "DMQA"){
          					  status[j] = "已镀膜";
          				  }
          			  }
          			  if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
          				  for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
          					  prd_id[i] = mtrl_box_id[i] = "";
          					  l_llhx_cnt[i] = l_yjyc_cnt[i] = l_atd_cnt[i] = l_pl_cnt[i] = l_hs_cnt[i] = l_wj_cnt[i] = l_other_cnt[i] = lCnt[i] = "";
          					  z_hs_cnt[i] = z_atd_cnt[i] = z_ssbj_cnt[i] = z_lw_cnt[i] = z_ps_cnt[i] = z_ly_cnt[i] = z_wj_cnt[i] = z_pit_cnt[i] = zCnt[i] = "";
          					  status[i] = remark[i] = "";
          				  }
          			  }
//          		   }
       			   var printObj = {
       					   printTyp : printTyp,
       					   procId : proc_id,
       					   stdQty : outObject.std_qty,
       					   cusId : outObject.cus_id,
       					   woId : outObject.wo_id,
       					   soId : outObject.so_id,
       					   boxId : outObject.box_id,
       					   toThickness : outObject.to_thickness,
       					   prdCnt : outObject.prd_cnt,
       					   mdlId : mdlId,
       					   prdAry : prd_id,
       					   mtrlBoxAry : mtrl_box_id,
       					   lLlhxAry : l_llhx_cnt,
       					   lYjycAry : l_yjyc_cnt,
       					   lAtdAry : l_atd_cnt,
       					   lPlAry : l_pl_cnt,
       					   lHsAry : l_hs_cnt,
       					   lWjAry : l_wj_cnt,
       					   lOtherAry : l_other_cnt,
       					   lCntAry : lCnt,
       					   zHsAry : z_hs_cnt,
       					   zAtdAry : z_atd_cnt,
       					   zSsbjAry : z_ssbj_cnt,
       					   zLwAry : z_lw_cnt,
       					   zPsAry : z_ps_cnt,
       					   zLyAry : z_ly_cnt,
       					   zWjAry : z_wj_cnt,
       					   zPitAry : z_pit_cnt,
       					   zCntAry : zCnt,
       					   statusAry : status,
       					   remarkAry : remark
       			   };
       			   label.PrintJBorDMQA115(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
        	   }else{//other 华南客户
       			   var oary = outObject.prd_cnt > 1 ? outObject.oaryHN : [outObject.oaryHN];
       			   var l_hnbl_cnt = [],l_llhx_cnt = [],l_atd_cnt = [],l_ps_cnt = [],l_lw_cnt = [],l_hs_cnt = [],l_zw_cnt = [],l_other = [],
       			   		z_d_atd_cnt = [],z_d_hs_cnt = [],z_d_ssbj_cnt = [],z_d_zw_cnt = [],z_d_ly_cnt = [],
       			   		z_ps_cnt = [],z_lw_cnt = [],z_ly_cnt = [],z_ls_cnt = [], z_fs_cnt = [],z_other = [],
       			   		status = [],remark = [];
          			  for(var j=0; j<outObject.prd_cnt ; j++){
          				  prd_id[j] = oary[j].fab_sn;// prd_seq_id->fab_sn
          				  l_hnbl_cnt[j] = oary[j].l_hnbl_cnt;
						  l_llhx_cnt[j] = oary[j].l_llhx_cnt;
						  l_atd_cnt[j] = oary[j].l_atd_cnt;
						  l_ps_cnt[j] = oary[j].l_ps_cnt;
						  l_lw_cnt[j] = oary[j].l_lw_cnt;
						  l_hs_cnt[j] = oary[j].l_hs_cnt;
						  l_zw_cnt[j] = oary[j].l_zw_cnt;
						  l_other[j] = oary[j].l_other;
						  z_d_atd_cnt[j] = oary[j].z_d_atd_cnt;
						  z_d_hs_cnt[j] = oary[j].z_d_hs_cnt;
						  z_d_ssbj_cnt[j] = oary[j].z_d_ssbj_cnt;
						  z_d_zw_cnt[j] = oary[j].z_d_zw_cnt;
						  z_d_ly_cnt[j] = oary[j].z_d_ly_cnt;
						  z_ps_cnt[j] = oary[j].z_ps_cnt;
						  z_lw_cnt[j] = oary[j].z_lw_cnt;
						  z_ly_cnt[j] = oary[j].z_ly_cnt;
						  z_ls_cnt[j] = oary[j].z_ls_cnt;
						  z_fs_cnt[j] = oary[j].z_fs_cnt;
						  z_other[j] = oary[j].z_other;
          				  if(oary[j].remark.length == 0){
          					  remark[j] = "";
          				  }else{
          					  remark[j] = oary[j].remark;
          				  }
          				  if(proc_id == "JBQA"){
          					  status[j] = "已减薄";
          				  }else if(proc_id == "DMQA"){
          					  status[j] = "已镀膜";
          				  }
          			  }
          			  if(parseInt(outObject.prd_cnt,10) < parseInt(outObject.std_qty,10)){
          				  for(var i=parseInt(outObject.prd_cnt,10) ; i<parseInt(outObject.std_qty,10);i++){
          					  prd_id[i] = "";
          					  l_hnbl_cnt[i] = l_llhx_cnt[i] = l_atd_cnt[i] = l_ps_cnt[i] = l_lw_cnt[i] = l_hs_cnt[i] = l_zw_cnt[i] = l_other[i] = "";
          					  z_d_atd_cnt[i] = z_d_hs_cnt[i] = z_d_ssbj_cnt[i] = z_d_zw_cnt[i] = z_d_ly_cnt[i] = "";
          					  z_ps_cnt[i] = z_lw_cnt[i] = z_ly_cnt[i] = z_ls_cnt[i] = z_fs_cnt[i] = z_other[i] = "";
          					  status[i] = remark[i] = "";
          				  }
          			  }
//          		   }
       			   var printObj = {
       					   printTyp : printTyp,
       					   procId : proc_id,
       					   stdQty : outObject.std_qty,
       					   cusId : outObject.cus_id,
       					   woId : outObject.wo_id,
       					   soId : outObject.so_id,
       					   actSoId : outObject.act_so_id,
       					   boxId : outObject.box_id,
       					   toThickness : outObject.to_thickness,
       					   prdCnt : outObject.prd_cnt,
						   modelCnt : outObject.model_cnt,
						   totalModelCnt : totalModelCnt,
						   whole_def_cnt : parseInt(outObject.d_def_cnt,10) + parseInt(outObject.p_def_cnt,10),
						   whole_yield : outObject.whole_yield,
						   checkUser : checkUser,
       					   mdlId : mdlId,
       					   prdAry : prd_id,
       					   lHnblAry : l_hnbl_cnt,
       					   lLlhxAry : l_llhx_cnt,
       					   lAtdAry : l_atd_cnt,
       					   lPsAry : l_ps_cnt,
       					   lLwAry : l_lw_cnt,
       					   lHsAry : l_hs_cnt,
       					   lZwAry : l_zw_cnt,
       					   lOther : l_other,
       					   zdAtdAry : z_d_atd_cnt,
       					   zdHsAry : z_d_hs_cnt,
       					   zdSsbjAry : z_d_ssbj_cnt,
       					   zdZwAry : z_d_zw_cnt,
       					   zdLyAry : z_d_ly_cnt,
       					   zPsAry : z_ps_cnt,
       					   zLwAry : z_lw_cnt,
       					   zLyAry : z_ly_cnt,
       					   zLsAry : z_ls_cnt,
       					   zFsAry : z_fs_cnt,
       					   zOther : z_other,
       					   statusAry : status,
       					   remarkAry : remark
       			   };
       			   label.PrintJBorDMQAHN(controlsQuery.print2Select.find("option:selected").text(),JSON.stringify(printObj));
        	   }
    	   }
       },
       getPrdRwCnt : function(isIssue){
           var inObj,
               outObj,
               out_box_id;

           //Get box info
           out_box_id = controlsQuery.okOutCrrTxt.val();
           if(!out_box_id){
               showErrorDialog("","请输入转入箱号！");
               return false;
           }

           inObj = {
               trx_id      : VAL.T_XPINQBOX,
               action_flg  : 'D'           ,
               box_id      : out_box_id
           };
           outObj = comTrxSubSendPostJson(inObj,'N');
           if (outObj.rtn_code == VAL.NORMAL) {
           }
           if(outObj.rtn_code == VAL.NORMAL && outObj.oary5_cnt != 0){
        	   var oary = outObj.oary5_cnt > 1? outObj.oary5 : [outObj.oary5];
        	   var msg="当前箱号[" +out_box_id+ "]";
        	   for(var i=0; i<outObj.oary5_cnt;i++){
        		   msg = msg+",["+oary[i].prd_seq_id+"]已重工["+oary[i].rwk_cnt+"]次";
        	   }
        	   btnQuery.f8.showCallBackWarnningDialog({
               	errMsg : msg +"!",
                   callbackFn : function(data) {
	               	   if(data.result){
	               		   btnFunc.f8_func(isIssue,out_box_id);
	               	   }
	        	   }
        	   });
           }else{
        	   btnFunc.f8_func(isIssue,out_box_id);
           }
       },
       changeColor : function(){
   		var ids = $("#wipInfoGrd").jqGrid('getDataIDs');
   		var prd_grade;
		for( var i=0 ; i<ids.length ; i++ ){
			var curRowData = $("#wipInfoGrd").jqGrid('getRowData', ids[i]);
			prd_grade = ($.trim(curRowData['prd_grade'])) ? $.trim(curRowData['prd_grade']) : $.trim(curRowData['pv_prd_grade']);
			if(prd_grade == "C1"){
				$("#"+ids[i]).css("background","red");
			}
		}
       },
       layoutChg : function(layout){
       	var letterCol = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'];
       	var i = parseInt(layout.substring(0,layout.indexOf('*')),10);
       	var j = parseInt(layout.substring(layout.indexOf('*')+1),10);
       	// return (letterCol[j-1]+""+i);
           return (i + "*" +j);
       },
       addPositionView : function(){
           var crGrid = controlsQuery.mainGrd.defectGrd;
           var ids = crGrid.jqGrid('getDataIDs');
           for(var i = 0;i < ids.length; i++){
           	var layout = crGrid.jqGrid("getCell",ids[i],"position");
           	crGrid.jqGrid("setCell",ids[i],"positionview",toolFunc.layoutChg(layout));
           }
       }

    };

    /**
     * All button click function
     * @type {Object}
     */
    var btnFunc = {
        //Query
        f1_func : function(){

            var inObj,
                outObj,
                lot_id,
                prd_id,
                box_id,
                mtrl_box_id,
                pv_box_id,
                wo_id,
                ope_id,
                ope_ver,
                tool_id,
                ope_info,
                ope_info_ary;

            tool_id = $.trim(controlsQuery.toolSelect.val());
            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];

            ope_ver = ope_info_ary[1];

            inObj = {
                trx_id : VAL.T_XPIWIPEQ,
                cr_ope_id  : ope_id,
                cr_ope_ver : ope_ver,
                tool_id    : tool_id
            };

            inObj.action_flg = "A";

            wo_id = controlsQuery.woIdSelect.val();
            if(wo_id){
                inObj.wo_id = wo_id;
            }

            lot_id = $.trim(controlsQuery.lotIdInput.val());
            if(lot_id){
                inObj.lot_id = lot_id;
            }

            prd_id = $.trim(controlsQuery.prdIdInput.val());
            if(prd_id){
                inObj.prd_id = prd_id;
                inObj.action_flg = "C";
            }

            mtrl_box_id = $.trim(controlsQuery.ppboxIdTxt.val());//来料箱号
            if(mtrl_box_id){
                inObj.mtrl_box_id = mtrl_box_id;
            }

            box_id = $.trim(controlsQuery.prdInBoxTxt.val());
            if(box_id){
                inObj.box_id = box_id;
            }

            pv_box_id = $.trim(controlsQuery.moveInBoxTxt.val()); //入账箱号
            if(pv_box_id){
                inObj.pv_box_id = pv_box_id;
            }
            $("#wipInfoGrd").jqGrid("clearGridData");
            outObj = comTrxSubSendPostJson(inObj);
            console.log(outObj.oary_prd);
            if (outObj.rtn_code == VAL.NORMAL) {
            	if(outObj.tbl_cnt == 0){
            		if(!skipDialog_flg){
            			showErrorDialog("","查询无记录");
            		}
            		skipDialog_flg = false;
            		return;
            	}

                for(var i = 0; i < outObj.oary_prd.length;i++){
                    outObj.oary_prd[i].def_count = outObj.oary_prd[i].prd_std_qty - outObj.oary_prd[i].prd_qty;
                }

            	toolFunc.enhanceGridOaryArr(outObj.oary_prd);
        		setGridInfo(outObj.oary_prd, "#wipInfoGrd",true);
        		if (wo_id || lot_id || mtrl_box_id || box_id || pv_box_id){
            		if (outObj.tbl_cnt == 1){
                        toolFunc.showGRDButton(outObj.oary_prd.cus_id);
                        toolFunc.get_all_defect_func(outObj.oary_prd.cus_id);;
            		}else {
                        toolFunc.showGRDButton(outObj.oary_prd[0].cus_id);
                        toolFunc.get_all_defect_func(outObj.oary_prd[0].cus_id);
            		}
        		}else{
	        		controlsQuery.gradeSelectDiv.empty();
        		}
        		 toolFunc.changeColor();
        	}
        },

        f1_query_wait_func : function(){
            var inObj,
                outObj,
                lot_id,
                prd_id,
                box_id,
                mtrl_box_id,
                pv_box_id,
                wo_id,
                ope_id,
                ope_ver,
                tool_id,
                proc_id,
                ope_info,
                ope_info_ary;

            tool_id = $.trim(controlsQuery.toolSelect.val());
            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];
            ope_ver = ope_info_ary[1];
            proc_id = ope_info_ary[2];

//            if (controlsQuery.wip_wait_chk[0].checked){
//            	checkFlg = "Y";
//            }else {
//            	checkFlg = "N";
//            }

            inObj = {
                trx_id : VAL.T_XPIWIPEQ,
                cr_ope_id  : ope_id,
                cr_ope_ver : ope_ver,
                cr_proc_id : proc_id,
                tool_id    : tool_id
            };
            inObj.action_flg = "W";

            wo_id = controlsQuery.woIdSelect.val();
            if(wo_id){
                inObj.wo_id = wo_id;
            }

            lot_id = $.trim(controlsQuery.lotIdInput.val());
            if(lot_id){
                inObj.lot_id = lot_id;
            }

            prd_id = $.trim(controlsQuery.prdIdInput.val());
            if(prd_id){
                inObj.prd_id = prd_id;
                inObj.action_flg = "C";
            }

            mtrl_box_id = $.trim(controlsQuery.ppboxIdTxt.val());//来料箱号
            if(mtrl_box_id){
                inObj.mtrl_box_id = mtrl_box_id;
            }

            box_id = $.trim(controlsQuery.prdInBoxTxt.val());
            if(box_id){
                inObj.box_id = box_id;
            }

            pv_box_id = $.trim(controlsQuery.moveInBoxTxt.val()); //入账箱号
            if(pv_box_id){
                inObj.pv_box_id = pv_box_id;
            }

            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
            	if(outObj.tbl_cnt == 0){
            		showErrorDialog("","查询无记录");
            		return;
            	}
            	toolFunc.enhanceGridOaryArr(outObj.oary_prd);
        		setGridInfo(outObj.oary_prd, "#wipInfoGrd",true);
        		 toolFunc.changeColor();
        		if ( wo_id || lot_id || mtrl_box_id || box_id || pv_box_id){
            		if (outObj.tbl_cnt == 1){
                        toolFunc.showGRDButton(outObj.oary_prd.cus_id);
                        toolFunc.get_all_defect_func(outObj.oary_prd.cus_id);
            		}else {
                        toolFunc.showGRDButton(outObj.oary_prd[0].cus_id);
                        toolFunc.get_all_defect_func(outObj.oary_prd[0].cus_id);
            		}
        		}else{
	        		controlsQuery.gradeSelectDiv.empty();
        		}
            }
        },
        //Process start
        process_start_func : function(){
            var i,
                crGrid,
                inObj,
                outObj,
                ids,
                idLen,
                rowData,
                prd_stat,
                row_box_id,
                process_user,
                sht_id,
                iary_sht_id = [];

            crGrid = controlsQuery.mainGrd.grdId;
            ids = crGrid.jqGrid('getGridParam','selarrrow');
            idLen = ids.length;
            if(0 === idLen){
                showErrorDialog("","请勾选需要开始制程的产品ID！");
                return false;
            }

            for( i = 0; i < idLen; i++ ){
                rowData = crGrid.jqGrid("getRowData", ids[i]);
                sht_id = $.trim(rowData['prd_seq_id']);
                prd_stat = $.trim(rowData['prd_stat']);
                row_box_id = $.trim(rowData['box_id']);

                if(prd_stat !== "INPR"){
                    showErrorDialog('',sht_id + '尚未入账，请先输入入账箱号做点击入账！');
                    return false;
                }

                if(row_box_id){
                    showErrorDialog("", sht_id + "已经转入" + row_box_id + "请勿重复操作！");
                    return false;
                }

                process_user = $.trim(rowData['process_user']);
                if(process_user){
                    showErrorDialog("", sht_id + "已经由" + process_user + "点击开始,请勿重复操作！");
                    return false;
                }
                //crGrid.jqGrid('setSelection',ids[i]);
                iary_sht_id.push(rowData['prd_seq_id']);
            }

            inObj = {
                trx_id : VAL.T_XPSTARTPROC ,
                act_flg : 'G',
                process_user : VAL.EVT_USER,
                prd_cnt  : idLen,
                prd_ids  : "'" + iary_sht_id.join("','") + "'"
            };

            if(gIsJBPG){
//                root_tool_id = controlsQuery.buffToolGrpSelect.val();
//                if(!root_tool_id){
//                    showErrorDialog("","请先选择抛光设备组--抛光设备！");
//                    return false;
//                }
                inObj.tool_id = controlsQuery.tftToolSelect.val();
                inObj.tft_buff_tool = inObj.tool_id;
                inObj.cf_buff_tool = controlsQuery.cfToolSelect.val();
            }else{
                inObj.tool_id = controlsQuery.toolSelect.val();
            }
            if (!inObj.tool_id) {
                showErrorDialog("","设备ID不能为空！");
                return false;
            }
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                for( i = 0; i < idLen; i++ ){
                    rowData = crGrid.jqGrid("getRowData", ids[i]);
                    rowData['process_user'] = VAL.EVT_USER;
                    rowData['start_time'] = outObj.start_time;
                    rowData['cr_tool_id_fk'] = outObj.tool_id;
                    toolFunc.enhanceGridOaryArr(rowData);
                    crGrid.jqGrid('setRowData', ids[i], rowData);
                }
            }
        },
        //Single Panel move in产品转入
        f6_func_s : function(id){
            var i,
                crGrid,
                ids,
                idLen,
                rowData,
                row_box_id,
                out_box_id,
                iary = [],
                crCnt = 0,
                maxCnt = 0,
                process_user,
                tmpIary = {},
                ope_info,
                ope_info_ary,
                auth_flg,
                inflg,
                user_func,
                woflg = "",
                yxflg = "",
                prd_ids = [],
                prd_grade;


            if( !id ){
                return false;
            }

            auth_flg = "Y";//管控权限
            crGrid = controlsQuery.mainGrd.grdId;
            rowData = crGrid.jqGrid("getRowData", id);
            idLen = 1;
            //Get box info
            out_box_id = controlsQuery.okOutCrrTxt.val();
            crCnt = parseInt(controlsQuery.OkBoxCurCntTxt.val(), 10);
            maxCnt = parseInt(controlsQuery.OkBoxMaxCntTxt.val(), 10);
            //Max count check
            if(maxCnt < crCnt + idLen){
                showErrorDialog("","箱子["+out_box_id+
                    "]当前数量为["+crCnt+"],最大容量为["+maxCnt+
                    "],不能再放入["+idLen+"]片产品！");
                return false;
            }

            if(!out_box_id){
                showErrorDialog("","请输入转入箱号！");
                return false;
            }
            if(rowData.cus_id=="068"&&rowData.th_judge_flg.substring(22,24)=="NG"&&rowData.mtrl_box_id_fk==out_box_id){
            	showErrorDialog("","此玻璃厚度超规，无法转入原箱");
            	return false;
            }

//          if(!confirm("您共选择了"+idLen+"片玻璃，出账箱号为"+out_box_id+",确认转入？")){
//            	return false;
//            }
            //获取站点信息
            ope_info = $.trim(controlsQuery.opeSelect.val());
            ope_info_ary = ope_info.split("@");

                //获取ope_eff_flgTxt
                    inObj = {
                            trx_id      : VAL.T_XPAPLYWO,
                            action_flg  : 'Q'           ,
                            tbl_cnt     : 1             ,
                            iary : {
                                wo_id     : rowData['wo_id']
                            }
                        };
                        outObj = comTrxSubSendPostJson(inObj);
                        woflg=outObj.oary.ope_eff_flg;
                        yxflg=outObj.oary.vcr_flg;
//                process_user = $.trim(rowData['process_user']);
//                if(!process_user){
//                    showErrorDialog("", rowData['prd_seq_id'] + "尚未点击开始,请先点击开始！");
//                    return false;
//                }
                row_box_id = $.trim(rowData['box_id']);
                if(row_box_id){
                    showErrorDialog("",rowData['prd_seq_id'] + "已经转入" + row_box_id + "请勿重复操作！");
                    return false;
                }
                //判断入账箱号和转入箱号
//                if(ope_info_ary[2]=="JBPK"||ope_info_ary[2]=="JBQA"||ope_info_ary[2]=="DMQA"||ope_info_ary[2]=="DMDB"){
//           			if(rowData['pv_box_id_fk']!=out_box_id){
//                        showErrorDialog("", "转入箱号与入账箱号不符，请检查！");
//                        return false;
//        			}
//                }

                prd_grade = ($.trim(rowData['prd_grade'])) ? $.trim(rowData['prd_grade']) : $.trim(rowData['pv_prd_grade']);
                if(rowData.cus_id=="068"&&prd_grade=="S"&&rowData.mtrl_box_id_fk==out_box_id){
                	showErrorDialog("","此玻璃等级为S，无法转入原箱");
                	return false;
                }
                //check 原箱返
                 if(woflg=="Y"||yxflg=="Y"){
                 	if(ope_info_ary[2]=="PGZQ"||ope_info_ary[2]=="DMZQ"){
                 			if(rowData['mtrl_box_id_fk']!=out_box_id&&prd_grade=="OK"){
                                 showErrorDialog("", "转入箱号与来料箱号不符，请检查！");
                                 return false;
                 			}
                 	}
                 }
                 //end
                 //判断转入站点
                 if(ope_info_ary[2]=="JBQA"){
                	 inflg="A";
                 }else if(ope_info_ary[2]=="JBPK"){
                	 inflg="B";
                 }else if(ope_info_ary[2]=="DMQA"){
                	 inflg="C";
                 }else{
                	 inflg="D";
                 }

                 //end
                tmpIary = {
                    prd_seq_id : rowData['prd_seq_id'],
                    judge_result : prd_grade,
                    tft_buff_time : $.trim(rowData['tft_buff_time']),
                    cf_buff_time : $.trim(rowData['cf_buff_time']),
                    tft_buff_tool : $.trim(rowData['tft_buff_tool']),
                    cf_buff_tool : $.trim(rowData['cf_buff_tool']),
                    isQaIns : $.trim(rowData['qa_flg']) === "Y",
                    mtrl_slot_no_fk : $.trim(rowData['mtrl_slot_no_fk'])
                };

                if(gIsJBPG){
                    if (!(tmpIary.tft_buff_tool && tmpIary.cf_buff_tool)) {
                        showErrorDialog("", "请先登记抛光信息！");
                        return false;
                    }
                }

                iary.push(tmpIary);
                tmpIary = {};
                prd_ids.push(rowData['prd_seq_id']);


           //检查权限
//            user_func = "F_" + ope_info_ary[2];
//        	if(checkUserFunc(user_func)){
        		auth_flg = "Y";
//        	}else{
//        		auth_flg = "N";
//        	}
        	tool_id = controlsQuery.toolSelect.val();
        	if(!tool_id){
                showErrorDialog("", "设备代码不能为空！");
                return false;
            }

//            var boxObj = toolFunc.getBoxInfo(out_box_id);
//            if(boxObj){
//            	if(boxObj.bnk_flg == "3"){
//                    btnQuery.f6.showCallBackWarnningDialog({
//                    	errMsg : "箱子" + out_box_id + "是在制保留箱，确定是否继续转入?",
//                        callbackFn : function(data) {
//                    	   if(data.result){
//                    		   btnFunc.prdIn_func(out_box_id,auth_flg,idLen,iary,prd_ids,ope_info_ary,ids,inflg,tool_id);
//                    	   }
//                        }
//                    });
//                    return;
//            	}else{
//            		btnFunc.prdIn_func(out_box_id,auth_flg,idLen,iary,prd_ids,ope_info_ary,ids,inflg,tool_id);
//            	}
//            }
        	btnFunc.prdIn_func(out_box_id,auth_flg,idLen,iary,prd_ids,ope_info_ary,id,inflg,tool_id,"S",woflg);
            return true;
        },
        //Panel move in产品转入
        f6_func : function(){
            var i,
                crGrid,
                ids,
                idLen,
                rowData,
                row_box_id,
                out_box_id,
                iary = [],
                crCnt = 0,
                maxCnt = 0,
                process_user,
                tmpIary = {},
                ope_info,
                ope_info_ary,
                auth_flg,
                inflg,
                user_func,
                woflg = "",
                yxflg = "",
                prd_ids = [],
                plant_id="",
                last_prd_ext_2="",
                last_prd_grade="",
                current_prd_ext_2="",
                current_prd_grade="",
                prd_grade;

            auth_flg = "Y";//管控权限
            crGrid = controlsQuery.mainGrd.grdId;
            ids = crGrid.jqGrid('getGridParam','selarrrow');
            idLen = ids.length;
            if(0 === idLen){
                showErrorDialog("","请勾选需要转入的产品ID！");
                return false;
            }
            // if(idLen>1){
            //     showErrorDialog("","每次只能转入一块玻璃！");
            //     return false;
            // }

            //Get box info
            out_box_id = controlsQuery.okOutCrrTxt.val();
            crCnt = parseInt(controlsQuery.OkBoxCurCntTxt.val(), 10);
            maxCnt = parseInt(controlsQuery.OkBoxMaxCntTxt.val(), 10);
            //Max count check
            if(maxCnt < crCnt + idLen){
                showErrorDialog("","箱子["+out_box_id+
                    "]当前数量为["+crCnt+"],最大容量为["+maxCnt+
                    "],不能再放入["+idLen+"]片产品！");
                return false;
            }

            if(!out_box_id){
                showErrorDialog("","请输入转入箱号！");
                return false;
            }

//          if(!confirm("您共选择了"+idLen+"片玻璃，出账箱号为"+out_box_id+",确认转入？")){
//            	return false;
//            }
            //获取站点信息
            ope_info = $.trim(controlsQuery.opeSelect.val());
            ope_info_ary = ope_info.split("@");

            for( i = 0; i < idLen; i++ ){
                rowData = crGrid.jqGrid("getRowData", ids[i]);



                //获取ope_eff_flgTxt
                if(i==0){
                    inObj = {
                            trx_id      : VAL.T_XPAPLYWO,
                            action_flg  : 'Q'           ,
                            tbl_cnt     : 1             ,
                            iary : {
                                wo_id     : rowData['wo_id']
                            }
                        };
                        outObj = comTrxSubSendPostJson(inObj);
                        woflg=outObj.oary.ope_eff_flg;
                        yxflg=outObj.oary.vcr_flg;
                        plant_id=outObj.oary.plant_id;
                }
                if(rowData.cus_id=="068"&&rowData.th_judge_flg.substring(22,24)=="NG"&&rowData.mtrl_box_id_fk==out_box_id &&"G6"==plant_id){
                	showErrorDialog("","此玻璃厚度超规，无法转入原箱");
                	return false;
                }
//                process_user = $.trim(rowData['process_user']);
//                if(!process_user){
//                    showErrorDialog("", rowData['prd_seq_id'] + "尚未点击开始,请先点击开始！");
//                    return false;
//                }
                row_box_id = $.trim(rowData['box_id']);
                if(row_box_id){
                    showErrorDialog("",rowData['prd_seq_id'] + "已经转入" + row_box_id + "请勿重复操作！");
                    return false;
                }
                //判断入账箱号和转入箱号
//                if(ope_info_ary[2]=="JBPK"||ope_info_ary[2]=="JBQA"||ope_info_ary[2]=="DMQA"||ope_info_ary[2]=="DMDB"){
//           			if(rowData['pv_box_id_fk']!=out_box_id){
//                        showErrorDialog("", "转入箱号与入账箱号不符，请检查！");
//                        return false;
//        			}
//                }

                prd_grade = ($.trim(rowData['prd_grade'])) ? $.trim(rowData['prd_grade']) : $.trim(rowData['pv_prd_grade']);
                if(rowData.cus_id=="068"&&prd_grade=="S"&&rowData.mtrl_box_id_fk==out_box_id &&"G6"==plant_id){
                	showErrorDialog("","此玻璃等级为S，无法转入原箱");
                	return false;
                }
                //check 原箱返
                 if(woflg=="Y"||yxflg=="Y"){
                 	if(ope_info_ary[2]=="PGZQ"||ope_info_ary[2]=="DMZQ"){
                 			if(rowData['mtrl_box_id_fk']!=out_box_id&&prd_grade=="OK"){
                                 showErrorDialog("", "转入箱号与来料箱号不符，请检查！");
                                 return false;
                 			}
                 	}
                 }

                 //华星原箱返(镀膜包装并是华星客户)
              /*  if("DMBZ" == rowData.cr_proc && "007" == rowData.cus_id){
                    if(rowData['mtrl_box_id_fk']!= out_box_id&&prd_grade=="OK"){
                        showErrorDialog("", "转入箱号与来料箱号不符，请检查！");
                        return false;
                    }
                }*/

                 //end
                 //判断转入站点
                 if(ope_info_ary[2]=="JBQA"){
                	 inflg="A";
                 }else if(ope_info_ary[2]=="JBPK"){
                	 inflg="B";
                 }else if(ope_info_ary[2]=="DMQA"){
                	 inflg="C";
                 }else{
                	 inflg="D";
                 }

                 //end
                tmpIary = {
                    prd_seq_id : rowData['prd_seq_id'],
                    judge_result : prd_grade,
                    tft_buff_time : $.trim(rowData['tft_buff_time']),
                    cf_buff_time : $.trim(rowData['cf_buff_time']),
                    tft_buff_tool : $.trim(rowData['tft_buff_tool']),
                    cf_buff_tool : $.trim(rowData['cf_buff_tool']),
                    isQaIns : $.trim(rowData['qa_flg']) === "Y",
                    mtrl_slot_no_fk : $.trim(rowData['mtrl_slot_no_fk'])
                };

                if(gIsJBPG){
                    if (!(tmpIary.tft_buff_tool && tmpIary.cf_buff_tool)) {
                        showErrorDialog("", "请先登记抛光信息！");
                        return false;
                    }
                }

                iary.push(tmpIary);
                tmpIary = {};
                prd_ids.push(rowData['prd_seq_id']);
            }


           //检查权限
//            user_func = "F_" + ope_info_ary[2];
//        	if(checkUserFunc(user_func)){
        		auth_flg = "Y";
//        	}else{
//        		auth_flg = "N";
//        	}
        	tool_id = controlsQuery.toolSelect.val();
        	if(!tool_id){
                showErrorDialog("", "设备代码不能为空！");
                return false;
            }

//            var boxObj = toolFunc.getBoxInfo(out_box_id);
//            if(boxObj){
//            	if(boxObj.bnk_flg == "3"){
//                    btnQuery.f6.showCallBackWarnningDialog({
//                    	errMsg : "箱子" + out_box_id + "是在制保留箱，确定是否继续转入?",
//                        callbackFn : function(data) {
//                    	   if(data.result){
//                    		   btnFunc.prdIn_func(out_box_id,auth_flg,idLen,iary,prd_ids,ope_info_ary,ids,inflg,tool_id);
//                    	   }
//                        }
//                    });
//                    return;
//            	}else{
//            		btnFunc.prdIn_func(out_box_id,auth_flg,idLen,iary,prd_ids,ope_info_ary,ids,inflg,tool_id);
//            	}
//            }
        	btnFunc.prdIn_func(out_box_id,auth_flg,idLen,iary,prd_ids,ope_info_ary,ids,inflg,tool_id,"G",woflg);
            return true;
        },
        prdIn_func :function(out_box_id,auth_flg,idLen,iary,prd_ids,ope_info_ary,ids,inflg,tool_id,func_flg,woflg){
        	var i,
        		inObj,
        		outObj,
                crGrid;
            crGrid = controlsQuery.mainGrd.grdId;
            inObj = {
                trx_id : VAL.T_XPPNLOUT ,
                box_id : out_box_id,
                evt_usr : VAL.EVT_USER,
                auth_flg : auth_flg,
                inflg : inflg,
                gchk_flg : gchk_flg,
                woflg : woflg,
                tool_id : tool_id,
                pnl_cnt : idLen,
                prd_ids : "'" + prd_ids.join("','") + "'",
                iary : iary
            };

            //没有权限，需检查箱子的设定代码是否一致
            if(auth_flg == "N"){
            	inObj.ope_id_fk = ope_info_ary[0];
            	inObj.ope_ver_fk = ope_info_ary[1];
            	inObj.ope_dsc = controlsQuery.opeSelect.find("option:selected").text();
            }
            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
            	if(func_flg=="G"){
                 for( i = 0; i < idLen; i++ ){
                    rowData = crGrid.jqGrid("getRowData", ids[i]);
                    rowData['box_id'] = out_box_id;
                    rowData['prd_grade'] = inObj.iary[i].judge_result;
                    rowData['end_user'] = inObj.evt_usr;
                    rowData['end_time'] = outObj.end_time;
                    rowData['evt_user'] = outObj.evt_user;
                    rowData['evt_timestamp'] = outObj.evt_timestamp;
                    rowData['slot_no'] = outObj.slot_no;
                    rowData['cr_tool_id_fk'] = outObj.tool_id;
                    toolFunc.enhanceGridOaryArr(rowData);
                    crGrid.jqGrid('setRowData', ids[i], rowData);
                 }
            	}else if(func_flg=="S"){
            		rowData = crGrid.jqGrid("getRowData", ids);
                    rowData['box_id'] = out_box_id;
                    rowData['prd_grade'] = inObj.iary[0].judge_result;
                    rowData['end_user'] = inObj.evt_usr;
                    rowData['end_time'] = outObj.end_time;
                    rowData['evt_user'] = outObj.evt_user;
                    rowData['evt_timestamp'] = outObj.evt_timestamp;
                    rowData['slot_no'] = outObj.slot_no;
                    rowData['cr_tool_id_fk'] = outObj.tool_id;
                    toolFunc.enhanceGridOaryArr(rowData);
                    crGrid.jqGrid('setRowData', ids, rowData);
            	}
                if(controlsQuery.okOutCrrTxt.val() == out_box_id){
                    controlsQuery.OkBoxCurCntTxt.val(outObj.prd_qty);
                    controlsQuery.OkBoxModeCntTxt.val(outObj.mode_cnt);
                }
                if(controlsQuery.yieldInfoDiv.is(':visible')){
                    toolFunc.syncYieldInfo(out_box_id, rowData['cr_proc']);
                }
                showMessengerSuccessDialog("转入成功!",1);
                crGrid.jqGrid("resetSelection");
            }
        },
        //产品取消转入
        f9_func : function(){
            var i,
                crGrid,
                inObj,
                outObj,
                ids,
                idLen,
                rowData,
                row_box_id,
                iary = [],
                tmpIary = {};

            crGrid = controlsQuery.mainGrd.grdId;
            ids = crGrid.jqGrid('getGridParam','selarrrow');
            idLen = ids.length;
            if(0 === idLen){
                showErrorDialog("","请勾选需要取消转入的产品ID！");
                return false;
            }
            for( i = 0; i < idLen; i++ ){
                rowData = crGrid.jqGrid("getRowData", ids[i]);
                row_box_id = $.trim(rowData['box_id']);
                if(!row_box_id){
                    showErrorDialog("",rowData['prd_seq_id'] + "尚未转入任何箱子，无法取消转入！");
                    return false;
                }

                tmpIary = {
                    prd_seq_id : rowData['prd_seq_id']
                };
                iary.push(tmpIary);
                tmpIary = {};
            }

            inObj = {
                trx_id : VAL.T_XPCNPNLOUT ,
                evt_usr : VAL.EVT_USER,
                pnl_cnt : idLen,
                iary : iary
            };

            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL) {
                for( i = 0; i < idLen; i++ ){
                    rowData = crGrid.jqGrid("getRowData", ids[i]);
                    rowData['box_id'] = "";
                    rowData['end_user'] = "";
                    rowData['end_time'] = "";
                    rowData['evt_user'] = outObj.evt_user;
                    rowData['evt_timestamp'] = outObj.evt_timestamp;
                    toolFunc.enhanceGridOaryArr(rowData);
                    crGrid.jqGrid('setRowData', ids[i], rowData);
                }

                if(controlsQuery.okOutCrrTxt.val() == outObj.box_id){
                    controlsQuery.OkBoxCurCntTxt.val(outObj.prd_qty);
                    controlsQuery.OkBoxModeCntTxt.val(outObj.mode_cnt);
                }

                showMessengerSuccessDialog("成功取消转入!",1);
            }
            return true;
        },
        //Box move out
        f8_func : function(isIssue,out_box_id){
            var inObj,
                outObj,
                root_tool_id,
                tool_id,
                swh_path_info,
                swh_path_info_ary,
                remark,
                ope_info,
                ope_info_ary,
                proc_id,
                qa_back_flg,
                judeUser,
                cntflg;
         /*   if(parseInt(controlsQuery.OkBoxCurCntTxt.val(), 10)<10){
            	if(checkUserFunc("F_1400_CNT_LOGOF","N")==false){
            		 showErrorDialog("","出账箱内玻璃数量小于10，无法出账");
            		return false;
            	}
            }*/
            judeUser= $.trim(controlsQuery.judgeUserID.val());

            ope_info = $.trim(controlsQuery.opeSelect.val());
            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];
            proc_id = ope_info_ary[2];

            //判断转入PGZQ/DMZQ， 校验 工单 G5，等级卡控，
            if(proc_id=="PGZQ"||proc_id=="DMZQ"){
           		 inObj = {
                            trx_id      : 'XPINQSHT',
                            action_flg  : 'C'           ,
                            box_id      : out_box_id
                            }
           		 var shtoutObj = comTrxSubSendPostJson(inObj);
           		 if(parseInt(shtoutObj.tbl_cnt, 10)>1){
           			 var prd_grade_target =shtoutObj.table[0].prd_grade;
           			 var prd_ext_2_target=shtoutObj.table[0].ext_2;
           			 var wo_id_temp =shtoutObj.table[0].wo_id_fk;
           			 var prd_id_target =shtoutObj.table[0].prd_seq_id;

           			 inObj = {
                             trx_id      : VAL.T_XPAPLYWO,
                             action_flg  : 'Q'           ,
                             tbl_cnt     : 1             ,
                             iary : {
                                 wo_id     : wo_id_temp
                             }
                         };
           			 outObj = comTrxSubSendPostJson(inObj);
                        var  plant_id=outObj.oary.plant_id;
		                if(plant_id=='G5'){

		           			 for(var i=1;i<parseInt(shtoutObj.tbl_cnt, 10);i++){
			           			if(prd_grade_target !== shtoutObj.table[i].prd_grade)
			           			 {
			           				 showErrorDialog("", "产品 "+prd_id_target +" 的等级 "+prd_grade_target+" 与 BOX 中"+shtoutObj.table[i].prd_seq_id+" 等级 "+shtoutObj.table[i].prd_grade+" 不一致");
			                            return false;
			           			 }
			           			 if(prd_ext_2_target=='G'&&prd_grade_target=='H'&&shtoutObj.table[i].ext_2=='H' &&shtoutObj.table[i].prd_grade=='H'){
			           				 showErrorDialog("", "降等品 "+prd_id_target+"不能与未降等品"+shtoutObj.table[i].prd_seq_id +"混装");
			           				 return false;
			           			 }
			           			 if(prd_ext_2_target=='H'&&prd_grade_target=='H'&&shtoutObj.table[i].ext_2=='G' &&shtoutObj.table[i].prd_grade=='H'){
			           				 showErrorDialog("", "降等品 "+shtoutObj.table[i].prd_seq_id+" 不能与未降等品 "+prd_id_target+" 混装");
			           				 return false;
			           			 }
		           			 }
		                }
           		 }
            }

            //华星包装原箱返回前台卡控


            //QA判退调用跳站功能
//            if(proc_id == "JBQA" || proc_id == "DMQA"){
//                qa_back_flg = "Y";
//            }else{
//                qa_back_flg = "N";
//            }
            //若返回站点为空则调用跳站流程
            if(!$.trim(controlsQuery.swhOutOpeSelect.val())){
            	qa_back_flg = "Y";
            }else{
            	qa_back_flg = "N";
            }
            //检查出入账玻璃数量
            if(proc_id == "JBPK"||proc_id=="JBQA"||proc_id=="DMQA"||proc_id=="DMDB"){
            	cntflg = "Y";
            }else{
            	cntflg = "N";
            }



            if(gIsJBPG){
//                root_tool_id = controlsQuery.buffToolGrpSelect.val();
//                if(!root_tool_id){
//                    showErrorDialog("","请先选择抛光设备组--抛光设备！");
//                    return false;
//                }
                tool_id = controlsQuery.tftToolSelect.val();
            }else{
                tool_id = controlsQuery.toolSelect.val();
            }
            if (!tool_id) {
                showErrorDialog("","设备ID不能为空！");
                return false;
            }
            remark = controlsQuery.remarkTxt.val();

            if (VAL.EVT_USER == "" || VAL.EVT_USER == null || VAL.EVT_USER == undefined){
            	showErrorDialog("","请重新登录！");
            	return false;
            }

            inObj = {
                trx_id  : VAL.T_XPMOVEOUT,
                evt_usr : VAL.EVT_USER,
                box_id  : out_box_id,
                tool_id : tool_id,
                remark  : remark,
                cntflg  : cntflg,
                gchk_flg : "N",
                judge_user:judeUser ,
                prd_grade:controlsQuery.gradeSelect.val()
            };

            if( true === isIssue ){
                inObj.issue_move_out = "Y";
            }

            if("checked" === controlsQuery.swhOpeChk.attr("checked")){
            	swh_path_info = $.trim(controlsQuery.swhPathSelect.val());
            	if(!swh_path_info){
                    showErrorDialog("","请选择异转站点！");
                    return false;
            	}
//                if(qa_back_flg == "N"){
//                    //获取重工工艺路线信息
//                    swh_path_info_ary = swh_path_info.split("@");
//                    inObj.swh_path_id = swh_path_info_ary[0];
//                    inObj.swh_path_ver = swh_path_info_ary[1];
//                }else{
//                    inObj.qa_back_flg = "Y";
//             	inObj.qa_back_flg = "Y";
            	swh_path_info_ary = swh_path_info.split("@");
            	inObj.swh_path_id = swh_path_info_ary[0];
            	inObj.swh_path_ver = swh_path_info_ary[1];
          	}
            inObj.wip_wait_ok = "false";


            outObj = comTrxSubSendPostJson(inObj);
            if (outObj.rtn_code == VAL.NORMAL && outObj.wip_wait_flg == "true"){
            	btnQuery.f8.showCallBackWarnningDialog({
                	errMsg : "箱子" + out_box_id + "是在制等待箱，确定是否继续出账?",
                    callbackFn : function(data) {
                	   if(data.result){
                		   inObj.wip_wait_ok = "true";
                		   outObj = comTrxSubSendPostJson(inObj);
                		   showMessengerSuccessDialog(out_box_id + "出账成功!",1);
                           controlsQuery.okOutCrrTxt.val("");
                           controlsQuery.OkBoxMaxCntTxt.val("");
                           controlsQuery.OkBoxCurCntTxt.val("");
                           controlsQuery.OkBoxModeCntTxt.val("");
                           skipDialog_flg = true;
                           btnFunc.f1_func();

                           controlsQuery.swhOpeChk.removeAttr("checked");
                	   }
            		}
            	});
            }else if (outObj.rtn_code == VAL.NORMAL) {
                showMessengerSuccessDialog(out_box_id + "出账成功!",1);
                controlsQuery.okOutCrrTxt.val("");
                controlsQuery.OkBoxMaxCntTxt.val("");
                controlsQuery.OkBoxCurCntTxt.val("");
                controlsQuery.OkBoxModeCntTxt.val("");
                skipDialog_flg = true;
                btnFunc.f1_func();

                if(controlsQuery.swhOpeChk.attr("checked") != "checked"){ //如果选择了异常转入站点则不打印
                	try{//打印时异常，只抛出异常信息，对话框也可消失
                        var boxObj = toolFunc.getBoxInfo(out_box_id);
                        if(proc_id == "PGZQ" && boxObj.mdl_cate_fk == "A"){
                        	if(boxObj.cus_id_fk=="058"){
                                showMessengerSuccessDialog(out_box_id + "正在打印出货标签...",1);
                                var printer = controlsQuery.print1Select.find("option:selected").text();
                                var ps = boxObj.mainT;
                                var mkr = "湖北鸿创";
                                var woId = boxObj.wo_id_fk;
                                var wocate = "";
                                if(boxObj.wo_cate=="ENG"){
                                	wocate = "试验品";
                                }else if(boxObj.wo_cate=="ONCE"){
                                	wocate = "ON CELL品";
                                }else if(boxObj.wo_cate=="PROD"){
                                	wocate = "量产品";
                                }else if(boxObj.wo_cate=="RD"){
                                	wocate = "研发品";
                                }else if(boxObj.wo_cate=="SAMP"){
                                	wocate = "样品";
                                }
                                var mtrlId = boxObj.th_mdl_id_fk;
                                var qty = boxObj.prd_qty + "*" + boxObj.smode_cnt + "=" + boxObj.mode_cnt;
                                var jgyq = boxObj.jgyq;
                                var date = boxObj.pln_cmp_date;
                                var outdate = boxObj.comp_date;
                                var boxId = out_box_id;
                                var thickness = boxObj.to_thickness;
                                var dir = boxObj.fhdir;
                                var user = boxObj.evt_user;
                                var note = boxObj.wo_dsc;
                                label.PrintHCT058(printer, ps, mkr, woId, wocate, mtrlId, qty,jgyq, date,
                                         outdate, boxId, thickness, dir, user,note);
                                var title = boxObj.shtT;
                                var shtAry = [];
                			    for(j=0; j<20; j++){
              				        shtAry[j] = "";
              			        }
                        		if( boxObj.prd_qty == 1){
          						    shtAry[0] = boxObj.table.prd_seq_id;
          					    }else if(boxObj.table.length > 1){
          						   for( i=0;i<boxObj.table.length;i++){
          							   shtAry[i] = boxObj.table[i].prd_seq_id;
          						   }
          					    }
                                label.PrintForHCT058ShtList(printer,title,JSON.stringify(shtAry),boxId);
                                var inTrxCRT = {
        		        				trx_id : "XPCRTNO",
        		        				action_flg : "C",
        		        				key_id : mtrlId,
        		        				type : "S"
        		        			};
        		        		 var outTrxCRT = comTrxSubSendPostJson(inTrxCRT);
        		            	 var str = mtrlId + "H" + outTrxCRT.serial_no;
        		            	 label.PrintQR( printer, str);
        		            	 label.PrintQRhc( printer);
                        	}else if(boxObj.cus_id_fk=="018"||boxObj.cus_id_fk=="088"){
                        		showMessengerSuccessDialog(out_box_id + "正在打印出货标签...",1);
                                var printer = controlsQuery.print1Select.find("option:selected").text();
                                var thickness = boxObj.to_thickness/2;
                                var time = boxObj.comp_date_all;
                                var qty = boxObj.prd_qty;
                                var userID = boxObj.evt_user;
                                var product = boxObj.th_mdl_id_fk;
                                var boxId = boxObj.ship_box_id;
                        		label.PrintHCToncell088(printer, boxId,thickness,time,qty,userID,product);

                        		var printer = controlsQuery.print1Select.find("option:selected").text();
                        		var soId = boxObj.so_id_fk;
                                var dateinfo = new Array();
                        		    dateinfo = boxObj.overdue_date.split("-");
                        		var overDate =  dateinfo[1] + "月" + dateinfo[2] + "日";
                        		var outwoId = boxObj.cus_info_fst;
                        		var qty = boxObj.prd_qty;
                        		var mdltyp = boxObj.guige;
                        		var cusId = boxObj.cus_dsc;
                        		var boxId = out_box_id;
                        		var pm = "LCD-TFT";
                        		//var blqty = boxObj.bl_cnt + "cell";
                        		var user = boxObj.evt_user;
                        	    var dateinfo1 = new Array();
                     		        dateinfo1 = boxObj.comp_date.split("/");
                     		    var date =  dateinfo1[1] + "月" + dateinfo1[2] + "日";
                        		var mdlId = boxObj.th_mdl_id_fk;
                        		var woId = boxObj.wo_id_fk;
                        		//var okyield = boxObj.okyield + "%";
                        		var okyield = "";
                        		var qauser = "HC0025";
                        		var bt = "薄化产品出货标签";
                        		var blqty = "";
                        		var grade = boxObj.prd_grade_fk;
                        		if(grade=="OK"){
                        			blqty="正常品";
                        		}else if(grade=="FX"){
                        			blqty="风险品";
                        		}else if(grade=="GK"){
                        			blqty="管控品";
                        		}else if(grade=="MR"){
                        			blqty="MRB品";
                        		}else if(grade=="HG"){
                        			blqty="合格品";
                        		}else if(grade=="LW"){
                        			blqty="裂纹品";
                        		}else if(grade=="PP"){
                        			blqty="破片管控";
                        		}
                        		label.PrintHCT088(printer, soId,overDate, outwoId,qty, mdltyp,cusId,pm,boxId,
                        	             blqty, user, date, mdlId, woId,okyield, qauser,bt);
                        	}else if(boxObj.cus_id_fk=="098"){
                        		var printer = controlsQuery.print1Select.find("option:selected").text();

                        		var gys = "HCT";
                        		var boxId = rowData.box_id;
                        		var cusName = "上海中航光电子有限公司";
                        		var mode = boxObj.th_mdl_id_fk;
                        		var date = boxObj.comp_date;
                        		var size = boxObj.guige;
                        		var indate = boxObj.overdue_date;
                        		var mtrllot = "";
                        		//var orderno = boxObj.so_id_fk;
                        		var orderno = "";
                        		var mode2 = boxObj.mtrl_prod_id_fk;
                        		var cnt = boxObj.prd_qty;
                        		var allcnt = boxObj.mode_cnt;
                        		var lotno = boxObj.wo_id_fk;
                                var procate = "";
                                if(boxObj.wo_cate=="ENG"){
                                	procate = "试验品";
                                }else if(boxObj.wo_cate=="ONCE"){
                                	procate = "ON CELL品";
                                }else if(boxObj.wo_cate=="PROD"){
                                	procate = "量产品";
                                }else if(boxObj.wo_cate=="RD"){
                                	procate = "研发品";
                                }else if(boxObj.wo_cate=="SAMP"){
                                	procate = "样品";
                                }
                        		label.PrintHCT0981(printer, gys,boxId, cusName,mode, date,size,indate,mtrllot, orderno, mode2, cnt, allcnt, lotno,procate);
                        		 var thickness = boxObj.to_thickness/2;
                                 var time = boxObj.comp_date_all;
                                 var qty = boxObj.prd_qty;
                                 var userID = boxObj.evt_user;
                                 var product = boxObj.th_mdl_id_fk;
                                 var shipboxId = boxObj.ship_box_id;
                         		label.PrintHCToncell088(printer, shipboxId,thickness,time,qty,userID,product);
                        	}else if(boxObj.cus_id_fk=="068"){
                        		var printer = controlsQuery.print1Select.find("option:selected").text();
                        		var soId = boxObj.so_id_fk;
                        		var cusId = "厦门天马";
                        		var compDate = boxObj.comp_date;
                        		var title = "薄化产品出货标签";
                        		var mdl_id = boxObj.th_mdl_id_fk;
                        		var toThickness = boxObj.to_thickness;
                        		var cusInfoFst = boxObj.cus_info_fst;
                        		var cusInfoSnd = boxObj.cus_info_snd;
                        		var prdQty = boxObj.prd_qty;
                        		var NgQty = "/"
                        		//var boxId = boxObj.box_id;
                        		var boxId = boxObj.ship_box_id;
                        		if(boxId ==null || boxId==""){
                        			boxId = boxObj.box_id;
                        		}
                        		var grade = boxObj.prd_grade_fk;
                        		var blqty = "";
                        		if(grade=="OK"){
                        			blqty="正常品";
                        		}else if(grade=="FX"){
                        			blqty="风险品";
                        		}else if(grade=="GK"){
                        			blqty="管控品";
                        		}else if(grade=="MR"){
                        			blqty="MRB品";
                        		}else if(grade=="HG"){
                        			blqty="合格品";
                        		}else if(grade=="LW"){
                        			blqty="裂纹品";
                        		}else if(grade=="PP"){
                        			blqty="破片管控";
                        		}
                        		var userId = boxObj.evt_user;
                        		var woId = boxObj.wo_id_fk;
                        		//label.PrintHCT068(printer,soId, cusId, compDate,title, mdl_id, toThickness,cusInfoFst,cusInfoSnd,prdQty,NgQty,boxId, blqty,userId,woId);
                          		var cusName = "鸿创科技有限公司";
                        		var prodName = "1/4一次切割中片";
                        		var dueDate = boxObj.overdue_date;
                        		var reportFLg = "YES";
                        		var hightFlg = "";
                        		label.PrintHCT068N(printer, cusName, prodName,
                        				mdl_id, woId, compDate, dueDate, boxId, cusInfoFst,
                        				cusInfoSnd, prdQty, reportFLg, hightFlg, userId);
                        	}
                        }else if(proc_id == "DMZQ" && boxObj.mdl_cate_fk == "AB"){
                        	if(boxObj.cus_id_fk=="058"){
                                showMessengerSuccessDialog(out_box_id + "正在打印出货标签...",1);
                                var printer = controlsQuery.print1Select.find("option:selected").text();
                                var ps = boxObj.mainT;
                                var mkr = "湖北鸿创";
                                var woId = boxObj.wo_id_fk;
                                var wocate = "";
                                if(boxObj.wo_cate=="ENG"){
                                	wocate = "试验品";
                                }else if(boxObj.wo_cate=="ONCE"){
                                	wocate = "ON CELL品";
                                }else if(boxObj.wo_cate=="PROD"){
                                	wocate = "量产品";
                                }else if(boxObj.wo_cate=="RD"){
                                	wocate = "研发品";
                                }else if(boxObj.wo_cate=="SAMP"){
                                	wocate = "样品";
                                }
                                var mtrlId = boxObj.fm_mdl_id_fk;
                                var qty = boxObj.prd_qty + "*" + boxObj.smode_cnt + "=" + boxObj.mode_cnt;
                                var jgyq = boxObj.jgyq;
                                var date = boxObj.pln_cmp_date;
                                var outdate = boxObj.comp_date;
                                var boxId = out_box_id;
                                var thickness = boxObj.to_thickness;
                                var dir = boxObj.fhdir;
                                var user = boxObj.evt_user;
                                var note = boxObj.wo_dsc;
                                label.PrintHCT058(printer, ps, mkr, woId, wocate, mtrlId, qty,jgyq, date,
                                         outdate, boxId, thickness, dir, user,note);
                                var title = boxObj.shtT;
                                var shtAry = [];
                			    for(j=0; j<20; j++){
              				        shtAry[j] = "";
              			        }
                        		if( boxObj.prd_qty == 1){
          						    shtAry[0] = boxObj.table.prd_seq_id;
          					    }else if(boxObj.table.length > 1){
          						   for( i=0;i<boxObj.table.length;i++){
          							   shtAry[i] = boxObj.table[i].prd_seq_id;
          						   }
          					    }
                                label.PrintForHCT058ShtList(printer,title,JSON.stringify(shtAry),boxId);
                                var inTrxCRT = {
        		        				trx_id : "XPCRTNO",
        		        				action_flg : "C",
        		        				key_id : mtrlId,
        		        				type : "S"
        		        			};
        		        		 var outTrxCRT = comTrxSubSendPostJson(inTrxCRT);
        		            	 var str = mtrlId + "H" + outTrxCRT.serial_no;
        		            	 label.PrintQR( printer, str);
        		            	 label.PrintQRhc( printer);
                        	}else if(boxObj.cus_id_fk=="018"||boxObj.cus_id_fk=="088"){
                                showMessengerSuccessDialog(out_box_id + "正在打印出货标签...",1);
                                var printer = controlsQuery.print1Select.find("option:selected").text();
                                var thickness = boxObj.to_thickness/2;
                                var time = boxObj.comp_date_all;
                                var qty = boxObj.prd_qty;
                                var userID = boxObj.evt_user;
                                var product = boxObj.fm_mdl_id_fk;
                                var boxId = boxObj.ship_box_id;
                        		label.PrintHCToncell088(printer, boxId,thickness,time,qty,userID,product);
                        		var printer = controlsQuery.print1Select.find("option:selected").text();
                        		var soId = boxObj.so_id_fk;
                                var dateinfo = new Array();
                        		    dateinfo = boxObj.overdue_date.split("-");
                        		var overDate =  dateinfo[1] + "月" + dateinfo[2] + "日";
                        		var outwoId = boxObj.cus_info_fst;
                        		var qty = boxObj.prd_qty;
                        		var mdltyp = boxObj.guige;
                        		var cusId = boxObj.cus_dsc;
                        		var boxId = out_box_id;
                        		var pm = "LCD-TFT";
                        	   // var blqty = boxObj.bl_cnt + "cell";
                        		var user = boxObj.evt_user;
                        	    var dateinfo1 = new Array();
                     		        dateinfo1 = boxObj.comp_date.split("/");
                     		    var date =  dateinfo1[1] + "月" + dateinfo1[2] + "日";
                        		var mdlId = boxObj.fm_mdl_id_fk;
                        		var woId = boxObj.wo_id_fk;
                        		//var okyield = boxObj.okyield + "%";
                        		var okyield = "";
                        		var qauser = "HC0025";
                        		var bt = "镀膜产品出货标签";
                        		var blqty = "";
                        		var grade = boxObj.prd_grade_fk;
                        		if(grade=="OK"){
                        			blqty="正常品";
                        		}else if(grade=="FX"){
                        			blqty="风险品";
                        		}else if(grade=="GK"){
                        			blqty="管控品";
                        		}else if(grade=="MR"){
                        			blqty="MRB品";
                        		}else if(grade=="HG"){
                        			blqty="合格品";
                        		}else if(grade=="LW"){
                        			blqty="裂纹品";
                        		}else if(grade=="PP"){
                        			blqty="破片管控";
                        		}
                        		label.PrintHCT088(printer, soId,overDate, outwoId,qty, mdltyp,cusId,pm,boxId,
                        	             blqty, user, date, mdlId, woId,okyield, qauser,bt);
                        	}else if(boxObj.cus_id_fk=="098"){
                        		var printer = controlsQuery.print1Select.find("option:selected").text();

                        		var gys = "HCT";
                        		var boxId = boxObj.box_id;
                        		var cusName = "上海中航光电子有限公司";
                        		var mode = boxObj.fm_mdl_id_fk;
                        		var date = boxObj.comp_date;
                        		var size = boxObj.guige;
                        		var indate = boxObj.overdue_date;
                        		var mtrllot = "";
                        		//var orderno = boxObj.so_id_fk;
                        		var orderno = "";
                        		var mode2 = boxObj.mtrl_prod_id_fk;
                        		var cnt = boxObj.prd_qty;
                        		var allcnt = boxObj.mode_cnt;
                        		var lotno = boxObj.wo_id_fk;
                                var procate = "";
                                if(boxObj.wo_cate=="ENG"){
                                	procate = "试验品";
                                }else if(boxObj.wo_cate=="ONCE"){
                                	procate = "ON CELL品";
                                }else if(boxObj.wo_cate=="PROD"){
                                	procate = "量产品";
                                }else if(boxObj.wo_cate=="RD"){
                                	procate = "研发品";
                                }else if(boxObj.wo_cate=="SAMP"){
                                	procate = "样品";
                                }
//                                var shtAry = [];
//                			    for(j=0; j<20; j++){
//              				        shtAry[j] = "";
//              			        }
//                        		if( boxObj.prd_qty == 1){
//          						    shtAry[0] = boxObj.table.prd_seq_id;
//          					    }else if(boxObj.table.length > 1){
//          						   for( i=0;i<boxObj.table.length;i++){
//          							   shtAry[i] = boxObj.table[i].prd_seq_id;
//          						   }
//          					    }
                        		label.PrintHCT0981(printer, gys,boxId, cusName,mode, date,size,indate,mtrllot, orderno, mode2, cnt, allcnt, lotno,procate);
                       // 		label.PrintHCT0982(printer, boxId, JSON.stringify(shtAry));
                        		var thickness = boxObj.to_thickness/2;
                                var time = boxObj.comp_date_all;
                                var qty = boxObj.prd_qty;
                                var userID = boxObj.evt_user;
                                var product = boxObj.fm_mdl_id_fk;
                                var shipboxId = boxObj.ship_box_id;
                        		label.PrintHCToncell088(printer, shipboxId,thickness,time,qty,userID,product);
                        	}else if(boxObj.cus_id_fk=="068"){
                        		var printer = controlsQuery.print1Select.find("option:selected").text();
                        		var soId = boxObj.so_id_fk;
                        		var cusId = "厦门天马";
                        		var compDate = boxObj.comp_date;
                        		var title = "镀膜产品出货标签";
                        		var mdl_id = boxObj.fm_mdl_id_fk;
                        		var toThickness = boxObj.to_thickness;
                        		var cusInfoFst = boxObj.cus_info_fst;
                        		var cusInfoSnd = boxObj.cus_info_snd;
                        		var prdQty = boxObj.prd_qty;
                        		var NgQty = "/"
                        		//var boxId = boxObj.box_id;
                        		var boxId = boxObj.ship_box_id;
                        		if(boxId ==null || boxId==""){
                        			boxId = boxObj.box_id;
                        		}

                        		var grade = boxObj.prd_grade_fk;
                        		var blqty = "";
                        		if(grade=="OK"){
                        			blqty="正常品";
                        		}else if(grade=="FX"){
                        			blqty="风险品";
                        		}else if(grade=="GK"){
                        			blqty="管控品";
                        		}else if(grade=="MR"){
                        			blqty="MRB品";
                        		}else if(grade=="HG"){
                        			blqty="合格品";
                        		}else if(grade=="LW"){
                        			blqty="裂纹品";
                        		}else if(grade=="PP"){
                        			blqty="破片管控";
                        		}
                        		var userId = boxObj.evt_user;
                        		var woId = boxObj.wo_id_fk;
                        		//label.PrintHCT068(printer,soId, cusId, compDate,title, mdl_id, toThickness,cusInfoFst,cusInfoSnd,prdQty,NgQty,boxId, blqty,userId,woId);
                          		var cusName = "鸿创科技有限公司";
                        		var prodName = "1/4一次切割中片";
                        		var dueDate = boxObj.overdue_date;
                        		var reportFLg = "YES";
                        		var hightFlg = "";
                        		label.PrintHCT068N(printer, cusName, prodName,
                        				mdl_id, woId, compDate, dueDate, boxId, cusInfoFst,
                        				cusInfoSnd, prdQty, reportFLg, hightFlg, userId);
                        	}
                        }else if(proc_id == "DMBZ" && (boxObj.mdl_cate_fk == "AB" || boxObj.mdl_cate_fk == "B")){//华星镀膜包装
                                if(boxObj.cus_id_fk=="007"){
                                    var mtrlId = boxObj.fm_mdl_id_fk;
                                    var mtrlBoxId = boxObj.box_phy_id;
                                    //var mtrlBoxId = "201706";
                                    var prdQty = boxObj.prd_qty;
                                    var boxId = boxObj.ship_box_id;
                                    var vender = "201706";
                                    label.PrintboxforHX(controlsQuery.print1Select.find("option:selected").text(), mtrlId,mtrlBoxId, prdQty,boxId,vender);
                                    // label.PrintLabelForDM007(controlsQuery.print1Select.find("option:selected").text(),mtrlId,mtrlBoxId, prdQty,boxId);
                                }

                        } else if(proc_id == "PGGY" && boxObj.mdl_cate_fk == "AB"){
                        	if(boxObj.cus_id_fk=="098"||boxObj.cus_id_fk=="068"||boxObj.cus_id_fk=="090"){
                        		var printer = controlsQuery.print1Select.find("option:selected").text();
                        		var boxId = boxObj.box_id;
                                var shtAry = [];
                			    for(j=0; j<40; j++){
              				        shtAry[j] = "";
              			        }
                        		if( boxObj.prd_qty == 1){
          						    shtAry[0] = boxObj.table.prd_seq_id;
          					    }else if(boxObj.table.length > 1){
          						   for( i=0;i<boxObj.table.length;i++){
          							   shtAry[i] = boxObj.table[i].prd_seq_id;
          						   }
          					    }
                        		label.PrintHCTshtList(printer, boxId, JSON.stringify(shtAry));
                        	}
                        }

                	}catch(ex){
                		showMessengerErrorDialog(out_box_id + "打印出货标签失败!",1);
                		console.error(ex);
                	}

//             	   try{
//                       if(proc_id == "JBQA" || proc_id == "DMQA"){//打印出货明细表
//                       	toolFunc.printQA(out_box_id, proc_id);
//                       }
//            	   }catch(ex){
//            		   showMessengerErrorDialog(out_box_id + "打印出货信息表失败!",1);
//            		   console.error(ex);
//            	   }
                }
                controlsQuery.swhOpeChk.removeAttr("checked");
            }
        },
        //调账
        issue_move_out : function(){
        	toolFunc.getPrdRwCnt(true);
        },
        //query sht in list
        query_sht : function(){
            var i, j,
                grd,
                hasFound,
                curRowData,
                curRowElement,
                prd_id,
                rowIds,
                out_box_id,
                scrollHeight = 200;

            hasFound = false;

            prd_id = $.trim(controlsQuery.prdIdInput.val());
            if(!prd_id){
                showErrorDialog("","请输入产品ID");
                return false;
            }
//            if(gCus_id=="088"){
//            	prd_id = prd_id.substring(0,prd_id.length-1);
//            }
            out_box_id = controlsQuery.okOutCrrTxt.val();

            if(!out_box_id){
                showErrorDialog("","请输入转入箱号！");
                return false;
            }
            grd = controlsQuery.mainGrd.grdId;

            rowIds = grd.jqGrid('getDataIDs');
            if(rowIds){
            	$("#wipInfoGrd tr").removeClass('bg-found').addClass('ui-widget-content');
                for(i = 0, j = rowIds.length; i < j; i++) {
                    curRowElement = $("#" + rowIds[i]);
//                    curRowElement.removeClass('bg-found').addClass('ui-widget-content');
                    curRowData = grd.jqGrid('getRowData', rowIds[i]);
                    if( !hasFound && curRowData['fab_sn'] == prd_id){
                       //grd.trigger("reloadGrid");
                    	if($.trim(curRowData['prd_stat']) !== "INPR"){
                            showErrorDialog('',prd_id + '尚未入账，请先输入入账箱号做点击入账！');
                            return false;
                    	}
                        //curRowElement.removeClass('ui-widget-content').addClass('bg-found');
                        if($("#" + curRowElement[0].children[0].children[0].id).attr("checked")!="checked"){
                        grd.jqGrid('setSelection',rowIds[i]);
                        }
                        hasFound = true;
                        //scroll
                        scrollHeight = (curRowElement[0].rowIndex-1) * curRowElement.height();
                        $($('div.ui-jqgrid-bdiv')[1]).scrollTop(scrollHeight);
                        console.log("Scroll %s", scrollHeight);

                        /**
                         * 刷入玻璃定位后，查询def明细
                         */
                    	toolFunc.showDefectDetail(rowIds[i]);
                       // toolFunc.process_start_one_row_func(rowIds[i]);
                    	btnFunc.f6_func_s(rowIds[i]);
                        toolFunc.showGRDButton(curRowData['cus_id']);
           			    toolFunc.get_all_defect_func(curRowData['cus_id']);
            			controlsQuery.prdIdInput.val("");
                        break;
                    }
                }
                if(!hasFound){
                    showErrorDialog("", "["+ prd_id +"]不在该批次中！");
                    return false;
                }
            }

            return true;
        },
        //Save buff time for panels
        save_buff_time_func : function(){
            var tftBuffTime,
                cfBuffTime,
                tftBuffTool,
                cfBuffTool,
                ids,
                idLen,
                rowData,
                i;

            //JBYZ JBZJ JBPG
            if(!gCanSaveBufTime){
                showErrorDialog("","此站点禁止登记抛光时间！");
                return false;
            }

            //Get input
            tftBuffTime = $.trim( controlsQuery.tftBuffTxt.val() );
            cfBuffTime = $.trim( controlsQuery.cfBuffTxt.val() );
            if(!tftBuffTime){
                showErrorDialog("","请输入TFT抛光时间!");
                return false;
            }
            if(!cfBuffTime){
                showErrorDialog("","请输入CF抛光时间!");
                return false;
            }

            if( 0 <= parseInt(tftBuffTime, 10) ){
                tftBuffTime = parseInt(tftBuffTime, 10);
            }else{
                showErrorDialog("","请输入正确的TFT抛光时间，必须为数字!");
                return false;
            }
            if( 0 <= parseInt(cfBuffTime, 10) ){
                cfBuffTime = parseInt(cfBuffTime, 10);
            }else{
                showErrorDialog("","请输入正确的CF抛光时间，必须为数字!");
                return false;
            }

            tftBuffTool = controlsQuery.tftToolSelect.val();
            cfBuffTool = controlsQuery.cfToolSelect.val();

            //Set value
            crGrid = controlsQuery.mainGrd.grdId;
            ids = crGrid.jqGrid('getGridParam','selarrrow');
            idLen = ids.length;
            if(0 === idLen){
                showErrorDialog("","请勾选需要登记抛光时间的产品ID！");
                return false;
            }

            for( i = 0; i < idLen; i++ ){
                rowData = crGrid.jqGrid("getRowData", ids[i]);
                rowData['tft_buff_time'] = tftBuffTime;
                rowData['cf_buff_time'] = cfBuffTime;
                rowData['tft_buff_tool'] = tftBuffTool;
                rowData['cf_buff_tool'] = cfBuffTool;
                toolFunc.enhanceGridOaryArr(rowData);
                crGrid.jqGrid('setRowData', ids[i], rowData);
            }

            showMessengerSuccessDialog("登记抛光时间成功!", 1);
            return true;
        },
        //Set judge_result in grid
        set_judge_result_func : function(grade){
            var i,
                crGrid,
                ids,
                idLen,
                rowData,
                tool_id,
                inTrx,
                tmpIary={},
                iary=[],
                outTrx;

            tool_id = $.trim(controlsQuery.toolSelect.val());
            crGrid = controlsQuery.mainGrd.grdId;
            ids = crGrid.jqGrid('getGridParam','selarrrow');
            idLen = ids.length;
            if(0 === idLen){
                showErrorDialog("","请勾选需要判定的产品ID！");
                return false;
            }

            for( i = 0; i < idLen; i++ ){
                rowData = crGrid.jqGrid("getRowData", ids[i]);
                if ( rowData['prd_stat'] == 'WAIT' || rowData['start_time'] == ''){
                    showErrorDialog("","产品ID状态为非INPR或者未开始！");
                	return false;
                }
                rowData['judge_result'] = grade;
                rowData['prd_grade'] = grade;
                toolFunc.enhanceGridOaryArr(rowData);
                crGrid.jqGrid('setRowData', ids[i], rowData);
                tmpIary = {
                	prd_grade:grade,
                	prd_seq_id:rowData['prd_seq_id']
                };
                iary.push(tmpIary);
            }

            inTrx = {
        		trx_id : VAL.T_XPIWIPEQ,
                action_flg : "E",  //S
                tool_id : tool_id,
                iary : iary
            };
            outObj = comTrxSubSendPostJson(inTrx);
            if (outObj.rtn_code == VAL.NORMAL) {
            	showMessengerSuccessDialog("判等成功!",1);
            }

            return true;
        },
        //QA inspection
        qa_func : function(){
            "use strict";
            var i,
                crGrid,
                ids,
                idLen,
                rowData,
                row_box_id,
                process_user,
                sht_id,
                qa_flg,
                proc_id,
                new_qa_flg,
                all_qa_flg;

            proc_id = toolFunc.getCurProc();

            if(0 > $.inArray(proc_id, VAL.QA_PROCS)){
                console.log("当前制程 %s， 不允许点击qa按钮！", proc_id);
                return false;
            }

            crGrid = controlsQuery.mainGrd.grdId;
            ids = crGrid.jqGrid('getGridParam','selarrrow');
            idLen = ids.length;
            if(0 === idLen){
                showErrorDialog("","请勾选需要检验的产品ID！");
                return false;
            }

            for( i = 0; i < idLen; i++ ){
                rowData = crGrid.jqGrid("getRowData", ids[i]);
                sht_id = $.trim(rowData['prd_seq_id']);

                row_box_id = $.trim(rowData['box_id']);
                if(row_box_id){
                    showErrorDialog("",sht_id + "已经转入" + row_box_id + "，只能在转入前检验！");
                    return false;
                }
                qa_flg = rowData.qa_flg.trim();
                if(rowData['cr_proc']=="JBQA"){
                	  new_qa_flg = "Y" === qa_flg.substr(0,1) ? "N" : "Y";
                	  all_qa_flg = new_qa_flg + qa_flg.substr(1,1);

                }else if(rowData['cr_proc']=="DMQA"){
                	    new_qa_flg = "Y" === qa_flg.substr(1,1) ? "N" : "Y";
                	    all_qa_flg = qa_flg.substr(0,1) + new_qa_flg;
                }

                crGrid.jqGrid('setRowData', ids[i], {
                    qa_flg : all_qa_flg
                });

                var inTrx,outTrx;

                inTrx = {
            		trx_id : VAL.T_XPPNLJGE,
            		action_flg : "B",
            		prd_seq_id : rowData['prd_seq_id'],
            		proc_id : rowData['cr_proc'],
            		new_qa_flg:new_qa_flg
                };

                outTrx = comTrxSubSendPostJson(inTrx);
                if (outTrx.rtn_code == VAL.NORMAL) {
                }

            }
            showSuccessDialog("检验成功！");
        	return;
        },
        //查询入账箱号信息
        query_box_info : function(){
            var inObj,
	            outObj,
	            box_id,
	            ope_id,
	            ope_ver,
	            tool_id,
	            ope_dsc,
	            ope_info,
	            ope_info_ary;

            tool_id = $.trim(controlsQuery.toolSelect.val());
            box_id = $.trim(controlsQuery.moveInBoxTxt.val());
            if(!box_id){
            	showErrorDialog("","请输入入账箱号！");
                return false;
            }

	        ope_info = $.trim(controlsQuery.opeSelect.val());
	        if(!ope_info){
	            return false;
	        }

	        ope_info_ary = ope_info.split("@");
	        ope_id = ope_info_ary[0];
	        ope_ver = ope_info_ary[1];
	        ope_dsc = $.trim(controlsQuery.opeSelect.find("option:selected").text());
	        if(!ope_id || !ope_ver){
            	showErrorDialog("","站点[" +ope_dsc+ "]信息不正确，请确认！");
                return false;
	        }
	        inObj = {
	            trx_id : VAL.T_XPIWIPEQ,
	            action_flg : "B",
	            cr_ope_id  : ope_id,
	            cr_ope_ver : ope_ver,
	            tool_id    : tool_id,
	            box_id     : box_id
	        };
            controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
	        outObj = comTrxSubSendPostJson(inObj);
	        if (outObj.rtn_code == VAL.NORMAL) {
	        	toolFunc.enhanceGridOaryArr(outObj.oary_prd);
	            setGridInfo(outObj.oary_prd, "#wipInfoGrd",true);
	            toolFunc.changeColor();
	        }else{
	        	controlsQuery.moveInBoxTxt.val("");
	        }
        },
        //查询需转入箱子的信息 未使用
        query_prd_in_box_info :function(){
            var inObj,
	            outObj,
	            box_id,
	            ope_id,
	            ope_ver,
	            tool_id,
	            ope_info,
	            ope_info_ary;

            tool_id = $.trim(controlsQuery.toolSelect.val());
            box_id = $.trim(controlsQuery.prdInBoxTxt.val());
            if(!box_id){
            	showErrorDialog("","请输入转入箱号！");
                return false;
            }
	        ope_info = $.trim(controlsQuery.opeSelect.val());

	        if(!ope_info){
	            return false;
	        }

	        ope_info_ary = ope_info.split("@");
	        ope_id = ope_info_ary[0];
	        ope_ver = ope_info_ary[1];

	        inObj = {
                trx_id : VAL.T_XPIWIPEQ,
                action_flg  : "P",
                cr_ope_id   : ope_id,
                cr_ope_ver  : ope_ver,
                tool_id     : tool_id,
                box_id      : box_id
	        };
	        outObj = comTrxSubSendPostJson(inObj);
	        if (outObj.rtn_code == VAL.NORMAL) {
	        	toolFunc.enhanceGridOaryArr(outObj.oary_prd);
	        	 setGridInfo(outObj.oary_prd, "#wipInfoGrd",true);
	        	 toolFunc.changeColor();
	        }else{
	        	controlsQuery.prdInBoxTxt.val("");
	        	controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
	        }
        },
        //查询需来料箱子的信息 未使用
        query_mtrl_box_info :function(){
            var inObj,
	            outObj,
	            box_id,
	            ope_id,
	            ope_ver,
	            tool_id,
	            ope_info,
	            ope_info_ary;

            tool_id = $.trim(controlsQuery.toolSelect.val());
            box_id = $.trim(controlsQuery.ppboxIdTxt.val());
            if(!box_id){
            	showErrorDialog("","请输入来料箱号！");
                return false;
            }
	        ope_info = $.trim(controlsQuery.opeSelect.val());

	        if(!ope_info){
	            return false;
	        }

	        ope_info_ary = ope_info.split("@");
	        ope_id = ope_info_ary[0];
	        ope_ver = ope_info_ary[1];

	        inObj = {
                trx_id : VAL.T_XPIWIPEQ,
                action_flg : "M",
                cr_ope_id  : ope_id,
                cr_ope_ver : ope_ver,
                tool_id    : tool_id,
                box_id     : box_id
	        };
	        outObj = comTrxSubSendPostJson(inObj);
	        if (outObj.rtn_code == VAL.NORMAL) {
	        	toolFunc.enhanceGridOaryArr(outObj.oary_prd);
	        	setGridInfo(outObj.oary_prd, "#wipInfoGrd",true);
	        	 toolFunc.changeColor();
	        }else{
	        	controlsQuery.ppboxIdTxt.val("");
	        	controlsQuery.mainGrd.grdId.jqGrid("clearGridData");
	        }
        },
        //入账
        f7_func : function(){
            var box_id,
                tool_id,
	            ope_id,
	            ope_ver,
	            ope_info,
	            ope_info_ary,
                inObj,
                outObj;

            tool_id = $.trim(controlsQuery.toolSelect.val());
	        ope_info = $.trim(controlsQuery.opeSelect.val());
	        if(!ope_info){
	            return false;
	        }
	        if(!tool_id){
            	showErrorDialog("","设备ID不能为空！");
	            return false;
	        }

	        ope_info_ary = ope_info.split("@");
	        ope_id = ope_info_ary[0];
	        ope_ver = ope_info_ary[1];

            box_id = $.trim(controlsQuery.moveInBoxTxt.val());
            if(!box_id){
                toolFunc.queryWAITbox();
                return;
            }

            if (VAL.EVT_USER == "" || VAL.EVT_USER == null || VAL.EVT_USER == undefined){
            	showErrorDialog("","请重新登录！");
            	return false;
            }

            inObj = {
                trx_id     : VAL.T_XPMOVEIN,
                evt_usr    : VAL.EVT_USER  ,
                box_id     : box_id        ,
                tool_id    : tool_id       ,
                cr_ope_id  : ope_id        ,
                cr_ope_ver : ope_ver
            };
            outObj = comTrxSubSendPostJson(inObj);
            if(outObj.rtn_code == VAL.NORMAL) {
            	btnFunc.query_box_info();
            }
        },

        wip_retain_sc_func : function (iary){
        	var inTrx,outTrx;
        	inTrx = {
                 trx_id     : VAL.T_XPWIPRETAIN,
                 action_flg : "S",
                 evt_usr    : VAL.EVT_USER  ,
                 iary       : iary
             };
        	outTrx = comTrxSubSendPostJson(inTrx);
            if(outTrx.rtn_code == VAL.NORMAL) {
            	showSuccessDialog("成功在制保留！");
            	return;
            }
        },

        wip_retain_func : function(){
        	var crGrid = controlsQuery.mainGrd.grdId;
            ids = crGrid.jqGrid('getGridParam','selarrrow');
            var sc_flg = false;
            var okng_flg = false;
            var box_flg = false;
            var sht_id = "";
            var iary=[];
            for (var i=0;i<ids.length;i++){
            	var iarytmp = {};
            	rowData = crGrid.jqGrid("getRowData", ids[i]);
            	prd_grade = $.trim(rowData['pv_prd_grade']);
            	judge_result = $.trim(rowData['judge_result']);
            	box_id = $.trim(rowData['box_id']);
            	iarytmp = {
        			prd_seq_id : rowData['prd_seq_id']
            	};
            	iary.push(iarytmp);
            	if (!sc_flg){
            		if (("SC" === prd_grade && "" === judge_result) || "SC" === judge_result){
            			sht_id = rowData['prd_seq_id'];
                		sc_flg = true;
                	}
            	}
            	if (!okng_flg){
            		if (!(("SC" === prd_grade && "" === judge_result) || "SC" === judge_result)){
                		okng_flg = true;
                	}
            	}
            	if (!box_flg){
            		if (box_id != ""){
                		box_flg = true;
                	}
            	}
            }

            if (sc_flg && okng_flg){
            	showErrorDialog("","报废品["+sht_id+"]不能与正常品一同保留！");
            	return false;
            }
            if (sc_flg){
            	if (!box_flg){
            		btnFunc.wip_retain_sc_func(iary);
                	return;
            	}
            }

        	var out_box_id,
        	    inObj,
        	    outObj,
        	    iary = [],
                tmpIary = {};
        	//Get box info
            out_box_id = controlsQuery.okOutCrrTxt.val();
            if(!out_box_id){
                showErrorDialog("","请输入出账箱号！");
                return false;
            }

            tmpIary = {
        		box_id : out_box_id
            };
            iary.push(tmpIary);

            inObj = {
                    trx_id     : VAL.T_XPWIPRETAIN,
                    action_flg : "R",
                    evt_usr    : VAL.EVT_USER  ,
                    box_cnt    : 1        ,
                    iary       : iary
                };
            outObj = comTrxSubSendPostJson(inObj);
            if(outObj.rtn_code == VAL.NORMAL) {
            	showSuccessDialog("成功在制保留！");
            	return;
            }
        },

        wip_retain_cancel_func : function(){
        	var ope_id,ope_ver,ope_info, ope_info_ary,wo_id;
            ope_info = $.trim(controlsQuery.opeSelect.val());
            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];
            ope_ver = ope_info_ary[1];
            if(!ope_id){
                return false;
            }
            if(!ope_ver){
                return false;
            }

            wo_id = $.trim(controlsQuery.woIdSelect.val());

            $("#wip_retain_cancel_btn").showWipRetainCancelDialog({
                action_flg : "H",//"H" for hide,"S" for show
		        ope_id     : ope_id,
		        ope_ver    : ope_ver,
				wo_id      : wo_id,
				user_id    : VAL.EVT_USER
              });
        }

    };

    var SpcDialog = function(){
    	var Val = {
    		rowId : "",
    		mes_id : ""
    	};
    	$("#ope_id").val("");
        this.showDialogFnc  = function(rowId,mes_id){
        	Val.rowId = rowId;
        	Val.mes_id = mes_id;

        	mainGrd = controlsQuery.mainGrd.grdId;
            rowData = mainGrd.jqGrid("getRowData",rowId);
            prd_seq_id = rowData.prd_seq_id;
            $("#prd_seq_id").val(prd_seq_id);
            if(""===prd_seq_id){
            	return false;
            }
            var iary,
                inTrx,
                outTrx;
            inTrx = {
                trx_id     : VAL.T_XPSPCRET ,
                action_flg : 'Q'        ,
                prd_seq_id_fk :  prd_seq_id,
                mes_id : mes_id
            };
            outTrx = comTrxSubSendPostJson(inTrx);
            var oary;
            if (outTrx.rtn_code==VAL.NORMAL){
            	if(!this.queryFnc(rowId,mes_id)){
            		return false;
            	}
            	this.initialDialogFnc(rowId,mes_id);


                $("#spcDialog_woGrd").jqGrid("clearGridData");
                outTrx.data_cnt == "1" ? outTrx.oary[0] = outTrx.oary : outTrx.oary;
                for(var i=0;i<outTrx.data_cnt;i++){
                	$("#ope_id").val(outTrx.oary[0].ope_des);
                	outTrx.oary[i].data_num_cn = outTrx.oary[i].data_num.replace('T','测点');
                }
                setGridInfo(outTrx.oary,"#spcDialog_woGrd");
            }
            return true;
        };

        this.queryFnc = function (rowId,mes_id) {
        	mainGrd = controlsQuery.mainGrd.grdId;
            rowData = mainGrd.jqGrid("getRowData",rowId);
            prd_seq_id = rowData.prd_seq_id;
            if(rowData['prd_stat'] == 'INPR' && rowData['start_time'] != ''){
            	var tool_id = rowData.cr_tool_id_fk;
            	if(tool_id==""){
            		tool_id = $.trim(controlsQuery.toolSelect.val());
            	}
    			var iary1 = {
    				tool_id_fk : tool_id,
    				ope_no : rowData.cr_ope_no_fk,
    				mes_id_fk : mes_id,
    				rep_unit : 'P',
    				data_pat : '00'
    			};
    			var inTrxObj1 = {
    				trx_id : 'XPMLITEM',
    				action_flg : 'Q',
    				wo_id_fk : rowData['wo_id'],
    				iaryA : iary1
    			};
    			var outTrxObj1 = comTrxSubSendPostJson(inTrxObj1);
    			$("#dataItemEditDiv").empty();
    			if (outTrxObj1.rtn_code == VAL.NORMAL) {
    				var tbl_cnt = outTrxObj1.tbl_cnt_b;
    				if(tbl_cnt<=0){
    					showErrorDialog("", "机台"+rowData.cr_tool_id_fk+",MES_ID["+mes_id + "] 没有设定机台量测项目");
    					return false;
    				}
    				var str = '<table id="dataItemTbl" class="table table-striped table-bordered table-condensed" >'
    					    + '<thead><tr>'
    						+ '<th class="Header_Text " >参数序号 </th>'
    						+ '<th class="Header_Text " >参数名称 </th>'
    						+ '<th class="Header_Text " >组内序号 </th>'
    						+ '<th class="Header_Text " >参数说明 </th>'
    						+ '<th class="Header_Text " >值 </th>'
    						+ '<th class="Header_Text " >下限 </th>'
    						+ '<th class="Header_Text " >上限</th></tr></thead><tbody> ';
    				for (var i = 0; i < tbl_cnt; i++) {
    					str = str + "<tr>" + '<td class=" dataItemTdCss">'
    							+ outTrxObj1.oaryB[i].data_id + "</td>"
    							+ '<td id ="tbl_data_group' + i
    							+ '\" class=" dataItemTdCss">'
    							+ outTrxObj1.oaryB[i].data_group + "</td>"
    							+ '<td id ="tbl_data_group_seq' + i
    							+ '\" class=" dataItemTdCss">'
    							+ outTrxObj1.oaryB[i].data_group_seq + "</td>"
    							+ '<td class=" dataItemTdCss">'
    							+ outTrxObj1.oaryB[i].data_dsc + "</td>"
    							+ '<td class=" dataItemTdCss">'
    							+ '<input type="text" name='+ i +' class ="span2 numberCssA" id="dataItem_'
    							+ i + '_Txt\" />' + "</td>";
    					if(outTrxObj1.oaryB[i].l_spec){
    						str = str + '<td class=" dataItemTdCss"'+'id="lsl_'+i +'" >'
    						+ outTrxObj1.oaryB[i].l_spec + "</td>";
    					}
    					if(outTrxObj1.oaryB[i].u_spec){
    						str = str + '<td class=" dataItemTdCss"'+'id="usl_'+i+ '" >'
    						+ outTrxObj1.oaryB[i].u_spec + "</td>";
    					}
    					str = str + "</tr>";
    				}
    				str = str + "</tbody></table>";
    				$(str).appendTo("#dataItemEditDiv");
    				for (var i = 0; i < tbl_cnt; i++) {
    				$("#dataItem_" + i + "_Txt").keypress(function(event){
    						if (event.keyCode == 13) {
    							var nextSelecter,uslSelector,lslSelector,usl,lsl,index,value;
    				    		index = this.id.split("_")[1];
    				    		uslSelector = "#usl_"+index ;
    				    		usl = parseFloat($(uslSelector).text());
    				    		lslSelector = "#lsl_"+index;
    				    		lsl = parseFloat($(lslSelector).text());
    				    		value = parseFloat($(this).val());
    				    		if(value>usl){
    				    			showMessengerErrorDialog("输入数值高于标准上限"+usl,2);
    				    			$(this).focus();
    				    			return false;
    				    		}else if(value<lsl){
    				    			showMessengerErrorDialog("输入数值低于标准下限"+lsl,2);
    				    			$(this).focus();
    				    			return false;
    				    		}
    				    		nextSelecter = "#dataItem_" +( parseInt(this.name,10)+1 ) + "_Txt";
    				    		$(nextSelecter).focus();
    						}
    					});
    				}
    				showMessengerSuccessDialog("刷新列表成功!!",1);

    				$(".numberCssA").blur(function() {
    					var $this = $(this);
    					var val = $this.val().trim();
    					if (val && !ComRegex.isNumber(val)) {
    						showErrorDialog("", "[" + val + "]不是一个有效的数字,错误");
    						$this.val("");
    					}
    				});
    			}
            }else{
            	$("#dataItemEditDiv").empty();
            }
    		return true;
    	};


        this.initialDialogFnc = function(rowId,mes_id){
            $('#spcDialog').modal({
                backdrop:true,
                keyboard:false,
                show:false
            });
            $('#spcDialog').unbind('shown.bs.modal');
            $("#spcDialog_reportBtn").unbind('click');
            $('#spcDialog').modal("show");
            $("#spcDialog_reportBtn").bind('click',this.reportFnc);
            $("#spcDialog_woGrd").jqGrid({
                url:"",
                datatype:"local",
                mtype:"POST",
                autowidth:true,//宽度根据父元素自适应
                height:350,
                shrinkToFit:false,
                scroll:true,
                rownumbers  :true ,
                rowNum:20,
                rownumWidth : 20,
                loadonce:true,
                pager : '#spcDialog_woPg',
                colModel:  [
                    {name: 'prd_seq_id_fk'       , index: 'prd_seq_id_fk'           , label: '产品ID',       width: PRD_SEQ_ID_CLM},
                    {name: 'ope_id_fk'           , index: 'ope_id_fk'               , label: '站点',         width: OPE_ID_FK_CLM,hidden:true },
                    {name: 'ope_des'             , index: 'ope_des'                 , label: '站点',         width: OPE_ID_CLM},
                    {name: 'data_num'            , index: 'data_num'                , label: '测点',         width: DATA_NUM_CLM , hidden:true },
                    {name: 'data_num_cn'         , index: 'data_num_cn'             , label: '测量项',         width: DATA_NUM_CLM },
                    {name: 'data_value'          , index: 'data_value'              , label: '测量值',        width: DATA_NUM_CLM},
                    {name: 'rpt_usr'             , index: 'rpt_usr'                 , label: '上报人',        width: RPT_USR_CLM },
                    {name: 'rpt_timestamp'       , index: 'rpt_timestamp'           , label: '上报时间',      width: RPT_TIMESTAMP_CLM}
                ]
            });
        };

        this.reportFnc = function (){
        	mainGrd = controlsQuery.mainGrd.grdId;
            rowData = mainGrd.jqGrid("getRowData",Val.rowId);

            if(rowData['prd_stat'] == 'WAIT' || rowData['start_time'] == ''){
            	showErrorDialog("","该玻璃为非INPR状态或没有开始,不能测试");
            	return false;
            }
    		prd_seq_id = rowData.prd_seq_id;
    		cus_id = rowData.cus_id;

    		var judgeStr = "OK";

    		var tableItemCnt = 0;
    		tableItemCnt = $("#dataItemTbl tr").length - 1;
    		var iaryAry = new Array();
    		for (var i = 0; i < tableItemCnt; i++) {
    			if ($("#dataItem_" + i + "_Txt").val() != "") {
    				var iary = {
    					data_group : $("#tbl_data_group" + i).text(),
    					data_value : $("#dataItem_" + i + "_Txt").val().trim(),
    					data_group_seq : $("#tbl_data_group_seq" + i).text()
    				};
    				iaryAry.push(iary);
    				if (judgeStr != "NG"){
    					var valueSpc = Number($("#dataItem_" + i + "_Txt").val());
    					var upSpc = Number($("#usl_"+i ).text());
    					var lowSpc = Number($("#lsl_"+i ).text());

    					if (valueSpc < lowSpc || valueSpc > upSpc){
    						judgeStr = "NG";
    					}else {
    						judgeStr = "OK";
    					}
    				}
    			}
    		}

    		// Send Tx
    		var inTrxObj = {
    			trx_id : 'XPCMSRDT',
    			prd_seq_id : prd_seq_id,
    			tool_id : $("#toolSelect").find("option:selected").text(),
    			data_pat_fk : '00',
                rep_unit_fk :  "P",
    			mes_id_fk : Val.mes_id,
    			path_id : rowData.cr_path_id_fk,
    			path_ver : rowData.cr_path_ver_fk,
    			ope_no : rowData.cr_ope_no_fk,
    			ope_id : rowData.cr_ope_id,
    			ope_ver : rowData.cr_ope_ver_fk,
    			proc_id : rowData.cr_proc,
    			rpt_usr : $("#userId").text(),
    			judgeStr : judgeStr,
    			data_cnt : iaryAry.length,
    			iary : iaryAry
    		};

    		spcThicknessRule.reportSPC(inTrxObj,Val.mes_id,rowData.cr_ope_id,rowData.wo_id,rowData.lot_id,function(outTrxObj){
    			if (outTrxObj.rtn_code == VAL.NORMAL) {
	    			showMessengerSuccessDialog("数据上报成功!!",1);
	    			$("[id^='dataItem_']").val("");
	                $('#spcDialog').modal("hide");
	    			return false;
	    		}
    		})
    		if(cus_id=="068"&&judgeStr=="NG"){
    			$("#spcDialog_reportBtn").showCallBackWarnningDialog({
					errMsg : "玻璃: ["+prd_seq_id+"]厚度超标，请取消转入",
					callbackFn : function(data) {
						if (data.result == true) {
			            inObj = {
			                trx_id : VAL.T_XPCNPNLOUT ,
			                evt_usr : VAL.EVT_USER,
			                pnl_cnt : "1",
			                iary : {
			                    prd_seq_id : prd_seq_id
			                }
			            };

			            outObj = comTrxSubSendPostJson(inObj);
			            if (outObj.rtn_code == VAL.NORMAL) {
			                    rowData['box_id'] = "";
			                    rowData['end_user'] = "";
			                    rowData['end_time'] = "";
			                    rowData['evt_user'] = outObj.evt_user;
			                    rowData['evt_timestamp'] = outObj.evt_timestamp;
			                    toolFunc.enhanceGridOaryArr(rowData);
			                    mainGrd.jqGrid('setRowData', Val.rowId, rowData);

			                if(controlsQuery.okOutCrrTxt.val() == outObj.box_id){
			                    controlsQuery.OkBoxCurCntTxt.val(outObj.prd_qty);
			                    controlsQuery.OkBoxModeCntTxt.val(outObj.mode_cnt);
			                }

			                showMessengerSuccessDialog("成功取消转入!",1);
			            }
				        }else {
				        	return false;
				        }
					}
	        	});
    		}
        };
    };


    var RemarkDetailDialog = function(){
        this.showDialogFnc  = function(rowId){
        	this.initialDialogFnc();
        	this.queryFnc(rowId);
        };

        this.initialDialogFnc = function(){
            $('#remarkDtlDialog').modal({
                backdrop:true,
                keyboard:false,
                show:false
            });
            $('#remarkDtlDialog').modal("show");

            $("#remarkDtlGrd").jqGrid({
                url:"",
                datatype:"local",
                mtype:"POST",
                autowidth:true,//宽度根据父元素自适应
                height:350,
                shrinkToFit:false,
                scroll:true,
                rownumbers  :true ,
                rowNum:20,
                rownumWidth : 20,
                loadonce:true,
                pager : '#remarkDtlGrdPg',
                colModel:  [
                        	{name : 'sta_time_d'   ,index : 'sta_time_d'   ,label : START_DATETIME_TAG  ,width : TIMESTAMP_CLM},
                        	{name : 'evt_cate_dsc' ,index : 'evt_cate_dsc' ,label : EVT_CATE_TAG        ,width : EVT_CATE_DSC_CLM,hidden : true},
                        	{name : 'evt_usr'      ,index : 'evt_usr'      ,label : EVT_USR             ,width : USER_CLM,hidden : true},
                        	{name : 'box_id_fk'    ,index : 'box_id_fk'    ,label : BOX_ID_TAG          ,width : BOX_ID_CLM,hidden : true},
                        	{name : 'slot_no'      ,index : 'slot_no'      ,label : SLOT_NO_TAG         ,width : SLOT_NO_CLM,hidden : true},
                        	{name : 'lot_id'       ,index : 'lot_id'       ,label : LOT_ID_TAG          ,width : LOT_ID_CLM,hidden : true},
                        	{name : 'mdl_id_fk'    ,index : 'mdl_id_fk'    ,label : MDL_ID_TAG          ,width : MDL_ID_CLM,hidden : true},
                        	{name : 'prd_qty'      ,index : 'prd_qty'      ,label : "模数"                ,width : QTY_CLM,hidden : true},
                        	{name : 'prd_stat'     ,index : 'prd_stat'     ,label : "产品" + STATUS_TAG   ,width : PRD_STAT_CLM,hidden : true},
                        	{name : 'ope_no_dsc'   ,index : 'ope_no_dsc'   ,label : OPE_ID_TAG          ,width : OPE_ID_CLM},
                        	{name : 'toolg_id_fk'  ,index : 'toolg_id_fk'  ,label : TOOLG_ID_TAG        ,width : TOOLG_ID_FK_CLM,hidden : true},
                        	{name : 'tool_id_fk'   ,index : 'tool_id_fk'   ,label : TOOL_ID_TAG         ,width : TOOL_ID_CLM,hidden : true},
                        	{name : 'ds_recipe_id' ,index : 'ds_recipe_id' ,label : REMARK_TAG          ,width : 800}
                    	]
            });
        };

        this.queryFnc = function (rowId) {
        	var prd_seq_id, inTrx, outTrx, i, oary, tbl_cnt;

        	mainGrd = controlsQuery.mainGrd.grdId;
            rowData = mainGrd.jqGrid("getRowData",rowId);
            prd_seq_id = rowData.prd_seq_id;

            inTrx = {
				trx_id : "HPSHTHIS",
				action_flg : "Q",
				prd_seq_id : prd_seq_id,
				evt_cate : "DFJG"
			};
			outTrx = comTrxSubSendPostJson(inTrx);
			if (outTrx.rtn_code == "0000000") {
				tbl_cnt = outTrx.tbl_cnt;
				if (tbl_cnt <= 0) {
					$("#remarkDtlGrd").jqGrid("clearGridData");
					showMessengerSuccessDialog("根据玻璃ID查询不到信息!!",1);
					return false;
				}
				var oaryArr = new Array();
				var j = 0;
				for ( var i = 0; i < tbl_cnt; i++) {
					oary = tbl_cnt > 1 ? outTrx.oary[i] : outTrx.oary;
					if ($.trim(oary.ds_recipe_id) != "") {
						oaryArr[j] = oary;
						j++;
					}
				}

				setGridInfo(oaryArr, "#remarkDtlGrd", true);
			}

			showMessengerSuccessDialog("刷新列表成功!!",1);
			return true;
    	};
    };

    var baseQueryFnc = {
    	queryMain : function() {
    		var prd_id = $.trim(controlsQuery.prdIdInput.val());
		 	if(prd_id){
	        	inObj = {
	            	trx_id     : "XPINQSHT",
	            	action_flg : "F",
	            	fab_sn     : prd_id
	            };
	            outObj = comTrxSubSendPostJson(inObj);
	            if(outObj.rtn_code == VAL.NORMAL){
	            	if(outObj.has_more_flg === "Y"){
	            		FabSn.showSelectWorderDialog(prd_id,outObj.oary3,function(data){
	            			controlsQuery.prdIdInput.val(data.prd_seq_id)
	            			baseQueryFnc.querySub();
	            		});
	            	}else {
	            		baseQueryFnc.querySub();
					}
	            }
		 	}else{
		 		baseQueryFnc.querySub();
		 	}
    	},
    	querySub: function(){
        	var ope_info,ope_info_ary, ope_id, ope_ver, proc_id;
            ope_info = $.trim(controlsQuery.opeSelect.val());

            if(!ope_info){
                return false;
            }

            ope_info_ary = ope_info.split("@");
            ope_id = ope_info_ary[0];
            ope_ver = ope_info_ary[1];
            proc_id = ope_info_ary[2];

            btnFunc.f1_func();

//            if(0 <= $.inArray(proc_id, VAL.WIP_WAIT_VIALD)){
//            	btnFunc.f1_query_wait_func();
//            }else{
//
//            }
    	}
    }
    /**
     * Bind button click action
     */
    var iniButtonAction = function(){
        btnQuery.f1.click(function(){
        	baseQueryFnc.queryMain()
        });
        btnQuery.process_start.click(function(){
            btnFunc.process_start_func();
        });
        btnQuery.f6.click(function(){
           if(checkUserFunc("F_1400_PRDIN","Y")==false){
       		   showErrorDialog("","您没有权限进行此操作");
       		   return false;
       	   }else{
       		   btnFunc.f6_func();
       	   }
        });
        btnQuery.f8.click(function(){
        	toolFunc.getPrdRwCnt();
        });
        btnQuery.save_buff_time_btn.click(function(){
            btnFunc.save_buff_time_func();
        });
        btnQuery.qa_btn.click(function(){
            btnFunc.qa_func();
        });
        btnQuery.issue_move_out_btn.click(function(){
            btnFunc.issue_move_out();
        });
        btnQuery.f9.click(function(){
            btnFunc.f9_func();
        });
        btnQuery.f7.click(function(){
            btnFunc.f7_func();
        });
        //在制保留
        btnQuery.wip_retain_btn.click(function(){
            btnFunc.wip_retain_func();
        });
        //取消在制保留
        btnQuery.wip_retain_cancel_btn.click(function(){
           btnFunc.wip_retain_cancel_func();
            resizeFnc();
        });
        //Set judge_result
        btnQuery.gradeSelectBtnOK.click(function(){
            btnFunc.set_judge_result_func("OK");
        });
        btnQuery.gradeSelectBtnGK.click(function(){
            btnFunc.set_judge_result_func("GK");
        });
        btnQuery.gradeSelectBtnLZ.click(function(){
            btnFunc.set_judge_result_func("LZ");
        });
        btnQuery.gradeSelectBtnNG.click(function(){
            btnFunc.set_judge_result_func("NG");
        });
        btnQuery.gradeSelectBtnSC.click(function(){
            btnFunc.set_judge_result_func("SC");
        });
        btnQuery.gradeSelectBtnRM.click(function(){
            btnFunc.set_judge_result_func("RM");
        });
        btnQuery.gradeSelectBtnRW.click(function(){
            btnFunc.set_judge_result_func("RW");
        });
    };

    /**
     * grid  initialization
     */
    var iniGridInfo = function(){
        var grdInfoCM = [
            {name: 'slot_no',         index: 'slot_no',      	  label: SLOT_NO_TAG            ,  width: SLOT_NO_CLM,align:"center"   },
//            {name: 'mtrl_slot_no_btn',index: 'mtrl_slot_no_btn',  label: '修改'            ,  width: JUDGE_CLM,align:"center"}, //button
            //{name: 'mtrl_slot_no_fk' ,index: 'mtrl_slot_no_fk',   label: '实位'                 ,  width: PRD_GRADE_CLM,align:"center" },
            {name: 'prd_seq_id',      index: 'prd_seq_id',   	  label: PRD_SEQ_ID_TAG         ,  width: PRD_SEQ_ID_CLM , hidden:true },
            {name: 'fab_sn',          index: 'fab_sn',       	  label: PRD_SEQ_ID_TAG         ,  width: PRD_SEQ_ID_CLM},
            {name: 'prd_stat',        index: 'prd_stat',          label: PRD_STAT_TAG           ,  width: PRD_STAT_CLM},
            {name: 'lot_id',          index: 'lot_id',            label: LOT_ID_TAG             ,  width: LOT_ID_CLM},
            {name: 'pv_prd_grade',    index: 'pv_prd_grade',      label: '上判'             ,  width: PRD_GRADE_CLM,align:"center"},
            {name: 'ipqc_flg',        index: 'ipqc_flg',          label: 'IPQC'             ,  width: PRD_GRADE_CLM,align:"center"},
            {name: 'oqc_skip_flg',    index: 'oqc_skip_flg',      label: '返品'             ,  width: PRD_GRADE_CLM,align:"center"},
            {name: 'box_id',          index: 'box_id',            label: '转入箱号'             ,  width: BOX_ID_CLM},
            {name: 'mtrl_box_id_fk',  index:'mtrl_box_id_fk',     label: '来料箱号'   		      ,  width: BOX_ID_CLM},
            {name: 'pv_box_id_fk',    index:'pv_box_id_fk',       label: '入账箱号'   		      ,  width: BOX_ID_CLM},
            {name: 'qa_flg',          index: 'qa_flg',            label: '检验' 					      ,  width: QA_FLG_CLM},
            {name: 'judge',           index: 'judge',             label: JUDGE_TAG              ,  width: JUDGE_CLM}, //button
            {name: 'judge_result',    index: 'judge_result',      label: JUDGE_RESULT_TAG       ,  width: PRD_GRADE_CLM, hidden: true},
            {name: 'prd_grade',       index: 'prd_grade',         label: JUDGE_RESULT_TAG       ,  width: PRD_GRADE_CLM,align:"center"},
            {name: 'def_count',       index: 'def_count',         label: NG_CNT_TAG             ,  width: DEF_COUNT_CLM},
            {name: 'defect_detail',   index: 'defect_detail',     label: DEF_DTL_TAG            ,  width: JUDGE_CLM,align:"center"}, //button
            {name: 'th_judge_flg',    index: 'th_judge_flg',      label: '厚度判定'            ,  width: TH_FZ_CLM,align:"center"},
            {name: 'fz_flg',          index: 'fz_flg',            label: '方阻判定'            ,  width: TH_FZ_CLM,align:"center"},
            {name: 'remark_detail',   index: 'remark_detail',     label: '备注查看'            ,  width: TH_FZ_CLM,align:"center"},
           // {name: 'process_user',    index: 'process_user',      label: '开始人'               ,  width: USER_CLM},
           // {name: 'start_time'  ,    index: 'start_time',        label: START_TIME_TAG         ,  width: TIMESTAMP_CLM},
            {name: 'cr_proc',         index: 'cr_proc',           label: ''                     ,  width: PRD_STAT_CLM, hidden:true},

            {name: 'end_user'  ,      index: 'end_user',          label: '结束人'               ,  width: USER_CLM, hidden: true},
            {name: 'end_time'  ,      index: 'end_time',          label: END_TIME_TAG           ,  width: TIMESTAMP_CLM, hidden: true},
            {name: 'logon_usr'  ,     index: 'logon_usr',         label: '释放人'               ,  width: USER_CLM, hidden: true},
            {name: 'logon_timestamp', index: 'logon_timestamp',   label: '释放时间'             ,  width: TIMESTAMP_CLM, hidden: true},
            {name: 'logof_usr'  ,     index: 'logof_usr',         label: '上一站出账人'         ,  width: USER_CLM, hidden: true},
            {name: 'logof_timestamp', index: 'logof_timestamp',   label: '上一站出账时间'       ,  width: TIMESTAMP_CLM, hidden: true},
            {name: 'evt_user'  ,      index: 'evt_user',          label: '最近操作用户' ,  width: USER_CLM, hidden: true},
            {name: 'evt_timestamp',   index: 'evt_timestamp',     label: '最近操作时间'         ,  width: TIMESTAMP_CLM, hidden: true},
            {name: 'tft_buff_time',   index: 'tft_buff_time',     label: TFT_BUFF_TIME_TAG      ,  width: QTY_CLM, hidden: true},
            {name: 'cf_buff_time',    index: 'cf_buff_time',      label: CF_BUFF_TIME_TAG       ,  width: QTY_CLM, hidden: true},
            {name: 'sht_ope_msg',     index: 'sht_ope_msg',       label: '备注信息'             ,  width: SHT_OPE_MSG_CLM, hidden: true},
            {name: 'ds_recipe_id',    index: 'ds_recipe_id',      label: '来料备注'             ,  width: SHT_OPE_MSG_CLM, hidden: true},
            {name: 'group_id',        index: 'group_id',          label: '组代码'               ,  width: BOX_ID_CLM, hidden: true},
            {name: 'tft_buff_tool',   index: 'tft_buff_tool',     label: ''                     ,  width: 1, hidden: true},
            {name: 'cf_buff_tool',    index: 'cf_buff_tool',      label: ''                     ,  width: 1, hidden: true},
            {name: 'wo_id',           index: 'wo_id',             label: ''                     ,  width: 1, hidden: true},
            {name: 'pep_lvl',         index: 'pep_lvl',           label: ''                     ,  width: 1, hidden: true},
            {name: 'cr_proc',         index: 'cr_proc',           label: ''                     ,  width: 1, hidden: true},
            {name: 'cr_ope_id',       index: 'cr_ope_id',         label: ''                     ,  width: 1, hidden: true},
            {name: 'def_code'  ,      index: 'def_code',          label: ''                     ,  width: 1, hidden: true},
            {name: 'mdl_id_fk',       index: 'mdl_id_fk',         label: ''                     ,  width: 1, hidden: true},
            {name: 'x_axis_cnt_fk',   index: 'x_axis_cnt_fk',     label: ''                     ,  width: 1, hidden: true},
            {name: 'y_axis_cnt_fk',   index: 'y_axis_cnt_fk',     label: ''                     ,  width: 1, hidden: true},
            {name: 'cr_ope_no_fk',    index: 'cr_ope_no_fk',      label: ''                     ,  width: 1, hidden: true},
            {name: 'cr_ope_ver_fk',   index: 'cr_ope_ver_fk',     label: ''                     ,  width: 1, hidden: true},
            {name: 'cr_path_id_fk',   index: 'cr_path_id_fk',     label: ''                     ,  width: 1, hidden: true},
            {name: 'cr_path_ver_fk',  index: 'cr_path_ver_fk',    label: ''                     ,  width: 1, hidden: true},
            {name: 'cr_tool_id_fk',   index: 'cr_tool_id_fk',     label: ''                     ,  width: 1, hidden: true},
            {name: 'cus_id'       ,   index: 'cus_id'       ,     label: ''                     ,  width: 1, hidden: true}
        ];
        controlsQuery.mainGrd.grdId.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:140,
            width:domObj.$wipInfoDiv.width(),
            shrinkToFit:false,
            scroll:true,
            rownumWidth : true,
            resizable : true,
            rowNum:40,
            loadonce:true,
            fixed:true,
            viewrecords:true,
            multiselect : true,
            cmTemplate: { sortable: false },
            pager : controlsQuery.mainGrd.grdPgText,
            colModel: grdInfoCM
        });

        var defectGrdInfoCM = [
            {name: 'defect_code' ,   index: 'defect_code',    label: ''               ,   width: DEFECT_CODE_CLM,hidden:true},
            {name: 'proc_id'     ,   index: 'proc_id'    ,    label: ''               ,   width: PROC_ID_CLM,hidden:true},
            {name: 'ope_dsc'     ,   index: 'ope_dsc'    ,    label: '判定站点'       ,   width: OPE_DSC_CLM},
            {name: 'defDsc'      ,   index: 'defDsc'     ,    label:DEF_DESC_TAG       ,   width: DEFDSC_CLM},
            {name: 'defect_side' ,   index: 'defect_side',    label: DEF_LC_TAG             ,   width: DEFECT_SIDE_CLM },
            {name: 'position'    ,   index: 'position'   ,    label: DEF_POSITION_TAG ,   width: POSITION_CLM,hidden:true},
            {name: 'positionview'    ,   index: 'positionview'   ,    label: DEF_POSITION_TAG ,   width: POSITION_CLM},
            {name: 'defect_cnt'  ,   index: 'defect_cnt' ,    label: DEF_QTY_TAG           ,   width: DEFECT_CNT_CLM},
            {name: 'judge_timestamp',index: 'judge_timestamp',label: '判定时间'       ,   width: TIMESTAMP_CLM}
        ];
        controlsQuery.mainGrd.defectGrd.jqGrid({
            url:"",
            datatype:"local",
            mtype:"POST",
            height:120,
            width:controlsQuery.mainGrd.defectDiv.width(),
            shrinkToFit:false,
            scroll:true,
            rownumWidth : true,
            resizable : true,
            rowNum:40,
            loadonce:true,
            fixed:true,
            viewrecords:true,
            cmTemplate: { sortable: false },
            colModel: defectGrdInfoCM
        });
    };

    var shortCutKeyBind = function(){
        document.onkeydown = function(event) {
            if(event.keyCode == F1_KEY){
                btnQuery.f1.click();
                return false;
            }else if(event.keyCode == F2_KEY){
                btnQuery.process_start.click();
                return false;
            }else if(event.keyCode == F3_KEY){
                btnQuery.f6.click();
                return false;
            }else if(event.keyCode == F12_KEY){
                btnQuery.f8.click();
                return false;
            }else if(event.keyCode == F6_KEY){
                btnQuery.save_buff_time_btn.click();
                return false;
            }else if(event.keyCode == F4_KEY){
                btnQuery.qa_btn.click();
                return false;
            }else if(event.keyCode == F8_KEY){
                btnQuery.issue_move_out_btn.click();
                return false;
            }else if(event.keyCode == F9_KEY){
                btnQuery.f9.click();
                return false;
            }else if(event.keyCode == F7_KEY){
                btnQuery.f7.click();
                return false;
            }

            return true;
        };
    };

    /**
     * Other action bind
     */
    var otherActionBind = function(){
        //Stop from auto commit
        $("form").submit(function(){
            return false;
        });

        //Ope select change ==> tool auto refresh
        controlsQuery.opeSelect.change(function(){
            var ope_info,ope_info_ary;

            toolFunc.iniToolSelect();
            toolFunc.check_proc_func();
            controlsQuery.toolSelect.select2();

            ope_info = $.trim(controlsQuery.opeSelect.val());
            if(!ope_info){
                return false;
            }
            controlsQuery.swhOutOpeSelect.empty();
            controlsQuery.swhOutOpeSelect.append;
            controlsQuery.swhOutOpeSelect.select2();

            controlsQuery.swhOutOpeSelect.append("<option value= '' ></option>");
            ope_info_ary = ope_info.split("@");
            controlsQuery.swhOutOpeSelect.append("<option value="+ ope_info_ary[0] + "@" + ope_info_ary[1] + "@" + ope_info_ary[2] +">"+ ope_info_ary[3] +"</option>");

        });
    	controlsQuery.okOutCrrTxt[0].disabled = false;
		controlsQuery.okOutCrrTxt.focus();
        btnQuery.okOutBtn.click(function (){
        	controlsQuery.okOutCrrTxt[0].disabled = false;
    		controlsQuery.okOutCrrTxt.focus();
    		controlsQuery.okOutCrrTxt.val("");
    		controlsQuery.OkBoxMaxCntTxt.val("");
            controlsQuery.OkBoxCurCntTxt.val("");
            controlsQuery.OkBoxModeCntTxt.val("");
        });


        //Jump ope checkbox check
        controlsQuery.swhOpeChk.click(function(){
            toolFunc.swhOpeCheckClick();
        });

        //Auto query when press enter after keyin prdID
        controlsQuery.prdIdInput.keydown(function(event){
            $(this).val($(this).val().toUpperCase());
            if(event.keyCode === 13){
            	 var prd_seq_id = $.trim(controlsQuery.prdIdInput.val());
            	 if(!prd_seq_id){
            		 showErrorDialog("","玻璃代码不能为空！");
                     return false;
            	 }
            	 var wo_id = $.trim(controlsQuery.woIdSelect.val());
            	 if(!wo_id){
            		 //showErrorDialog("","请选择内部订单！");
                     return false;
            	 }
            	 var inTrxObj = {
                			trx_id     : "XPUNPACK",
                			action_flg : "X",
                			prd_seq_id : prd_seq_id,
                			wo_id      : wo_id,
                			box_id     : "box",
                			out_chk_flg:"Y"
                		};
                		var outTrxObj = comTrxSubSendPostJson(inTrxObj);
                		if(outTrxObj.rtn_code === VAL.NORMAL){
                  			if(outTrxObj.out_control_flg === "Y"){
               				  btnQuery.f6.showCallBackWarnningDialog({
                                 	errMsg : "此片玻璃编码不符合产品设定，是否继续投产",
                                    callbackFn : function(data) {
                                 	    if(data.result){
                                 	    	btnFunc.query_sht();                                                   	    }
                                     }
                                 });
               			    }else{
               				    btnFunc.query_sht();
               			    }
                		}
            }
        });

        //Ope select change ==> tool auto refresh
//        controlsQuery.buffToolGrpSelect.change(function(){
//            toolFunc.iniBuffToolSelect();
//        });

        //Query wo info when select wo
        controlsQuery.woIdSelect.change(function(){
        	var ope_info,ope_info_ary,proc_id,wo_id;
            wo_id = controlsQuery.woIdSelect.val();
            if (!wo_id){
        		controlsQuery.gradeSelectDiv.empty();
        		return false;
            }
            toolFunc.getWoInfoFunc(wo_id);
            toolFunc.showGRDButton(gCus_id);
            toolFunc.get_all_defect_func(gCus_id);
        });

        //查询产品id回车选中表格ZJH
        var triggeredId =[];
        controlsQuery.prdIdInput.keydown(function(event){
            if(event.keyCode == 13){
                //console.log("1-----:"+triggeredId);
                prdId = $("#prdIdInput").val();
                var arr=[];
                var ids=[];
                $("tbody tr").each(function(){
                    arr.push($(this).attr("id"));
                });
                for(var i=0;i<arr.length;i++){
                    //去除未定义id
                    if(typeof(arr[i])!='undefined'){
                        ids.push(arr[i]);
                    }
                }
                //遍历数组
                if(triggeredId!=null){
                    //console.log("2-----:"+triggeredId);
                    for(var i=0;i<triggeredId.length;i++) {
                        $(triggeredId[i]).trigger('click');
                    }
                    triggeredId=[];
                }
                for(var i=0;i<ids.length;i++){
                    //获取表格中产品id并和输入的产品id比较
                    if(prdId==($("#"+ids[i] +" :eq(3)").html())) {
                        $("#"+ids[i]).trigger('click');
                        triggeredId.push("#"+ids[i]);
                    }
                }
            }
        });



        //查询需入账箱子的信息
        controlsQuery.moveInBoxTxt.keydown(function(event){
            if(event.keyCode === 13){
            	 box_id = $.trim(controlsQuery.moveInBoxTxt.val());
                 if(!box_id){
                 	showErrorDialog("","请输入入账箱号！");
                     return false;
                 }
                 btnQuery.f1.click();
            }
        });

        //查询需转入箱子的信息
        controlsQuery.prdInBoxTxt.keydown(function(event){
            if(event.keyCode === 13){
                box_id = $.trim(controlsQuery.prdInBoxTxt.val());
                if(!box_id){
                	showErrorDialog("","请输入转入箱号！");
                    return false;
                }
                btnQuery.f1.click();
            }
        });

        //查询需来料箱子的信息
        controlsQuery.ppboxIdTxt.keydown(function(event){
            if(event.keyCode === 13){
            	box_id = $.trim(controlsQuery.ppboxIdTxt.val());
                if(!box_id){
                	showErrorDialog("","请输入来料箱号！");
                    return false;
                }
            	btnQuery.f1.click();
            }
        });
        shortCutKeyBind();

        $("#swhOpeChk").attr({"disabled":false});
        controlsQuery.yieldInfoDiv.hide();
        console.info("swhOpeChk set to disabled");
    };

    /**
     * Ini contorl's data
     */
    var iniContorlData = function(){
        toolFunc.clearInput();
        toolFunc.iniOpeSelect();
        toolFunc.iniRwkOpeSelect();
        toolFunc.iniToolSelect();
        //toolFunc.iniBuffToolGrpSelect();
        toolFunc.iniBuffToolSelect();
        toolFunc.iniWoIdSelect();//Wo info
        toolFunc.check_proc_func();
        toolFunc.initDelegate();
        toolFunc.getPrinters();
    };

    /**
     * Ini view, data and action bind
     */
    var initializationFunc = function(){
    	iniGridInfo();
        iniButtonAction();
        otherActionBind();
        toolFunc.get_all_defect_func();
//        controlsQuery.wip_wait_div.hide();
//        controlsQuery.wip_wait_chk[0].checked = false;
        iniContorlData();
    };

    initializationFunc();

    controlsQuery.okOutCrrTxt.keydown(function(e){
    	if(e.keyCode==13){
    		toolFunc.crrInputKeyDownFnc(btnQuery.okOutBtn,
    				controlsQuery.okOutCrrTxt,
		            controlsQuery.OkBoxMaxCntTxt,
    		        controlsQuery.OkBoxCurCntTxt,
    		        controlsQuery.OkBoxModeCntTxt);
    	}
	});


    function resizeFnc(){
    	comResize(controlsQuery.W,controlsQuery.mainGrd.fatherDiv,controlsQuery.mainGrd.grdId);

    	var offsetBottom, divWidth;
		divWidth = domObj.$window.width() - domObj.$defectInfoDiv.offset().left;
		domObj.$defectInfoGrd.setGridWidth(divWidth * 0.99 - 25);

		offsetBottom = controlsQuery.W.height() - controlsQuery.mainGrd.fatherDiv.offset().top;
		controlsQuery.mainGrd.fatherDiv.height(offsetBottom * 0.99);
		controlsQuery.mainGrd.grdId.setGridHeight(offsetBottom * 0.99 - 51);
		var dialogHeight = $("#wip_retain_Dialog").height();
		$("#modalBodyDiv").width(domObj.$window.width()*0.85);
		domObj.$retainBoxInfoDiv.width(domObj.$window.width() * 0.25);
		divWidth = domObj.$retainBoxInfoDiv.width();
		domObj.$retainBoxInfoDiv.height(dialogHeight * 0.65);
		domObj.$wipRetainInfoGrd.setGridWidth(divWidth * 0.99);
		domObj.$wipRetainInfoGrd.setGridHeight(dialogHeight * 0.65 - 60);

		domObj.$wipRetainDetailInfoDiv.width(domObj.$window.width() * 0.40);
		divWidth = domObj.$wipRetainDetailInfoDiv.width();
		domObj.$wipRetainDetailInfoDiv.height(dialogHeight * 0.65);
		domObj.$wipRetainDetailInfoGrd.setGridWidth(divWidth * 1.08);
		domObj.$wipRetainDetailInfoGrd.setGridHeight(dialogHeight * 0.65 - 60);

		divWidth = domObj.$boxInfoDiv.width();
		domObj.$boxInfoGrd.setGridWidth(divWidth * 0.99);

		divWidth = $("#spcDialog").width() - $("#dataItemEditDiv").width();
		//$("#spcDiv").width(divWidth - 80);
		$("#spcDialog_woGrd").setGridWidth(divWidth - 85);

		offsetBottom = $("#spcDialogDiv").height();
		//$("#spcDiv").height(offsetBottom - 10);
		$("#spcDialog_woGrd").setGridHeight(offsetBottom - 15);


		divWidth = $("#remarkDtlDialog").width();
		$("#remarkDtlDiv").width(divWidth - 80);
		$("#remarkDtlGrd").setGridWidth(divWidth - 85);

		offsetBottom = $("#remarkDtlDialogDiv").height();
		$("#remarkDtlDiv").height(offsetBottom - 20);
		$("#remarkDtlGrd").setGridHeight(offsetBottom - 55);
        $("#infoDiv").height($("#infoDiv").height() * 0.85);
        if(($(window).width() < 1200)){
            $("#gview_wipInfoGrd").children(".ui-jqgrid-bdiv").height("200px");
        }
        if(($(window).width() > 1200)){
            $("#gview_wipInfoGrd").children(".ui-jqgrid-bdiv").height("400px");
        }
    };
    resizeFnc();
    domObj.$window.resize(function() {                                                         
    	resizeFnc();                                                                             
	}); 
});