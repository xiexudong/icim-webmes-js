$(document).ready(function() {
	var mes_id  ;

  var prdItemInfoCM = [
        {name: 'prd_seq_id', index: 'prd_seq_id',label: PRD_SEQ_ID_TAG, width: 80 },
        {name: 'box_id'    , index: 'box_id'    ,label: BOX_TAG       , width: 80 },
        {name: 'mdl_id'    , index: 'mdl_id'    ,label: BOX_TAG       , width: 80 ,hidden:true },
        {name: 'path_id'   , index: 'path_id'   ,label: BOX_TAG       , width: 80 ,hidden:true },
        {name: 'path_ver'  , index: 'path_ver'  ,label: BOX_TAG       , width: 80 ,hidden:true },
        {name: 'ope_no'    , index: 'ope_no'    ,label: BOX_TAG       , width: 80 ,hidden:true },
        {name: 'ope_id'    , index: 'ope_id'    ,label: BOX_TAG       , width: 80 ,hidden:true },
        {name: 'ope_ver'   , index: 'ope_ver'   ,label: BOX_TAG       , width: 80 ,hidden:true },
        {name: 'proc_id'   , index: 'proc_id'   ,label: BOX_TAG       , width: 80 ,hidden:true },
      ];
  $("#prdSeqInfoGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:400,//TODO:需要根据页面自适应，要相对很高
      // width:"99%",
      autowidth:true,//宽度根据父元素自适应
      shrinkToFit:true,
      scroll:true,

      viewrecords:true,
      rownumbers  :true ,
      rowNum:20,
      rownumWidth : 20,

      resizable : true,
      loadonce:true,
      toppager: true ,
      fixed:true,
      pager : '#prdSeqInfoPg',
      pginput :true,
      fixed: true,
      cellEdit : true,
      cellsubmit : "clientArray",
      jsonReader : {
            // repeatitems: false
          },
    emptyrecords :true ,//当数据源为null时，显示为0
      colModel: prdItemInfoCM  
  })

  var itemInfoCM = [
        {name: 'data_id',        index: 'data_id',       label: MLITEM_ID_TAG,            width: 40},
        {name: 'data_group',     index: 'data_group',    label: MLITEM_GROUP_TAG,         width: 80 },
        {name: 'data_group_seq', index: 'data_group_seq',label: MLITEM_DATA_GROUP_SEQ_TAG,width: 117},
        {name: 'data_dsc',       index: 'data_dsc',      label: MLITEM_DSC_TAG,           width: 117},
        {name: 'data_value',     index: 'data_value',    label: MLITEM_VALUE_TAG,         width: 117,editable:true, formatter:'number',formatoptions:{thousandsSeparator: ','},editrules:{required:true,number:true}, editoptions:{size:10}},

        
      ];
  $("#itemInfoGrd").jqGrid({
      url:"",
      datatype:"local",
      mtype:"POST",
      height:400,//TODO:需要根据页面自适应，要相对很高
      // width:"99%",
      autowidth:true,//宽度根据父元素自适应
      shrinkToFit:true,
      scroll:true,

      viewrecords:true,
      rownumbers  :true ,
      rowNum:20,
      rownumWidth : 20,

      resizable : true,
      loadonce:true,
      toppager: true ,
      fixed:true,
      pager : '#itemInfoPg',
      pginput :true,
      fixed: true,
      cellEdit : true,
      cellsubmit : "clientArray",
      jsonReader : {
						// repeatitems: false
					},
	  emptyrecords :true ,//当数据源为null时，显示为0
      colModel: itemInfoCM  
  })
  // var ary = new Array();
  //  ary.push({data_id:'M0001',data_group:'testgroup1',data_dsc:'test1'});
  //  ary.push({data_id:'M0002',data_group:'testgroup2',data_dsc:'test2'});
  //  ary.push({data_id:'M0003',data_group:'testgroup2',data_dsc:'test3'});
  //  setGridInfo(ary,"#itemInfoGrd");
  var resetJqgrid = function(){
      $(window).unbind("onresize"); 
      $("#itemInfoGrd").setGridWidth($("#itemInfoDiv").width()*0.95); 
      $("#itemInfoGcrd").setGridHeight($("#itemInfoDiv").height()*0.80);     
      $(window).bind("onresize", this);  
  }
  function initFnc(){
  	 $("#itemDiv").css( {height     : ($(document).height()*0.7) } ) ;
  	resetJqgrid();
    itemRadioFnc();
    getOpeListFnc();
    getToolListFnc();
    $("#boxIDTxt").val("W0520618001");
    $("#toolIDSel").val("WG-GH-0001");
    $("#opeIDSel").val("0400");
  }
  initFnc();
  $(window).resize(function(){  
  	resetJqgrid();
  })
  function getOpeListFnc(){
    var inTrxObj ={
      trx_id     : "XPBISOPE" ,
      action_flg : "L"        
      // iary       : iary
    };
    setSelectObjByinTrx("#opeIDSel",inTrxObj,"ope_id");
  }
  function getToolListFnc(){
    var inTrxObj ={
      trx_id     : "XPBISTOL" ,
      action_flg : "L"        
      // iary       : iary
    };
    setSelectObjByinTrx("#toolIDSel",inTrxObj,"tool_id");
  }
  function itemRadioFnc(){
     var item = $("input[name='items']:checked").val();
     switch(item){
       case "lot":
          $("#lotIDTxt").attr({'disabled':false});
          $("#boxIDTxt").attr({'disabled':true});
          $("#prdSeqIdTxt").attr({'disabled':true});
          break;
       case "box" :
          $("#lotIDTxt").attr({'disabled':true});
          $("#boxIDTxt").attr({'disabled':false});
          $("#prdSeqIdTxt").attr({'disabled':true});
          break;
       case "prdSeq":
          $("#lotIDTxt").attr({'disabled':true});
          $("#boxIDTxt").attr({'disabled':true});
          $("#prdSeqIdTxt").attr({'disabled':false});
          break;
       default:
          $("#lotIDTxt").attr({'disabled':true});
          $("#boxIDTxt").attr({'disabled':true});
          $("#prdSeqIdTxt").attr({'disabled':true});
          alert("please choose one");
          break;
     }
  }
  $("input[name='items']").click(function(){
     itemRadioFnc();
  })
  function queryMLItem(prd_seq_id){
  }

  $("#boxIDTxt").keypress(function(event){
    if(event.keyCode==13){
       var inTrxObj ={
         trx_id     : 'XPINQBOX',
         action_flg : 'Q'       ,
         box_id     : $("#boxIDTxt").val()
       };
       var outTrxObj = comTrxSubSendPostJson(inTrxObj);
       if(outTrxObj.rtn_code == "0000000"){
          setGridInfo(outTrxObj.oary,"#prdSeqInfoGrd");
       }
    }
  });
  function f1QueryFnc(){
    // var sht_id = $("prdSeqIdTxt").val();
      var rowIDs  = $("#prdSeqInfoGrd").jqGrid("getDataIDs");
      if(rowIDs.length==0){
        //TODO:输入玻璃或box
        return false;
      }
      var rowData = $("#prdSeqInfoGrd").jqGrid("getRowData",rowIDs[0]);
      var iary = {
        tool_id_fk  : $("#toolIDSel").find("option:selected").text(),
        mdl_id_fk   : rowData.mdl_id   ,
        path_id_fk  : rowData.path_id  ,
        path_ver_fk : rowData.path_ver ,
        ope_no_fk   : rowData.ope_no   ,
        ope_id_fk   : $("#opeIDSel").find("option:selected").text() ,
        ope_ver_fk  : rowData.ope_ver  
      };
      var inTrxObj = {
        trx_id     : 'XPBISPAM' ,
        action_flg : 'Q'        ,
        iary       : iary       ,
        tbl_cnt    :  1
      };
      var outTrxObj = comTrxSubSendPostJson(inTrxObj);
      var oary ;
      if(outTrxObj.rtn_code == "0000000") {

        if(outTrxObj.tbl_cnt=1){
          oary = outTrxObj.oary;
        }else if( outTrxObj.tbl_cnt>1 ) {
          oary =outTrxObj.oary[0];
        }else{
          return false;
        }
        var iary1 ={
           tool_id_fk    : $("#toolIDSel").find("option:selected").text() ,      
           mes_id_fk     :  oary.mes_id ,
           rep_unit      :  'P'         ,//TODO
           data_pat      :  '00'         //TODO
        };
        var inTrxObj1 = {
           trx_id     : 'XPMLITEM' ,
           action_flg : 'Q'        ,
           iaryA      : iary1
        };
        var outTrxObj1 = comTrxSubSendPostJson(inTrxObj1);
        if(outTrxObj1.rtn_code == "0000000"){
           setGridInfo(outTrxObj1.oaryB,"#itemInfoGrd");
           mes_id = oary.mes_id;
        }
      }
  }
  $("#f1_btn").click(f1QueryFnc);
  $("#f8_btn").click(function(){
      var selectID  = $("#prdSeqInfoGrd").jqGrid("getGridParam","selrow");
      if( prd_seq_id ==""){
        showErrorDialog("003","请选择需要上报的玻璃");
        return false;
      }
      var prdSeqRowData = $("#prdSeqInfoGrd").jqGrid("getRowData",selectID)
      var prd_seq_id    =  prdSeqRowData.prd_seq_id;
      // if( prd_seq_id ==""){
      //   showErrorDialog("003","请选择需要上报的玻璃");
      //   return false;
      // }
  		var dirtyData = $("#itemInfoGrd").jqGrid("getChangedCells", "dirty");
  		var iaryAry = new Array();
  		var updateRowIds = new Array();
  		for(var i=0;i<dirtyData.length;i++){
  			var rowID = dirtyData[i].id;
  			var rowData = $("#itemInfoGrd").jqGrid("getRowData",rowID);
  			var iary = {
            data_group     : rowData.data_group,
            data_value     : dirtyData[i].data_value,
            data_group_seq : rowData.data_group_seq
          };
  			iaryAry.push(iary);
  		}
  		var tool_id = "C1POL220";
  		
  		//Send Tx
  		var inTrxObj = {
          	trx_id     : 'XPCMSRDT'        ,
            prd_seq_id :  prd_seq_id       ,           
          	tool_id    : $("#toolIDSel").find("option:selected").text()    ,
            data_pat_fk: '00'              ,
          	mes_id_fk  :  mes_id           ,
            path_id    :  prdSeqRowData.path_id,
            path_ver   :  prdSeqRowData.path_ver,
            ope_no     :  prdSeqRowData.ope_no,
            ope_id     :  prdSeqRowData.ope_id,
            ope_ver    :  prdSeqRowData.ope_ver,
            proc_id    :  prdSeqRowData.proc_id,
            rpt_usr    :  "L",//TODO
          	data_cnt   :  iaryAry.length   ,
          	iary       :  iaryAry       
          	
      	};
     	var outTrxObj = comTrxSubSendPostJson(inTrxObj);
    	if(outTrxObj.rtn_code == "0000000") {
    		 showSuccessDialog("数据上报成功");  
    	}

  })
  $("#test_btn").click(function(){
  	$.ajax({
	     type:"post",
	     url:"spc/findSPCGroupMain.action",
       data:{
            col_typ : "2",
            grp_name: "2"
       },
	     timeout:60000, 
	     async:false, //false代表等待ajax执行完毕后才执行alert("ajax执行完毕")语句;
	     beforeSend:function () {
	     },
	     complete:function () {// ajaxStop改为ajaxComplete也是一样的
	     },
	     success:function (data) {
	    	 alert(data.grp_name)
	     }
	    });
  })
})