$(document).ready(function() {

	var displayChartCount = 1;

	$(".firstLevelMenu>li").hover(function() {
		$(this).find("ul").addClass("hover");
		$(this).find("ul").stop(true, true).slideToggle();
	}, function() {
		$(this).find("ul").removeClass("hover");
		$(this).find("ul").stop(true, true).slideToggle();
	});

	$("#main #chartList ul li").click(function() {

		// test data
		var dataArray = new Array();
		var data = new Object();
		data.upl = 500;
		data.usl = 450;
		data.ucl = 300;
		data.target = 250;
		data.lcl = 200;
		data.lsl = 50;
		data.lpl = 0;
		data.cname = "1ADS01CE_M";
		data.ctitle = "R4 (11.6')(TEG hold)";
		data.gname = "PP1A1907";
		data.point = new Array();
		for ( var i = 0; i < 24; i++) {
			var point = new Object();
			point.inputTimestamp = "2013-06-27 23:18:12." + Math.ceil(Math.random() * 900000 + 100000);
			point.value = Math.ceil(Math.random() * 200 + 150);
			point.glassId = "4P78764397" + Math.ceil(Math.random() * 89 + 10);
			point.processEqptId = "A1COA2"  + Math.ceil(Math.random() * 89 + 10);
			point.measurementEqptId = "A1ADS0" + Math.ceil(Math.random() * 89 + 10);
			point.comment = "come into bowl";
			data.point.push(point);
		}

		dataArray.push(data);

		$(this).toggleClass("chartListSelectedBackgroundColor");

		switch (displayChartCount) {

		case 1:
			var chart = new Chart();
			chart.initializeCanvas("chart", 1222, 550);
			chart.initializeChart(dataArray[0], 1, 1);
			chart.drawPoint();
			chart.activateClickEvent();
			break;

		case 2:
			var chart = new Chart();
			chart.initializeCanvas("chart", 1222, 550);
			chart.initializeChart(dataArray[0], 2, 1);
			chart.drawPoint();
			chart.initializeChart(dataArray[0], 2, 2);
			chart.drawPoint();
			chart.activateClickEvent();
			break;

		case 3:
			var chart = new Chart();
			chart.initializeCanvas("chart", 1222, 550);
			chart.initializeChart(dataArray[0], 3, 1);
			chart.drawPoint();
			chart.initializeChart(dataArray[0], 3, 2);
			chart.drawPoint();
			chart.initializeChart(dataArray[0], 3, 3);
			chart.drawPoint();
			chart.activateClickEvent();
			break;
		}
		displayChartCount++;
	});
});