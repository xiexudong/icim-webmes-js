var SelectDom = {
	isEmptyContent : function($selectDomObj) {
		var val, txt;
		val = $selectDomObj.val();
		txt = $selectDomObj.find("option:selected").text();
		if (!val) {
			return true;
		} else if (!txt) {
			return true;
		} else if (txt === " ") {
			return true;
		}
		return false;
	},
	hasTxt : function($selectDomObj, txt) {

		var $options, optCnt, i;

		$options = $($selectDomObj.selector + " option");
		optCnt = $options.length;

		for (i = 0; i < optCnt; i++) {
			if ($options[i].text === txt) {
				break;
			}
		}

		return i < optCnt ? true : false;
	},
	hasValue : function($selectDomObj, val) {
		return $($selectDomObj.selector + " option[value='" + val + "']").length > 0 ? true : false;
	},
	initWithValTxt: function($selectDomObj,val,txt){
		$selectDomObj.empty();
		this.addSelect($selectDomObj, val, txt);
	},
	initWithSpace : function($selectDomObj) {
		$selectDomObj.empty();
		$selectDomObj.append("<option value='' > </option>");
	},
	addSelect : function($selectDomObj, val, txt) {
		txt = (typeof (txt) === "undefined" ? val : txt);
		if (val && !this.hasValue(val)) {
			$selectDomObj.append("<option value=" + val + ">" + txt + "</option>");
		}
		$selectDomObj.select2({
	    	theme : "bootstrap"
	    });
	},
	setSelect : function($selectDomObj, val, txt) {
		if (val !== "" && !val) {
			return false;
		}
		if (this.hasValue($selectDomObj, val)) {
			$selectDomObj.val(val).trigger("change");;
		}else if(this.hasTxt($selectDomObj, txt)){
			this.setSelectByTxt($selectDomObj,val,txt);
		}else{
			txt = txt === "" ? " " : txt
			this.addSelect($selectDomObj, val, txt);
			$selectDomObj.val(val).trigger("change");;
		}
		$selectDomObj.select2({
	    	theme : "bootstrap"
	    });
	},
	setSelectByTxt : function($selectDomObj, val, txt) {
		var $options, optCnt, i;

		$options = $($selectDomObj.selector + " option");
		optCnt = $options.length;

		for (i = 0; i < optCnt; i++) {
			if ($options[i].text === txt) {
				$selectDomObj.get(0).selectedIndex = i;
//				$selectDomObj.val($options[i].value);
			}
		}
		$selectDomObj.select2({
	    	theme : "bootstrap"
	    });

	},
	addSelectArr:function($selectDom,arr,valProp,txtProp,checkExistedFlg){
		var i,options;
		if(!arr){
			return false;
		}
		if(!$.isArray(arr)){
			arr = [arr];
		}
		txtProp = (typeof(txtProp)==="undefined" ? valProp :txtProp);
		if(typeof(checkExistedFlg)!=="undefined" && checkExistedFlg){
			for(i=0;i<arr.length;i++){
				this.addSelect($selectDom, arr[i][valProp], arr[i][txtProp]);
			}
		}else{
			/***
			 * 提高执行效率，避免多次给Select添加元素
			 * 使用array.join("") 代替"+"拼接字符串
			 */
			options = [];
			for(i=0;i<arr.length;i++){
				options[i] = "<option value ="+ arr[i][valProp] +">" + arr[i][txtProp] + "</option>";
			}
			$selectDom.append(options.join(""));
			$selectDom.select2({
		    	theme : "bootstrap"
		    });
		}
		$selectDom.select2();
	},
	setSelectArr:function($selectDomObj,arr,valProp,txtProp){
		var i,oldVal,oldTxt;
		
		if(!$.isArray(arr)){
			arr = [arr];
		}
		txtProp = (typeof(txtProp)=="undefined" ? valProp :txtProp);
		oldVal = $selectDomObj.val();
		oldTxt = $selectDomObj.find("option:selected").text();
		
		for(i=0;i<length;i++){
			this.addSelect($selectDomObj, arr[i][valProp], arr[i][txtProp]);
		}
		
		this.setSelect($selectDomObj, oldVal, oldTxt);
		$selectDomObj.select2({
	    	theme : "bootstrap"
	    });
	}

};